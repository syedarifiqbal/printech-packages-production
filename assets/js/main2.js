$(document).ready(function() {


	var userData=$('#userData');


	/**************************************************
	*=DATATABLE INITIALIZE.
	**************************************************/ 

	$('.boxData').dataTable();


	/**************************************************
	*=FLASH MESSAGE DISPLAY TO USER.
	**************************************************/

	if ( userData ) {
		userData
			.delay('500')
				.fadeIn(1500)
					.delay(5000)
						.fadeOut(5000,function(){
					    	$( this ).remove();
					    	var url = window.location.href;
							window.open(url);
							window.close();
						});
	};

	$( "#radio" ).buttonset();

	/**************************************************
	*					=ANIMATE INPUT TEXT ADD ON.
	**************************************************/ 
	// $('.form-control').removeAttr('placeholder');
	// $('.form-control').on('focus',function () {
	// 	var $this = $(this);

	// 	if ( $this.val() == '' ) {
	// 		$this.prev().animate({'top':'-30px'},300);
	// 		$this.css('border-bottom-color','#333');
	// 	};
	// });
	

	/**************************************************
	*					=AUTOCOMPLETE.
	**************************************************/ 
	$('.autocomplete').each(function(i,el){
		var el = $(el),
			url = el.data('method');

		el.autocomplete({
		
			source: function( request, response ) {
		        //console.log(el.data('method'));

		        jQuery.ajax({
					type: "POST",
					url: url,
					dataType: 'json',
					data: { term: request.term },
					success: function(data) {
						var parsed = JSON.parse(data);
		                var newArray = new Array();

		                parsed.forEach(function (entry) {
		                    var newObject = {
		                        id: entry.id,
		                        label: entry.label
		                    };
		                    newArray.push(newObject);
		                });

		                response(newArray);

					},

			        error: function(xhr, ajaxOptions, thrownError) {
			        	console.log(url);
			            container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>');
			        },
			        async: false
	    		});

			},

			minLength: 1,

			select: function( event, ui ) {
				$(this).prev().prev().val(ui.item.id);
			 },

	      	focus: function (event, ui) {
				$(this).prev().prev().val(ui.item.id);
				$(this).val(ui.item.label);
	      	}

		});
	});




    var overlay = {
        init : function () {
            
            $("<div/>",{
                id:"overlayContainer"
            }).appendTo('body');

            console.log('overlay appended');
        }
    }

    $('.wrapper').on('click','.findMaterial',overlay.init);




}); // end document ready function
