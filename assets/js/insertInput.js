$(document).ready(function(){


	function entrySerial(){
		var rows = $('.rows');
		for (var i = 1; i <= rows.length; i++) {
				$(this).children('td').find('input').val(i);
		};
	}


//******************************** ADD ENTRY ***********************************//
	var prtCounter = 1;
        $('#addPrinting').on('click',function(e){
            e.preventDefault();
            prtCounter ++;
            var row = '<tr class="rows"><td><input type="text" readonly name="sr[]" class="form-control" value="'+prtCounter+'"></td>';
            row += '<td><input type="text" autocomplete="off" name="pln_wt[]" class="form-control is_empty is_num" id="pln_wt"></td>';
            row += '<td><input type="text" autocomplete="off" name="prt_wt[]" class="form-control is_empty is_num" id="prt_wt"></td>';
            row += '<td><input type="text" autocomplete="off" name="meters[]" class="form-control" id="dynes"></td>';
            row += '<td><input type="text" autocomplete="off" name="bug_stop_time[]" class="form-control"></td>';
            row += '<td><input type="text" autocomplete="off" name="bug_start_time[]" class="form-control"></td>';
            row += '<td class="del"><textarea name="remarks[]" class="form-control" id="rmk" placeholder="Remarks" cols="75" rows="1" value=""></textarea>';
            row += '<a href="#" id="delBtn" style="font-size: 1.5em; display: none; overflow: hidden;" class="glyphicon glyphicon-remove delete" style="font-size:1.5em;"></a></td></tr>';
  			$('.delete').hide();
            $('.printing_entry').append(row);
            entrySerial()
        });
        $('#addRewinding').on('click',function(e){
            e.preventDefault();
            prtCounter ++;
            var row = '<tr class="rows"><td><input type="text" readonly name="sno[]" class="form-control" value="'+prtCounter+'"></td>';
            row += '<td><input type="text" autocomplete="off" name="wt_b4[]"class="form-control is_empty" placeholder="Weight Before Rewinding"></td>';
            row += '<td><input type="text" autocomplete="off" name="wt_after[]" class="form-control is_empty" placeholder="Weight After Rewinding"></td>';
            row += '<td class="del"><textarea name="remarks[]" class="form-control" id="rmk" placeholder="Remarks" cols="" rows="1"></textarea>';
            row += '<a href="#" id="delBtn" style="font-size: 1.5em; display: none; overflow: hidden;" class="glyphicon glyphicon-remove delete" style="font-size:1.5em;"></a></td></tr>';
  			$('.delete').hide();
            $('.printing_entry').append(row);
        });
		$('#addLam').on('click',function(e){
		    e.preventDefault();
		    prtCounter ++;
		    var row  = '<tr class="rows">';
		        row += '<td><input type="text" autocomplete="off" name="sno[]" class="form-control" readonly value="'+prtCounter+'"></td>';
		        row += '<td><input type="text" autocomplete="off" name="prt_role_wt[]" class="form-control is_empty is_num"></td>';
		        row += '<td><input type="text" autocomplete="off" name="lam_film_wt[]" class="form-control is_empty is_num"></td>';
		        row += '<td><input type="text" autocomplete="off" name="treatment_level[]" class="form-control is_empty is_num"></td>';
		        row += '<td><input type="text" autocomplete="off" name="Lamd_roll[]" class="form-control is_empty is_num"></td>';
		        row += '<td><input type="text" autocomplete="off" name="mtr[]" class="form-control is_empty is_num"></td>';
		        row += '<td><input type="text" autocomplete="off" name="gsm[]" class="form-control is_num" id="wt_gn"></td>';
		        row += '<td class="del"><textarea name="remarks[]" class="form-control" id="rmk" placeholder="Remarks" cols="" rows="1" value=""></textarea>';
            	row += '<a href="#" id="delBtn" style="font-size: 1.5em; display: none; overflow: hidden;" class="glyphicon glyphicon-remove delete" style="font-size:1.5em;"></a></td>';
		        row += '</tr>';
  			$('.delete').hide();
		    $('.printing_entry').append(row);
		});
		$('#addGlue').on('click',function(e){
		    e.preventDefault();
		    prtCounter ++;
		    var row  = '<tr class="issueReturn rows2">';
		        row += '<td>';
			    row += '<select class="form-control" name="glue_name[]">';
			    row += '<option>Water Base</option>';
			    row += '<option>Solvent Base</option>';
			    row += '<option>Ethyle</option>';
			    row += '<option>Glue</option>';
			    row += '<option>Hardner</option>';
			    row += '<option>Mixture</option>';
			    row += '</select>';
			    row += '</td>';
		        row += '<td><input type="text" autocomplete="off" name="type[]" class="form-control is_empty"></td>';
		        row += '<td><input type="text" autocomplete="off" name="code[]" class="form-control is_empty is_num"></td>';
		        row += '<td><input type="text" autocomplete="off" name="t_rcv[]" class="form-control is_empty is_num receive"></td>';
		        row += '<td><input type="text" autocomplete="off" name="t_rtn[]" class="form-control is_num return"></td>';
		        row += '<td class="del2"><input type="text" autocomplete="off" name="consume[]" readonly class="form-control balance" value="-">';
            	row += '<a href="#" id="delBtn2" style="font-size: 1.5em; display: none; overflow: hidden;" class="glyphicon glyphicon-remove delete2" style="font-size:1.5em;"></a></td>';
		        row += '</tr>';
  			$('.delete2').hide();
		    $('.addGlue_entry').append(row);
		});
		$('#addSlitting').on('click',function(e){
		    e.preventDefault();
		    prtCounter ++;
		    var row  = '<tr class="rows">';
		        row += '<td><input type="text" readonly name="sno[]" class="form-control" value="'+prtCounter+'"></td>';
		        row += '<td><input type="text" autocomplete="off" name="wt_b4[]"class="form-control is_empty"></td>';
		        row += '<td><input type="text" autocomplete="off" name="wt_after[]" class="form-control is_empty"></td>';
		        row += '<td class="del"><textarea name="remarks[]" class="form-control" cols="" rows="1" value=""></textarea>';
            	row += '<a href="#" id="delBtn" style="font-size: 1.5em; display: none; overflow: hidden;" class="glyphicon glyphicon-remove delete" style="font-size:1.5em;"></a></td>';
		        row += '</tr>';
  			$('.delete').hide();
		    $('.slitting_entry').append(row);
		});


//******************************** ADD ENTRY ***********************************//
});