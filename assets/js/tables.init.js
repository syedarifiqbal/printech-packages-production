(function ($) {
	
	$(document).on('click', '.activate', function(event) {
		if (!confirm('Please make sure you want to reactivate the record!')) {
			event.preventDefault();
		}
	});
	
	$(document).on('click', '.inactivate', function(event) {
		if (!confirm('Please make sure you want to inactivate the record!')) {
			event.preventDefault();
		}
	});
	
	$(document).on('click', '.delete, .delete-entry', function(event) {
		if (!confirm("Please make sure you want to delete the record.\nThis will delete all associated record!")) {
			event.preventDefault();
		}
	});

	var tableButtons = [
            {
              extend: "copy",
              className: "btn-sm"
            },
            {
              extend: "csv",
              className: "btn-sm"
            },
            {
              extend: "excel",
              className: "btn-sm"
            },
            {
              extend: "pdfHtml5",
              className: "btn-sm"
            },
            {
              extend: "print",
              className: "btn-sm"
            },
          ];
    var categoryOptions = {
      	  "dom": "Bfrtip",
      	  "processing":true,
    	    "serverSide":true,
    	    "order":[],
      		"ajax": {
      			url: $('#ajax-url').val(),
      			type:"POST"
      		},
        	buttons: tableButtons,
        	responsive: true
    };

	var handleDataTableButtons = function() {
      if ($("#inactive_datatable").length) {
          $("#inactive_datatable").DataTable(categoryOptions);
          // $("#inactive_datatable").css('min-width', 'style');
      }
      if ($("#active_datatable").length) {
          categoryOptions.ajax.url = categoryOptions.ajax.url+'1'
          $("#active_datatable").DataTable(categoryOptions);
          // $("#active_datatable").css('min-width', 'style');
      }
    };

    TableManageButtons = function() {
      "use strict";
      return {
        init: function() {
          handleDataTableButtons();
        }
      };
    }();

    TableManageButtons.init();

})(jQuery);