$(document).ready(function() {
	$('html').fadeIn('slow');
	// Hide Calculate Button.
	$('#calMaterial').hide();
	// Date Picker
	$('.date').datepicker({
			// prevText: "<",
			// nextText: ">",
			// inline: true,
			weekHeader: 'W',
			dateFormat: 'dd/mm/yy'
		});

// Initilize Tool Tip
	$('[data-toggle="tooltip"]').tooltip({'placement': 'top'});
// Dashboard Slide
	$('.sum-list').hide();
	$(document).on('click', '.sum-icon', function(){
		var el=$(this).closest('.sum-news').find('.sum-list')
			news=$(this).closest('.sum-news');
 
		if ( $(el).is(':visible') ) {
			$('.sum-list').slideUp('fast');
			$(news).removeClass('ic');
			$('.sum-icon').removeClass('ic');
		}else{
			$('.sum-list').slideUp('fast');
			el.slideToggle();
			$(news).addClass('ic'); 
			$('.sum-icon').addClass('ic');
		}
	});

	// $('#sort').dataTable();
	$(".sortableTable").dataTable({
		  "aaSorting": [],
		  "columns": [
		    { "width": "50px" },
		    { "width": "50px" },
		    null,
		    { "width": "100px" }
		  ]
		});

	$("#jobList").dataTable({
		  "columns": [
		    { "width": "80px" },
		    { "width": "50px" },
		    null,
		    { "width": "100px" },
		    { "width": "80px" }
		  ]
		});
	// CREATE SEARCH ICONE
	$('#jobList, #sortableTable').prev().append('<span class="searchIcon glyphicon glyphicon-search"></span>');


	// open new window
	// $('body').on('click',function(){ window.open("printing.php","popupWindow", "width=600,height=600,scrollbars=yes"); });
	$('.completedLink').on('click',function(e){
	    e.preventDefault();
	    var link = $(this).attr('href');
	    window.open(link,"popupWindow", "width=1280,height=800,scrollbars=yes");
	});

		$('.completed').on('click',function(e){
	    e.preventDefault();
	    var link = $(this).attr('href');
	    window.open(link,"popupWindow", "width=1280,height=800,scrollbars=yes");
	});


    // CHECKING JOB IS WAITING OR NOT
	$(document).on('change','.waiting',function(){
	    var checked = ($(this).is(':checked')) ? true:false;
	    if(checked){
	        $(this).parent().next().attr('placeholder',"Why? please write Reason Here");
	    }else{
	        $(this).parent().next().attr('placeholder',"Okay Feel Free to submit the form");
	    }
	})

	// menu
	// $('.nav-container').removeAttr('id','$navvv');

	// var ri8 = $('#mainNavRight').width();
	// var left = $('#mainNavLeft').width();

	// $(window).resize(function () {
	// 	if ( $(window).width() <= (ri8 + left) ) {
	// 		$('#navvv').slicknav({
	// 			duplicate : false
	// 		});
	// 	}else{
	// 		$('.nav-container').removeAttr('id','navvv');
	// 	}
	// });


	jQuery('.waiting_features').on('click',function(e){
		e.preventDefault();
		alert("This Option Is Under Process. Please Wait...");
	});
// SETTING FOOTER POSITION
	var winHeight = $(window).height(),
		footerHeight = $('.gry').height(),
		headHeight = $('.org').height();
	$('.main').css('minHeight',winHeight-footerHeight-headHeight + "px");

	$(window).on('scroll',function(){
		var winHeight = $(window).height(),
		footerHeight = $('.gry').height(),
		headHeight = $('.org').height();
		$('.main').css('minHeight',winHeight-footerHeight-headHeight + "px");
	});

//************************** CALCULATION **************************************//

	function getRatio(selector,mic){
		var select = $(selector).val();
		var micron = $(mic).val();
		if ( select != 'Choose Material' ) {
				var density  = $(selector).find(':selected').data('density');
			}else{
				var density  = 0;
			}//endif
		return micron*density;

	}//getRatio end

	$('#material01, #material01Mic').on('change',function(){
		var el = $('#material01').find(':selected').text();
		var mic = $('#material01Mic').val();
		jQuery('#calMaterial').fadeIn();

		if ( el != 'Choose Material' && mic != '' && mic != 0 ) {
			$('#calMaterial').prop("disabled", false);
		}else{
			$('#calMaterial').attr("disabled","ture");
		}	
	});


	jQuery('#calMaterial').on('click',function(e){
		e.preventDefault();
		// getting order quantity
		var orderQuantity = $('#quantity').val();
		// alert(orderQuantity);

		// getting total ratio
		var ratio01 = getRatio('#material01','#material01Mic');
		var ratio02 = getRatio('#material02','#material02Mic');
		var ratio03 = getRatio('#material03','#material03Mic');
		var ratio04 = getRatio('#material04','#material04Mic');
		totalRatio = ratio01 + ratio02 + ratio03 + ratio04;

		// getting indivisual weight
		var structure01 = Number(ratio01 / totalRatio * orderQuantity).toFixed(2);
		var structure02 = Number(ratio02 / totalRatio * orderQuantity).toFixed(2);
		var structure03 = Number(ratio03 / totalRatio * orderQuantity).toFixed(2);
		var structure04 = Number(ratio04 / totalRatio * orderQuantity).toFixed(2);

		// setting value
		$('#materialWeight01').attr('value',structure01);
		$('#materialWeight02').attr('value',structure02);
		$('#materialWeight03').attr('value',structure03);
		$('#materialWeight04').attr('value',structure04);
	});

//************************** end CALCULATION **************************************//


//************************** SHOW OR HIDE OPTIONAL MATERIAL **************************************//
	$('.optionalMaterial,.optionalMaterial2,.optionalMaterial3').hide();

	$('#noLamination').on('change',function(){
		var noLam = $(this).find(':selected').text();
		if( noLam == 'Select Lamination' ){
			$('.optionalMaterial,.optionalMaterial2,.optionalMaterial3').fadeOut();
		}
		else if ( noLam == 1 ) {
			$('.optionalMaterial,.optionalMaterial2,.optionalMaterial3').hide();
			$('.optionalMaterial').fadeIn('slow');
		}
		else if ( noLam == 2 ) {
			$('.optionalMaterial,.optionalMaterial2,.optionalMaterial3').hide();
			$('.optionalMaterial').fadeIn('slow');
			$('.optionalMaterial2').fadeIn('slow');
		}
		else if ( noLam == 3 ) {
			$('.optionalMaterial,.optionalMaterial2,.optionalMaterial3').hide();
			$('.optionalMaterial').fadeIn('slow');
			$('.optionalMaterial2').fadeIn('slow');
			$('.optionalMaterial3').fadeIn('slow');
		}
	});

//************************** end SHOW OR HIDE OPTIONAL MATERIAL **************************************//



//********************** Date Wise Stock Report ******************************//
	    $('#to, #from').on('change', function(e){
	    	var to = $('#to').val();
	    	var from = $('#from').val();
	    	var url     = 'to=' + to + '&from=' + from;
	    	if ( to != '' && from != '' ) {
		        $.ajax({
			        type : 'GET',
			        data : url,
			        url  : 'date_wise_stock_info.php',
			        cache: false,
			        success: function(data){
		               			$('#dwsr').hide();
			               		$('#dwsr').html(data);
			               		$('#dwsr').fadeIn();
			                 },//success
			        error: function(){
			        	alert('Sorry There Were Some Problem');	
			        }
		        });//ajax
	        };
		});
//********************** Date Wise Stock Report ******************************//


//********************** NEAREST Stock Report ******************************//
	    $('#Nmm, #Nmic').on('change', function(e){
	    	var Nmm = $('#Nmm').val();
	    	var Nmic = $('#Nmic').val();
	    	var url     = 'mm=' + Nmm + '&mic=' + Nmic;
	    	if ( Nmm != '' && Nmic != '' ) {
		        $.ajax({
			        type : 'GET',
			        data : url,
			        url  : 'nearest_stock_info.php',
			        cache: false,
			        success: function(data){
		               			$('#dwsr').hide();
			               		$('#dwsr').html(data);
			               		$('#dwsr').fadeIn();
			                 },//success
			        error: function(){
			        	alert('Sorry There Were Some Problem');	
			        }
		        });//ajax
	        };
		});
//********************** NEAREST Stock Report ******************************//


//********************** SIZE Wise Stock Report ******************************//
	    $('#mm, #mic, #materialType').on('change', function(e){
	    	var mType = $('#materialType').val();
	    	var mm = $('#mm').val();
	    	var mic = $('#mic').val();
	    	var url     = 'mm=' + mm + '&mic=' + mic + '&type=' + mType;
	    	if ( mm != '' && mic != '' ) {
		        $.ajax({
			        type : 'GET',
			        data : url,
			        url  : 'size_wise_stock_info.php',
			        cache: false,
			        success: function(data){
		               			$('#dwsr').hide();
			               		$('#dwsr').html(data);
			               		$('#dwsr').fadeIn();
			                 },//success
			        error: function(){
			        	alert('Sorry There Were Some Problem');	
			        }
		        });//ajax
	        };
		});
//********************** SIZE Wise Stock Report ******************************//


//********************** SIZE Wise Stock Report ******************************//
$( "#radio" ).button();
//********************** SIZE Wise Stock Report ******************************//


//********************** find job details for quality checker ******************************//
	    $('#qcJobCode').on('keyup', function(e){
	    	var jobCode = $(this).val();
	    	var url     = 'jobCode=' + jobCode;
		    if( isNaN(jobCode) ){
				alert('Sorry Invalid Job Code Please Type Correct Job Code.');	
			}
			else if(jobCode.length >= 3) {
		        $.ajax({
			        type : 'GET',
			        data : url,
			        url  : 'job_info_qc.php',
			        cache: false,
			        success: function(data){
		               			$('.job-info').hide();
			               		$('.job-info').html(data);
			               		$('.job-info').slideDown();
			                 },//success
			        error: function(){
			        	alert('Sorry There Were Some Problem');	
			        }
		        });//ajax
		    }
		});
//********************** end find job details for quality checker ******************************//

//********************** find job details for quality checker ******************************//
	    $('#dispatchJobCode').on('keyup', function(e){
	    	var jobCode = $(this).val();
	    	var url     = 'jobCode=' + jobCode;
		    if( isNaN(jobCode) ){
				alert('Sorry Invalid Job Code Please Type Correct Job Code.');	
			}
			else if(jobCode.length >= 3) {
		        $.ajax({
			        type : 'GET',
			        data : url,
			        url  : 'job_info_dispatch.php',
			        cache: false,
			        success: function(data){
		               			$('.job-info').hide();
			               		$('.job-info').html(data);
			               		$('.job-info').slideDown();
			                 },//success
			        error: function(){
			        	alert('Sorry There Were Some Problem');	
			        }
		        });//ajax
		    }
		});
//********************** end find job details for quality checker ******************************//


//********************** BALANCE CALCULATION FOR GLUE ******************************//

	$(document).on('blur','.receive, .return', function()
     {
        var $row = $(this).closest(".issueReturn");
        var totalRcv = parseInt($row.find('.receive').val()) || 0;
        var totalRtn = parseInt($row.find('.return').val()) || 0;
        $row.find('.balance').val(totalRcv - totalRtn);
     });

//********************** END BALANCE CALCULATION FOR GLUE ******************************//


	var table = $('#entryTable'),
		firstRow = table.find('tr').eq(1).hide().find('input, textarea').removeClass('er error is_empty is_num'),
		firstRowInUpdateForm = $('.updateForm').find('tr').eq(1).show(),
    	value = 1,
    	addButton = $('#addEntry');
          
      	addButton.on('click',function(e){
            e.preventDefault();
            var table = $('#entryTable'),
                rows = table.find('tr'),
                lastRow = table.find('tr:last'),
                firstRow = rows.eq(1).clone();

            firstRow.insertAfter( lastRow ).removeAttr('style');
            $.each(table.find('tr.rows'),function(i){
                var $this = $(this),
                    input = $this.find('td:first').find('input');
                    input.val(i);
                // console.log(i);
            });
      	});

      	if ( !$('#addEntry').closest('table').hasClass('updateForm') ) {
      		$('#addEntry').click();
      	};
    
    $('.updateForm').find('tr').eq(1).hide().find('input, textarea').val('').removeClass('er error is_empty is_num');


//********************** Delete Entries From Form ******************************//

  $('.delete , .delete2').hide();
  $('#deleteEntry').on("click",function(e)
    {
      e.preventDefault();
      var totalEntries = $('.rows');
      if(totalEntries.length > 2) $('.delete').fadeToggle('fast'); // make change 1 to 2
    });
  $(document).on("click",'#delBtn',function(e)
    {
        e.preventDefault();
        if (confirm("Realy Wanna Delete ???"))
        {
          var row = $(this).closest('.rows');
          row.remove();
          $('.delete').fadeOut();
        }else{
          $('.delete').fadeOut();
        }
    });

  $('#deleteEntry2').on("click",function(e)
    {
      e.preventDefault();
      var totalEntries = $('.rows2');
      if(totalEntries.length > 1) $('.delete2').fadeToggle('fast');
    });
  $(document).on("click",'#delBtn2',function(e)
    {
        e.preventDefault();
        if (confirm("Realy Wanna Delete ???"))
        {
          var row = $(this).closest('.rows2');
          row.remove();
          $('.del2 .delete2').fadeOut();
        }else{
          $('.del2 .delete2').fadeOut();
        }
    });

//********************** End Delete Entries From Form ******************************//

//********************** FORM VALIDATION ******************************//

        $('#formToSubmit').on('submit',function(e){
        	var error = false;
       		$('.is_empty, .is_checked, .is_select').removeClass('er error');

        	$('.is_empty').each(function () 
        	{
        		if($.trim($(this).val()) == ""){
        			$(this).addClass('er error');
        			error = true;
        		}
        	});

        	$('.is_num').each(function () 
        	{
        		var value = $( this ).val();
       			// $(this).removeClass('er error');
        		if( isNaN( value ) ){
        			$(this).addClass('er error');
        			error = true;
        		}
        	});

        	$('.is_select').each(function () 
        	{
        		var value = $( this ).find(':selected').text();
        		if( value == 'Choose Machine' || value == 'Please Choose...' || value == 'Select Hold Stage' || value == 'Select Location' ){
        			$(this).addClass('er error');
        			error = true;
        		}
        	});

        	$('.is_checked').each(function () 
        	{
        		if( !$('.is_checked').prop('checked') ){
        			$(this).addClass('er error');
        			error = true;
        		}
        	});

        	if(error){
        		e.preventDefault();
        	}else{
				$('#entryTable').find('tr').eq(1).remove();
        	}
        });

//********************** END FORM VALIDATION ******************************//

//********************** AUTO COMPLETE ******************************//

  $('#supplierField').autocomplete({ 
		minLength: 0,
		source : 'supplier_chk.php',
		focus: function( event, ui ) {
		$( "#supplierField" ).val( ui.item.label );
		return false;
		},
		select: function( event, ui ) {
		$( "#supplierField" ).val( ui.item.label );
		$( "#supplierId" ).val( ui.item.id );

		return false;
		}
  });
  $('#materialType').autocomplete({
		minLength: 0,
        source : 'material_chk.php',
        focus: function( event, ui ) {
			$( "#materialType" ).val( ui.item.label );
				return false;
			}
  });

  $('#material').autocomplete({
		minLength: 0,
        source : 'material_chk.php?size=true',
        focus: function( event, ui ) {
		$( "#material" ).val( ui.item.label );
		return false;
		},
		select: function( event, ui ) {
		$( "#material" ).val( ui.item.label );
		$( "#materialId" ).val( ui.item.id );

		return false;
		}
  });

  $('#job_name').autocomplete({
		minLength: 0,
        source : 'chk_job_name.php',
        focus: function( event, ui ) {
		$( "#job_name" ).val( ui.item.label );
			return false;
		},
		select: function( event, ui ) {
			var job_code = ui.item.id,
			    depart = $('input[name=job_code]').data('depart');

			$( "#job_name" ).val( ui.item.description );
			$( "#hiddenJobCode" ).val( job_code );

		        $.ajax({
			        type : 'GET',
			        data : {'job_code':ui.item.id,'depart':depart},
			        url  : 'job_info.php',
			        cache: false,
			        success: function(data){
               			$('.job-info').hide();
	               		$('.job-info').html(data);
	               		$('.job-info').fadeIn();
	                },//success
			        error: function(){
			        	alert('Sorry There Were Some Problem with loading job info. please contact to your admin.');	
			        }
		        });//ajax

			return false;
		} // end select method
  });

//********************** NET WIGHT CALCULATION **************************//

	$(document).on('keyup','#gross,#tare',function(){
	    var $gross= $('#gross').val(),
	        $tare= $('#tare').val();
	    $('#net').val($gross-$tare);
	});

//********************** END AUTO COMPLETE ******************************//
	var count = 1;
	$('.zoomBtn').on(
		'click',
		function(){
			count+=.25;
			if( count > 1.5 ){
				count=1.5;
			}else if( count <= 1.5 ){
				$('.zoom').css({
				  '-webkit-transform' : 'scale('+count+')',
				  '-moz-transform'    : 'scale('+count+')',
				  '-ms-transform'     : 'scale('+count+')',
				  '-o-transform'      : 'scale('+count+')',
				  'transform'         : 'scale('+count+')'
				});
			}
		}
	);
	$('.zoomOutBtn').on(
		'click',
		function(){
			count-=.25;
			if( count > 0.5 ){
				$('.zoom').css({
				  '-webkit-transform' : 'scale('+count+')',
				  '-moz-transform'    : 'scale('+count+')',
				  '-ms-transform'     : 'scale('+count+')',
				  '-o-transform'      : 'scale('+count+')',
				  'transform'         : 'scale('+count+')'
				});
			}else if( count < .5 ){
				count=.5;
			}
		}
	);


}); // end document ready function
