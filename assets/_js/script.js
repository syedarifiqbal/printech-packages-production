$(document).ready(function() {


	/*** SET HEIGHT BASED ON VIEW PORT ***/				
	function thirty_pc() {
	var height = $(window).height();
	var thirtypc = (100 * height) / 100;
	thirtypc = parseInt(thirtypc) + 'px';
	$(".right-bar").css('height',thirtypc);
	$(".menu").css('height',thirtypc);
	}
	$(document).ready(function() {
		thirty_pc();
		$(window).bind('resize', thirty_pc);
	});		

	/*** Account Toogle ***/	
	$('#account').click(function() {
	$('.account2').slideToggle('slow');
	return false;
	});
	
	/*** User Online Toogle ***/	
	$('#user-online').click(function() {
	$('.user-online2').slideToggle('slow');
	return false;
	});
	
	/*** Disk Usage Toogle ***/	
	$('#disk-usage').click(function() {
	$('.disk-usage').slideToggle('slow');
	return false;
	});
	
	/*** Pending Task Toogle ***/	
	$('#pending-task').click(function() {
	$('.pending-task').slideToggle('slow');
	return false;
	});
	
	/*** Notification Toogle ***/	
	$(".notification-btn").click( function(){
	$('.message').fadeOut();
	$(this).next('.notification').fadeToggle();
	});	
	
	/*** Message Toogle ***/	
	$(".message-btn").click( function(){
	$(this).next('.message').fadeToggle();
	$('.notification').fadeOut();
	});	
	
	/*** Upload Files Toogle ***/	
	$(".upload-btn").click( function(){
	$(this).next('.upload-files').fadeToggle();
	$('.message').fadeOut();
	$('.notification').fadeOut();
	});	

	
	/*** Responsive Menu  ***/
	$(".responsive-menu ul li").click( function(){
	$("ul",this).slideToggle();
	});		

	/*** Responsive Menu  ***/
	$(".responsive-menu-dropdown").click( function(){
	$(".responsive-menu > ul").slideToggle();
	});
	/*** Responsive Menu  ***/
	$(".right-bar-btn-mobile").click( function(){
	$(".right-bar").slideToggle('slow');
	});

	
	/*** Visitor Widget Tab ***/	
	$('body').on("click",".tabs-menu a",function(event) {
		event.preventDefault();
		$(this).parent().addClass("current");
		$(this).parent().siblings().removeClass("current");
		var tab = $(this).attr("href");
		$(".tab-content").not(tab).css("display", "none");
		$(tab).fadeIn();
	});
	
	
	/*** Gallery Delete Function  ***/
	$(".hide-btn").click( function(){
	$(this).parent().parent().parent().parent().parent().fadeOut();
	});	
		
});

$(document).ready(function(){ 	
	
	/*** Carousal Widget ***/
	$('.slidewrap').carousel({
	slider: '.slider',
	slide: '.slide1',
	slideHed: '.slidehed',
	nextSlide : '.next',
	prevSlide : '.prev',
	addPagination: false,
	addNav : false
	});

	/** Profile Tab **/
	$('#tab-content div').hide();
	$('#tab-content div:first').show();

	$('#nav1 li').click(function() {
	$('#nav1 li a').removeClass("active");
	$(this).find('a').addClass("active");
	$('#tab-content div').hide();

	var indexer = $(this).index(); //gets the current index of (this) which is #nav1 li
	$('#tab-content div:eq(' + indexer + ')').fadeIn(); //uses whatever index the link has to open the corresponding box 
	});
});	

/*** Sortable Table  ***/
		$(document).ready(function(){

		$(".sortable-table th").click(function(){
		sort_table($(this));
		});

		});

		function sort_table(clicked){
		var current_table = clicked.parents(".sortable-table"),
		sorted_column = clicked.index(),
		column_count = current_table.find("th").length,
		sort_it = [];

		current_table.find("tbody tr").each(function(){
		var new_line = "",
		sort_by = "";
		$(this).find("td").each(function(){
		if($(this).next().length){
		new_line += $(this).html() + "+";
		}else{
		new_line += $(this).html();
		}
		if($(this).index() == sorted_column){
		sort_by = $(this).text(); 
		}
		});

		new_line = sort_by + "*" + new_line;
		sort_it.push(new_line);
		//console.log(sort_it);

		});

		sort_it.sort();
		$("th span").text("");
		if(!clicked.hasClass("sort-down")){
		clicked.addClass("sort-down");
		clicked.find("span").text("â–¼");
		}else{
		sort_it.reverse();
		clicked.removeClass("sort-down");
		clicked.find("span").text("â–²");
		}

		$("#country-list tr:not('.country-table-head')").each(function(){
		$(this).remove();
		});

		$(sort_it).each(function(index, value) {
		$('<tr class="current-tr"></tr>').appendTo(clicked.parents("table").find("tbody"));
		var split_line = value.split("*"),
		td_line = split_line[1].split("+"),
		td_add = "";

		//console.log(td_line.length);
		for (var i = 0; i < td_line.length; i++){
		td_add += "<td>" + td_line[i] + "</td>";
		}
		$(td_add).appendTo(".current-tr");
		$(".current-tr").removeClass("current-tr");

});
}

		
		
		
$(document).ready(function() {
  $('#reportrange').daterangepicker(
	 {
		startDate: moment().subtract('days', 29),
		endDate: moment(),
		minDate: '01/01/2012',
		maxDate: '12/31/2014',
		dateLimit: { days: 60 },
		showDropdowns: true,
		showWeekNumbers: true,
		timePicker: false,
		timePickerIncrement: 1,
		timePicker12Hour: true,
		ranges: {
		   'Today': [moment(), moment()],
		   'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
		   'Last 7 Days': [moment().subtract('days', 6), moment()],
		   'Last 30 Days': [moment().subtract('days', 29), moment()],
		   'This Month': [moment().startOf('month'), moment().endOf('month')],
		   'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		},
		opens: 'left',
		buttonClasses: ['btn btn-default'],
		applyClass: 'btn-small btn-primary',
		cancelClass: 'btn-small',
		format: 'MM/DD/YYYY',
		separator: ' to ',
		locale: {
			applyLabel: 'Submit',
			fromLabel: 'From',
			toLabel: 'To',
			customRangeLabel: 'Custom Range',
			daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
			monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			firstDay: 1
		}
	 },
	 function(start, end) {
	  console.log("Callback has been called!");
	  $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	 }
  );
  //Set the initial state of the picker label
  $('#reportrange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));



	// $('body').on('click','#daterange',function () {
	// 	$(this).daterangepicker(
	// 	{
	// 		startDate: moment().subtract('days', 29),
	// 		endDate: moment(),
	// 		minDate: moment().subtract('years', 1),
	// 		maxDate: moment(),
	// 		dateLimit: { days: 60 },
	// 		showDropdowns: true,
	// 		showWeekNumbers: true,
	// 		timePicker: false,
	// 		timePickerIncrement: 1,
	// 		timePicker12Hour: true,
	// 		ranges: {
	// 		   'Today': [moment(), moment()],
	// 		   'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
	// 		   'Last 7 Days': [moment().subtract('days', 6), moment()],
	// 		   'Last 30 Days': [moment().subtract('days', 29), moment()],
	// 		   'This Month': [moment().startOf('month'), moment().endOf('month')],
	// 		   'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
	// 		},
	// 		opens: 'left',
	// 		buttonClasses: ['btn btn-default'],
	// 		applyClass: 'btn-small btn-primary',
	// 		cancelClass: 'btn-small',
	// 		format: 'DD/MM/YYYY',
	// 		separator: ' to ',
	// 		locale: {
	// 			applyLabel: 'Submit',
	// 			fromLabel: 'From',
	// 			toLabel: 'To',
	// 			customRangeLabel: 'Custom Range',
	// 			daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
	// 			monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	// 			firstDay: 1
	// 			}
	// 		},
	// 		function(start, end) {
	// 			$('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	// 			$('#from').val(start.format('YYYY-MM-DD'));
	//   			$('#to').val(end.format('YYYY-MM-DD'));
	// 		});
	// });


});


$(function () {

	// $('.notification').enscroll({
	// 	showOnHover: true,
	// 	verticalTrackClass: 'track3',
	// 	verticalHandleClass: 'handle3'
	// });

    $('body').on('click','[data-clear-filter]',function(event) {
    	event.preventDefault();
    	$( $(this).data('clear-filter') )
    		.find('input[type="radio"]:checked')
    		.prop('checked', false);
    });

  	if (Notification.permission !== "granted")
    	Notification.requestPermission();

	var userData = $('#userMsg').hide();

	/**************************************************
	*=FLASH MESSAGE DISPLAY TO USER.
	**************************************************/

	if ( userData ) {
		userData
			.delay('800')
				.fadeIn(1500)
					.delay(3000)
						.fadeOut(3000,function(){
					    	$( this ).slideUp('slow',function () {
					    		$(this).remove();
					    	});
						});
	};

	var lastURL;

	// AJAX Request
	$(document).on('click', 'a[data-request="ajax"]', function(event) {
		event.preventDefault();

		var $this = $(this),
			href = $this.attr('href'),
			// url = base_url() + href,
			url = href,
			action = $this.data('action');
			lastURL = $this;

		
		if (action == 'delete') { // DELETE Data with Given URL

			if ( confirm("Do you really want to delete Perminently.?") ){
				deleteRecord(url,$(this).closest('tr'));
				// console.log($(this).closest('tr'));
			}

		}else if (action == 'edit') { // EDIT Record with Given URL

			loadURL(url,$('.wrapper'));
			// alert('Edit To Call.');

		}else{ // LOAD Data with Given URL

			loadURL(url,$('.wrapper'));

		}

	});

	// FORM SUMIT VALIDATION
	$(document).on('submit', '#formToSubmit', function(event) {
		// console.log('yes submited');
		event.preventDefault();
		var error = false;
		var formdata = $(this).serialize();
		// reset form feedback on error
		$(document).find('.has-error').removeClass('has-error');
		$(document).find('.has-feedback').removeClass('has-feedback');
		$('.feedback-msg').remove();

		$(document).find('#formToSubmit input').each(function(index, el) {
			var $this = $(this),
				dataType = $this.data('type'),
				value = $this.val(),
				msg = '',
				parent = $this.parent();
				var decimalEXP = /^\d+(\.\d{1,4})?$/;

			if ($this.hasClass('required')) {
				if ($.trim(value)=='') {
					error = true;
					msg = 'Please Must Fill This Feild.';
					parent.addClass('has-error has-feedback');
					$('<span class="feedback-msg">'+msg+'</span>').insertAfter($this).delay(3000).fadeOut('slow');
				}
			}
			if ( $this.hasClass("selfvalidate") ) {
				if ($.trim(value)=='') {
					error = true;
					msg = 'Please Must Fill This Feild.';
					$this.addClass('has-error has-feedback');
					$('<span class="feedback-msg">'+msg+'</span>').insertAfter($this).delay(3000).fadeOut('slow');
				}
			}
			if ( dataType == 'decimal' ) {
				if ( !decimalEXP.test(value) ) {
					error = true;
					msg = 'Please Fill Valid Data.';
					if ( $this.hasClass("selfvalidate") ) {
						$this.addClass('has-error has-feedback');
					}else{
						parent.addClass('has-error has-feedback');
					}
					$('<span class="feedback-msg">'+msg+'</span>').insertAfter($this).delay(3000).fadeOut('slow');
				}
			}

		}); // each input loop

		$(document).find('#formToSubmit select').each(function(index, el) {
			var $this = $(this);
			if ( $(this).find('option:selected').val() == '' ) {
				error = true;
				msg = 'Please Select one of the Given Options.';
				$this.parent().addClass('has-error has-feedback');
				$('<span class="feedback-msg">'+msg+'</span>').insertAfter($this).delay(3000).fadeOut('slow');
			}

		});

		$(document).find('#formToSubmit .input-group>p').each(function(index, el) {
			console.log($(this).text());
			if( $(this).text().toLowerCase() == 'choose customer' ){
				$(this).parent().addClass('has-error has-feedback');
			}

			if( $(this).text().toLowerCase() == 'choose material' ){
				$(this).parent().addClass('has-error has-feedback');
			}

			if( $(this).text().toLowerCase() == 'choose structure' ){
				$(this).parent().addClass('has-error has-feedback');
			}

			if( $(this).text().toLowerCase() == 'choose job' ){
				$(this).parent().addClass('has-error has-feedback');
			}
		});

		if (!error) {
			var form = $(this);
			var href = $(this).attr('action');
				// url = base_url() + href;
			fromSubmit(href,formdata,$(this).closest('tr'));
			form[0].reset();
			// lastURL.trigger('click');
		};

	}); // form submit function

	function base_url () {
		var subfolder = '/printechProduction',
			host = window.location.origin + subfolder +'/index.php/';

		return host;
	}

	function basicNotify(success,msg,icon) {
		icon = icon ? icon: 'fa-check';
		notify({
            type: success, //alert | success | error | warning | info
            title: "Notification",
            message: msg,
            position: {
                x: "right", //right | left | center
                y: "bottom" //top | bottom | center
            },
            icon: '<i class="fa '+icon+'"></i>', //<i>
            // icon: '<img src="images/paper_plane.png" />', //<i>
            size: "normal", //normal | full | small
            overlay: false, //true | false
            closeBtn: true, //true | false
            overflowHide: false, //true | false
            spacing: 20, //number px
            theme: "default", //default | dark-theme
            autoHide: true, //true | false
            delay: 4000, //number ms
            onShow: null, //function
            onClick: null, //function
            onHide: null, //function
            template: '<div class="notify"><div class="notify-text"></div></div>'
        });
    };

	function balanceNotification (msg) {
		// icon = icon ? icon: 'fa-check';
		notify({
            type: 'info', //alert | success | error | warning | info
            title: "Approximate Balance",
            message: msg,
            position: {
                x: "center", //right | left | center
                y: "top" //top | bottom | center
            },
            icon: '<i class="fa fa-balance-scale"></i>', //<i>
            // icon: '<img src="images/paper_plane.png" />', //<i>
            size: "normal", //normal | full | small
            overlay: false, //true | false
            closeBtn: true, //true | false
            overflowHide: false, //true | false
            spacing: 20, //number px
            theme: "default", //default | dark-theme
            autoHide: false, //true | false
            delay: 4000, //number ms
            onShow: null, //function
            onClick: null, //function
            onHide: null, //function
            template: '<div class="notify"><div class="notify-text"></div></div>'
        });
    };

	function loadURL(url, container, settitle) {

	    $.ajax({
	        type: "GET",
	        url: url,
	        dataType: 'text',
	        cache: true,
	        beforeSend: function() {
	            // cog placed
	            container.html(
	            	$('<div id="preloader"></div>')
	            	.css('margin-top',( $(window).height()/2 ) + 'px' )
	            );
	            // only draw breadcrumb if it is content material
	            // TODO: check if document title injection refreshes in IE...
	            // TODO: see the framerate for the animation in touch devices
	            if (container[0] == $("#content")[0]) {
	                //drawBreadCrumb();
	                // update title with breadcrumb...
	                document.title = $(".breadcrumb li:last-child").text();

	                // scroll up
	                $("html, body").animate({
	                    scrollTop: 0
	                }, "fast");

	            } else {
	                container.animate({
	                    scrollTop: 0
	                }, "fast");
	            }
	        },
	        success: function(data) {
	            // alert("success")
	            if (typeof settitle === 'undefined') {
		            var loc = url.split('/'),
		            	title = loc[loc.length-2]+' '+loc[loc.length-1];
		            document.title = 'PT - '+title.replace('_',' ').toUpperCase();
		            window.location.hash = title.replace(' ','#');

		            container.css({
		                opacity: '0.0'
		            }).html(data).delay(50).animate({
		                opacity: '1.0'
		            }, 300)

	            }else{

		            container.html(data);

	            }
	            // .addClass('animated bounceInRight');

	            // setTimeout(function () {
	            // 	container.removeClass('animated bounceInRight');
	            // },2000);
	        },

	        error: function(xhr, ajaxOptions, thrownError) {
	        	// console.log(url);
	            container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>'+xhr.responseText);
	            // $('.wrapper').html(xhr.responseText);
	        }
	    });
	}

	function deleteRecord(url,row) {
	    $.ajax({
	        type: "GET",
	        url: url,
	        dataType: 'text',
	        cache: true,
	        beforeSend: function() {
	            // console.log(url);
	        },
	        success: function(data) {
	            // cog replaced here...
	            console.log(data);
	            if(data == 'Successfully Deleted'){

	            	row.fadeOut( function() {
	            		$(this).remove();
	            		basicNotify('success','Record Deleted.');
	            		if (!Notification) {
	            			alter('Your Browser Not Supported Notification System. Make Sure You have Latest Version Browser.!')
	            		}else{
	            			var notification = new Notification('Notification title', {
						      icon: 'http://www.printechpackages.com/images/logo.gif',
						      body: "1 Record Deleted Successfully!",
						    });
	            		}
	            	});
	            }else if(data.toLowerCase().indexOf("can not") >= 0) {
	            	basicNotify('error',data,'fa-times');
	            }
	        },

	        error: function(xhr, ajaxOptions, thrownError) {

	            alert('Some Error Found.!');
	            // container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>');
	            $('.wrapper').html(xhr.responseText);
	        },
	        async: false
	    });
	}

	function removeRowFromView(row) {
    	row.fadeOut( function() {
    		$(this).remove();
    	});
	}


	function fromSubmit(url,formdata,row) {
	    $.ajax({
	        type: "POST",
	        url: url,
	        // dataType: 'html',
	        data : formdata,
	        cache: true,
	        beforeSend: function() {
	            // cog placed
	            // container.append('<div class="preloader-overlay"<div id="preloader"></div></div>');
	        },
	        success: function(data) {
	            if (data == 'Successfully Inserted') {
	            	basicNotify('success','Record Submited.');
	            }else if(data == 'Job Approved'){
	            	console.log(row);
	            	console.log(data);
	            	basicNotify('success','Job Approved');
	            	removeRowFromView(row);
	            	MenuNotificationAlert();
	            }else{
	            	// $('.wrapper').html(data);
	        		console.log(data);
	            }
	        }, // end success function

	        error: function(xhr, ajaxOptions, thrownError) {

	            basicNotify('error','There may something wrong going on.','fa-fa-times');
	            // console.log(url);
	            // $('.wrapper').html(xhr.responseText);
	            console.log(xhr);
	            console.log(ajaxOptions);
	            console.log(thrownError);
	        	alert('some error here.');
	            // container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>');
	        },
	        async: false
	    });

	} // form submit

    var overlay = {
    	element : false,
    	job_code : false,
    	showBalance : false,
    	url : false,
        init : function () {
            overlay.element = $(this);
            overlay.url = $(this).data('method');

            $("<div/>",{
                id:"overlayContainer"
            }).hide()
            .appendTo('body')
            .fadeIn('slow')
            .append($('<div />',{id:'overlayContent'}));

            $('<div class="content-header clearfix"><h1>Search</h1><div class="overlaySearch-container"><input type="search" id="filter"/></div><i id="closeOverlay" class="fa fa-times-circle-o overlayCloseBTN"></i></div><div class="overlay-body"></div><div class="overlay-footer"><button id="selectMaterialBTN" class="btns blue sml-btn">Select</button><button class="btns pink sml-btn overlayCloseBTN">Close</button></div>').appendTo('#overlayContent');
            var overlayBody = $('.overlay-body').text('Please Type that your want to seach.');

            overlayBody.find('input[type=radio]:first').prop('checked', true);

            $('.overlayCloseBTN').click(overlay.close);
            $('#selectMaterialBTN').click(overlay.select);
            
            $('#filter').keyup(function(pressed) {
            	loadURL(overlay.url+'?term='+$(this).val(),overlayBody);            
            });
        },
        close: function (e) {
        	e.preventDefault();
        	$('#overlayContainer').fadeOut('fast', function() {
        		$(this).remove();
        		// console.log('before checking element data banace');
        		// console.log(overlay.element.data('balance'));
        		if ($(overlay.element).data('balance')) {
	        		// console.log('after checking element data banace');
        			var balanceURL = overlay.element.data('balance') + '/' +overlay.job_code;
        			$.ajax({
        				url: balanceURL,
        			})
        			.done(function(data) {
        				balanceNotification(data);
        				$('.notify-top-center').draggable({snap:'.wapper',snapNode:'inner'});
        				// console.log("Done Function Calls");
        			})
        			.fail(function() {
        				// console.log("error this is from selecting balance");
        			})
        			.always(function () {
        				// console.log("Always Function");
        			});
        		}
        	}); // #overlay-container fadeout
        },
        select: function (e) {
        	e.preventDefault();
        	$targetedField = $(overlay.element);
        	$selected = $('#overlayContainer input[type="radio"]:checked');
        	// console.log($selected.val());
        	if ($selected.val()) {
	        	$targetedField.text( $selected.parent().text() );
	        	$targetedField.parent().find('input[type=hidden]').val( $selected.val() );
	        	overlay.job_code = $selected.val();
	        	overlay.showBalance = true;
	            $('.overlayCloseBTN').trigger('click');
        	}else{
	            $('.overlayCloseBTN').trigger('click');
        	}
        }
    }

    $('.wrapper').on('click','.findMaterial',overlay.init);

    // panel expand to full screen
    $('.wrapper').on('click','.expand',function (e) {
    	e.preventDefault();
    	var $this = $(this);
    	$this.parents('.expandable').toggleClass('expanded');
    	$this.toggleClass('fa-expand fa-compress');
    });

    // apply datetime picker on element
	$('body').on('focus', '.dateTime', function() { $(this).appendDtpicker(); });

	$('body').on('mouseover', 'input', function() {
		var $this = $(this);
		if ( ! $this.attr('title') ) {
        	$this.attr( 'title', $this.attr('placeholder') );
		}
    });


	function loadNotification (url, container) {
		$.ajax({
			url: url,
			async: false
		})
		.done(function(data) {
			container.html(data);
		})
		.fail(function() {
			console.log("error on loading notification");
		});
		
	}

	function MenuNotificationAlert() {
		$('.menuNotification').each(function(index, el) {
			var $this = $(el),
				url = $(this).data('notify');
			loadNotification(url,$this.find('span:first i'));
		});
		// console.log('tick');
	}
	MenuNotificationAlert();
	// setInterval(MenuNotificationAlert,1000);

	function updateNotification () {
		var url = $('#siteURL').text();
		$.ajax({
			url: url+'index.php/widget/get_notification/',
			dataType: 'html'
		})
		.done(function(data) {
			// console.log("successfully get data for notification.");
			// console.log(data);
			var data = jQuery.parseHTML(data);
			$('.notification span:first').after($(data));

		})
		.fail(function() {
			// console.log("error");
		})
		.always(function() {
			// console.log("complete");
		});
	}

	updateNotification();

	
	
	



});