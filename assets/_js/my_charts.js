
	// var chart = new Morris.Donut({
	//   // ID of the element in which to draw the chart.
	//   element: 'myfirstchart',
	//   // Chart data records -- each entry in this array corresponds to a point on
	//   // the chart.
	//   data: [
	//     {label: "Download Sales", value: 12},
	//     {label: "In-Store Sales", value: 30},
	//     {label: "Mail-Order Sales", value: 20},
	//     {label: "In-Store Sales", value: 30},
	//     {label: "Mail-Order Sales", value: 20},
	//     {label: "In-Store Sales", value: 30},
	//     {label: "Mail-Order Sales", value: 20},
	//     {label: "In-Store Sales", value: 30},
	//     {label: "Mail-Order Sales", value: 20},
	//     {label: "In-Store Sales", value: 30},
	//     {label: "Mail-Order Sales", value: 20},
	//     {label: "In-Store Sales", value: 30},
	//     {label: "Mail-Order Sales", value: 20},
	//     {label: "In-Store Sales", value: 30},
	//     {label: "Mail-Order Sales", value: 20},
	//     {label: "In-Store Sales", value: 30},
	//     {label: "Mail-Order Sales", value: 20},
	//     {label: "In-Store Sales", value: 30},
	//     {label: "Mail-Order Sales", value: 20}
	//   ],
	//   resize: true,
	//   formatter: function (y, data) { 
	//   		return y = y.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Kg' ;
	//   }
	// });
	// var chart = new Morris.Donut({
	//   // ID of the element in which to draw the chart.
	//   element: 'myfirstchart',
	//   // Chart data records -- each entry in this array corresponds to a point on
	//   // the chart.
	//   data: [0,0],
	//   xkey: 'x',
	//   ykeys: ['y', 'z', 'a'],
	//   labels: ['Y', 'Z', 'A'],
	//   resize: true
	// });

	// $.get('<?php echo base_url('index.php/widget/chart'); ?>', function(data) {
	// 	// chart.setData(data);
	// });

	$(function() {

		$('[data-date=true]').datepicker({
	        dateFormat : "yy-mm-dd",
	        firstDay : 1,
	        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
	    });

		// $('#customersOption').tokenize({
		//     onAddToken: function(value, text, e){
  //        		updateChart();
  //       	},
		//     onRemoveToken: function(value, text, e){
		//     	updateChart();
  //       	}
  //   	});

		$('#fromDate').val(moment().startOf('month').format('YYYY-MM-DD'));
		$('#toDate').val(moment().endOf('month').format('YYYY-MM-DD'));

		function updateChart() {
			var fromDate = $('#fromDate').val(),
				toDate = $('#toDate').val(),
				charturl = $('#chart_location').val(),
				data,
				customers = Array();

			// $('#customersOption option:selected').each(function(index, el) {
			// 		customers.push(this.value);
			// });
			// console.log(customers.join(","));
			// console.log($('select[name=order_type] option:selected').val());

			$.ajax({
				url: charturl+'/'+fromDate+'/'+toDate,
				type: 'GET',
				dataType: 'json',
				async: false,
				data: { order_type: "'"+$('select[name=order_type] option:selected').val()+"'" },
				success: function(d) {
					data = d;
				}
			})
			.fail(function(x) {
				console.log("error");
				console.log(x);
			});

			chart.unload();

			chart.load({
	        	columns: data.columns,
	        	axis: {
	        		// x:{
	        		// 	type: 'category',
	        		// 	categories: date,
	        		// }
	        	}
		    });
			// return data;
		}

		var chart = c3.generate({
		    data: {
		    	x: 'x',
		        columns: [],
		        type: 'bar'
		    },
		    axis: {
		        x: {
		        	label: {
		        		text: 'Customers Name',
	              		position: 'outer-center',
	              	},
		            type: 'category',
		            categories: [],
		            tick: {
		              centered: true
		            }
		        },
		        y: {
		        	label : {
		        		text: 'Order Quantities',
		        		position: 'outer-center',
		        	},
		            padding: {
		              top: 0,
		              bottom: 0
		            }
		        }
		    },
		    bar: {
		        width: {
		            ratio: 1 // this makes bar width 50% of length between ticks
		        }
		        // or
		        //width: 100 // this makes bar width 100px
		    },
		 //    donut : {
			//     label: {
			//       format: function (value, ratio) { 
			//       		return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ' Kg' ;
			//       	}
			//     }
			// },
		    size: {
			  height: 500
			},
		    // subchart: {
		    // 	show: true
		    // },
	        // legend: {
	        //   show: false
	        // }
		});

		updateChart();

		$('#fromDate, #toDate, select[name="order_type"]').change(function(event) {
			
			updateChart();

		});
	
		$('button').on('click', function(event) {
			event.preventDefault();
			chart.transform('line');
		});

	});
