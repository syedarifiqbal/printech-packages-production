<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
 *
 */
class MY_Controller extends CI_Controller
{
    public $data = array();
    private $roles = array();
    public $maintenance_mode = 0;

    public function __construct()
    {
        parent::__construct();

        $roles = new Role_Model();
        $roles = $roles->get();

        foreach ($roles as $role) {
            $this->roles[] = $role->role_name;
        }

        // Check if maintenance mode and redirect to maintenance mode page of user not developer.
        $settings = $this->Settings_model->getWhere(['name'=>'maintenance'], true);
        $this->maintenance_mode = $settings->value;
        if( $settings->value && $this->session->userdata('user_id') != 1 ){
            redirect(site_url('shared/pages/maintenance'));
        }

        /* check if user logged in or not
            * if not logged in redirect to login page
            * otherwise get user roles and assign to Allowed Role Array
        */

        if (!$this->session->userdata('logged_in')) {

            if ($this->router->fetch_method() != 'login') {
                redirect(base_url() . 'index.php/user/login');
            }

        } else {

            $status = $this->Xx_Authentication_Model->getAuthenticationStatus();

            if ($status->expire) {

                redirect(site_url('Authentication/update_token/'));

            } else {

                if ($status->notification) {
                    $this->session->set_flashdata('expireNotification', '<div id="expireNotification" class="alert alert-warning" style="z-index:1000000000;" role="alert">Please <a href="' . site_url('Authentication/update_token/') . '">Update Subscription</a> Token before ' . date('d F, Y', strtotime($status->end)) . '</div>');
                }

                if (!$this->session->userdata('section')) {
                    $methods = ['section' => true, 'set_section' => true];

                    if (!array_key_exists($this->router->fetch_method(), $methods)) {
                        redirect(base_url() . 'index.php/dashboard/section');
                    }

                } else {

                    $this->User_Model->getPermissions();

//                    $user_role = new User_Model();
//                    $user_role->getRolePerms($this->session->userdata('user_id'));
//
//                    foreach ($this->roles as $role) {
//
//                        if ($user_role->hasPermission($role)) {
//
//                            $this->allowed_roles[$role] = true;
//
//                        } else {
//
//                            $this->allowed_roles[$role] = false;
//
//                        }
//
//                    } // foreach loop

                } // else session is not set

            } // else expire


        } // if user logged in

    } // custruct function

    function set_data($key, $value){
        $this->data[$key] = $value;
    }

    function get_data(){
        return $this->data;
    }

}


?>