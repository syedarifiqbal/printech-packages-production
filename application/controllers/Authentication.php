<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller {

	# Methodes 

	public function update_token()
	{
		if ($this->session->userdata('user_id')) {
			// echo md5("");
		}

		$auth = $this->Xx_Authentication_Model->getAuthenticationStatus();

		if ( $auth->expire==1 || $auth->notification==1 ) {
			
			if ($this->input->post()) {

				if ( trim($this->input->post('token')) == md5($auth->generated_key)) {
					$authentication = new Xx_Authentication_Model();
					$authentication->start = $auth->end;
					$authentication->end = date('Y-m-d',strtotime("+6 months", strtotime(date('Y-m-d'))));
					$authentication->authorized = 1;
					$authentication->generated_key = generateRandomString();
					
					if ($authentication->save()) {
						$this->session->set_flashdata('userMsg', 'Registration Success.');
						redirect(site_url());
					}

				}else{
					echo '<div class="alert alert-danger" role="alert">Provided Token is invalid.!</div>';
				}
			}
		
		}

		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';
		
		$data['auth'] = $auth;
		$data['auth'] = $auth;
		$data['site_title'] = 'Add New Group';
		$this->load->view('main/header', $data);
		$this->load->view('main/topbar', $data);
		$this->load->view('Maintenance/update_token', $data);
		$this->load->view('main/footer', $data);

	}


}