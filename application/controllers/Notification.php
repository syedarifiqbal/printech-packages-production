<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notification extends MY_Controller {


	/**************************************************
	*				UNAPPROVED SALE ORDER.
	**************************************************/

	public function unapproved_sale_order()
	{
		// if ($this->input->is_ajax_request()) {
	 //        $this->load->view('');
	 //    } else {
	 //        echo "string";
	 //    }

		$this->db->select('job_code')->from('sale_order')->where('approved','0');
		$result = $this->db->count_all_results();
		$ret = [];
		if ($result>=100)
			echo "99+";
		else if( $result<10 )
			echo '0'.$result;
		else
			echo $result;
	}

	/**************************************************
	*			CURRENT MONTH SALE ORDER.
	**************************************************/

	public function current_month_order()
	{
		$result = $this->db->query('SELECT COUNT(date) AS total, MONTHNAME(NOW()) AS month FROM sale_order
							WHERE MONTH(date) = MONTH(NOW())')->row();
		echo $result->total;
	}


// SELECT COUNT(date), MONTHNAME(NOW()) FROM sale_order
// WHERE MONTH(date) = MONTH(NOW())


}