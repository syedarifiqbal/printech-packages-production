<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Statistics extends MY_Controller {

	# Methodes 

	public function order_chart()
	{

		$data['site_title'] = site_title("Order Statistics");
		
		$customer = $this->Customer_Model->get();
		$select = '';
		
		foreach ($customer as $key => $value)
			$select .= '<option value="'.$key.'">'.$value->customer_name. '</option>';

		$data['customers_option'] = $select;

		if ($this->input->is_ajax_request()) {
			$this->load->view('widgets/sale_order',$data);
		}else{
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('widgets/sale_order',$data);
			$this->load->view('main/footer',$data);
		}
	}

	public function imap()
	{
		$mbox = imap_open ("{printechpackages.com:465/imap}", "arif.iqbal@printechpackages.com", "Aiqbal@#123");
		var_dump($mbox);
	}


}