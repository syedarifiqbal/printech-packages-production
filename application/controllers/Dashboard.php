<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	# Methodes 

	public function index()
	{
		$todolist = new Todolist_Model();
		$data['todolist'] = $this->Todolist_Model->getWhere(['user_id'=>$this->session->userdata('user_id')]);
		$data['messages'] = $this->db->query("SELECT * FROM(
											    SELECT msg.*, u.user_name,u.avater_path 
											    FROM `message` AS msg
											    LEFT JOIN `user` AS u ON msg.user_id = u.user_id
											    ORDER BY msg.timestump DESC
											    LIMIT 50) AS message
											ORDER BY message.timestump ASC")->result();

		$data['vacancy'] = $this->db->query("SELECT * FROM vocancy
												WHERE CURRENT_DATE() BETWEEN start_date AND end_date
												AND ISNULL(position_register)
												ORDER BY id DESC
												LIMIT 1")->row();
		
		if( $this->session->userdata('section') == 'offset' ){

			$this->load->model('offset/Offset_sale_order_model');
			$this->set_data('total_orders_for_the_month', $this->Offset_sale_order_model->count_all_orders_current_month());
			$this->set_data('total_orders_for_last_six_month', $this->Offset_sale_order_model->count_last_six_month_orders());
			$this->load->view( 'offset/offset/dashboard', $this->get_data() );

		}else{

			$data['site_title'] = site_title("PT - Dashboard");
			if ($this->input->is_ajax_request()) {
				$this->load->view('dashboard/index',$data);
			}else{
				// var_dump($data['roles']);
				$this->load->view('main/header',$data);
				$this->load->view('main/navigation',$data);
				$this->load->view('main/rightNavigation',$data);
				$this->load->view('main/topbar',$data);
				$this->load->view('dashboard/index',$data);
				$this->load->view('main/footer',$data);
			}

		}

	}

	public function inbox()
	{
		$user_id = $this->session->userdata('user_id');
		$todolist = new Todolist_Model();
		$data['users'] = $this->User_Model->getWhere('user_id !='.$this->session->userdata('user_id'));
		$data['recent_messages'] = $this->db
		->query("SELECT
				  personal_message.*
				FROM
				  ( SELECT MAX(id) AS id
				  	FROM
				    	personal_message
				  	WHERE
				    	$user_id IN(`from`,`to`)
				  	GROUP BY IF ($user_id = `from`, `to`, `from`)
				   ) AS latest
				LEFT JOIN
				  personal_message USING(id)")->result();

		$data['site_title'] = site_title("Personal Chat!");
		$data['roles'] = $this->allowed_roles;
		if ($this->input->is_ajax_request()) {
			$this->load->view('dashboard/inbox',$data);
		}else{
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('dashboard/inbox',$data);
			$this->load->view('main/footer',$data);
		}

	}

	public function section(){

		$data['site_title'] = site_title("PT - SELECT SECTION");
		$this->load->view('main/header',$data);
		// $this->load->view('main/rightNavigation');
		$this->load->view('main/topbar');
		$this->load->view('dashboard/section');
		$this->load->view('main/footer');

	}

	public function set_section($section){
		$sections = ['roto','offset','extrusion','maintenance','hr','marketing','health_sefty'];
		if ( isset($section) && in_array($section, $sections)) {
			$this->session->set_userdata('section',$section);
			redirect(base_url());
		}else{
			redirect(base_url()."index.php/Dashboard/section");
		}

	}

	public function get_currency_rate()
	{
		$currencyCode = [
				"USD/PKR" => "USD DOLLER ", 
				"AUD/PKR" => "AUSTRALIA DOLLER ", 
				"EUR/PKR" => "EURO ", 
				"GBP/PKR" => "POUND ", 
				"AED/PKR" => "DIRHAM ", 
				"INR/PKR" => "INDIAN RATE ", 
				"OMR/PKR" => "OAMN RIAL ", 
				"JPY/PKR" => "JAPAN YEN ", 
				"MYR/PKR" => "MALAYSIA RINGGIT ", 
				"KWD/PKR" => "KUWAIT DINAR "
			];

			$url = 'http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in ("USDPKR", "AUDPKR", "EURPKR", "GBPPKR", "AEDPKR", "INRPKR", "OMRPKR", "JPYPKR", "MYRPKR", "KWDPKR")&env=store://datatables.org/alltableswithkeys';
			$sxml = simplexml_load_file($url);
			if ($sxml) {
				$sxml = $sxml->xpath('//results/rate[Name[contains(text(),"PKR")]]');
				
				$currency = array();
				foreach ($sxml as $key => $value) {
					$name = (string) $value->Name ;
					$curr=['Name'=>$currencyCode[$name],'Rate'=> $value->Rate];
					$currency[] = $curr;
				}
				echo json_encode($currency);
				// echo json_encode($sxml);
			}else{
				"Sorry Internet Problem.!";
			}
			// echo "<pre>";
			// echo print_r($sxml);
			// echo "</pre>";
	}

	public function get_metals_rate()
	{

			$url = 'http://www.xmlcharts.com/cache/precious-metals.php';
			// $feed = file_get_contents($url);
			$sxml = simplexml_load_file($url);
			if ($sxml) {
				$sxml = $sxml->xpath('//prices/currency[@access="usd"]');
				// echo json_encode($sxml);
				// echo "<pre>";
				// echo json_encode($sxml);
				// echo "</pre>";
				// $sxml = array_shift($sxml);
				$x = (array_shift($sxml)->price);
				$matels = array('gold'=>(double)$x[0],'palladium'=>(double)$x[1],'platinum'=>(double)$x[2],'silver'=>(double)$x[3]);
				var_dump($matels);
				// foreach (array_shift($sxml)->price as $key => $value) {
				// 	if ( is_array($value) ) { continue;}
				// 	$key = (int) $key;
				// 	echo $matels[$key] . ' => ' . $value . '<br>';
				// }
			}else{
				"Sorry Internet Problem.!";

			}
	}



}