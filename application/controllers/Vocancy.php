<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vocancy extends MY_Controller {

	/**************************************************
	*					=ADD VOCANCY.
	**************************************************/ 
	public function add_vocancy()
	{
		$data['title'] = 'Create Job Vocancy for Printech Packages!';
		$data['session'] = $this->session->all_userdata();
		if ( $_POST ) {
			$vocancy = new Vocancy_Model();
			$vocancy->id = $this->Vocancy_Model->max();
			$vocancy->start_date = dbDate($this->input->post('start_date'));
			$vocancy->end_date = dbDate($this->input->post('end_date'));
			$vocancy->no_position = $this->input->post('no_position');
			$vocancy->position_name = $this->input->post('position_name');
			$vocancy->description = $this->input->post('description');
			$vocancy->department = $this->input->post('department');
			$vocancy->created_by = $this->session->userdata('user_id');
			$vocancy->created_time = dbTime();
			$vocancy->updated_by = $this->input->post('');
			$vocancy->updated_time = $this->input->post('');
			if ($vocancy->save()) {
				if ($this->input->is_ajax_request()) {
					echo "Successfully Inserted";
					return;
				}else{
					$this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
					redirect(site_url('Vocancy/add_vocancy'));
				}
			}
		}
		
		$data['roles'] = $this->allowed_roles;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('HR/vocancy',$data);
		}else{
			$data['site_title'] = 'Add New Rewinding Entry.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('HR/vocancy',$data);
			$this->load->view('main/footer',$data);
		}

	} // add_vocancy()

	/**************************************************
	*					=UPDATE VOCANCY.
	**************************************************/ 
	public function update_vocancy($id=false)
	{
		if ($id) {
			$data['title'] = 'Update Job Vocancy for Printech Packages!';
			$data['session'] = $this->session->all_userdata();
			if ( $_POST ) {
				$vocancy = new Vocancy_Model();
				$vocancy->load($id);
				$vocancy->start_date = dbDate($this->input->post('start_date'));
				$vocancy->end_date = dbDate($this->input->post('end_date'));
				$vocancy->no_position = $this->input->post('no_position');
				$vocancy->position_name = $this->input->post('position_name');
				$vocancy->description = $this->input->post('description');
				$vocancy->department = $this->input->post('department');
				// $vocancy->created_by = $this->session->userdata('user_id');
				// $vocancy->created_time = dbTime();
				$vocancy->updated_by = $this->session->userdata('user_id');
				$vocancy->updated_time = dbTime();
				if ($vocancy->save()) {
					if ($this->input->is_ajax_request()) {
						echo "Successfully Inserted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
						redirect(site_url('Vocancy/vocancy_list'));
					}
				}
			}
			
			$data['roles'] = $this->allowed_roles;
			$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';
			$v = new Vocancy_Model();
			$v->load($id);
			$data['record'] = $v;
			$data['update'] = true;

			if ($this->input->is_ajax_request()) {
				$this->load->view('HR/vocancy',$data);
			}else{
				$data['site_title'] = 'Add New Rewinding Entry.';
				$this->load->view('main/header',$data);
				$this->load->view('main/navigation',$data);
				$this->load->view('main/rightNavigation',$data);
				$this->load->view('main/topbar',$data);
				$this->load->view('HR/vocancy',$data);
				$this->load->view('main/footer',$data);
			}
		} // if id isset

	} // update_vocancy()

	/**************************************************
	*					=REGISTER VOCANCY.
	**************************************************/ 
	public function complete_vocancy($id=false)
	{
		if ($id) {
			$vocancy = new Vocancy_Model();
			$vocancy->load($id);
			$vocancy->position_register = dbTime();
			if ($vocancy->save()) {
				if ($this->input->is_ajax_request()) {
					echo "Successfully Inserted";
					return;
				}else{
					$this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
					redirect(site_url('Vocancy/vocancy_list'));
				}
			}else{
				echo "something goes wrong! Cannot Complete this Vocancy.";
			}
			
		}else{ // if id isset

			echo "No Parameter Pass!";

		}

	} // complete_vocancy()


/**************************************************
*					=VOCANCY LIST.
**************************************************/ 
	public function vocancy_list()
	{
		$data['title'] = 'Vocancy List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$data['roles'] = $this->allowed_roles;
		$data['records'] = $this->Vocancy_Model->get();

		if ($this->input->is_ajax_request()) {
			$this->load->view('user/user_list',$data);
		}else{
			$data['site_title'] = 'Software Users List!';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('HR/vocancy_list',$data);
			$this->load->view('main/footer',$data);
		}

	} // vocancy_list()


	public function delete_vocancy($id=null)
	{
		if ($id) {
			
			$record = new Vocancy_Model();
			$record->load($id);

			if ($record->id && !$record->position_register){
				$record->delete();
				if($this->db->_error_message()){
					echo "Can not delete this Record.!";
				}else{
					
					if ($this->input->is_ajax_request()) {
						echo "Successfully Deleted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', $record->position_name.' Deleted Successfully.');
						redirect(site_url('Vocancy/Vocancy_list'));
					}
				}
			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
		{
			echo "<h1>Please Follow Given Link.!</h1>";
		}

	} // END DELETE CUSTOMER FUCTION



}