<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Offset_purchase_order extends MY_Controller
{

    public $allowed_roles = array();

    # Methodes

    public function index(){
        echo 'Purchase order list in pending';
    }

    /**************************************************
     *                    =PURCHASE ORDER.
     **************************************************/
    public function add()
    {
        $data['title'] = 'Receive Inventory || Create GRN.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $po = new Purchase_order_Model();
            $po->po_id = $this->Purchase_order_Model->max();
            $po->date = dbDate($this->input->post('date'));
            $po->supplier_id = $this->input->post('supplier_id');
            $hold = ($this->input->post('hold') == "on") ? 1 : 0;
            $po->hold = $hold;
            $po->description = $this->input->post('description');
            $po->created_by = $this->session->userdata('user_id');
            $po->created_time = dbTime();
            $po->updated_by = $this->input->post('');
            $po->updated_time = $this->input->post('');
            // if (true) {
            if ($po->save()) {
                // var_dump($_POST);
                for ($i = 0; $i < count($this->input->post('material')); $i++) {
                    $item = new PO_Detail_Model();
                    $item->pod_id = '';
                    $item->po_id = $po->po_id;
                    $item->material_id = $this->input->post('material')[$i];
                    $item->qty = $this->input->post('mQuantity')[$i];
                    $item->rate = $this->input->post('mRate')[$i];
                    $item->description = $this->input->post('mDescription')[$i];
                    if ($item->save()) {
                        // echo "Sub Statement Inserted.!<br>";
                    }

                }
                echo "Successfully Inserted";
                return;
            };
        }

        $data['roles'] = $this->allowed_roles;

        // $this->load->view('header', $data);
        $this->load->view('offset/purchase_order', $data);
        // $this->load->view('footer', $data);

    } // purchase_order()


    /**************************************************
     *                    =GOODS RECEIVED NOTE.
     **************************************************/
    public function goods_receive_note()
    {
        $data['title'] = 'Receive Inventory || Create GRN.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $grn = new Grn_Model();
            $grn->grn_id = $this->Grn_Model->max();
            $grn->date = dbDate($this->input->post('date'));
            // $grn->supplier_id = $this->input->post('supplier_id');
            $grn->description = $this->input->post('description');
            $grn->created_by = $this->session->userdata('user_id');
            $grn->created_time = dbTime();
            $grn->updated_by = $this->input->post('');
            $grn->updated_time = $this->input->post('');
            // if (true) {
            if ($grn->save()) {
                // var_dump($_POST);
                for ($i = 0; $i < count($this->input->post('material')); $i++) {
                    $item = new Grn_detail_Model();
                    $item->grnd_id = '';
                    $item->grn_id = $grn->grn_id;
                    $item->material_id = $this->input->post('material')[$i];
                    $item->store_id = $this->input->post('mStore')[$i];
                    $item->roll_carton = $this->input->post('mRoll')[$i];
                    $item->qty = $this->input->post('mQuantity')[$i];
                    $item->rate = $this->input->post('mRate')[$i];
                    $item->defective = $this->input->post('mDefective')[$i];
                    $item->description = $this->input->post('mDescription')[$i];
                    $item->po_id = $this->input->post('po_id')[$i];
                    if ($item->save()) {
                        // echo "Sub Statement Inserted.!<br>";
                    }

                }
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Record Added Successfully.');
                    redirect(site_url('maintenance/grn_list'));
                }
            };
        }

        $data['roles'] = $this->allowed_roles;
        $store = new Store_Model();
        $data['stores'] = $store->get();
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('store/grn', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('store/grn', $data);
            $this->load->view('main/footer', $data);
        }

    } // goods_receive_note()


    /**************************************************
     *                    =GOODS ISSUE NOTE.
     **************************************************/
    public function goods_issue_note()
    {
        $data['title'] = 'Receive Inventory || Create Goods Issue Note.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $gin = new Gin_Model();
            $gin->gin_id = $this->Gin_Model->max();
            $gin->date = dbDate($this->input->post('date'));
            $gin->issue_to = $this->input->post('issue_to');
            $gin->description = $this->input->post('description');
            $gin->created_by = $this->session->userdata('user_id');
            $gin->created_time = dbTime();
            $gin->updated_by = $this->input->post('');
            $gin->updated_time = $this->input->post('');
            // if (true) {
            if ($gin->save()) {
                // var_dump($_POST);
                for ($i = 0; $i < count($this->input->post('material')); $i++) {
                    $item = new Gin_detail_Model();
                    $item->gind_id = '';
                    $item->gin_id = $gin->gin_id;
                    $item->material_id = $this->input->post('material')[$i];
                    $item->store_id = $this->input->post('mStore')[$i];
                    $item->roll_carton = $this->input->post('mRoll')[$i];
                    $item->qty = $this->input->post('mQuantity')[$i];
                    $item->rate = $this->input->post('mRate')[$i];
                    $div = $this->input->post('mDefective')[$i];
                    $div = $div ? $div : 0;
                    $item->defective = $div;
                    $item->description = $this->input->post('mDescription')[$i];
                    $item->job_code = '';
                    if ($item->save()) {
                        // echo "Sub Statement Inserted.!<br>";
                    }

                }
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Record Added Successfully.');
                    redirect(site_url('maintenance/gin_list'));
                }
            };
        }

        $data['roles'] = $this->allowed_roles;
        $store = new Store_Model();
        $data['stores'] = $store->get();
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('store/gin', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('store/gin', $data);
            $this->load->view('main/footer', $data);
        }

    } // goods_issue_note()


}