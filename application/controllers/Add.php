<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Add extends MY_Controller
{

    public $allowed_roles = array();

    # Methodes

    /**************************************************
     *                =REGISTRATION USER.
     **************************************************/
    public function register_user()
    {
        $data['title'] = 'REGISTRATION || REGISTER NEW USER.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $user = new User_Model();
            // $user->user_id = $this->User_Model->max();
            $user->user_name = $this->input->post('user_name');
            $user->hashed_password = password_hash($this->input->post('hashed_password'), PASSWORD_BCRYPT, ['cost' => 12]);
            $user->plain_password = $this->input->post('hashed_password');
            $user->avater_path = $this->input->post('avater_path');
            $user->role_id = $this->input->post('role_id');
            $user->cell = $this->input->post('cell');
            $active = ($this->input->post('active') != null) ? $this->input->post('active') : 0;
            $user->active = $active;
            $user->avater_path = '';
            $user->address = $this->input->post('address');

            $user->added_by = $this->session->userdata('user_id');
            $user->added_time = dbTime();
            $user->updated_by = 0;
            $user->updated_time = $this->input->post('');
            $user->save();
            if( $this->db->error()['message'] ){
                echo $this->db->error()['message'];
            }else{
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New User Added Successfully.');
                    redirect(site_url('Maintenance/user_list'));
                }
            }

        } // if post is set

        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        $this->load->model('Roles2_Model');
        $data['roles'] = $this->Roles2_Model->get(500,0,false);

        if ($this->input->is_ajax_request()) {
            $this->load->view('user/register', $data);
        } else {
            $data['site_title'] = 'Add New User';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('user/register', $data);
            $this->load->view('main/footer', $data);
        }

    } // register_user()


    /**************************************************
     *                    ADDING CUSTOMER.
     **************************************************/

    public function customer()
    {
        if (isset($_POST['customer_name'])) {
            // isset($_POST['submit']) ? var_dump($_POST):null;

            $mobile = isset($_POST['mobile']) ? count($_POST['mobile']) : 0;
            $address = isset($_POST['address']) ? count($_POST['address']) : 0;

            $count = ($mobile > $address) ? $mobile : $address;

            $customer = new Customer_Model();
            // $customer->customer_id = $this->Customer_Model->max();
            $customer->customer_name = $this->input->post('customer_name');
            $customer->customer_code = $this->Customer_Model->max_where('customer_code');
            $customer->active = 1;
            $customer->toured = 0;
            $customer->customer = 1;
            $customer->client_password = '$2y$12$qT3jQi1JqRb7duft8B3re.qkmN0nAxpta4hSJ7ZCehZJAeT8uxwu2';

            if ($customer->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Customer Added Successfully.');
                    redirect(site_url('Maintenance/customer_list'));
                }
            }else{
                check($this->db->error());
            }

        } else {

            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            $data['roles'] = $this->allowed_roles;
            if ($this->input->is_ajax_request()) {
                $this->load->view('Maintenance/addCustomer', $data);
            } else {
                $data['site_title'] = 'Add New Customer';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('Maintenance/addCustomer', $data);
                $this->load->view('main/footer', $data);
            }
        }
    }

    /**************************************************
     *                    ADDING SUPPLIER.
     **************************************************/

    public function supplier()
    {
        if (isset($_POST['supplier_name'])) {

            $mobile = isset($_POST['mobile']) ? count($_POST['mobile']) : 0;
            $address = isset($_POST['address']) ? count($_POST['address']) : 0;

            $count = ($mobile > $address) ? $mobile : $address;

            $supplier = new Supplier_Model();
            // $supplier->supplier_id = $this->Supplier_Model->max();
            $supplier->supplier_name = $this->input->post('supplier_name');
            $supplier->supplier_code = $this->Supplier_Model->max_where('supplier_code');
            $supplier->active = $this->input->post('active');
            $supplier->toured = 0;
            $supplier->supplier = 1;
            $supplier->client_password = '$2y$12$qT3jQi1JqRb7duft8B3re.qkmN0nAxpta4hSJ7ZCehZJAeT8uxwu2';
            $supplier->create_by = $this->session->userdata('user_id');
            $supplier->create_time = dbTime();
            $supplier->update_by = $this->input->post('');
            $supplier->update_time = $this->input->post('');

            if ($supplier->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Supplier Added Successfully.');
                    redirect(site_url('Maintenance/supplier_list'));
                }
            };

        }

        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        $data['roles'] = $this->allowed_roles;
        if ($this->input->is_ajax_request()) {
            $this->load->view('Maintenance/addSupplier', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('Maintenance/addSupplier', $data);
            $this->load->view('main/footer', $data);
        }
    }

    /**************************************************
     *                    ADDING NEW =CATEGORY.
     **************************************************/
    public function category()
    {
        $data['title'] = 'Category List | Edit | Delete.!';
        $data['session'] = $this->session->all_userdata();

        if (isset($_POST['category_name'])) {
            $category = new category_Model();
            // $category->category_id = $this->Category_Model->max();
            $category->category_name = $this->input->post('category_name');
            $category->density = $this->input->post('density');
            $category->description = $this->input->post('description');
            $category->created_by = $this->session->userdata('user_id');
            $category->created_time = dbTime();
            $category->updated_by = 0;
            $category->updated_time = $this->input->post('');
            if ($category->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Category Added Successfully.');
                    redirect(site_url('Maintenance/category_list'));
                }
            }

        }

        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        $data['roles'] = $this->allowed_roles;
        if ($this->input->is_ajax_request()) {
            $this->load->view('Maintenance/addCategory', $data);
        } else {
            $data['site_title'] = 'Add New Category';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('Maintenance/addCategory', $data);
            $this->load->view('main/footer', $data);
        }

    }

    /**************************************************
     *                    ADDING NEW =GROUP.
     **************************************************/
    public function group()
    {
        $data['title'] = 'Group List | Edit | Delete.!';
        $data['session'] = $this->session->all_userdata();

        if ($_POST) {
            $group = new Group_Model();
            // $group->group_id = $this->Group_Model->max();
            $group->group_name = $this->input->post('group_name');
            $group->description = $this->input->post('description');
            $group->created_by = $this->session->userdata('user_id');
            $group->created_time = dbTime();
            $group->updated_by = 0;
            $group->updated_time = $this->input->post('');
            if ($group->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Group Added Successfully.');
                    redirect(site_url('Maintenance/group_list'));
                }
            };
        }

        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
        $data['roles'] = $this->allowed_roles;
        if ($this->input->is_ajax_request()) {
            $this->load->view('Maintenance/addGroup', $data);
        } else {
            $data['site_title'] = 'Add New Group';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('Maintenance/addGroup', $data);
            $this->load->view('main/footer', $data);
        }
    }

    /**************************************************
     *                    ADD MATERIAL.
     **************************************************/

    public function material()
    {
        if (isset($_POST['material_name'])) {

            $material = new Material_Model();
            // $material->material_id = $this->Material_Model->max();
            $material->material_name = $this->input->post('material_name');
            $material->category_id = $this->input->post('category');
            $material->group_id = $this->input->post('group');
            $material->unit = $this->input->post('unit');
            $material->size = $this->input->post('size');
            $material->thickness = $this->input->post('micron');
            $material->rate = $this->input->post('price');
            $material->density = $this->input->post('density');
            $material->created_by = $this->session->userdata('user_id');
            $material->created_time = dbTime();
            $material->updated_by = 0;
            $material->updated_time = '';
            if ($material->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Material Added Successfully.');
                    redirect(site_url('maintenance/material_list'));
                }
            };
            // check($this->db->last_query());
        }

        $categories = $this->Category_Model->get();
        foreach ($categories as $category) {
            $data['categories'][$category->category_id] = $category->category_name;
        }

        $groups = $this->Group_Model->get();
        foreach ($groups as $group) {
            $data['groups'][$group->group_id] = $group->group_name;
        }

        $data['units'] = ['KG' => 'KG', 'PCS' => 'PCS', 'MTR' => 'MTR', 'FT' => 'FT'];
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';


        $data['roles'] = $this->allowed_roles;
        if ($this->input->is_ajax_request()) {
            $this->load->view('Maintenance/addMaterial', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('Maintenance/addMaterial', $data);
            $this->load->view('main/footer', $data);
        }
    }

    /**************************************************
     *                    ADD MATERIAL.
     **************************************************/

    public function job_structure()
    {
        if (isset($_POST['job_name'])) {

            $structure = new Job_Master_Model();

            // $structure->id = $this->Job_Master_Model->max();
            $structure->job_name = $this->input->post('job_name');
            $structure->file_number = $this->input->post('file_number');
            $structure->print_type = $this->input->post('print_type');
            $structure->print_as_per = $this->input->post('print_as_per');
            $structure->cylinder = $this->input->post('cylinder');
            $structure->cylinder_len = $this->input->post('cylinder_len');
            $structure->cylinder_circum = $this->input->post('cylinder_circum');
            $structure->no_ups = $this->input->post('no_ups');
            $structure->no_repeat = $this->input->post('no_repeat');
            $structure->no_lamination = $this->input->post('no_lamination');
            $structure->print_material = $this->input->post('print_material');
            $structure->lam1_material = $this->input->post('lam1_material');
            $structure->lam2_material = $this->input->post('lam2_material');
            $structure->lam3_material = $this->input->post('lam3_material');
            $structure->slitting_reel_width = $this->input->post('slitting_reel_width');
            $structure->slitting_cutting_size = $this->input->post('slitting_cutting_size');
            $structure->slitting_direction = $this->input->post('slitting_direction');
            $structure->ink_series = $this->input->post('ink_series');
            $structure->no_of_inks = $this->input->post('no_of_inks');
            $structure->bm_qty = $this->input->post('bm_qty');
            $structure->bm_width = $this->input->post('bm_width');
            $structure->bm_height = $this->input->post('bm_height');
            $structure->bm_types = $this->input->post('bm_type');
            $structure->coa_requird = $this->input->post('coa_requird');
            $structure->dc = $this->input->post('dc');
            $structure->hold = $this->input->post('hold');
            $structure->remarks = $this->input->post('remarks');
            $structure->reason = $this->input->post('reason');
            $structure->create_by = $this->session->userdata('user_id');
            $structure->created_time = dbTime();
            $structure->update_by = $this->session->userdata('');
            $structure->updated_time = '';
            $structure->reason = '';

            if ($structure->save()) {

                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Structure Added Successfully.');
                    redirect(site_url('maintenance/structure_list'));
                }

            }else{
                echo json_encode($this->db->error());
                return;
            }
        }

        $data['roles'] = $this->allowed_roles;
        $data['job_code'] = $this->Job_Master_Model->max();

        $data['print_material_name'] = '';
        $data['lam1_material_name'] = '';
        $data['lam2_material_name'] = '';
        $data['lam3_material_name'] = '';
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('transictions/job_structure2', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('transictions/job_structure2', $data);
            $this->load->view('main/footer', $data);
        }
    }


    /**************************************************
     *                    ADD SALE ORDER.
     **************************************************/

    public function sale_order()
    {
        $data['job_code'] = $this->Sale_Order_Model->max();
        if (isset($_POST['structure_id'])) {

            $meter = 0;
            $weight = [];
            $quantity = $this->input->post('quantity');


            $structure = new Job_Master_Model();
            $structure->load($this->input->post('structure_id'));

            $films = ['print_material', 'lam1_material', 'lam2_material', 'lam3_material'];

            if ($this->input->post('order_type') == 'pcs') {
                $meter = $structure->bm_height * $quantity / ($structure->no_ups > 0 ? $structure->no_ups : 1) / 1000;

                $i = 0;
                $add = 0;
                foreach ($films as $film) {
                    // $add = ( $structure->{$film} ) ? 20: 0;

                    $material = new Material_Model();
                    $material->load($structure->{$film});
                    $weight[] = $meter / 1000000 * $material->size * $material->thickness * $material->density + $add;

                }

            }// if order in pcs
            elseif ($this->input->post('order_type') == 'kg') {
                $gramage = [];

                foreach ($films as $film) {
                    $material = new Material_Model();
                    $material->load($structure->{$film});
                    $gramage[] = $material->density * $material->thickness;
                }

                foreach ($gramage as $g) {
                    $weight[] = $g / array_sum($gramage) * $quantity;
                }

                $material = new Material_Model();
                $material->load($structure->print_material);
                $meter = $weight[0] / $material->density / $material->thickness / $material->size * 1000000;
            }
            $so = new Sale_Order_Model();

            // $so->job_code       = $data['job_code'];
            $so->date           = dbDate($this->input->post('date'));
            $so->po_num         = $this->input->post('po_num');
            $so->po_date        = dbDate($this->input->post('po_date'));
            $so->delivery_date  = dbDate($this->input->post('delivery_date'));
            $so->structure_id   = $this->input->post('structure_id');
            $so->customer_id    = $this->input->post('customer_id');
            $so->order_type     = $this->input->post('order_type');
            $so->quantity       = $this->input->post('quantity');
            $so->rate           = $this->input->post('rate');
            $so->meter          = $meter;
            $so->film1_wt       = $weight[0];
            $so->film2_wt       = $weight[1];
            $so->film3_wt       = $weight[2];
            $so->film4_wt       = $weight[3];
            $so->remarks        = $this->input->post('remarks');
            $so->deliveries     = serialize($this->input->post('deliveries'));
            $so->hold           = $this->input->post('hold') ? 1 : 0;
            $so->completed      = 0;
            $so->created_by     = $this->session->userdata('user_id');
            $so->created_time   = dbTime();
            $so->updated_by     = 0;
            $so->updated_time   = '';
            $result = $so->save();
            // x($this->db->last_query());
            if ($result) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'Sale Order Generated Successfully.');
                    redirect(site_url('maintenance/sale_order_list'));
                }
            }
        } // if post isset()

        $data['roles']      = $this->allowed_roles;
        $data['feed_back']  = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('transictions/sale_order', $data);
        } else {
            $data['site_title'] = 'Add New Sale Order';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('transictions/sale_order', $data);
            $this->load->view('main/footer', $data);
        }

    } // SALE ORDER

    /**************************************************
     *                    =GOODS RECEIVED NOTE.
     **************************************************/
    public function goods_receive_note()
    {
        $data['title'] = 'Receive Inventory || Create GRN.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $grn = new Grn_Model();
            // $grn->grn_id = $this->Grn_Model->max();
            $grn->date = dbDate($this->input->post('date'));
            // $grn->supplier_id = $this->input->post('supplier_id');
            $grn->description = $this->input->post('description');
            $grn->created_by = $this->session->userdata('user_id');
            $grn->created_time = dbTime();
            $grn->updated_by = 0;
            $grn->updated_time = $this->input->post('');
            // if (true) {
            if ($grn->save()) {
                // var_dump($_POST);
                for ($i = 0; $i < count($this->input->post('material')); $i++) {
                    $item = new Grn_detail_Model();
                    $item->grnd_id = '';
                    $item->grn_id = $grn->grn_id;
                    $item->material_id = $this->input->post('material')[$i];
                    $item->store_id = $this->input->post('mStore')[$i];
                    $item->roll_carton = $this->input->post('mRoll')[$i];
                    $item->qty = $this->input->post('mQuantity')[$i];
                    $item->rate = $this->input->post('mRate')[$i];
                    $item->defective = $this->input->post('mDefective')[$i];
                    $item->description = $this->input->post('mDescription')[$i];
                    $item->po_id = $this->input->post('po_id')[$i];
                    if ($item->save()) {
                        // echo "Sub Statement Inserted.!<br>";
                    }

                }
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Record Added Successfully.');
                    redirect(site_url('maintenance/grn_list'));
                }
            };
        }

        $data['roles'] = $this->allowed_roles;
        $store = new Store_Model();
        $data['stores'] = $store->get();
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('store/grn', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('store/grn', $data);
            $this->load->view('main/footer', $data);
        }

    } // goods_receive_note()


    /**************************************************
     *                    =GOODS ISSUE NOTE.
     **************************************************/
    public function goods_issue_note()
    {
        $data['title'] = 'Receive Inventory || Create Goods Issue Note.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $gin = new Gin_Model();
            // $gin->gin_id = $this->Gin_Model->max();
            $gin->date = dbDate($this->input->post('date'));
            $gin->issue_to = $this->input->post('issue_to');
            $gin->description = $this->input->post('description');
            $gin->created_by = $this->session->userdata('user_id');
            $gin->created_time = dbTime();
            $gin->updated_by = 0;
            $gin->updated_time = $this->input->post('');
            // if (true) {
            if ($gin->save()) {
                // var_dump($_POST);
                for ($i = 0; $i < count($this->input->post('material')); $i++) {
                    $item = new Gin_detail_Model();
                    $item->gind_id = '';
                    $item->gin_id = $gin->gin_id;
                    $item->material_id = $this->input->post('material')[$i];
                    $item->store_id = $this->input->post('mStore')[$i];
                    $item->roll_carton = $this->input->post('mRoll')[$i];
                    $item->qty = $this->input->post('mQuantity')[$i];
                    $item->rate = $this->input->post('mRate')[$i];
                    $div = $this->input->post('mDefective')[$i];
                    $div = $div ? $div : 0;
                    $item->defective = $div;
                    $item->description = $this->input->post('mDescription')[$i];
                    $item->job_code = '';
                    if ($item->save()) {
                        // echo "Sub Statement Inserted.!<br>";
                    }

                }
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Record Added Successfully.');
                    redirect(site_url('maintenance/gin_list'));
                }
            };
        }

        $data['roles'] = $this->allowed_roles;
        $store = new Store_Model();
        $data['stores'] = $store->get();
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('store/gin', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('store/gin', $data);
            $this->load->view('main/footer', $data);
        }

    } // goods_issue_note()


    /**************************************************
     *                    =PRINTING ENTRY.
     **************************************************/
    public function printing_entry()
    {
        $data['title'] = 'Printing Entry || Generate New Printing Data.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $print = new Printing_Model();
            // $print->printing_id = $this->Printing_Model->max();
            $print->so = $this->input->post('so');
            $print->date = dbDate($this->input->post('date'));
            $print->machine = $this->input->post('machine');
            $print->operator = $this->input->post('operator');
            // $print->assistant = $this->input->post('assistant');
            // $print->helper = $this->input->post('helper');
            $print->production_meter = $this->input->post('production_meter');
            $print->plain_wastage = $this->input->post('plain_wastage');
            $print->printed_wastage = $this->input->post('printed_wastage');
            $print->setting_wastage = $this->input->post('setting_wastage');
            $print->matching_wastage = $this->input->post('matching_wastage');
            $print->qc_hold = $this->input->post('qc_hold');
            $print->start_time = $this->input->post('machine_start_time');
            $print->stop_time = $this->input->post('machine_stop_time');
            $print->created_by = $this->session->userdata('user_id');
            $print->created_time = dbTime();
            $print->updated_by = 0;
            $print->updated_time = $this->input->post('');
            // if (true) {
            if ($print->save()) {
                // var_dump($_POST);
                for ($i = 0; $i < count($this->input->post('plain_wt')); $i++) {
                    $item = new Printing_Detail_Model();
                    // $item->pd_id = $this->Printing_Detail_Model->max();
                    $item->printing_id = $print->printing_id;
                    $item->plain_wt = $this->input->post('plain_wt')[$i];
                    $item->printed_wt = $this->input->post('printed_wt')[$i];
                    $item->meter = $this->input->post('meter')[$i];
                    $item->start_time = $this->input->post('start_time')[$i];
                    $item->end_time = $this->input->post('end_time')[$i];
                    $item->remarks = $this->input->post('remarks')[$i];
                    $item->save();
                }

                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Record Added Successfully.');
                    redirect(site_url('maintenance/printing_entry_list'));
                }

            }

        }

        $data['roles'] = $this->allowed_roles;
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('transictions/printing', $data);
        } else {
            $data['site_title'] = 'Add New Printing Entry!';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('transictions/printing', $data);
            $this->load->view('main/footer', $data);
        }

    } // printing_entry()


    /**************************************************
     *                    =REWINDING ENTRY.
     **************************************************/
    public function rewinding_entry()
    {
        $data['title'] = 'Rewinding Entry || Generate New Rewinding Data.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $rewind = new Rewinding_Model();
            // $rewind->id = $this->Rewinding_Model->max();
            $rewind->date = dbDate($this->input->post('date'));
            $rewind->so = $this->input->post('so');
            $rewind->operator = $this->input->post('operator');
            $rewind->start_time = $this->input->post('start_time');
            $rewind->end_time = $this->input->post('end_time');
            $rewind->weight_before = $this->input->post('weight_before');
            $rewind->weight_after = $this->input->post('weight_after');
            $rewind->wastage = $this->input->post('wastage');
//            $rewind->trim = $this->input->post('trim');
            $rewind->qc_hold = $this->input->post('qc_hold');
//            $rewind->direction = $this->input->post('direction');
            $rewind->remarks = $this->input->post('remarks');
            $rewind->created_by = $this->session->userdata('user_id');
            $rewind->created_time = dbTime();
            $rewind->updated_by = 0;
            $rewind->updated_time = $this->input->post('');
            // if (true) {
            if ($rewind->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
                    redirect(site_url('maintenance/rewinding_entry_list'));
                }
            }
        }

        $data['roles'] = $this->allowed_roles;
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('transictions/rewinding', $data);
        } else {
            $data['site_title'] = 'Add New Rewinding Entry.';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('transictions/rewinding', $data);
            $this->load->view('main/footer', $data);
        }

    } // rewinding_entry()


    /**************************************************
     *                    =LAMINATION ENTRY.
     **************************************************/
    public function lamination_entry()
    {
        $data['title'] = 'Lamination Entry || Generate New Printing Data.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $this->db->trans_start();
            $lam = new Lamination_Model();
            // $lam->id = $this->Lamination_Model->max();
            $lam->so = $this->input->post('so');
            $lam->date = dbDate($this->input->post('date'));
            $lam->machine = $this->input->post('machine');
            $lam->operator = $this->input->post('operator');
            $lam->assistant = $this->input->post('assistant');
            $lam->helper = $this->input->post('helper');
            $lam->temperature = $this->input->post('temperature');
            $lam->rubber_size = $this->input->post('rubber_size');
            $lam->speed = $this->input->post('speed');
            $lam->qc_hold = $this->input->post('qc_hold');
            $lam->start_time = $this->input->post('start_time');
            $lam->end_time = $this->input->post('end_time');
            $lam->blade = $this->input->post('blade');
            $lam->lamination_number = $this->input->post('lamination_number');
            $lam->total_wastage = $this->input->post('total_wastage');
            $lam->balance = $this->input->post('balance');
            $lam->glue = $this->input->post('glue');
            $lam->glue_qty = $this->input->post('glue_qty');
            $lam->hardner = $this->input->post('hardner');
            $lam->hardner_qty = $this->input->post('hardner_qty');
            $lam->ethyl = $this->input->post('ethyl');
            $lam->ethyl_qty = $this->input->post('ethyl_qty');
            $lam->created_by = $this->session->userdata('user_id');
            $lam->created_time = dbTime();
            $lam->updated_by = 0;
            $lam->updated_time = $this->input->post('');
            if ($lam->save()) {

                for ($i = 0; $i < count($this->input->post('printed_weight')); $i++) {
                    $item = new Lamination_Detail_Model();
                    // $item->ld_id = $this->Lamination_Detail_Model->max();
                    $item->lamination_id = $lam->id;
                    $item->printed_weight = $this->input->post('printed_weight')[$i];
                    $item->plain_weight = $this->input->post('plain_weight')[$i];
                    $item->laminated_weight = $this->input->post('laminated_weight')[$i];
                    $item->treatment = $this->input->post('treatment')[$i];
                    $item->meter = $this->input->post('meter')[$i];
                    $item->gsm = $this->input->post('gsm')[$i];
                    $item->remarks = $this->input->post('remarks')[$i];
                    $item->save();
                }
                if (!$this->db->trans_status()) {
                    $this->db->trans_rollback();
                } else {
                    $this->db->trans_complete();
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
                        redirect(site_url('maintenance/lamination_entry_list'));
                    }
                }

            } // lamination insert

        } // if post is set

        $data['roles'] = $this->allowed_roles;
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('transictions/lamination', $data);
        } else {
            $data['site_title'] = 'Add New Lamination Entry.';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('transictions/lamination', $data);
            $this->load->view('main/footer', $data);
        }

    } // lamination()


    /**************************************************
     *                    =DISPATCH ENTRY.
     **************************************************/
    public function dispatch_entry()
    {
        $data['title'] = 'Lamination Entry || Generate New Printing Data.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            // var_dump($_POST);
            $dispatch = new Dispatch_Model();
            // $dispatch->id = $this->Dispatch_Model->max();
            $dispatch->date = dbDate($this->input->post('date'));
            $dispatch->so = $this->input->post('so');
            $dispatch->challan = $this->input->post('challan');
            $dispatch->gross_weight = $this->input->post('gross_weight');
            $dispatch->tare_weight = $this->input->post('tare_weight');
            $dispatch->net_weight = $this->input->post('net_weight');
            $dispatch->destination = $this->input->post('destination');
            $dispatch->receiver = $this->input->post('receiver');
            $dispatch->no_carton = $this->input->post('no_carton');
            $dispatch->no_pcs = $this->input->post('no_pcs');
            $dispatch->no_roll = $this->input->post('no_roll');
            $dispatch->meter = $this->input->post('meter');
            $dispatch->remarks = $this->input->post('remarks');
            $dispatch->driver_name = $this->input->post('driver_name');
            $dispatch->vehicle = $this->input->post('vehicle');
            $dispatch->created_by = $this->session->userdata('user_id');
            $dispatch->created_time = dbTime();
            $dispatch->updated_by = 0;
            $dispatch->updated_time = $this->input->post('');

            $this->db->trans_start();

            if ($dispatch->save()) {
                // var_dump($_POST);
                if ($this->input->post('fl_weight')) {
                    for ($i = 0; $i < count($this->input->post('fl_weight')); $i++) {
                        $item = new Dispatch_Detail_Model();
                        // $item->dd_id = $this->Dispatch_Detail_Model->max();
                        $item->dispatch_id = $dispatch->id;
                        $item->weight = $this->input->post('fl_weight')[$i];
                        $item->no_carton = $this->input->post('fl_no_carton')[$i];
                        $item->no_pcs = $this->input->post('fl_no_pcs')[$i];
                        $item->no_roll = $this->input->post('fl_no_roll')[$i];
                        $item->description = $this->input->post('description')[$i];
                        $item->save();
                    }
                } // if flavour isset

                if (!$this->db->trans_status()) {
                    $this->db->trans_rollback();
                } else {

                    $this->db->trans_complete();

                    if (net_on()) {

                        // $this->load->library('email');

                        // $this->email->from('arif.iqbal@printechpackages.com');
                        // $this->email->to('hdarif2@gmail.com', 'Gmail');
                        // // $this->email->cc('another@another-example.com');
                        // // $this->email->bcc('them@their-example.com');

                        // $this->email->subject('Job Dispatch to Customer!');
                        // $this->email->message(dispatch_email_body('2345', 'punch pan masala', 'syed arif iqbal'));
                        // $this->email->send();
                    }
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
                        // redirect(site_url('maintenance/dispatch_entry_list'));
                    }
                }

            } // lamination insert

        } // if post is set

        $data['roles'] = $this->allowed_roles;
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('transictions/dispatch', $data);
        } else {
            $data['site_title'] = 'Add New Dispatch Entry.';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('transictions/dispatch', $data);
            $this->load->view('main/footer', $data);
        }
    } // dispatch_entry()


    /**************************************************
     *                    =SLITTING ENTRY.
     **************************************************/
    public function slitting_entry()
    {
        $data['title'] = 'Slitting Entry || Generate New Slitting Data.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $slitting = new Slitting_Model();
            // $slitting->id = $this->Slitting_Model->max();
            $slitting->date = dbDate($this->input->post('date'));
            $slitting->so = $this->input->post('so');
            $slitting->operator = $this->input->post('operator');
            $slitting->start_time = $this->input->post('start_time');
            $slitting->end_time = $this->input->post('end_time');
            $slitting->weight_before = $this->input->post('weight_before');
            $slitting->weight_after = $this->input->post('weight_after');
            $slitting->wastage = $this->input->post('wastage');
            $slitting->trim = $this->input->post('trim');
            $slitting->direction = $this->input->post('direction');
            $slitting->remarks = $this->input->post('remarks');
            $slitting->created_by = $this->session->userdata('user_id');
            $slitting->created_time = dbTime();
            $slitting->updated_by = 0;
            $slitting->updated_time = $this->input->post('');
            if ($slitting->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
                    redirect(site_url('maintenance/slitting_entry_list'));
                }
            }
        }

        $data['roles'] = $this->allowed_roles;
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('transictions/slitting', $data);
        } else {
            $data['site_title'] = 'Add New Rewinding Entry.';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('transictions/slitting', $data);
            $this->load->view('main/footer', $data);
        }

    } // slitting_entry()


    /**************************************************
     *                    =MAINENANCE REQUEST.
     **************************************************/
    public function maintenance_request()
    {
        $data['title'] = 'Maintenance Request Form';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $request = new Maintenance_Model();
            // $request->id = $this->Maintenance_Model->max();
            // $request->date = dbDate($this->input->post('date'));
            $request->req_type = $this->input->post('req_type');
            $request->req_for = $this->input->post('req_for');
            $request->request_from = $this->input->post('request_from');
            $request->request_person = ''; // $this->input->post('request_person');
            $request->problem = $this->input->post('problem');
            $request->suggestion = $this->input->post('suggestion');
            $request->pending = 1;
            $request->verified_requester = 0;
            $request->created_by = $this->session->userdata('user_id');
            $request->created_time = dbTime();

            if ($request->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
                    redirect(site_url('maintenance/maintenance_list'));
                }
            }
        }

        $data['roles'] = $this->allowed_roles;
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('maintenance/maintenance_request', $data);
        } else {
            $data['site_title'] = 'Add New Maintenance Request.';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('maintenance/maintenance_request', $data);
            $this->load->view('main/footer', $data);
        }

    } // maintenance_request()


    /**************************************************
     *                    =MAINENANCE REVIEW.
     **************************************************/
    public function maintenance_review()
    {
        $data['title'] = 'Maintenance Request Review';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $request = new Maintenance_Model();
            // $request->id = $this->Maintenance_Model->max();
            // $request->date = dbDate($this->input->post('date'));
            $request->treatment = $this->input->post('treatment');
            $request->solution = $this->input->post('solution');
            $request->solved_by = $this->input->post('solved_by');
            $request->verified_incharge = $this->input->post('verified_incharge');
            $request->problem = $this->input->post('problem');
            $request->suggestion = $this->input->post('suggestion');
            $request->pending = 1;
            $request->verified_requester = 0;
            $request->created_by = $this->session->userdata('user_id');
            $request->treat_time = dbTime();

            if ($request->save()) {
                echo "Successfully Inserted";
                return;
            }
        }

        $data['roles'] = $this->allowed_roles;

        $this->load->view('maintenance/maintenance_request', $data);

    } // maintenance_review()


    /**************************************************
     *                    =PURCHASE ORDER.
     **************************************************/
    public function purchase_order()
    {
        $data['title'] = 'Receive Inventory || Create GRN.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $po = new Purchase_order_Model();
            // $po->po_id = $this->Purchase_order_Model->max();
            $po->date = dbDate($this->input->post('date'));
            $po->supplier_id = $this->input->post('supplier_id');
            $hold = ($this->input->post('hold') == "on") ? 1 : 0;
            $po->hold = $hold;
            $po->description = $this->input->post('description');
            $po->created_by = $this->session->userdata('user_id');
            $po->created_time = dbTime();
            $po->updated_by = 0;
            $po->updated_time = $this->input->post('');
            // if (true) {
            if ($po->save()) {
                // var_dump($_POST);
                for ($i = 0; $i < count($this->input->post('material')); $i++) {
                    $item = new PO_Detail_Model();
                    $item->pod_id = '';
                    $item->po_id = $po->po_id;
                    $item->material_id = $this->input->post('material')[$i];
                    $item->qty = $this->input->post('mQuantity')[$i];
                    $item->rate = $this->input->post('mRate')[$i];
                    $item->description = $this->input->post('mDescription')[$i];
                    if ($item->save()) {
                        // echo "Sub Statement Inserted.!<br>";
                    }

                }
                echo "Successfully Inserted";
                return;
            };
        }

        $data['roles'] = $this->allowed_roles;

        // $this->load->view('header', $data);
        $this->load->view('store/purchase_order', $data);
        // $this->load->view('footer', $data);

    } // purchase_order()


    /**************************************************
     *                    =QUOTATIONS.
     **************************************************/
    public function quotation($form_type)
    {
        $data['title'] = 'New Quotation Form.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            $quotation                      = new Quotation_Model();
            // $quotation->id                  = $this->Quotation_Model->max();
            $quotation->date                = dbDate($this->input->post('date'));
            $quotation->gender              = $this->input->post('gender');
            $quotation->party               = $this->input->post('party');
            $quotation->attention           = $this->input->post('attention');
            $quotation->product             = $this->input->post('product');
            $quotation->structure           = $this->input->post('structure');
            $quotation->printing            = $this->input->post('printing');
            if ($form_type == 'Offset') {
            $quotation->gsm                 = $this->input->post('gsm');
            $quotation->quality             = $this->input->post('quality');
            $quotation->finishing           = $this->input->post('finishing');
            $quotation->other_specification = $this->input->post('other_specification');
            }
            if ( strtolower($form_type) == 'gravure' ) {
            $quotation->yield               = $this->input->post('yield');
            $quotation->cylinder_cost       = $this->input->post('cylinder_cost');
            $quotation->size                = $this->input->post('size');
            $quotation->bag_making          = $this->input->post('bag_making');
            $quotation->condition_4         = $this->input->post('condition_4');
            }
            $quotation->price               = $this->input->post('price');
            $quotation->moq                 = $this->input->post('moq');
            $quotation->department          = $form_type;
            $quotation->person_name         = $this->input->post('person_name');
            $quotation->designation         = $this->input->post('designation');
            $quotation->contact             = $this->input->post('contact');
            $quotation->condition_1         = $this->input->post('condition_1');
            $quotation->condition_2         = $this->input->post('condition_2');
            $quotation->condition_3         = $this->input->post('condition_3');
            $quotation->condition_5         = $this->input->post('condition_5');
            $quotation->created_by          = $this->session->userdata('user_id');
            $quotation->created_time        = dbTime();
            $quotation->updated_by          = 0;
            $quotation->updated_time        = '';
            // if (true) {
            if ($quotation->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
                    redirect(site_url('maintenance/quotation_list/' . $quotation->department));
                }
            }
        }

        $data['roles'] = $this->allowed_roles;
        $data['form_type'] = $form_type;
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('Marketing/quotation', $data);
        } else {
            $data['site_title'] = 'Add New Quotation.';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('Marketing/quotation', $data);
            $this->load->view('main/footer', $data);
        }

    } // quotation()


    /**************************************************
     *                    =QUOTATIONS.
     **************************************************/
    public function offset_costing()
    {
        $data['title'] = 'New Costing.!';
        $data['session'] = $this->session->all_userdata();
        if ($_POST) {
            // var_dump( $_POST );
            $obj = new Offset_Costing_Model();
            // $obj->id = $this->Offset_Costing_Model->max();
            $obj->date = dbDate($this->input->post('date'));
            $obj->customer_id = $this->input->post('customer_id');
            $obj->item_name = $this->input->post('item_name');
            $obj->moq = $this->input->post('moq');
            $obj->no_of_color = $this->input->post('no_of_color');
            $obj->rate_per_color = $this->input->post('rate_per_color');
            $obj->pcs_per_sheet = $this->input->post('pcs_per_sheet');
            $obj->sale_tax = $this->input->post('sale_tax');

            if ($this->input->post('show_both_amount')) {
                $obj->show_both_amount = 1;
            } else {
                $obj->show_both_amount = 0;
            }

            $obj->board_width = $this->input->post('board_width');
            $obj->board_height = $this->input->post('board_height');
            $obj->board_gsm = $this->input->post('board_gsm');
            $obj->board_rate = $this->input->post('board_rate');
            $obj->no_unit_per_packet = $this->input->post('no_unit_per_packet');
            $obj->wastage_percent = $this->input->post('wastage_percent');
            $obj->uv_width = $this->input->post('uv_width');
            $obj->uv_height = $this->input->post('uv_height');
            $obj->uv_rate = $this->input->post('uv_rate');
            $obj->lamination_width = $this->input->post('lamination_width');
            $obj->lamination_height = $this->input->post('lamination_height');
            $obj->lamination_rate = $this->input->post('lamination_rate');
            $obj->foil_rate = $this->input->post('foil_rate');
            if ($this->input->post('both_side_lamination')) {
                $obj->both_side_lamination = 1;
            } else {
                $obj->both_side_lamination = 0;
            }
            $obj->sorting_rate = $this->input->post('sorting_rate');
            $obj->die_cutting_rate = $this->input->post('die_cutting_rate');
            $obj->embossing_rate = $this->input->post('embossing_rate');
            $obj->pasting_rate = $this->input->post('pasting_rate');
            $obj->packing_rate = $this->input->post('packing_rate');
            $obj->pcs_per_carton = $this->input->post('pcs_per_carton');

            $obj->film_width = $this->input->post('film_width');
            $obj->film_height = $this->input->post('film_height');
            $obj->no_film_development = $this->input->post('no_film_development');
            $obj->film_rate = $this->input->post('film_rate');
            $obj->no_plate = $this->input->post('no_plate');
            $obj->plate_rate = $this->input->post('plate_rate');
            $obj->no_ups = $this->input->post('no_ups');
            $obj->ups_rate = $this->input->post('ups_rate');
            $obj->proofing = $this->input->post('proofing');
            $obj->blanket = $this->input->post('blanket');
            $obj->block = $this->input->post('block');
            $obj->development_foil = $this->input->post('development_foil');
            $obj->designing = $this->input->post('designing');
            $obj->cartage = $this->input->post('cartage');
            $obj->pcs_per_delivery = $this->input->post('pcs_per_delivery');
            $obj->margin = $this->input->post('margin');
            $obj->description = $this->input->post('description');
            $obj->die_to_die_width = $this->input->post('die_to_die_width');
            $obj->die_to_die_height = $this->input->post('die_to_die_height');

            $obj->created_by = $this->session->userdata('user_id');
            $obj->created_time = dbTime();
            $obj->updated_by = 0;
            $obj->updated_time = '';


            $config['upload_path'] = './assets/offset_costing_products/';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size'] = '25600'; // 25 MB
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $this->session->set_flashdata('userMsg', $this->upload->display_errors() . ' Submited Without image!');
            } else {
                $data = $this->upload->data();
                $obj->attachment = $data['raw_name'] . $data['file_ext'];
            }

            $this->load->model('Offset_Costing_Foil_Model');

            $this->db->trans_start();
            $obj->save();
            if ($this->input->post('foil_width')[0] > 0) {
                for ($i = 0; $i < count($this->input->post('foil_width')); $i++) {
                    $foil = new Offset_Costing_Foil_Model();
                    // $foil->id = $this->Offset_Costing_Foil_Model->max();
                    $foil->costing_id = $obj->id;
                    $foil->width = $this->input->post('foil_width')[$i];
                    $foil->height = $this->input->post('foil_height')[$i];
                    $foil->amount = $this->input->post('foil_rate') * $this->input->post('foil_width')[$i] * $this->input->post('foil_height')[$i];
                    $foil->save();
                }
            }
            $result = $this->db->trans_complete();
            if ($result) {

                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
                    redirect(site_url('maintenance/offset_costing_list/'));
                }
            }
        }

        $data['cal']['printing_cost_per_pcs'] = '';
        $data['cal']['board_weight'] = '';
        $data['cal']['packet_cost'] = '';
        $data['cal']['per_pcs_cost'] = '';
        $data['cal']['final_cost'] = '';
        $data['cal']['uv_rate_per_packet'] = '';
        $data['cal']['uv_rate_per_pcs'] = '';
        $data['cal']['lamination_rate_per_packet'] = '';
        $data['cal']['lamination_rate_per_pcs'] = '';
        $data['cal']['sorting_cost'] = '';
        $data['cal']['die_cutting_cost'] = '';
        $data['cal']['pasting_cost'] = '';
        $data['cal']['packing_cost'] = '';
        $data['cal']['film_cost'] = '';
        $data['cal']['plates_cost'] = '';
        $data['cal']['die_cost'] = '';
        $data['cal']['total_development_cost'] = '';
        $data['cal']['cartage_cost'] = '';
        $data['cal']['embossing_cost'] = '';
        $data['cal']['margin_percent'] = '';
        $data['roles'] = $this->allowed_roles;
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('offset/costing', $data);
        } else {
            $data['site_title'] = 'Add New Quotation.';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('offset/costing', $data);
            $this->load->view('main/footer', $data);
        }

    } // offset_costing()


    function sticker_print(){

        if( isset( $_POST['submit'] ) ){
            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetMargins(26,20);
            $pdf->SetY(12);
            $w = 20; $h = 4.2; $c = 240;

            // $pdf->SetTextColor($c,$c,$c);
            // $pdf->SetDrawColor($c,$c,$c);
            // $pdf->SetDrawColor(0,0,0);
            for($i=1; $i<=9;$i++){

                $pdf->Ln(2.2);
                $pdf->SetFont("Arial","B",10);
                $pdf->Cell(80, $h+1,'PRINTECH PACKAGES (PVT) LTD',1,1,'C');
                $pdf->SetFont("Arial","",10);
                $pdf->Cell($w, $h,'Date:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('date'),1,0,'C');

                $pdf->Cell($w, $h,'Roll No:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('no_rolls'),1,1,'C');

                $pdf->Cell($w, $h,'Machine:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('machine_no'),1,0,'C');

                $pdf->Cell($w, $h,'Set No:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('number_of_set'),1,1,'C');

                $pdf->Cell($w, $h,'Job No:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('job_number'),1,0,'C');

                $pdf->Cell($w, $h,'Operator:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('operator'),1,1,'C');

                $pdf->Cell($w, $h,'Note:',1,0,'C');
                $pdf->Cell($w*3, $h,'',1,1,'C');
                $pdf->Ln(2.2);

            }

            for($i=1; $i<=9;$i++){

                // X and Y for second column
                $xCode = 112;
                if( $i == 1 ) { $pdf->SetY(12); }

                $pdf->Ln(2.2);

                $pdf->SetX($xCode);
                $pdf->SetFont("Arial","B",10);
                $pdf->Cell(80, $h+1,'PRINTECH PACKAGES (PVT) LTD',1,1,'C');
                $pdf->SetFont("Arial","",10);

                $pdf->SetX($xCode);
                $pdf->Cell($w, $h,'Date:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('date'),1,0,'C');

                $pdf->Cell($w, $h,'Roll No:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('no_rolls'),1,1,'C');

                $pdf->SetX($xCode);
                $pdf->Cell($w, $h,'Machine:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('machine_no'),1,0,'C');

                $pdf->Cell($w, $h,'Set No:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('number_of_set'),1,1,'C');

                $pdf->SetX($xCode);
                $pdf->Cell($w, $h,'Job No:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('job_number'),1,0,'C');

                $pdf->Cell($w, $h,'Operator:',1,0,'C');
                $pdf->Cell($w, $h,$this->input->post('operator'),1,1,'C');

                $pdf->SetX($xCode);
                $pdf->Cell($w, $h,'Note:',1,0,'C');
                $pdf->Cell($w*3, $h,'',1,1,'C');
                $pdf->Ln(2.2);

            }
            $pdf->Output();
        }else{
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            $data['site_title'] = 'Roll Sticker Print.';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('transictions/sticker_printing', $data);
            $this->load->view('main/footer', $data);
        }

    }

}