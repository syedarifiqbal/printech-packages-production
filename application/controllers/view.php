<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class View extends MY_Controller
{

    public $allowed_roles = array();

    # Methodes

    /**************************************************
     *                    =PRINTING ENTRY.
     **************************************************/

    public function printing_entry($id)
    {

        if (isset($id)) {

            $p = $this->db->query('SELECT st.job_name, p.*
										FROM printing AS p 
											INNER JOIN sale_order AS so 
												ON p.so = so.job_code 
											INNER JOIN job_master AS st ON st.id = so.structure_id
										WHERE p.printing_id = ' . $id
            )->row();

            // var_dump($material);

            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetMargins(15, 15);
            $pdf->SetFont('Arial', 'B', 16);
            $pdf->cell(0, 10, "Printech Packages PVT. LTD", 0, 1, 'C');
            $pdf->SetFont('Arial', 'BU', 12);
            $pdf->cell(0, 10, "Printing Entry", 0, 0, 'C');
            $pdf->Ln(20);

            $pdf->SetFont('Arial', '', 10);
            $vars = get_object_vars($p);
            $flds = [
                "printing_id" => "Ref#",
                'job_name' => "Job Name",
                'so' => "Job Code",
                'date' => 'Date',
                'machine' => 'Machine #',
                'operator' => 'Operator Name',
                'assistant' => 'Assistant Name',
                'helper' => 'Helper Name',
                'production_meter' => 'Meters',
                'plain_wastage' => 'Plain Wastage',
                'printech_wastage' => 'Printed Wastage',
                'setting_wastage' => 'Setting Wastage',
                'matching_wastage' => 'Matching Wastage',
                'rubber_size' => 'Rubber Size',
                'circum' => 'Curcum Size',
                'start_time' => 'Start Time',
                'end_time' => 'End Time'
            ];
            // var_dump($vars);
            $iterator = 1;
            foreach ($vars as $key => $value) {
                if (array_key_exists($key, $flds)) {
                    $val = $flds[$key] . '  :  ' . $p->{$key};
                    $pdf->cell(90, 8, $val, 0, 0);
                    if ($iterator % 2 == 0)
                        $pdf->Ln();
                    $iterator++;
                }
            }
            $pdf->Ln();

            $pd = new Printing_Detail_Model();
            $pd = $pd->getWhere(['printing_id' => $p->printing_id]);

            $detail = [
                'plain_wt' => 'Plain Weight',
                'printed_wt' => 'Printed Weight',
                'meter' => 'Meter',
                'start_time' => 'Start Time',
                'end_time' => 'End Time',
                'remarks' => 'Remarks'
            ];

            $detail_wd = [
                'plain_wt' => 28,
                'printed_wt' => 28,
                'meter' => 28,
                'start_time' => 35,
                'end_time' => 35,
                'remarks' => 28
            ];

            $pdf->SetFont('Arial', 'B', 10);
            foreach ($detail as $key => $heading) {
                $pdf->Cell($detail_wd[$key], 8, $heading, 1, 0, 'C');
            }
            $pdf->Ln();

            foreach ($pd as $d) {
                $dd = get_object_vars($d);
                foreach ($dd as $key => $value) {
                    if (array_key_exists($key, $detail)) {
                        $pdf->Cell($detail_wd[$key], 8, $d->{$key}, 1, 0);
                    }
                }
                $pdf->Ln();
            }
            // var_dump($pd);

            $pdf->Output();

        } // if id isset
        else {
            echo "Sorry No Record Found!";
        }

    } // printing_entry()

    /**************************************************
     *                    =REWINDING ENTRY.
     **************************************************/

    public function rewinding_entry($id)
    {

        if (isset($id)) {

            $rew = $this->db->query('SELECT st.job_name, r.*
										FROM rewinding AS r 
											INNER JOIN sale_order AS so 
												ON r.so = so.job_code 
											INNER JOIN job_master AS st ON st.id = so.structure_id
										WHERE r.id = ' . $id
            )->row();

            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetMargins(15, 15);
            $pdf->SetFont('Arial', 'B', 16);
            $pdf->cell(0, 10, "Printech Packages PVT. LTD", 0, 1, 'C');
            $pdf->SetFont('Arial', 'BU', 12);
            $pdf->cell(0, 10, "Rewinding Entry", 0, 0, 'C');
            $pdf->Ln(20);

            $pdf->SetFont('Arial', '', 10);
            $vars = get_object_vars($rew);
            $flds = [
                "id" => "Ref#",
                'job_name' => "Job Name",
                'so' => "Job Code",
                'date' => 'Date',
                'operator' => 'Operator Name',
                'weight_before' => 'Weight Before',
                'weight_after' => 'Weight After',
                'wastage' => 'Wastage',
                'trim' => 'Trim',
                'start_time' => 'Start Time',
                'end_time' => 'End Time'
            ];
            // var_dump($vars);
            $iterator = 1;
            foreach ($vars as $key => $value) {
                if (array_key_exists($key, $flds)) {
                    $val = $flds[$key] . '  :  ' . $rew->{$key};
                    $pdf->cell(90, 8, $val, 0, 0);
                    if ($iterator % 2 == 0)
                        $pdf->Ln();
                    $iterator++;
                }
            }
            $pdf->Ln();

            $pdf->cell(0, 8, "Remarks: ", 0, 1);
            $pdf->MultiCell(0, 8, $rew->remarks, $border = 0, $align = 'J', $fill = false);

            $pdf->Output();

        } // if id isset
        else {
            echo "Sorry No Record Found!";
        }

    } // rewinding_entry()

    /**************************************************
     *                    =SLITTING ENTRY.
     **************************************************/

    public function slitting_entry($id)
    {

        if (isset($id)) {

            // $slit = new Slitting_Model();
            $slit = $this->db->query('SELECT st.job_name, s.*
										FROM slitting AS s 
											INNER JOIN sale_order AS so 
												ON s.so = so.job_code 
											INNER JOIN job_master AS st ON st.id = so.structure_id
										WHERE s.id = ' . $id
            )->row();

            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetMargins(15, 15);
            $pdf->SetFont('Arial', 'B', 16);
            $pdf->cell(0, 10, "Printech Packages PVT. LTD", 0, 1, 'C');
            $pdf->SetFont('Arial', 'BU', 12);
            $pdf->cell(0, 10, "Rewinding Entry", 0, 0, 'C');
            $pdf->Ln(20);

            $pdf->SetFont('Arial', '', 10);
            $vars = get_object_vars($slit);
            $flds = [
                "id" => "Ref#",
                'date' => 'Date',
                'so' => "Job Code",
                'job_name' => "Job Name",
                'operator' => 'Operator Name',
                'weight_before' => 'Weight Before',
                'weight_after' => 'Weight After',
                'wastage' => 'Wastage',
                'trim' => 'Trim',
                'start_time' => 'Start Time',
                'end_time' => 'End Time'
            ];
            // var_dump($vars);
            $iterator = 1;
            foreach ($vars as $key => $value) {
                if (array_key_exists($key, $flds)) {
                    $val = $flds[$key] . '  :  ' . $slit->{$key};
                    $pdf->cell(90, 8, $val, 0, 0);
                    if ($iterator % 2 == 0)
                        $pdf->Ln();
                    $iterator++;
                }
            }
            $pdf->Ln();

            $pdf->cell(0, 8, "Remarks: ", 0, 1);
            $pdf->MultiCell(0, 8, ($slit->remarks) ? $slit->remarks : "", $border = 0, $align = 'J', $fill = false);

            $pdf->Output();

        } // if id isset
        else {
            echo "Sorry No Record Found!";
        }

    } // slitting_entry()

    /**************************************************
     *                    =LAMINATION ENTRY.
     **************************************************/

    public function lamination_entry($id)
    {

        if (isset($id)) {

            $lam = $this->db->query('SELECT st.job_name, l.*
										FROM lamination AS l 
											INNER JOIN sale_order AS so 
												ON l.so = so.job_code 
											INNER JOIN job_master AS st ON st.id = so.structure_id
										WHERE l.id =' . $id
            )->row();
            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetMargins(15, 15);
            $pdf->SetFont('Arial', 'B', 16);
            $pdf->cell(0, 10, "Printech Packages PVT. LTD", 0, 1, 'C');
            $pdf->SetFont('Arial', 'BU', 12);
            $pdf->cell(0, 10, "Lamination Entry", 0, 0, 'C');
            $pdf->Ln(20);

            $pdf->SetFont('Arial', '', 10);
            $vars = get_object_vars($lam);
            $flds = [
                "id" => "Ref#",
                'job_name' => "Job Name",
                'so' => "Job Code",
                'date' => 'Date',
                'operator' => 'Operator Name',
                'assistant' => 'Assistant Name',
                'helper' => 'Helper Name',
                'temperature' => 'Temperature',
                'rubber_size' => 'Rubber Size',
                'speed' => 'Speed',
                'blade' => 'Blade',
                'lamination_number' => 'Lamination #',
                'total_wastage' => 'Wastage',
                'balance' => 'Balance',
                'glue' => 'Glue',
                'glue_qty' => 'Glue Weight',
                'harder' => 'Harder',
                'hardner_qty' => 'Hardner Weight',
                'start_time' => 'Start Time',
                'end_time' => 'End Time'
            ];

            $iterator = 1;
            foreach ($vars as $key => $value) {
                if (array_key_exists($key, $flds)) {
                    $val = $flds[$key] . '  :  ' . $lam->{$key};
                    $pdf->cell(90, 8, $val, 0, 0);
                    if ($iterator % 2 == 0)
                        $pdf->Ln();
                    $iterator++;
                }
            }
            $pdf->Ln();

            $pd = new Lamination_Detail_Model();
            $pd = $pd->getWhere(['lamination_id' => $lam->id]);

            $detail = [
                'printed_weight' => 'Printed Weight',
                'plain_weight' => 'Plain Weight',
                'laminated_weight' => 'Laminated Weight',
                'treatment' => 'Treatment',
                'meter' => 'Meter',
                'remarks' => 'Remarks'
            ];

            $detail_wd = [
                'printed_weight' => 28,
                'plain_weight' => 28,
                'laminated_weight' => 32,
                'treatment' => 28,
                'meter' => 28,
                'gsm' => 35,
                'remarks' => 0
            ];

            $pdf->SetFont('Arial', 'B', 10);
            foreach ($detail as $key => $heading) {
                $pdf->Cell($detail_wd[$key], 8, $heading, 1, 0, 'C');
            }
            $pdf->Ln();

            foreach ($pd as $d) {
                $dd = get_object_vars($d);
                foreach ($dd as $key => $value) {
                    if (array_key_exists($key, $detail)) {
                        $pdf->Cell($detail_wd[$key], 8, $d->{$key}, 1, 0);
                    }
                }
                $pdf->Ln();
            }
            $pdf->Output();
        } // if id isset
        else {
            echo "Sorry No Record Found!";
        }

    } // lamination_entry()


    /**************************************************
     *                    =MATERIAL.
     **************************************************/

    public function material($id)
    {

        if (isset($id)) {

            $material = new Material_Model();
            $material->load($id);

            // var_dump($material);

            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetMargins(15, 15);
            $pdf->SetFont('Arial', 'B', 16);
            $pdf->cell(0, 10, "Printech Packages PVT. LTD", 0, 0, 'C');
            $pdf->Ln();

            $pdf->SetFont('Arial', '', 10);
            $material = get_object_vars($material);
            // var_dump($material);
            foreach ($material as $key => $value) {
                if (is_array($key)) {
                    continue;
                }
                // $pdf->cell(70,10,$key,0,0);
                // $pdf->cell(70,10,$value,0,1);
            }

            $pdf->Output();

        } // if id isset

    } // material()


}