<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Practice extends MY_Controller {

	# Methodes 

	public function index()
	{
		$data['db'] = $this->db->query("SHOW DATABASES")->result();
		$this->config->item('testConfig','Hello this is testing from runtime config');
		var_dump($data['db']);
		var_dump($this->config->item('site_name'));
	}



	public function directoryIterator()
	{
		$dir = new DirectoryIterator("assets/images/");
		foreach ($dir as $file) {
			if ($file->isFile()) {
				$files[] = clone $file;
			}
		}
		foreach ($files as $f) {
			var_dump($f->getFilename());
		}

	} //  directoryIterator()


	public function filesystemIterator()
	{
		$dir = new FilesystemIterator("assets");
		foreach ($dir as $file) {
				echo $file->getFilename() ." : size ";
				echo ($file->getSize()/1024/1024)." <br/>";
				var_dump( ( $file ) );
				$files[] = $file;
			if ($file->isFile()) {
				$files[] = clone $file;
				// echo $file;
			}
		}

		var_dump( get_object_vars($files[0]) );

	} //  filesystemIterator()


}