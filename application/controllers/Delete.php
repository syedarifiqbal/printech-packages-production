<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Delete extends MY_Controller {

	public $allowed_roles = array();

	# Methodes
	/**************************************************
	*					DELETE CUSTOMER.
	**************************************************/

	public function customer($id=null)
	{
		if ($id) {
			
			$customer = new Customer_Model();
			$customer->load($id);

			if ($customer->customer_id){

				$customer->delete();

				if($this->db->error()['message']){
					check($this->db->error()['message']);
					
				}else{
					
					if ($this->input->is_ajax_request()) {
						echo "Successfully Deleted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', $customer->customer_name.' Deleted Successfully.');
						redirect(site_url('Maintenance/Customer_list'));
					}
				}

			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
		{
			echo "<h1>Please Follow Given Link.!</h1>";
		}

	} // END DELETE CUSTOMER FUCTION

	/**************************************************
	*					DELETE SUPPLIER.
	**************************************************/

	public function supplier($id=null)
	{
		if ($id) {
			
			$supplier = new Supplier_Model();
			$supplier->load($id);

			if ($supplier->supplier_id){
				$supplier->delete();
				if($this->db->error()['message']){
					check($this->db->error()['message']);
					echo "Can not delete this Record.!";
				}else{
					
					if ($this->input->is_ajax_request()) {
						echo "Successfully Deleted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', $customer->customer_name.' Deleted Successfully.');
						redirect(site_url('Maintenance/supplier_list'));
					}
				}
			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
		{
			echo "<h1>Please Follow Given Link.!</h1>";
		}

	} // END DELETE CUSTOMER FUCTION

/**************************************************
*					DELETING =CATEGORY.
**************************************************/ 

	public function category($id=null)
	{
		if ($id) {
			
			$category = new Category_Model();
			$category->load($id);

			if ($category->category_id){

				$category->delete();
				if($this->db->error()['message']){
					check($this->db->error()['message']);
					echo "Can not delete this Record.!";
				}else{
					
					if ($this->input->is_ajax_request()) {
						echo "Successfully Deleted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', $category->category_name.' Deleted Successfully.');
						redirect(site_url('Maintenance/category_list'));
					}
				}

			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
			echo "<h1>Please Follow Given Link.!</h1>";

	} // END DELETE CATEGORY FUNCTION

/**************************************************
*					DELETING =GROUP.
**************************************************/ 

	public function group($id=null)
	{
		if ($id) {
			
			$group = new Group_Model();
			$group->load($id);

			if ($group->group_id){

				
				$group->delete();

				if($this->db->error()['message']){
					check($this->db->error()['message']);
					echo "Can not delete this Record.!";
				}else{
					
					if ($this->input->is_ajax_request()) {
						echo "Successfully Deleted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', $group->group_name.' Deleted Successfully.');
						redirect(site_url('Maintenance/group_list'));
					}
				}

			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
			echo "<h1>Please Follow Given Link.!</h1>";

	} // END DELETE CATEGORY FUCTION



/**************************************************
*					DELETING =MATERIAL.
**************************************************/ 

	public function material($id=null)
	{
		if ($id) {
			
			$material = new Material_Model();
			$material->load($id);

			if ($material->material_id){

				$sql = $this->db->query("SELECT * FROM `job_master` WHERE `print_material` = $material->material_id OR `lam1_material` = $material->material_id OR `lam2_material` = $material->material_id OR `lam3_material` = $material->material_id");

				if( $sql->num_rows() > 0 ){

					echo "Can't Delete.";

				}else{

					$material->delete();
					// if($this->db->error()['message']){
					check($this->db->error()['message']);
					// 	echo "Can not delete this Record.!";
					// }else{

						if ($this->input->is_ajax_request()) {
							echo "Successfully Deleted";
							return;
						}else{
							$this->session->set_flashdata('userMsg', $material->material_name.' Deleted Successfully.');
							redirect(site_url('Maintenance/material_list'));
						}
					// }

				}
				
			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
			echo "<h1>Please Follow Given Link.!</h1>";

	} // END DELETE CATEGORY FUCTION



/**************************************************
*					DELETING =JOB STRUCTURE.
**************************************************/ 

	public function job_structure($id=null)
	{
		if ($id) {
			
			$structure = new Job_Master_Model();
			$structure->load($id);

			if ($structure->id){
				$structure->delete();
				if($this->db->error()['message']){
					check($this->db->error()['message']);
					echo "Can not delete this Record.!";
				}else{
					if ($this->input->is_ajax_request()) {
						echo "Successfully Deleted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', $material->material_name.' Deleted Successfully.');
						redirect(site_url('Maintenance/structure_list'));
					}
				}
			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
			echo "<h1>Please Follow Given Link.!</h1>";

	} // END DELETE CATEGORY FUCTION

	/**************************************************
	 *					DELETING =CATEGORY.
	 **************************************************/

	public function maintenance_request($id=null)
	{
		if ($id) {

			$record = new Maintenance_Model();
			$record->load($id);

			if ($record->id){

				$record->delete();
				if($this->db->error()['message']){
					check($this->db->error()['message']);
					echo "Can not delete this Record.!";
				}else{

					if ($this->input->is_ajax_request()) {
						echo "Successfully Deleted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', ' Deleted Successfully.');
						redirect(site_url('Maintenance/maintenance_list'));
					}
				}

			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
			echo "<h1>Please Follow Given Link.!</h1>";

	} // END DELETE CATEGORY FUNCTION




/**************************************************
*				DELETING =SALE ORDER.
**************************************************/ 

	public function sale_order($id=null)
	{
		if ($id) {
			
			$so = new Sale_Order_Model();
			$so->load($id);

			if ($so->job_code){
				$so->delete();
				echo "Successfully Deleted";
			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
			echo "<h1>Please Follow Given Link.!</h1>";

	} // END DELETE CATEGORY FUCTION


	public function offset_costing($id=null)
	{
		if ($id) {
			
			$result = $this->db->query("DELETE costing, foil FROM offset_costing AS costing
							JOIN offset_costing_foil AS foil ON costing.id = foil.costing_id
							WHERE costing.id = ".$id);
			// echo $this->db->error()['message'];
			// echo "<br>. Affected Rows: ".$result;
			if ($result){
				echo "Successfully Deleted";
			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
			echo "<h1>Please Follow Given Link.!</h1>";

	} // END DELETE CATEGORY FUCTION


/**************************************************
*				DELETING =SLITTING ORDER.
**************************************************/ 

	public function slitting_entry($id=null)
	{
		if ($id) {
			
			$slitting = new Slitting_Model();
			$slitting->load($id);

			if ($slitting->id){
				$slitting->delete();

				if ($this->input->is_ajax_request()) {
					
					if($this->db->error()['message']){
						check($this->db->error()['message']);
						echo "Can not delete this Record.!";
					}else{
						echo "Successfully Deleted";
						return;
					}

				}else{

					if($this->db->error()['message']){
						check($this->db->error()['message']);
						$this->session->set_flashdata('userMsg', ' Can not delete this Record.!.');
					}else{
						$this->session->set_flashdata('userMsg', ' Deleted Successfully.');
					}
					redirect(site_url('Maintenance/rewinding_entry_list'));
				}

			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
			echo "<h1>Please Follow Given Link.!</h1>";

	} // END DELETE CATEGORY FUCTION


/**************************************************
*				DELETING =REWINDING ORDER.
**************************************************/ 

	public function rewinding_entry($id=null)
	{
		if ($id) {
			
			$rewinding = new Rewinding_Model();
			$rewinding->load($id);

			if ($rewinding->id){
				$rewinding->delete();

				if ($this->input->is_ajax_request()) {
					
					if($this->db->error()['message']){
						check($this->db->error()['message']);
						echo "Can not delete this Record.!";
					}else{
						echo "Successfully Deleted";
						return;
					}

				}else{

					if($this->db->error()['message']){
						check($this->db->error()['message']);
						$this->session->set_flashdata('userMsg', ' Can not delete this Record.!.');
					}else{
						$this->session->set_flashdata('userMsg', ' Deleted Successfully.');
					}
					redirect(site_url('Maintenance/rewinding_entry_list'));
				}

			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
			echo "<h1>Please Follow Given Link.!</h1>";

	} // END DELETE CATEGORY FUCTION


	/**************************************************
	*					=PURCHASE ORDER.
	**************************************************/ 
	public function purchase_order($id)
	{
		if (isset($id)) {
			$po = new Purchase_order_Model();
			$po->load($id);

			$item = new PO_Detail_Model();
			$item = $item->getWhere(["po_id"=>$po->po_id]);

			foreach ($item as $i) {
				$delete = new PO_Detail_Model();
				$delete->load($i->pod_id);
				$delete->delete();
			}

			$po->delete();
			echo "Successfully Deleted";
			return;
			

		}// if $id isset

	} // purchase_order()


	/**************************************************
	*					=PRINTING ENTRY.
	**************************************************/ 
	public function printing_entry($id)
	{
		if (isset($id)) {
			$print = new Printing_Model();
			$print->load($id);

			$item = new Printing_Detail_Model();
			$item = $item->getWhere(["printing_id"=>$print->printing_id]);
			
			$this->db->trans_start();
			foreach ($item as $i) {
				$delete = new Printing_Detail_Model();
				$delete->load($i->pd_id);
				$delete->delete();
			}

			$print->delete();
			
			if (!$this->db->trans_status()) {
				$this->db->trans_rollback();
			}else{
				$this->db->trans_complete();
				if ($this->input->is_ajax_request()) {
					echo "Successfully Deleted";
					return;
				}else{
					$this->session->set_flashdata('userMsg', ' Deleted Successfully.');
					redirect(site_url('Maintenance/printing_entry_list'));
				}
			}

		}// if $id isset

	} // purchase_order()


	/**************************************************
	*					=PRINTING ENTRY.
	**************************************************/ 
	public function lamination_entry($id)
	{
		if (isset($id)) {
			$lam = new Lamination_Model();
			$lam->load($id);

			$items = new Lamination_Detail_Model();
			$items = $items->getWhere(["lamination_id"=>$lam->id]);
			
			$this->db->trans_start();
			foreach ($items as $i) {
				$delete = new Lamination_Detail_Model();
				$delete->load($i->ld_id);
				$delete->delete();
			}

			$lam->delete();
			
			if (!$this->db->trans_status()) {
				$this->db->trans_rollback();
			}else{
				$this->db->trans_complete();
				if ($this->input->is_ajax_request()) {
					echo "Successfully Deleted";
					return;
				}else{
					$this->session->set_flashdata('userMsg', ' Deleted Successfully.');
					redirect(site_url('Maintenance/lamination_entry_list'));
				}
			}

		}// if $id isset

	} // lamination_entry()


	/**************************************************
	*					=DISPATCH ENTRY.
	**************************************************/ 
	public function dispatch_entry($id)
	{
		if (isset($id)) {
			$dispatch = new Dispatch_Model();
			$dispatch->load($id);

			$items = new Dispatch_Detail_Model();
			$items = $items->getWhere(["dispatch_id"=>$dispatch->id]);
			
			$this->db->trans_start();
			foreach ($items as $i) {
				$delete = new Dispatch_Detail_Model();
				$delete->load($i->dd_id);
				$delete->delete();
			}

			$dispatch->delete();
			
			if (!$this->db->trans_status()) {
				$this->db->trans_rollback();
				echo "Transiction Could not complete successfully.";
				return;
			}else{
				$this->db->trans_complete();
				if ($this->input->is_ajax_request()) {
					echo "Successfully Deleted";
					return;
				}else{
					$this->session->set_flashdata('userMsg', 'Updated Successfully.');
					redirect(site_url('maintenance/dispatch_entry_list/'));
				}
			}

		}// if $id isset

	} // dispatch_entry()

	


	/**************************************************
	*					=GRN Received Note.
	**************************************************/ 
	public function goods_receiving_note($id)
	{
		if (isset($id)) {
			$grn = new Grn_Model();
			$grn->load($id);

			$item = new Grn_Detail_Model();
			$item = $item->getWhere(["grn_id"=>$grn->grn_id]);

			$this->db->trans_start();
			foreach ($item as $i) {
				$delete = new Grn_Detail_Model();
				$delete->load($i->grnd_id);
				$delete->delete();
			}

			$grn->delete();
			if (!$this->db->trans_status()) {
				$this->db->trans_rollback();
			}else{
				$this->db->trans_complete();
				if ($this->input->is_ajax_request()) {
					echo "Successfully Deleted";
					return;
				}else{
					$this->session->set_flashdata('userMsg', ' Deleted Successfully.');
					redirect(site_url('Maintenance/grn_list'));
				}
			}
			

		}// if $id isset

	} // goods_receiving_note()


	/**************************************************
	*					=GRN Received Note.
	**************************************************/ 
	public function goods_issue_note($id)
	{
		if (isset($id)) {
			$gin = new Gin_Model();
			$gin->load($id);

			$item = new Gin_Detail_Model();
			$item = $item->getWhere(["gin_id"=>$gin->gin_id]);

			$this->db->trans_start();
			foreach ($item as $i) {
				$delete = new Gin_Detail_Model();
				$delete->load($i->gind_id);
				$delete->delete();
			}

			$gin->delete();
			if (!$this->db->trans_status()) {
				$this->db->trans_rollback();
			}else{
				$this->db->trans_complete();
				if ($this->input->is_ajax_request()) {
					echo "Successfully Deleted";
					return;
				}else{
					$this->session->set_flashdata('userMsg', ' Deleted Successfully.');
					redirect(site_url('Maintenance/gin_list'));
				}
			}

		}// if $id isset

	} // goods_issue_note()


	/**************************************************
	*					=QUOTATION.
	**************************************************/ 
	public function quotation($id)
	{
		if (isset($id)) {
			$q = new Quotation_Model();
			$q->load($id);

			$q->delete();
			if ($this->input->is_ajax_request()) {
				echo "Successfully Deleted";
				return;
			}else{
				$this->session->set_flashdata('userMsg', ' Deleted Successfully.');
				redirect(site_url('Maintenance/quotation_list/'.$q->department));
			}
			
		}// if $id isset

	} // quotation()


	

}