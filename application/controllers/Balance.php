<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Balance extends MY_Controller {


	/**************************************************
	*					GET PRINTING.
	**************************************************/

	public function printing($job_code)
	{
		if (isset($job_code)) {
			$sql = "SELECT so.film1_wt AS needed, 
					SUM(pd.plain_wt) printed,
					( so.film1_wt - SUM(pd.plain_wt) ) AS Balance
					FROM `sale_order` AS so
						LEFT JOIN printing AS p
					    	ON so.job_code = p.so
					    JOIN printing_detail AS pd
					    	ON p.printing_id = pd.printing_id
					WHERE so.job_code = {$job_code}";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0)
			{
			   $row = $query->row(); 
			   $toBe = $row->needed;
			   $done = $row->printed ? $row->printed: 0;
			   $balance = $row->Balance? $row->Balance: $row->needed;
				echo "<p>Order Quantity: ".number_format($toBe,2)."</p>";
				echo "<p>Printed Quantity: ".number_format($done,2)."</p>";
				echo "<p>Balance Quantity: ".number_format($balance,2)."</p>";
			}
		}else{
			echo "sorry";
		}
	}

	/**************************************************
	*					GET REWINDING.
	**************************************************/

	public function rewinding($job_code)
	{
		if (isset($job_code)) {
			$sql = "SELECT SUM(pd.printed_wt) printed, SUM(r.weight_after) done,
					( SUM(r.weight_after) - SUM(pd.printed_wt) ) AS Balance
					FROM `printing` AS p
						LEFT JOIN rewinding AS r
					    	ON r.so = p.so
					    JOIN printing_detail AS pd
					    	ON p.printing_id = pd.printing_id
					WHERE p.so = {$job_code}";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0)
			{
			   $row = $query->row(); 
			   $toBe = $row->printed;
			   $done = $row->done ? $row->done: 0;
			   $balance = $row->Balance? $row->Balance: $row->printed;
				echo "<p>Order Quantity: ".number_format($toBe,2)."</p>";
				echo "<p>Printed Quantity: ".number_format($done,2)."</p>";
				echo "<p>Balance Quantity: ".number_format($balance,2)."</p>";
			}
		}else{
			echo "sorry";
		}
	}

	/**************************************************
	*					GET PRINTING.
	**************************************************/

	public function lamination($job_code)
	{
		if (isset($job_code)) {

			$order = new Sale_Order_Model();
			$order->load($job_code);

			$structure = new Job_Master_Model();
			$structure->load($order->structure_id);

			if ($structure->lam1_material) {
				$sql = "SELECT so.film2_wt AS needed, 
						SUM(ld.plain_weight) plain,
						( so.film2_wt - SUM(ld.plain_weight) ) AS Balance
						FROM `sale_order` AS so
							LEFT JOIN lamination AS l
						    	ON so.job_code = l.so
						    JOIN lamination_detail AS ld
						    	ON l.id = ld.lamination_id
						WHERE so.job_code = {$job_code}
						AND l.lamination_number = 1";
				$query = $this->db->query($sql);
				if ($query->num_rows() > 0)
				{

					echo "<h3>First Lamination</h3>";
				    $row = $query->row(); 
				    $toBe = $row->needed;
				    $done = $row->plain ? $row->plain: 0;
				    $balance = $row->Balance? $row->Balance: $row->needed;
					echo "<p>Order: ".number_format($toBe,2)."</p>";
					echo "<p>Done: ".number_format($done,2)."</p>";
					echo "<p>Balance: ".number_format($balance,2)."</p>";
				}
			} // if First lmeterial
			if ($structure->lam2_material) {
				$sql = "SELECT so.film3_wt AS needed, 
						SUM(ld.plain_weight) plain,
						( so.film3_wt - SUM(ld.plain_weight) ) AS Balance
						FROM `sale_order` AS so
							LEFT JOIN lamination AS l
						    	ON so.job_code = l.so
						    JOIN lamination_detail AS ld
						    	ON l.id = ld.lamination_id
						WHERE so.job_code = {$job_code}
						AND l.lamination_number = 2";
				$query = $this->db->query($sql);
				if ($query->num_rows() > 0)
				{

				    $row = $query->row(); 
				    $toBe = $row->needed;
				    $done = $row->plain ? $row->plain: 0;
				    $balance = $row->Balance? $row->Balance: $row->needed;
					echo "<hr>";
					echo "<h3>Second Lamination</h3>";
					echo "<p>Order: ".number_format($toBe,2)."</p>";
					echo "<p>Done: ".number_format($done,2)."</p>";
					echo "<p>Balance: ".number_format($balance,2)."</p>";
				}
			} // if Second lmeterial
			if ($structure->lam3_material) {
				$sql = "SELECT so.film4_wt AS needed, 
						SUM(ld.plain_weight) plain,
						( so.film4_wt - SUM(ld.plain_weight) ) AS Balance
						FROM `sale_order` AS so
							LEFT JOIN lamination AS l
						    	ON so.job_code = l.so
						    JOIN lamination_detail AS ld
						    	ON l.id = ld.lamination_id
						WHERE so.job_code = {$job_code}
						AND l.lamination_number = 3";
				$query = $this->db->query($sql);
				if ($query->num_rows() > 0)
				{

				    $row = $query->row(); 
				    $toBe = $row->needed;
				    $done = $row->plain ? $row->plain: 0;
				    $balance = $row->Balance? $row->Balance: $row->needed;
					echo "<hr>";
					echo "<h3>Second Lamination</h3>";
					echo "<p>Order: ".number_format($toBe,2)."</p>";
					echo "<p>Done: ".number_format($done,2)."</p>";
					echo "<p>Balance: ".number_format($balance,2)."</p>";
				}
			} // if Third lmeterial

		}else{
			echo "sorry";
		}
	}

	/**************************************************
	*					GET SLITTING.
	**************************************************/

	public function slitting($job_code)
	{
		if (isset($job_code)) {
			$sql = "SELECT SUM(pd.printed_wt) printed, SUM(s.weight_after) done,
					( SUM(s.weight_after) - SUM(pd.printed_wt) ) AS Balance
					FROM `printing` AS p
						LEFT JOIN slitting AS s
					    	ON s.so = p.so
					    JOIN printing_detail AS pd
					    	ON p.printing_id = pd.printing_id
					WHERE p.so = {$job_code}";
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0)
			{
			   $row = $query->row(); 
			   $toBe = $row->printed;
			   $done = $row->done ? $row->done: 0;
			   $balance = $row->Balance? $row->Balance: $row->printed;
				echo "<p>Order Quantity: ".number_format($toBe,2)."</p>";
				echo "<p>Printed Quantity: ".number_format($done,2)."</p>";
				echo "<p>Balance Quantity: ".number_format($balance,2)."</p>";
			}
		}else{
			echo "sorry";
		}
	}



	/**************************************************
	*					GET CURRENT STOCK.
	**************************************************/

	public static function current_stock($material_id)
	{
		if (isset($material_id)) {
			$sql = "SELECT
					  m.material_name,
					  IFNULL(SUM(grnd.qty),
					  0) - IFNULL(SUM(gind.qty),
					  0) AS balance
					FROM
					  `grn_detail` AS grnd
					LEFT JOIN
					  gin_detail AS gind ON grnd.material_id = gind.material_id
					JOIN
					  material AS m ON m.material_id = grnd.material_id
					WHERE
					  m.material_id = {$material_id}
					GROUP BY
					  m.material_id";
			$CI =& get_instance();
			$query = $CI->db->query($sql);
			// echo $this->db->last_query();

			if ($query->num_rows() > 0)
			{
			   $row = $query->row(); 
			   $balance = $row->balance;
				echo strtoupper("<p>Available Stock For: ".$row->material_name
				." ".number_format($balance,2)." KGs</p>");
			}else{
				$material = new Material_Model();
				$material -> load($material_id);
				echo "<p>".strtoupper($material->material_name) . " Not In Stock.</p>";
			}
		}else{
			echo "sorry";
		}
	}

	public function structure_balance($structure_id)
	{
		if (isset($structure_id)) {
			$structure = new Job_Master_Model();
			$structure->load($structure_id);

			if ($structure->print_material > 0)
			{
			   self::current_stock($structure->print_material);
			}
			if ($structure->lam1_material > 0)
			{
			   self::current_stock($structure->lam1_material);
			}
			if ($structure->lam2_material > 0)
			{
			   self::current_stock($structure->lam2_material);
			}
			if ($structure->lam3_material > 0)
			{
			   self::current_stock($structure->lam3_material);
			}

		}else{
			echo "sorry";
		}
	}


}