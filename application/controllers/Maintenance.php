<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Maintenance extends MY_Controller {

	public $allowed_roles = array();

	public function index()
	{
		$data['roles'] = $this->allowed_roles;
		$this->load->view('Header',$data);
		$this->load->view('Maintenance/Maintenance_home',$data);
		$this->load->view('Footer',$data);
		// echo "<h1>This is from Maintenance Controller</h1>";
	}

/**************************************************
*					=USER LIST.
**************************************************/ 
	public function user_list()
	{
		$data['title'] = 'User List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$data['roles'] = $this->allowed_roles;
		$data['users'] = $this->User_Model->get();

		if ($this->input->is_ajax_request()) {
			$this->load->view('user/user_list',$data);
		}else{
			$data['site_title'] = 'Software Users List!';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('user/user_list',$data);
			$this->load->view('main/footer',$data);
		}

	}

/**************************************************
*					=CUSTOMER LIST.
**************************************************/ 
	public function customer_list()
	{
		$data['title'] = 'Customer List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;
		$data['customers'] = $this->Customer_Model->get();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/customer_list',$data);
		}else{
			$fb = $this->session->flashdata('userMsg');
			$data['feed_back'] = ($fb)? feed_back($fb):'';
			$data['site_title'] = 'Add New Material';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/customer_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

/**************************************************
*					=SALE ORDER LIST.
**************************************************/ 
	public function sale_order_list()
	{
		$data['title'] = 'Sale Order List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;

		$sql = "CALL sale_order_list()";
		$return = $this->db->query($sql);
		$data['so'] = $return->result();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/sale_order_list',$data);
		}else{
			$fb = $this->session->flashdata('userMsg');
			$data['feed_back'] = ($fb)? feed_back($fb):'';
			$data['site_title'] = 'Add New Material';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/sale_order_list',$data);
			$this->load->view('main/footer',$data);
		}
		
	}

/**************************************************
*					=COMPLETED SALE ORDER LIST.
**************************************************/ 
	public function completed_sale_order_list()
	{
		$data['title'] = 'Sale Order List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;

		$sql = "CALL completed_sale_order_list()";
		$return = $this->db->query($sql);
		$data['so'] = $return->result();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/completed_sale_order_list',$data);
		}else{
			$fb = $this->session->flashdata('userMsg');
			$data['feed_back'] = ($fb)? feed_back($fb):'';
			$data['site_title'] = 'Add New Material';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/completed_sale_order_list',$data);
			$this->load->view('main/footer',$data);
		}
		
	}

/**************************************************
*					=UNCOMPLETED SALE ORDER LIST.
**************************************************/ 
	public function uncompleted_sale_order_list()
	{
		$data['title'] = 'Sale Order List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;

		$sql = "CALL uncompleted_sale_order_list()";
		$return = $this->db->query($sql);
		$data['so'] = $return->result();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/uncompleted_sale_order_list',$data);
		}else{
			$fb = $this->session->flashdata('userMsg');
			$data['feed_back'] = ($fb)? feed_back($fb):'';
			$data['site_title'] = 'Add New Material';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/uncompleted_sale_order_list',$data);
			$this->load->view('main/footer',$data);
		}
		
	}

/**************************************************
*					=SUPPLIER LIST.
**************************************************/ 
	public function supplier_list()
	{
		$data['title'] = 'Supplier List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;
		$data['suplliers'] = $this->Supplier_Model->get();

		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/supplier_list',$data);
		}else{
			$fb = $this->session->flashdata('userMsg');
			$data['feed_back'] = ($fb)? feed_back($fb):'';
			$data['site_title'] = 'Add New Material';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/supplier_list',$data);
			$this->load->view('main/footer',$data);
		}
	}


/**************************************************
*					=CATEGORY LIST.
**************************************************/ 
	public function category_list()
	{
		$data['title'] = 'Category== List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;
		$data['categories'] = $this->Category_Model->get();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/category_list',$data);
		}else{
			$fb = $this->session->flashdata('userMsg');
			$data['feed_back'] = ($fb)? feed_back($fb):'';
			$data['site_title'] = 'Add New Material';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/category_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	public function group_list()
	{
		$data['title'] = 'Group List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;
		$data['groups'] = $this->Group_Model->get();
		// var_dump($data['groups']);
		// $x = $this->db->query("Select * from group")->result();
		// var_dump($x);
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/group_list',$data);
		}else{
			$fb = $this->session->flashdata('userMsg');
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/group_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	public function printing_entry_list()
	{
		$data['title'] = 'Printing Entry List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';
		$data['roles'] = $this->allowed_roles;
		$data['groups'] = $this->Group_Model->get();

		$query = $this->db->query('CALL printing_list')->result();
		$data['printing'] = $query;
		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/printing_list',$data);
		}else{
			$data['site_title'] = 'Add New Material';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/printing_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	public function dispatch_entry_list()
	{
		$data['title'] = 'Dispatch Entry List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';
		$query = $this->db->query('CALL dispatch_list()')->result();
		$data['dispatch'] = $query;
		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/dispatch_list',$data);
		}else{
			$data['site_title'] = 'Add New Rewinding Entry.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/dispatch_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	public function lamination_entry_list()
	{
		$data['title'] = 'Lamination Entry List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$query = $this->db->query('CALL lamination_list()')->result();
		$data['lamination'] = $query;
		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/lamination_list',$data);
		}else{
			$data['site_title'] = 'Add New Rewinding Entry.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/lamination_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	public function rewinding_entry_list()
	{
		$data['title'] = 'Rewinding Entry List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;
		$data['groups'] = $this->Group_Model->get();
		$query = $this->db->query('CALL rewinding_list()')->result();
		$data['rewinding'] = $query;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/rewinding_list',$data);
		}else{
			$data['site_title'] = 'Add New Rewinding Entry.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/rewinding_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	public function slitting_entry_list()
	{
		$data['title'] = 'Slitting Entry List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();

		$data['roles'] = $this->allowed_roles;
		$data['groups'] = $this->Group_Model->get();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$query = $this->db->query('CALL slitting_list()')->result();
		$data['slitting'] = $query;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';
		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/slitting_list',$data);
		}else{
			$data['site_title'] = 'Add New Rewinding Entry.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/slitting_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	public function material_list()
	{
		$data['title'] = 'Material List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['feed_back'] = '';
		$data['roles'] = $this->allowed_roles;

		$materials = $this->Material_Model->get();
		$material_details = [];
		if (!empty($materials)) {
			foreach ($materials as $material) {
				$material_details['material_id'] = html_escape($material->material_id);
				$material_details['material_name'] = html_escape($material->material_name);
				$material_details['size'] = html_escape($material->size);
				$material_details['thickness'] = html_escape($material->thickness);
				$material_details['price'] = html_escape($material->rate);
				$material_details['density'] = html_escape($material->density);

				$category = new Category_Model();
				$category->load($material->category_id);
				$material_details['category'] = html_escape($category->category_name);

				$group = new Group_Model();
				$group->load($material->group_id);
				$material_details['group'] = html_escape($group->group_name);

				$data['materials'][] = $material_details;
			}
		}else{
			$data['materials'] = [];
		}

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/material_list',$data);
		}else{
			$fb = $this->session->flashdata('userMsg');
			$data['feed_back'] = ($fb)? feed_back($fb):'';
			$data['site_title'] = 'Add New Material';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/material_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	public function structure_list()
	{
		$data['title'] = 'Job Structure List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['roles'] = $this->allowed_roles;
		$fb = $this->session->flashdata('userMsg');
		$data['feed_back'] = ($fb)? feed_back($fb):'';

		$job = new Job_Master_Model();
		$data['structures'] = $job->Job_Master_Model->get(1000);

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/structure_list',$data);
		}else{
			$data['site_title'] = 'Add New Job Structure.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/structure_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	// Purchase order list
	public function po_list()
	{
		$data['title'] = 'Purchase Order List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['roles'] = $this->allowed_roles;

		$query = $this->db->query("CALL purchase_order_list()");

		$data['pos'] = $query->result();

		$this->load->view('Maintenance/po_list', $data);
	}


	// goods received note list
	public function grn_list()
	{
		$data['title'] = 'Goods Receiving Note List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['roles'] = $this->allowed_roles;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$grn = new Grn_Model();
		
		$data['grnList'] = $grn->get();

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/grn_list',$data);
		}else{
			$data['site_title'] = 'Add New Job Structure.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/grn_list',$data);
			$this->load->view('main/footer',$data);
		}
	}

	// goods issue note list
	public function gin_list()
	{
		$data['title'] = 'Goods Issue Note List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['roles'] = $this->allowed_roles;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';
		$gin = new Gin_Model();
		$data['ginList'] = $gin->get();

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/gin_list',$data);
		}else{
			$data['site_title'] = 'Add New Job Structure.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/gin_list',$data);
			$this->load->view('main/footer',$data);
		}
	}


	// maintenance rquest list
	public function maintenance_list()
	{
		$data['title'] = 'Maintenance List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['roles'] = $this->allowed_roles;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$query = $this->Maintenance_Model->getWhere(['created_by'=>$this->session->userdata('user_id')]);
		// $query = $this->db->query("SELECT * FROM `maintenance`
		// 							WHERE created_by = ".$this->session->userdata('user_id'));
		$data['maintenance_list'] = $query;

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/maintenance_list',$data);
		}else{
			$data['site_title'] = 'Add New Job Structure.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/maintenance_list',$data);
			$this->load->view('main/footer',$data);
		}
	}


	// maintenance pending rquest list
	public function maintenance_pending_list()
	{
		$data['title'] = 'Pending Request List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['roles'] = $this->allowed_roles;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$data['pending'] = $this->Maintenance_Model->getWhere(['pending'=>1,'verified_requester'=>0]);

		if ($this->input->is_ajax_request()) {
			$this->load->view('Maintenance/maintenance_pending_list',$data);
		}else{
			$data['site_title'] = 'Add New Job Structure.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Maintenance/maintenance_pending_list',$data);
			$this->load->view('main/footer',$data);
		}
	}


	/**************************************************
	*					=QUOTATION LIST.
	**************************************************/ 
	public function quotation_list($department)
	{
		$data['title'] = 'Quotation List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$data['roles'] = $this->allowed_roles;
		$data['quotations'] = $this->Quotation_Model->getWhere(['department'=>$department]);
		$data['form_type'] = $department;

		if ($this->input->is_ajax_request()) {
			$this->load->view('Marketing/quotation_list',$data);
		}else{
			$data['site_title'] = 'Add New Job Structure.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Marketing/quotation_list',$data);
			$this->load->view('main/footer',$data);
		}
	}


	/**************************************************
	*					=OFFSET COSTING LIST.
	**************************************************/ 
	public function offset_costing_list()
	{
		$data['title'] = 'Costing List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$data['roles'] = $this->allowed_roles;
		$data['records'] = $this->db->query("SELECT costing.*, c.customer_name 
												FROM offset_costing AS costing
												JOIN customer AS c ON c.customer_id = costing.customer_id
												ORDER BY id DESC")->result();

		if ($this->input->is_ajax_request()) {
			$this->load->view('Offset/costing_list',$data);
		}else{
			$data['site_title'] = 'Add New Job Structure.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Offset/costing_list',$data);
			$this->load->view('main/footer',$data);
		}
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */