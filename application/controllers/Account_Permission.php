<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_Permission extends MY_Controller
{

    public function index()
    {
        $this->load->model(['Roles2_Model', 'Permission_Model']);
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');

        $data['roles'] = $this->Roles2_Model->get(1000,0,false);
        $permission = $this->Permission_Model->get(1000,0,false);

        $department = $this->db->query("SELECT DISTINCT department from permissions")->result();

        $col = [];
        foreach ($permission as $perm) {
            if (!array_key_exists($perm->department, $col)) {
                $col[$perm->department] = array();
            }
            if (!array_key_exists($perm->group_name, $col[$perm->department])) {
                $col[$perm->department][$perm->group_name] = array();
            }
        }
        foreach ($permission as $perm) {
            $col[$perm->department][$perm->group_name][] = $perm;
        }
        $data['permissions'] = $col;
        $data['site_title'] = "Roles Group";
        $data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

        $data['title'] = 'Account Permission';

        if ($this->input->is_ajax_request()) {
            $this->load->view('user/account_permission',$data);
        }else{
            $this->load->view('main/header', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('user/account_permission', $data);
            $this->load->view('main/footer', $data);
        }

    }

    public function save()
    {
        $data['message'] = '';
        if (isset($_POST['role_id'])) {

            $this->load->model(['Role_Permission_Model']);

            $sql = "DELETE FROM role_permission WHERE role_id = " . $this->input->post('role_id');
            if ($this->db->simple_query($sql)) {
                foreach ($this->input->post('permissions') as $perm) {
                    $rp = new Role_Permission_Model();
                    $rp->role_id = $this->input->post('role_id');
                    $rp->perm_id = $perm;
                    $rp->save(1);
                }
                $this->session->set_flashdata('userMsg', 'Setting saved successfully.');
                redirect(site_url('account_permission/'));
            } else {
                echo "failed!";

            }

        } else {
            $data ['message'] = 'Please Choose the form in order to manage role';
            // $this->load->view('error_view', $data);
            echo $data["message"];
        }
    }

    public function insert()
    {
        $data['message'] = '';
        if (isset($_POST['role_name'])) {

            $this->load->model(['Role_Permission_Model', 'Roles2_Model']);
            // $this->load->halper(['date']);
            $this->load->helper('date');

            $role = new Roles2_Model();
            $role->role_name = $this->input->post('role_name');
            $role->description = $this->input->post('description');
            $role->added_by = $this->session->userdata('user_id');
            $role->added_time = mysql_to_unix(time());
            $role->save();

            // check($this->db->_error_message());
            // check($this->db->last_query());
            // return;

            foreach ($this->input->post('permissions') as $perm) {
                $rp = new Role_Permission_Model();
                $rp->role_id = $role->id;
                $rp->perm_id = $perm;
                $rp->save();
            }

            $this->session->set_flashdata('userMsg', 'Setting saved successfully.');
            redirect(site_url('account_permission/'));

        } else {
            $data ['message'] = 'Page not found do you want to add '. anchor(base_url('account_permission/'),'new permission group?');
//            $this->load->view('error_view', $data);
            echo $data["message"];

        }
    }

    public function delete($id)
    {
        if(!User_Model::hasAccess('deleteAccountPermission')){
            die("You dont have privilege in order to take this action");
        }
        $data['message'] = '';
        if (isset($id)) {

            $this->load->model(['Role_Permission_Model','Roles2_Model']);

            $sql = "DELETE FROM role_permission WHERE role_id = " . $id;
            if ($this->db->simple_query($sql)) {
                if ($this->db->simple_query('DELETE FROM roles2 WHERE id = ' . $id)) {
                    $this->session->set_flashdata('userMsg', 'Permission Group deleted successfully.!');
                    redirect(site_url('account_permission/'));
                }

            } else {
                echo "failed!";
            }

        } else {
            $data ['message'] = 'Please Choose the form in order to manage role';
            $this->load->view('error_view', $data);
        }
    }


}
