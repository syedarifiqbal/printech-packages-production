<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile_Image extends MY_Controller
{

	public $allowed_roles = array();

	# Methodes

	function __construct()
	{
		parent::__construct();
		$this->icons = array('.jpg' => false,
			'.jped' => false,
			'.png' => false,
			'.xls' => 'ExcelFileIcon.png',
			'.xlsx' => 'ExcelFileIcon.png',
			'.doc' => 'WordFileIcon.png',
			'.docx' => 'WordFileIcon.png',
			'.pdf' => 'pdficon.png'
		);

		$this->load->library('imagehelper');

		$config = array();
		$config['upload_path'] = './assets/img/profile_img/';
		$config['allowed_types'] = 'psd|ai|gif|jpg|png|xlsx|xls|pdf|doc|docx';
		$config['max_size'] = '25600'; // 25 MB
//        $config['max_width']  = '59';
//        $config['max_height']  = '59';

		$this->load->library('upload', $config);
	}

	public function index()
	{
		$data['site_title'] = 'Profile Image Uploader.';

		$this->load->view('main/header', $data);
		$this->load->view('main/navigation', $data);
		$this->load->view('main/rightNavigation', $data);
		$this->load->view('main/topbar', $data);
		$this->load->view('upload_profile_image', $data);
		$this->load->view('main/footer', $data);
	}

	public function upload()
	{
		$this->load->helper(array('form', 'url'));

		if (!$this->upload->do_upload('file')) {
			var_dump($_FILES);
			// $error = array('error' => $this->upload->display_errors());
			check($this->upload->display_errors());
		} else {
			$data = $this->upload->data();

			$config['image_library']  = 'gd2';
			$config['source_image']   = './assets/img/profile_img/'.$data['raw_name'] . $data['file_ext'];
			$config['create_thumb']   = TRUE;
			$config['maintain_ratio'] = TRUE;
			$config['width']          = 59;
			$config['height']         = 59;

			$this->load->library('image_lib', $config);

			$this->image_lib->resize();

			$this->load->model('User_Model');
			$user = new User_Model();
			$user->load($this->session->userdata('user_id'));
			$user->avater_path = $data['raw_name'] .'_thumb' . $data['file_ext'];

			$this->session->set_userdata('avater_path',$data['raw_name'] .'_thumb' . $data['file_ext']);

			if ($user->save()) {
				redirect(base_url());
			} else {
				echo $this->upload->display_errors();
			}

		}
	}


}