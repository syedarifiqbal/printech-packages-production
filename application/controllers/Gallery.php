<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Gallery extends MY_Controller
{
    public $allowed_roles = array();
    private $gallery_path = './uploads/gallery/';
    private $gellary_upload_error = array();

    function __construct()
    {
        parent::__construct();
        $this->load->model(array(
                'Gallery_model',
                'Gallery_images_model',
                'Off_Job_Structure_Model',
                'Job_Master_Model'
            ));
        $this->set_data('active_menu', 'property');
        $this->set_data('class_name', strtolower(get_class($this)));
        $this->context = 'property';
    }

    function index($context, $context_id)
    {
        $this->set_data('context_id', $context_id);
        $this->set_data('context', $context);

        $this->set_data('title', 'Job Structure Gallery.!');
        $this->set_data('session', $this->session->all_userdata());
        $this->set_data('roles', $this->allowed_roles);

        $this->set_data( 'records', $this->Gallery_model->getWhere(['context'=> $context, 'context_id'=> $context_id, 'active'=>1]) );
        $this->set_data( 'inactive_records', $this->Gallery_model->getWhere(['context'=> $context, 'context_id'=> $context_id, 'active'=>0]) );
        
        $job = $this->get_job_by_context($context);
        $job->load($context_id);
        $this->set_data('job', $job);

        $this->set_data('site_title', 'Add New Job Structure.');
        $fb = $this->session->flashdata('userMsg');
        $this->set_data('feed_back', ($fb)? feed_back($fb):'');

        $this->load->view('main/header',$this->get_data());
        $this->load->view('main/navigation',$this->get_data());
        $this->load->view('main/rightNavigation',$this->get_data());
        $this->load->view('main/topbar',$this->get_data());
        $this->load->view('gallery/lists',$this->get_data());
        $this->load->view('main/footer',$this->get_data());
    }

    function offset($context, $context_id)
    {
        $this->set_data('context_id', $context_id);
        $this->set_data('context', $context);

        $this->set_data('title', 'Job Gallery.!');
        $this->set_data('session', $this->session->all_userdata());
        $this->set_data('roles', $this->allowed_roles);

        $this->set_data( 'records', $this->Gallery_model->getWhere(['context'=> $context, 'context_id'=> $context_id, 'active'=>1]) );
        
        $job = $this->get_job_by_context($context);
        $job->load($context_id);
        $this->set_data('job', $job);

        $this->set_data('page_title', "Gallery List <small>(".ucwords($job->job_name).')</small>');

        $this->load->view('offset/offset/gallery/list',$this->get_data());
    }

    function save($context, $context_id, $gallery_id=0)
    {
        $this->context = $context;

        $this->set_data('context_id', $context_id);
        $this->set_data('context', $context);
        $this->context = $context;
        $this->load->library('form_validation');

        $record = new Gallery_model();

        if ($gallery_id) { $record->load($gallery_id); }
        
        $job = $this->get_job_by_context($context);
        $job->load($context_id);
        $this->set_data('job', $job);

        $this->set_data('record', $record);
        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('data[name]','Gallery name','required');
            if ( $this->form_validation->run() == TRUE ) {
                foreach ($this->input->post('data') as $key => $value) {
                    $record->{$key} = $value;
                }
                $record->context_id = $context_id;
                $record->context = $this->context;
                $record->active = $record->id? $record->active: 1;
                $record->{$gallery_id?'updated_by':'added_by'} = $this->session->userdata('user_id');

                $file_datas = $this->multiple_upload();

                if ($this->is_uploaded($file_datas)) {
                    if ($gal_id = $record->save()) {
                        $this->insert_images($file_datas, $gal_id);
                        if ($this->session->userdata('section') == 'offset') {
                            set_flash_message(SUCCESS, $id?'Record updated successfully.':'Record added successfully.');
                            redirect( site_url( "gallery/offset/$context/$context_id" ) );
                        }else{
                            $this->session->set_flashdata('userMsg', "Gallery Deleted Successfully");
                            redirect( site_url( "gallery/index/$context/$context_id" ) );
                        }
                    }
                }else{
                    $this->delete_images($file_datas);
                    $this->session->set_flashdata('userMsg', '<p>'. join('</p><p>', $this->gellary_upload_error) .'</p>');
                }
            }
        }

        if ($this->session->userdata('section') == 'offset') {
            $this->load->view('offset/offset/gallery/form',$this->get_data());
        }else{
            $fb = $this->session->flashdata('userMsg');
            $this->set_data('feed_back', ($fb)? feed_back($fb):'');
            $this->set_data('site_title', "Add New Gallery for ($job->job_name)");
            $this->load->view('main/header',$this->get_data());
            $this->load->view('main/navigation',$this->get_data());
            $this->load->view('main/rightNavigation',$this->get_data());
            $this->load->view('main/topbar',$this->get_data());
            $this->load->view('gallery/form',$this->get_data());
            $this->load->view('main/footer',$this->get_data());
        }
    }

    function edit($gallery_id)
    {
        $this->load->library('form_validation');

        $record = new Gallery_model();
        $record->load($gallery_id);
        $this->context = $record->context;
        $this->set_data('context', $record->context);
        $this->set_data('context_id', $record->context_id);
        $this->set_data('record', $record);
        
        $job = $this->get_job_by_context($record->context);
        $job->load($record->context_id);
        $this->set_data('job', $job);

        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('data[name]','Gallery name','required');
            if ( $this->form_validation->run() == TRUE ) {
                foreach ($this->input->post('data') as $key => $value) {
                    $record->{$key} = $value;
                }
                $record->updated_by = $this->session->userdata('user_id');
                if ($gal_id = $record->save()) {
                    $this->session->set_flashdata('userMsg', "Gallery Updated Successfully");
                    redirect( site_url( "gallery/index/$record->context/$record->context_id" ) );
                }else{
                    $this->session->set_flashdata('userMsg', "No Changes Made!");
                }
            }
        }
        
        $fb = $this->session->flashdata('userMsg');
        $this->set_data('feed_back', ($fb)? feed_back($fb):'');
        $this->set_data('site_title', "Add New Gallery for ($job->job_name)");
        $this->load->view('main/header',$this->get_data());
        $this->load->view('main/navigation',$this->get_data());
        $this->load->view('main/rightNavigation',$this->get_data());
        $this->load->view('main/topbar',$this->get_data());
        $this->load->view('gallery/form',$this->get_data());
        $this->load->view('main/footer',$this->get_data());
    }

    function append_gallery($gallery_id)
    {
        $this->load->library('form_validation');

        $record = new Gallery_model();
        $record->load($gallery_id);
        $this->set_data('record', $record);
        $this->context = $record->context;
        $this->set_data('context', $record->context);
        $this->set_data('context_id', $record->context_id);
        $images = $this->Gallery_images_model->getWhere(array('gallery_id'=>$gallery_id));
        $this->set_data('images', $images);

        $images = $this->Gallery_images_model->getWhere(array('gallery_id'=>$gallery_id));
        $this->set_data('images', $images);
        $job = $this->get_job_by_context($record->context);
        $job->load($record->context_id);
        $this->set_data('job', $job);

        if (isset($_POST['submit'])) {
            $file_datas = $this->multiple_upload();

            if ($this->is_uploaded($file_datas)) {
                $this->insert_images($file_datas, $record->id);
                $this->session->set_flashdata('userMsg', "Images Added Successfully");
                redirect( site_url( "gallery/index/$record->context/$record->context_id" ) );
            }else{
                $this->delete_images($file_datas);
                $this->session->set_flashdata('userMsg', '<p>'. join('</p><p>', $this->gellary_upload_error) .'</p>');
            }
        }

        $fb = $this->session->flashdata('userMsg');
        $this->set_data('feed_back', ($fb)? feed_back($fb):'');
        $this->set_data('site_title', "Add New Gallery for ($job->job_name)");
        $this->load->view('main/header',$this->get_data());
        $this->load->view('main/navigation',$this->get_data());
        $this->load->view('main/rightNavigation',$this->get_data());
        $this->load->view('main/topbar',$this->get_data());
        $this->load->view('gallery/append_form',$this->get_data());
        $this->load->view('main/footer',$this->get_data());
    }

    function gallery_description($gallery_id)
    {
        $this->load->library('form_validation');

        $record = new Gallery_model();
        $record->load($gallery_id);
        $this->set_data('record', $record);
        $this->set_data('context', $record->context);
        $this->set_data('context_id', $record->context_id);

        $images = $this->Gallery_images_model->getWhere(array('gallery_id'=>$gallery_id));
        $this->set_data('images', $images);
        $this->set_data('context_id', $record->context_id);

        $job = $this->get_job_by_context($record->context);
        $job->load($record->context_id);
        $this->set_data('job', $job);

        if (isset($_POST['submit'])) {
            foreach ($this->input->post('data') as $id => $data) {
                $image = new Gallery_images_model();
                $image->load($id);
                $image->title = $data['title'];
                $image->description = $data['description'];
                $image->save();
            }
            $this->session->set_flashdata('userMsg', "Gallery Images Description Updated Successfully");
            redirect( site_url( "gallery/index/$record->context/$record->context_id" ) );
        }

        $fb = $this->session->flashdata('userMsg');
        $this->set_data('feed_back', ($fb)? feed_back($fb):'');
        $this->set_data('site_title', "Add New Gallery for ($job->job_name)");
        $this->load->view('main/header',$this->get_data());
        $this->load->view('main/navigation',$this->get_data());
        $this->load->view('main/rightNavigation',$this->get_data());
        $this->load->view('main/topbar',$this->get_data());
        $this->load->view('gallery/description_form',$this->get_data());
        $this->load->view('main/footer',$this->get_data());
    }

    function gallery_slider($gallery_id)
    {
        $gallery = new Gallery_model();
        $gallery->load($gallery_id);
        $this->set_data('record', $gallery);
        $images = $this->Gallery_images_model->getWhere(array('gallery_id'=>$gallery_id));
        $this->set_data('images', $images);
        $this->load->view('gallery/gallery_view', $this->get_data());
    }

    function delete_gallery($gallery_id)
    {
        $gallery = new Gallery_model();
        $gallery->load($gallery_id);
        
        $context_id = $gallery->context_id;
        $context = $gallery->context;

        $images = $this->Gallery_images_model->getWhere(array('gallery_id'=>$gallery_id));

        foreach ($images as $image) {
            $path = $this->gallery_path.$image->image;
            // x($path);
            if ( file_exists($path) ) {
                unlink($path);
            }
            $image->delete();
        }

        $gallery->delete();

        $this->session->set_flashdata('userMsg', "Gallery Deleted Successfully");
        redirect( site_url( "gallery/index/$context/$context_id" ) );
    }

    function get_job_by_context($context)
    {
        $job = null;
        switch ($context) {
            case 'gravure_structure':
                $job = new Job_Master_Model();
                break;
            
            default:
                $job = new Off_Job_Structure_Model();
                break;
        }
        return $job;
    }

    function is_uploaded($data)
    {
        $uploaded = true;
        foreach ($data as $file) {
            if (!$file) {
                $uploaded = false;
                break;
            }
        }
        return $uploaded;
    }

    function insert_images($data, $gallery_id)
    {
        $this->load->model('Gallery_images_model');
        foreach ($data as $file) {
            $record = new Gallery_images_model();
            $record->gallery_id = $gallery_id;
            $record->image = $file;
            $record->save();
        }
    }

    function delete_gallery_image()
    {
        if (isset($_POST)) {
            $image = new Gallery_images_model();
            $image->load($_POST['image_id']);
            $path = $this->gallery_path.$image->image;
            if (file_exists($path)) {
                unlink($path);
            }
            $image->delete();
            echo json_encode(array('status'=>true));
        }
    }

    function delete_images($data)
    {
        foreach ($data as $file) {
            $path = $this->gallery_path.$file;
            if (file_exists($path) && $file) {
                unlink($path);
            }
        }
    }
    
    public function multiple_upload()
    {
        $number_of_files_uploaded = count($_FILES['upl_files']['name']);
        $files = array();
        // Faking upload calls to $_FILE
        for ($i = 0; $i < $number_of_files_uploaded; $i++) :
            $_FILES['userfile']['name']     = $_FILES['upl_files']['name'][$i];
            $_FILES['userfile']['type']     = $_FILES['upl_files']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['upl_files']['tmp_name'][$i];
            $_FILES['userfile']['error']    = $_FILES['upl_files']['error'][$i];
            $_FILES['userfile']['size']     = $_FILES['upl_files']['size'][$i];
            $config = array(
                // 'file_name'     => '',
                'allowed_types' => 'jpg|jpeg|png|gif',
                // 'max_size'      => 3000,
                'overwrite'     => FALSE,
                'upload_path' => $this->gallery_path
            );
            $this->load->library('upload', $config);
            // $this->upload->initialize($config);
            if ( ! $this->upload->do_upload() ) :
                $files[] = false;
                $error = array('error' => $this->upload->display_errors());
                $this->gellary_upload_error[] = $error['error'];
              else :
                $data = $this->upload->data();
                $files[] = $data['file_name'];
            endif;
        endfor;
        return $files;
    }

}