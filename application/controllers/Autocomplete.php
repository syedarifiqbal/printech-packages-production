<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Autocomplete extends MY_Controller
{

    public $allowed_roles = array();

    # Methodes

    public function __construct()
    {
        parent::__construct();
        $this->load->model(['Customer_Model',
            'Supplier_Model',
            'Category_Model',
            'Group_Model',
            'Material_Model']);
    }

    public function index()
    {
        // $this->load->view('welcome_message');
        echo "Hello Job Order.!";
        check($this->allowed_roles);
    }

    /**************************************************
     *                    GET MATERIAL.
     **************************************************/

    public function material()
    {
        $material = new Material_Model();
        $term = $_POST['term'];
        // $term['q'] = $_POST['q'];

        // echo "Q Data ".$q;
        // echo json_encode($term);

        echo json_encode($material->get_material_autocomplete($term));
    }

    /**************************************************
     *                    GET SUPPLIER.
     **************************************************/

    public function supplier()
    {
        $supplier = new Supplier_Model();
        $term = $_POST['term'];
        // $term['q'] = $_POST['q'];

        // echo "Q Data ".$q;
        // echo json_encode($term);

        echo json_encode($supplier->get_supplier_autocomplete($term));
    }

    /**************************************************
     *                GET Pending Purchase Order.
     **************************************************/

    public function pending_po()
    {
        $po = new Purchase_Order_Model();
        $term = $_POST['term'];
        $supplier = $_POST['supplier'];

        // echo "Q Data ".$q;
        // echo json_encode($term);

        echo json_encode($po->get_pending_po_autocomplete($term));
    }

    public function findMaterial()
    {
        if (isset($_GET['term'])) {
            $term = $_GET['term'];
            $material = $this->Material_Model->getLike($term, "material_name");
            $js = [];
            echo "<ul>";
            // for ($i=0; $i < 100; $i++) {
            foreach ($material as $m) {

                echo '<li>';

                echo '<label class="radio-label">';
                echo '<input type="radio" value="' . $m->material_id . '" name="selected">';
                echo '<div class="radio-button"></div>';
                echo $m->material_name;
                echo '</label>';

                echo '</li>';
                // $js['id']=$out;
            }
            // }
            echo "</ul>";
            // echo json_encode($js);
        } else {
            echo "Please Find type Material Name";
        }
    }

    public function findHardner()
    {
        if (isset($_GET['term'])) {
            $term = $_GET['term'];
            $material = $this->Material_Model->getLike($term, "material_name", ['category_id' => 2]);
            $js = [];
            echo "<ul>";
            // for ($i=0; $i < 100; $i++) {
            foreach ($material as $m) {

                echo '<li>';

                echo '<label class="radio-label">';
                echo '<input type="radio" value="' . $m->material_id . '" name="selected">';
                echo '<div class="radio-button"></div>';
                echo $m->material_name;
                echo '</label>';

                echo '</li>';
                // $js['id']=$out;
            }
            // }
            echo "</ul>";
            // echo json_encode($js);
        } else {
            echo "Please Find type Material Name";
        }
    }

    public function findEthyl()
    {
        if (isset($_GET['term'])) {
            $term = $_GET['term'];
            $material = $this->Material_Model->getLike($term, "material_name", ['category_id' => 4]);
            $js = [];
            echo "<ul>";
            // for ($i=0; $i < 100; $i++) {
            foreach ($material as $m) {

                echo '<li>';

                echo '<label class="radio-label">';
                echo '<input type="radio" value="' . $m->material_id . '" name="selected">';
                echo '<div class="radio-button"></div>';
                echo $m->material_name;
                echo '</label>';

                echo '</li>';
                // $js['id']=$out;
            }
            // }
            echo "</ul>";
            // echo json_encode($js);
        } else {
            echo "Please Find type Material Name";
        }
    }

    public function findGlue()
    {
        if (isset($_GET['term'])) {
            $term = $_GET['term'];
            $material = $this->Material_Model->getLike($term, "material_name", ['category_id' => 1]);
            $js = [];
            echo "<ul>";
            // for ($i=0; $i < 100; $i++) {
            foreach ($material as $m) {

                echo '<li>';

                echo '<label class="radio-label">';
                echo '<input type="radio" value="' . $m->material_id . '" name="selected">';
                echo '<div class="radio-button"></div>';
                echo $m->material_name;
                echo '</label>';

                echo '</li>';
                // $js['id']=$out;
            }
            // }
            echo "</ul>";
            // echo json_encode($js);
        } else {
            echo "Please Find type Material Name";
        }
    }


    public function findCustomer()
    {
        if (isset($_GET['term'])) {
            $term = $_GET['term'];
            $material = $this->Customer_Model->getLike($term, "customer_name");
            $js = [];
            echo "<ul>";
            // for ($i=0; $i < 100; $i++) {
            foreach ($material as $m) {

                echo '<li>';

                echo '<label class="radio-label">';
                echo '<input type="radio" value="' . $m->customer_id . '" name="selected">';
                echo '<div class="radio-button"></div>';
                echo $m->customer_name;
                echo '</label>';

                echo '</li>';
                // $js['id']=$out;
            }
            // }
            echo "</ul>";
            // echo json_encode($js);
        } else {
            echo "Please Find type Material Name";
        }
    }


    public function findJob()
    {
        if (isset($_GET['term'])) {
            $term = $_GET['term'];
            $sql = "SELECT so.job_code, so.date, st.job_name
						FROM `sale_order` AS so
						LEFT JOIN job_master AS st
							ON so.structure_id = st.id
						WHERE so.completed = 0 
						AND (st.job_name LIKE '%{$term}%'
                        OR so.job_code LIKE '%{$term}%')
					LIMIT 25";
            $material = $this->db->query($sql)->result();
            echo "<ul>";
            foreach ($material as $m) {

                echo '<li>';

                echo '<label class="radio-label">';
                echo '<input type="radio" value="' . $m->job_code . '" name="selected">';
                echo '<div class="radio-button"></div>';
                echo "( " . $m->job_code . " ) " . $m->job_name;
                echo '</label>';

                echo '</li>';
            }
            echo "</ul>";
        } else {
            echo "findJob Is to Change";
        }
    }

    public function findStructure()
    {
        if (isset($_GET['term'])) {
            $term = $_GET['term'];
            $material = $this->Job_Master_Model->getLike($term, "job_name");
            $js = [];
            echo "<ul>";
            // for ($i=0; $i < 100; $i++) {
            foreach ($material as $m) {

                echo '<li>';

                echo '<label class="radio-label">';
                echo '<input type="radio" value="' . $m->id . '" name="selected">';
                echo '<div class="radio-button"></div>';
                echo $m->job_name;
                echo '</label>';

                echo '</li>';
                // $js['id']=$out;
            }
            // }
            echo "</ul>";
            // echo json_encode($js);
        } else {
            echo "Please Find type Material Name";
        }
    }

    public function findOffsetStructure()
    {
        if (isset($_GET['term'])) {
            $term = $_GET['term'];
            $this->load->model('Off_Job_Structure_Model');
            $material = $this->Off_Job_Structure_Model->getLike($term, "job_name");
            echo "<ul>";
            foreach ($material as $m) {

                echo '<li>';

                echo '<label class="radio-label">';
                echo '<input type="radio" value="' . $m->id . '" name="selected">';
                echo '<div class="radio-button"></div>';
                echo $m->job_name;
                echo '</label>';

                echo '</li>';
            }
            echo "</ul>";
        } else {
            echo "Please Find type Material Name";
        }
    }


}