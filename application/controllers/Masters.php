<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Masters extends MY_Controller {

	protected $roles = array(
		'jobMasterFile',
		'updateJobMasterFile',
		'deleteJobMasterFile',
		'jobOrder',
		'updateJobOrder',
		'deleteJobOrder',
		'addCustomer',
		'updateCustomer',
		'deleteCustomer'
	);

	public $allowed_roles = array();

	# Methodes 

	public function __construct()
	{
		parent::__construct();
		$this->load->model(['Customer_Model']);
	}
	
	public function index()
	{
		// $this->load->view('welcome_message');
		echo "Hello Job Order.!";
		check($this->allowed_roles);
	}

	public function updateCustomer($id)
	{
		if (isset($id)) {
			// isset($_POST['submit']) ? var_dump($_POST):null;

			$mobile = isset($_POST['mobile']) ? count($_POST['mobile']):0;
			$address = isset($_POST['address']) ? count($_POST['address']):0;

			$count = ( $mobile > $address ) ? $mobile: $address;

			$customer = new Customer_Model();
			$customer->customer_id=$this->Customer_Model->max();
			$customer->customer_name=$this->input->post('customer_name');
			$customer->customer_code=$this->Customer_Model->max_where('customer_code');
			$customer->active=1;
			$customer->toured=0;
			$customer->customer=1;
			$customer->client_password = '$2y$12$qT3jQi1JqRb7duft8B3re.qkmN0nAxpta4hSJ7ZCehZJAeT8uxwu2';

			if ($customer->save()) {
				$this->session->set_flashdata('userMsg', 'New Customer Inserted.');
				redirect(base_url()."index.php/Maintenance/customer_list");
			};


			for ($i=0; $i < $count; $i++) {

			}

			echo $count;

			$data['roles'] = $this->allowed_roles;
			$this->load->view('Header',$data);
			$this->load->view('Maintenance/addCustomer',$data);
			$this->load->view('Footer',$data);
		}
	}

}