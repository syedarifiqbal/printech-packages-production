<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function customers($section='offset')
    {
        if ( !isset($_GET['term']) ){ echo json_encode([]); }
        else
        {
            $term = $_GET['term'];
            
            if($section == 'offset'){
                $this->load->model('offset/Offset_customer_model');
                $datas = $this->Offset_customer_model->getLike($term, 'customer_name');
            }else{
                $this->load->model('Customer_Model');
                $datas = $this->Customer_Model->getLike($term, 'customer_name');
            }

            $ret_data = $this->getFormattedData($datas,'customer_name', 'customer_id' );

            $this->output
                ->set_content_type('application/json')
                    ->set_output(json_encode($ret_data));
        }
    }
    
    public function structures()
    {
        if ( !isset($_GET['term']) ){ echo json_encode([]); }
        else
        {
            $term = $_GET['term'];
            $this->load->model('Off_Job_Structure_Model');

            $datas = $this->Off_Job_Structure_Model->getLike($term, 'job_name');

            $ret_data = $this->getFormattedData($datas,'job_name');

            $this->output
                ->set_content_type('application/json')
                    ->set_output(json_encode($ret_data));
        }
    }
    
    public function sale_orders($section='offset')
    {
        if ( !isset($_GET['term']) ){ echo json_encode([]); }
        else
        {
            $term = $_GET['term'];
            
            if($section == 'offset'){
                $this->load->model('offset/Offset_sale_order_model');

                $sql = "SELECT so.job_code, st.job_name, so.po_num
                    FROM offset_sale_order AS so
                    JOIN off_job_structure AS st ON st.id = so.structure_id
                    WHERE st.job_name LIKE '%{$_GET['term']}%'
                    LIMIT 50
                ";
                $datas = $this->db->query($sql)->result();
            }else{
                $this->load->model('Customer_Model');
                $datas = $this->Customer_Model->getLike($term, 'customer_name');
            }

            $ret_data = $this->getFormattedData($datas,'job_name', 'job_code', 'po_num');

            $this->output
                ->set_content_type('application/json')
                    ->set_output(json_encode($ret_data));
        }
    }

    private function getFormattedData($datas, $defaultName='name', $defaultId='id', $preview='')
    {
        $dataArray = [];
        foreach ($datas as $data) {
            $x = [];
            $x['id'] = $data->{$defaultId};
            if($preview)
                $x['text'] = "(". $data->{$preview} . ") ".$data->{$defaultName};
            else
                $x['text'] = $data->{$defaultName};
            $dataArray[] = $x;
        }
        return $dataArray;
    }

}

/* End of file Api.php */
