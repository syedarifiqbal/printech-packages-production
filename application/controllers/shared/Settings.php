<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Controller {
    
    public function toggle_maintenance_model()
    {
        $old_setting = $this->Settings_model->getWhere(['name'=>'maintenance'], true);
        $setting = new Settings_model();
        $setting->load($old_setting->id);
        $setting->value = !$setting->value;
        $setting->save();
        redirect(site_url());
    }

}

/* End of file Api.php */
