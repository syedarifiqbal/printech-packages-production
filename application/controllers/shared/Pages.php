<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
    
    public function maintenance()
    {
        $setting = $this->Settings_model->getWhere(['name'=>'maintenance'], true);
        if( !$setting->value )
            redirect( site_url() );
        else
            $this->load->view('shared/maintenance_mode');
    }

}

/* End of file Api.php */
