<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Approval extends MY_Controller
{


    /**************************************************
     *                    Sale Order.
     **************************************************/

    public function sale_order()
    {

        if (isset($_POST['job_code'])) {

            $so = new Sale_Order_Model();
            $so->load($_POST['job_code']);

            $so->description = $this->input->post('description');
            $so->approved = $this->session->userdata('user_id');

            if ($so->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Job Approved";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'Job Approved.');
                    redirect(site_url('approval/sale_order/'));
                }
            };
        } // IF POST IS SET

        $data['title'] = 'Sale Order List | Edit | Delete.!';
        $data['session'] = $this->session->all_userdata();

        $sql = "SELECT so.job_code, so.date, so.po_date, so.delivery_date, st.job_name, cs.customer_name, so.remarks, so.hold
				FROM sale_order AS so
				LEFT JOIN job_master AS st
					ON so.structure_id = st.id
				LEFT JOIN customer AS cs
					ON so.customer_id = cs.customer_id
				WHERE so.approved = 0
				ORDER BY so.job_code DESC
				LIMIT 1000";
        $return = $this->db->query($sql);
        $data['so'] = $return->result();
        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('approval/approval_sale_order_list', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('approval/approval_sale_order_list', $data);
            $this->load->view('main/footer', $data);
        }

    }

    public function complete_job($id){

        if(isset($id)){

            $sale = new Sale_Order_Model();
            $sale->load($id);
            check($sale);
            if($sale){
                if($sale->completed == 1){
                    $this->session->set_flashdata('userMsg', 'This Order is already completed.');
                }
                $sale->completed = 1;
                if($sale->save()){
                    $this->session->set_flashdata('userMsg', 'Order shifted in completed list successfully.');
                }
                redirect(site_url("maintenance/uncompleted_sale_order_list/"));
            }

        }else{
            $this->session->set_flashdata('userMsg', 'No Record Found.');
            redirect(base_url());
        }

    }



}