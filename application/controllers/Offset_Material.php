<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Offset_Material extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Offset_Material_Model','Offset_Category_Model','Offset_Group_Model'));

    }

    /**************************************************
     *                    ADD Material.
     **************************************************/

    public function add($id = false)
    {
        $data['units'] = array('PKT','PCS','KG');
        $data['availableCategories'] = $this->Offset_Category_Model->get();
        $data['availableGroups'] = $this->Offset_Group_Model->get();

        if ($id) {
            $data['update'] = true;
            $data['categories'] = $this->Offset_Material_Model->GetCategoryIdByMaterialId($id);
            $data['groups'] = $this->Offset_Material_Model->GetGroupIdByMaterialId($id);
            $material = new Offset_Material_Model();
            $material->load($id);
            $data['material'] = $material;
            $material->updated_by = $this->session->userdata('user_id');
            $material->updated_time = dbTime();

        } else {
            $data['update'] = false;
            $data['categories'] = array();
            $data['groups'] = array();
            $material = new Offset_Material_Model();
            $material->created_by = $this->session->userdata('user_id');
            $material->created_time = dbTime();
        }

        if (isset($_POST['submit'])) {

            $material->name	  		= $this->input->post('material_name');
//            $material->length  		= $this->input->post('length');
            $material->unit	        = $this->input->post('unit');
            $material->width 	    = $this->input->post('width');
            $material->height    	= $this->input->post('height');
            $material->gsm 	        = $this->input->post('gsm');
            $material->group_id     = $this->input->post('group_id');
            $material->category_id  = 0;
            $material->rate         = $this->input->post('price');
            if ($material->save()) {

                $sql = "DELETE FROM offset_material_categories WHERE material_id = " . $material->id;
                if ($this->db->simple_query($sql)) {
                    $this->load->model('Offset_Material_Categories_Model');
                    foreach ($this->input->post('categories') as $gro) {
                        $c = new Offset_Material_Categories_Model();
                        $c->category_id = $gro;
                        $c->material_id = $material->id;
                        $c->save();
                    }
                }
                $sql = "DELETE FROM offset_material_groups WHERE material_id = " . $material->id;
                if ($this->db->simple_query($sql)) {
                    $this->load->model('Offset_Material_Groups_Model');
                    foreach ($this->input->post('groups') as $gro) {
                        $g = new Offset_Material_Groups_Model();
                        $g->group_id = $gro;
                        $g->material_id = $material->id;
                        $g->save();
                    }
                }
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                }else{
                    $this->session->set_flashdata('userMsg', 'Updated Successfully.');
					redirect(site_url('offset_material/show_all'));
                }

            }else{
                echo $this->db->last_query() . '</br>';
            }
        }

        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
        if ($this->input->is_ajax_request()) {
            $this->load->view('offset/add_material', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('offset/add_material', $data);
            $this->load->view('main/footer', $data);
        }
    }


    /**************************************************
     *					=SALE ORDER LIST.
     **************************************************/
    public function show_all()
    {
//        echo 'this method is called';
        $data['title'] = 'Sale Order List | Edit | Delete.!';
        $data['session'] = $this->session->all_userdata();

        $data['materials'] = $this->Offset_Material_Model->listWithCategory();
        $data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';
        if ($this->input->is_ajax_request()) {
            $this->load->view('offset/material_list',$data);
        }else{
            $fb = $this->session->flashdata('userMsg');
            $data['feed_back'] = ($fb)? feed_back($fb):'';
            $data['site_title'] = 'Material List';
            $this->load->view('main/header',$data);
            $this->load->view('main/navigation',$data);
            $this->load->view('main/rightNavigation',$data);
            $this->load->view('main/topbar',$data);
            $this->load->view('offset/material_list',$data);
            $this->load->view('main/footer',$data);
        }

    }

    /**************************************************
     *				DELETING =SALE ORDER.
     **************************************************/

    public function delete($id=null)
    {
        if ($id) {
            $this->load->model('Offset_Material_Model');
            $material = new Offset_Material_Model();
            $material->load($id);

            if ($material && $material->id){

                $material->delete();

                if ($this->input->is_ajax_request()) {
                    echo "Successfully Deleted";
                    return;
                }else{
                    $this->session->set_flashdata('userMsg', 'Successfully Deleted.');
                    redirect(site_url('offset_material/show_all'));
                }

            }else{
                echo "<h1>Sorry No Record Found.!</h1>";
            }

        } // END IF ID PRAMETER IS SET
        else
            echo "<h1>Please Follow Given Link.!</h1>";

    } // END DELETE CATEGORY FUCTION

    /**************************************************
     *					=sale order.
     **************************************************/

    public function so_print($job_code)
    {

        if(User_Model::hasAccess('viewOffsetSaleOrder')){

            if (isset($job_code)) {

                $material = new Material_Model();
                $this->load->model('Offset_Sale_Order_Model');
                $job = $this->Offset_Sale_Order_Model->get_order_by_job($job_code);
//                 var_dump($job);

                $pdf = new fpdf("P","mm","A4");
                $pdf->SetMargins(15,10);

                $pdf->AddPage();
                $pdf->SetFillColor(233,234,237);
                $pdf->SetTitle("Sale Order # ".$job_code);
                $pdf->Image(base_url('assets/img/logo.jpg'),15,10,30,25);
                // $pdf->ln(20);
                $pdf->SetFont('Arial','U',18);
                $pdf->Cell(0,4,"",0,1);
                $pdf->Cell(0,10,""."SALE ORDER (Offset)",0,0,'C');
                $pdf->SetFont('Arial','',8);
                $pdf->Cell(0,10,""."PTP.FM.PR.008",0,1,'R');
                $pdf->SetFont('Arial','I',10);
//                $pdf->Cell(0,10,""."Offset",0,1,'C');

                $pdf->ln(28);

                $colors = array();
                $spotColors = array();
                if($job->c)
                    $colors[] = 'Cyan';
                if($job->m)
                    $colors[] = 'Magenta';
                if($job->y)
                    $colors[] = 'Yellow';
                if($job->k)
                    $colors[] = 'Black';
                if($job->spot1)
                    $spotColors[] = $job->spot1;
                if($job->spot2)
                    $spotColors[] = $job->spot2;
                if($job->spot3)
                    $spotColors[] = $job->spot3;

//                check($job);
                $datas = [
                    "Date:" 		=> pkDate($job->date),
                    "Delivery Date:"=> pkDate($job->delivery_date),
                    "Job #:" 		=> $job->job_code,
                    "PO #:" 		=> $job->po_num,
                    "Party:" 		=> $job->customer_name,
                    "Item:" 		=> $job->job_name,
                    "Material:" 	=> $job->material_id,
                    "Structure:" 	=> strtoupper($job->sheet_type).', ('.$job->board_width.'" x '.$job->board_height.'") '.$job->gsm.'gsm',
                    "Seet to be Print:" => number_format(($job->no_ups>0)?$job->quantity/$job->no_ups:0),
                    "Order Quantity:" 	=> number_format($job->quantity,2),
                    "Rate:" 		=> number_format($job->rate,2) . " PKR",
                    "Colors:" 	    => implode(', ',$colors),
                    "Spot Colors:" 	=> implode(', ',$spotColors),
                ];

                $height = 15;
                foreach ($datas as $key => $value) {
                    if (empty(str_replace(" ", "", $key)) && empty($value)) {
                        $height += 15;
                        continue;
                    }
                    $pdf->SetFont('Arial','B',10);
                    $pdf->cell(45,12,strtoupper($key),0,0);
                    $pdf->SetFont('Arial','',9);
                    $pdf->cell(60,12,$value,0,1);
                }

                if ($job->description) {
                    $pdf->setXY(125,55);
                    // $pdf->cell(75,8,'PRINTING',1,1,"C");
                    $pdf->cell(75,8,'Instructions',1,1,"C");
                    $pdf->setXY(125,63);
                    $pdf->MultiCell(75, 7, $job->description, $border=1);
                }

                $pdf->setXY(15,275);
                $pdf->SetAutoPageBreak(false, $margin=0);
                $pdf->SetFont('Arial','B',12);
                $pdf->cell(100,10,'Checked By: _______________________');
                $pdf->cell(30,10,'Approved By: _______________________');
                $pdf->Ln();
                // $pdf->setX(110);
                $pdf->SetFont('courier','U',8);
                $pdf->cell(30,10,'Prepare By: '.($job->created_by)." (".created_time($job->created_time).")");

                $fileName = 'sale_order_'.$job->job_name;
                (isset($_GET['download']) && $_GET['download']=='true' ) ?$pdf->Output($fileName.'.pdf','D'):$pdf->Output();

            } // if id isset

        }else{ // if roll exist
            echo "Your are not allowed for this view";
        }

    } // sale_order()


    public function job_card($job_code)
    {
        if(!isset($job_code)){
            echo 'Please follow the link';
            return;
        }
        if (User_Model::hasAccess('viewOffsetJobCard')) {
            $this->load->model('Offset_Sale_Order_Model');

            $data = $this->Offset_Sale_Order_Model->get_job_card_data($job_code);
            $this->load->library('PDF_Offset_Job_Card');
            ob_start();
            $pdf = new PDF_Offset_Job_Card();
            $pdf->data = $data;
            $pdf->AddPage('L');
            $pdf->SetTitle('Job Card For Job # '.$job_code);

            $pdf->DisplayTable();


            $pdf->Output();

        }else{
            echo "Your are not allowed for this view";
        }
    }

}