<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Update extends MY_Controller
{

    public $allowed_roles = array();

    # Methodes

    /**************************************************
     *                =REGISTRATION USER.
     **************************************************/
    public function register_user($id)
    {
        if (isset($id)) {
            $data['title'] = 'REGISTRATION || REGISTER NEW USER.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $this->db->trans_start();
                $user = new User_Model();
                $user->load($id);
                // $user_id = $user->user_id;
                $user->user_name = $this->input->post('user_name');
                // $user->hashed_password = password_hash($this->input->post('hashed_password'), PASSWORD_BCRYPT, ['cost'=>12]);
                // $user->plain_password = $this->input->post('hashed_password');
                // $user->avater_path = $this->input->post('avater_path');
                $user->cell = $this->input->post('cell');
                $active = ($this->input->post('active') != null) ? $this->input->post('active') : 0;
                $user->role_id = $this->input->post('role_id');
                $user->active = $active;
                // $user->address = $this->input->post('address');

                $user->added_by = $this->session->userdata('');
                $user->added_time = '';
                $user->updated_by = $this->session->userdata('user_id');
                $user->updated_time = dbTime();;
                if ($user->save()) {

                    if (!$this->db->trans_status()) {
                        $this->db->trans_rollback();
                        echo "Transiction Could not complete successfully.";
                    } else {
                        $this->db->trans_complete();
                        if ($this->input->is_ajax_request()) {
                            echo "Successfully Inserted";
                            return;
                        } else {
                            $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                            redirect(site_url('Update/register_user/' . $user->user_id));
                        }
                    }

                }

            } // if post is set
            $data['update'] = true;
            $user = new User_Model();
            $user->load($id);
            $data['user'] = $user;

            $this->load->model('Roles2_Model');
            $data['roles'] = $this->Roles2_Model->get(500,0,false);
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            if ($this->input->is_ajax_request()) {
                $this->load->view('user/register', $data);
            } else {
                $data['site_title'] = 'Add New Customer';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('user/register', $data);
                $this->load->view('main/footer', $data);
            }

        } // if isset $id

    } // register_user()


    /**************************************************
     *                    UPDATE CUSTOMER.
     **************************************************/

    public function customer($id = null)
    {
        if ($id) {
            if (isset($_POST['customer_name'])) {
                // isset($_POST['submit']) ? var_dump($_POST):null;

                $mobile = isset($_POST['mobile']) ? count($_POST['mobile']) : 0;
                $address = isset($_POST['address']) ? count($_POST['address']) : 0;
                $count = ($mobile > $address) ? $mobile : $address;
                $customer = new Customer_Model();
                $customer->load($id);
                $customer->customer_name = $this->input->post('customer_name');
                $customer->active = $this->input->post('active');

                if ($customer->save()) {

                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('update/customer/' . $customer->customer_id));
                    }

                }

            } // END IF POST IS SET

            $customer = new Customer_Model();
            $customer->load($id);
            $data['customer'] = $customer;
            $data['roles'] = $this->allowed_roles;
            $data['update'] = true;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            if ($this->input->is_ajax_request()) {
                $this->load->view('Maintenance/addCustomer', $data);
            } else {
                $data['site_title'] = 'Add New Customer';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('Maintenance/addCustomer', $data);
                $this->load->view('main/footer', $data);
            }

        } else { // END IF ID PRAMETER IS SET
            echo "<h1>Please Follow Given Link.!</h1>";
        }

    } // END UPDATE CUSTOMER FUCTION

    /**************************************************
     *                    UPDATE SUPPLIER.
     **************************************************/

    public function supplier($id = null)
    {
        if ($id) {
            if (isset($_POST['supplier_name'])) {

                $mobile = isset($_POST['mobile']) ? count($_POST['mobile']) : 0;
                $address = isset($_POST['address']) ? count($_POST['address']) : 0;

                $count = ($mobile > $address) ? $mobile : $address;

                $supplier = new Supplier_Model();
                $supplier->load($id);

                $supplier->supplier_name = $this->input->post('supplier_name');
                $supplier->active = $this->input->post('active');
                // $supplier->toured=0;
                // $supplier->supplier=1;
                // $supplier->client_password = '$2y$12$qT3jQi1JqRb7duft8B3re.qkmN0nAxpta4hSJ7ZCehZJAeT8uxwu2';

                if ($supplier->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('update/supplier/' . $supplier->supplier_id));
                    }
                }

            } // END IF POST IS SET

            $supplier = new Supplier_Model();
            $supplier->load($id);
            $data['supplier'] = $supplier;
            $data['roles'] = $this->allowed_roles;
            $data['update'] = true;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            if ($this->input->is_ajax_request()) {
                $this->load->view('Maintenance/addsupplier', $data);
            } else {
                $data['site_title'] = 'Add New Supplier';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('Maintenance/addsupplier', $data);
                $this->load->view('main/footer', $data);
            }
        } // END IF ID PRAMETER IS SET
        else {
            echo "<h1>Please Follow Given Link.!</h1>";
        }

    } // END UPDATE CUSTOMER FUCTION

    /**************************************************
     *                    UPDATE =CATEGORY.
     **************************************************/
    public function category($id = null)
    {
        if ($id) {
            $data['title'] = 'Category Update Form.!';
            $data['session'] = $this->session->all_userdata();

            if ($_POST) {
                $category = new category_Model();
                $category->load($id);
                // $category->category_id = $this->Category_Model->max();
                $category->category_name = $this->input->post('category_name');
                $category->description = $this->input->post('description');
                $category->density = $this->input->post('density');
                // $category->created_by = $this->session->userdata('user_id');
                // $category->created_time = dbTime();
                $category->updated_by = $this->session->userdata('user_id');
                $category->updated_time = dbTime();
                if ($category->save()) {

                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('update/category/' . $category->category_id));
                    }

                }
            }

            $c = new category_Model();
            $c->load($id);

            $data['cate'] = $c;

            $data['roles'] = $this->allowed_roles;
            $data['update'] = true;

            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            if ($this->input->is_ajax_request()) {
                $this->load->view('Maintenance/addCategory', $data);
            } else {
                $data['site_title'] = 'Add New Supplier';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('Maintenance/addCategory', $data);
                $this->load->view('main/footer', $data);
            }
        } // END IF ID PRAMETER IS SET
        else {
            echo "<h1>Please Follow Given Link.!</h1>";
        }

    } // END OF CATEGORY UPDATE FUNCTION

    /**************************************************
     *                    UPDATE =GROUP.
     **************************************************/
    public function group($id = null)
    {
        if ($id) {
            $data['title'] = 'Group Update Form.!';
            $data['session'] = $this->session->all_userdata();

            if ($_POST) {
                $group = new Group_Model();
                $group->load($id);
                // $group->group_id = $this->group_Model->max();
                $group->group_name = $this->input->post('group_name');
                $group->description = $this->input->post('description');
                // $group->created_by = $this->session->userdata('user_id');
                // $group->created_time = dbTime();
                $group->updated_by = $this->session->userdata('user_id');
                $group->updated_time = dbTime();
                if ($group->save()) {

                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('update/group/' . $group->group_id));
                    }

                }
            }

            $c = new Group_Model();
            $c->load($id);

            $data['group'] = $c;

            $data['roles'] = $this->allowed_roles;
            $data['update'] = true;

            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            if ($this->input->is_ajax_request()) {
                $this->load->view('Maintenance/addGroup', $data);
            } else {
                $data['site_title'] = 'Add New Group';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('Maintenance/addGroup', $data);
                $this->load->view('main/footer', $data);
            }

        } // END IF ID PRAMETER IS SET
        else {
            echo "<h1>Please Follow Given Link.!</h1>";
        }

    } // END OF GROUP UPDATE FUNCTION


    /**************************************************
     *                    UPDATE MATERIAL.
     **************************************************/

    public function material($id)
    {
        if (isset($id)) {

            if (isset($_POST['material_name'])) {

                $material = new Material_Model();
                $material->load($id);
                // $material->material_id=$this->Material_Model->max();
                $material->material_name = $this->input->post('material_name');
                $material->category_id = $this->input->post('category');
                $material->group_id = $this->input->post('group');
                $material->unit = $this->input->post('unit');
                $material->size = $this->input->post('size');
                $material->thickness = $this->input->post('micron');
                $material->rate = $this->input->post('price');
                $material->density = $this->input->post('density');
                $material->updated_by = $this->session->userdata('user_id');
                $material->updated_time = dbTime();
                if ($material->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('update/material/' . $material->material_id));
                    }
                };

            } // if post isset

            $categories = $this->Category_Model->get();
            foreach ($categories as $category) {
                $data['categories'][$category->category_id] = $category->category_name;
            }

            $groups = $this->Group_Model->get();
            foreach ($groups as $group) {
                $data['groups'][$group->group_id] = $group->group_name;
            }

            $material = new Material_Model();
            $material->load($id);
            $data['material'] = $material;
            $data['update'] = true;

            $data['units'] = ['KG' => 'KG', 'PCS' => 'PCS', 'MTR' => 'MTR', 'FT' => 'FT'];
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            $data['roles'] = $this->allowed_roles;

            if ($this->input->is_ajax_request()) {
                $this->load->view('Maintenance/addMaterial', $data);
            } else {
                $data['site_title'] = 'Update Material';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('Maintenance/addMaterial', $data);
                $this->load->view('main/footer', $data);
            }

        }// end if id is not set

    } // material update


    /**************************************************
     *                    =UPDATE JOB STRUCTURE.
     **************************************************/

    public function job_structure($id)
    {
        if ($id) {

            if (isset($_POST['job_name'])) {

                // var_dump($_POST);
                // return;

                $structure = new Job_Master_Model();
                $structure->load($id);
                $structure->file_number =$this->input->post('file_number');
                $structure->job_name = $this->input->post('job_name');
                $structure->print_type = $this->input->post('print_type');
                $structure->print_as_per = $this->input->post('print_as_per');
                $structure->cylinder = $this->input->post('cylinder');
                $structure->cylinder_len = $this->input->post('cylinder_len');
                $structure->cylinder_circum = $this->input->post('cylinder_circum');
                $structure->no_ups = $this->input->post('no_ups');
                $structure->no_repeat = $this->input->post('no_repeat');
                $structure->no_lamination = $this->input->post('no_lamination');
                $structure->print_material = $this->input->post('print_material');
                $structure->lam1_material = $this->input->post('lam1_material');
                $structure->lam2_material = $this->input->post('lam2_material');
                $structure->lam3_material = $this->input->post('lam3_material');
                $structure->slitting_reel_width = $this->input->post('slitting_reel_width');
                $structure->slitting_cutting_size = $this->input->post('slitting_cutting_size');
                $structure->slitting_direction = $this->input->post('slitting_direction');
                $structure->ink_series = $this->input->post('ink_series');
                $structure->no_of_inks = $this->input->post('no_of_inks');
                $structure->bm_qty = $this->input->post('bm_qty');
                $structure->bm_width = $this->input->post('bm_width');
                $structure->bm_height = $this->input->post('bm_height');
                $structure->bm_types = $this->input->post('bm_type');
                $structure->coa_requird = $this->input->post('coa_requird');
                $structure->dc = $this->input->post('dc');
                $structure->hold = $this->input->post('hold');
                $structure->remarks = $this->input->post('remarks');
                $structure->reason = $this->input->post('reason');
                // $structure->create_by 	  		 = $this->session->userdata('user_id');
                // $structure->created_time  		 = dbTime();
                $structure->update_by = $this->session->userdata('user_id');
                $structure->updated_time = dbTime();

                if ($structure->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/job_structure/' . $structure->job_code));
                    }
                };
            }
            $data['update'] = true;
            $str = new Job_Master_Model();
            $str->load($id);
            // var_dump($str);
            $print_material = new Material_Model();
            $print_material->load($str->print_material);
            $lam1_material = new Material_Model();
            $lam1_material->load($str->lam1_material);
            $lam2_material = new Material_Model();
            $lam2_material->load($str->lam2_material);
            $lam3_material = new Material_Model();
            $lam3_material->load($str->lam3_material);

            $data['structure'] = $str;
            $data['print_material_name'] = $print_material->material_name;
            $data['lam1_material_name'] = $lam1_material->material_name;
            $data['lam2_material_name'] = $lam2_material->material_name;
            $data['lam3_material_name'] = $lam3_material->material_name;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            $data['roles'] = $this->allowed_roles;

            if ($this->input->is_ajax_request()) {
                $this->load->view('transictions/job_structure2', $data);
            } else {
                $data['site_title'] = 'Update Material';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('transictions/job_structure2', $data);
                $this->load->view('main/footer', $data);
            }
        } // if isset $id
    }


    /**************************************************
     *                    ADD SALE ORDER.
     **************************************************/

    public function sale_order($id)
    {
        if ($id) {

            $data['job_code'] = $id;
            if (isset($_POST['structure_id'])) {

                $meter = 0;
                $weight = [];
                $quantity = $this->input->post('quantity');


                $structure = new Job_Master_Model();
                $structure->load($this->input->post('structure_id'));

                $films = ['print_material', 'lam1_material', 'lam2_material', 'lam3_material'];

                if ($this->input->post('order_type') == 'pcs') {
                    $meter = $structure->bm_height * $quantity / ($structure->no_ups > 0 ? $structure->no_ups : 1) / 1000;

                    $i = 0;
                    $add = 0;
                    foreach ($films as $film) {
                        // $add = ( $structure->{$film} ) ? 20: 0;

                        $material = new Material_Model();
                        $material->load($structure->{$film});
                        $weight[] = $meter / 1000000 * $material->size * $material->thickness * $material->density + $add;

                    }

                }// if order in pcs
                elseif ($this->input->post('order_type') == 'kg') {
                    $gramage = [];

                    foreach ($films as $film) {
                        $material = new Material_Model();
                        $material->load($structure->{$film});
                        $gramage[] = $material->density * $material->thickness;
                    }

                    foreach ($gramage as $g) {
                        $weight[] = $g / array_sum($gramage) * $quantity;
                    }

                    $material = new Material_Model();
                    $material->load($structure->print_material);
                    $meter = $weight[0] / $material->density / $material->thickness / $material->size * 1000000;

                }

                $so = new Sale_Order_Model();
                $so->load($id);

                $so->job_code = $data['job_code'];
                $so->date = dbDate($this->input->post('date'));
                $so->po_num = $this->input->post('po_num');
                $so->po_date = dbDate($this->input->post('po_date'));
                $so->delivery_date = dbDate($this->input->post('delivery_date'));
                $so->structure_id = $this->input->post('structure_id');
                $so->customer_id = $this->input->post('customer_id');
                $so->order_type = $this->input->post('order_type');
                $so->quantity = $this->input->post('quantity');
                $so->rate = $this->input->post('rate');
                $so->meter = $meter;
                $so->film1_wt = $weight[0];
                $so->film2_wt = $weight[1];
                $so->film3_wt = $weight[2];
                $so->film4_wt = $weight[3];
                $so->remarks = $this->input->post('remarks');
                $so->deliveries     = serialize($this->input->post('deliveries'));
                $so->hold = $this->input->post('hold') ? 1 : 0;;
                $so->completed = $this->input->post('completed') ? 1 : 0;
                // $so->created_by 	= $this->session->userdata('user_id');
                // $so->created_time  	= dbTime();
                $so->updated_by = $this->session->userdata('user_id');
                $so->updated_time = dbTime();

                if ($so->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/sale_order/' . $so->job_code));
                    }
                }
            } // IF POST IS SET

            $data['update'] = true;
            $sql = "SELECT so.*, st.job_name, cs.customer_name
						FROM sale_order AS so
						LEFT JOIN job_master AS st
							ON so.structure_id = st.id
						LEFT JOIN customer AS cs
							ON so.customer_id = cs.customer_id
						WHERE so.job_code = {$id}";

            $return = $this->db->query($sql);
            $data['so'] = array_shift($return->result());
            $data['deliveries'] = unserialize( $data['so']->deliveries );

            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            $data['roles'] = $this->allowed_roles;

            if ($this->input->is_ajax_request()) {
                $this->load->view('transictions/sale_order', $data);
            } else {
                $data['site_title'] = 'Update Sale Order';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('transictions/sale_order', $data);
                $this->load->view('main/footer', $data);
            }

        } // if id isset()

    } // UPDATE SALE ORDER


    /**************************************************
     *                    =PRINTING ENTRY.
     **************************************************/
    public function printing_entry($id)
    {
        if ($id) {
            $data['title'] = 'Printing Entry || Update Printing Data.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $print = new Printing_Model();
                $print->load($id);
                // $print->printing_id = $this->Printing_Model->max();
                $print->so = $this->input->post('so');
                $print->date = dbDate($this->input->post('date'));
                $print->machine = $this->input->post('machine');
                // $print->operator = $this->input->post('operator');
                // $print->assistant = $this->input->post('assistant');
                $print->helper = $this->input->post('helper');
                $print->production_meter = $this->input->post('production_meter');
                $print->plain_wastage = $this->input->post('plain_wastage');
                $print->printed_wastage = $this->input->post('printed_wastage');
                $print->setting_wastage = $this->input->post('setting_wastage');
                $print->matching_wastage = $this->input->post('matching_wastage');
                $print->qc_hold = $this->input->post('qc_hold');
                $print->start_time = $this->input->post('machine_start_time');
                $print->stop_time = $this->input->post('machine_stop_time');
                // $print->created_by = $this->session->userdata('');
                $print->created_time = dbTime();
                $print->updated_by = $this->input->post('user_id');
                // $print->updated_time = dbTime();
                if ($print->save()) {

                    $item = new Printing_Detail_Model();
                    $item = $item->getWhere(["printing_id" => $print->printing_id]);

                    foreach ($item as $i) {
                        $delete = new Printing_Detail_Model();
                        $delete->load($i->pd_id);
                        $delete->delete();
                    }

                    // var_dump($_POST);
                    for ($i = 0; $i < count($this->input->post('plain_wt')); $i++) {
                        $item = new Printing_Detail_Model();
                        $item->pd_id = $this->Printing_Detail_Model->max();
                        $item->printing_id = $print->printing_id;
                        $item->plain_wt = $this->input->post('plain_wt')[$i];
                        $item->printed_wt = $this->input->post('printed_wt')[$i];
                        $item->meter = $this->input->post('meter')[$i];
                        $item->start_time = $this->input->post('start_time')[$i];
                        $item->end_time = $this->input->post('end_time')[$i];
                        $item->remarks = $this->input->post('remarks')[$i];
                        $item->save();
                    }

                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/printing_entry/' . $print->printing_id));
                    }

                };
            }
            $data['update'] = true;
            $sql = "SELECT p.*, st.job_name
					FROM printing p 
					LEFT JOIN sale_order so
						ON p.so = so.job_code
					LEFT JOIN job_master st
						ON so.structure_id = st.id
					WHERE p.printing_id = {$id}";

            $return = $this->db->query($sql);
            $data['print'] = $return->row();
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            $detail = new Printing_Detail_Model();
            $detail = $detail->getWhere(['printing_id' => $id]);
            $data['print_detail'] = $detail;
            $data['roles'] = $this->allowed_roles;

            if ($this->input->is_ajax_request()) {
                $this->load->view('transictions/printing', $data);
            } else {
                $data['site_title'] = 'Update Printing Entry';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('transictions/printing', $data);
                $this->load->view('main/footer', $data);
            }
        }// if id isset()

    } // printing_entry()


    /**************************************************
     *                    =DISPATCH ENTRY.
     **************************************************/
    public function dispatch_entry($id)
    {
        if ($id) {
            $data['title'] = 'Dispatch Entry || Update Dispatch Data.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                // var_dump($_POST);
                // $this->db->trans_start();
                $dispatch = new Dispatch_Model();
                $dispatch->load($id);
                // $dispatch->id = $this->Dispatch_Model->max();
                $dispatch->date = dbDate($this->input->post('date'));
                $dispatch->so = $this->input->post('so');
                $dispatch->challan = $this->input->post('challan');
                $dispatch->gross_weight = $this->input->post('gross_weight');
                $dispatch->tare_weight = $this->input->post('tare_weight');
                $dispatch->net_weight = $this->input->post('net_weight');
                $dispatch->destination = $this->input->post('destination');
                $dispatch->receiver = $this->input->post('receiver');
                $dispatch->no_carton = $this->input->post('no_carton');
                $dispatch->no_pcs = $this->input->post('no_pcs');
                $dispatch->no_roll = $this->input->post('no_roll');
                $dispatch->meter = $this->input->post('meter');
                $dispatch->remarks = $this->input->post('remarks');
                $dispatch->driver_name = $this->input->post('driver_name');
                $dispatch->vehicle = $this->input->post('vehicle');
                // $dispatch->created_by = $this->session->userdata('user_id');
                // $dispatch->created_time = dbTime();
                $dispatch->updated_by = $this->input->post('user_id');
                $dispatch->updated_time = dbTime();

                if ($dispatch->save()) {

                    if (net_on()) {

                        $this->load->library('email');

                        $this->email->from('arif.iqbal@printechpackages.com');
                        $this->email->to('hdarif2@gmail.com, arif.iqbal@printechpackages.com, arifiqbal@outlook.com');
                        // $this->email->cc('another@another-example.com');
                        // $this->email->bcc('them@their-example.com');

                        $this->email->subject('Job Dispatch to Customer!');
                        $this->email->message(dispatch_email_body('2345', 'punch pan masala', 'syed arif iqbal'));
                        $this->email->send();

                    }

                    $item = new Dispatch_Detail_Model();
                    $item = $item->getWhere(["dispatch_id" => $dispatch->id]);
                    if (isset($item) || !empty($item)) {
                        foreach ($item as $i) {
                            $delete = new Dispatch_Detail_Model();
                            $delete->load($i->dd_id);
                            $delete->delete();
                        }
                    }

                    if ($this->input->post('fl_weight')) {
                        for ($i = 0; $i < count($this->input->post('fl_weight')); $i++) {
                            $item = new Dispatch_Detail_Model();
                            $item->dd_id = $this->Dispatch_Detail_Model->max();
                            $item->dispatch_id = $dispatch->id;
                            $item->weight = $this->input->post('fl_weight')[$i];
                            $item->no_carton = $this->input->post('fl_no_carton')[$i];
                            $item->no_pcs = $this->input->post('fl_no_pcs')[$i];
                            $item->no_roll = $this->input->post('fl_no_roll')[$i];
                            $item->description = $this->input->post('description')[$i];
                            $item->save();
                        }
                    } // if flavour isset
                    // if (!$this->db->trans_status()) {
                    // 	$this->db->trans_rollback();
                    // 	echo "Transiction Could not complete successfully.";
                    // 	return;
                    // }else{
                    // 	$this->db->trans_complete();

                    if (net_on()) {

                        $this->load->library('email');

                        $this->email->from('arif.iqbal@printechpackages.com');
                        $this->email->to('hdarif2@gmail.com', 'Gmail');
                        // $this->email->cc('another@another-example.com');
                        // $this->email->bcc('them@their-example.com');

                        $this->email->subject('Job Dispatch to Customer!');
                        $this->email->message(dispatch_email_body('2345', 'punch pan masala', 'syed arif iqbal'));
                        $this->email->send();

                    }


                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/dispatch_entry/' . $dispatch->id));
                    }
                    // }
                } // if dispatch update

            } // if post isset

            $data['update'] = true;
            $sql = "SELECT d.*, st.job_name
					FROM dispatch d 
					LEFT JOIN sale_order so
						ON d.so = so.job_code
					LEFT JOIN job_master st
						ON so.structure_id = st.id
					WHERE d.id = {$id}";

            $return = $this->db->query($sql);
            $data['dispatch'] = $return->row();

            $detail = new Dispatch_Detail_Model();
            $detail = $detail->getWhere(['dispatch_id' => $id]);
            $data['dispatch_detail'] = $detail;

            $data['roles'] = $this->allowed_roles;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            if ($this->input->is_ajax_request()) {
                $this->load->view('transictions/dispatch', $data);
            } else {
                $data['site_title'] = 'Update Printing Entry';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('transictions/dispatch', $data);
                $this->load->view('main/footer', $data);
            }

        }// if id isset()

    } // dispatch_entry()


    /**************************************************
     *                    =REWINDING ENTRY.
     **************************************************/
    public function rewinding_entry($id)
    {
        if (isset($id)) {

            $data['title'] = 'Rewinding Entry || Update Rewinding Data.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $rewind = new Rewinding_Model();
                $rewind->load($id);
                // $rewind->id = $this->Rewinding_Model->max();
                $rewind->date = dbDate($this->input->post('date'));
                $rewind->so = $this->input->post('so');
                $rewind->operator = $this->input->post('operator');
                $rewind->start_time = $this->input->post('start_time');
                $rewind->end_time = $this->input->post('end_time');
                $rewind->weight_before = $this->input->post('weight_before');
                $rewind->weight_after = $this->input->post('weight_after');
                $rewind->wastage = $this->input->post('wastage');
                $rewind->trim = $this->input->post('trim');
                $rewind->direction = $this->input->post('direction');
                $rewind->qc_hold = $this->input->post('qc_hold');
                $rewind->remarks = $this->input->post('remarks');
                // $rewind->created_by = $this->session->userdata('user_id');
                // $rewind->created_time = $this->input->post('')
                $rewind->updated_by = $this->input->post('user_id');
                $rewind->updated_time = dbTime();;
                // if (true) {
                if ($rewind->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/rewinding_entry/' . $rewind->id));
                    }
                }
            }
            $data['update'] = true;
            $query = $this->db->query("SELECT r.*, st.job_name
									FROM rewinding r 
									LEFT JOIN sale_order so
										ON r.so = so.job_code
									LEFT JOIN job_master st
										ON so.structure_id = st.id
									WHERE r.id = $id")->row();
            $data['rewinding'] = $query;
            $data['roles'] = $this->allowed_roles;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            if ($this->input->is_ajax_request()) {
                $this->load->view('transictions/rewinding', $data);
            } else {
                $data['site_title'] = 'Update Rewinding';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('transictions/rewinding', $data);
                $this->load->view('main/footer', $data);
            }

        }

    } // rewinding_entry()


    /**************************************************
     *                    =LAMINATION ENTRY.
     **************************************************/
    public function lamination_entry($id)
    {
        if ($id) {
            $data['title'] = 'Lamination Entry || Update Lamination Data.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $this->db->trans_start();
                $lam = new Lamination_Model();
                $lam->load($id);
                // $lam->id = $this->Lamination_Model->max();
                $lam->so = $this->input->post('so');
                $lam->date = dbDate($this->input->post('date'));
                $lam->machine = $this->input->post('machine');
                $lam->operator = $this->input->post('operator');
                $lam->assistant = $this->input->post('assistant');
                $lam->helper = $this->input->post('helper');
                $lam->temperature = $this->input->post('temperature');
                $lam->rubber_size = $this->input->post('rubber_size');
                $lam->speed = $this->input->post('speed');
                $lam->qc_hold = $this->input->post('qc_hold');
                $lam->start_time = $this->input->post('start_time');
                $lam->end_time = $this->input->post('end_time');
                $lam->blade = $this->input->post('blade');
                $lam->lamination_number = $this->input->post('lamination_number');
                $lam->total_wastage = $this->input->post('total_wastage');
                $lam->balance = $this->input->post('balance');
                $lam->glue = $this->input->post('glue');
                $lam->glue_qty = $this->input->post('glue_qty');
                $lam->hardner = $this->input->post('hardner');
                $lam->hardner_qty = $this->input->post('hardner_qty');
                $lam->ethyl = $this->input->post('ethyl');
                $lam->ethyl_qty = $this->input->post('ethyl_qty');
                $lam->updated_by = $this->input->post('user_id');
                $lam->updated_time = dbTime();
                if ($lam->save()) {

                    $item = new Lamination_Detail_Model();
                    $item = $item->getWhere(["lamination_id" => $lam->id]);

                    foreach ($item as $i) {
                        $delete = new Lamination_Detail_Model();
                        $delete->load($i->ld_id);
                        $delete->delete();
                    }

                    // var_dump($_POST);
                    for ($i = 0; $i < count($this->input->post('printed_weight')); $i++) {
                        $item = new Lamination_Detail_Model();
                        $item->ld_id = $this->Lamination_Detail_Model->max();
                        $item->lamination_id = $lam->id;
                        $item->printed_weight = $this->input->post('printed_weight')[$i];
                        $item->plain_weight = $this->input->post('plain_weight')[$i];
                        $item->laminated_weight = $this->input->post('laminated_weight')[$i];
                        $item->treatment = $this->input->post('treatment')[$i];
                        $item->meter = $this->input->post('meter')[$i];
                        $item->gsm = $this->input->post('gsm')[$i];
                        $item->remarks = $this->input->post('remarks')[$i];
                        $item->save();
                    }
                    if (!$this->db->trans_status()) {
                        $this->db->trans_rollback();
                        echo "Transiction Could not complete successfully.";
                        return;
                    } else {
                        $this->db->trans_complete();
                        if ($this->input->is_ajax_request()) {
                            echo "Successfully Inserted";
                            return;
                        } else {
                            $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                            redirect(site_url('Update/lamination_entry/' . $lam->id));
                        }
                    }
                }
            }
            $data['update'] = true;
            $sql = "SELECT l.*, st.job_name, glue.material_name glue_name, hardner.material_name hardner_name, ethyl.material_name ethyl_name
					FROM lamination l 
					LEFT JOIN sale_order so
						ON l.so = so.job_code
					LEFT JOIN job_master st
						ON so.structure_id = st.id
                    LEFT JOIN material glue
                    	ON l.glue = glue.material_id
                    LEFT JOIN material hardner
                    	ON l.hardner = hardner.material_id
                    LEFT JOIN material ethyl
                    	ON l.ethyl = ethyl.material_id
					WHERE l.id = {$id}";

            $return = $this->db->query($sql);
            $data['lamination'] = $return->row();

            $detail = new Lamination_Detail_Model();
            $detail = $detail->getWhere(['lamination_id' => $id]);
            $data['lamination_detail'] = $detail;
            $data['roles'] = $this->allowed_roles;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            if ($this->input->is_ajax_request()) {
                $this->load->view('transictions/lamination', $data);
            } else {
                $data['site_title'] = 'Update Lamination Entry';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('transictions/lamination', $data);
                $this->load->view('main/footer', $data);
            }

        }// if id isset()

    } // lamination_entry()


    /**************************************************
     *                    =SLITTING ENTRY.
     **************************************************/
    public function slitting_entry($id)
    {
        if (isset($id)) {

            $data['title'] = 'Slitting Entry || Update Slitting Data.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $slitting = new Slitting_Model();
                $slitting->load($id);
                // $slitting->id = $this->Slitting_Model->max();
                $slitting->date = dbDate($this->input->post('date'));
                $slitting->so = $this->input->post('so');
                $slitting->operator = $this->input->post('operator');
                $slitting->start_time = $this->input->post('start_time');
                $slitting->end_time = $this->input->post('end_time');
                $slitting->weight_before = $this->input->post('weight_before');
                $slitting->weight_after = $this->input->post('weight_after');
                $slitting->wastage = $this->input->post('wastage');
                $slitting->trim = $this->input->post('trim');
                $slitting->remarks = $this->input->post('remarks');
                // $slitting->created_by = $this->session->userdata('user_id');
                // $slitting->created_time = $this->input->post('')
                $slitting->updated_by = $this->input->post('user_id');
                $slitting->updated_time = dbTime();;
                // if (true) {
                if ($slitting->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/slitting_entry/' . $slitting->id));
                    }
                }
            }
            $data['update'] = true;
            $query = $this->db->query("SELECT s.*, st.job_name
									FROM slitting s
									LEFT JOIN sale_order so
										ON s.so = so.job_code
									LEFT JOIN job_master st
										ON so.structure_id = st.id
									WHERE s.id = $id")->row();
            $data['slitting'] = $query;
            $data['roles'] = $this->allowed_roles;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            if ($this->input->is_ajax_request()) {
                $this->load->view('transictions/slitting', $data);
            } else {
                $data['site_title'] = 'Update Printing Entry';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('transictions/slitting', $data);
                $this->load->view('main/footer', $data);
            }
        }

    } // slitting_entry()

    public function goods_receiving_note($id)
    {

        if (isset($id)) {
            $data['title'] = 'Receive Inventory || Create GRN.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $grn = new Grn_Model();
                $grn->load($id);
                // $grn->grn_id = $this->Grn_Model->max();
                $grn->date = dbDate($this->input->post('date'));
                // $grn->supplier_id = $this->input->post('supplier_id');
                $grn->description = $this->input->post('description');
                $grn->created_by = '';
                // $grn->created_time = ;
                $grn->updated_by = $this->session->userdata('user_id');
                $grn->updated_time = dbTime();
                // var_dump($grn);
                if ($grn->save()) {

                    $item = new Grn_detail_Model();
                    $item = $item->getWhere(["grn_id" => $grn->grn_id]);

                    foreach ($item as $i) {
                        $delete = new Grn_detail_Model();
                        $delete->load($i->grnd_id);
                        $delete->delete();
                    }

                    for ($i = 0; $i < count($this->input->post('material')); $i++) {
                        $item = new Grn_detail_Model();
                        $item->grnd_id = '';
                        $item->grn_id = $grn->grn_id;
                        $item->material_id = $this->input->post('material')[$i];
                        $item->store_id = $this->input->post('mStore')[$i];
                        $item->roll_carton = $this->input->post('mRoll')[$i];
                        $item->qty = $this->input->post('mQuantity')[$i];
                        $item->rate = $this->input->post('mRate')[$i];
                        $item->defective = $this->input->post('mDefective')[$i];
                        $item->description = $this->input->post('mDescription')[$i];
                        $item->po_id = $this->input->post('po_id')[$i];
                        $item->save();
                    }
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/goods_receiving_note/' . $grn->grn_id));
                    }

                } // if GRN Update()

            } // If POST Isset()
            $data['update'] = true;

            $query = $this->db->query("SELECT grnd.grn_id, grnd.material_id, m.material_name, grnd.store_id, st.store_name, grnd.roll_carton, grnd.qty, grnd.rate, grnd.defective, grnd.description, grnd.po_id
					FROM GRN_detail AS grnd
					LEFT JOIN material AS m
						ON m.material_id = grnd.material_id
					LEFT JOIN store AS st
						ON st.store_id = grnd.store_id
					WHERE grnd.grn_id = $id");

            $data['grn_items'] = $query->result();

            $grn = new Grn_Model();
            $grn->load($id);
            $data['grn'] = $grn;
            $data['roles'] = $this->allowed_roles;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            $store = new Store_Model();
            $data['stores'] = $store->get();
            if ($this->input->is_ajax_request()) {
                $this->load->view('store/grn', $data);
            } else {
                $data['site_title'] = 'Update Goods Received Note!';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('store/grn', $data);
                $this->load->view('main/footer', $data);
            }

        } // if ID is SET

    } // goods_receiving_note() corrent


    public function goods_issue_note($id)
    {

        if (isset($id)) {
            $data['title'] = 'Receive Inventory || Create GRN.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $gin = new Gin_Model();
                $gin->load($id);
                // $grn->grn_id = $this->Grn_Model->max();
                $gin->date = dbDate($this->input->post('date'));
                // $grn->supplier_id = $this->input->post('supplier_id');
                $gin->description = $this->input->post('description');
                $gin->created_by = '';
                // $grn->created_time = ;
                $gin->updated_by = $this->session->userdata('user_id');
                $gin->updated_time = dbTime();
                if ($gin->save()) {

                    $item = new Gin_detail_Model();
                    $item = $item->getWhere(["gin_id" => $gin->gin_id]);

                    foreach ($item as $i) {
                        $delete = new Gin_detail_Model();
                        $delete->load($i->gind_id);
                        $delete->delete();
                    }

                    for ($i = 0; $i < count($this->input->post('material')); $i++) {
                        $item = new Gin_detail_Model();
                        $item->gind_id = '';
                        $item->gin_id = $gin->gin_id;
                        $item->material_id = $this->input->post('material')[$i];
                        $item->store_id = $this->input->post('mStore')[$i];
                        $item->roll_carton = $this->input->post('mRoll')[$i];
                        $item->qty = $this->input->post('mQuantity')[$i];
                        $item->rate = $this->input->post('mRate')[$i];
                        $div = $this->input->post('mDefective')[$i];
                        $div = $div ? $div : 0;
                        $item->defective = $div;
                        $item->description = $this->input->post('mDescription')[$i];
                        $item->job_code = '';
                        $item->save();
                    }
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/goods_issue_note/' . $gin->gin_id));
                    }

                } // if GRN Update()

            } // If POST Isset()
            $data['update'] = true;

            $query = $this->db->query("SELECT gind.gin_id, gind.material_id, m.material_name, gind.store_id, st.store_name, gind.roll_carton, gind.qty, gind.rate, gind.defective, gind.description
					FROM GIN_detail AS gind
					LEFT JOIN material AS m
						ON m.material_id = gind.material_id
					LEFT JOIN store AS st
						ON st.store_id = gind.store_id
					WHERE gind.gin_id = $id");

            $data['gin_items'] = $query->result();

            $gin = new Gin_Model();
            $gin->load($id);
            $data['gin'] = $gin;
            $data['roles'] = $this->allowed_roles;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            $store = new Store_Model();
            $data['stores'] = $store->get();
            if ($this->input->is_ajax_request()) {
                $this->load->view('store/gin', $data);
            } else {
                $data['site_title'] = 'Update Goods Received Note!';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('store/gin', $data);
                $this->load->view('main/footer', $data);
            }

        } // if ID is SET

    } // goods_issue_note()


    /**************************************************
     *                    =PURCHASE ORDER.
     **************************************************/
    public function purchase_order($id)
    {
        if (isset($id)) {
            $data['title'] = 'Receive Inventory || Create GRN.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $po = new Purchase_order_Model();
                $po->load($id);
                // $po->po_id = $this->Purchase_order_Model->max();
                $po->date = dbDate($this->input->post('date'));
                $po->supplier_id = $this->input->post('supplier_id');
                $hold = ($this->input->post('hold') == "on") ? 1 : 0;
                $po->hold = $hold;
                $po->description = $this->input->post('description');
                // $po->created_by = $this->session->userdata('user_id');
                // $po->created_time = dbTime();
                $po->updated_by = $this->session->userdata('user_id');
                $po->updated_time = dbTime();
                // if (true) {
                if ($po->save()) {
                    // var_dump($_POST);
                    $item = new PO_Detail_Model();
                    $item = $item->getWhere(["po_id" => $po->po_id]);

                    foreach ($item as $i) {
                        $delete = new PO_Detail_Model();
                        $delete->load($i->pod_id);
                        $delete->delete();
                    }

                    for ($i = 0; $i < count($this->input->post('material')); $i++) {
                        $item = new PO_Detail_Model();
                        $item->pod_id = '';
                        $item->po_id = $po->po_id;
                        $item->material_id = $this->input->post('material')[$i];
                        $item->qty = $this->input->post('mQuantity')[$i];
                        $item->rate = $this->input->post('mRate')[$i];
                        $item->description = $this->input->post('mDescription')[$i];
                        if ($item->save()) {
                            // echo "Sub Statement Inserted.!<br>";
                        }

                    }
                    echo "Successfully Inserted";
                    return;
                };
            }

            $data['update'] = true;

            $query = $this->db->query("SELECT pod.material_id, m.material_name, pod.qty, pod.rate, pod.description
										FROM po_detail AS pod
										JOIN purchase_order AS po
											ON po.po_id = pod.po_id
										LEFT JOIN material AS m
											ON m.material_id = pod.material_id
										WHERE po.po_id = $id");

            $data['po_items'] = $query->result();

            $query = $this->db->query("SELECT po.po_id, po.date, po.supplier_id, s.supplier_name, po.hold, po.description
										FROM purchase_order AS po
                                        LEFT JOIN supplier AS s
											ON s.supplier_id = po.supplier_id
										WHERE po.po_id = $id");

            $data['po'] = array_shift($query->result());
            $data['roles'] = $this->allowed_roles;

            // $this->load->view('header', $data);
            $this->load->view('store/purchase_order', $data);
            // $this->load->view('footer', $data);

        }// if $id isset

    } // purchase_order()


    /**************************************************
     *                    =MAINENANCE REQUEST.
     **************************************************/
    public function maintenance_request($id)
    {
        if (isset($id)) {
            $data['title'] = 'Maintenance Request Form';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $request = new Maintenance_Model();
                $request->load($id);
                // $request->id = $this->Maintenance_Model->max();
                // $request->date = dbDate($this->input->post('date'));
                // $request->req_type = $this->input->post('req_type');
                $request->req_for = $this->input->post('req_for');
                $request->request_from = $this->input->post('request_from');
                $request->request_person = ''; // $this->input->post('request_person');
                $request->problem = $this->input->post('problem');
                $request->suggestion = $this->input->post('suggestion');

                if ($request->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/maintenance_request/' . $request->id));
                    }
                }
            }

            $data['roles'] = $this->allowed_roles;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';
            $data['update'] = true;
            $request = new Maintenance_Model();
            $request->load($id);
            $data['req'] = $request;
            if ($this->input->is_ajax_request()) {
                $this->load->view('maintenance/maintenance_request', $data);
            } else {
                $data['site_title'] = 'Update Goods Received Note!';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('maintenance/maintenance_request', $data);
                $this->load->view('main/footer', $data);
            }
        }

    } // maintenance_request()


    /**************************************************
     *                    =QUOTATIONS.
     **************************************************/
    public function quotation($form_type, $id)
    {
        if (isset($id)) {
            $data['title'] = 'Update Quotation Form.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $quotation = new Quotation_Model();
                $quotation->load($id);
                $quotation->date            = dbDate($this->input->post('date'));
                $quotation->gender          = $this->input->post('gender');
                $quotation->party           = $this->input->post('party');
                $quotation->attention       = $this->input->post('attention');
                $quotation->product         = $this->input->post('product');
                $quotation->structure       = $this->input->post('structure');
                $quotation->printing        = $this->input->post('printing');
                if ($form_type == 'offset') {
                $quotation->gsm             = $this->input->post('gsm');
                $quotation->quality         = $this->input->post('quality');
                $quotation->finishing       = $this->input->post('finishing');
                $quotation->other_specification = $this->input->post('other_specification');
                }
                if (strtolower($form_type) == 'gravure') {
                $quotation->yield           = $this->input->post('yield');
                $quotation->cylinder_cost   = $this->input->post('cylinder_cost');
                $quotation->size            = $this->input->post('size');
                $quotation->bag_making          = $this->input->post('bag_making');
                $quotation->condition_4     = $this->input->post('condition_4');
                }
                $quotation->price           = $this->input->post('price');
                $quotation->moq             = $this->input->post('moq');
                $quotation->department      = $form_type;
                $quotation->person_name     = $this->input->post('person_name');
                $quotation->designation     = $this->input->post('designation');
                $quotation->contact         = $this->input->post('contact');
                $quotation->condition_1     = $this->input->post('condition_1');
                $quotation->condition_2     = $this->input->post('condition_2');
                $quotation->condition_3     = $this->input->post('condition_3');
                $quotation->condition_5     = $this->input->post('condition_5');
                $quotation->updated_by      = $this->session->userdata('user_id');
                $quotation->updated_time    = dbTime();
                // if (true) {
                if ($quotation->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('Update/quotation/' . $form_type . '/' . $id));
                    }
                }
            }

            $quotation = new Quotation_Model();
            $quotation->load($id);
            $data['update'] = true;
            $data['quotation'] = $quotation;
            $data['roles'] = $this->allowed_roles;
            $data['form_type'] = $form_type;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            if ($this->input->is_ajax_request()) {
                $this->load->view('Marketing/quotation', $data);
            } else {
                $data['site_title'] = 'Update Quotation Form.';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('Marketing/quotation', $data);
                $this->load->view('main/footer', $data);
            }

        } // end if id isset

    } // quotation()


    /**************************************************
     *                    =QUOTATIONS.
     **************************************************/
    public function offset_costing($id)
    {
        if (isset($id)) {

            $this->load->model('Offset_Costing_Foil_Model');

            $data['title'] = 'New Costing.!';
            $data['session'] = $this->session->all_userdata();
            if ($_POST) {
                $obj = new Offset_Costing_Model();
                $obj->load($id);

                $obj->date = dbDate($this->input->post('date'));
                $obj->customer_id = $this->input->post('customer_id');
                $obj->item_name = $this->input->post('item_name');
                $obj->moq = $this->input->post('moq');
                $obj->no_of_color = $this->input->post('no_of_color');
                $obj->rate_per_color = $this->input->post('rate_per_color');
                $obj->pcs_per_sheet = $this->input->post('pcs_per_sheet');
                $obj->sale_tax = $this->input->post('sale_tax');

                if ($this->input->post('show_both_amount')) {
                    $obj->show_both_amount = 1;
                } else {
                    $obj->show_both_amount = 0;
                }

                $obj->board_width = $this->input->post('board_width');
                $obj->board_height = $this->input->post('board_height');
                $obj->board_gsm = $this->input->post('board_gsm');
                $obj->board_rate = $this->input->post('board_rate');
                $obj->no_unit_per_packet = $this->input->post('no_unit_per_packet');
                $obj->wastage_percent = $this->input->post('wastage_percent');
                $obj->uv_width = $this->input->post('uv_width');
                $obj->uv_height = $this->input->post('uv_height');
                $obj->uv_rate = $this->input->post('uv_rate');
                $obj->lamination_width = $this->input->post('lamination_width');
                $obj->lamination_height = $this->input->post('lamination_height');
                $obj->lamination_rate = $this->input->post('lamination_rate');
                $obj->foil_rate = $this->input->post('foil_rate');
                if ($this->input->post('both_side_lamination')) {
                    $obj->both_side_lamination = 1;
                } else {
                    $obj->both_side_lamination = 0;
                }
                $obj->sorting_rate = $this->input->post('sorting_rate');
                $obj->die_cutting_rate = $this->input->post('die_cutting_rate');
                $obj->embossing_rate = $this->input->post('embossing_rate');
                $obj->pasting_rate = $this->input->post('pasting_rate');
                $obj->packing_rate = $this->input->post('packing_rate');
                $obj->pcs_per_carton = $this->input->post('pcs_per_carton');

                $obj->film_width = $this->input->post('film_width');
                $obj->film_height = $this->input->post('film_height');
                $obj->no_film_development = $this->input->post('no_film_development');
                $obj->film_rate = $this->input->post('film_rate');
                $obj->no_plate = $this->input->post('no_plate');
                $obj->plate_rate = $this->input->post('plate_rate');
                $obj->no_ups = $this->input->post('no_ups');
                $obj->ups_rate = $this->input->post('ups_rate');
                $obj->proofing = $this->input->post('proofing');
                $obj->blanket = $this->input->post('blanket');
                $obj->block = $this->input->post('block');
                $obj->development_foil = $this->input->post('development_foil');
                $obj->designing = $this->input->post('designing');
                $obj->cartage = $this->input->post('cartage');
                $obj->pcs_per_delivery = $this->input->post('pcs_per_delivery');
                $obj->margin = $this->input->post('margin');
                $obj->description = $this->input->post('description');
                $obj->die_to_die_width = $this->input->post('die_to_die_width');
                $obj->die_to_die_height = $this->input->post('die_to_die_height');

                // $obj->created_by = $this->session->userdata('user_id');
                // $obj->created_time = dbTime();
                $obj->updated_by = $this->session->userdata('user_id');
                $obj->updated_time = dbTime();


                $this->db->trans_start();
                $obj->save();

                $this->db->delete("offset_costing_foil", array(
                    'costing_id' => $id,
                ));

                if ($this->input->post('foil_width')[0] > 0) {
                    for ($i = 0; $i < count($this->input->post('foil_width')); $i++) {
                        $foil = new Offset_Costing_Foil_Model();
                        $foil->id = $this->Offset_Costing_Foil_Model->max();
                        $foil->costing_id = $obj->id;
                        $foil->width = $this->input->post('foil_width')[$i];
                        $foil->height = $this->input->post('foil_height')[$i];
                        $foil->amount = $this->input->post('foil_rate') * $this->input->post('foil_width')[$i] * $this->input->post('foil_height')[$i];
                        $foil->save();
                    }
                }

                $result = $this->db->trans_complete();

                if ($result) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
                        redirect(site_url('update/offset_costing/' . $obj->id));
                    }
                }
            }


            $record = new Offset_Costing_Model();
            $record->load($id);

            $customer_id = new Customer_Model();
            $customer_id->load($record->customer_id);

            $record->customer_name = $customer_id->customer_name;

            $data['update'] = true;
            $data['record'] = $record;
            $data['cal']['printing_cost_per_pcs'] = $record->get_printing_cost();
            $data['cal']['board_weight'] = $record->get_board_weight();
            $data['cal']['packet_cost'] = $record->get_board_packet_cost();
            $data['cal']['per_pcs_cost'] = $record->get_board_per_pcs_cost();
            $data['cal']['final_cost'] = $record->get_total_board_cost();
            $data['cal']['uv_rate_per_packet'] = $record->get_uv_rate_per_packet();
            $data['cal']['uv_rate_per_pcs'] = $record->get_uv_rate_per_pcs();
            $data['cal']['lamination_rate_per_packet'] = $record->get_lamination_rate_per_packet();
            $data['cal']['lamination_rate_per_pcs'] = $record->get_lamination_rate_per_pcs();
            $data['cal']['sorting_cost'] = $record->get_sorting_cost();
            $data['cal']['die_cutting_cost'] = $record->get_die_cutting_cost();
            $data['cal']['embossing_cost'] = $record->get_embossing_cost();
            $data['cal']['pasting_cost'] = $record->get_pasting_cost();
            $data['cal']['packing_cost'] = $record->get_packing_cost();
            $data['cal']['film_cost'] = $record->get_film_cost();
            $data['cal']['plates_cost'] = $record->get_plates_cost();
            $data['cal']['die_cost'] = $record->get_die_cost();
            $data['cal']['total_development_cost'] = $record->get_development_cost();
            $data['cal']['cartage_cost'] = $record->get_cartage_cost();
            $data['cal']['margin_percent'] = $record->get_margin_amount();

            $data['foils'] = $this->Offset_Costing_Foil_Model->getWhere(['costing_id' => $id]);
            $data['roles'] = $this->allowed_roles;
            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            if ($this->input->is_ajax_request()) {
                $this->load->view('offset/costing', $data);
            } else {
                $data['site_title'] = 'Update Quotation Form.';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('offset/costing', $data);
                $this->load->view('main/footer', $data);
            }

        } // end if id isset

    } // offset_costing()

}