<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Sale_order extends MY_Controller
{
    public $allowed_roles = array();

    # Methodes
    /**************************************************
     *                    ADD Sale Order.
     **************************************************/

    public function __construct()
    {
        parent::__construct();
        $this->load->model([
            'offset/Offset_sale_order_model',
            'offset/Customer_Model',
            'Off_Job_Structure_Model',
        ]);
    }

    public function index($disabled = 0)
    {
        $this->set_data('title', 'Sale Order List | Edit | Delete.!');
        $this->set_data('page_title', 'Sale Order Lists');
        $this->set_data('table_title', 'Sale Order <small>View, Edit, Delete, Disable from the List</small>');
        $this->set_data('session', $this->session->all_userdata());
        $this->load->view('offset/offset/sale_order/list', $this->get_data());
    }

    public function save($id = false)
    {
        // Get Estimated Job code just for showing in form for users.
        $this->set_data('job_code', $this->Offset_sale_order_model->max());

        // Set default variables if user not posted the form more first edit
        $this->set_data('selected_customer', []);
        $this->set_data('selected_structure', []);

        // Get Customer with posted id so that we can populate customer when submited invalid data
        if ($this->input->post('data[customer_id]')) {
            $customer = new Customer_Model();
            $customer->load($this->input->post('data[customer_id]'));
            $this->set_data('selected_customer', [$customer->customer_id => $customer->customer_name]);
        }

        // Get Structure with posted id so that we can populate customer when submited invalid data
        if ($this->input->post('data[structure_id]')) {
            $structure = new Off_Job_Structure_Model();
            $structure->load($this->input->post('data[structure_id]'));
            $this->set_data('selected_structure', [$structure->id => $structure->job_name]);
        }

        $so = new Offset_Sale_Order_Model();
        $this->set_data('record', $so);

        // Get and send form data if this is an update request check by record ID provided
        if ($id) {
            $joinRecord = $this->Offset_sale_order_model->get_join_data_by_id($id);
            $this->set_data('record', $joinRecord);
            $so->load($id);
            $this->set_data('selected_customer', [$joinRecord->customer_id => $joinRecord->customer_name]);
            $this->set_data('selected_structure', [$joinRecord->structure_id => $joinRecord->job_name]);
        }

        // Check if Form submitted and procced form and insert record
        if (isset($_POST['submit'])) {

            // Validated Input Fields when submitting the form.
            $this->validate_fields($id);

            // Procced the form if validation passes
            if ($this->form_validation->run() == true) {
                // Populate object with input values
                foreach ($this->input->post('data') as $key => $value) {
                    $so->{$key} = $value;
                }
                // Populate Dates with database format
                $so->date = dbDate($this->input->post('data[date]'));
                $so->po_date = dbDate($this->input->post('data[po_date]'));
                $so->delivery_date = dbDate($this->input->post('data[delivery_date]'));
                $so->hold = $this->input->post('hold') ? 1 : 0;
                $so->completed = $this->input->post('completed') ? 1 : 0;
                $so->{$id ? 'updated_by' : 'created_by'} = $this->session->userdata('user_id');
                $so->{$id ? 'updated_time' : 'created_time'} = dbTime();

                // Save Record and get ID if new Record otherwise affected rows.
                $inserted_id = $so->save();
                $id = $id ? $id : $inserted_id;

                // Check if there is any database error and flash message and redirect.
                if ($error_code = $this->db->error()['code']) {
                    set_flash_message(ERROR, "Failed with Error code: $error_code.");
                } elseif ($id) {
                    set_flash_message(SUCCESS, $id ? 'Record updated successfully.' : 'Record added successfully.');
                    redirect(site_url('offset/sale_order/'));
                } // end if else database error or not.

            } // end form validation
        }

        // send data to view
        $this->set_data('roles', $this->allowed_roles);
        $this->set_data('site_title', $id ? 'Edit Sale Order' : 'Add New Sale Order');
        $this->load->view('offset/offset/sale_order/form', $this->get_data());
    }

    /**************************************************
     *           ADD ORDER WASTAGE DETAILS.
     **************************************************/

    public function add_wastage($job_code, $id = false)
    {
        $this->load->model('Offset_Sale_Order_Model');
        $so = new Offset_Sale_Order_Model();
        $so->load($job_code);
        $this->set_data('so', $so);
        $this->set_data('job_code', $job_code);

        $this->load->model('offset/Offset_order_wastage_model');
        $record = new Offset_order_wastage_model();
        if ($id) { $record->load($id); }
        $this->set_data('record', $record);

        // Check if Form submitted and procced form and insert record
        if (isset($_POST['submit'])) {
            // Populate object with input values
            foreach ($this->input->post('data') as $key => $value) {
                $record->{$key} = $value;
            }
            $record->dispatch_date = dbDate($this->input->post('data[dispatch_date]'));
            $record->{($id) ? 'updated_by' : 'added_by'} = $this->session->userdata('user_id');
            $record->{($id) ? 'updated_time' : 'added_time'} = dbTime();

            // Save Record and get ID if new Record otherwise affected rows.
            $affected_row = $record->save();
            // Check if there is any database error and flash message and redirect.
            if ($error_code = $this->db->error()['code']) {
                set_flash_message(ERROR, "Failed with Error code: $error_code.");
            } elseif ($affected_row) {
                $id = $id ? $id : $affected_row;
                set_flash_message(SUCCESS, $id ? 'Record updated successfully.' : 'Record added successfully.');
                redirect(site_url('offset/sale_order/'));
            } // end if else database error or not.
        }// if save

        $this->set_data('roles', $this->allowed_roles);
        $this->set_data('site_title', $id ? 'Edit Order Details' : 'Add New Order Details');
        $this->load->view('offset/offset/sale_order/detail-form', $this->get_data());
    }

    public function production_detail($id)
    {
        if (!$id) {
            return;
        }else{
            $this->load->model('offset/Offset_order_wastage_model');
            $x = new Offset_order_wastage_model();
            $record = $x->load_by_id($id);
            $this->load->library('PDF_offset_production_detail');
            $pdf = new PDF_offset_production_detail();
            $pdf->detail_id = $id;
            $pdf->set_data($record);
            $pdf->AddPage();

            $pdf->DisplayTable();
        }
    }

    public function validate_fields($id)
    {
        $this->form_validation->set_rules('data[po_num]', 'Purchase Order no.', 'required');
        $this->form_validation->set_rules('data[customer_id]', 'Customer', 'required');
        $this->form_validation->set_rules('data[structure_id]', 'Structure', 'required');
        $this->form_validation->set_rules('data[order_type]', 'Order Type', 'required');
        $this->form_validation->set_rules('data[quantity]', 'Quantity', 'required');
        $this->form_validation->set_rules('data[rate]', 'Rate', 'required');
        $this->form_validation->set_rules('data[excess_quantity]', 'Excess Quantity', 'required');
    }

    /**************************************************
     *                    =SALE ORDER LIST.
     **************************************************/

    public function list_json($hold = 0)
    {
        $hold = $hold ? 0 : 1;
        $fetch_data = $this->Offset_sale_order_model->make_datatables($hold);
        $data = array();
        foreach ($fetch_data as $row) {
            $activation = $row->hold ? 0 : 1;
            $activation_text = $row->hold ? "Unhold" : "Hold";
            $activation_icon = $row->hold ? "unlock" : "lock";
            $activation_class = $row->hold ? "activate" : "inactivate";
            $sub_array = array();

            $sub_array[] = anchor(site_url('offset/sale_order/save/' . $row->job_code), $row->job_code . ' <i class="fa fa-pencil"></i>');
            $sub_array[] = pkDate($row->date);
            $sub_array[] = pkDate($row->po_date);
            $sub_array[] = pkDate($row->delivery_date);
            $sub_array[] = $row->customer_name;
            $sub_array[] = $row->job_name;
            $actions = '<div class="btn-group">' .
                '<button data-toggle="dropdown" class="dropdown-toggle btn btn-icon-toggle btn-danger btn-sm">
                        <i class="fa fa-ellipsis-h "></i>
                    </button>
                    <ul class="dropdown-menu">';
            if (User_Model::hasAccess("viewOffsetJobCard")):
                $actions .= '<li>' . anchor(site_url("offset/sale_order/job_card/$row->job_code"), '<i class="fa fa-file-pdf-o"></i> Job card', ' data-remote="false" data-toggle="modal" data-target="#myModal" class=""') . '</li>';
                $actions .= '<li>' . anchor(site_url("offset/sale_order/die_making_form/$row->job_code"), '<i class="fa fa-file-text"></i> Die Making Form', ' data-remote="false" data-toggle="modal" data-target="#myModal"') . '</li>';
                $actions .= '<li>' . anchor(site_url("offset/sale_order/plate_making_form/$row->job_code"), '<i class="fa fa-file-text"></i> Plate Making Form', ' data-remote="false" data-toggle="modal" data-target="#myModal"') . '</li>';
            endif;
            if (User_Model::hasAccess("viewOffsetSaleOrder")):
                $actions .= '<li>' . anchor(site_url('offset/sale_order/so_print/' . $row->job_code), '<i class="fa fa-file-pdf-o"></i> Sale Order (Production)', ' data-remote="false" data-toggle="modal" data-target="#myModal"') . '</li>';
                $actions .= '<li>' . anchor(site_url('offset/sale_order/so_print/' . $row->job_code . '/accounts'), '<i class="fa fa-file-pdf-o"></i> Sale Order (Accounts)', ' data-remote="false" data-toggle="modal" data-target="#myModal"') . '</li>';
            endif;
            if (User_Model::hasAccess("editOffsetSaleOrder")):
                if($row->info_id){
                    $actions .= '<li>' . anchor(site_url('offset/sale_order/add_wastage/' . $row->job_code . '/' . $row->info_id), '<i class="fa fa-pencil"></i>  Edit Wastage Details') . '</li>';
                    $actions .= '<li>' . anchor(site_url("offset/sale_order/production_detail/$row->info_id"), '<i class="fa fa-file-pdf-o"></i> Print Wastage Details', ' data-remote="false" data-toggle="modal" data-target="#myModal" class=""') . '</li>';
                }else{
                    $actions .= '<li>' . anchor(site_url('offset/sale_order/add_wastage/' . $row->job_code), '<i class="fa fa-plus"></i>  Add Wastage Details') . '</li>';                    
                }
                $actions .= '<li>' . anchor(site_url('offset/sale_order/save/' . $row->job_code), '<i class="fa fa-pencil"></i> Edit') . '</li>';
                $actions .= '<li>' . anchor(site_url("offset/sale_order/change_hold/$row->job_code/$activation"),"<i class='fa fa-$activation_icon'></i> $activation_text", "class='$activation_class'") . '</li>';
                $actions .= '<li>' . anchor(site_url('offset/sale_order/delete/' . $row->job_code), '<i class="fa fa-trash"></i> Delete', 'class="delete-entry"') . '</li>';
            endif;

            $actions .= '</ul>' .
                '</div>';
            $sub_array[] = $actions;
            $data[] = $sub_array;
        }
        $output = array(
            "draw" => intval($_POST["draw"]),
            "recordsTotal" => $this->Offset_sale_order_model->get_all_data($hold),
            "recordsFiltered" => $this->Offset_sale_order_model->get_filtered_data($hold),
            "data" => $data,
        );
        // echo sprintf("<script> console.log('%s'); </script>", addslashes($this->Offset_group_model->get_all_data($active)));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($output));
    }

    public function change_hold($id, $active)
    {
        $record = new Offset_sale_order_model();
        $record->load($id);
        $record->hold = $active;
        $affactedRows = $record->save();
        if ($error_code = $this->db->error()['code']) {
            set_flash_message(ERROR, "Failed with error code: $error_code.");
        } elseif ($affactedRows) {
            set_flash_message(SUCCESS, $active ? 'Record holded successfully.' : 'Record unholded successfully.');
            redirect(site_url("offset/sale_order/index/$active"));
        } // if category inserted
    }

    /**************************************************
     *                DELETING =SALE ORDER.
     **************************************************/

    public function delete($id = null)
    {
        if ($id) {
            $this->load->model('Offset_Sale_Order_Model');
            $so = new Offset_Sale_Order_Model();
            $so->load($id);

            if ($so->job_code) {
                $so->delete();
                echo "Successfully Deleted";
            } else {
                echo "<h1>Sorry No Record Found.!</h1>";
            }

        } // END IF ID PRAMETER IS SET
        else {
                echo "<h1>Please Follow Given Link.!</h1>";
            }

        } // END DELETE CATEGORY FUCTION

        /**************************************************
         *                    =sale order.
         **************************************************/

        public function so_print($job_code, $accounts = false)
    {

            if (User_Model::hasAccess('viewOffsetSaleOrder')) {

                if (isset($job_code)) {

                    $material = new Material_Model();
                    $this->load->model('Offset_Sale_Order_Model');
                    $job = $this->Offset_Sale_Order_Model->get_order_by_job($job_code);
                    // var_dump($job);

                    $pdf = new fpdf("P", "mm", "A4");
                    $pdf->SetMargins(15, 10);

                    $pdf->AddPage();
                    $pdf->SetFillColor(233, 234, 237);
                    $pdf->SetTitle("Sale Order # " . $job_code);
                    $pdf->Image(base_url('assets/img/logo.jpg'), 15, 10, 30, 25);
                    // $pdf->ln(20);
                    $pdf->SetFont('Arial', 'U', 18);
                    $pdf->Cell(0, 4, "", 0, 1);
                    $pdf->Cell(0, 10, "" . "SALE ORDER (Offset)", 0, 0, 'C');
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->Cell(0, 10, "" . "PTP.FM.PR.008", 0, 1, 'R');
                    $pdf->SetFont('Arial', 'I', 10);
                    // $pdf->Cell(0,10,""."Offset",0,1,'C');

                    $pdf->ln(28);

                    $colors = array();
                    $spotColors = array();
                    if ($job->c) {
                        $colors[] = 'Cyan';
                    }

                    if ($job->m) {
                        $colors[] = 'Magenta';
                    }

                    if ($job->y) {
                        $colors[] = 'Yellow';
                    }

                    if ($job->k) {
                        $colors[] = 'Black';
                    }

                    if ($job->spot1) {
                        $spotColors[] = $job->spot1;
                    }

                    if ($job->spot2) {
                        $spotColors[] = $job->spot2;
                    }

                    if ($job->spot3) {
                        $spotColors[] = $job->spot3;
                    }

                    $uv = array();
                    if ($job->uv_gloss) {
                        $uv[] = 'Gloss';
                    }

                    if ($job->uv_matt) {
                        $uv[] = 'Matt';
                    }

                    if ($job->uv_spot) {
                        $uv[] = 'Spot';
                    }

                    $waterBase = array();
                    if ($job->water_gloss) {
                        $waterBase[] = 'Gloss';
                    }

                    if ($job->water_matt) {
                        $waterBase[] = 'Matt';
                    }

                    // check($job);
                    $datas = [
                        "Date:" => pkDate($job->date),
                        "Delivery Date:" => pkDate($job->delivery_date),
                        "Job #:" => $job->job_code,
                        "PO #:" => $job->po_num,
                        "Party:" => $job->customer_name,
                        "Item:" => $job->job_name,
                        "Material:" => $job->material_id,
                        "Structure:" => strtoupper($job->sheet_type) . ', (' . $job->board_width . '" x ' . $job->board_height . '") ' . $job->gsm . 'gsm' . ' (' . number_format(($job->no_ups > 0) ? $job->quantity / $job->no_ups : 0) . ' sheets)',
                        "No. of UPs" => sprintf("%s UPs (%s)", $job->no_ups, number_format(($job->quantity / $job->no_ups))),
                        "Grain:" => ($job->grain_size == 'w') ? $job->board_width . '"' : $job->board_height . '"',
                        "Order Quantity:" => number_format($job->quantity, 2),
                        "Rate:" => ($accounts == 'accounts') ? number_format($job->rate, 2) . " PKR" : '',
                        "Colors:" => implode(', ', $colors),
                        "Spot Colors:" => implode(', ', $spotColors),
                        "UV" => join(', ', $uv),
                        "Water Base" => join(', ', $waterBase),
                        "Fluting" => $job->flutting,
                        "Remarks" => $job->remarks,
                    ];

                    $height = 15;
                    foreach ($datas as $key => $value) {
                        if (empty(str_replace(" ", "", $key)) && empty($value)) {
                            $height += 15;
                            continue;
                        }
                        if ($key == 'Remarks') {
                            $pdf->SetFont('Arial', 'B', 10);
                            $pdf->cell(45, 10, strtoupper($key), 0, 0);
                            $pdf->SetFont('Arial', '', 9);
                            $pdf->MultiCell(0, 6, $job->remarks);
                        } else {
                            $pdf->SetFont('Arial', 'B', 10);
                            $pdf->cell(45, 10, strtoupper($key), 0, 0);
                            $pdf->SetFont('Arial', '', 9);
                            $pdf->cell(60, 10, $value, 0, 1);
                        }
                    }

                    $pdf->setXY(15, 275);
                    $pdf->SetAutoPageBreak(false, $margin = 0);
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->cell(100, 10, 'Checked By: _______________________');
                    $pdf->cell(30, 10, 'Approved By: _______________________');
                    $pdf->Ln();
                    // $pdf->setX(110);
                    $pdf->SetFont('courier', 'U', 8);
                    $pdf->cell(30, 10, 'Prepare By: ' . ($job->created_by) . " (" . created_time($job->created_time) . ")");

                    $fileName = 'sale_order_' . $job->job_name;
                    (isset($_GET['download']) && $_GET['download'] == 'true') ? $pdf->Output($fileName . '.pdf', 'D') : $pdf->Output();

                } // if id isset

            } else { // if roll exist
                echo "Your are not allowed for this view";
            }

        } // so_print()

        public function job_card($job_code)
    {
            if (!isset($job_code)) {
                echo 'Please follow the link';
                return;
            }
            if (User_Model::hasAccess('viewOffsetJobCard')) {
                $this->load->model('Offset_Sale_Order_Model');

                $data = $this->Offset_Sale_Order_Model->get_job_card_data($job_code);
                $this->load->library('PDF_Offset_Job_Card');
                ob_start();
                $pdf = new PDF_Offset_Job_Card();
                $pdf->data = $data;
                $pdf->job_code = $job_code;
                $pdf->AddPage('L');
                $pdf->SetTitle('Job Card For Job # ' . $job_code);

                $pdf->DisplayTable();

                $pdf->Output();

            } else {
                echo "Your are not allowed for this view";
            }
        }

        public function die_making_form($job_code)
    {
            if (!isset($job_code)) {
                echo 'Please follow the link';
                return;
            }
            if (User_Model::hasAccess('viewOffsetJobCard')) {
                $this->load->model('Offset_Sale_Order_Model');

                $data = $this->Offset_Sale_Order_Model->get_job_card_data($job_code);
                $this->load->library('fpdf');
                ob_start();
                $pdf = new FPDF();
                $pdf->AddPage();
                $resetX = $pdf->GetX();
                $resetY = $pdf->GetY();
                $pdf->SetTitle('Die Making Form For Sale Order # ' . $job_code);

                $pdf->SetFont('times', 'U', 18);
                $pdf->Cell(0, 10, 'Offset Die Making Department', 0, 1, "C");

                $pdf->SetFont('times', 'BU', 20);
                $pdf->Cell(0, 10, 'Order Form', 0, 1, "C");
                $pdf->Ln(3);
                $pdf->SetFont('times', '', 14);
                $pdf->Cell(100, 10, "Job #: $data->job_code", 0, 0);
                $pdf->Cell(0, 10, 'Date: ' . date('d/m/Y'), 0, 1, "R");
                $pdf->Ln(3);
                // check($data);
                $w = 40;
                $h = 6.5;
                $pdf->Cell($w, $h, 'Customer P.O # : ');
                $pdf->Cell(0, 7, $data->po_num, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Master File # : ');
                $pdf->Cell(0, 7, $data->structure_id, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Customer Name : ');
                $pdf->Cell(0, 7, $data->customer_name, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Job Name : ');
                $pdf->Cell(0, 7, $data->job_name, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Epson No. : ');
                $pdf->Cell(0, 7, $data->structure_id, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Job UPS : ');
                $pdf->Cell(0, 7, $data->no_ups, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Order Status : ');
                $pdf->Cell($w / 2, 7, 'Repeat', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'New', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'Proofing', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Die : ');
                $pdf->Cell($w / 2, 7, 'New', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'Rule Change', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'Remake', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Rule Used : ');
                $pdf->Cell($w / 2, 7, 'Cutting', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'Creasing', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'Perforation', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Rule Thickness : ');
                $pdf->Cell($w / 2, 7, '2PT', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'Special', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Wooden Ply Quality: ');
                $pdf->Cell($w / 2, 7, 'White', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'Black', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Priority : ');
                $pdf->Cell($w / 2, 7, 'Urgent', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'Normal', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Die Made Against : ');
                $pdf->Cell($w / 2, 7, 'Order', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 10, 7, 'Proofing', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(5);

                $pdf->SetFont('times', 'B', 18);
                $pdf->Cell(0, 7, 'Remarks', 1, 1, 'C');
                $pdf->Cell(0, 7, '', 1, 1, 'C');
                $pdf->Cell(0, 7, '', 1, 1, 'C');
                $pdf->Cell(0, 7, '', 1, 1, 'C');
                $pdf->Cell(0, 7, '', 1, 1, 'C');
                $pdf->Ln(5);

                $pdf->Cell(0, 8, 'Post Production', 1, 1, 'C');
                $pdf->SetFont('times', '', 14);
                $pdf->Cell($w + 10, 8, 'Die Making On: ', 1, 0);
                $pdf->Cell(0, 8, '', 1, 1);
                $pdf->Cell($w + 10, 8, 'Die No.: ', 1, 0);
                $pdf->Cell(0, 8, '', 1, 1, 'C');
                $pdf->Cell($w + 10, 8, 'Operator Name: ', 1, 0);
                $pdf->Cell(0, 8, '', 1, 1, 'C');
                $pdf->Cell($w + 10, 8, 'Shift: ', 1, 0);
                $pdf->Cell($w, 8, '', 1, 0, 'C');
                $pdf->Cell($w + 10, 8, 'Date: ', 1, 0);
                $pdf->Cell(0, 8, 'Time: ', 1, 1);
                $pdf->Ln(14);

                $pdf->Cell(65, 8, '    _______________        ', 0, 0, "C");
                $pdf->Cell(65, 8, '       _______________        ', 0, 0, "C");
                $pdf->Cell(65, 8, '       _______________        ', 0, 1, "C");

                $pdf->Cell(65, 8, '    Operator Sign        ', 0, 0, "C");
                $pdf->Cell(65, 8, '       Supervisor Sign        ', 0, 0, "C");
                $pdf->Cell(65, 8, '       Authorized Sign        ', 0, 0, "C");

                $pdf->Output();

            } else {
                echo "Your are not allowed for this view";
            }
        }

        public function plate_making_form($job_code)
    {
            if (!isset($job_code)) {
                echo 'Please follow the link';
                return;
            }
            if (User_Model::hasAccess('viewOffsetJobCard')) {
                $this->load->model('Offset_Sale_Order_Model');

                $data = $this->Offset_Sale_Order_Model->get_job_card_data($job_code);
                $this->load->library('fpdf');
                ob_start();
                $pdf = new FPDF();
                $pdf->AddPage();
                $resetX = $pdf->GetX();
                $resetY = $pdf->GetY();
                $pdf->SetTitle('Plate Making Form For Sale Order # ' . $job_code);

                $pdf->SetFont('times', 'U', 18);
                $pdf->Cell(0, 10, 'Offset Plate Making Department', 0, 1, "C");

                $pdf->SetFont('times', 'BU', 20);
                $pdf->Cell(0, 10, 'Order Form', 0, 1, "C");
                $pdf->Ln(3);
                $pdf->SetFont('times', '', 14);
                $pdf->Cell(100, 10, "Job # $data->job_code", 0, 0);
                $pdf->Cell(0, 10, 'Date: ' . date('d/m/Y'), 0, 1, "R");
                $pdf->Ln(3);
                // check($data);
                $w = 40;
                $h = 6.5;
                $pdf->Cell($w, $h, 'Customer P.O # : ');
                $pdf->Cell(0, 7, $data->po_num, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Master File # : ');
                $pdf->Cell(0, 7, $data->structure_id, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Customer Name : ');
                $pdf->Cell(0, 7, $data->customer_name, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Job Name : ');
                $pdf->Cell(0, 7, $data->job_name, "B", 1);
                $pdf->Ln(3);

                $pdf->Cell($w, $h, 'Eppson No. : ');
                $pdf->Cell(0, 7, $data->structure_id, "B", 1);
                $pdf->Ln(8);

                $pdf->Cell($w, $h, 'Order Status : ');
                $pdf->Cell($w / 2, 7, 'Repeat', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, 'New', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, 'Proofing', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Plate : ');
                $pdf->Cell($w / 2, 7, 'New', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, 'Remake', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, 'Design Change', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Required Color: ');
                $pdf->Cell($w / 2, 7, '1', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, '2', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, '3', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, ' ');
                $pdf->Cell($w / 2, 7, '4', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, '5', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, '6', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Priority : ');
                $pdf->Cell($w / 2, 7, 'Urgent', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, 'Normal', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(1);

                $pdf->Cell($w, $h, 'Plate Made Against : ');
                $pdf->Cell($w / 2, 7, 'Order', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 0, 'R');
                $pdf->Cell($w - 6, 7, 'Proofing', 0, 0, 'R');
                $pdf->Cell($w / 2, 7, '', 1, 1, 'R');
                $pdf->Ln(10);

                $pdf->SetFont('times', 'B', 18);
                $pdf->Cell(0, 7, 'Remarks', 1, 1, 'C');
                $pdf->Cell(0, 7, '', 1, 1, 'C');
                $pdf->Cell(0, 7, '', 1, 1, 'C');
                $pdf->Cell(0, 7, '', 1, 1, 'C');
                $pdf->Cell(0, 7, '', 1, 1, 'C');
                $pdf->Ln(5);

                $pdf->Cell(0, 8, 'Post Production', 1, 1, 'C');
                $pdf->SetFont('times', '', 14);
                $pdf->Cell($w + 10, 8, 'Plate Making On: ', 1, 0);
                $pdf->Cell(0, 8, '', 1, 1);
                $pdf->Cell($w + 10, 8, 'Plate No.: ', 1, 0);
                $pdf->Cell(0, 8, '', 1, 1, 'C');
                $pdf->Cell($w + 10, 8, 'Operator Name: ', 1, 0);
                $pdf->Cell(0, 8, '', 1, 1, 'C');
                $pdf->Cell($w + 10, 8, 'Shift: ', 1, 0);
                $pdf->Cell($w, 8, '', 1, 0, 'C');
                $pdf->Cell($w + 10, 8, 'Date: ', 1, 0);
                $pdf->Cell(0, 8, 'Time: ', 1, 1);
                $pdf->Ln(22);

                $pdf->Cell(65, 8, '    _______________        ', 0, 0, "C");
                $pdf->Cell(65, 8, '       _______________        ', 0, 0, "C");
                $pdf->Cell(65, 8, '       _______________        ', 0, 1, "C");

                $pdf->Cell(65, 8, '    Operator Sign        ', 0, 0, "C");
                $pdf->Cell(65, 8, '       Supervisor Sign        ', 0, 0, "C");
                $pdf->Cell(65, 8, '       Authorized Sign        ', 0, 0, "C");

                $pdf->Output();

            } else {
                echo "Your are not allowed for this view";
            }
        }

    }
