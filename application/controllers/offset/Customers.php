<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Customers extends MY_Controller
{
    public $allowed_roles = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model([
            'offset/Offset_customer_model',
        ]);
    }
    
    public function index($disabled = 0)
    {
        $this->set_data('title', 'Customer List | Edit | Delete.!');
        $this->set_data('page_title', 'Customer Lists');
        $this->set_data('table_title', 'Customer <small>View, Edit, Delete, Disable from the List</small>');
        $this->set_data('session', $this->session->all_userdata());
        $this->load->view('offset/offset/customers/list', $this->get_data());
    }

    # Methodes
    /**************************************************
     *       ADD Customer or Edit if id provided.
     **************************************************/

    public function save($id = false)
    {
        $record = new Offset_customer_model();
        
        // Get and send form data if this is an update request check by record ID provided
        if ($id) { $record->load($id); }
        $this->set_data('record', $record);

        // Check if Form submitted and procced form and insert record
        if (isset($_POST['submit'])) {
            // Validated Input Fields when submitting the form.
            $this->validate_fields($id);

            // Procced the form if validation passes
            if ($this->form_validation->run() == true) {
                // Populate object with input values
                foreach ($this->input->post('data') as $key => $value) {
                    $record->{$key} = $value;
                }
                // Populate Dates with database format
                $record->active = $this->input->post('active') ? 1 : 0;
                $record->{$id ? 'update_by' : 'create_by'} = $this->session->userdata('user_id');
                $record->{$id ? 'update_time' : 'create_time'} = dbTime();

                // Save Record and get ID if new Record otherwise affected rows.
                $inserted_id = $record->save();
                $id = $id ? $id : $inserted_id;

                // Check if there is any database error and flash message and redirect.
                if ($error_code = $this->db->error()['code']) {
                    set_flash_message(ERROR, "Failed with Error code: $error_code.");
                } elseif ($id) {
                    set_flash_message(SUCCESS, $id ? 'Record updated successfully.' : 'Record added successfully.');
                    redirect(site_url('offset/customers/'));
                } // end if else database error or not.

            } // end form validation
        }

        // send data to view
        $this->set_data('roles', $this->allowed_roles);
        $this->set_data('site_title', $id ? 'Edit Sale Order' : 'Add New Sale Order');
        $this->load->view('offset/offset/customers/form', $this->get_data());
    }

    /**************************************************
     *           ADD ORDER WASTAGE DETAILS.
     **************************************************/

    public function validate_fields($id)
    {
        if ($id){
            $this->form_validation->set_rules('data[customer_name]', 'Customer Name', 'required|callback_custom_name_check['.$id.']');
        }else{
            $this->form_validation->set_rules('data[customer_name]', 'Customer Name', 'required|is_unique[offset_customer.customer_name]');
        }
    }
    
    protected function custom_name_check($name, $id){
        $this->db->where('customer_name',$name);
        $this->db->where('customer_id !=',$id);
        $users = $this->db->get('offset_customer');
        if($users->row()){
            $this->form_validation->set_message('custom_name_check', 'The {field} must be unique. This is already in use.');
            return false;
        }else{
            return true;
        }
    }

    /**************************************************
     *                  CUSTOMER LIST.
     **************************************************/

    public function list_json($active=false)
    {
        $active = $active?1:0;
        $fetch_data = $this->Offset_customer_model->make_datatables($active);  
        $data = array();
        foreach($fetch_data as $row)  
        {
            $activation = $row->active? 0: 1;
            $activation_text = $row->active? "Inactivate": "Activate";
            $activation_icon = $row->active? "lock": "unlock";
            $activation_class = $row->active? "inactivate": "activate";
            $sub_array=array();
            
            $sub_array[] = anchor(site_url('offset/customers/save/'.$row->customer_id), $row->customer_id.' <i class="fa fa-pencil"></i>');
            $sub_array[] = $row->customer_name;
            $sub_array[] = $row->active? '<i class="fa fa-unlock"></i>':'<i class="fa fa-lock"></i>';
            $sub_array[] = '<div class="btn-group">'.
                    '<button data-toggle="dropdown" class="dropdown-toggle btn btn-icon-toggle btn-danger btn-sm">
                        <i class="fa fa-ellipsis-h "></i>
                    </button>
                    <ul class="dropdown-menu">'.
                        '<li>'. anchor(site_url('offset/customers/save/'.$row->customer_id),'<i class="fa fa-pencil"></i> Edit') . '</li>'.
                        '<li>'. anchor(site_url("offset/customers/change_activation/$row->customer_id/$activation"),"<i class='fa fa-$activation_icon'></i> $activation_text", "class='$activation_class'") . '</li>'.
                        '<li>'. anchor(site_url('offset/customers/delete/'.$row->customer_id),'<i class="fa fa-trash"></i> Delete', 'class="delete-entry"') . '</li>'.
                    '</ul>'.
                '</div>';
            $data[] = $sub_array;
        }
        $output = array(  
            "draw"              =>     intval($_POST["draw"]),  
            "recordsTotal"      =>     $this->Offset_customer_model->get_all_data($active),  
            "recordsFiltered"   =>     $this->Offset_customer_model->get_filtered_data($active),  
            "data"              =>     $data  
        );
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($output));
    }

    public function change_activation($id, $active)
    {
        $record = new Offset_customer_model();
        $record->load($id);
        $record->active = $active;
        $affactedRows = $record->save();
        if ($error_code = $this->db->error()['code']) {
            set_flash_message(ERROR, "Failed with error code: $error_code.");
        } elseif ($affactedRows) {
            set_flash_message(SUCCESS, $active ? 'Record holded successfully.' : 'Record unholded successfully.');
            redirect(site_url("offset/customers/index/$active"));
        } // if status changed
    }

    /**************************************************
     *                DELETING.
     **************************************************/

    public function delete($id = null)
    {
        if ($id) {
            $this->load->model('Offset_Sale_Order_Model');
            $so = new Offset_Sale_Order_Model();
            $so->load($id);

            if ($so->job_code) {
                $so->delete();
                echo "Successfully Deleted";
            } else {
                echo "<h1>Sorry No Record Found.!</h1>";
            }

        } else { // END IF ID PRAMETER IS SET
            echo "<h1>Please Follow Given Link.!</h1>";
        }

    } // END DELETE FUCTION

    public function delete_extra_customers()
    {
        $this->Offset_customer_model->extract_customers_from_gravure();
    }

}
