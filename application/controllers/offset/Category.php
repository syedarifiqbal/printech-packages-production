<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category extends MY_Controller
{
    public $allowed_roles = array();

    function __construct()
    {
        parent::__construct();
        $this->load->model("offset/Offset_category_Model");
    }

    function index($disabled=0)
    {
        $this->set_data( 'title', 'Category List | Edit | Delete.!');
        $this->set_data('page_title', 'Category Lists');
        $this->set_data('table_title', 'Category <small>View, Edit, Delete, Disable from the List</small>');
        $this->set_data( 'session', $this->session->all_userdata());
        $this->load->view('offset/offset/category/list', $this->get_data());
    }

    public function save($id=false)
    {
        $this->set_data( 'title', 'Add new category | Offset - Printech Packages');
        $this->set_data('session', $this->session->all_userdata());
        $this->set_data('page_title', 'Category');
        
        $record = new Offset_category_Model();
        if( $id ){ $record->load($id); }
        $this->set_data('record', $record);

        if (isset($_POST['submit'])) {
            foreach ($this->input->post('data') as $key => $value) {
                $record->{$key} = $value;
            }
            $record->{$id?'updated_by':'created_by'} = $this->session->userdata('user_id');
            $inserted_id = $record->save();
            $id = $id? $id: $inserted_id;
            if($error_code = $this->db->error()['code']){
                set_flash_message(ERROR, "Failed with Error code: $error_code.");
            }elseif ($id) {
                set_flash_message(SUCCESS, $id?'Record updated successfully.':'Record added successfully.');
                redirect(site_url('offset/category/'));
            } // if category inserted
            // x($this->db->last_query());
        } // if isset post

        $this->set_data('roles', $this->allowed_roles);
        $this->set_data('site_title', $id?'Edit Category':'Add New Category');
        $this->load->view('offset/offset/category/form', $this->get_data());

    } // Add Category

    function change_activation($id, $active)
    {
        $record = new Offset_category_Model();
        $record->load($id);
        $record->active = $active;
        $affactedRows = $record->save();
        if($error_code = $this->db->error()['code']){
            set_flash_message(ERROR, "Failed with error code: $error_code.");
        }elseif ($affactedRows) {
            set_flash_message(SUCCESS, $active?'Record activated successfully.':'Record inactivated successfully.');
            redirect(site_url('offset/category/'));
        } // if category inserted
    }

    // Delete Category
    public function delete($id=null)
    {
        if ($id) {

            $category = new Offset_Category_Model();
            $category->load($id);

            if ($category->category_id){

                $category->delete();
                if($this->db->error()['code']){
                    set_flash_message(ERROR, "Failed with Error code: $error_code.");
                }else{
                    set_flash_message(SUCCESS, $category->category_name.' Deleted Successfully.');
                    redirect(site_url('offset/category/'));
                }

            }else{
                set_flash_message(ERROR, 'Sorry No Record Found.!');
                redirect(site_url('offset/category/'));
            }

        } // END IF ID PRAMETER IS SET
        else
            echo "<h1>Please Follow Given Link.!</h1>";
    } // END DELETE CATEGORY FUCTION


    public function list_json($active=false)
    {
        $active = $active?1:0;
        $fetch_data = $this->Offset_category_Model->make_datatables($active);  
        $data = array();
        foreach($fetch_data as $row)  
        {
            $activation = $row->active? 0: 1;
            $activation_text = $row->active? "Inactivate": "Activate";
            $activation_icon = $row->active? "lock": "unlock";
            $activation_class = $row->active? "inactivate": "activate";
            $sub_array=array();
            
            $sub_array[] = anchor(site_url('offset/category/save/'.$row->category_id), $row->category_id.' <i class="fa fa-pencil"></i>');
            $sub_array[] = $row->category_name;
            $sub_array[] = $row->description;
            $sub_array[] = '<div class="btn-group">'.
                    '<button data-toggle="dropdown" class="dropdown-toggle btn btn-icon-toggle btn-danger btn-sm">
                        <i class="fa fa-ellipsis-h "></i>
                    </button>
                    <ul class="dropdown-menu">'.
                        '<li>'. anchor(site_url('offset/category/save/'.$row->category_id),'<i class="fa fa-pencil"></i> Edit') . '</li>'.
                        '<li>'. anchor(site_url("offset/category/change_activation/$row->category_id/$activation"),"<i class='fa fa-$activation_icon'></i> $activation_text", "class='$activation_class'") . '</li>'.
                        '<li>'. anchor(site_url('offset/category/delete/'.$row->category_id),'<i class="fa fa-trash"></i> Delete', 'class="delete-entry"') . '</li>'.
                    '</ul>'.
                '</div>';
            $data[] = $sub_array;
        }
        $output = array(  
            "draw"              =>     intval($_POST["draw"]),  
            "recordsTotal"      =>     $this->Offset_category_Model->get_all_data($active),  
            "recordsFiltered"   =>     $this->Offset_category_Model->get_filtered_data($active),  
            "data"              =>     $data  
        );
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($output));
    }

}