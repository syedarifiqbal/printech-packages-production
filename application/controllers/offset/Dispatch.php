<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Dispatch extends MY_Controller
{
    public $allowed_roles = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model([
            'offset/Offset_dc_model',
            'offset/Offset_dc_details_model',
        ]);
    }

    # Methodes
    /**************************************************
     *                    ADD DC.
     **************************************************/

    public function index($disabled = 0)
    {
        $this->set_data('title', 'Delivery Challan List | Edit | Delete.!');
        $this->set_data('page_title', 'Delivery Challan Lists');
        $this->set_data('table_title', 'Delivery Challan <small>View, Edit, Delete from the List</small>');
        $this->set_data('session', $this->session->all_userdata());
        $this->load->view('offset/offset/dispatch/list', $this->get_data());
    }

    public function save($id = false)
    {
        $record = new Offset_dc_Model();
        $this->set_data('selected_jobs', []);
        
        // Get and send form data if this is an update request check by record ID provided
        if ($id) {
            $record->load($id);
            $this->set_data('selected_jobs', $this->Offset_dc_details_model->get_dc_items($id));
        }
        $this->set_data('record', $record);
        
        // Check if Form submitted and procced form and insert record
        if (isset($_POST['submit'])) {

            // Validated Input Fields when submitting the form.
            $this->validate_fields($id);

            // Procced the form if validation passes
            if ($this->form_validation->run() == true) {
                // Populate object with input values
                foreach ($this->input->post('data') as $key => $value) {
                    $record->{$key} = $value;
                }
                // Populate Dates with database format
                $record->date = dbDate($this->input->post('data[date]'));
                $record->{$id ? 'updated_by' : 'created_by'} = $this->session->userdata('user_id');
                $record->{$id ? 'updated_time' : 'created_time'} = dbTime();

                $this->db->trans_start();
                // Save Record and get ID if new Record otherwise affected rows.
                $inserted_id = $record->save();
                $id = $id ? $id : $inserted_id;

                $this->add_dc_items($id);
                $this->db->trans_complete();

                // Check if there is any database error and flash message and redirect.
                if ($error_code = $this->db->error()['code']) {
                    set_flash_message(ERROR, "Failed with Error code: $error_code.");
                } elseif ($id) {
                    set_flash_message(SUCCESS, $id ? 'Record updated successfully.' : 'Record added successfully.');
                    redirect(site_url('offset/dispatch/'));
                } // end if else database error or not.

            } // end form validation
        }

        // send data to view
        $this->set_data('roles', $this->allowed_roles);
        $this->set_data('site_title', $id ? 'Edit Sale Order' : 'Add New Sale Order');
        $this->load->view('offset/offset/dispatch/form', $this->get_data());
    }

    /**************************************************
     *           ADD ORDER WASTAGE DETAILS.
     **************************************************/

    public function validate_fields($id)
    {
        $this->form_validation->set_rules('data[date]', 'Dispatch Date', 'required');
        $this->form_validation->set_rules('data[challan_no]', 'Chanllan no.', 'required');
        $this->form_validation->set_rules('data[gatepass_no]', 'Gate pass no.', 'required');

        if( $this->input->post('items') ){
            foreach ($this->input->post('items') as $key => $item) {
                $this->form_validation->set_rules("items[$key][job_code]", 'Job code.', 'required');
                $this->form_validation->set_rules("items[$key][weight]", 'Weight.', 'required');
                $this->form_validation->set_rules("items[$key][pcs]", 'PCS.', 'required');
                $this->form_validation->set_rules("items[$key][cartons]", 'Cartons.', 'required');
            }
        }
    }

    private function add_dc_items($id)
    {
        $this->Offset_dc_details_model->deleteWhere(['dc_id'=>$id]);
        foreach ($this->input->post('items') as $data) {
            $item = new Offset_dc_details_model();
            $item->dc_id        = $id;
            $item->job_code     = $data['job_code'];
            $item->weight       = $data['weight'];
            $item->pcs          = $data['pcs'];
            $item->cartons      = $data['cartons'];
            $item->description  = $data['description'];
            $item->save(1);
        }

    }

    /**************************************************
     *                    =SALE ORDER LIST.
     **************************************************/

    public function list_json($hold = 0)
    {
        $hold = $hold ? 0 : 1;
        $fetch_data = $this->Offset_dc_model->make_datatables($hold);
        $data = array();
        foreach ($fetch_data as $row) {
            $sub_array = array();
            $sub_array[] = anchor(site_url('offset/dispatch/save/' . $row->id), $row->id . ' <i class="fa fa-pencil"></i>');
            $sub_array[] = $row->job_code;
            $sub_array[] = pkDate($row->date);
            $sub_array[] = anchor(site_url('offset/dispatch/show/'.$row->id), $row->job_name, ' data-remote="false" data-toggle="modal" data-target="#myModal" class=""');
            $sub_array[] = $row->po_num;
            $sub_array[] = $row->weight;
            $sub_array[] = $row->pcs;
            $sub_array[] = $row->cartons;
            $actions = '<div class="btn-group">' .
                '<button data-toggle="dropdown" class="dropdown-toggle btn btn-icon-toggle btn-danger btn-sm">
                        <i class="fa fa-ellipsis-h "></i>
                    </button>
                    <ul class="dropdown-menu">';
            if (User_Model::hasAccess("editOffsetSaleOrder")):
                $actions .= '<li>' . anchor(site_url('offset/dispatch/save/' . $row->id), '<i class="fa fa-pencil"></i> Edit') . '</li>';
                $actions .= '<li>' . anchor(site_url('offset/dispatch/delete/' . $row->id), '<i class="fa fa-trash"></i> Delete', 'class="delete-entry"') . '</li>';
            endif;

            $actions .= '</ul>' .
                '</div>';
            $sub_array[] = $actions;
            $data[] = $sub_array;
        }
        $output = array(
            "draw" => intval($_POST["draw"]),
            "recordsTotal" => $this->Offset_dc_model->get_all_data($hold),
            "recordsFiltered" => $this->Offset_dc_model->get_filtered_data($hold),
            "data" => $data,
        );
        // echo sprintf("<script> console.log('%s'); </script>", addslashes($this->Offset_group_model->get_all_data($active)));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($output));
    }

    /**************************************************
     *                DELETING =SALE ORDER.
     **************************************************/

    public function delete($id = null)
    {
        if ($id) {
            $this->load->model('offset/Offset_dc_Model');
            $so = new Offset_dc_Model();
            $so->load($id);

            if ($so->job_code) {
                $so->delete();
                echo "Successfully Deleted";
            } else {
                echo "<h1>Sorry No Record Found.!</h1>";
            }

        } else { // END IF ID PRAMETER IS SET
            echo "<h1>Please Follow Given Link.!</h1>";
        }

    } // END DELETE CATEGORY FUCTION

    /**************************************************
     *                    =sale order.
     **************************************************/

    public function show($id)
    {
        if (User_Model::hasAccess('viewOffsetDC')) {

            if (isset($id)) {

                $items = $this->Offset_dc_model->get_details_by_id($id);
                $record = $items[0];
                
                $pdf = new FPDF();
                $pdf->AddPage();

                $pdf->SetFont('Arial', 'B', 16);
                $pdf->Cell(0, 10, "Printech Packages (PVT). LTD.", 0, 1, "C");
                $pdf->SetFont('Arial', 'U', 14);
                $pdf->Cell(0, 10, "Delivery Challan", 0, 1, "C");
                
                $left = [
                    "Delivered to: " => $record->customer_name,
                    "" => "",
                    " " => "",
                    "  " => ""
                ];
                
                $right = [
                    "DC number: " => substr("000000$record->id", -5),
                    "Date: " => pkDate($record->date),
                    "Job code: " => $record->job_code,
                    "Po number: " => $record->po_num,
                ];
                
                $pdf->SetFont('Arial', '', 8);
                $i = 0;
                $keys = array_keys($right);
                foreach ($left as $key => $value) {
                    $pdf->Cell(25, 8, $key, 1, 0);
                    $pdf->Cell(40, 8, $value, 1, 0);
                    $pdf->Cell(80, 8, "", 0, 0);
                    $pdf->Cell(20, 8, $keys[$i], 1, 0);
                    $pdf->Cell(25, 8, $right[$keys[$i]], 1, 1);
                    $i++;
                }
                $pdf->Ln(10);

                $pdf->SetFont('Arial', 'B', 12);
                $pdf->SetFillColor(205);
                // $pdf->SetDrawColor(245);

                $pdf->Cell(0, 10, "Goods Description", 1, 1, "C", 1);
                $x1 = $pdf->GetX();
                $y1 = $pdf->GetY();
                $h = 8;
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(10,$h,"S. no", 1,0,"C");
                $pdf->Cell(115,$h,"Item Name", 1,0 ,"C");
                $pdf->Cell(25,$h,"No. Cartons", 1,0,"C");
                $pdf->Cell(20,$h,"Weight", 1,0,"C");
                $pdf->Cell(20,$h,"No. PCS", 1,1,"C");

                $pdf->SetFont('Arial', '', 8);
                $i=0;
                foreach ($items as $item) {
                    $i++;
                    $pdf->Cell(10,$h,$i, "TLF",0,"C");
                    $pdf->Cell(115,$h,$item->job_name, 1,0);
                    $pdf->Cell(25,$h,$item->cartons, "TLR",0);
                    $pdf->Cell(20,$h,$item->weight, "TLR",0);
                    $pdf->Cell(20,$h,$item->pcs, "TLR",1);


                    $pdf->Cell(10,$h,"", "LR", 0,"C");
                    $pdf->MultiCell(115, $h, $item->description,1);
                    // $pdf->Cell(25,$h,'', "TLR",0);
                    // $pdf->Cell(20,$h,'', "TLR",0);
                    // $pdf->Cell(20,$h,'', "TLR",1);
                }
                $pdf->Cell(0,0,"", "T",1);
                
                $x2 = $pdf->GetX();
                $y2 = $pdf->GetY();

                $pdf->Line($x1, $y1, $x2, $y2);
                $pdf->Line(200, $y1, 200, $y2);
                $pdf->Line(180, $y1, 180, $y2);
                $pdf->Line(160, $y1, 160, $y2);
                
                $pdf->setXY(15, 275);
                $pdf->SetAutoPageBreak(false, $margin = 0);
                $pdf->SetFont('courier', 'U', 8);
                $pdf->cell(30, 10, 'Print time: ' . " (" . created_time($record->created_time) . ")");
                $pdf->SetFont('Arial', '', 10);
                $pdf->cell(80, 10, '');
                $pdf->cell(30, 10, 'Received : _______________________');
                $pdf->Ln();
                // $pdf->setX(110);

                $fileName = 'delivery_challan_' . $record->id;
                (isset($_GET['download']) && $_GET['download'] == 'true') ? $pdf->Output($fileName . '.pdf', 'D') : $pdf->Output();

            } // if id isset

        } else { // if roll exist
            echo "Your are not allowed for this view";
        }

    } // show()

}
