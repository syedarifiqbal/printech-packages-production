<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Material extends MY_Controller
{
    public $allowed_roles = array();

    function __construct()
    {
        parent::__construct();
        $this->load->model([
            'offset/Offset_material_model',
            'offset/Offset_category_model',
            'offset/Offset_group_model',
            'offset/Offset_material_categories_model',
            'offset/Offset_material_groups_model'
        ]);
    }

    function index($disabled=0)
    {
        $this->set_data( 'title', 'Material List | Edit | Delete.!' );
        $this->set_data('page_title', 'Material Lists' );
        $this->set_data('table_title', 'Material <small>View, Edit, Delete, Disable from the List</small>' );
        $this->set_data( 'session', $this->session->all_userdata() );
        // $this->set_data( 'active_record', $this->Offset_material_model->getWhere(['active'=>1]) );
        // $this->set_data( 'inactive_record', $this->Offset_material_model->getWhere(['active'=>0]) );
        $this->load->view('offset/offset/material/list', $this->get_data() );
    }

    public function save($id=false)
    {
        $this->set_data( 'title', 'Add new Material | Offset - Printech Packages');
        $this->set_data('session', $this->session->all_userdata());
        $this->set_data('page_title', 'Materials');
        $this->set_data( 'categories', $this->Offset_category_model->get_dropdown_lists(false) );
        $this->set_data( 'selected_categories', $this->Offset_material_model->get_categories_by_material_id($id) );
        $this->set_data( 'groups', $this->Offset_group_model->get_dropdown_lists(false) );
        $this->set_data( 'selected_groups', $this->Offset_material_model->get_groups_by_material_id($id) );

        $record = new Offset_material_model();
        if( $id ){ $record->load($id); }
        $this->set_data('record', $record);

        if (isset($_POST['submit'])) {
            
            $this->validate_fields($id);
            
            if ( $this->form_validation->run() == TRUE ) {

                foreach ($this->input->post('data') as $key => $value) {
                    $record->{$key} = $value;
                }
                $record->{$id?'updated_by':'created_by'} = $this->session->userdata('user_id');
                
                $this->db->trans_start();
                $inserted_id = $record->save();
                $id = $id? $id: $inserted_id;
                
                $this->add_categories($id);
                $this->add_groups($id);
                
                $this->db->trans_complete();

                if($this->db->trans_status() === FALSE){
                    $error_code = $this->db->error()['code'];
                    set_flash_message(ERROR, "Failed with Error code: $error_code.");
                }elseif ($id) {
                    set_flash_message(SUCCESS, $id?'Record updated successfully.':'Record added successfully.');
                    redirect(site_url('offset/material/'));
                } // if category inserted
                
                // x($this->db->last_query());

            } // end form validation

        } // if isset post

        $this->set_data('roles', $this->allowed_roles);
        $this->set_data('site_title', $id?'Edit Category':'Add New Category');
        $this->load->view('offset/offset/material/form', $this->get_data());

    } // Add Category

    function add_categories($material_id)
    {
        $this->Offset_material_categories_model->deleteWhere(['material_id'=>$material_id]);
        $this->load->model('Offset_material_categories_model');
        foreach ($this->input->post('categories') as $category_id) {
            $c = new Offset_material_categories_model();
            $c->category_id = $category_id;
            $c->material_id = $material_id;
            $c->save();
        }
    }

    function add_groups($material_id)
    {
        $this->Offset_material_groups_model->deleteWhere(['material_id'=>$material_id]);
        $this->load->model('Offset_material_groups_model');
        foreach ($this->input->post('groups') as $group_id) {
            $c = new Offset_material_groups_model();
            $c->group_id = $group_id;
            $c->material_id = $material_id;
            $c->save();
        }
    }

    function change_activation($id, $active)
    {
        $record = new Offset_material_model();
        $record->load($id);
        $record->active = $active;
        $affactedRows = $record->save();
        if($error_code = $this->db->error()['code']){
            set_flash_message(ERROR, "Failed with error code: $error_code.");
        }elseif ($affactedRows) {
            set_flash_message(SUCCESS, $active?'Record activated successfully.':'Record inactivated successfully.');
            redirect(site_url("offset/material/index/$active"));
        } // if category inserted
    }

    function validate_fields($id)
    {
        if ($id) {
            $this->form_validation->set_rules('data[name]','Material name','required|callback_custom_name_check['.$id.']|min_length[6]');
        }else{
            $this->form_validation->set_rules('data[name]','Material name','required|is_unique[offset_material.name]|min_length[6]');
        }
        $this->form_validation->set_rules('data[unit]','Unit','required');
        $this->form_validation->set_rules('data[rate]','List Price','required');
        $this->form_validation->set_rules('data[gsm]','GSM','required');
        $this->form_validation->set_rules('data[width]','Width','required');
        $this->form_validation->set_rules('data[height]','Height','required');
        $this->form_validation->set_rules('categories[]','Category','required');
        $this->form_validation->set_rules('groups[]','Group','required');
    }

    // Delete Category
    public function delete($id=null)
    {
        if ($id) {
            $record = new Offset_material_model();
            $record->load($id);
            if ($record->id){
                $record->delete();
                if($this->db->error()['code']){
                    set_flash_message(ERROR, "Failed with Error code: $error_code.");
                }else{
                    set_flash_message(SUCCESS, $record->name.' Deleted Successfully.');
                    redirect(site_url("offset/material/"));
                }
            }else{
                set_flash_message(ERROR, 'Sorry No Record Found.!');
                redirect(site_url("offset/material/"));
            }

        } // END IF ID PRAMETER IS SET
        else
            echo "<h1>Please Follow Given Link.!</h1>";
    } // END DELETE MATERIAL FUCTION

    public function custom_name_check($name,$id){
        $this->db->where('name', $name);
        $this->db->where('id !=', $id);
        $users = $this->db->get('offset_material');
        if($users->row()){
            $this->form_validation->set_message('custom_name_check', 'The {field} must be unique. This is already in use.');
            return false;
        }else{
            return true;
        }
    }

    public function list_json($active=false)
    {
        $active = $active?1:0;
        $fetch_data = $this->Offset_material_model->make_datatables($active);  
        $data = array();
        foreach($fetch_data as $row)  
        {
            $activation = $row->active? 0: 1;
            $activation_text = $row->active? "Inactivate": "Activate";
            $activation_icon = $row->active? "lock": "unlock";
            $activation_class = $row->active? "inactivate": "activate";
            $sub_array=array();
            
            $sub_array[] = anchor(site_url('offset/material/save/'.$row->id), $row->id.' <i class="fa fa-pencil"></i>');
            $sub_array[] = $row->name;
            $sub_array[] = $row->width;
            $sub_array[] = $row->height;
            $sub_array[] = $row->gsm;
            $sub_array[] = $row->rate;
            $sub_array[] = $row->groups;
            $sub_array[] = $row->categories;
            $sub_array[] = '<div class="btn-group">'.
                    '<button data-toggle="dropdown" class="dropdown-toggle btn btn-icon-toggle btn-danger btn-sm">
                        <i class="fa fa-ellipsis-h "></i>
                    </button>
                    <ul class="dropdown-menu">'.
                        '<li>'. anchor(site_url('offset/material/save/'.$row->id),'<i class="fa fa-pencil"></i> Edit') . '</li>'.
                        '<li>'. anchor(site_url("offset/material/change_activation/$row->id/$activation"),"<i class='fa fa-$activation_icon'></i> $activation_text", "class='$activation_class'") . '</li>'.
                        '<li>'. anchor(site_url('offset/material/delete/'.$row->id),'<i class="fa fa-trash"></i> Delete', 'class="delete-entry"') . '</li>'.
                    '</ul>'.
                '</div>';
            $data[] = $sub_array;
        }
        $output = array(  
            "draw"              =>     intval($_POST["draw"]),  
            "recordsTotal"      =>     $this->Offset_material_model->get_all_data($active),  
            "recordsFiltered"   =>     $this->Offset_material_model->get_filtered_data($active),
            "data"              =>     $data  
        );
        // echo sprintf("<script> console.log('%s'); </script>", addslashes($this->Offset_group_model->get_all_data($active)));
        $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($output));
    }

}