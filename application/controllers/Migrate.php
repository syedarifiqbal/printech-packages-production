<?php 

/**
* DATABASE MIGRATION CLASS
*/
class Migrate extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		if ($this->input->is_cli_request() == FALSE) {
			show_404();
		}

		$this->load->dbforge();
		$this->load->library('migration');
	}

	function latest()
	{
		$this->migration->latest();
		// if ($this->migration->current() === FALSE)
  //       {
  //           show_error($this->migration->error_string());
  //       }

		echo $this->migration->error_string() . PHP_EOL;
		echo "This latest function ran.";
	}

	function reset()
	{
		$this->migration->version(0);
		echo $this->migration->error_string() . PHP_EOL;
	}
}

 ?>