<?php if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
 *
 */
class User extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_Model');
        $this->load->helper('form');

        $data['session'] = $this->session->all_userdata();

    }

    public function index()
    {
        $data['title'] = 'Users List | New User.';
        $data['session'] = $this->session->all_userdata();

        $data['roles'] = $this->allowed_roles;

        $data['users'] = $this->Users->get();
        $data['max'] = $this->Users->max();
        $this->load->view('includes/header', $data);
        $this->load->view('user/User', $data);
        $this->load->view('includes/footer', $data);
    }

    public function login()
    {
        $data['session'] = $this->session->all_userdata();

        if (isset($_POST['submit'])) {
            // var_dump($_POST);
            $this->load->library('form_validation');

            $config = array(
                array(
                    'field' => 'user_name',
                    'label' => 'User Name',
                    'rules' => 'required|trim'
                ),
                array(
                    'field' => 'hashed_password',
                    'label' => 'Password',
                    'rules' => 'required'
                )
            );
            $this->form_validation->set_rules($config);

            if ($this->form_validation->run()) {
                $user_name = $this->input->post('user_name', true);
                $password = $this->input->post('hashed_password', true);
                $user = $this->User_Model->authenticate($user_name, $password);

                if ($user) {
                    $this->session->set_userdata('logged_in', TRUE);
                    $this->session->set_userdata('user_id', $user['user_id']);
                    $this->session->set_userdata('user_name', $user['user_name']);
                    $this->session->set_userdata('avater_path', $user['avater_path']);
                    $this->session->set_userdata('role_id', $user['role_id']);
                    $this->session->set_userdata('section', null);
                    redirect(base_url() . "index.php/Dashboard/section/");
                } else {
                    $data['login_error'] = 'User Name / Password Incorrect.';
                }

            }

        }

        $data['title'] = site_title() . ' | Log In To System.';
        // $this->load->view('includes/header', $data);
        $this->load->view('user/login', $data);
        // $this->load->view('includes/footer', $data);

    } // user login function

    public function logout()
    {
        $this->session->unset_userdata([
            'user_id' => '',
            'user_name' => '',
            'logged_in' => false
        ]);
        // unset($_SESSION['database']);
        redirect(base_url() . 'index.php/user/login');
    }

    public function add()
    {
        $data['title'] = 'Add User | User List.';
        $data['session'] = $this->session->all_userdata();

        $data['roles'] = $this->allowed_roles;

        $data['user'] = $this->Users->get();
        $data['max'] = $this->Users->max();

        $config = array(
            array(
                'field' => 'user_name',
                'label' => 'User Name',
                'rules' => 'trim|required|is_unique[users.user_name]'
            ),
            array(
                'field' => 'hashed_password',
                'label' => 'Password',
                'rules' => 'required|min_length[5]'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[hashed_password]'
            ),
            array(
                'field' => 'email',
                'label' => 'Emai Address',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'cell',
                'label' => 'Mobile Number',
                'rules' => 'required|numeric'
            )
        );

        $this->form_validation->set_rules($config);

        if (!$this->form_validation->run()) {
            $data['error'] = true;

            $this->load->view('includes/header', $data);
            $this->load->view('user/User', $data);
            $this->load->view('includes/footer', $data);
        } else {
            foreach ($_POST as $key => $value) {
                if ($key == 'confirm_password' || $key == 'submit') {
                    continue;
                } elseif ($key == 'hashed_password') {
                    $this->Users->hashed_password = password_hash($value, PASSWORD_BCRYPT, ['cost' => 12]);
                } else {
                    $this->Users->{$key} = $value;
                }
            }
            $this->Users->added_by = 1;

            $this->Users->save();
            redirect(base_url() . 'index.php/user/set_permissions/' . $data['max']);

        }

    }

    /**
     * USER DELETE FUNCTION
     */

    public function delete_user($user_id)
    {
        $data['session'] = $this->session->all_userdata();

        // DELETE ASSOSIATIVE ROLES TO THIS USER
        $this->load->model('User_role');
        $delete_role = new User_role();
        $delete_role->user_id = $user_id;
        $delete_role->delete();

        $x = new Users();
        $x->load($user_id);
        $x->delete();
        redirect(base_url() . 'index.php/user');
    }


    /**
     * USER EDIT FUNCTION
     */

    public function update_user($user_id)
    {
        // $this->load->model('Users');
        // $this->load->helper(['form','url']);
        $data['session'] = $this->session->all_userdata();

        $data['title'] = 'Add User | User List.';

        $data['roles'] = $this->allowed_roles;

        $data['users'] = $this->Users->get();

        $user_single = new Users();
        $user_single->load($user_id);
        $data['user'] = $user_single;
        $this->load->library('form_validation');
        $config = array(
            array(
                'field' => 'user_name',
                'label' => 'User Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'email',
                'label' => 'Emai Address',
                'rules' => 'required|valid_email'
            ),
            array(
                'field' => 'cell',
                'label' => 'Mobile Number',
                'rules' => 'required|numeric'
            )
        );
        if ($this->input->post('hashed_password')) {
            $config[] =
                array(
                    'field' => 'hashed_password',
                    'label' => 'Password',
                    'rules' => 'required|min_length[5]'
                );
            $config[] =
                array(
                    'field' => 'confirm_password',
                    'label' => 'Password',
                    'rules' => 'required|matches[hashed_password]'
                );
        }

        $this->form_validation->set_rules($config);

        if (!$this->form_validation->run()) {
            $data['error'] = true;

            $this->load->view('includes/header', $data);
            $this->load->view('user/Update_user', $data);
            $this->load->view('includes/footer', $data);

        } else {

            $u = new Users();
            $u->load($user_id);

            $u->user_id = $this->input->post('user_id');
            $u->user_name = $this->input->post('user_name');
            $u->email = $this->input->post('email');
            $u->cell = $this->input->post('cell');
            $u->active = $this->input->post('active');
            $u->address = $this->input->post('address');
            $u->updated_by = $this->session->userdata['user_id'];

            if ($u->save()) {
                // echo $this->db->last_query();
                redirect(base_url() . 'index.php/user');
            }
        }

    }

    /**
     * SET USER PERMISSIONS
     */
    public function set_permissions($user_id)
    {
        $this->load->model('Roles');
        $this->load->model('User_role');

        $data['title'] = 'Set User Permissions.';
        $data['user_id'] = $user_id;
        $data['session'] = $this->session->all_userdata();
        $data['permissions'] = $this->Roles->get();

        $data['allowed'] = $this->User_role->load_many($user_id);
        $allowed = [];
        foreach ($data['allowed'] as $allow) {
            $allowed[] = $allow->role_id;
        }
        $data['allowed'] = $allowed;

        $this->load->view('includes/header', $data);
        $this->load->view('user/permissions', $data);
        $this->load->view('includes/footer', $data);

        if ($this->input->post('submit')) {
            $user_id = $this->input->post('user_id');
            $roles = $this->input->post('roles');

            $delete_role = new User_role();
            $delete_role->user_id = $user_id;
            $delete_role->delete();

            foreach ($roles as $role) {
                $insert_role = new User_role();
                $insert_role->user_id = $user_id;
                $insert_role->role_id = $role;
                $insert_role->save();
            }
            redirect(base_url() . 'index.php/user');
        }

    }

}


?>