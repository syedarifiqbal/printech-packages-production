<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Utils extends CI_Controller
{

    function codeigniter_backup()
    {
        $this->load->dbutil();
        $prefs = array(
            'format'    => 'zip',                         // gzip, zip, txt
            'filename'  => 'strip_payment_db_backup.sql', // File name - NEEDED ONLY WITH ZIP FILES
            'newline'   => "\n"                           // Newline character used in backup file
        );

        $backup = $this->dbutil->backup($prefs);

        // Load the file helper and write the file to your server
        $this->load->helper('file');
        write_file('/mybackup.zip', $backup);

        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        force_download('mybackup.zip', $backup);
    }

    function get_backup(){

        $sql = "";

        // setting meta deta for sql file
        $sql .= "\n\n-- Host: " . $_SERVER["SERVER_NAME"];
        date_default_timezone_set("Asia/Karachi");
        $sql .= "\n-- Generation Time: " . date('g:ia \o\n l jS F Y');
        $sql .= "\n-- PHP Version: " . phpversion();

        $sql .= "\n\nSET FOREIGN_KEY_CHECKS=0;";
        $sql .= "\n\nSET time_zone = \"+05:00\";";


        $sql .= "\n\n--";
        $sql .= "\n-- Database: `".$this->db->database."`";
        $sql .= "\n--\n\n";

        // backing up store procedure
        $procedures = $this->db->query('SHOW PROCEDURE STATUS WHERE Db = "'.$this->db->database .'"');
        $procedures = $procedures->result();

        $sql .= "DELIMITER $$\n\n--\n-- Procedures\n--\n\n\n";

        foreach ($procedures as $procedure) {
            $pro = $this->db->query('SHOW CREATE PROCEDURE '.$procedure->Name);
            $pro = $pro->row();
            $sql .= "DROP PROCEDURE IF EXISTS `$procedure->Name`$$\n";
            $sql .= $pro->{'Create Procedure'};
            $sql .= "$$\n\n";
        }
        $sql .= "\n\nDELIMITER ;";

        // backing up database tables
        $tables = $this->db->query('SHOW TABLES');
        $tables = $tables->result();

        $sql .= "\n\n--\n-- Tables\n--\n\n\n";

        if (!empty($tables) && $tables && isset($tables)) {
            foreach ($tables as $table) {
                $tName = $table->{'Tables_in_'.$this->db->database};

                $sql .= "DROP TABLE IF EXISTS `" . $tName . "`;";

                $cols = $this->db->query('SHOW CREATE TABLE `' . $tName . '`');
                $cols = $cols->row();
                $sql .= "\n\n" . $cols->{'Create Table'} . ";\n\n";


                $rows = $this->db->query("SELECT * FROM  `" . $tName . '`');
                $rows = $rows->result();

                // backing up table data
                if( !empty($rows) ){

                    $sql .= "INSERT INTO `" . $tName . "` VALUES \n";
                    $string_rows = [];
                    foreach ($rows as $row) {
                        $clean_cols = [];
                        foreach ($row as $col) {
                            $clean_cols[] = "'" . addslashes($col) . "'";
                        }
                        $string_rows[] = '(' . join(',', $clean_cols) . ')';
                    }

                    $sql .= join(",\n", $string_rows);
                    $sql .= ";\n\n";
                }
//                "CREATE TABLE `contact` (   `add_id` int(11) NOT NULL AUTO_INCREMENT,   `customer_id` int(11) unsigned NOT NULL,   `address` varchar(255) DEFAULT NULL,   `email` varchar(255) DEFAULT NULL,   `cell` varchar(255) DEFAULT NULL,   PRIMARY KEY (`add_id`),   KEY `customer_id` (`customer_id`),   CONSTRAINT `contact_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`customer_id`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1	Error Code: 1005. Can't create table `check_for_production`.`contact` (errno: 150 "Foreign key constraint is incorrectly formed")	0.578 sec";

            }
        }

        $sqlFile = 'sql_backup-' . str_replace(':', '.', date('g:i a \o\n l jS F Y')) . '.sql';
        $f = fopen($sqlFile, 'w+');
        fwrite($f, $sql);
        fclose($f);

//        check(base_url($sqlFile));
//        check(FCPATH);
//
//        check($sql);
//        return;

        if(net_on()){

            $this->load->library('email');
            $to = 'arifiqbal@outlook.com';
            $this->email->from('arif.iqbal@printechpackages.com','Printech Packages Software');
            $this->email->to($to, 'Arif Iqbal');

            $this->email->subject('PT DATABASE BACKUP ON '.date('g:i a l jS F Y'));
            $this->email->message("Printech packages software database backup .sql file Automatically send by the system.");
            $this->email->attach(FCPATH.$sqlFile);

            if ($this->email->send()){
                echo "<div><p>Successfully Backedup and email to $to on ".date('g:i a l jS F Y').'</p></div>';
                unlink(FCPATH.$sqlFile);
            }else{
                echo "<div><p>There may some problem while sending email to $to </p></div>";
            }

        }else{
            echo "<div><p>Make sure you have working internet connection in order to send as email but don't worry DATA was backed up on ".date('g:i a l jS F Y').' at <strong>'.substr(__DIR__,0,strlen(__DIR__)-strlen('application\controllers')).'</strong></p></div>';
        }
        echo "<style>
                div {
                    padding: 25px;
                    background: rgba(150, 255, 143, 0.88);
                    margin: 20px auto;
                    width: 650px;
                    color: #008223;
                    border-radius: 5px;
                    box-shadow: 4px 5px 10px -5px #008223;
                    font-size: 18px;
                }

                html,body {
                    margin: 0;
                    padding: 0;
                }
            </style>";


    }

    function get_backup_procedure(){

//        echo 'SHOW CREATE PROCEDURE printing_list';
        $procedures = $this->db->query('SHOW PROCEDURE STATUS WHERE Db = "'.$this->db->database .'"');
        $procedures = $procedures->result();
        $sql = "DELIMITER $$\n\n--\n-- Procedures\n--\n\n\n";
        foreach ($procedures as $procedure) {
            $pro = $this->db->query('SHOW CREATE PROCEDURE '.$procedure->Name);
            $pro = $pro->row();
            $sql .= "DROP PROCEDURE IF EXISTS `$procedure->Name`$$\n";
            $sql .= $pro->{'Create Procedure'};
            $sql .= "$$.\n\n";
        }
        $sql .= "\n\nDELIMITER ;";
        check($sql);
    }


    public function imap()
    {
        $inbox = $this->load->library('Imap_Email_Reader');
        echo "<pre>";
        print_r($inbox);
        echo "</pre>";
    }


    public function test()
    {
        $possible_values_or_header = array(
            'inv.1','inv.2','inv.3','inv.4','inv.5','inv.6','inv.7','inv.8','inv.9','inv.10'
            );

        //dispay header
        echo "<tr>";
        foreach ($possible_values_or_header as $header) {
            echo sprintf("<td>%s</td>",$header);
        }
        echo "</tr>";

        $trainee = array( new t(), new t(), new t() );
        echo "<table>";

        foreach ($trainee as $traine):
            echo "<tr>";
            $tds = get_object_vars($traine);
            foreach ($possible_values_or_header as $value) {
                echo sprintf("<td>%s</td>", in_array($value, $tds)? $traine->{$tds[$value]}:'');
            }
            echo "</tr> ";
        endforeach;
        echo "</table>";        


        // foreach ($trainee as $traine):
        //      echo "<tr>";
        //     $tds = get_object_vars($traine);
        //     foreach ($tds as $property => $value) {
        //         echo sprintf("<td>%s</td>",$traine->{$property});
        //     }
        //      echo "</tr> ";
        // endforeach;
        // echo "</table>";

    }

    public function test2()
    {
        $header = array(
                'Firstname' => 'First Name',
                'Lastname' => 'Last Name',
                'Organization' => 'Organization',
                'Designation' => 'Designation',
                'Email' => 'Email',
                'Address' => 'Address',
                'City' => 'City',
            );
        echo "<table>";

        echo "<tr>";
        foreach ($header as $head) {
            echo sprintf("<th>%s</th>",$head);
        }
        echo "</tr>";

        $customResult = array();
        $column = 1;
        $row = 0;
        foreach($candidates as $candidate)
        {
            $customResult[$row][$candidate->field_name] = $candidate->field_value;
            if(++$column == 7){
                $row++;
            }
        }

        foreach ($customResult as $result) {
            echo "<tr>";
            foreach ($header as $field => $value) {
                echo sprintf("<td>%s</td>",$result[$field]);
            }
            echo "</tr>";
        }

        echo "</table>";

    }


}
class t
{
    public $t1 = 1;
    public $t2 = 2;
}