<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sharing extends MY_Controller {

	public $allowed_roles = array();

	# Methodes 
	public $icons;

	function __construct(){
		parent::__construct();
		$this->icons = array('.jpg'=>false,
								'.jped'=>false,
								'.png'=>false,
								'.xls'=>'ExcelFileIcon.png',
								'.xlsx'=>'ExcelFileIcon.png',
								'.doc'=>'WordFileIcon.png',
								'.docx'=>'WordFileIcon.png',
								'.pdf'=>'pdficon.png'
								);

		$this->load->model('Sharing_Model');

		$config['upload_path'] = './sharing_on_server/';
		$config['allowed_types'] = 'psd|ai|gif|jpg|png|xlsx|xls|pdf|doc|docx';
		$config['max_size']	= '25600'; // 25 MB
		// $config['max_width']  = '1024';
		// $config['max_height']  = '768';

		$this->load->library('upload', $config);
	}

	public function index()
	{
		$data['site_title'] = 'Sharing On Server.';
		$data['files'] = $this->Sharing_Model->get_current_user_file();

		$data['icons'] = $this->icons;

		$this->load->view('main/header',$data);
		$this->load->view('main/navigation',$data);
		$this->load->view('main/rightNavigation',$data);
		$this->load->view('main/topbar',$data);
		$this->load->view('sharing/index',$data);
		$this->load->view('main/footer',$data);
	}

	public function upload()
	{
		$this->load->helper(array('form', 'url'));
		$data['icons'] = $this->icons;

		if ( ! $this->upload->do_upload() )
		{
			var_dump($_FILES);
			// $error = array('error' => $this->upload->display_errors());
			$data['error'] = $this->upload->display_errors();
			$data['site_title'] = 'Sharing On Server.';
			$data['files'] = $this->Sharing_Model->get_current_user_file();

			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('sharing/index',$data);
			$this->load->view('main/footer',$data);
		}
		else
		{
			$data = $this->upload->data();
			$this->load->model('Sharing_Model');
			// var_dump($data);
			$sharing = new Sharing_Model();
			$sharing->id = $this->Sharing_Model->max();
			$sharing->file_name = $data['raw_name'] . $data['file_ext'];
			$sharing->size = $data['file_size'];
			$sharing->file_type = $data['file_ext'];
			$sharing->uploaded_by = $this->session->userdata('user_id');
			$sharing->uploaded_time = dbTime();
			// var_dump($sharing);
			if ( $sharing->save() ) {
				redirect(site_url('sharing'));
			}else{
				echo $this->upload->display_errors();
			}

		}
	}




}