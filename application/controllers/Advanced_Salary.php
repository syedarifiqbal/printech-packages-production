<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Advanced_Salary extends MY_Controller {

	/**************************************************
	*					=ADD VOCANCY.
	**************************************************/ 
	public function create()
	{
		$data['title'] = 'Create Adavanced Salary!';
		$data['session'] = $this->session->all_userdata();
		$this->db->trans_start();
		if ( $_POST ) {
			for ($i = 0; $i < count($this->input->post('name')); $i++) {
				$record = new Advanced_Model();
				$record->id = $this->Advanced_Model->max()+$i;
				$record->date = dbDate($this->input->post('date'));
				$record->department = $this->input->post('department');

				$record->name = $this->input->post('name')[$i];
				$record->father_name = $this->input->post('father_name')[$i];
				$record->code = $this->input->post('code')[$i];
				$record->designation = $this->input->post('designation')[$i];
				$record->amount = $this->input->post('amount')[$i];

				$record->created_by = $this->session->userdata('user_id');
				$record->created_time = dbTime();
				// $record->updated_by = $this->input->post('');
				// $record->updated_time = $this->input->post('');
				$record->save();
			} // end for loop

			if (!$this->db->trans_status()) {
					$this->db->trans_rollback();
					echo "Transiction Could not complete successfully.";
				}else{

					$this->db->trans_complete();
					if ($this->input->is_ajax_request()) {
						echo "Successfully Inserted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
						redirect(site_url('Advanced_Salary/show_list'));
					}
				}
		} // if post isset
		
		$data['roles'] = $this->allowed_roles;
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		if ($this->input->is_ajax_request()) {
			$this->load->view('HR/Advanced_Salary',$data);
		}else{
			$data['site_title'] = 'Create Advanced Salary.';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('HR/Advanced_Salary',$data);
			$this->load->view('main/footer',$data);
		}

	} // create()

	/**************************************************
	*					=UPDATE.
	**************************************************/ 
	public function edit($id=false)
	{
		if ($id) {
			$data['title'] = 'Update Advance Salary!';
			$data['session'] = $this->session->all_userdata();
			if ( $_POST ) {
				$record = new Advanced_Model();
				$record->load($id);
				$record->date = dbDate($this->input->post('date'));
				$record->department = $this->input->post('department');

				$record->name = $this->input->post('name');
				$record->father_name = $this->input->post('father_name');
				$record->code = $this->input->post('code');
				$record->designation = $this->input->post('designation');
				$record->amount = $this->input->post('amount');

				// $record->created_by = $this->session->userdata('user_id');
				// $record->created_time = dbTime();
				$record->updated_by = $this->session->userdata('user_id');
				$record->updated_time = dbTime();
				if ($record->save()) {
					if ($this->input->is_ajax_request()) {
						echo "Successfully Inserted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', 'Recored Submited Successfully.');
						redirect(site_url('Advanced_Salary/show_list'));
					}
				}
			}
			
			$data['roles'] = $this->allowed_roles;
			$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';
			$v = new Advanced_Model();
			$v->load($id);
			$data['record'] = $v;
			$data['update'] = true;

			if ($this->input->is_ajax_request()) {
				$this->load->view('HR/update_Advanced_Salary',$data);
			}else{
				$data['site_title'] = 'Add New Rewinding Entry.';
				$this->load->view('main/header',$data);
				$this->load->view('main/navigation',$data);
				$this->load->view('main/rightNavigation',$data);
				$this->load->view('main/topbar',$data);
				$this->load->view('HR/update_Advanced_Salary',$data);
				$this->load->view('main/footer',$data);
			}
		} // if id isset

	} // edit()


/**************************************************
*					=SHOW LIST.
**************************************************/ 
	public function show_list()
	{
		$data['title'] = 'Advanced Salary List | Edit | Delete.!';
		$data['session'] = $this->session->all_userdata();
		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$data['roles'] = $this->allowed_roles;
		$data['records'] = $this->Advanced_Model->get();

		if ($this->input->is_ajax_request()) {
			$this->load->view('user/user_list',$data);
		}else{
			$data['site_title'] = 'Software Users List!';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('HR/Advanced_Salary_List',$data);
			$this->load->view('main/footer',$data);
		}

	} // vocancy_list()


	public function delete($id=null)
	{
		if ($id) {
			
			$record = new Advanced_Model();
			$record->load($id);

			if ($record->id){
				$record->delete();
				if($this->db->_error_message()){
					echo "Can not delete this Record.!";
				}else{
					
					if ($this->input->is_ajax_request()) {
						echo "Successfully Deleted";
						return;
					}else{
						$this->session->set_flashdata('userMsg', $record->name.' Deleted Successfully.');
						redirect(site_url('Advanced_Salary/show_list'));
					}
				}
			}else{
				echo "<h1>Sorry No Record Found.!</h1>";
			}

		} // END IF ID PRAMETER IS SET
		else
		{
			echo "<h1>Please Follow Given Link.!</h1>";
		}

	} // END DELETE CUSTOMER FUCTION


	public function print_voucher()
	{
		if (isset($_GET['month']) && isset($_GET['department'])) {
			$advance = $this->db->query("SELECT * FROM advanced_salary
											WHERE department = '".$_GET['department']."'
											AND date_format(`date`,'%M-%Y') = '".$_GET['month']."'")->result();
			if ($advance) {
				$pdf = new fpdf();
				$pdf->SetMargins(15,20);
				foreach ($advance as $record) {
					$pdf->AddPage();
					
					$pdf->Image(base_url('assets/img/logo.jpg'),10,10,25);
					$pdf->SetFont('Arial','B',16);
					$pdf->Ln(20);
					$pdf->cell(0,10,"Printech Packages PVT. LTD",0,1,'C');
					$pdf->SetFont('Arial','BU',12);
					$pdf->cell(0,10,"SALARY ADVANCED FORM",0,0,'C');
					$pdf->Ln(20);

					$h = 7;
					$pdf->SetFont('Arial','',10);
					$pdf->cell(25,$h,'Date: ',0,0);
					$pdf->cell(52,$h,pkDate($record->date,'/'),1,0,'C');

					$pdf->cell(25,$h,'',0,0,'C');

					$pdf->cell(25,$h,'Amount: ',0,0,'C');
					$pdf->cell(52,$h,number_format($record->amount).'  ',1,0,'C');
					$pdf->Ln(12);

					$pdf->cell(25,$h,'Code: ',0,0);
					$pdf->cell(52,$h,$record->code,1,0,'C');

					$pdf->cell(25,$h,'',0,0,'C');

					$pdf->cell(25,$h,'Designation: ',0,0,'C');
					$pdf->cell(52,$h,$record->designation,1,0,'C');
					$pdf->Ln(12);

					$pdf->cell(25,$h,'Name: ',0,0);
					$pdf->cell(0,$h,$record->name,1,0);

					$pdf->cell(25,$h,'',0,0);
					$pdf->Ln(12);

					$pdf->cell(25,$h,'F.Name: ',0,0);
					$pdf->cell(0,$h,$record->father_name,1,0);
					$pdf->Ln(25);

					$pdf->cell(130,$h,'Sig. Department Manager/Head: ',0,0);
					$pdf->cell(50,$h,'Sig. Pro Manager',0,1);
					// $pdf->Ln(12);

					$pdf->cell(0,1,'','B',1); // draw border
					$pdf->Ln(12);

					$pdf->cell(35,$h,'Date of Appointment: ',0,0);
					$pdf->cell(50,$h,'',1,0);
					$pdf->cell(45,$h,'Salary @: ',0,0,'R');
					$pdf->cell(50,$h,'',1,0);
					$pdf->Ln(12);

					$pdf->cell(35,$h,'Attendance: ',0,0);
					$pdf->cell(50,$h,'',1,0);
					$pdf->cell(45,$h,'O.T Hrs: ',0,0,'R');
					$pdf->cell(50,$h,'',1,0);
					$pdf->Ln(12);

					$pdf->cell(0,$h,'Advance may be paid up to: ',0,1);
					$pdf->Ln(5);

					$pdf->cell(35,$h,'Sig. HR Manager: ',0,0);
					$pdf->cell(50,$h,'',1,0);
					$pdf->cell(45,$h,'Sig. Accounts Manager: ',0,0,'R');
					$pdf->cell(50,$h,'',1,0);
				}
				$pdf->output();
			}else{
				echo "Sorry No Entry Available!";
			}
		}else{
			$data['months'] = $this->db->query("SELECT
												  CONCAT(date_format(date,'%M'),'-',date_format(date,'%Y')) AS month
												FROM
												  `advanced_salary`
												GROUP BY date_format(date,'%M'),date_format(date,'%Y')
												ORDER BY date DESC")->result();
			if ($this->input->is_ajax_request()) {
				$this->load->view('HR/print_voucher',$data);
			}else{
				$data['roles'] = $this->allowed_roles;
				$data['site_title'] = 'Print Advanced Salary List!';
				$this->load->view('main/header',$data);
				$this->load->view('main/navigation',$data);
				$this->load->view('main/rightNavigation',$data);
				$this->load->view('main/topbar',$data);
				$this->load->view('HR/print_voucher',$data);
				$this->load->view('main/footer',$data);
			}
		}
	} // END DELETE CUSTOMER FUCTION


	public function view($id=null)
	{
		if ($id) {
			
			$record = new Advanced_Model();
			$record->load($id);

			$pdf = new FPDF();
			$pdf->AddPage();
			$pdf->SetMargins(15,50);
			$pdf->Image(base_url('assets/img/logo.jpg'),10,10,25);
			$pdf->SetFont('Arial','B',16);
			$pdf->Ln(20);
			$pdf->cell(0,10,"Printech Packages PVT. LTD",0,1,'C');
			$pdf->SetFont('Arial','BU',12);
			$pdf->cell(0,10,"SALARY ADVANCED FORM",0,0,'C');
			$pdf->Ln(20);

			$h = 7;
			$pdf->SetFont('Arial','',10);
			$pdf->cell(25,$h,'Date: ',0,0);
			$pdf->cell(52,$h,pkDate($record->date,'/'),1,0,'C');

			$pdf->cell(25,$h,'',0,0,'C');

			$pdf->cell(25,$h,'Amount: ',0,0,'C');
			$pdf->cell(52,$h,number_format($record->amount).'  ',1,0,'C');
			$pdf->Ln(12);

			$pdf->cell(25,$h,'Code: ',0,0);
			$pdf->cell(52,$h,$record->code,1,0,'C');

			$pdf->cell(25,$h,'',0,0,'C');

			$pdf->cell(25,$h,'Designation: ',0,0,'C');
			$pdf->cell(52,$h,$record->designation,1,0,'C');
			$pdf->Ln(12);

			$pdf->cell(25,$h,'Name: ',0,0);
			$pdf->cell(0,$h,$record->name,1,0);

			$pdf->cell(25,$h,'',0,0);
			$pdf->Ln(12);

			$pdf->cell(25,$h,'F.Name: ',0,0);
			$pdf->cell(0,$h,$record->father_name,1,0);
			$pdf->Ln(25);

			$pdf->cell(130,$h,'Sig. Department Manager/Head: ',0,0);
			$pdf->cell(50,$h,'Sig. Pro Manager',0,1);
			// $pdf->Ln(12);

			$pdf->cell(0,1,'','B',1); // draw border
			$pdf->Ln(12);

			$pdf->cell(35,$h,'Date of Appointment: ',0,0);
			$pdf->cell(50,$h,'',1,0);
			$pdf->cell(45,$h,'Salary @: ',0,0,'R');
			$pdf->cell(50,$h,'',1,0);
			$pdf->Ln(12);

			$pdf->cell(35,$h,'Attendance: ',0,0);
			$pdf->cell(50,$h,'',1,0);
			$pdf->cell(45,$h,'O.T Hrs: ',0,0,'R');
			$pdf->cell(50,$h,'',1,0);
			$pdf->Ln(12);

			$pdf->cell(0,$h,'Advance may be paid up to: ',0,1);
			$pdf->Ln(5);

			$pdf->cell(35,$h,'Sig. HR Manager: ',0,0);
			$pdf->cell(50,$h,'',1,0);
			$pdf->cell(45,$h,'Sig. Accounts Manager: ',0,0,'R');
			$pdf->cell(50,$h,'',1,0);
			$pdf->Ln(12);

			$pdf->output();

		} // END IF ID PRAMETER IS SET
		else
		{
			echo "<h1>Please Follow Given Link.!</h1>";
		}

	} // END DELETE CUSTOMER FUCTION



}