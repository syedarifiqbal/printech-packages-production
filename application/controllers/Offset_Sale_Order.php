<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Offset_Sale_Order extends MY_Controller
{

    public $allowed_roles = array();

    # Methodes

    /**************************************************
     *                    ADD Sale Order.
     **************************************************/

    public function add($id = false)
    {
        $this->load->model('Offset_Sale_Order_Model');
        $data['roles'] = $this->allowed_roles;
        $data['job_code'] = $this->Offset_Sale_Order_Model->max();
        if ($id) {
            $data['update'] = true;
            $sql = "SELECT so.*, st.job_name, cs.customer_name
						FROM offset_sale_order AS so
						LEFT JOIN off_job_structure AS st
							ON so.structure_id = st.id
						LEFT JOIN customer AS cs
							ON so.customer_id = cs.customer_id
						WHERE so.job_code = {$id}";

            $return = $this->db->query($sql);
            $data['so'] = $return->row();



            $so = new Offset_Sale_Order_Model();
            $so->load($id);

        } else {
            $data['update'] = false;
            $so = new Offset_Sale_Order_Model();
            $so->job_code = $this->Offset_Sale_Order_Model->max();
        }


        if (isset($_POST['submit'])) {

            // $so->job_code 	    = $data['job_code'];
            $so->date	  		= dbDate($this->input->post('date'));
            $so->po_num	  		= $this->input->post('po_num');
            $so->po_date  		= dbDate($this->input->post('po_date'));
            $so->delivery_date	= dbDate($this->input->post('delivery_date'));
            $so->structure_id 	= $this->input->post('structure_id');
            $so->customer_id 	= $this->input->post('customer_id');
            $so->order_type 	= $this->input->post('order_type');
            $so->quantity 		= $this->input->post('quantity');
            $so->excess_quantity= $this->input->post('excess_quantity');
            $so->rate 			= $this->input->post('rate');
            $so->meter 			= 0;
            $so->remarks		= $this->input->post('remarks');
            $so->hold 		 	= $this->input->post('hold')?1:0;
            $so->completed 		= $this->input->post('completed')?1:0;

            if ($id) {

                $so->updated_by = $this->session->userdata('user_id');
                $so->updated_time = dbTime();
                $so->save();

                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                }else{
                    $this->session->set_flashdata('userMsg', 'Updated Successfully.');
					redirect(site_url('offset_sale_order/lists'));
                }

            }else{

                $so->created_by = $this->session->userdata('user_id');
                $so->created_time = dbTime();

                // $s = $so->save();
                // die( $this->db->last_query() );
                if ($so->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    }else{
                        $this->session->set_flashdata('userMsg', 'Sale order generated.');
					    redirect(site_url('offset_sale_order/lists'));
                    }

                }

            }
        }

        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('offset/sale_order', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('offset/sale_order', $data);
            $this->load->view('main/footer', $data);
        }
    }
    
    /**************************************************
     *           ADD ORDER WASTAGE DETAILS.
     **************************************************/

    public function add_wastage($job_code, $id = false)
    {
        // if(1!=$this->session->userdata('user_id')){ return; }
        $this->load->model('Offset_Sale_Order_Model');
        $so = new Offset_Sale_Order_Model();
        $so->load($job_code);
        $data['so'] = $so;
        $data['job_code'] = $job_code;
        
        $this->load->model('Offset_order_wastage_model');
        $record = new Offset_order_wastage_model();
        if ($id) { $record->load($id); }
        $data['roles'] = $this->allowed_roles;
        $data['record'] = $record;

        if (isset($_POST['submit'])) {

            foreach ($this->input->post('data') as $key => $value) {
                $record->{$key} = $value;
            }
            $record->dispatch_date = dbDate($this->input->post('date'));
            $record->{ ($id)? 'updated_by': 'added_by' } = $this->session->userdata('user_id');
            $record->{ ($id)? 'updated_time': 'added_time' } = dbTime();
            $affected_row = $record->save();
            if ($affected_row) {
                $id = $id? $id: $affected_row;
                $this->session->set_flashdata('userMsg', 'Sale order generated.');
                redirect(site_url("offset_sale_order/lists"));
            }elseif ( !empty($this->db->error())) {
                $this->session->set_flashdata('userMsg', addslashes($this->db->error()['message']));
            }
            // if save
        }

        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        $data['site_title'] = 'Add New Order Details';
        $this->load->view('main/header', $data);
        $this->load->view('main/navigation', $data);
        $this->load->view('main/rightNavigation', $data);
        $this->load->view('main/topbar', $data);
        $this->load->view('offset/order_wastage', $data);
        $this->load->view('main/footer', $data);
    }

    public function production_detail($id)
    {
        if (!$id) {
            return;
        }else{
            $this->load->model('Offset_order_wastage_model');
            $x = new Offset_order_wastage_model();
            $record = $x->load_by_id($id);
            $this->load->library('PDF_offset_production_detail');
            $pdf = new PDF_offset_production_detail();
            $pdf->detail_id = $id;
            $pdf->set_data($record);
            $pdf->AddPage();

            $pdf->DisplayTable();
        }
    }


    /**************************************************
     *					=SALE ORDER LIST.
     **************************************************/
    public function lists()
    {
        // echo 'this method is called';
        $data['title'] = 'Sale Order List | Edit | Delete.!';
        $data['session'] = $this->session->all_userdata();

        $data['roles'] = $this->allowed_roles;

        $sql = "CALL offset_so_list()";
        $return = $this->db->query($sql);
        $data['so'] = $return->result();
        $data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

        if ($this->input->is_ajax_request()) {
            $this->load->view('offset/sale_order_list',$data);
        }else{
            $fb = $this->session->flashdata('userMsg');
            $data['feed_back'] = ($fb)? feed_back($fb):'';
            $data['site_title'] = 'Sale Order List';
            $this->load->view('main/header',$data);
            $this->load->view('main/navigation',$data);
            $this->load->view('main/rightNavigation',$data);
            $this->load->view('main/topbar',$data);
            $this->load->view('offset/sale_order_list',$data);
            $this->load->view('main/footer',$data);
        }

    }

    /**************************************************
     *				DELETING =SALE ORDER.
     **************************************************/

    public function delete($id=null)
    {
        if ($id) {
            $this->load->model('Offset_Sale_Order_Model');
            $so = new Offset_Sale_Order_Model();
            $so->load($id);

            if ($so->job_code){
                $so->delete();
                echo "Successfully Deleted";
            }else{
                echo "<h1>Sorry No Record Found.!</h1>";
            }

        } // END IF ID PRAMETER IS SET
        else
            echo "<h1>Please Follow Given Link.!</h1>";

    } // END DELETE CATEGORY FUCTION

    /**************************************************
     *					=sale order.
     **************************************************/

    public function so_print($job_code,$accounts=false)
    {

        if(User_Model::hasAccess('viewOffsetSaleOrder')){

            if (isset($job_code)) {

                $material = new Material_Model();
                $this->load->model('Offset_Sale_Order_Model');
                $job = $this->Offset_Sale_Order_Model->get_order_by_job($job_code);

                $pdf = new fpdf("P","mm","A4");
                $pdf->SetMargins(15,10);

                $pdf->AddPage();
                $pdf->SetFillColor(233,234,237);
                $pdf->SetTitle("Sale Order # ".$job_code);
                $pdf->Image(base_url('assets/img/logo.jpg'),15,10,30,25);
                // $pdf->ln(20);
                $pdf->SetFont('Arial','U',18);
                $pdf->Cell(0,4,"",0,1);
                $pdf->Cell(0,10,""."SALE ORDER (Offset)",0,0,'C');
                $pdf->SetFont('Arial','',8);
                $pdf->Cell(0,10,""."PTP.FM.PR.008",0,1,'R');
                $pdf->SetFont('Arial','I',10);
                // $pdf->Cell(0,10,""."Offset",0,1,'C');

                $pdf->ln(28);

                $colors = array();
                $spotColors = array();
                if($job->c)
                    $colors[] = 'Cyan';
                if($job->m)
                    $colors[] = 'Magenta';
                if($job->y)
                    $colors[] = 'Yellow';
                if($job->k)
                    $colors[] = 'Black';
                if($job->spot1)
                    $spotColors[] = $job->spot1;
                if($job->spot2)
                    $spotColors[] = $job->spot2;
                if($job->spot3)
                    $spotColors[] = $job->spot3;

                $uv = array();
                if($job->uv_gloss)
                    $uv[] = 'Gloss';
                if($job->uv_matt)
                    $uv[] = 'Matt';
                if($job->uv_spot)
                    $uv[] = 'Spot';

                $waterBase = array();
                if($job->water_gloss)
                    $waterBase[] = 'Gloss';
                if($job->water_matt)
                    $waterBase[] = 'Matt';

                // check($job);
                $datas = [
                    "Date:"         => pkDate($job->date),
                    "Delivery Date:"=> pkDate($job->delivery_date),
                    "Job #:"        => $job->job_code,
                    "PO #:"         => $job->po_num,
                    "Party:"        => $job->customer_name,
                    "Item:"         => $job->job_name,
                    "Material:"     => $job->material_id,
                    "Structure:"    => strtoupper($job->sheet_type).', ('.$job->board_width.'" x '.$job->board_height.'") '.$job->gsm.'gsm' . ' ('.number_format(($job->no_ups>0)?$job->quantity/$job->no_ups:0).' sheets)',
                    "No. of UPs"    => sprintf("%s UPs (%s)", $job->no_ups, number_format(($job->quantity/$job->no_ups))),
                    "Grain:"        => ( $job->grain_size == 'w' )? $job->board_width.'"' : $job->board_height.'"',
                    "Order Quantity:"   => number_format($job->quantity,2),
                    "Rate:"         => ($accounts=='accounts')? number_format($job->rate,2) . " PKR":'',
                    "Colors:"       => implode(', ',$colors),
                    "Spot Colors:"  => implode(', ',$spotColors),
                    "UV"            => join(', ',$uv),
                    "Water Base"    => join(', ',$waterBase),
                    "Fluting"      => $job->flutting,
                    "Remarks"      => $job->remarks,
                ];

                $height = 15;
                foreach ($datas as $key => $value) {
                    if (empty(str_replace(" ", "", $key)) && empty($value)) {
                        $height += 15;
                        continue;
                    }
                    if ($key == 'Remarks') {
                        $pdf->SetFont('Arial','B',10);
                        $pdf->cell(45,10,strtoupper($key),0,0);
                        $pdf->SetFont('Arial','',9);
                        $pdf->MultiCell(0, 6, $job->remarks);
                    }else{
                        $pdf->SetFont('Arial','B',10);
                        $pdf->cell(45,10,strtoupper($key),0,0);
                        $pdf->SetFont('Arial','',9);
                        $pdf->cell(60,10,$value,0,1);
                    }
                }

                $pdf->setXY(15,275);
                $pdf->SetAutoPageBreak(false, $margin=0);
                $pdf->SetFont('Arial','B',12);
                $pdf->cell(100,10,'Checked By: _______________________');
                $pdf->cell(30,10,'Approved By: _______________________');
                $pdf->Ln();
                // $pdf->setX(110);
                $pdf->SetFont('courier','U',8);
                $pdf->cell(30,10,'Prepare By: '.($job->created_by)." (".created_time($job->created_time).")");

                $fileName = 'sale_order_'.$job->job_name;
                (isset($_GET['download']) && $_GET['download']=='true' ) ?$pdf->Output($fileName.'.pdf','D'):$pdf->Output();

            } // if id isset

        }else{ // if roll exist
            echo "Your are not allowed for this view";
        }


    } // so_print()


    public function job_card($job_code)
    {
        if(!isset($job_code)){
            echo 'Please follow the link';
            return;
        }
        if (User_Model::hasAccess('viewOffsetJobCard')) {
            $this->load->model('Offset_Sale_Order_Model');

            $data = $this->Offset_Sale_Order_Model->get_job_card_data($job_code);
            $this->load->library('PDF_Offset_Job_Card');
            ob_start();
            $pdf = new PDF_Offset_Job_Card();
            $pdf->data = $data;
            $pdf->job_code = $job_code;
            $pdf->AddPage('L');
            $pdf->SetTitle('Job Card For Job # '.$job_code);

            $pdf->DisplayTable();

            $pdf->Output();

        }else{
            echo "Your are not allowed for this view";
        }
    }


    public function die_making_form($job_code)
    {
        if(!isset($job_code)){
            echo 'Please follow the link';
            return;
        }
        if (User_Model::hasAccess('viewOffsetJobCard')) {
            $this->load->model('Offset_Sale_Order_Model');

            $data = $this->Offset_Sale_Order_Model->get_job_card_data($job_code);
            $this->load->library('fpdf');
            ob_start();
            $pdf = new FPDF();
            $pdf->AddPage();
            $resetX = $pdf->GetX();
            $resetY = $pdf->GetY();
            $pdf->SetTitle('Die Making Form For Sale Order # '.$job_code);

            $pdf->SetFont('times','U',18);
            $pdf->Cell(0,10,'Offset Die Making Department',0,1,"C");

            $pdf->SetFont('times','BU',20);
            $pdf->Cell(0,10,'Order Form',0,1,"C");
            $pdf->Ln(3);
            $pdf->SetFont('times','',14);
            $pdf->Cell(100,10,"Job #: $data->job_code",0,0);
            $pdf->Cell(0,10,'Date: '.date('d/m/Y'),0,1,"R");
            $pdf->Ln(3);
//            check($data);
            $w = 40;$h = 6.5;
            $pdf->Cell($w,$h,'Customer P.O # : ');
            $pdf->Cell(0,7,$data->po_num,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Master File # : ');
            $pdf->Cell(0,7,$data->structure_id,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Customer Name : ');
            $pdf->Cell(0,7,$data->customer_name,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Job Name : ');
            $pdf->Cell(0,7,$data->job_name,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Epson No. : ');
            $pdf->Cell(0,7,$data->structure_id,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Job UPS : ');
            $pdf->Cell(0,7,$data->no_ups,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Order Status : ');
            $pdf->Cell($w/2,7,'Repeat',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'New',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'Proofing',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Die : ');
            $pdf->Cell($w/2,7,'New',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'Rule Change',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'Remake',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Rule Used : ');
            $pdf->Cell($w/2,7,'Cutting',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'Creasing',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'Perforation',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Rule Thickness : ');
            $pdf->Cell($w/2,7,'2PT',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'Special',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Wooden Ply Quality: ');
            $pdf->Cell($w/2,7,'White',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'Black',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Priority : ');
            $pdf->Cell($w/2,7,'Urgent',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'Normal',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Die Made Against : ');
            $pdf->Cell($w/2,7,'Order',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-10,7,'Proofing',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(5);

            $pdf->SetFont('times','B',18);
            $pdf->Cell(0,7,'Remarks',1,1,'C');
            $pdf->Cell(0,7,'',1,1,'C');
            $pdf->Cell(0,7,'',1,1,'C');
            $pdf->Cell(0,7,'',1,1,'C');
            $pdf->Cell(0,7,'',1,1,'C');
            $pdf->Ln(5);

            $pdf->Cell(0,8,'Post Production',1,1,'C');
            $pdf->SetFont('times','',14);
            $pdf->Cell($w+10,8,'Die Making On: ',1,0);
            $pdf->Cell(0,8,'',1,1);
            $pdf->Cell($w+10,8,'Die No.: ',1,0);
            $pdf->Cell(0,8,'',1,1,'C');
            $pdf->Cell($w+10,8,'Operator Name: ',1,0);
            $pdf->Cell(0,8,'',1,1,'C');
            $pdf->Cell($w+10,8,'Shift: ',1,0);
            $pdf->Cell($w,8,'',1,0,'C');
            $pdf->Cell($w+10,8,'Date: ',1,0);
            $pdf->Cell(0,8,'Time: ',1,1);
            $pdf->Ln(14);

            $pdf->Cell(65,8,'    _______________        ',0,0,"C");
            $pdf->Cell(65,8,'       _______________        ',0,0,"C");
            $pdf->Cell(65,8,'       _______________        ',0,1,"C");

            $pdf->Cell(65,8,'    Operator Sign        ',0,0,"C");
            $pdf->Cell(65,8,'       Supervisor Sign        ',0,0,"C");
            $pdf->Cell(65,8,'       Authorized Sign        ',0,0,"C");


            $pdf->Output();

        }else{
            echo "Your are not allowed for this view";
        }
    }


    public function plate_making_form($job_code)
    {
        if(!isset($job_code)){
            echo 'Please follow the link';
            return;
        }
        if (User_Model::hasAccess('viewOffsetJobCard')) {
            $this->load->model('Offset_Sale_Order_Model');

            $data = $this->Offset_Sale_Order_Model->get_job_card_data($job_code);
            $this->load->library('fpdf');
            ob_start();
            $pdf = new FPDF();
            $pdf->AddPage();
            $resetX = $pdf->GetX();
            $resetY = $pdf->GetY();
            $pdf->SetTitle('Plate Making Form For Sale Order # '.$job_code);

            $pdf->SetFont('times','U',18);
            $pdf->Cell(0,10,'Offset Plate Making Department',0,1,"C");

            $pdf->SetFont('times','BU',20);
            $pdf->Cell(0,10,'Order Form',0,1,"C");
            $pdf->Ln(3);
            $pdf->SetFont('times','',14);
            $pdf->Cell(100,10,"Job # $data->job_code",0,0);
            $pdf->Cell(0,10,'Date: '.date('d/m/Y'),0,1,"R");
            $pdf->Ln(3);
//            check($data);
            $w = 40;$h = 6.5;
            $pdf->Cell($w,$h,'Customer P.O # : ');
            $pdf->Cell(0,7,$data->po_num,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Master File # : ');
            $pdf->Cell(0,7,$data->structure_id,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Customer Name : ');
            $pdf->Cell(0,7,$data->customer_name,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Job Name : ');
            $pdf->Cell(0,7,$data->job_name,"B",1);
            $pdf->Ln(3);

            $pdf->Cell($w,$h,'Eppson No. : ');
            $pdf->Cell(0,7,$data->structure_id,"B",1);
            $pdf->Ln(8);

            $pdf->Cell($w,$h,'Order Status : ');
            $pdf->Cell($w/2,7,'Repeat',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'New',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'Proofing',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Plate : ');
            $pdf->Cell($w/2,7,'New',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'Remake',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'Design Change',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Required Color: ');
            $pdf->Cell($w/2,7,'1',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'2',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'3',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,' ');
            $pdf->Cell($w/2,7,'4',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'5',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'6',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Priority : ');
            $pdf->Cell($w/2,7,'Urgent',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'Normal',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(1);

            $pdf->Cell($w,$h,'Plate Made Against : ');
            $pdf->Cell($w/2,7,'Order',0,0,'R');
            $pdf->Cell($w/2,7,'',1,0,'R');
            $pdf->Cell($w-6,7,'Proofing',0,0,'R');
            $pdf->Cell($w/2,7,'',1,1,'R');
            $pdf->Ln(10);

            $pdf->SetFont('times','B',18);
            $pdf->Cell(0,7,'Remarks',1,1,'C');
            $pdf->Cell(0,7,'',1,1,'C');
            $pdf->Cell(0,7,'',1,1,'C');
            $pdf->Cell(0,7,'',1,1,'C');
            $pdf->Cell(0,7,'',1,1,'C');
            $pdf->Ln(5);

            $pdf->Cell(0,8,'Post Production',1,1,'C');
            $pdf->SetFont('times','',14);
            $pdf->Cell($w+10,8,'Plate Making On: ',1,0);
            $pdf->Cell(0,8,'',1,1);
            $pdf->Cell($w+10,8,'Plate No.: ',1,0);
            $pdf->Cell(0,8,'',1,1,'C');
            $pdf->Cell($w+10,8,'Operator Name: ',1,0);
            $pdf->Cell(0,8,'',1,1,'C');
            $pdf->Cell($w+10,8,'Shift: ',1,0);
            $pdf->Cell($w,8,'',1,0,'C');
            $pdf->Cell($w+10,8,'Date: ',1,0);
            $pdf->Cell(0,8,'Time: ',1,1);
            $pdf->Ln(22);

            $pdf->Cell(65,8,'    _______________        ',0,0,"C");
            $pdf->Cell(65,8,'       _______________        ',0,0,"C");
            $pdf->Cell(65,8,'       _______________        ',0,1,"C");

            $pdf->Cell(65,8,'    Operator Sign        ',0,0,"C");
            $pdf->Cell(65,8,'       Supervisor Sign        ',0,0,"C");
            $pdf->Cell(65,8,'       Authorized Sign        ',0,0,"C");

            $pdf->Output();

        }else{
            echo "Your are not allowed for this view";
        }
    }

}