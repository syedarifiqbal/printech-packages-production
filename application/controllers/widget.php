<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Widget extends CI_Controller {

	# Methodes 

	/**************************************************
	*					=ADDTODOLIST.
	**************************************************/

	public function todolist()
	{

		if (isset($_POST['title'])) {
			$todo = new Todolist_Model();
			$todo->id = $this->Todolist_Model->max();
			$todo->title = $this->input->post('title');
			$todo->done = 0;
			$todo->user_id = $this->session->userdata('user_id');
			$todo->created_time = dbTime();
			if ($todo->save()) {
				$data['title_id'] = $todo->id ;
				$data['label'] = $todo->title ;
				$data['result'] = 'success' ;
				echo json_encode($data);
			}

		}else{
			echo 'not submited';
		}

	} // todolist()


	/**************************************************
	*					=EIDTTODOLIST.
	**************************************************/

	public function delete_todolist($id)
	{

		if (isset($id)) {
			$todo = new Todolist_Model();
			$todo->load($id);
			$todo->delete();
			echo 'success';

		}else{
			echo 'Sorry some error';
		}

	} // delete_todolist()


	/**************************************************
	*					=EIDTTODOLIST.
	**************************************************/

	public function update_todolist($id,$undo=false)
	{

		$todo = new Todolist_Model();
		$todo->load($id);
		if (!$undo) {
			$todo->done = 0;
			$todo->save();
			echo 'unchecked' ;
		}else{
			$todo->done = 1;
			$todo->save();
			echo 'checked' ;
		}

	} // update_todolist()
	

	/**************************************************
	*					=MESSAGE.
	**************************************************/

	public function send_message()
	{

		if (isset($_POST['message'])) {
			$msg = new Message_Model();
			$msg->msg_id = $this->Message_Model->max();
			$msg->message = $this->input->post('message');
			$msg->user_id = $this->session->userdata('user_id');
			$msg->timestump = dbTime();
			if ($msg->save()) {
				$current_user_id = $this->session->userdata('user_id');
				$user = new User_Model();
				$user->load( $current_user_id );

				$notification_message = $user->user_name." Post on form: " . $msg->message;

				$notification = new Notification_Model();
				$notification->notification_id = $this->Notification_Model->max();
				$notification->msg_id = $msg->msg_id;
				$notification->notification = $notification_message;
				$notification->send_user_id = $current_user_id;
				// $notification->seen = 0;
				$notification->save();
				$data['message_id'] = $msg->msg_id;
				$data['message'] = $msg->message;
				$data['img_path'] = ($user->avater_path) ? base_url('assets/img/profile_img/'.$user->avater_path):base_url('assets/img/sign-in.jpg');
				$data['timestump'] = $msg->timestump;
				$data['result'] = 'success';
				echo json_encode($data);
			}

		}else{
			echo 'not submited';
		}

	} // send_message()
	

	/**************************************************
	*					=PERSONAL MESSAGE.
	**************************************************/

	public function send_personal_message($to_user_id)
	{

		if (isset($_POST['message'])) {
			$msg = new Personal_Message_Model();
			$msg->id = $this->Personal_Message_Model->max();
			$msg->message = $this->input->post('message');
			$msg->from = $this->session->userdata('user_id');
			$msg->to = $to_user_id;
			$msg->seen = 0;
			$msg->attachment = '';
			$msg->timestamp = dbTime();
			$msg->save();
			if ($msg->save()) {
				$current_user_id = $this->session->userdata('user_id');
				$user = new User_Model();
				$user->load( $current_user_id );

				$notification_message = $user->user_name." Post on form: " . $msg->message;

				$notification = new Notification_Model();
				$notification->notification_id = $this->Notification_Model->max();
				$notification->msg_id = $msg->id;
				$notification->notification = $notification_message;
				$notification->send_user_id = $current_user_id;
				// $notification->seen = 0;
				$notification->save();
				$data['message_id'] = $msg->id;
				$data['message'] = $msg->message;
				$data['img_path'] = ($user->avater_path) ? base_url('assets/img/profile_img/'.$user->avater_path):base_url('assets/img/sign-in.jpg');
				$data['timestump'] = $msg->timestamp;
				$data['result'] = 'success is going to lol';
				echo json_encode($data);
			}

		}else{
			echo json_encode('not submited');
		}

	} // send_message()

	/**************************************************
	*				=GET MESSAGE BY USER.
	**************************************************/

	public function get_message_user($for_user)
	{
		$user_id = $this->session->userdata('user_id');
		$sql = $this->db->query("SELECT * FROM personal_message
								WHERE $user_id IN (`from`,`to`) AND $for_user IN (`from`,`to`)")->result();
		// echo json_encode($sql);
		foreach ($sql as $msg) {
			$reply = ($this->session->userdata('user_id')==$msg->from)?' class="reply"':'';
			$image = ($msg->avater_path) ? base_url('assets/img/profile_img/'.$msg->avater_path):base_url('assets/img/sign-in.jpg');
		echo '<li'.$reply.' id="'.$msg->msg_id.'">
				<div class="chat-thumb">
					<a href="#" data-tooltip="'.ucfirst(str_replace("_", " ", $msg->user_name)).'" data-placement="top"><img src="'.$image.'" width="61" height="61" alt="'.$msg->user_name.'"></a>
				</div>
				<div class="chat-desc">
					<p>'.$msg->message.'</p>
					<i class="chat-time" data-livestamp="'.$msg->timestump.'">Syed arif iqbal</i>
				</div>
			 </li>';
		}

	} // send_message()
	

	/**************************************************
	*					=MESSAGE.
	**************************************************/

	public function get_notification()
	{
		$out = '';
		$notification = $this->Notification_Model->get();
		
		if (!empty($notification)) {
			foreach ($notification as $row) {
				// echo $row->notification ." Seen by.".$row->seen."<br/>";
				$send_by = new User_Model();
				$send_by->load($row->send_user_id);
				$avater = ($send_by->avater_path) ? base_url('assets/img/profile_img/'.$send_by->avater_path):base_url('assets/img/sign-in.jpg');
				$out .= '<a href="#" title="'.$row->notification.'"><img src="'.$avater.'" width="40" height="40"/>';
				$out .= substr($row->notification,0,38).'...<p><i class="fa fa-clock-o"><span data-livestamp="'.$row->timestump.'"></span></i></p></a>';
			}
		}
		echo $out;

	} // send_message()


	public function update_remote_server()
	{
		
		$remote_server = $this->load->database('remote_server',TRUE);
		if(!$remote_server->conn_id)
			die("Maybe Internet Problem!");
		// $sql = $remote_server->order_by('customer_id','desc')->limit(1)->get('customer');
		$sql = $remote_server->get('customer');
		if (!empty($sql->result())) {
			foreach ($sql->result() as $c) {
				$available_customer[] = $c->customer_id;
			}
		}
		if (isset($available_customer)) {
			$customer = $this->Customer_Model->where('customer_id not in ( '.join(', ',$available_customer).' )','',"*");
		}else{
			$customer = $this->Customer_Model->get();
		}
		$affected_rows = 0;
		if (empty($customer)) {
			$data['messages'][] = "Customers Up to date!.<br>";
		}else{

			foreach ($customer as $cus) {
				$cus_feild = ['customer_id' => $cus->customer_id,
				 'customer_name' => $cus->customer_name, 
				 'customer_code' => $cus->customer_code, 
				 'active' => $cus->active, 
				 'toured' => $cus->toured, 
				 'customer' => $cus->customer, 
				 'client_password' => $cus->client_password, 
				 'create_by' => $cus->create_by, 
				 'create_time' => $cus->create_time];

				$affected_rows += $remote_server->save('customer', $cus_feild);
			}

			$data['messages'][] = "Numner of Customers Inserted: ". $affected_rows .'<br>';

		}

		/****************************** Insertion of sale order ******************************/

		$sql = $remote_server->get('sale_order');
		if (!empty($sql->result())) {
			foreach ($sql->result() as $c) {
				$available_so[] = $c->job_code;
			}
		}
		if (isset($available_so)) {
			$sale_order = $this->db->query('SELECT so.*, st.job_name FROM `sale_order` AS so
										INNER JOIN job_master AS st ON st.id = so.structure_id 
										WHERE so.job_code NOT IN ( '.join(', ',$available_so).' )')->result();
		}else{
			$sale_order = $this->db->query('SELECT so.*, st.job_name FROM `sale_order` AS so
										INNER JOIN job_master AS st ON st.id = so.structure_id')->result();
		}
		$affected_rows = 0;
		if (empty($sale_order)) {
			$data['messages'][] = "Sale Orders Up to date!.<br>";
		}else{

			foreach ($sale_order as $so) {
				
				$so_feild = [
					'job_code' => $so->job_code,
				    'date' => $so->date,
				    'po_num' => $so->po_num,
				    'po_date' => $so->po_date,
				    'delivery_date' => $so->delivery_date,
				    'job_name' => $so->job_name,
				    'customer_id' => $so->customer_id,
				    'order_type' => $so->order_type,
				    'quantity' => $so->quantity,
				    'description' => $so->description,
				    'rate' => $so->rate,
				    'meter' => $so->meter,
				    'remarks' => $so->remarks,
				    'hold' => $so->hold,
				    'approved' => $so->approved,
				    'created_time' => $so->created_time,
				    'created_by' => $so->created_by
			    ];

				$affected_rows += $remote_server->save('sale_order', $so_feild);
			}

			$data['messages'][] = "Numner of Orders Inserted: ". $affected_rows .'<br>';

		}

		/****************************** Insertion of Deliveries ******************************/

		$sql = $remote_server->get('dispatch');
		if (!empty($sql->result())) {
			foreach ($sql->result() as $c) {
				$deliveries[] = $c->id;
			}
		}
		if (isset($deliveries)) {
			$dispatch = $this->Dispatch_Model->where('id NOT IN ( '.join(', ',$deliveries).' )', '', "*");
		}else{
			$dispatch = $this->Dispatch_Model->get();
		}
		$affected_rows = 0;
		if (empty($dispatch)) {
			$data['messages'][] = "Dispatches Up to date!.<br>";
		}else{

			foreach ($dispatch as $d) {
				
				$dispatch_feilds = [
					  'id' => $d->id,
					  'date' => $d->date,
					  'so' => $d->so,
					  'challan' => $d->challan,
					  'gross_weight' => $d->gross_weight,
					  'tare_weight' => $d->tare_weight,
					  'net_weight' => $d->net_weight,
					  'destination' => $d->destination,
					  'receiver' => $d->receiver,
					  'no_carton' => $d->no_carton,
					  'no_pcs' => $d->no_pcs,
					  'no_roll' => $d->no_roll,
					  'meter' => $d->meter,
					  'remarks' => $d->remarks,
					  'driver_name' => $d->driver_name,
					  'vehicle' => $d->vehicle,
					  'created_by' => $d->created_by,
					  'created_time' => $d->created_time,
			    ];

				$affected_rows += $remote_server->save('dispatch', $dispatch_feilds);
			}

			$data['messages'][] = "Numner of Deliveries Inserted: ". $affected_rows .'<br>';

		}

		$this->load->view('close_window.php',$data);
		
		// $this->load->database('remote_server',FALSE);
		// $this->load->database('default', TRUE);
		// var_dump($jo);

	}

	public function chart($from='2015-01-01', $to='2016-03-30')
	{
		$order_type = 'kg';
		if (isset($_GET['order_type']))
			$order_type = str_replace("'", "", $_GET['order_type']);
			// var_dump($_GET);
		// getting all customer who have order between given dates
		$customers = $this->db->query("SELECT DISTINCT c.customer_id, c.customer_name FROM sale_order so 
							JOIN customer c ON so.customer_id = c.customer_id
							WHERE so.date BETWEEN '{$from}' AND '{$to}' AND so.order_type = '{$order_type}'
							");
		// check($this->db->last_query());
		$customers = $customers->result();
		// get available months between given dates
		$dates = $this->db->query("SELECT DISTINCT MONTHNAME(so.date) AS d, year(so.date) AS y FROM sale_order so 
									JOIN customer c ON so.customer_id = c.customer_id
									WHERE so.date BETWEEN '{$from}' AND '{$to}' AND so.order_type = '{$order_type}' ")->result();
		// all customer data array
		$data = [];
		foreach ($customers as $row) {
			// getting sum orders monthwise for each customer
			$dataArray = array();
			// loop all available months
			foreach ($dates as $date) {
				$qty = $this->db->query("SELECT SUM(so.quantity) AS qty FROM sale_order so
											JOIN customer c ON c.customer_id = so.customer_id
											WHERE c.customer_id = {$row->customer_id} AND so.order_type = '{$order_type}'
											AND MONTHNAME(so.date) = '{$date->d}' AND year(so.date) = {$date->y}
											GROUP BY c.customer_id, MONTHNAME(so.date), year(so.date)");
				$qty = $qty->row();
				// check if user don't have order current month the assign to 0 other wise the order quantity
				if (empty($qty)) {
					$dataArray[] = 0;
				}else{
					$dataArray[] = $qty->qty;
				}
			} // end date loop
			$data[$row->customer_name] = $dataArray;
		} // end customer loop

		// prepend customer name for each customer data row
		foreach ($data as $key => $value) {
			array_unshift($data[$key], $key);
		}
		// create date/month array
		$x = array('x');
		foreach ($dates as $value) {
			$x[] = substr($value->d, 0,3).' '.$value->y;
		}
		// prepend date array to data array
		array_unshift($data, $x);
		//create final array for output
		$arr['columns'] = array();
		foreach ($data as $row) {
			$arr['columns'][] = $row;
		}

		$this->output
	    ->set_content_type('application/json')
	    ->set_output(json_encode($arr));
	}


}