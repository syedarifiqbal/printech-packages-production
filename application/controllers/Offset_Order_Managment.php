<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Offset_Order_Managment extends MY_Controller
{

    public $allowed_roles = array();
    private $context = "offset_structure";
    private $gallery_path = './uploads/gallery/';

    # Methodes

    /**************************************************
     *                    ADD MATERIAL.
     **************************************************/

    public function job_structure($id = false)
    {
        $this->load->model('Off_Job_Structure_Model');
        if ($id) {
            $data['update'] = true;
            $x = new Off_Job_Structure_Model();
            $x->load($id);
            $structure = $x;
            $data['structure'] = $x;
        } else {
            $data['update'] = false;
            $structure = new Off_Job_Structure_Model();
            if( $this->input->post('reference_type') != 1 ){
                $new_id = $this->db->query("SELECT MAX(id)+1 AS id FROM off_job_structure WHERE id < 1001")->row();
                if( $new_id->id <= 1000 ){
                    $structure->id = $new_id->id;
                }else{
                    $structure->id = $this->Off_Job_Structure_Model->max();
                }
            }else{
                $structure->id = $this->Off_Job_Structure_Model->max();
            }
        }
        if (isset($_POST['job_name'])) {
            $structure->job_name = $this->input->post('job_name');
            $structure->c = $this->input->post('c') ? 1 : 0;
            $structure->m = $this->input->post('m') ? 1 : 0;
            $structure->y = $this->input->post('y') ? 1 : 0;
            $structure->k = $this->input->post('k') ? 1 : 0;
            $structure->spot1 = $this->input->post('spot1');
            $structure->spot2 = $this->input->post('spot2');
            $structure->spot3 = $this->input->post('spot3');
            $structure->machine = $this->input->post('machine')?$this->input->post('machine'):'';
            $structure->sheet_type = ($this->input->post('sheet_type') == 'board') ? 'board' : 'paper';
            $structure->board_width = $this->input->post('board_width');
            $structure->board_height = $this->input->post('board_height');
            $structure->gsm = $this->input->post('gsm');
            $structure->no_ups = $this->input->post('no_ups');
            $structure->grain_size = $this->input->post('grain_size');
            $structure->material_id = $this->input->post('material_id');
            $structure->water_gloss = $this->input->post('water_gloss') ? 1 : 0;
            $structure->water_matt = $this->input->post('water_matt') ? 1 : 0;
            $structure->uv_gloss = $this->input->post('uv_gloss') ? 1 : 0;
            $structure->uv_spot = $this->input->post('uv_spot') ? 1 : 0;
            $structure->uv_matt = $this->input->post('uv_matt') ? 1 : 0;
            $structure->lamination_gloss = $this->input->post('lamination_gloss') ? 1 : 0;
            $structure->lamination_matt = $this->input->post('lamination_matt') ? 1 : 0;
            $structure->lamination_both = $this->input->post('lamination_both') ? 1 : 0;
            $structure->lamination_type = $this->input->post('lamination_type');
            $structure->warnish_gloss = $this->input->post('warnish_gloss') ? 1 : 0;
            $structure->warnish_matt = $this->input->post('warnish_matt') ? 1 : 0;
            if( $this->input->post('foil') == 'None' && $this->input->post('custom_foil_color') != '' ){
                $structure->foil = $this->input->post('custom_foil_color');
            }else{
                $structure->foil = $this->input->post('foil');
            }
            $structure->foil_type = $this->input->post('foil_type');
            $structure->emboss = $this->input->post('emboss');
            $structure->color_remarks = $this->input->post('color_remarks')?$this->input->post('color_remarks'):'';
            $structure->ply = $this->input->post('ply');
            $structure->panel = $this->input->post('panel');
            $structure->roll_size = $this->input->post('roll_size');
            $structure->cutting_size = $this->input->post('cutting_size');
            $structure->corrugation_remarks = $this->input->post('corrugation_remarks');
            $structure->flutting = $this->input->post('flutting');
            // $structure->dubai_flutting = $this->input->post('dubai_flutting') ? 1 : 0;
            $structure->die_cutting_type = $this->input->post('die_cutting_type');
            $structure->die_remarks = $this->input->post('die_remarks');
            $structure->pasting_remarks = $this->input->post('pasting_remarks');
            $structure->pasting_type = $this->input->post('pasting_type');
            $structure->packing_qty = $this->input->post('packing_qty');
            $structure->correction = $this->input->post('correction');

            if ($id) {

                $structure->updated_by = $this->session->userdata('user_id');
                $structure->updated_time = dbTime();
                $structure->save();

                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                }else{
                    $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                    // redirect(site_url('Offset_Order_Managment/structure_list'));
                }

            }else{

                $structure->added_by = $this->session->userdata('user_id');
                $structure->added_time = dbTime();
                $res = $structure->save();
                // check($_POST);
                // check($this->db->last_query());
                if ($res) {
                    // insertion for colors/Requisation
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    }else{
                        $this->session->set_flashdata('userMsg', 'New Structure Added Successfully.');
					    redirect(site_url('Offset_Order_Managment/job_structure_list'));
                    }

                }

            }
        }

        $data['job_code'] = $this->Off_Job_Structure_Model->max();

        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        if ($this->input->is_ajax_request()) {
            $this->load->view('offset/job_structure', $data);
        } else {
            $data['site_title'] = 'Add New Material';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('offset/job_structure', $data);
            $this->load->view('main/footer', $data);
        }
    }

    public function job_structure_list()
    {
        $data['title'] = 'Job Structure List | Edit | Delete.!';
        $data['session'] = $this->session->all_userdata();
        $data['roles'] = $this->allowed_roles;
        $fb = $this->session->flashdata('userMsg');
        $data['feed_back'] = ($fb)? feed_back($fb):'';

        $this->load->model('Off_Job_Structure_Model');
        $job = new Off_Job_Structure_Model();
        $data['structures'] = $job->Off_Job_Structure_Model->get(1000);

        if ($this->input->is_ajax_request()) {
            $this->load->view('offset/structure_list',$data);
        }else{
            $data['site_title'] = 'Add New Job Structure.';
            $this->load->view('main/header',$data);
            $this->load->view('main/navigation',$data);
            $this->load->view('main/rightNavigation',$data);
            $this->load->view('main/topbar',$data);
            $this->load->view('offset/structure_list',$data);
            $this->load->view('main/footer',$data);
        }
    }

    public function save_gallery($str_id, $gallery_id=false)
    {
        $data['title'] = 'Job Structure Gallery.!';
        $data['session'] = $this->session->all_userdata();
        $data['roles'] = $this->allowed_roles;
        $data['context'] = $this->context;
        $data['context_id'] = $str_id;

        $this->load->model('Gallery_model');
        $record = new Gallery_model();
        if ($gallery_id) { $record->load($gallery_id); }
        $data['record'] = $record;

        $this->load->model('Off_Job_Structure_Model');
        $job = new Off_Job_Structure_Model();
        $job->load($str_id);
        $data['job'] = $job;

        if (isset($_POST['submit'])) {
            $this->form_validation->set_rules('data[name]','Gallery name','required');
            if ( $this->form_validation->run() == TRUE ) {
                foreach ($this->input->post('data') as $key => $value) {
                    $record->{$key} = $value;
                }
                $record->context_id = $str_id;
                $record->context = $this->context;
                $record->active = $record->id? $record->active: 1;
                $record->{$gallery_id?'updated_by':'added_by'} = $this->session->userdata('user_id');

                $file_datas = $this->multiple_upload();

                if ($this->is_uploaded($file_datas)) {
                    if ($gal_id = $record->save()) {
                        $this->insert_images($file_datas, $gal_id);
                        $this->session->set_flashdata('userMsg', $gallery_id? "Gallery Updated Successfully":"Gallery Added Successfully");
                        redirect( site_url( "offset_order_managment/gallery_list/$str_id" ) );
                    }
                }else{
                    $this->delete_images($file_datas);
                    $this->session->set_flashdata('userMsg', '<p>'. join('</p><p>', $this->gellary_upload_error) .'</p>');
                }
            }
        }

        $fb = $this->session->flashdata('userMsg');
        $data['feed_back'] = ($fb)? feed_back($fb):'';
        $data['site_title'] = "Add New Gallery for ($job->job_name)";
        $this->load->view('main/header',$data);
        $this->load->view('main/navigation',$data);
        $this->load->view('main/rightNavigation',$data);
        $this->load->view('main/topbar',$data);
        $this->load->view('gallery/form',$data);
        $this->load->view('main/footer',$data);
    }

    function delete_gallery($gallery_id)
    {
        $this->load->model('Gallery_model');
        $this->load->model('Gallery_images_model');
        $gallery = new Gallery_model();
        $gallery->load($gallery_id);
        
        $context_id = $gallery->context_id;
        $context = $gallery->context;

        $images = $this->Gallery_images_model->getWhere(array('gallery_id'=>$gallery_id));

        foreach ($images as $image) {
            $path = $this->gallery_path.$image->image;
            // x($path);
            if ( file_exists($path) ) {
                unlink($path);
            }
            $image->delete();
        }

        $gallery->delete();

        $this->session->set_flashdata('userMsg', "Gallery Deleted Successfully");
        redirect( site_url( "offset_order_managment/gallery_list/$gallery->context_id" ) );
    }

    public function view_structure($id){
        // if( !User_Model::hasAccess('viewOffsetJobStructure') )
        // not_permitted('Sorry You Are Not Permitted for the operation');

        if(isset($id)){

            $this->load->model('Off_Job_Structure_Model');
            $record = new Off_Job_Structure_Model();
            $record->load($id);

            $data = array(
                'General Info' => array(
                    'File #'=>$record->id*1,
                    'Job Name'=>$record->job_name
                ),
                'Printing Colors' => array(
                    'Colors' => sprintf('%s%s%s%s',$record->c ? "Cyan":"",$record->m ? ", Magenta":"",$record->y ? ", Yellow":"",$record->k ? ", Black ":""),
                    'Spot Colors' => sprintf('%s%s%s',$record->spot1 ? $record->spot1:"-",$record->spot2 ? ",".$record->spot2:", -",$record->spot3 ? ", ".$record->spot3:", -"),
                ),
                'Board' => array(
                    'Width'=> sprintf('%s"  (%smm)',$record->board_width,($record->board_width*25.4)),
                    'Height' => sprintf('%s"  (%smm)',$record->board_height,($record->board_height*25.4)),
                    'GSM' => $record->gsm,
                    'No ups'=>$record->no_ups,
                    'Material' => $record->material_id,
                    'Grain Size' => $record->displayGrain(),
                    'Sheet Type' => $record->sheet_type,
                    'Water Base' => sprintf('%s%s', $record->water_gloss?'Gloss':'',$record->water_matt?", Matt":''),
                    'UV' => sprintf('%s%s%s', $record->uv_gloss?'Gloss':'',$record->uv_matt?", Matt":'',$record->uv_spot?", Spot":''),
                    'Lamination' => sprintf('%s%s%s', $record->lamination_gloss?'Gloss':'',$record->lamination_matt?", Matt":'',$record->lamination_both?"(Both Side)":''),
                    'Lamination Type' => $record->lamination_type,
                    'Foil' => sprintf('%s %s', $record->foil? $record->foil:'',$record->foil_type),
                    'Varnish' => sprintf('%s%s', $record->warnish_gloss?'Gloss ':'',$record->warnish_matt?', Matt, ':''),
                ),
                'Corrugations'=>array(
                    'Play'=>$record->ply?$record->ply.' Ply':"No Ply",
                    'Flutting'=>$record->flutting,
                    'Panel' => $record->panel,
                    'Remarks' => $record->corrugation_remarks
                ),
                'Die Cutting' => array(
                    'Die Type' => $record->die_cutting_type,
                    'Emboss' => $record->emboss,
                    'Remarks' => $record->die_remarks
                ),
                'Pasting' => array(
                    'Pasting' => $record->pasting_type,
                    'Remarks' => $record->pasting_remarks
                ),
                'Dispatch' => array(
                'Packing List' => $record->packing_qty
            )
            );
            // check($record);
            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetTitle(sprintf("Job Strucuture for %s",$record->job_name));
            $pdf->SetFont('ARIAL','UB',16);
            $pdf->Cell(0,10,'Job Structure',0,1,'C');
            foreach ($data as $key => $value) {
                $pdf->SetFont('ARIAL','BU',12);
                $pdf->Cell(0,10,'  ' . $key . ':  ',1,1);

                foreach ($value as $key => $item) {
                    $pdf->SetFont('ARIAL','',11);
                    $pdf->Cell(60,6.5,$key . ': ',1,0);
                    $pdf->Cell(0,6.5,'  ' . $item,1,1);
                }
            }

            $pdf->Ln(5);
            $pdf->Write(4,'Corrections: '.$record->correction);

            $pdf->SetAutoPageBreak(false,5);
            $pdf->SetY(278);

            $user = new User_Model();
            $user->load($record->added_by);
            $pdf->Cell(30,4,"Created By:","B",1,'C');
            $pdf->SetFont('ARIAL','I',9);
            $pdf->Cell(30,3,$user->getName(),0,1,"C");
            $pdf->Cell(30,3,$record->added_time,0,1,"C");

            $pdf->Output();

        }else{
            $this->session->set_flashdata('userMsg', 'Please Follow the link.');
            redirect(site_url('Offset_Order_Managment/structure_list'));
        }

    }

    public function delete($id=null)
    {
        if ($id) {
            
            $this->load->model('Off_Job_Structure_Model');
            $structure = new Off_Job_Structure_Model();
            $structure->load($id);

            if ($structure->id){
                $structure->delete();
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Deleted";
                    return;
                }else{
                    $this->session->set_flashdata('userMsg', $material->material_name.' Deleted Successfully.');
                    redirect(site_url('Offset_Order_Managment/job_structure_list'));
                }
            }else{
                echo "<h1>Sorry No Record Found.!</h1>";
            }

        } // END IF ID PRAMETER IS SET
        else
            echo "<h1>Please Follow Given Link.!</h1>";

    } // END DELETE CATEGORY FUCTION


    function is_uploaded($data)
    {
        $uploaded = true;
        foreach ($data as $file) {
            if (!$file) {
                $uploaded = false;
                break;
            }
        }
        return $uploaded;
    }

    function insert_images($data, $gallery_id)
    {
        $this->load->model('Gallery_images_model');
        foreach ($data as $file) {
            $record = new Gallery_images_model();
            $record->gallery_id = $gallery_id;
            $record->image = $file;
            $record->save();
        }
    }

    function delete_images($data)
    {
        foreach ($data as $file) {
            $path = $this->gallery_path.$file;
            if (file_exists($path) && $file) {
                unlink($path);
            }
        }
    }
    
    public function multiple_upload()
    {
        $number_of_files_uploaded = count($_FILES['upl_files']['name']);
        $files = array();
        // Faking upload calls to $_FILE
        for ($i = 0; $i < $number_of_files_uploaded; $i++) :
            $_FILES['userfile']['name']     = $_FILES['upl_files']['name'][$i];
            $_FILES['userfile']['type']     = $_FILES['upl_files']['type'][$i];
            $_FILES['userfile']['tmp_name'] = $_FILES['upl_files']['tmp_name'][$i];
            $_FILES['userfile']['error']    = $_FILES['upl_files']['error'][$i];
            $_FILES['userfile']['size']     = $_FILES['upl_files']['size'][$i];
            $config = array(
                'allowed_types' => 'jpg|jpeg|png|gif',
                'overwrite'     => FALSE,
                'upload_path'   => $this->gallery_path
            );
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if ( !$this->upload->do_upload() ) :
                $files[] = false;
                $error = array('error' => $this->upload->display_errors());
                $this->gellary_upload_error[] = $error['error'];
              else :
                $data = $this->upload->data();
                $files[] = $data['file_name'];
            endif;
        endfor;
        return $files;
    }

}