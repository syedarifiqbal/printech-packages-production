<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Report extends MY_Controller
{

    public $allowed_roles = array();

    # Methodes

    /**************************************************
     *                    =JOBCARD.
     **************************************************/

    public function job_card($job_code)
    {

        if (User_Model::hasAccess('viewRotoJobCard')) {

            if (isset($job_code)) {

                $material = new Material_Model();
                $job = $this->db->query("SELECT so.date, so.po_num, st.file_number, so.po_date, so.delivery_date, so.job_code, so.created_by, so.created_time, st.job_name, c.customer_name, so.quantity, so.order_type, so.meter, so.rate AS rate, st.print_type, st.print_as_per, st.cylinder, st.cylinder_len, st.cylinder_circum, st.no_ups, st.no_repeat, st.no_lamination, st.coa_requird, st.dc, st.remarks, st.address,
										f1.material_name AS printing_material, so.film1_wt AS printing_weight,
										f2.material_name AS lm1_material, so.film2_wt AS lam1_weight,
										f3.material_name AS lm2_material, so.film3_wt AS lam2_weight,
										f4.material_name AS lm3_material, so.film4_wt AS lam3_weight
										FROM sale_order AS so
										    LEFT JOIN job_master AS st ON so.structure_id = st.id
										    LEFT JOIN customer AS c ON so.customer_id = c.customer_id
										    LEFT JOIN material AS f1 ON st.print_material = f1.material_id
										    LEFT JOIN material AS f2 ON st.lam1_material = f2.material_id
										    LEFT JOIN material AS f3 ON st.lam2_material = f3.material_id
										    LEFT JOIN material AS f4 ON st.lam3_material = f4.material_id
										WHERE so.job_code = $job_code")->row();
                // var_dump($job); return;
                $title = "JOB CARD";
                $customer = htmlentities($job->customer_name);
                $job_name = htmlentities($job->job_name);
                $order_quantity = htmlentities($job->quantity);
                $unit = htmlentities(strtoupper($job->order_type));
                $meter = number_format($job->meter, 0);
                $date = pkDate($job->date);
                $po_date = htmlentities($job->po_date);
                $po_num = htmlentities($job->po_num);
                $job_code = htmlentities($job->job_code);
                $coa = htmlentities(ucfirst($job->coa_requird));
                $dc = htmlentities(ucfirst($job->dc));
                $remarks = htmlentities(ucfirst($job->remarks));
                $address = htmlentities(ucfirst($job->address));
                $company_rep = htmlentities(ucfirst(' '));
                // PRINTING SUBSTRATE
                $printing_material = htmlentities($job->printing_material);
                $printing_weight = number_format($job->printing_weight, 2);
                $type = htmlentities($job->print_as_per);
                // 1st LAMINATION SUBSTRATE
                $lamination1material = htmlentities($job->lm1_material);
                $lamination1weight = number_format($job->lam1_weight, 2);
                // 2nd LAMINATION SUBSTRATE
                $lamination2material = htmlentities($job->lm2_material);
                $lamination2weight = number_format($job->lam2_weight, 2);
                // 3rd LAMINATION SUBSTRATE
                $lamination3material = htmlentities($job->lm3_material);
                $lamination3weight = number_format($job->lam3_weight, 2);

                $customer = htmlentities($job->customer_name);

                $pdf = new fpdf("L", "mm", "A4");
                $pdf->SetMargins(1, 1, 1, 1);

                $pdf->SetTitle('this is title');
                $pdf->AddPage();
                $pdf->SetFillColor(233, 234, 237);
                $pdf->Image(base_url('assets/img/logo.jpg'), 10, 1, 15);
                $pdf->SetFont('Arial', '', 14);
                $pdf->Cell(0, 4, "", 0, 1);
                $pdf->Cell(235, 10, "                   " . $title, 0, 0, 'C');

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(27, 8, "Date: ", 1, 0, 'L');
                $pdf->Cell("", 8, $date, 1, 1, 'L');

                $pdf->SetFont('Courier', '', 9);
                $pdf->Cell(27, 10, "Customer Name: ", 0, 0, 'L');
                $pdf->SetFont('Courier', 'U', 9);
                $pdf->Cell(55, 10, $customer, 0, 0, 'L');

                $pdf->SetFont('Courier', '', 9);
                $pdf->Cell(20, 10, "Job Name: ", 0, 0, 'L');
                $pdf->SetFont('Courier', 'U', 9);
                $pdf->Cell(65, 10, strtoupper($job_name), 0, 0, 'L');

                $pdf->SetFont('Courier', '', 9);
                $pdf->Cell(30, 8, "Order Quantity: ", 0, 0, 'L');
                $pdf->Cell(38, 8, number_format($order_quantity, 2) . " " . $unit, 0, 0, 'L');

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(27, 8, "PO #: ", 1, 0, 'L');
                $pdf->Cell("", 8, $po_num, 1, 1, 'L');

                $pdf->SetFont('Arial', 'U', 14);
                $pdf->Cell(235, 10, "                   JOB DETAILS                   ", 0, 0, 'C');

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(27, 8, "Job Code: ", 1, 0, 'L');
                $pdf->Cell("", 8, $job_code, 1, 1, 'L');
                // $pdf->Ln();

                $pdf->Cell(50, 16, "Structure, Size & Quantity: ", 1, 0, 'C');

                $pdf->Cell(65, 8, "Printing Substrate: ", 1, 0, 'C');
                $pdf->Cell(50, 8, "1st Lamination Substrate: ", 1, 0, 'C');
                $pdf->Cell(50, 8, "2nd Lamination Substrate: ", 1, 0, 'C');
                $pdf->Cell(50, 8, "3rd Lamination Substrate: ", 1, 0, 'C');
                $pdf->Cell(0, 8, "File Number: ", 1, 0, 'C');
                $pdf->Ln();


                $pdf->SetFont('Arial', '', 7);
                $pdf->Cell(50, 8, "", 0, 0);

                $pdf->Cell(35, 8, ($printing_material) ? $printing_material : "-", 1, 0, 'C');
                $pdf->Cell(15, 8, $printing_weight . " KG", 1, 0, 'C');
                $pdf->Cell(15, 8, ($meter) ? $meter . " mtr" : "-", 1, 0, 'C');

                $pdf->Cell(35, 8, ($lamination1material != 'Choose Material') ? $lamination1material : "-", 1, 0, 'C');
                $pdf->Cell(15, 8, ($lamination1weight != 0) ? $lamination1weight . " KG" : '-', 1, 0, 'C');

                $pdf->Cell(35, 8, ($lamination2material != 'Choose Material') ? $lamination2material : "-", 1, 0, 'C');
                $pdf->Cell(15, 8, ($lamination2weight != 0) ? $lamination2weight . " KG" : '-', 1, 0, 'C');

                $pdf->Cell(35, 8, ($lamination3material != 'Choose Material') ? $lamination3material : "-", 1, 0, 'C');
                $pdf->Cell(15, 8, ($lamination3weight != 0) ? $lamination3weight . " KG" : '-', 1, 0, 'C');

                $pdf->Cell(0, 8, $job->file_number, 1, 1, 'C');

//                $pdf->Cell('', 3, "", 0, 1); // set margin

                $pdf->Cell(85, 6, 'DEPART 1', 1, 0, 'C');
                $pdf->Cell(210, 6, 'DEPART 2', 1, 1, 'C');

                $pdf->Cell(85, 6, 'FILM STORE', 1, 0, 'C', 1);
                $pdf->Cell(60, 6, 'PRINTING', 1, 0, 'C', 1);
                $pdf->Cell(150, 6, 'RECOMMENDED INKS', 1, 1, 'C', 1);

                $pdf->Cell(35, 8, 'DEPART', 1, 0, 'C');
                $pdf->Cell(25, 8, 'ISSUE', 1, 0, 'C');
                $pdf->Cell(25, 8, 'RETURN', 1, 0, 'C');

                $pdf->Cell(60, 8, 'PRINT AS PER', 1, 0, 'C');

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(5, 8, 'S#', 1, 0, 'C');
                $pdf->Cell(20, 8, 'COLORS', 1, 0, 'C');
                $pdf->Cell(20, 8, 'INK SERIES', 1, 0, 'C');
                $pdf->Cell(20, 8, 'NAME', 1, 0, 'C');
                $pdf->Cell(20, 8, 'QTY', 1, 0, 'C');
                $pdf->Cell(20, 8, 'USED', 1, 0, 'C');
                $pdf->Cell(45, 8, 'REMARKS', 1, 1, 'C');

                // first line
                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(35, 6, 'PRINTING', 1, 0, 'l');
                $pdf->Cell(25, 6, '', 1, 0, 'C');
                $pdf->Cell(25, 6, '', 1, 0, 'C');

                $pdf->Cell(40, 6, 'PARTY APPROVAL', 1, 0, 'L');
                $pdf->Cell(20, 6, ($type == 'Party Approval') ? 'Yes' : '', 1, 0, 'L');

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(5, 6, '1', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->SetFont('Arial', '', 6);
                $pdf->Cell(45, 6, '', 1, 1, 'L');

                // second line
                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(35, 6, '1st Lamination', 1, 0, 'l');
                $pdf->Cell(25, 6, '', 1, 0, 'C');
                $pdf->Cell(25, 6, '', 1, 0, 'C');

                $pdf->Cell(40, 6, 'AMENDMENT', 1, 0, 'L');
                $pdf->Cell(20, 6, ($type == 'Amendment') ? 'Yes' : '', 1, 0, 'C');

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(5, 6, '2', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->SetFont('Arial', '', 6);
                $pdf->Cell(45, 6, '', 1, 1, 'L');

                // third line
                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(35, 6, '2nd Lamination', 1, 0, 'l');
                $pdf->Cell(25, 6, '', 1, 0, 'C');
                $pdf->Cell(25, 6, '', 1, 0, 'C');

                $pdf->Cell(40, 6, 'Approved Sample', 1, 0, 'L');
                $pdf->Cell(20, 6, ($type == 'Approved Sample') ? 'Yes' : '', 1, 0, 'L');

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(5, 6, '3', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->SetFont('', '', 6);
                $pdf->Cell(45, 6, '', 1, 1, 'L');

                // forth line
                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(35, 6, '3rd Lamination', 1, 0, 'L');
                $pdf->Cell(25, 6, '', 1, 0, 'C');
                $pdf->Cell(25, 6, '', 1, 0, 'C');

                $pdf->Cell(40, 6, 'Last Production', 1, 0, 'L');
                $pdf->Cell(20, 6, ($type == 'Last Production') ? 'Yes' : '', 1, 0, 'C');

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(5, 6, '4', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->SetFont('Arial', '', 6);
                $pdf->Cell(45, 6, '', 1, 1, 'L');

                $pdf->Cell(85, 12, "REMARKS:", 1, 0); // set margin
                $pdf->Cell(40, 6, "MACHINE #:", 1, 0); // set margin
                $pdf->Cell(20, 6, "", 1, 0); // set margin

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(5, 6, '5', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->SetFont('Arial', '', 6);
                $pdf->Cell(45, 6, '', 1, 1, 'L');


                $pdf->Cell(85, 0, "", 0, 0); // set margin
                $pdf->Cell(40, 6, "DIRECTION: ", 1, 0); // set margin
                $pdf->Cell(20, 6, "", 1, 0); // set margin

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(5, 6, '6', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->SetFont('Arial', '', 6);
                $pdf->Cell(45, 6, '', 1, 1, 'L');

                $pdf->SetFont('Arial', '', 12);
                $pdf->Cell(145, 6, "LAMINATION", 1, 0, "C", 1); // set margin

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(5, 6, '7', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->SetFont('Arial', '', 6);
                $pdf->Cell(45, 6, '', 1, 1, 'L');

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(50, 6, '', 1, 0, 'C');
                $pdf->Cell(50, 6, 'STRUCTURE', 1, 0, 'C');
                $pdf->Cell(45, 6, 'TYPE', 1, 0, 'C');

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(5, 6, '8', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->Cell(20, 6, '', 1, 0, 'C');
                $pdf->SetFont('Arial', '', 6);
                $pdf->Cell(45, 6, '', 1, 1, 'L');

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(50, 6, '1st LAMINATION', 1, 0, 'L');
                $pdf->Cell(50, 6, '', 1, 0, 'C');
                $pdf->Cell(45, 6, '', 1, 0, 'C');

                $pdf->SetFont('Arial', '', 12);
                $pdf->Cell(150, 6, 'BAG MAKING', 1, 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(50, 6, '2nd LAMINATION', 1, 0, 'L');
                $pdf->Cell(50, 6, '', 1, 0, 'C');
                $pdf->Cell(45, 6, '', 1, 0, 'C');

                $pdf->Cell(75, 6, 'CUTTING SIZE', 1, 0, 'L');
                $pdf->Cell(75, 6, '', 1, 1, 'L');

                $pdf->Cell(50, 6, '3rd LAMINATION', 1, 0, 'L');
                $pdf->Cell(50, 6, '', 1, 0, 'C');
                $pdf->Cell(45, 6, '', 1, 0, 'C');

                $pdf->Cell(75, 6, 'ORDER QUANTITY', 1, 0, 'L');
                $pdf->Cell(75, 6, '', 1, 1, 'L');

                $pdf->SetFont('Arial', '', 12);
                $pdf->Cell(145, 6, "SLITTING", 1, 0, "C", 1);
                $pdf->Cell(150, 6, 'DISPATCH', 1, 1, 'C', 1);

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(75, 6, "REEL WIDTH", 1, 0, "L");
                $pdf->Cell(70, 6, "", 1, 0, "L");
                $pdf->Cell(35, 6, 'CONTACT PERSON', 1, 0, 'L');
                $pdf->Cell(115, 6, '', 1, 1, 'L');

                $pdf->Cell(75, 6, "CUT OF LENGTH", 1, 0, "L");
                $pdf->Cell(70, 6, "", 1, 0, "L");
                $pdf->Cell(35, 6, 'ADDRESS', 1, 0, 'L');
                $pdf->Cell(115, 6, $address, 1, 1, 'L');

                $pdf->Cell(75, 6, "DIRECTION", 1, 0, "L");
                $pdf->Cell(70, 6, "", 1, 0, "L");
                $pdf->Cell(35, 6, 'COA REQUIRED', 1, 0, 'L');
                $pdf->Cell(115, 6, $coa, 1, 1, 'L');

                $pdf->Cell(75, 6, "REEL OD", 1, 0, "L");
                $pdf->Cell(70, 6, "", 1, 0, "L");
                $pdf->Cell(35, 6, 'DC', 1, 0, 'L');
                $pdf->Cell(115, 6, $dc, 1, 1, 'L');

                $pdf->Cell(75, 6, "METER", 1, 0, "L");
                $pdf->Cell(70, 6, "", 1, 0, "L");
                $pdf->Cell(35, 6, 'COMPANY REP.', 1, 0, 'L');
                $pdf->Cell(115, 6, $company_rep, 1, 1, 'L');

                $pdf->Cell(75, 6, "PICES", 1, 0, "L");
                $pdf->Cell(70, 6, "", 1, 0, "L");
                $pdf->Cell(35, 6, 'REMARKS', 1, 0, 'L');
                $pdf->Cell(115, 6, $remarks, 1, 1, 'L');

                // SIGNATURE
                $pdf->Cell('', 6, '', 0, 1); // margin

                $pdf->Cell(10, 6, '', 0, 0); // margin
                $pdf->Cell(97, 7, 'Approved By: _________________________________', 0, 0, 'L');
                // $pdf->Cell(97,7,'Production Manager: _________________________________',0,0,'L');
                $created_by = new User_Model();
                $created_by->load($job->created_by);
                $pdf->cell(97, 6, 'Prepare By: ' . ($created_by->user_name) . " (" . created_time($job->created_time) . ")");
                $pdf->Cell(97, 7, 'Operation Director: _________________________________', 0, 0, 'L');

                $pdf->SetXY(190,5);
                $pdf->SetFont('Arial','BU',9);
                $pdf->Cell(30, 7, 'PTP.FM.PR.004', 0, 0, 'R');

                $fileName = "job_card_" . $job_name;
                (isset($_GET['download']) && $_GET['download'] == 'true') ? $pdf->Output($fileName . '.pdf', 'D') : $pdf->Output();


            } // if id isset
            else {
                $this->load->view('report/job_card_form');
            }

        } else { // if roll exist
            echo "Your are not allowed for this view";
        }

    } // job_card()

    /**************************************************
     *                    =sale order.
     **************************************************/

    public function sale_order($job_code)
    {

        if (User_Model::hasAccess('viewRotoSaleOrder')) {

            if (isset($job_code)) {

                $material = new Material_Model();
                $structure = new Job_Master_Model();
                $job = $this->Sale_Order_Model->get_order_by_job($job_code);
                // var_dump($job);
                $structure->load($job->structure_id);

                $material->load($structure->print_material);
                if ($structure->print_material) {
                    $material->load($structure->print_material);
                    $print_scock = $material->get_current_stock($material->material_id);
                } else {
                    $print_scock = '0.00';
                }

                if ($structure->lam1_material) {
                    $material->load($structure->lam1_material);
                    $lam1_scock = $material->get_current_stock($material->material_id);
                } else {
                    $lam1_scock = '0.00';
                }

                if ($structure->lam2_material) {
                    $material->load($structure->lam2_material);
                    $lam2_scock = $material->get_current_stock($material->material_id);
                } else {
                    $lam2_scock = '0.00';
                }

                if ($structure->lam3_material) {
                    $material->load($structure->lam3_material);
                    $lam3_scock = $material->get_current_stock($material->material_id);
                } else {
                    $lam3_scock = '0.00';
                }

                $pdf = new fpdf("P", "mm", "A4");
                $pdf->SetMargins(15, 10);

                $pdf->AddPage();
                $pdf->SetFillColor(233, 234, 237);
                $pdf->Image(base_url('assets/img/logo.jpg'), 15, 10, 30, 25);
                // $pdf->ln(20);
                $pdf->SetFont('Arial', 'U', 18);
                $pdf->SetTitle('Sale Order');
                $pdf->Cell(0, 4, "", 0, 1);
                $pdf->Cell(0, 10, "" . "SALE ORDER", 0, 0, 'C');
                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(0, 10, "" . "PTP.FM.PR.002", 0, 1, 'R');

                $pdf->ln(28);

                $f1 = $job->printing_material ? $job->printing_material . " " . number_format($job->printing_weight, 2) . " kg" : '';
                $f2 = $job->lm1_material ? $job->lm1_material . " " . number_format($job->lam1_weight, 2) . " kg" : '';
                $f3 = $job->lm2_material ? $job->lm2_material . " " . number_format($job->lam2_weight, 2) . " kg" : '';
                $f4 = $job->lm3_material ? $job->lm3_material . " " . number_format($job->lam3_weight, 2) . " kg" : '';
                $approved = $job->aproved_by ? $job->aproved_by : '';
                $datas = [
                    "Date:" => pkDate($job->po_date),
                    "Delivery Date:" => pkDate($job->delivery_date),
                    "Job #:" => $job->job_code,
                    "PO #:" => $job->po_num,
                    "Party:" => $job->customer_name,
                    "Item:" => $job->job_name,
                    "Structure:" => ($f1) ? $f1 . "  ( In Stock $print_scock )" : '',
                    " " => ($f2) ? $f2 . "  ( In Stock $lam1_scock )" : '',
                    "  " => ($f3) ? $f3 . "  ( In Stock $lam2_scock )" : '',
                    "   " => ($f4) ? $f4 . "  ( In Stock $lam3_scock )" : '',
                    "Quantity:" => number_format($job->quantity, 2),
                    "Rate:" => number_format($job->rate, 2) . " PKR",
                    "Cylinder:" => "",
                    "Cost:" => "",
                    "Finish Goods:" => ""
                ];

                $height = 15;
                foreach ($datas as $key => $value) {
                    if (empty(str_replace(" ", "", $key)) && empty($value)) {
                        $height += 15;
                        continue;
                    }
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->cell(45, 12, strtoupper($key), 0, 0);
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->cell(60, 12, $value, 0, 1);
                }

                if ($job->description) {
                    $pdf->setXY(125, 55);
                    // $pdf->cell(75,8,'PRINTING',1,1,"C");
                    $pdf->cell(75, 8, 'Instructions', 1, 1, "C");
                    $pdf->setXY(125, 63);
                    $pdf->MultiCell(75, 7, $job->description, $border = 1);
                }

                $pdf->setXY(15, 220);

                $pdf->setXY(15, 250);
                $pdf->SetAutoPageBreak(false, $margin = 0);
                $pdf->SetFont('Arial', 'B', 12);
                $pdf->cell(100, 10, 'Checked By: ______________________________');
                $pdf->cell(30, 10, 'Approved By:     ' . strtoupper($approved));
                $pdf->Ln();
                // $pdf->setX(110);
                $pdf->SetFont('courier', 'U', 8);
                $pdf->cell(30, 10, 'Prepare By: ' . ($job->created_by) . " (" . created_time($job->created_time) . ")");

                $this->sro($pdf);

                (isset($_GET['download']) && $_GET['download'] == 'true') ? $pdf->Output('Sale Order.pdf', 'D') : $pdf->Output();

            } // if id isset

        } else { // if roll exist
            echo "Your are not allowed for this view";
        }

    } // sale_order()


    public function sro($pdf){
//        echo $this->getX() .' x, '. $this->getY() . 'y';
        $pdf->SetX(10.00125);
        $pdf->SetY(280);

        $pdf->SetFont('ARIAL','',6);
        $pdf->Cell(30,3,"",0,0,"C");
        $pdf->Cell(15,3,"This Revision",1,0,"C");
        $pdf->Cell(15,3,"0",1,0,"C");
        $pdf->Cell(30,3,"Revised By",1,0,"C");
        $pdf->Cell(25,3,"Production Manager",1,0,"C");
        $pdf->Cell(15,3,"",1,0,"C");
        $pdf->Cell(15,3,"",1,0,"C");
        $pdf->Cell(50,3,"",0,1,"C");

        $pdf->Cell(30,3,"",0,0,"C");
        $pdf->Cell(15,3,"This Revision",1,0,"C");
        $pdf->Cell(15,3,"01/03/2016",1,0,"C");
        $pdf->Cell(30,3,"Frequency",1,0,"C");
        $pdf->Cell(25,3,"Two Years",1,0,"C");
        $pdf->Cell(15,3,"Next Rev Due",1,0,"C");
        $pdf->Cell(15,3,"01/03/2018",1,0,"C");
        $pdf->Cell(50,3,"",0,1,"C");

        $pdf->Cell(30,3,"",0,0,"C");
        $pdf->Cell(15,3,"Approve by",1,0,"C");
        $pdf->Cell(45,3,"Operation Director",1,0,"C");
        $pdf->Cell(25,3,"Approval Date",1,0,"C");
        $pdf->Cell(15,3,"",1,0,"C");
        $pdf->Cell(15,3,"01/03/2016",1,0,"C");
        $pdf->Cell(50,3,"",0,1,"C");

        $pdf->SetFont('ARIAL','B',6);
        $pdf->Cell(0,3,"UNCONTROLLED ONCE PRINTED",0,1,"C");

    }

    /**************************************************
     *            =DATEWISE LAMINATION REPORT.
     **************************************************/

    public function datewise_lamination()
    {
        if (User_Model::hasAccess('rotoLaminationReport')) {

            if (isset($_POST['report'])) {

                // var_dump($_POST);
                $job_name = ($this->input->post('job_name') == "") ? '' :
                    " AND o.job_name LIKE '%" . escape_string($this->input->post('job_name')) . "%'";

                $to = $this->input->post('to');
                $from = $this->input->post('from');
                $job_code = isset($_POST['job_code']) ? $_POST['job_code'] : '';
                $machine = (isset($_POST['machine_name']) && $_POST['machine_name'] != "both") ? $_POST['machine_name'] : '';

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "PRODUCTION SUMMARY FOR LAMINATION (".strtoupper($_POST['machine_name']).")       " . pkDate($from) . " To " . pkDate($to) . $jobinTitle];

                // $material = new Material_Model();
                $query_result = $this->Lamination_Model->datewise_lamination($from, $to, $job_code, $machine);

                $pdf = new PDF_table("L", 'mm', 'A4', $option);
                $pdf->cFontSize = 8;
                $pdf->hFontSize = 9;
                // $pdf->headData = get_lamination_report_datewise($to,$from,$job_name);;
                $pdf->data = $query_result;
                $pdf->width = [16, 12, 50, 12, 25, 25, 25, 20, 15, 25, 25, 25, 15];
                $pdf->set_sum_col(['print weight', 'plain weight', 'glue', 'glue wt', 'weight out', 'weight gain', 'meter', 't wst']);
                $pdf->set_col_align(['print weight'=>"R", 'plain weight'=>"R", 'glue'=>"R", 'glue wt'=>"R", 'weight out'=>"R", 'weight gain'=>"R", 'meter'=>"R", 't wst'=>"R"]);
                $pdf->set_col_format(['print weight' => 2, 'plain weight' => 2, 'glue' => 2, 'glue wt' => 2, 'weight out' => 2, 'weight gain' => 2, 'meter' => 0, 't wst' => 2]);

//                $pdf->set_col_align(['B4 REW' => 'R', 'AFT. REW' => 'R', 'HOLD' => 'R', 'TRIM' => 'R', 'WAST' => 'R']);
//                $pdf->set_col_format(['B4 REW' => 2, 'AFT. REW' => 2, 'hold' => 2, 'TRIM' => 2, 'WAST' => 2]);

                $pdf->SetMargins(4, 5);
                $pdf->SetFont('Arial', '', 8);
                $pdf->AddPage();
                $pdf->FancyTable();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->Output();

            } else { // if id isset

                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $this->load->view('report/datewise_lamination', $data);

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // datewise_lamination()


    /**************************************************
     *            =DATEWISE LAMINATION REPORT.
     **************************************************/

    public function datewise_dispatch()
    {
        if (User_Model::hasAccess('rotoDispatchReport')) {

            if (isset($_POST['report'])) {

                // var_dump($_POST);
                $job_name = ($this->input->post('job_name') == "") ? '' :
                    " AND o.job_name LIKE '%" . escape_string($this->input->post('job_name')) . "%'";

                $to = $this->input->post('to');
                $from = $this->input->post('from');
                $job_code = isset($_POST['job_code']) ? $_POST['job_code'] : '';

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "Dispatch Reprot:     From   " . pkDate($from, '/') . "    To    " . pkDate($to, '/') . $jobinTitle];

                $query_result = $this->Dispatch_Model->datewise_dispatch($from, $to, $job_code);

                $pdf = new PDF_table("P", 'mm', 'A4', $option);
                $pdf->cFontSize = 8;
                $pdf->hFontSize = 9;
                $pdf->text_elipse = ['job name' => 35];
                $pdf->data = $query_result;
                $pdf->width = [16, 12, 80, 12, 15, 20, 15, 20];
                $pdf->set_sum_col(['NO PCS', 'gross', 'tare', 'net']);
                $pdf->set_col_align(['job #' => 'C', 'CH #' => 'C', 'NO PCS' => 'R', 'gross' => 'R', 'tare' => 'R', 'net' => 'R']);
                $pdf->set_col_format(['NO PCS' => 0, 'gross' => 2, 'tare' => 2, 'net' => 2]);

                $pdf->SetMargins(10, 5);
                $pdf->SetFont('Arial', '', 8);
                $pdf->AddPage();
                $pdf->FancyTable();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->Output();

            } else { // if id isset

                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $data['customers'] = $this->Customer_Model->get();
                $this->load->view('report/datewise_dispatch', $data);

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // datewise_dispatch()


    /**************************************************
     *            =DATEWISE SALE ORDER SUMMARY REPORT.
     **************************************************/

    public function sale_order_balance()
    {
        if (User_Model::hasAccess('rotoOrderBalanecReport')) {

            if (isset($_POST['report'])) {

                // var_dump($_POST);
                $job_name = ($this->input->post('job_name') == "") ? '' :
                    " AND o.job_name LIKE '%" . escape_string($this->input->post('job_name')) . "%'";

                $to = $this->input->post('to');
                $from = $this->input->post('from');
                $job_code = isset($_POST['job_code']) ? $_POST['job_code'] : '';

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "PRODUCTION SUMMARY FOR DISPATCH        " . pkDate($from) . " To " . pkDate($to) . $jobinTitle];

                $query_result = $this->Dispatch_Model->datewise_dispatch($from, $to, $job_code);

                $pdf = new PDF_table("P", 'mm', 'A4', $option);
                $pdf->cFontSize = 8;
                $pdf->hFontSize = 9;

                $pdf->data = $query_result;
                $pdf->width = [16, 12, 50, 12, 25, 25, 25, 20];

                $pdf->SetMargins(4, 5);
                $pdf->SetFont('Arial', '', 8);
                $pdf->AddPage();
                $pdf->FancyTable();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->Output();

            } else { // if id isset

                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $this->load->view('report/datewise_dispatch', $data);

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // sale_order_balance()


    /**************************************************
     *            =DATEWISE SALE ORDER SUMMARY REPORT.
     **************************************************/

    public function detail_order_balance()
    {
        if (User_Model::hasAccess('rotoPartyWiseOrderBalanecReport')) {

            if (isset($_POST['report'])) {

                // var_dump($_POST);
                $job_name = ($this->input->post('job_name') == "") ? '' :
                    " AND o.job_name LIKE '%" . escape_string($this->input->post('job_name')) . "%'";

                $to = $this->input->post('to');
                $from = $this->input->post('from');
                $job_code = isset($_POST['job_code']) ? $_POST['job_code'] : '';

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "PARTY WISE ORDER BALANCE        " . pkDate($from) . " To " . pkDate($to) . $jobinTitle];

                $head = [
                    'JOB #' => 15,
                    'PO #' => 25,
                    'PO DATE' => 20,
                    'DUE DATE' => 20,
                    'PRODUCT NAME' => 65,
                    'RATE' => 15,
                    'ORDER QTY' => 45,
                    'REF #' => 20,
                    'DATE' => 20,
                    'DELIVERED QTY' => 45,
                    'BALANCE' => 45
                ];

                $pdf = new PDF_Order_Balance("L", 'mm', 'LEGAL', $option);
                $pdf->set_title("Sale Order Balance");
                $pdf->set_duration(pkDate($from, '/'), pkDate($to, '/'));
                $pdf->set_header($head);

                $pdf->SetMargins(10, 5);
                $pdf->AddPage();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->SetLineWidth(0.1);

                $pdf->SetFont('Arial', '', 9);
                if ($this->input->post('customer_id')) {
                    $x = new Customer_Model();
                    $x->load($this->input->post('customer_id'));
                    $customers[] = $x;
                } else {
                    $customers = $this->Customer_Model->get();
                }


                // customer loop
                foreach ($customers as $customer) {
                    $grand_total = ['PRODUCT NAME' => 'Grand Total', 'ORDER KG' => 0, 'ORDER PCS' => 0, 'DELIVERED KG' => 0, 'DELIVERED PCS' => 0];

                    $query_result = $this->Dispatch_Model->detail_order_balance($from, $to, $job_code, $customer->customer_id);

                    if (!empty($query_result)) {

                        $pdf->SetFont('Arial', 'UB', 10);
                        $pdf->cell(0, 7, $customer->customer_name, 0, 1);

                        foreach ($query_result as $result) {
                            $party_total = ["PO #" => "Total", 'ORDER KG' => 0, 'ORDER PCS' => 0, 'DELIVERED KG' => 0, 'DELIVERED PCS' => 0, 'BALANCE KG' => 0, 'BALANCE PCS' => 0];

                            $pdf->SetFont('Arial', '', 8);
                            $dots = (strlen($result['PRODUCT NAME']) > 32) ? '...' : '';
                            $balancePCS = $result['PCS'];
                            $balanceKG = $result['KG'];
                            $balance = ($result['KG'] > 0) ? $result['KG'] : $result['PCS'];

                            $pdf->cell($head['JOB #'], 7, $result['job_code'], 0, 0);
                            $pdf->cell($head['PO #'], 7, $result['PO #'], 0, 0);
                            $pdf->cell($head['PO DATE'], 7, pkDate($result['PO DATE'], '/'), 0, 0);
                            $pdf->cell($head['DUE DATE'], 7, pkDate($result['DUE DATE'], '/'), 0, 0);
                            $pdf->cell($head['PRODUCT NAME'], 7, substr($result['PRODUCT NAME'], 0, 32) . $dots, 0, 0);
                            $pdf->cell($head['RATE'], 7, number_format($result['RATE'], 2) . ' ', 0, 0, 'R');
                            $pdf->cell($head['ORDER QTY'] / 2, 7, ($result['KG'] > 0) ? number_format($result['KG'], 2) : '-' . ' ', 0, 0, 'R');
                            $pdf->cell($head['ORDER QTY'] / 2, 7, ($result['PCS'] > 0) ? number_format($result['PCS'], 0) : '-' . ' ', 0, 0, 'R');
                            $pdf->cell($head['REF #'], 7, $result['REFERENCE #'], 0, 0);
                            $pdf->cell($head['DATE'], 7, '', 0, 0);
                            $pdf->cell($head['DELIVERED QTY'], 7, $result['DELIVERED QTY'], 0, 0);
                            // $pdf->cell($head['BALANCE'],7,number_format($balance).' ',0,0,'R');

                            $pdf->cell($head['BALANCE'] / 2, 7, ($result['KG'] > 0) ? number_format($balanceKG, 2) . ' ' : '', "B", 0, 'R');
                            $pdf->cell($head['BALANCE'] / 2, 7, ($result['PCS'] > 0) ? number_format($balancePCS) . ' ' : '', "B", 0, 'R');

                            $pdf->Ln();
                            $pdf->SetDrawColor(200, 200, 200);
                            $pdf->cell(0, 0, '', 1, 1);

                            $party_total['ORDER KG'] += $result['KG'];
                            $party_total['ORDER PCS'] += $result['PCS'];
                            $party_total['BALANCE KG'] += $result['KG'];
                            $party_total['BALANCE PCS'] += $result['PCS'];

                            $grand_total['ORDER KG'] += $result['KG'];
                            $grand_total['ORDER PCS'] += $result['PCS'];

                            $dispatches = $this->Dispatch_Model->find_associated_dispatches($result['job_code']);
                            $pdf->SetDrawColor(200, 200, 200);

                            // write all dispatches associated with order
                            foreach ($dispatches as $del) {
                                $balanceKG -= $del['KG'];
                                $balancePCS -= $del['PCS'];
                                $pdf->cell($head['JOB #'], 7, '', "B", 0);
                                $pdf->cell($head['PO #'], 7, '', "B", 0);
                                $pdf->cell($head['PO DATE'], 7, '', "B", 0);
                                $pdf->cell($head['DUE DATE'], 7, '', "B", 0);
                                $pdf->cell($head['PRODUCT NAME'], 7, '', "B", 0);
                                $pdf->cell($head['RATE'], 7, '', "B", 0, 'R');
                                $pdf->cell($head['ORDER QTY'], 7, '', "B", 0, 'R');
                                $pdf->cell($head['REF #'], 7, ' ' . $del['id'] . '   ', "B", 0, "R");
                                $pdf->cell($head['DATE'], 7, pkDate($del['date'], '/'), "B", 0);
                                $pdf->cell($head['DELIVERED QTY'] / 2, 7, ($del['KG'] > 0) ? number_format($del['KG'], 2) . ' ' : '', "B", 0, "R");
                                $pdf->cell($head['DELIVERED QTY'] / 2, 7, ($del['PCS'] > 0) ? number_format($del['PCS']) . ' ' : '', "B", 0, "R");
                                $pdf->cell($head['BALANCE'] / 2, 7, ($del['KG'] > 0) ? number_format($balanceKG, 2) . ' ' : '', "B", 0, 'R');
                                $pdf->cell($head['BALANCE'] / 2, 7, ($del['PCS'] > 0) ? number_format($balancePCS) . ' ' : '', "B", 0, 'R');
                                $pdf->Ln();

                                $party_total['DELIVERED KG'] += $del['KG'];
                                $party_total['DELIVERED PCS'] += $del['PCS'];

                                $party_total['BALANCE KG'] -= $del['KG'];
                                $party_total['BALANCE PCS'] -= $del['PCS'];

                                $grand_total['DELIVERED KG'] += $del['PCS'];
                                $grand_total['DELIVERED PCS'] += $del['PCS'];

                            } // inner query for dispatches

                            // Print Subtotal for indivitual order
                            if ($party_total['DELIVERED KG'] > 0 || $party_total['DELIVERED PCS'] > 0) {

                                $pdf->SetFont('Arial', 'BI', 8);

                                $pdf->cell($head['JOB #'], 7, "", 0, 0);
                                $pdf->cell($head['PO #'], 7, "", 0, 0);
                                $pdf->cell($head['PO DATE'], 7, "Total:", 0, 0);
                                $pdf->cell($head['DUE DATE'], 7, "", 0, 0);
                                $pdf->cell($head['PRODUCT NAME'], 7, "", 0, 0);
                                $pdf->cell($head['RATE'], 7, "", 0, 0, 'R');
                                $pdf->cell($head['ORDER QTY'] / 2, 7, ($party_total['ORDER KG'] > 0) ? number_format($party_total['ORDER KG'], 2) : '', 0, 0, 'R');
                                $pdf->cell($head['ORDER QTY'] / 2, 7, ($party_total['ORDER PCS'] > 0) ? number_format($party_total['ORDER PCS']) : '', 0, 0, 'R');
                                $pdf->cell($head['REF #'], 7, "", 0, 0);
                                $pdf->cell($head['DATE'], 7, '', 0, 0);
                                $pdf->cell($head['DELIVERED QTY'] / 2, 7, ($party_total['ORDER KG'] > 0) ? number_format($party_total['DELIVERED KG'], 2) . ' ' : '', 0, 0, 'R');
                                $pdf->cell($head['DELIVERED QTY'] / 2, 7, ($party_total['ORDER PCS'] > 0) ? number_format($party_total['DELIVERED PCS']) . ' ' : '', 0, 0, 'R');
                                $pdf->cell($head['BALANCE'] / 2, 7, ($party_total['ORDER KG'] > 0) ? number_format($party_total['BALANCE KG'], 2) . ' ' : '', 0, 0, 'R');
                                $pdf->cell($head['BALANCE'] / 2, 7, ($party_total['ORDER PCS'] > 0) ? number_format($party_total['BALANCE PCS']) . ' ' : '', 0, 0, 'R');

                                $pdf->Ln();
                            } // end if any delivery made  by current sale order

                            $pdf->SetDrawColor(100, 100, 100);
                            $pdf->cell(0, 0, '', 1, 1); // double under line after each party

                            $pdf->SetDrawColor(100, 100, 100);
                            $pdf->cell(0, 0, '', 1, 1); // double under line after each party

                        }// outer query loop
                        $pdf->cell(0, 0.5, '', 1, 1);
                        $pdf->cell(0, 2, '', 0, 1); // for litle space after each set of record

                        // Print Grand Total
                        $pdf->SetFont('Arial', 'BI', 9);

                        $pdf->cell($head['JOB #'], 7, "", 0, 0);
                        $pdf->cell($head['PO #'], 7, "", 0, 0);
                        $pdf->cell($head['PO DATE'], 7, ":", 0, 0);
                        $pdf->cell($head['DUE DATE'], 7, "", 0, 0);
                        $pdf->cell($head['PRODUCT NAME'], 7, "GRAND TOTAL", 0, 0, 'R');
                        $pdf->cell($head['RATE'], 7, "", 0, 0, 'R');
                        $pdf->cell($head['ORDER QTY'] / 2, 7, ($grand_total['ORDER KG'] > 0) ? number_format($grand_total['ORDER KG'], 2) : '', 0, 0, 'R');
                        $pdf->cell($head['ORDER QTY'] / 2, 7, ($grand_total['ORDER PCS'] > 0) ? number_format($grand_total['ORDER PCS']) : '', 0, 0, 'R');
                        $pdf->cell($head['REF #'], 7, "", 0, 0);
                        $pdf->cell($head['DATE'], 7, '', 0, 0);
                        $pdf->cell($head['DELIVERED QTY'] / 2, 7, ($grand_total['DELIVERED KG'] > 0) ? number_format($grand_total['DELIVERED KG'], 2) . ' ' : '', 0, 0, 'R');
                        $pdf->cell($head['DELIVERED QTY'] / 2, 7, ($grand_total['DELIVERED PCS'] > 0) ? number_format($grand_total['DELIVERED PCS']) . ' ' : '', 0, 0, 'R');
                        $pdf->cell($head['BALANCE'] / 2, 7, ($grand_total['ORDER KG'] > 0) ? number_format($grand_total['ORDER KG'] - $grand_total['DELIVERED KG'], 2) . ' ' : '', 0, 0, 'R');
                        $pdf->cell($head['BALANCE'] / 2, 7, ($grand_total['ORDER PCS'] > 0) ? number_format($grand_total['ORDER PCS'] - $grand_total['DELIVERED PCS']) . ' ' : '', 0, 0, 'R');

                        $pdf->Ln();

                    } // if query_result Not EMPTY

                } // end customer foreach loop


                $pdf->Output();


            } else { // if id isset
                $detail = true;
                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $data['customers'] = $this->Customer_Model->get();

                if ($this->input->is_ajax_request()) {
                    $this->load->view('report/detail_sale_order_balance', $data);
                    // var_dump($_SERVER);
                } else {
                    $data['site_title'] = 'Sale Order Balance Report In Detail.';
                    $this->load->view('main/header', $data);
                    $this->load->view('main/navigation', $data);
                    $this->load->view('main/rightNavigation', $data);
                    $this->load->view('main/topbar', $data);
                    $this->load->view('report/detail_sale_order_balance', $data);
                    $this->load->view('main/footer', $data);
                }

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // detail_order_balance()


    /**************************************************
     *            =ORDER BALANCE REPORT.
     **************************************************/

    public function order_balance()
    {
        if (User_Model::hasAccess('rotoOrderBalanecReport')) {

            if (isset($_POST['report'])) {

                $to = $this->input->post('to');
                $from = $this->input->post('from');
                $job_code = isset($_POST['job_code']) ? $_POST['job_code'] : '';
                $customer_id = isset($_POST['customer_id']) ? $_POST['customer_id'] : '';

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "SALE ORDER BALANCE:  From " . pkDate($from, '/') . " To " . pkDate($to, '/') . $jobinTitle];

                $query_result = $this->Dispatch_Model->order_balance($from, $to, $job_code, $customer_id);

                $pdf = new PDF_table("L", 'mm', 'A4', $option);
                $pdf->cFontSize = 9;
                $pdf->hFontSize = 10;

                $pdf->data = $query_result;
                $pdf->width = [20, 15, 23, 20, 50, 75, 25, 25, 25];

                $pdf->SetMargins(10, 5);
                $pdf->SetFont('Arial', '', 8);
                $pdf->AddPage();
                $pdf->FancyTable();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->Output();

            } else { // if id isset

                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $data['customers'] = $this->Customer_Model->get();
                $this->load->view('report/order_balance', $data);

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // order_balance()

    /**************************************************
     *            =WASTAGE REPORT.
     **************************************************/

    public function wastage_report()
    {
        if (User_Model::hasAccess('rotoWastageReport')) {

            $from = $this->input->post('from');
            $to = $this->input->post('to');
            $machine = $this->input->post('machine');
            // echo($this->db->last_query()); return;
            $filename = "JobStatus From {$from} TO {$to}";


            if (isset($_POST['PDF'])) {

                $jobs = $this->Sale_Order_Model->get_wastage_report_by_date($from, $to, $machine, 'array', false);
                $job_name = ($this->input->post('job_name') == "") ? '' :
                    " AND o.job_name LIKE '%" . escape_string($this->input->post('job_name')) . "%'";

                $to = $this->input->post('to');
                $from = $this->input->post('from');
                $job_code = isset($_POST['job_code']) ? $_POST['job_code'] : '';

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "JOB STATUS FROM " . pkDate($from) . " To " . pkDate($to) . $jobinTitle];

                // $material = new Material_Model();
                // $query_result = $this->Lamination_Model->datewise_lamination($from,$to,$job_code);

                $pdf = new PDF_table("L", 'mm', 'A4', $option);
                $pdf->cFontSize = 7;
                $pdf->hFontSize = 8;
                // $pdf->headData = get_lamination_report_datewise($to,$from,$job_name);;
                $pdf->data = $jobs;
                $pdf->width = [16, 10, 35, 10, 15, 15, 15, 20, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15];

                $pdf->SetMargins(4, 5);
                $pdf->SetFont('Arial', '', 8);
                $pdf->AddPage();
                $pdf->FancyTable();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->Output();

            } elseif (isset($_POST['excel'])) {


                try {
                    $jobs = $this->Sale_Order_Model->get_wastage_report_by_date($from, $to, $machine);
                    /** Error reporting */
                    error_reporting(E_ALL);
                    ini_set('display_errors', TRUE);
                    ini_set('display_startup_errors', TRUE);
                    date_default_timezone_set('Europe/London');

                    if (PHP_SAPI == 'cli')
                        die('This example should only be run from a Web Browser');

                    $borders = array(
                        'top' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THICK,
                            'color' => array(
                                'rgb' => '999999'
                            )
                        ),
                        'bottom' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array(
                                'rgb' => '999999'
                            )
                        ),
                        'left' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array(
                                'rgb' => '999999'
                            )
                        ),
                        'right' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => array(
                                'rgb' => '999999'
                            )
                        ),
                    );
                    $headingStyle = array(
                        'font' => array(
                            'name' => 'Century',
                            'bold' => true,
                            'color' => array(
                                'rgb' => '595959'
                            ),
                        ),
                        'alignment' => array(
                            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        ),
                        'borders' => $borders,
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'rotation' => 0,
                            'startcolor' => array(
                                'rgb' => 'ECEEE1',
                            ),
                            'endcolor' => array(
                                'rgb' => 'ECEEE1',
                            ),
                        ),
                    );

                    $sheet = new PHPExcel();

                    // Setting Properties
                    $sheet->getProperties()
                        ->setCreator("System Generated. (Printech Production Software.)")
                        ->setLastModifiedBy("System Generated. (Printech Production Software.)")
                        ->setTitle("Printech Packages PVT. LTD.")
                        ->setSubject("Job Status.")
                        ->setDescription("System Developed By: Syed Arif Iqbal. Contact# 03452650236, 03052068006")
                        // ->setKeywords("office 2007 openxml php")
                        ->setCategory("Excel Report.");

                    $sheet->getDefaultStyle()->getAlignment()->setVertical(
                        PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                    // PHPExcel_Style_Alignment::VERTICAL_CENTER
                    );
                    $sheet->getDefaultStyle()->getAlignment()->setHorizontal(
                        PHPExcel_Style_Alignment::VERTICAL_CENTER
                    // PHPExcel_Style_Alignment::VERTICAL_CENTER
                    );

                    $sheet->getDefaultStyle()->getFont()->setName("Century");
                    $sheet->getDefaultStyle()->getFont()->setSize("11");

                    $sheet->setActiveSheetIndex(0);
                    $activeSheet = $sheet->getActiveSheet();

                    $activeSheet->getDefaultRowDimension()->setRowHeight(25);

                    // Setting Print Options
                    $activeSheet->getPageSetup()->setOrientation(
                        PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE
                    )->setFitToWidth(1)
                        ->setFitToHeight(0);

                    // Setting Header And Footer
                    $activeSheet->getHeaderFooter()->setOddHeader("&C&B&16 " .
                        $sheet->getProperties()->getTitle())
                        ->setOddFooter("&CPage &P of &N");


                    $head1 = [
                        'P Date' => 10,
                        'Job Code' => 10,
                        'Job Name' => 26,
                        'Printing' => 9,
                        'g1' => '',
                        'g2' => '',
                        'g3' => '',
                        'Rewinding' => 9,
                        'g4' => '',
                        'g5' => '',
                        'Lamination1' => 9,
                        'g6' => '',
                        'g7' => '',
                        'g8' => '',
                        'Lamination2' => 9,
                        'g9' => '',
                        'g10' => '',
                        'g11' => '',
                        'Lamination3' => 9,
                        'g12' => '',
                        'g13' => '',
                        'g14' => '',
                        'Slitting' => 9,
                        'g15' => '',
                        'g16' => '',
                        'g17' => '',
                        'Dispatch' => 9,
                        'g18' => '',
                        'Wastage' => 7,
                        '%' => 5
                    ];
                    $head2 = [
                        'Date' => '',
                        'code' => '',
                        'Job' => '',
                        'print_Mc#' => 5,
                        'print_Plain' => 13,
                        'print_print' => 13,
                        'print_wstg' => 9,
                        'rewind_wght in' => 13,
                        'rewind_wght out' => 13,
                        'rewind_wstg' => 9,
                        'lam1_wght in' => 13,
                        'lam1_pln wt' => 13,
                        'lam1_wght out' => 13,
                        'lam1_wstg' => 9,
                        'lam2_wght in' => 13,
                        'lam2_pln wt' => 13,
                        'lam2_wght out' => 13,
                        'lam2_wstg' => 9,
                        'lam3_wght in' => 13,
                        'lam3_pln wt' => 13,
                        'lam3_wght out' => 13,
                        'lam3_wstg' => 9,
                        'slit_wght in' => 13,
                        'slit_wght out' => 11,
                        'slit_wast' => 8,
                        'slit_Trim' => 10,
                        'dis_wght in' => 11,
                        'dis_wght out' => 11,
                        '_Total Wast' => 7,
                        '_%' => 5
                    ];


                    if ($jobs) {

                        // First Line Of Heading
                        $col = 'A';
                        $row = 2;
                        foreach ($head1 as $colName => $width) {

                            if ($width == '') {
                                $col++;
                            } else {
                                $activeSheet->setCellValue($col . $row, $colName);
                                // $activeSheet->getStyle($col.$row)->getFont()->setBold(true);
                                $activeSheet->getStyle($col . $row)->applyFromArray($headingStyle);
                                $activeSheet->getRowDimension($row)->setRowHeight(25);
                                $activeSheet->getColumnDimension($col)->setWidth($width);
                                $col++;
                            }

                        }

                        // Second Line Of Heading
                        $col = "A";
                        $row = 3;
                        foreach ($head2 as $colName => $width) {
                            if ($width == '') {
                                $col++;
                            } else {
                                $ss = substr($colName, strpos($colName, '_') + 1);
                                $colName = $ss;
                                $activeSheet->setCellValue($col . $row, ucwords($colName));
                                // $activeSheet->getStyle($col.$row)->getFont()->setBold(true);
                                $activeSheet->getStyle($col . $row)->applyFromArray($headingStyle);
                                $activeSheet->getRowDimension($row)->setRowHeight(25);
                                $activeSheet->getColumnDimension($col)->setWidth($width);
                                $col++;
                            }
                        }

                        // Merging Heading First and Second Line Ad Needed
                        $activeSheet->mergeCells('A2:A3');
                        $activeSheet->getStyle('A2:A3')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('B2:B3');
                        $activeSheet->getStyle('B2:B3')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('C2:C3');
                        $activeSheet->getStyle('C2:C3')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('D2:G2');
                        $activeSheet->getStyle('D2:G2')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('H2:J2');
                        $activeSheet->getStyle('H2:J2')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('K2:N2');
                        $activeSheet->getStyle('K2:N2')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('O2:R2');
                        $activeSheet->getStyle('O2:R2')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('S2:V2');
                        $activeSheet->getStyle('S2:V2')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('W2:Z2');
                        $activeSheet->getStyle('W2:Z2')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('AA2:AB2');
                        $activeSheet->getStyle('AA2:AB2')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('AC2:AC3');
                        $activeSheet->getStyle('AC2:AC3')->applyFromArray($headingStyle);
                        $activeSheet->mergeCells('AD2:AD3');
                        $activeSheet->getStyle('AD2:AD3')->applyFromArray($headingStyle);


                        // Write Job Status Job Numbers in cell "A1"
                        $activeSheet->setCellValue("A1", "Job Status Date From {$from} Job Number(s) To {$to}");
                        $activeSheet->getStyle("A1")->getAlignment()->setHorizontal(
                            PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                        );
                        $activeSheet->getRowDimension("A1")->setRowHeight(25);

                        // Write Cell Data
                        $row = 4;
                        foreach ($jobs as $job) {
                            $col = "A";

                            foreach ($job as $field) {
                                $value = str_replace("Machine ", '', $field);
                                $value = $value ? $value : 0;
                                $activeSheet->setCellValue($col . $row, $value);
                                $activeSheet->setCellValue('AC' . $row, '=SUM(G' . $row . ',J' . $row . ',N' . $row . ',R' . $row . ',V' . $row . ',Z' . $row . ')');
                                $activeSheet->setCellValue('AD' . $row, '=IFERROR(AC' . $row . '/SUM(E' . $row . ',L' . $row . ',P' . $row . ',T' . $row . ')*100,0)');
                                $activeSheet->getRowDimension($row)->setRowHeight(25);
                                // $activeSheet->getStyle($col.$row)->getFont()->setBold(true);
                                $col++;
                            }

                            $row++;
                        }

                        // Write Formula in the end
                        $row4mula = $row - 1;
                        $activeSheet->setCellValue('E' . $row, '=SUM(E4:E' . $row4mula . ')');
                        $activeSheet->setCellValue('F' . $row, '=SUM(F4:F' . $row4mula . ')');
                        $activeSheet->setCellValue('G' . $row, '=SUM(G4:G' . $row4mula . ')');
                        $activeSheet->setCellValue('H' . $row, '=SUM(H4:H' . $row4mula . ')');
                        $activeSheet->setCellValue('I' . $row, '=SUM(I4:I' . $row4mula . ')');
                        $activeSheet->setCellValue('J' . $row, '=SUM(J4:J' . $row4mula . ')');
                        $activeSheet->setCellValue('K' . $row, '=SUM(K4:K' . $row4mula . ')');
                        $activeSheet->setCellValue('L' . $row, '=SUM(L4:L' . $row4mula . ')');
                        $activeSheet->setCellValue('M' . $row, '=SUM(M4:M' . $row4mula . ')');
                        $activeSheet->setCellValue('N' . $row, '=SUM(N4:N' . $row4mula . ')');
                        $activeSheet->setCellValue('O' . $row, '=SUM(O4:O' . $row4mula . ')');
                        $activeSheet->setCellValue('P' . $row, '=SUM(P4:P' . $row4mula . ')');
                        $activeSheet->setCellValue('Q' . $row, '=SUM(Q4:Q' . $row4mula . ')');
                        $activeSheet->setCellValue('R' . $row, '=SUM(R4:R' . $row4mula . ')');
                        $activeSheet->setCellValue('S' . $row, '=SUM(S4:S' . $row4mula . ')');
                        $activeSheet->setCellValue('T' . $row, '=SUM(T4:T' . $row4mula . ')');
                        $activeSheet->setCellValue('U' . $row, '=SUM(U4:U' . $row4mula . ')');
                        $activeSheet->setCellValue('V' . $row, '=SUM(V4:V' . $row4mula . ')');
                        $activeSheet->setCellValue('W' . $row, '=SUM(W4:W' . $row4mula . ')');
                        $activeSheet->setCellValue('X' . $row, '=SUM(X4:X' . $row4mula . ')');
                        $activeSheet->setCellValue('Y' . $row, '=SUM(Y4:Y' . $row4mula . ')');
                        $activeSheet->setCellValue('Z' . $row, '=SUM(Z4:Z' . $row4mula . ')');
                        $activeSheet->setCellValue('AA' . $row, '=SUM(AA4:AA' . $row4mula . ')');
                        $activeSheet->setCellValue('AB' . $row, '=SUM(AB4:AB' . $row4mula . ')');
                        $activeSheet->setCellValue('AC' . $row, '=SUM(AC4:AC' . $row4mula . ')');
                        $activeSheet->setCellValue('AD' . $row, '=SUM(AD4:AD' . $row4mula . ')');
                        $activeSheet->getStyle('E' . $row . ':AD' . $row)->getFont()->setBold(true);

                        $activeSheet->getStyle('A' . $row . ':AD' . $row)->getFill()->applyFromArray(array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'startcolor' => array(
                                'rgb' => 'cccccc'
                            )
                        ));
                        $activeSheet->getRowDimension($row)->setRowHeight(25);


                        // Setting Alignment of Job Name Column
                        $activeSheet->getStyle('C4:C' . $row)->getAlignment()->setHorizontal(
                            PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                        )->setWrapText(true);

                        // Setting Number Format for cell contain numbers
                        $activeSheet->getStyle('E4:AD' . $row)->getNumberFormat()->setFormatCode('[>0]#,##0.00 ;[<0]"[RED]("#,##0.00);" - ";@');
                        // $activeSheet->getStyle('E4:Y'.$row)->getNumberFormat()->setFormatCode('#,##0.00');

                        // Setting Alignment of cell contain numbers
                        $activeSheet->getStyle('D4:AD' . $row)->getAlignment()->setHorizontal(
                            PHPExcel_Style_Alignment::HORIZONTAL_RIGHT
                        );

                        // Freezing Column and Row at the top
                        $activeSheet->freezePane("D4");

                    } // end if jobs array not empty
                    else {
                        $activeSheet->setCellValue("B2", "No Job Found.!\n With this {$to} to this {$from} Job Number(s)");
                        $activeSheet->mergeCells('B2:H8');
                        $activeSheet->getStyle('B2')->getAlignment()->setWrapText(true);
                    }

                    $activeSheet->setTitle("Job Status");

                    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                    header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
                    header('Cache-Control: max-age=0');
                    // If you're serving to IE 9, then the following may be needed
                    header('Cache-Control: max-age=1');
                    // If you're serving to IE over SSL, then the following may be needed
                    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
                    header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                    header('Pragma: public'); // HTTP/1.0
                    ob_clean();
                    $writer = PHPExcel_IOFactory::createWriter($sheet, 'Excel2007');
                    $writer->save('php://output');
                    exit;

                } catch (Exception $e) {
                    $error = $e->getMessage();
                }

                if (isset($error)) {
                    echo($error);
                }

            } else { // if id isset

                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $this->load->view('report/wastage_report', $data);

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // wastage_report()


    /**************************************************
     *            =QUOTATION REPORT.
     **************************************************/

    public function quotation($id)
    {
        if (isset($id)) {

            $q = new Quotation_Model();
            $q->load($id);
            $title = 'PRINTECH PACKAGES PVT. LTD.';
            $tagline = 'F-656, Adjacent D-50, S.I.T.E, Karachi';
            $fs10 = 12;
            $inputs = [
                'product' => $q->product,
                'board GSM' => $q->gsm,
                'quality or brand' => $q->quality,
                'Structure' => $q->structure,
                'Size' => $q->size,
                'printing' => $q->printing,
                'other specifications' => $q->other_specification,
                'finishing' => $q->finishing,
                'price' => $q->price,
                'M.O.Q' => $q->moq,
                'Yield' => $q->yield,
                'Cylinder Cost' => $q->cylinder_cost,
            ];
            $term_condition = [
                'Lead time for delivery of material : ' . $q->condition_1,
                'Payment terms: ' . $q->condition_2,
                'Delivery point: ' . $q->condition_3
            ];
            if ($q->department == 'gravure'){
                $term_condition[] = 'Cylinder payment: ' . $q->condition_4;
                $inputs['Bag Making'] = $q->bag_making;
            }
            $term_condition[] = 'Validity of this quotation: ' . $q->condition_5;
            
            $concern = [
                $q->person_name,
                $q->designation,
                'Printech Packages (Pvt.) Ltd.',
                $q->contact
            ];

            $pdf = new fpdf("p", "mm", "A4");
            $pdf->SetMargins(15, 15);
            $pdf->AddPage();
            $pdf->SetFillColor(233, 234, 237);
            $pdf->Image(base_url('assets/img/logo.jpg'), 10, 10, 20);

            // $pdf->Image('img/logo.jpg',15,5,35);
            $pdf->SetFont('Arial', 'B', 18);
            $pdf->Cell(0, 4, $title, 0, 1, 'C');
            $pdf->ln();
            $pdf->SetFont('Arial', '', $fs10);
            $pdf->Cell(0, 4, $tagline, 0, 1, 'C');
            $pdf->ln(10);

            $pdf->SetFont('Arial', '', $fs10);
            $pdf->Cell(20, 12, "To", 0, 1, 'L');
            $pdf->Cell(20, 0, ucfirst($q->gender) . '. ' . $q->attention, 0, 0);

            $pdf->SetFont('Arial', 'UB', $fs10);
            $pdf->Cell(0, 4, 'Date: ' . PKdate($q->date), 0, 0, 'R');
            $pdf->ln(5);

            $pdf->SetFont('Arial', '', $fs10);
            $pdf->Cell(20, 4, $q->party, 0, 0, 'L');

            $pdf->SetFont('Arial', '', $fs10 - 2);
            $pdf->Cell(0, 4, 'Quotation #' . $q->id, 0, 0, 'R');
            $pdf->ln(10);

            $pdf->SetFont('Arial', 'UB', $fs10);
            $pdf->Cell(0, 4, "Subject: Quotation For {$q->product}", 0, 0);
            $pdf->ln(10);

            $pdf->SetFont('Arial', '', $fs10 - 1);
            $pdf->Cell(0, 4, ($q->gender == 'mr') ? 'Dear Sir,' : 'Dear Madam,', 0, 1);
            $pdf->ln(3);
            $pdf->Cell(0, 4, 'It is our utmost pleasure to furnish this quotation according to your query with the following specifications.', 0, 1);

            $pdf->SetFont('Arial', 'U', $fs10);
            $gp = ($q->department == 'gravure') ? 4 : 5;
            $table_line_height = ($q->department == 'gravure') ? 2 : 5;
            $pdf->Cell(0, $gp, '                                                                                                                                                        ', 0, 1);
            $pdf->ln($gp);

            foreach ($inputs as $input => $value) {
                if (trim($value)) {
                    $pdf->SetFont('Arial', 'B', $fs10);
                    $pdf->Cell(80, $table_line_height, ucwords($input), 0, 0);
                    $pdf->SetFont('Arial', '', $fs10);
                    $pdf->Cell(0, $table_line_height, ':         ' . ucwords($value), 0, 0);
                    $pdf->ln(($q->department == 'gravure') ? 6 : 7);
                }
            }

            $pdf->SetFont('Arial', 'U', $fs10);
            $pdf->Cell(0, $gp, '                                                                                                                                                         ', 0, 1);

            $pdf->SetFont('Arial', '', $fs10 - 2);
            $pdf->Cell(0, 10, 'Terms and conditions:', 0, 0);
            $pdf->ln(10);

            foreach ($term_condition as $term) {
                if (trim($term))
                    $pdf->Cell(0, 6, '         --       ' . $term, 0, 1);
            }
            $pdf->ln();

            $pdf->Cell(0, 6, 'Looking forward to a long and mutually beneficial business relationship.', 0, 1);
            $pdf->SetFont('Arial', '', $fs10 + 1);
            $pdf->Cell(0, $gp + 10, 'Thanking you', 0, 1);
            $pdf->SetFont('Arial', '', $fs10);
            $pdf->Cell(0, 8, 'Best Regards', 0, 1);

            foreach ($concern as $key) {
                $pdf->SetFont('Arial', 'B', $fs10);
                $pdf->Cell(0, 6, ucwords($key), 0, 1);
            }
            $pdf->ln($gp + 10);

            $pdf->SetFont('Arial', 'B', $fs10 - 2);
            // $pdf->Cell(10,6,'',0,0); // margin
            $pdf->Cell(97, 7, 'Approved By:   _________________________', 0, 0, 'L');
            $pdf->Cell(97, 7, 'Approved By Client:   _________________________', 0, 0, 'L');
            // $pdf->Cell(97,7,'Production Manager: _________________________________',0,0,'L');
            // $pdf->Cell(97,7,'Operation Director: _________________________________',0,0,'L');

            $fileName = "Quatation";
            (isset($_GET['download']) && $_GET['download'] == 'true') ? $pdf->Output($fileName . '.pdf', 'D') : $pdf->Output();


        } else { // if roll exist
            echo not_permitted();
        }

    } // quotation()


    /**************************************************
     *            =MONTHLY PRINTING REPORT.
     **************************************************/

    public function printing_monthly_report()
    {
        if (User_Model::hasAccess('rotoPrintingReport')) {

            if (isset($_POST['PDF'])) {
                // check($_POST);
                $to = $this->input->post('to');
                $from = $this->input->post('from');
                $machine = $this->input->post('machine');
                $job_code = isset($_POST['job_code']) ? $_POST['job_code'] : '';
                $customer_id = isset($_POST['customer_id']) ? $_POST['customer_id'] : '';

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "PRINTING PRODUCTION SUMMARY:  From " . pkDate($from, '/') . " To " . pkDate($to, '/') . $jobinTitle];

                $query_result = $this->Printing_Model->get_monthly_printing_report($from, $to, $machine, $job_code, $customer_id, 'array');

                $pdf = new PDF_table("L", 'mm', 'A4', $option);
                $pdf->cFontSize = 9;
                $pdf->hFontSize = 10;
                $pdf->set_sum_col(['P WT', 'PRT WT', 'WT GAIN', 'P WST', 'METER', 'PRT WST', 'T WST']);
                $pdf->set_col_align(['P WT' => 'R', 'PRT WT' => 'R', 'WT GAIN' => 'R', 'P WST' => 'R', 'METER' => 'R', 'PRT WST' => 'R', 'T WST' => 'R', '%' => 'R']);
                $pdf->set_col_format(['P WT' => 2, 'PRT WT' => 2, 'WT GAIN' => 2, 'P WST' => 2, 'METER' => 0, 'PRT WST' => 2, 'T WST' => 2, '%' => 2]);

                $pdf->set_machine($machine);
                $pdf->data = $query_result;
                $pdf->width = [20, 20, 80, 20, 20, 20, 20, 20, 20, 20, 15];

                $pdf->SetMargins(10, 5);
                $pdf->SetFont('Arial', '', 8);
                $pdf->AddPage();
                $pdf->FancyTable();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->Output();

            } else { // if id isset

                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $data['customers'] = $this->Customer_Model->get();
                $this->load->view('report/monthly_printing_report', $data);

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // printing_monthly_report()


    /**************************************************
     *            =PRODUCTION REPORT.
     **************************************************/

    public function production_report()
    {
        if (User_Model::hasAccess('rotoPrintingReport')) {

            if (isset($_POST['PDF'])) {
                $this->load->library('pdf_production_report');
                check($_POST);
                $date = $this->input->post('selected_date');
                $machine = $this->input->post('machine');

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "Date: " . pkDate($date, '/')];

                $print1 = $this->Printing_Model->get_daily_printing_report($date, 1, 'array');
                $print2 = $this->Printing_Model->get_daily_printing_report($date, 2, 'array');
                $print3 = $this->Printing_Model->get_daily_printing_report($date, 3, 'array');
                $dryLam = $this->Lamination_Model->get_daily_lamination_report($date, 'dl', 'array');
                $solLam = $this->Lamination_Model->get_daily_lamination_report($date, 'sl', 'array');

//                check($query_result);
                
                $pdf = new PDF_production_report("L", 'mm', 'A4', $option);
                $pdf->cFontSize = 7;
                $pdf->hFontSize = 7;
                $pdf->set_sum_col(['PROD. QTY',' P WT', 'PRT WT', 'WT GAIN', 'P WST', 'METER', 'PRT WST', 'T WST']);
                $pdf->set_col_align(['JOB NAME' => 'L', 'PROD. QTY' => 'R', 'T WST' => 'R', 'BL. QTY' => 'R', 'METER' => 'R', 'M. SPEED' => 'R', 'S. TIME' => 'R', 'E. TIME' => 'R']);
                $pdf->set_col_format(['PROD. QTY' => 2, 'T WST' => 2, 'BL. QTY' => 2, 'METER' => 0, 'M. SPEED' => 2]);
                $pdf->width = [82, 20, 20, 20, 20, 20, 20, 20, 20, 20, 15];

                $pdf->set_machine($machine);
                $pdf->add_table($print1, 'Printing Machine #1');                
                $pdf->add_table($print2, 'Printing Machine #2');
                $pdf->add_table($print3, 'Printing Machine #3');
                $pdf->add_table($dryLam, 'DRY Lamination');                
                $pdf->add_table($solLam, 'Solventless Lamination');

                $pdf->SetMargins(10, 5);
                $pdf->SetFont('Arial', '', 8);
                $pdf->AddPage();
                $pdf->FancyTable();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->Output();

            } else { // if id isset

                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $data['customers'] = $this->Customer_Model->get();
                $this->load->view('report/production_report', $data);

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // printing_monthly_report()


    /**************************************************
     *            =MONTHLY REWINDING REPORT.
     **************************************************/

    public function rewinding_monthly_report()
    {
        if (User_Model::hasAccess('rotoRewindingReport')) {

            if (isset($_POST['PDF'])) {

                $to = $this->input->post('to');
                $from = $this->input->post('from');
                $machine = $this->input->post('machine');
                $job_code = isset($_POST['job_code']) ? $_POST['job_code'] : '';
                $customer_id = isset($_POST['customer_id']) ? $_POST['customer_id'] : '';

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "REWINDING PRODUCTION SUMMARY:  From " . pkDate($from, '/') . " To " . pkDate($to, '/') . $jobinTitle];


                $query_result = $this->Rewinding_Model->get_monthly_rewinding_report($from, $to, $machine, $job_code, $customer_id, 'array');

                $pdf = new PDF_table("L", 'mm', 'A4', $option);
                $pdf->cFontSize = 9;
                $pdf->hFontSize = 10;
                $pdf->data = $query_result;
                $pdf->text_elipse = ['job name' => 35, 'material name' => 19];
                $pdf->width = [20, 15, 75, 40, 15, 20, 20, 15, 15, 15, 15, 15];
                $pdf->SetMargins(10, 5);
                $pdf->SetFont('Arial', '', 8);
                $pdf->AddPage();
                $pdf->set_sum_col(['B4 REW', 'AFT. REW', 'HOLD', 'TRIM', 'WAST']);
                $pdf->set_col_align(['B4 REW' => 'R', 'AFT. REW' => 'R', 'HOLD' => 'R', 'TRIM' => 'R', 'WAST' => 'R']);
                $pdf->set_col_format(['B4 REW' => 2, 'AFT. REW' => 2, 'hold' => 2, 'TRIM' => 2, 'WAST' => 2]);
                $pdf->FancyTable();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->Output();

            } else { // if id isset

                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $data['customers'] = $this->Customer_Model->get();
                $this->load->view('report/monthly_rewinding_report', $data);

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // rewinding_monthly_report()


    /**************************************************
     *            =MONTHLY SLITTING REPORT.
     **************************************************/

    public function slitting_monthly_report()
    {
        if (User_Model::hasAccess('rotoSlittingReport')) {

            if (isset($_POST['PDF'])) {

                $to = $this->input->post('to');
                $from = $this->input->post('from');
                $machine = $this->input->post('machine');
                $job_code = isset($_POST['job_code']) ? $_POST['job_code'] : '';
                $customer_id = isset($_POST['customer_id']) ? $_POST['customer_id'] : '';

                $jobinTitle = (isset($_POST['job_name']) == "") ? '' : " for ";
                $option = ['title' => "Printech Packages PVT LTD", 'subtitle' => "SLITTING PRODUCTION SUMMARY:  From " . pkDate($from, '/') . " To " . pkDate($to, '/') . $jobinTitle];

                $query_result = $this->Slitting_Model->get_monthly_rewinding_report($from, $to, $machine, $job_code, $customer_id, 'array');
                $pdf = new PDF_table("L", 'mm', 'A4', $option);
                $pdf->cFontSize = 9;
                $pdf->hFontSize = 10;
                $pdf->data = $query_result;
                $pdf->text_elipse = ['job name' => 35, 'material name' => 19];
                $pdf->width = [20, 15, 75, 18, 18, 18, 18, 18, 18, 18, 18, 18];
                $pdf->SetMargins(10, 5);
                $pdf->SetFont('Arial', '', 8);
                $pdf->AddPage();
                $pdf->set_sum_col(['B4 SL', 'AFT. SL', 'TRIM', 'WAST']);
                $pdf->set_col_align(['MCHN' => 'C', 'CUT SZ' => 'C', 'B4 SL' => 'R', 'AFT. SL' => 'R', 'TRIM' => 'R', 'WAST' => 'R']);
                $pdf->set_col_format(['B4 SL' => 2, 'AFT. SL' => 2, 'TRIM' => 2, 'WAST' => 2]);
                $pdf->FancyTable();
                $pdf->SetDisplayMode('fullwidth');
                $pdf->Output();

            } else { // if id isset

                $data['orders'] = $this->Sale_Order_Model->get_all_orders();
                $data['customers'] = $this->Customer_Model->get();
                $this->load->view('report/monthly_slitting_report', $data);

            }

        } else { // if roll exist
            echo not_permitted();
        }

    } // slitting_monthly_report()


    /**************************************************
     *            =MONTHLY SLITTING REPORT.
     **************************************************/

    public function maintenance_request($id)
    {
        if ($id) {

            $record = new Maintenance_Model();
            $record->load($id);

            $pdf = new FPDF();;
            $pdf->SetMargins(10, 5);
            $pdf->SetFont('Arial', 'BU', 16);
            $pdf->AddPage();
            $w=70; $h=10;
            $pdf->Cell(0,15,'Maintenance Request',0,1,"C");
            $pdf->SetFont('Arial', '', 10);

            $pdf->Cell($w,$h,'Req #');
            $pdf->Cell(0,$h,$record->id);
            $pdf->Ln();

            $pdf->Cell($w,$h,'Priority');
            $pdf->Cell(0,$h,$record->req_type);
            $pdf->Ln();

            $pdf->Cell($w,$h,'Request For');
            $pdf->Cell(0,$h,$record->req_for);
            $pdf->Ln();

            $pdf->Cell($w,$h,'Department');
            $pdf->Cell(0,$h,$record->request_from);
            $pdf->Ln();

            $pdf->Cell($w,$h,'Problem');
            $pdf->Ln();
            $pdf->Write($h/2,'      '.$record->problem);
            $pdf->Ln();

            $pdf->Cell($w,$h,'Request For');
            $pdf->Ln();
            $pdf->Write($h/2,'      '.$record->suggestion);
            $pdf->Ln();

            $pdf->Output();


        } else { // if roll exist
            echo not_permitted();
        }

    } // slitting_monthly_report()


    /**************************************************
     *            =QUOTATION REPORT BACKUP.
     **************************************************/

    public function offset_costing($id)
    {
        if (isset($id)) {
            $record = new Offset_Costing_Model();
            $record->load($id);

            $customer = new Customer_Model();
            $customer->load($record->customer_id);
            $record->customer_name = $customer->customer_name;


            if ($record->sale_tax > 0 && $record->show_both_amount == 1) {
                $alternet = new Offset_Costing_Model();
                $alternet->load($id);
                $alternet->sale_tax = 0;
                $board_cost = ' | ' . $alternet->get_total_board_cost();

                $final_cost = $alternet->get_final_cost();

            } else {
                $board_cost = '';
                $final_cost = '';
            }

            $left = ['BOARD' => number_format($record->get_total_board_cost(), 3) . $board_cost,
                'PRINTING' => number_format($record->get_printing_cost(), 3),
                'SORTING' => number_format($record->get_sorting_cost(), 3),
                'U.V' => number_format($record->get_uv_rate_per_pcs(), 3),
                'LAMINATION' => number_format($record->get_lamination_rate_per_pcs(), 3),
                'FOIL' => number_format($record->get_foil_cost(), 3),
                'DIE CUTTING' => ($record->embossing_rate > 0) ? number_format($record->get_die_cutting_cost(), 3) . ' | ' . number_format($record->get_embossing_cost(), 3) : number_format($record->get_die_cutting_cost(), 3),
                'PASTING' => number_format($record->get_pasting_cost(), 3),
                'PACKING' => number_format($record->get_packing_cost(), 3),
                'DELIVERY' => number_format($record->get_cartage_cost(), 3),
                'DEVELOPMENT' => number_format($record->get_development_cost_per_pcs(), 3),
                'total_' => number_format($record->get_total_cost_befor_margin(), 3),
                'MARGIN' => $record->margin . '%  |  ' . number_format($record->get_margin_amount(), 3),
                'TAX' => ($record->sale_tax > 0) ? $record->get_final_cost() : '-',
                'WITHOUT TAX' => ($record->sale_tax > 0 && $record->show_both_amount == 1) ? $final_cost : ''
            ];
            $board = [
                'SIZE' => $record->board_width . ' x ' . $record->board_height . ' | ' . $record->board_gsm . 'gsm',
                'KG' => number_format($record->get_board_weight(), 3),
                'RS' => $record->board_rate . '/=',
                ' %' => number_format($record->get_board_packet_cost(), 3),
                'PCS' => $record->no_unit_per_packet . ' | ' . number_format($record->get_board_per_pcs_cost(), 3),
                '%' => number_format($record->get_wastage_percentage(), 3),
                'TOTAL' => $record->get_total_board_cost() . $board_cost
            ];
            $development = [
                'FILM' => $record->get_film_cost(),
                'PLATES' => $record->get_plates_cost(),
                'DIE' => $record->get_die_cost(),
                'PROOFING' => $record->proofing,
                'BLOCK' => $record->block,
                'BLANKET' => $record->blanket,
                'FOIL' => $record->development_foil,
                'DESIGNING' => $record->designing,
                'TOTAL' => $record->get_development_cost(),
                '%' => $record->get_development_cost_per_pcs(),
            ];

            $title = 'PRINTECH PACKAGES (PVT.) LTD.';
            $tagline = 'OFFSET DEPARTMENT';

            $pdf = new fpdf("p", "mm", "A4");
            $pdf->SetMargins(15, 15);
            $pdf->AddPage();
            $pdf->SetFillColor(233, 234, 237);
            $pdf->Image(base_url('assets/images/costing_format.jpg'), 10, 10, 190);
            $pdf->SetLineWidth(0.5);

            $pdf->SetXY(17, 15);
            $pdf->SetFont('Arial', '', 12);
            $pdf->Cell(26, 4, pkDate($record->date, '/'), 0, 1, 'C');

            $pdf->SetXY(50, 28);
            $pdf->cell(128, 10, strtoupper($record->customer_name), 0, 0);

            $pdf->SetXY(50, 36);
            $pdf->cell(128, 10, strtoupper($record->item_name), 0, 0);

            $pdf->SetXY(50, 54);
            $pdf->cell(128, 10, number_format($record->moq), 0, 0);

            $y = 67;
            foreach ($left as $key => $item) {
                $pdf->SetXY(55, $y += 10);
                $pdf->Cell(50, 12, $item, 0, 0);
                $pdf->Ln();
            }
            $y = 75;
            $tempY = $y;
            $x = 155;
            $h = 10;
            foreach ($board as $key => $item) {
                $pdf->SetXY($x, $y += 8);
                $pdf->Cell(50, $h, $item, 0, 0);
                $pdf->Ln();
            }
            $pdf->SetXY(135, 123);
            $pdf->Cell(55, 12, $record->wastage_percent);

            $y = 147;
            $tempY = $y;
            $x = 155;
            $h = 10;
            foreach ($development as $key => $item) {
                $pdf->SetXY($x, $y += 8);
                $pdf->Cell(50, $h, $item, 0, 0);
                $pdf->Ln();
            }

            $x1 = 135;
            $x2 = $x1;
            $y1 = 35;
            $y2 = 73;
            // $pdf->Line($x1,$y1,$x2,$y2); // board Graph line
            // $pdf->Line($x1+64,$y1,$x2+64,$y2); // board Graph line
            $board_graph = array(
                'DIE TO DIE SIZE',
                $record->die_to_die_width . 'mm X ' . $record->die_to_die_height . 'mm',
                'PRINT TO PRINT SIZE',
                ($record->die_to_die_width + 15) . 'mm X ' . ($record->die_to_die_height + 6) . 'mm'
            );
            $y = 0;
            $pdf->setXY(135, 38);
            $pdf->SetFont('Arial', '', 7);
            $d2dsize = ($record->die_to_die_width > 0) ? 'D2D Size: ' . $record->die_to_die_width . 'mm X ' . $record->die_to_die_height . 'mm' : 'D2D Size: ';
            $pdf->Cell(64, 8, $d2dsize, 0, 1, 'C');

            $pdf->setXY(135, 62);
            $p2psize = ($record->die_to_die_width > 0) ? 'P2P Size: ' . ($record->die_to_die_width + 15) . 'mm X ' . ($record->die_to_die_height + 6) . 'mm' : 'P2P Size: ';
            $pdf->Cell(64, 8, $p2psize, 0, 1, 'C');

            $pdf->setXY(135, 50);
            $pdf->SetFont('Arial', '', 9);
            $pdf->Cell(64, 8, 'No of Ups: ' . $record->no_ups, 0, 1, 'C');

            $pdf->SetXY(16, 244);
            $pdf->Write(6, 'Description: ' . $record->description);

            $fileName = "Costing No $id";
            (isset($_GET['download']) && $_GET['download'] == 'true') ? $pdf->Output($fileName . '.pdf', 'D') : $pdf->Output();


        } else { // if roll exist
            echo not_permitted();
        }

    } // offset_costing()


}