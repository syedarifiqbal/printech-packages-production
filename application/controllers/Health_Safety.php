<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Health_Safety extends MY_Controller {

	public $allowed_roles = array();

	# Methodes

	/**************************************************
	*					ADDING SUPPLIER.
	**************************************************/

	public function accident_investigation()
	{
		if (isset($_POST['submit'])) {

			$this->load->model('Health_and_Safety_Model');
			$hse = new Health_and_Safety_Model();
			$hse->id=$this->Health_and_Safety_Model->max();
			$hse->date=dbDate($this->input->post('date'));

		    $hse->department = $this->input->post('department');
		    $hse->detail = $this->input->post('detail');
		    $hse->shift = $this->input->post('shift');
		    $hse->reported_person = $this->input->post('reported_person');
		    $hse->injured_person = $this->input->post('injured_person');
		    $hse->employee_id = $this->input->post('employee_id');
		    $hse->contractor_emp_id = $this->input->post('contractor_emp_id');
		    $hse->risk_category = $this->input->post('risk_category');
		    $hse->accident_category = $this->input->post('accident_category');
		    $hse->early_decision = $this->input->post('early_decision');
		    $hse->root_cause = $this->input->post('root_cause');
		    $hse->preventice = $this->input->post('preventice');
		    $hse->reviewed_by = $this->input->post('reviewed_by');
		    $hse->reviewed_time = $this->input->post('reviewed_time');

			$hse->created_by = $this->session->userdata('user_id');
			$hse->created_time = dbTime();
			$hse->updated_by = $this->input->post('');
			$hse->updated_time = $this->input->post('');

			if ($hse->save()) {
				if ($this->input->is_ajax_request()) {
					echo "Successfully Inserted";
					return;
				}else{
					$this->session->set_flashdata('userMsg', 'New Supplier Added Successfully.');
					redirect(site_url('Maintenance/supplier_list'));
				}
			};

		}

		$data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

		$data['roles'] = $this->allowed_roles;
		if ($this->input->is_ajax_request()) {
			$this->load->view('Health_Safety/Investigation_form',$data);
		}else{
			$data['site_title'] = 'Accident / Incident Investigation Form';
			$this->load->view('main/header',$data);
			$this->load->view('main/navigation',$data);
			$this->load->view('main/rightNavigation',$data);
			$this->load->view('main/topbar',$data);
			$this->load->view('Health_Safety/Investigation_form',$data);
			$this->load->view('main/footer',$data);
		}
	}

}