<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Offset_Group extends MY_Controller
{
    public $allowed_roles = array();

    function __construct()
    {
        parent::__construct();
        $this->load->model("Offset_Group_Model");
    }

    public function add()
    {
        $data['title'] = 'Group List | Edit | Delete.!';
        $data['session'] = $this->session->all_userdata();

        if (isset($_POST['group_name'])) {
            $category = new Offset_Group_Model();
//            $category->group_id = $this->Offset_Category_Model->max();
            $category->group_name = $this->input->post('group_name');
            $category->description = $this->input->post('description');
            $category->created_by = $this->session->userdata('user_id');
            $category->created_time = dbTime();
            $category->updated_by = $this->input->post('');
            $category->updated_time = $this->input->post('');
            if ($category->save()) {
                if ($this->input->is_ajax_request()) {
                    echo "Successfully Inserted";
                    return;
                } else {
                    $this->session->set_flashdata('userMsg', 'New Category Added Successfully.');
                    redirect(site_url('Maintenance/category_list'));
                } // if else is ajax request?
            } // if category inserted

        } // if isset post

        $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

        $data['roles'] = $this->allowed_roles;
        if ($this->input->is_ajax_request()) {
            $this->load->view('offset/addGroup', $data);
        } else {
            $data['site_title'] = 'Add New Category';
            $this->load->view('main/header', $data);
            $this->load->view('main/navigation', $data);
            $this->load->view('main/rightNavigation', $data);
            $this->load->view('main/topbar', $data);
            $this->load->view('offset/addGroup', $data);
            $this->load->view('main/footer', $data);
        } // if else is ajax request

    } // Add Category

    // Update Category
    public function update($id = null)
    {
        if ($id) {
            $data['title'] = 'Group Update Form.!';
            $data['session'] = $this->session->all_userdata();

            if ($_POST) {
                $category = new Offset_Group_Model();
                $category->load($id);
                $category->group_name = $this->input->post('group_name');
                $category->description = $this->input->post('description');
                $category->updated_by = $this->session->userdata('user_id');
                $category->updated_time = dbTime();
                if ($category->save()) {
                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Inserted";
                        return;
                    } else {
                        $this->session->set_flashdata('userMsg', 'Updated Successfully.');
                        redirect(site_url('offset_category/update/' . $category->category_id));
                    } // check if or not ajax request

                } // if record updated
            } // end if post is set

            $g = new Offset_Group_Model();
            $g->load($id);

            $data['cate'] = $g;
            $data['roles'] = $this->allowed_roles;
            $data['update'] = true;

            $data['feed_back'] = ($this->session->flashdata('userMsg')) ? feed_back($this->session->flashdata('userMsg')) : '';

            if ($this->input->is_ajax_request()) {
                $this->load->view('offset/addGroup', $data);
            } else {
                $data['site_title'] = 'Add New Supplier';
                $this->load->view('main/header', $data);
                $this->load->view('main/navigation', $data);
                $this->load->view('main/rightNavigation', $data);
                $this->load->view('main/topbar', $data);
                $this->load->view('offset/addGroup', $data);
                $this->load->view('main/footer', $data);
            }
        } // END IF ID PRAMETER IS SET
        else {
            echo "<h1>Please Follow Given Link.!</h1>";
        }

    } // END OF CATEGORY UPDATE FUNCTION

    // Show All List
    public function show_all()
    {
        $data['title'] = 'Group List | Edit | Delete.!';
        $data['session'] = $this->session->all_userdata();

        $data['roles'] = $this->allowed_roles;
        $data['groups'] = $this->Offset_Group_Model->get();
        $data['feed_back'] = ($this->session->flashdata('userMsg'))? feed_back($this->session->flashdata('userMsg')):'';

        if ($this->input->is_ajax_request()) {
            $this->load->view('offset/group_list',$data);
        }else{
            $fb = $this->session->flashdata('userMsg');
            $data['feed_back'] = ($fb)? feed_back($fb):'';
            $data['site_title'] = 'Add New Group';
            $this->load->view('main/header',$data);
            $this->load->view('main/navigation',$data);
            $this->load->view('main/rightNavigation',$data);
            $this->load->view('main/topbar',$data);
            $this->load->view('offset/group_list',$data);
            $this->load->view('main/footer',$data);
        }
    }

    // Delete Category
    public function delete($id=null)
    {
        if ($id) {

            $category = new Offset_Category_Model();
            $category->load($id);

            if ($category->category_id){

                $category->delete();
                if($this->db->_error_message()){
                    echo "Can not delete this Record.!";
                }else{

                    if ($this->input->is_ajax_request()) {
                        echo "Successfully Deleted";
                        return;
                    }else{
                        $this->session->set_flashdata('userMsg', $category->category_name.' Deleted Successfully.');
                        redirect(site_url('Maintenance/category_list'));
                    }
                }

            }else{
                echo "<h1>Sorry No Record Found.!</h1>";
            }

        } // END IF ID PRAMETER IS SET
        else
            echo "<h1>Please Follow Given Link.!</h1>";

    } // END DELETE CATEGORY FUCTION

}