<?php

define("SUCCESS", 1);
define("INFO", 2);
define("WARNING", 3);
define("ERROR", 4);

function x($value)
{
    echo "<pre>";
    if (is_string($value)) {
        echo $value;
    }else{
        print_r($value);
    }
    echo "</pre>";
}

function set_active_menu($menu, $active_menu){
    if ( $active_menu == $menu) {
      echo 'active';
    }
}

function set_sub_menu($menu, $sub_menu){
    if ( $sub_menu == $menu) {
      echo 'active';
    }
}

function set_flash_message($title, $message){
    $ci =& get_instance();
    $ci->session->set_flashdata(['flash_title'=>$title,'flash_message'=>$message]);
}

function get_flash_message($data = array()){
    $ci =& get_instance();

    if (!$ci->session->flashdata('flash_title')) {
        return;
    }

    $types = array('','success','info','warning','error');
    $type = $types[$ci->session->flashdata('flash_title')];
    
    if (!$ci->session->flashdata('flash_title') && !$ci->session->flashdata('flash_message')) { return; }

    $title = ucfirst($types[$ci->session->flashdata('flash_title')]);
    $message = addslashes($ci->session->flashdata('flash_message'));
    
    $output = "<script>
        $(function (e) {
            setTimeout(function() {

                new PNotify({
                    title: '$title',
                    type: '$type',
                    text: '$message',
                    nonblock: {
                        nonblock: false
                    },
                    // addclass: 'dark',
                    styling: 'bootstrap3',
                    hide: true,
                });

            }, 300);
        });
    </script>";

    return $output;
}

function display_input($data, $title='', $fullWidth=false)
{ 
    $name = (isset($data['name'])&&$data['name'])?'data['.$data['name'].']':'';
?>
<div class="form-group <?php echo form_error( $name )? 'has-error':''; ?>">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo $title; ?> 
        <?php echo (isset($data['required'])&&$data['required'])?'<span class="required">*</span>':'' ?>
    </label>
    <div class="<?php echo $fullWidth? 'col-md-9 col-sm-9':'col-md-6 col-sm-6' ?> col-xs-12">
        <input 
            type="<?php echo (isset($data['type'])&&$data['type'])?$data['type']:'text' ?>"
            <?php echo (isset($data['name'])&&$data['name'])?'name="data['.$data['name'].']"':'' ?> 
            <?php echo (isset($data['required'])&&$data['required'])?'required="required"':'' ?>
            <?php echo $data['readonly']?? '' ?>
            <?php echo $data['disabled']?? '' ?>
            <?php echo $data['datepicker']?? '' ?>
            value="<?php echo set_value($name, isset($data['value'])? $data['value']:''); ?>"
            class="form-control col-md-7 col-xs-12">
            <?php echo form_error($name, '<p class="error-msg">', '</p>') ?>
    </div>
</div>
<?php 
}

function display_textarea($data, $title='')
{ ?>

<div class="form-group">
<label class="control-label col-md-3 col-sm-3 col-xs-12"><?php echo $title; ?></label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <textarea class="resizable_textarea form-control" 
            placeholder="Description..." 
            <?php echo (isset($data['name'])&&$data['name'])?'name="data['.$data['name'].']"':'' ?> 
            style="overflow: hidden; word-wrap: break-word; resize: horizontal; height: 54px;"><?php echo set_value('data['.$data['name'].']', isset($data['value'])? $data['value']:''); ?></textarea>
    </div>
</div>
<?php 
}

function default_dropdown($data, $title='')
{ 
    $multiple = (isset($data['multiple'])&&$data['multiple'])? 'multiple="multiple"':"";
    if (isset($data['prefix']) && $data['prefix'] === false ) {
        $name = (isset($data['name'])&&$data['name'])? $data['name']:'';
    }else{
        $name = (isset($data['name'])&&$data['name'])?'data['.$data['name'].']':'';
    }
?>
<div class="form-group <?php echo form_error( (!$multiple)?$name:$name.'[]' )? 'has-error':''; ?>">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo $title; ?> 
        <?php echo (isset($data['required'])&&$data['required'])?'<span class="required">*</span>':'' ?>
    </label>
    <div class="col-md-6 col-sm-6 col-xs-12">
        <!-- <select class="select2_single form-control" tabindex="-1"> -->
        <?php 
        $required = (isset($data['required'])&&$data['required'])?'required="required"':'';
        $placeholder = (isset($data['placeholder'])&&$data['placeholder'])?'placeholder="'.$data['placeholder'].'"':'placeholder="Choose..."';
        $default_class = (isset($data['multiple'])&&$data['multiple'])? 'select2_multiple':"select2_single";
        $data_ajax_url = (isset($data['url']))? 'data-ajax-url="'.$data['url'].'"':"";
        // echo ('<h1></h1>');
        if( isset($data['options']) && $data['options']!==false ){
            if (!$multiple) {
                if( !isset($data['empty']) ){  $data['options'] = [''=>''] + $data['options']; }
            }
            if ($multiple) {
                echo form_multiselect($name.'[]', 
                    $data['options'], 
                    set_value($name, isset($data['value'])? $data['value']:''), 
                    "class='$default_class form-control' $required $placeholder $multiple $data_ajax_url");
            }else{
                echo form_dropdown($name, 
                    $data['options'],
                    set_value($name, isset($data['value'])? $data['value']:''), 
                    "class='$default_class form-control' $required $placeholder $multiple $data_ajax_url");
            }
        }
        ?>
        <!-- </select> -->
        <?php echo form_error((!$multiple)?$name:$name.'[]', '<p class="error-msg">', '</p>') ?>
    </div>
</div>
<?php 
}

function display_switcher($data, $title='', $main_title="")
{
    $data['class'] = isset($data['class'])?$data['class'].' js-switch':'js-switch';
    $name = $data['name']??''; 
?>

<div class="form-group <?php echo form_error( $name )? 'has-error':''; ?>">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo $main_title; ?> 
        <?php echo (isset($data['required'])&&$data['required'])?'<span class="required">*</span>':'' ?>
    </label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="">
            <label>
                <?php echo form_checkbox($data); ?><?php echo $title; ?>
                <!-- <input type="checkbox" class="js-switch" checked /> -->
            </label>
        </div>
        <?php echo form_error($name, '<p class="error-msg">', '</p>') ?>
    </div>
</div>
<?php 
}

function display_radio($data, $title='', $main_title="")
{
    $data['class'] = isset($data['class'])?$data['class'].' flat':'flat';
    $name = $data['name']??'';
?>
<div class="form-group <?php echo form_error( $name )? 'has-error':''; ?>">
    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"><?php echo $main_title; ?> 
        <?php echo (isset($data['required'])&&$data['required'])?'<span class="required">*</span>':'' ?>
    </label>
    <div class="col-md-9 col-sm-9 col-xs-12">
        <div class="">
            <label>
                <?php echo form_radio($data); ?><?php echo $title; ?>
            </label>
        </div>
        <?php echo form_error($name, '<p class="error-msg">', '</p>') ?>
    </div>
</div>
<?php 
}

function local_date($date)
{
    if( !$date ){ return ''; }

    $d = DateTime::createFromFormat('Y-m-d',substr($date, 0,10));
    return $d->format('d/m/Y');
}

function db_date($date, $from_format='d/m/Y')
{
    $d = DateTime::createFromFormat($from_format,$date);
    return $d->format('Y-m-d');
}

function is_checked_item($id, $data)
{
    foreach ($data as $item) {
        if ($id == $item->consumable_id) {
            return true;
        }
    }
    return false;
}

function get_checked_item_qty($id, $field, $data)
{
    foreach ($data as $item) {
        if ($id == $item->consumable_id) {
            return $item->{$field};
        }
    }
    return '';
}

function validate_date($date)
{
    $d = DateTime::createFromFormat('Y-m-d', $date);
    return $d && $d->format('Y-m-d') === $date;
}



?>