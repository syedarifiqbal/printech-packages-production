<?php

function login()
{
    // return $this->session->userdata('logged_id');
}

function site_title($value = '')
{
    return $value ? $value : "Printech Packages (PVT) LTD.";
}

function PKdate($date, $seprator = "-")
{
    if (!$date) { return ''; }

    $format = ($seprator != "-") ? 'd/m/Y' : 'd-m-Y';
    return date_format(new DateTime(html_escape($date)), $format);
}

function dbDate($date)
{
    // DateTime::createFromFormat('d/m/')
    return date_format(new DateTime(html_escape($date)), 'Y-m-d');
}


function input($data, $required = true, $icon = 'fa-file-o', $label = '')
{
    $data['class'] = ($required) ? 'form-control required' : 'form-control';
    $data['title'] = ($data['placeholder']) ? $data['placeholder'] : '';
    $out = '';
    $out .= '<div class="input-group margin-bottom-sm">';
    $out .= '<span class="input-group-addon"><i class="fa ' . $icon . '"></i> ' . $label . '</span>';
    $out .= form_input($data);
    // <input class="form-control required" name="material_name" autofocus="true" type="text" value="" placeholder="Material Name">
    $out .= '</div><hr class="spacer"/>';
    return $out;
}


function input2($label, $data, $value, $required = true)
{
    $data['class'] = ($required) ? 'form-control required' : 'form-control';
    $data['title'] = isset($data['placeholder']) ? $data['placeholder'] : strtoupper($label);
    $data['placeholder'] = isset($data['placeholder']) ? $data['placeholder'] : strtoupper($label);
    $out = '';
    $out .= '<div class="input-group margin-bottom-sm">';
    $out .= '<span class="input-group-addon">' . $label . '</span>';
    $out .= form_input($data, $value);
    // <input class="form-control required" name="material_name" autofocus="true" type="text" value="" placeholder="Material Name">
    $out .= '</div><hr class="spacer"/>';
    return $out;
}

function checkbox( $label, $data, $value = '', $checked, $switch=true,$inline=false){
    $out = '';
    if(!$inline){
        $out .= '<div class="checkbox">';
    }
    $out .= '<label> ';
    $out .= form_checkbox($data, $value, $checked, $switch? 'class="switch2"':"") . ' ' . $label;
    $out .= ' </label>';
    if(!$inline){
        $out .= '</div>';
    }

    return $out;
}

function feed_back($msg, $title = "Success! ")
{
    $out = '<div class="alert alert-success" id="userMsg" style="display:none;">';
    $out .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
    $out .= '<strong>' . $title . '</strong> ' . $msg;
    $out .= '</div>';
    return $out;
}

function select($data, $required = true, $icon = 'fa-file-o', $label = '')
{
    $data['class'] = ($required) ? 'form-control required' : 'form-control';
    // $data['title'] = ($data['placeholder'])? $data['placeholder']:'';
    $out = '';
    $out .= '<div class="input-group margin-bottom-sm">';
    $out .= '<span class="input-group-addon"><i class="fa ' . $icon . '"></i> ' . $label . '</span>';
    $out .= form_dropdown($data['name'], $data['options'], $data['value'], 'class="' . $data['class'] . '"');
    $out .= '</div><hr class="spacer"/>';
    return $out;
}


function not_permitted($value = '')
{
    $ci =& get_instance();
    $user = new User_Model();
    $user->load($ci->session->userdata('user_id'));

    $output = '<div class="container">
	        		<div class="row">
			            <div class="col-md-12">
			                <div class="jumbotron">
			                    <div class="container">
			                        <h1>Sorry, Mr.' . strtoupper($user->user_name) . ' !</h1>
			                        <p>You are not permitted for this operation make sure you have permission in order to deal with, For more info please contact your administrator</p>
			                        <p>
			                            <a href="#add-post-title" data-toggle="modal" class="btn btn-primary btn-lg">Any Query? <i class="fa fa-plus"></i></a>
			                        </p>
			                    </div>
			                </div>
			            </div>
			        </div>
			    </div>';
    return $output;
}

function created_time($date)
{
    // return date_format( new DateTime( html_escape($date) ), 'g:i a F j, Y');
    return date('g:i a F j, Y', strtotime($date));
}


function dbTime($date = '')
{
    $f = $date ? 'd-m-Y H:i:s A' : 'Y-m-d H:i:s';
    $t = $date ? strtotime($date) : time();
    // var_dump($t);
    // !$date ? date_default_timezone_set('Asia/Karachi'):null;
    return date($f, $t);
}

function humanDateTime($date)
{
    return date('d-m-Y H:i:s A', time($date));
}

/**
 * CURRENCY CONVERTOR
 */
function in_words($number)
{

    $hyphen = '-';
    $conjunction = ' and ';
    $separator = ', ';
    $negative = 'negative ';
    $decimal = ' point ';
    $dictionary = array(
        0 => 'zero',
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',
        8 => 'eight',
        9 => 'nine',
        10 => 'ten',
        11 => 'eleven',
        12 => 'twelve',
        13 => 'thirteen',
        14 => 'fourteen',
        15 => 'fifteen',
        16 => 'sixteen',
        17 => 'seventeen',
        18 => 'eighteen',
        19 => 'nineteen',
        20 => 'twenty',
        30 => 'thirty',
        40 => 'fourty',
        50 => 'fifty',
        60 => 'sixty',
        70 => 'seventy',
        80 => 'eighty',
        90 => 'ninety',
        100 => 'hundred',
        1000 => 'thousand',
        1000000 => 'million',
        1000000000 => 'billion',
        1000000000000 => 'trillion',
        1000000000000000 => 'quadrillion',
        1000000000000000000 => 'quintillion'
    );

    if (!is_numeric($number)) {
        return false;
    }

    if (($number >= 0 && (int)$number < 0) || (int)$number < 0 - PHP_INT_MAX) {
        // overflow
        trigger_error(
            'in_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
            E_USER_WARNING
        );
        return false;
    }

    if ($number < 0) {
        return $negative . in_words(abs($number));
    }

    $string = $fraction = null;

    if (strpos($number, '.') !== false) {
        list($number, $fraction) = explode('.', $number);
    }

    switch (true) {
        case $number < 21:
            $string = $dictionary[$number];
            break;
        case $number < 100:
            $tens = ((int)($number / 10)) * 10;
            $units = $number % 10;
            $string = $dictionary[$tens];
            if ($units) {
                $string .= $hyphen . $dictionary[$units];
            }
            break;
        case $number < 1000:
            $hundreds = $number / 100;
            $remainder = $number % 100;
            $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
            if ($remainder) {
                $string .= $conjunction . in_words($remainder);
            }
            break;
        default:
            $baseUnit = pow(1000, floor(log($number, 1000)));
            $numBaseUnits = (int)($number / $baseUnit);
            $remainder = $number % $baseUnit;
            $string = in_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
            if ($remainder) {
                $string .= $remainder < 100 ? $conjunction : $separator;
                $string .= in_words($remainder);
            }
            break;
    }

    if (null !== $fraction && is_numeric($fraction)) {
        $string .= $decimal;
        $words = array();
        foreach (str_split((string)$fraction) as $number) {
            $words[] = $dictionary[$number];
        }
        $string .= implode(' ', $words);
    }

    return $string;
}

function check($value)
{
    echo "<pre>";
    print_r($value);
    echo "</pre>";
}

function generateRandomString($length = 16)
{
    return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
}

/**
 * Returns an encrypted & utf8-encoded
 */
function encrypt($pure_string, $encryption_key = "!@#$%^&*")
{
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $encrypted_string = mcrypt_encrypt(MCRYPT_BLOWFISH, $encryption_key, utf8_encode($pure_string), MCRYPT_MODE_ECB, $iv);
    return $encrypted_string;
}

/**
 * Returns decrypted original string
 */
function decrypt($encrypted_string, $encryption_key = "!@#$%^&*")
{
    $iv_size = mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB);
    $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
    $decrypted_string = mcrypt_decrypt(MCRYPT_BLOWFISH, $encryption_key, $encrypted_string, MCRYPT_MODE_ECB, $iv);
    return $decrypted_string;
}

function net_on()
{
    $connected = @fsockopen("www.printechpackages.com", 80);
    //website, port  (try 80 or 443)
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;

}

function dispatch_email_body($job_code, $job_name, $customer)
{
    $out = '<div style="background:#ccc;padding:10px 0">
				    <div style="width: 600px;background:#fff;margin:0 auto;padding:10px;font-size:.9em;border-radius:10px;font-family: Segoe UI;">
				        <table style="width:100%">
				            <tbody>
				                <tr>
				                    <td>
				                        <img src="https://ci6.googleusercontent.com/proxy/cDoTFHFNOAM5NfBA15be28KDOK4Aobra7YZlr65aP_gX4sVWrF6-kjXIs4B8qwVok6_pMdTD1OyJbVMJ2RyyLxc=s0-d-e1-ft#http://printechpackages.com/images/logo.gif" class="CToWUd">
				                    </td>
				                    <td>
				                        <h1 style="text-align:right;margin-bottom:10px;border-bottom:1px solid #203569;padding-bottom:10px;font-weight:bold;color:#203569">Job Dispatch</h1>
				                    </td>
				                </tr>
				                <tr>
				                    <td style="background: #888;color: white;padding-left: 20px;margin-right: 20px;padding-top: 20px;padding-bottom: 20px;font-size: 18px;" colspan="2">
				                        <p>Job Code: ' . $job_code . '</p>
				                        <p>Job Name: ' . $job_name . '</p>
				                        <p>Customer Name: ' . $customer . '</p>
				                    </td>
				                </tr>
				                <tr>
				                    <td colspan="2" style="
				                        font-size: 16px;
				                        color: #333;
				                        ">
				                        <p>Order QTY: 1,000,000 Pcs</p>
				                        <p>Delivered QTY: 500,000 Pcs</p>
				                        <p>Balance QTY: 500,000 Pcs</p>
				                        <p>No of Rolls: 120</p>
				                    </td>
				                </tr>
				                <tr>
				                    <td colspan="2">
				                        <table style="
				                            width: 100%;
				                            border: 1px solid #ccc;
				                            ">
				                            <tbody>
				                                <tr>
				                                    <td colspan="2" style="text-align: center;">
				                                        <h2>Flavours</h2>
				                                    </td>
				                                </tr>
				                                <tr style="
				                                    background: #ccc;
				                                    padding: 10px;
				                                    border: 0;
				                                    ">
				                                    <th>Flavours Name</th>
				                                    <th style="text-align: right;">QTY</th>
				                                </tr>
				                                <tr>
				                                    <td>Flavours Name</td>
				                                    <td style="text-align: right;">20000</td>
				                                </tr>
				                                <tr>
				                                    <td>Flavours Name</td>
				                                    <td style="text-align: right;">20000</td>
				                                </tr>
				                            </tbody>
				                        </table>
				                    </td>
				                </tr>
				            </tbody>
				        </table>
				    </div>
				</div>';
    return $out;

}

function get_current_department(){
    $ci = get_instance();
    $section = $ci->session->userdata('section');
    switch($section){
        case 'roto':
            return 'ROTO GRAVURE';
        case 'offset':
            return 'OFFSET PRINTING';
        case 'extrusion':
            return 'EXTRUSION';
        case 'marketing':
            return 'MARKETING';
        case 'maintenance':
            return 'MAINTENANCE';
        case 'hr':
            return 'HUMAN RESOURCES';
        case 'health_sefty':
            return 'HEALTH AND SAFETY';
    }
}


function getPermissionsByRoleId($role_id){
    $ci =& get_instance();
    $allowed = array();

    $s = $ci->db->query("SELECT perm_id FROM role_permission WHERE role_id = ".$role_id);
    $s = $s->result();

    foreach ($s as $all) {
        $allowed[] = $all->perm_id;
    };

    return $allowed;

}


?>