<?php
require_once('fpdf.php');
// require_once('../functions.php');

class PDF_Order_Balance extends FPDF
{
    protected $tag_line;
    protected $from_date;
    protected $to_date;
    // protected $header = array();
    public $header = array();
    public $hFontSize = 11;
    public $cFontSize = 11;

    public function __construct($orientation='P', $unit='mm', $size='A4')
    {
        parent::__construct($orientation, $unit, $size);
    }

    public function set_title($title)
    {
        $this->tag_line = $title;
    }

    public function set_duration($from, $to)
    {
        $this->from_date = $from;
        $this->to_date = $to;
    }

    public function set_header($head)
    {
        $this->header = $head;
    }
    // Load data
    function Header()
    {
        // Logo
        // Arial bold 15
        $this->SetFont('Arial','B',14);
        // Move to the right
        // Title
        $this->Cell(0,10,"Printech Packages PVT LTD.",0,0);
        $this->Image(base_url('assets/img/logo.jpg'),320,6,25);
        $this->Ln(10);
        $this->SetFont('Arial','',12);
        $this->Cell(0,10,$this->tag_line,0,0);
        $this->Ln(5);
        $this->SetDrawColor(50,50,50);

        if ( $this->from_date && $this->to_date ) {
            $this->SetFont('Arial','',12);
            $this->Cell(0,10,'From      '.$this->from_date.'     To     '. $this->to_date,0,0);
            $this->Ln(10);
        }

        $this->SetFont('Arial','B',8);
        $doubleHead = ['order qty', 'delivered qty', 'balance'];
        foreach ($this->header as $key => $w) {
            $headHeight = 10;
            if (in_array(strtolower($key), $doubleHead)) { $headHeight /= 2; }
            $this->cell($w,$headHeight,$key,1,0,'C');
        }

        $this->Ln();
        $width = 0;
        foreach ($this->header as $key => $w) {
            $headHeight = 10;
            if (in_array(strtolower($key), $doubleHead)) { 
                $this->cell(($w/2),$headHeight/2,"KG",1,0,'C');
                $this->cell(($w/2),$headHeight/2,"PCS",1,0,'C');
            }else
                $this->cell($w,$headHeight,"");
        }
        $this->Ln();

        // Colors, line width and bold font
        $this->SetFillColor(224,235,255);
        $this->SetTextColor(0);
        $this->SetDrawColor(50,50,50);
        $this->SetLineWidth(.3);
        $this->SetFont('','B',$this->hFontSize);
        $this->SetLineWidth(.1);
        // Header
        // $headData = $this->data;
        // if( count($this->data) ){
        //     $header = array_keys(array_shift($headData));
        //     for($i=0;$i<count($header);$i++){
        //         $w = isset($this->width[$i]) ? $this->width[$i] : 15;
        //         $this->Cell($w,7,strtoupper($header[$i]),1,0,'C',true);
        //     }
        //     $this->Ln();
        // }else{
        //     die( 'Sorry no Result Found.!' );
        // }


    }

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        // $this->Cell(array_sum($this->width),0,'','T');
        // $this->Ln();
        $this->SetY(-10);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->AliasNbPages('{totalPages}');
        $this->Cell(0,10,'Page '.$this->PageNo().'/{totalPages} '.date('l jS \of F Y h:i:s A'),0,0,'C');
    }

    // Colored table
    // function FancyTable()
    // {

    //     // $x = get_production_planning(2,2);
    //     // $this->width = $width;

    //     // Color and font restoration
    //     $this->SetFillColor(240,240,240);
    //     $this->SetTextColor(0);
    //     $this->SetFont('Arial','',$this->cFontSize);
    //     $fill = false;
    //     // Data
    //     foreach ($this->data as $row) {
    //         $i=0;
    //         foreach ($row as $cell) {
    //             // $cell = ($cell>0);
    //             $align = is_numeric($cell) ?  "R":"L" ;
    //             if (is_numeric($cell)) {
    //                 // ($i == 1)? continue:null;
    //                 if( $i==1 ){

    //                 }else if ($cell<0) {
    //                 $this->SetTextColor(255,0,0);
    //                     $cell = "(".number_format(abs($cell),1).")";
    //                 }else if( $cell==0 ){
    //                     $cell = '-';
    //                     $align = 'C';
    //                 }else{
    //                     $cell = number_format($cell,1);
    //                 }
                    
    //             }else if (!$cell) {
    //                 $cell = '-';
    //                 $align = 'C';
    //             }
    //             $w = isset($this->width[$i]) ? $this->width[$i] : 10;
    //             $this->Cell($this->width[$i],5,$cell,"LR",0,$align,$fill);
    //             $this->SetTextColor(0);
    //             $i++;
    //         }
    //         $fill = !$fill;
    //         $this->Ln();
    //     }
    //     $this->Cell(array_sum($this->width),0,'','T');
    //     $this->Ln();
        
    // } // FancyTable ()



} // end class

?>
