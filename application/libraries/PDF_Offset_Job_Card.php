<?php
require_once('fpdf.php');
// require_once('../functions.php');

class PDF_Offset_Job_Card extends FPDF
{
    protected $tag_line;
    protected $from_date;
    protected $to_date;
    protected $print_x;
    protected $lamination_x;
    protected $lamination_width = 55;
    protected $print_width = 40;
    protected $colH = 5;
    protected $second_row;
    protected $third_row;
    // protected $header = array();
    public $job_code;
    public $so;
    public $data;
    public $hFontSize = 11;
    public $cFontSize = 11;

    public function __construct($orientation='P', $unit='mm', $size='A4')
    {
        parent::__construct($orientation, $unit, $size);
        $this->SetDrawColor(200,200,200);
        $this->SetFillColor(240,240,240);
    }

    // Load data
    function Header()
    {
        $this->so = new Offset_Sale_Order_Model();
        $this->so->load($this->job_code);
        $record = $this->data;
        $this->SetFont('Arial','B',16);
        // Move to the right
        // Title
        $this->Cell(0,6,"Printech Packages PVT LTD.",0,0,"C");
        $this->Image(base_url('assets/img/logo.jpg'),10,5,25);
        $this->Ln(7);
        $this->SetFont('Arial','',12);
        $this->Cell(0,6,"Offset Job Card",0,0,"C");
        $this->Ln(10);

        $b = "B";
        $this->SetFont('Arial','B',9);
        // $this->Cell(20,8,"Customer:",$b);
        $this->Cell(70,6,"Customer: ".ucfirst($record->customer_name),$b);

        // $this->Cell(20,8,"Job Name:",$b);
        $this->Cell(110,6,"Job Name: ".ucfirst($record->job_name),$b);

        // $this->Cell(25,8,"Order Quantity:",$b);
        $this->Cell(55,6,"Order Quantity: ".number_format($record->quantity).' '.$record->order_type,$b,1);

        // $this->Cell(25,8,"Paper/Board:",$b);
        $this->Cell(62,6,"Material: ".$record->material_id,$b);

        $this->Cell(35,6,"Paper/Board: ".ucfirst($record->sheet_type),$b);

        // $this->Cell(15,8,"Size:",$b);
        $size = ($record->grain_size == 'w' )? $record->board_height .'" X '. $record->board_width.'"': $record->board_width .'" X '. $record->board_height.'"';
        $this->Cell(30,6,"Size: ".$size,$b);

        // $this->Cell(25,8,"GSM:",$b);
        $this->Cell(20,6,"GSM: ".$record->gsm,$b);

        $dir = ( $record->grain_size == 'w' )? $record->board_width.'"' : $record->board_height.'"';;
        $this->Cell(28,6,"Grain Dir.: ".$dir,$b);

        // $this->Cell(30,8,"Sheet to be Print:",$b);
        if($record->no_ups>0){
            $this->Cell(40,6,"Printed Qty: ".number_format(($record->quantity/$record->no_ups)),$b);
        }else{
            $this->Cell(40,6,"Printed Qty: 0",$b);
        }

        // $this->Cell(25,8,"Up Size:",$b);
        $this->Cell(20,6,"Up's: ".$record->no_ups,$b,1);

        if($record->no_ups>0){
            $this->Cell(235,6,"Sheets to be issued for Printing: ".number_format(($record->quantity/$record->no_ups)+$record->excess_quantity),$b);
        }else{
            $this->Cell(235,6,"Sheets to be issued for Printing: ".number_format($record->quantity+$record->excess_quantity),$b);
        }

        $this->Ln();

        ######################## TOP RIGHT INFORMATION SECTION ########################
        $topRight = array(
            'Date' => PKdate($record->date,'/'),
            'PO #' => $record->po_num,
            'PO Date'=> PKdate($record->po_date),
            'Job #'=> $record->job_code,
            'Due Date'=> PKdate($record->delivery_date,'/'),
            'File #'=>substr($record->id,4)
        );
        $this->SetY(7);
        foreach ($topRight as $key => $value) {
            $this->SetX(247);
            $this->Cell(20,6,$key.':',1,0,"R",1);
            $this->Cell(20,6,$value,1,1);
        }
        $this->SetFont('Arial','',9);
//        check($record);
    }

    public function PrintTable(){
        $record = $this->data;
        $this->print_x = $this->GetX();
        $this->second_row = $this->GetY() + 3;

        $this->SetXY($this->print_x,$this->second_row);
        $w = $this->print_width;

        $this->Cell($w,$this->colH,"Printing",1,1,'C',1);

        $this->Cell($w/2,$this->colH,"Cyan  ",1,0,'R');
//        $this->setIconFont();
        $this->ConditionalCell($w/2,$this->colH,$record->c,1,1,'L');
//        $this->resetFont();

        $this->Cell($w/2,$this->colH,"Magenta  ",1,0,'R');
//        $this->setIconFont();
        $this->ConditionalCell($w/2,$this->colH,$record->m,1,1,'L');
//        $this->resetFont();

        $this->Cell($w/2,$this->colH,"Yellow  ",1,0,'R');
//        $this->setIconFont();
        $this->ConditionalCell($w/2,$this->colH,$record->y,1,1,'L');
//        $this->resetFont();

        $this->Cell($w/2,$this->colH,"Black  ",1,0,'R');
//        $this->setIconFont();
        $this->ConditionalCell($w/2,$this->colH,$record->k,1,1,'L');
//        $this->resetFont();

        $this->Cell($w,$this->colH,"Spot Colors",1,1,'C',1);
        $this->SetFont('Arial','',8.5);

//        $this->Cell($w/3,$this->colH*2,$record->spot1,1,0,'C');
//        $this->Cell($w/3,$this->colH*2,$record->spot2,1,0,'C');
//        $this->Cell($w/3,$this->colH*2,$record->spot3,1,1,'C');

        $this->Cell($w,($this->colH*2/3),' '.$record->spot1,1,1);
        $this->Cell($w,($this->colH*2/3),' '.$record->spot2,1,1);
        $this->Cell($w,($this->colH*2/3),' '.$record->spot3,1,1);

        $this->resetFont();
        $this->Cell($w/2,$this->colH,"Foil Color",1,0,'C');
//        $this->setIconFont();
        $this->Cell($w/2,$this->colH,$record->foil,1,1,'C');
//        $this->resetFont();
        $this->Cell($w/2,$this->colH,"Foil Type",1,0,'C');
//        $this->Cell($w/2,$this->colH);
//        $this->setIconFont();
        $this->Cell($w/2,$this->colH,$record->foil_type,1,0,'C');
//        $this->resetFont();
    }

    public function LaminationTable(){
        $record = $this->data;
        $this->lamination_x = $this->print_x + $this->print_width;

        $this->SetXY($this->lamination_x,$this->second_row);
        $w = $this->lamination_width;

        $this->Cell($w,$this->colH,"Coating/Lamination",1,1,'C',1);

        $this->SetX($this->lamination_x);
        $this->Cell($w/3,$this->colH*2,"Water Base",1,0,'R');
        $this->Cell($w/3,$this->colH,"Gloss",1,0,'C');
//        $this->setIconFont();
        $this->ConditionalCell($w/3,$this->colH,$record->water_gloss,1,1,'L');
//        $this->resetFont();
        $this->SetX($this->lamination_x+($w/3));
        $this->Cell($w/3,$this->colH,"Matt",1,0,'C');
//        $this->setIconFont();
        $this->ConditionalCell($w/3,$this->colH,$record->water_matt,1,1,'L');
//        $this->resetFont();
        $this->SetX($this->lamination_x);
        $this->Cell($w/3,$this->colH*3,"U.V",1,0,'C');
        $this->Cell($w/3,$this->colH,"Gloss",1,0,'C');
//        $this->setIconFont();
        $this->ConditionalCell($w/3,$this->colH,$record->uv_gloss,1,1,'L');
//        $this->resetFont();
        $this->SetX($this->lamination_x+($w/3));
        $this->Cell($w/3,$this->colH,"Matt",1,0,'C');
//        $this->setIconFont();
        $this->ConditionalCell($w/3,$this->colH,$record->uv_matt,1,1,'L');
        $this->SetX($this->lamination_x+($w/3));
//        $this->resetFont();
        $this->Cell($w/3,$this->colH,"Spot",1,0,'C');
//        $this->setIconFont();
        $this->ConditionalCell($w/3,$this->colH,$record->uv_spot,1,1,'L');
//        $this->resetFont();

        $this->SetX($this->lamination_x);
        $this->Cell($w/3,$this->colH*2,"Lamination",1,0,'R');
        $this->Cell($w/3,$this->colH,"Gloss",1,0,'C');
//        $this->setIconFont();
        $this->ConditionalCell($w/3,$this->colH,$record->lamination_gloss,1,1,'L');
//        $this->resetFont();
        $this->SetX($this->lamination_x+($w/3));
        $this->Cell($w/3,$this->colH,"Matt",1,0,'C');
//        $this->setIconFont();
        $this->ConditionalCell($w/3,$this->colH,$record->lamination_matt,1,1,'L');
//        $this->resetFont();

        $this->SetX($this->lamination_x);
        $this->Cell($w/3,$this->colH*2,"Varnish",1,0,'C');
        $this->Cell($w/3,$this->colH,"Gloss",1,0,'C');
//        $this->setIconFont();
        $this->ConditionalCell($w/3,$this->colH,$record->warnish_gloss,1,1,"L");
//        $this->resetFont();
        $this->SetX($this->lamination_x+($w/3));
        $this->Cell($w/3,$this->colH,'Matt',1,0,'C');
//        $this->setIconFont();
        $this->ConditionalCell($w/3,$this->colH,$record->warnish_matt,1,0,"L");
//        $this->resetFont();

        $this->SetY($this->GetY()-$this->colH);
        $this->SetX($this->lamination_x+$this->lamination_width);
        $this->Cell(30,$this->colH*2,'Special Comments',1);
        // remarks
        $this->Cell(0,$this->colH,$this->so->remarks,1,1);

        $this->SetX($this->lamination_x+$this->lamination_width+30);
        $this->Cell(0,$this->colH,'',1,1);
    }

    public function InksTable(){
        $record = $this->data;
        $this->lamination_x = $this->print_x + $this->print_width;
        $x = $this->lamination_x+$this->lamination_width;
        $this->SetXY($x,$this->second_row);

        $this->Cell(0,$this->colH,"Recommended Inks",1,1,'C',1);
        $inks = array(
            'S.No' =>8,
            'Ink Brand'=>15,
            'Code'=>15,
            'Colors' =>15,
            'S.Shade' =>20,
            'Remarks' => 0
        );
        $this->SetX($x);

        for($i=0;$i<7;$i++){
            $this->SetX($x);
            foreach ($inks as $key => $w) {

                if($i == 0){
                    $this->Cell($w,$this->colH,$key,1,0,'C');
                }else{
                    $s = ( $key == 'S.No' )?$i:'';
                    $this->Cell($w,$this->colH,$s,1,0,"C");
                }

            }
            $this->Ln();

        }
        $this->third_row = $this->GetY()+($this->colH*2);
    }

    public function DisplayTable(){
        $this->PrintTable();
        $this->LaminationTable();
        $this->InksTable();
        $this->CorrugationTable();
        $this->DieCuttingTable();
        $this->PastingTable();
        $this->DispatchTable();
    }

    public function CorrugationTable(){
        $this->SetY($this->third_row);
        $record = $this->data;
        $this->Cell(0,$this->colH,"Corrugation",1,1,"C",1);
        $this->Cell($this->print_width,$this->colH,"Ply Required",1,1,"C");

        $this->SetXY($this->lamination_x,$this->third_row+$this->colH);
        $this->Cell($this->lamination_width,$this->colH,"Panel Required",1,0,"C");
        $this->Cell($this->lamination_width,$this->colH,"Fluting",1,0,"C");

//        $this->Cell(15,$this->colH*2,"Roll Size",1,0,"C");
//        $this->Cell(15,$this->colH*2,$record->roll_size,1,0,"C");
//        $this->Cell(20,$this->colH*2,"Cutting Size",1,0,"C");
//        $this->Cell(15,$this->colH*2,$record->cutting_size,1,0,"C");
//        $this->Cell(0,$this->colH,"Remarks",1,1,"C");
//
        $this->Cell(25,$this->colH,"Roll Size",1,0,"C");
        $this->Cell(15,$this->colH,$record->roll_size,1,0,"C");
        $this->Cell(15,$this->colH*2,"Emboss",1,0,"C");
        $this->ConditionalCell(10,$this->colH*2,$record->emboss,1,0,"L");
        $this->Cell(0,$this->colH,"Remarks",1,1,"C");

        if ($record->ply) {
            $this->Cell($this->print_width,$this->colH,$record->ply,1,0,"C");
        }else{
//            $this->setIconFont();
            $this->Cell($this->print_width,$this->colH,'-',1,0,"C");
//            $this->resetFont();
        }

        if ($record->panel) {
            $this->Cell($this->lamination_width,$this->colH,$record->panel,1,0,"C");
        }else{
//            $this->setIconFont();
            $this->Cell($this->lamination_width,$this->colH, "-",1,0,"C");
//            $this->resetFont();
        }

        $this->Cell($this->lamination_width,$this->colH,ucwords($record->flutting),1,0,"C");
        $this->Cell(25,$this->colH,"Cutting Size",1,0,"C");
        $this->Cell(15,$this->colH,$record->cutting_size,1,0,"C");

        $this->Cell(25,$this->colH,"",0,0,"C");
        $this->Cell(0,$this->colH,"",1,1,"C");
    }

    public function DieCuttingTable(){
        $record = $this->data;
        $this->Cell(0,$this->colH,"Die Cutting",1,1,"C",1);
        $this->Cell($this->print_width,$this->colH,"Die Type",1,0,"C");
        $this->Cell(0,$this->colH,"Remarks",1,1,"C");

        $this->Cell($this->print_width,$this->colH*2,$record->die_cutting_type,1,0,"C");
        $this->Cell(0,$this->colH,"",1,1,"C");
        $this->Cell($this->print_width,$this->colH,"");
        $this->Cell(0,$this->colH,"",1,1,"C");

        return;
        $this->Cell($this->print_width/2,$this->colH,"New",1,0,"C");
//        $this->setIconFont();
        $this->ConditionalCell($this->print_width/2,$this->colH,($record->die_cutting_type=="n")?true:'',1,0,"L");
//        $this->resetFont();
        $this->Cell(0,$this->colH,"",1,1,"C");
        $this->Cell($this->print_width/2,$this->colH,"Old",1,0,"C");
//        $this->setIconFont();
        $this->ConditionalCell($this->print_width/2,$this->colH,($record->die_cutting_type=="o")?true:'',1,0,"L");
//        $this->resetFont();
        $this->Cell(0,$this->colH,"",1,1,"C");
    }

    public function PastingTable(){
        $record = $this->data;
        $this->Cell(50,$this->colH,"Pasting Type",1,0,"C",1);
        $this->Cell(0,$this->colH,"Pasting Detail",1,1,"C",1);

        $this->Cell(50,$this->colH,'',"RL",0,"C");
        $this->Cell(0,$this->colH,"",1,1,"C");

        $this->Cell(50,$this->colH,$record->pasting_type,"RL",0,"C");
        $this->Cell(0,$this->colH,"",1,1,"C");

//        $this->Cell(50,$this->colH,"","RL",0,"C");
//        $this->Cell(0,$this->colH,"",1,1,"C");
    }

    public function DispatchTable(){
        $record = $this->data;
        $this->Cell(0,$this->colH,"Board Details",1,1,"C",1);
        $y = $this->GetY();
        $x = $this->lamination_x+$this->lamination_width;

        $this->Cell($this->print_width,$this->colH,"Packet QTY:",1);
        $this->Cell(100,$this->colH,"",1);
        $this->Cell(30,$this->colH,"Board Brand:",1);
        $this->Cell(0,$this->colH,"",1,1);

        $this->Cell($this->print_width,$this->colH,"Board Size:",1);
        $this->Cell(100,$this->colH,"",1);
        $this->Cell(30,$this->colH,"Cutting Size:",1);
        $this->Cell(0,$this->colH,"",1,1);

        $this->Cell($this->print_width,$this->colH,"GSM:",1);
        $this->Cell(100,$this->colH,"",1);
        $this->Cell(30,$this->colH,"Total Sheets:",1);
        $this->Cell(0,$this->colH,"",1,1);
    }

    // Page footer
    function Footer()
    {
        $record = $this->data;
        $user = new User_Model();
        $user->load($record->added_by);
        $created_at = ucfirst($user->user_name) . ' ' . created_time($record->added_time);

        $this->Ln(5);
        $this->Cell(0,5,"Prepared By:  _________________________________               Production Manager:  ___________________________________             Operation Director:  _____________________________",0,1);
        $this->Ln(5);
        $this->SetFont('ARIAL','',6);
        $this->Cell(0,3,"This document was originally created by Production Department on 01/10/2014",0,1,"C");

        $this->SetXY(40, 175);
        $this->Cell(50,6,$created_at);
        $this->sro();

        // Position at 1.5 cm from bottom
        // $this->Cell(array_sum($this->width),0,'','T');
        // $this->Ln();
        $this->SetY(-10);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->AliasNbPages('{totalPages}');
        $this->Cell(0,10,'Page '.$this->PageNo().'/{totalPages} '.date('l jS \of F Y h:i:s A'),0,0,'C');
    }

    public function ConditionalCell($width, $height, $string, $border, $line, $align){
        if($string){
            $this->setIconFont();
            $this->Cell($width,$height,"  3",$border,$line,$align);
            $this->resetFont();
        }else{
            $this->Cell($width,$height,"  -",$border,$line,$align);
        }
    }

    public function setIconFont()
    {
        $this->SetFont('zapfdingbats','',10);
    }

    public function resetFont()
    {
        $this->SetFont('ARIAL','',9);
    }

    public function sro(){
//        echo $this->getX() .' x, '. $this->getY() . 'y';
        $this->SetX(10.00125);
        $this->SetY(189);

        $this->Cell(80,3,"",0,0,"C");
        $this->Cell(15,3,"This Revision",1,0,"C");
        $this->Cell(15,3,"0",1,0,"C");
        $this->Cell(30,3,"Revised By",1,0,"C");
        $this->Cell(25,3,"Production Manager",1,0,"C");
        $this->Cell(15,3,"",1,0,"C");
        $this->Cell(15,3,"",1,0,"C");
        $this->Cell(50,3,"",0,1,"C");

        $this->Cell(80,3,"",0,0,"C");
        $this->Cell(15,3,"This Revision",1,0,"C");
        $this->Cell(15,3,"01/10/2014",1,0,"C");
        $this->Cell(30,3,"Frequency",1,0,"C");
        $this->Cell(25,3,"Two Years",1,0,"C");
        $this->Cell(15,3,"Next Rev Due",1,0,"C");
        $this->Cell(15,3,"01/10/2016",1,0,"C");
        $this->Cell(50,3,"",0,1,"C");

        $this->Cell(80,3,"",0,0,"C");
        $this->Cell(15,3,"Approve by",1,0,"C");
        $this->Cell(45,3,"Operation Director",1,0,"C");
        $this->Cell(25,3,"Approval Date",1,0,"C");
        $this->Cell(15,3,"",1,0,"C");
        $this->Cell(15,3,"01/10/2014",1,0,"C");
        $this->Cell(50,3,"",0,1,"C");

        $this->SetFont('ARIAL','B',6);
        $this->Cell(0,3,"UNCONTROLLED ONCE PRINTED",0,1,"C");

    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

} // end class

?>
