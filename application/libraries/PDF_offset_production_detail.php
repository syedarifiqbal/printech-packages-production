<?php
require_once('fpdf.php');
// require_once('../functions.php');

class PDF_offset_production_detail extends FPDF
{
    public $detail_id;
    public $record;
    public $width;
    public $hFontSize = 11;
    public $cFontSize = 11;

    public function __construct($orientation='L', $unit='mm', $size='A4')
    {
        parent::__construct($orientation, $unit, $size);
        $this->SetDrawColor(200,200,200);
        $this->SetFillColor(240,240,240);
    }

    // Load data
    function Header()
    {        
        $this->SetFont('Arial','B',16);
        // Move to the right
        // Title
        $this->Cell(0,6,"Printech Packages PVT LTD.",0,0,"C");
        $this->Image(base_url('assets/img/logo.jpg'),10,5,25);
        $this->Ln(7);
        $this->SetFont('Arial','',12);
        $this->Cell(0,6,"Job Details",0,0,"C");
        $this->Ln(10);

        $d = [
            ' Job Name:      ' => $this->record->job_name,
            ' Dispatch Date: ' => pkDate($this->record->dispatch_date),
            ' Job Number:    ' => $this->record->job_code,
            ' Number of UPs: ' => $this->record->no_ups,
        ];

        $this->SetFont('Arial','B',9);
        $this->Ln(5);
        $row = 0;
        foreach ($d as $key => $value) {
            if ($row%2 === 0) { $this->Ln(); }
            $this->Cell(($this->w/2-10),8,$key . ' ' . $value,1);
            $row++;
        }
        $this->Ln();
    }

    public function DisplayTable()
    {
        $this->Ln(5);
        $col_width = ($this->w/2-10);
        $cell1_width = ($col_width/100*70);
        $cell2_width = ($col_width/100*30);
        $cell3_width = ($cell1_width/100*50);
        $cell4_width = ($cell1_width/100*25);
        $cell5_width = ($cell1_width/100*25);
        $cell6_width = ($cell2_width/100*50);
        $cell7_width = ($cell2_width/100*50);

        $this->SetFont('Arial','B',9);

        // Left Side Colum for Conventional
        $this->Cell($cell1_width, 8,'CONVENTIONAL',1, 0, 'C');
        $this->Cell($cell2_width, 8,'Wastage',1, 0, 'C');

        // Left Side Colum for UV
        $this->Cell($cell1_width, 8,'UV',1, 0, 'C');
        $this->Cell($cell2_width, 8,'Wastage',1, 1, 'C');

        $this->Cell($cell3_width, 6, "Process Stages", 1, 0, "C");
        $this->Cell($cell4_width, 6, "# of Sheets", 1, 0, "C");
        $this->Cell($cell5_width, 6, "Number of unit", 1, 0, "C");
        $this->Cell($cell6_width, 6, "Sheets", 1, 0, "C");
        $this->Cell($cell7_width, 6, "%", 1, 0, "C");

        $this->Cell($cell3_width, 6, "Process Stages", 1, 0, "C");
        $this->Cell($cell4_width, 6, "# of Sheets", 1, 0, "C");
        $this->Cell($cell5_width, 6, "Number of unit", 1, 0, "C");
        $this->Cell($cell6_width, 6, "Sheets", 1, 0, "C");
        $this->Cell($cell7_width, 6, "%", 1, 1, "C");



        $headings = [
            ' Sheet Issued From Store',
            ' Pollar',
            ' Printing',
            ' Lamination',
            ' Embossing',
            ' Coating (W/B UV or Screen)',
            ' Die Cutting',
            ' Corrugation',
            ' Sorting',
            ' Pasting',
            ' Dispatch',
        ];
        $left_sheets = [
            ' Sheet Issued From Store'      => number_format($this->record->con_no_sheet),
            ' Pollar'                       => number_format($this->record->con_pollar),
            ' Printing'                     => number_format($this->record->con_printing),
            ' Lamination'                   => number_format($this->record->con_lamination),
            ' Embossing'                    => number_format($this->record->con_embossing),
            ' Coating (W/B UV or Screen)'   => number_format($this->record->con_coating),
            ' Die Cutting'                  => number_format($this->record->con_die_cutting),
            ' Corrugation'                  => number_format($this->record->con_corrugation),
            ' Sorting'                      => number_format($this->record->con_sorting),
            ' Pasting'                      => number_format($this->record->con_pasting/$this->record->no_ups),
            ' Dispatch'                     => number_format($this->record->con_dispatch/$this->record->no_ups),
        ];
        $left_pcs = [
            ' Sheet Issued From Store'      => $this->record->con_no_sheet*$this->record->no_ups,
            ' Pollar'                       => $this->record->con_pollar*$this->record->no_ups,
            ' Printing'                     => $this->record->con_printing*$this->record->no_ups,
            ' Lamination'                   => $this->record->con_lamination*$this->record->no_ups,
            ' Embossing'                    => $this->record->con_embossing*$this->record->no_ups,
            ' Coating (W/B UV or Screen)'   => $this->record->con_coating*$this->record->no_ups,
            ' Die Cutting'                  => $this->record->con_die_cutting*$this->record->no_ups,
            ' Corrugation'                  => $this->record->con_corrugation*$this->record->no_ups,
            ' Sorting'                      => $this->record->con_sorting*$this->record->no_ups,
            ' Pasting'                      => number_format($this->record->con_pasting),
            ' Dispatch'                     => number_format($this->record->con_dispatch),
        ];
        $right_sheets = [
            ' Sheet Issued From Store'      => $this->record->uv_no_sheet,
            ' Pollar'                       => $this->record->uv_pollar,
            ' Printing'                     => $this->record->uv_printing,
            ' Lamination'                   => $this->record->uv_lamination,
            ' Embossing'                    => $this->record->uv_embossing,
            ' Coating (W/B UV or Screen)'   => $this->record->uv_coating,
            ' Die Cutting'                  => $this->record->uv_die_cutting,
            ' Corrugation'                  => number_format($this->record->uv_corrugation),
            ' Sorting'                      => number_format($this->record->uv_sorting),
            ' Pasting'                      => number_format($this->record->uv_pasting),
            ' Dispatch'                     => number_format($this->record->uv_dispatch),
        ];
        $right_pcs = [
            ' Sheet Issued From Store'      => $this->record->uv_no_sheet*$this->record->no_ups,
            ' Pollar'                       => $this->record->uv_pollar*$this->record->no_ups,
            ' Printing'                     => $this->record->uv_printing*$this->record->no_ups,
            ' Lamination'                   => $this->record->uv_lamination*$this->record->no_ups,
            ' Embossing'                    => $this->record->uv_embossing*$this->record->no_ups,
            ' Coating (W/B UV or Screen)'   => $this->record->uv_coating*$this->record->no_ups,
            ' Die Cutting'                  => $this->record->uv_die_cutting*$this->record->no_ups,
            ' Corrugation'                  => $this->record->uv_corrugation*$this->record->no_ups,
            ' Sorting'                      => $this->record->uv_sorting*$this->record->no_ups,
            ' Pasting'                      => number_format($this->record->uv_pasting/$this->record->no_ups),
            ' Dispatch'                     => number_format($this->record->uv_dispatch/$this->record->no_ups),
        ];

        $row = 0;
        foreach ($headings as $heading) {
            // if ($row%5 == 0) { $this->Ln(); }
            $this->Cell( $cell3_width, 8, $heading, 1);

            $this->Cell( $cell4_width, 8, $left_sheets[$heading] . '  ', 1, 0, 'R');
            $this->Cell( $cell5_width, 8, $left_pcs[$heading] . '  ', 1, 0, 'R');
            $this->Cell( $cell6_width, 8, '', 1, 0, 'R');
            $this->Cell( $cell7_width, 8, '', 1, 0, 'R');


            $this->Cell( $cell3_width, 8, $heading, 1);

            $this->Cell( $cell4_width, 8, $right_sheets[$heading] . '  ', 1, 0, 'R');
            $this->Cell( $cell5_width, 8, $right_pcs[$heading] . '  ', 1, 0, 'R');
            $this->Cell( $cell6_width, 8, '', 1, 0, 'R');
            $this->Cell( $cell7_width, 8, '', 1, 1, 'R');

            $row++;
        }

        // Total Row
        $net_wastage = $left_pcs[' Sheet Issued From Store']-$this->record->con_dispatch;
        $wst_sheet = $net_wastage/$this->record->no_ups;
        $wst_percentage = $wst_sheet? ($wst_sheet/$this->record->con_no_sheet*100):0;

        $this->Cell($cell3_width, 6, "Net Wastage", 1, 0, "C");
        $this->Cell($cell4_width, 6, "", 1, 0, "C");
        $this->Cell($cell5_width, 6, number_format($net_wastage,2), 1, 0, "C");
        $this->Cell($cell6_width, 6, number_format($wst_sheet, 2), 1, 0, "C");
        $this->Cell($cell7_width, 6, number_format($wst_percentage, 2).'%', 1, 0, "C");

        $net_wastage = $right_pcs[' Sheet Issued From Store']-$this->record->uv_dispatch;
        $wst_sheet = $net_wastage/$this->record->no_ups;
        $wst_percentage = $wst_sheet? ($wst_sheet/$this->record->uv_no_sheet*100):0;

        $this->Cell($cell3_width, 6, "Net Wastage", 1, 0, "C");
        $this->Cell($cell4_width, 6, "", 1, 0, "C");
        $this->Cell($cell5_width, 6, number_format($net_wastage,2), 1, 0, "C");
        $this->Cell($cell6_width, 6, number_format($wst_sheet, 2), 1, 0, "C");
        $this->Cell($cell7_width, 6, number_format($wst_percentage, 2).'%', 1, 1, "C");
        
        $this->Output();
    }

    // Page footer
    function Footer()
    {
        
    }

    public function resetFont()
    {
        $this->SetFont('ARIAL','',9);
    }

    public function set_data($data)
    {
        $this->record = $data;
        // $this->record->no_ups = 6;
    }

} // end class

?>
