<?php
require_once('fpdf.php');
// require_once('../functions.php');

class PDF_table extends FPDF
{
    protected $option = array();
    // protected $header = array();
    public $width = array();
    public $data = array();
    public $text_elipse = array();
    private $total_col = array();
    private $cell_alignment = array();
    private $cell_format = array();
    private $showMachineNumber = false;
    private $machineNumber = false;
    public $hFontSize = 10;
    public $cFontSize = 10;

    public function __construct($orientation='P', $unit='mm', $size='A4',$option=array('title'=>'Title page','to'=>false,'from'=>false))
    {
        parent::__construct($orientation, $unit, $size);
        $this->option = $option;
    }
    // Load data
    function Header()
    {
        // Logo
        // $this->Image('logo.png',10,6,30);
        // Arial bold 15
        $this->SetFont('Arial','B',14);
        // Move to the right
        // Title
        $this->Cell(0,10,$this->option['title'],0,0,'C');
        // Line break
        $this->Ln(10);

        if (isset($this->option['subtitle'])) {
            $this->SetFont('Arial','B',12);
            $this->Cell(0,10,$this->option['subtitle'],0,0);
            $this->Ln(10);
        }
        if ($this->showMachineNumber) {
            if (($this->machineNumber))
                $machine = "Machine : ".$this->machineNumber;
            else
                $machine = "Machine : All";
            
            $this->Cell(0,10,$machine,0,1);
            
        }
        // Colors, line width and bold font
        // $this->SetFillColor(224,235,255);
        $this->SetFillColor(255);
        $this->SetTextColor(0);
        $this->SetDrawColor(50,50,50);
        $this->SetLineWidth(.3);
        $this->SetFont('','B',$this->hFontSize);
        $this->SetLineWidth(.1);
        // Header
        $headData = $this->data;
        if( count($this->data) ){
            $header = array_keys(array_shift($headData));
            for($i=0;$i<count($header);$i++){
                $w = isset($this->width[$i]) ? $this->width[$i] : 15;
                $this->Cell($w,8,strtoupper($header[$i]),1,0,'C',true);
            }
            $this->Ln();
        }else{
            die( 'Sorry no Result Found.!' );
        }
    } // header

    // Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        // $this->Cell(array_sum($this->width),0,'','T');
        $this->Ln();
        $this->SetY(-10);
        // Arial italic 8
        $this->SetFont('Arial','I',8);
        // Page number
        $this->AliasNbPages('{totalPages}');
        $this->Cell(0,10,'Page '.$this->PageNo().'/{totalPages} '.date('l jS \of F Y h:i:s A'),0,0,'C');
    }

    // Colored table
    function FancyTable()
    {
        // Color and font restoration
        $this->SetFillColor(248,248,248);
        $this->SetTextColor(0);
        $this->SetFont('Arial','',$this->cFontSize);
        $fill = false;

        // initialize total array;
        if (!empty($this->total_col)) {
            foreach ($this->total_col as $col) {
                $sum[$col] = 0;
            }
        }

//        check($sum);

        // Data
        foreach ($this->data as $row) {
            $i=0;
            foreach ($row as $key => $cell) {
                $key = strtolower($key);
                if (array_key_exists($key, $this->text_elipse)){
                    if ( strlen($cell) > $this->text_elipse[$key] ) {
                        $cell = substr($cell, 0,$this->text_elipse[$key]) . '...';
                    }
                }

                (!empty($this->total_col)&&array_key_exists($key, $sum))? $sum[$key] += $cell: null;

                $align = ( array_key_exists($key, $this->cell_alignment) )? $this->cell_alignment[$key]: "L";

                if( array_key_exists($key, $this->cell_format )  && is_numeric($cell) ){
                    $cell = number_format($cell, $this->cell_format[$key]);
                }


                $w = isset($this->width[$i]) ? $this->width[$i] : 10;
                $this->Cell($this->width[$i],6,$cell,"LR",0,$align,$fill);
                $this->SetTextColor(0);
                $i++;
            }
            $fill = !$fill;
            $this->Ln();

        } // row foreach loop

        $this->Cell(array_sum($this->width),4,'','T');
        $this->Ln();
        $i = 0;


        if (!empty($this->total_col)) {
//            check($this->total_col);
            $this->SetFont('Arial','B',9);
            foreach ($this->data[0] as $key => $cell) {
//                check($this->cell_format);
                $key = strtolower($key);
                if ($key == 'job name')
                    $this->Cell($this->width[$i++],6,'TOTAL: ',"B",0,'L');
                elseif (array_key_exists($key, $sum)){
                    $this->Cell($this->width[$i++],6,number_format($sum[$key],$this->cell_format[$key]),"B",0,'R');
                }
                else
                    $this->Cell($this->width[$i++],6,'',"B",0,'R');
            }
        }

    } // FancyTable ()

    public function set_machine($machine)
    {
        $this->showMachineNumber = true;

        if (isset($machine))
            $this->machineNumber = $machine;
    }

    public function set_sum_col($col){
        $columns = array();
        if(is_array($col)){
            foreach ($col as $v) {
                $columns[] = strtolower($v);
            }
            $this->total_col = $columns;
//            check($columns);
        }
    }

    public function set_col_align($col) {
        $columns = array();
        if(is_array($col)){
            foreach ($col as $k=>$v) {
                $columns[strtolower($k)] = $v;
            }
            $this->cell_alignment = $columns;
        }
    }

    public function set_col_format($col) {
        $columns = array();
        if(is_array($col)){
            foreach ($col as $k=>$v) {
                $columns[strtolower($k)] = $v;
            }
            $this->cell_format = $columns;
        }
    }



} // end class

?>
