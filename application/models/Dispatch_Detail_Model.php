<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Dispatch_Detail_Model extends MY_Model
{
	const DB_TABLE = 'dispatch_detail';
    const DB_TABLE_PK = 'dd_id';

    public $dd_id;
    public $dispatch_id;
    public $weight;
    public $no_carton;
    public $no_pcs;
    public $no_roll;
    public $description;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>