<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Xx_Authentication_Model extends MY_Model
{
	const DB_TABLE = 'xx_authentication';
    const DB_TABLE_PK = 'id';

    public $id;
    public $start;
    public $end;
    public $authorized;
    public $generated_key;


    public function getAuthentication()
    {
        $result = $this->db->query("SELECT authorized FROM xx_authentication ORDER BY id DESC LIMIT 1")->row();
        return $result;
    }

    public function getAuthenticationStatus()
    {

        return $this->db->query("SELECT *, 
                                    now() > end AS expire, 
                                    NOW() BETWEEN DATE_SUB(end,INTERVAL +10 DAY) AND end AS notification
                                FROM xx_authentication ORDER BY id DESC LIMIT 1")->row();
        
    }


}
