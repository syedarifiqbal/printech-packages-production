<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Material_Model extends MY_Model
{
	const DB_TABLE = 'material';
    const DB_TABLE_PK = 'material_id';

    public $material_id;
    public $material_name;
    public $category_id;
    public $group_id;
    public $unit; // enum ('KG','PCS','MTR','FT')
    public $size;
    public $thickness;
    public $rate;
    public $density;

    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}

    public function get_material_autocomplete($value='')
    {
        if ($value!="") {
            $this->db->select(['material_id AS id','material_name AS label']);
            $this->db->like('material_name',$value);
            return json_encode( $this->db->get($this::DB_TABLE)->result() );
            // var_dump($this->db->last_query());
        }
    }


    /**************************************************
    *                   GET CURRENT STOCK.
    **************************************************/

    public function get_current_stock($material_id)
    {
        if (isset($material_id)) {
            $sql = "SELECT
                      m.material_name,
                      IFNULL(SUM(grnd.qty),
                      0) - IFNULL(SUM(gind.qty),
                      0) AS balance
                    FROM
                      `grn_detail` AS grnd
                    LEFT JOIN
                      gin_detail AS gind ON grnd.material_id = gind.material_id
                    JOIN
                      material AS m ON m.material_id = grnd.material_id
                    WHERE
                      m.material_id = {$material_id}
                    GROUP BY
                      m.material_id";
            $query = $this->db->query($sql);

            if ($query->num_rows() > 0)
            {
               $row = $query->row(); 
               $balance = $row->balance;
                return number_format($balance,2);
            }else{
                return "0.00";
            }
        }else{
            echo "sorry";
        }
    }


}

 ?>