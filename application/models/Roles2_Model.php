<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Roles2_Model extends MY_Model
{
    const DB_TABLE = 'roles2';
    const DB_TABLE_PK = 'id';

    public $id;
    public $role_name;
    public $description;
    public $added_by;
    public $added_time;
    public $updated_by;
    public $updated_time;

}

 ?>