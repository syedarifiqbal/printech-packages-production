<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Lamination_Detail_Model extends MY_Model
{
	const DB_TABLE = 'Lamination_detail';
    const DB_TABLE_PK = 'ld_id';

    public $ld_id;
    public $lamination_id;
    public $printed_weight;
    public $plain_weight;
    public $laminated_weight;
    public $treatment;
    public $meter;
    public $gsm;
    public $remarks;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>