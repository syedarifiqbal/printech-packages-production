<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Dispatch_Model extends MY_Model
{
	const DB_TABLE = 'dispatch';
    const DB_TABLE_PK = 'id';

    public $id;
    public $date;
    public $so;
    public $gross_weight;
    public $tare_weight;
    public $net_weight;
    public $challan;
    public $destination;
    public $receiver;
    public $no_carton;
    public $no_pcs;
    public $no_roll;
    public $meter;
    public $remarks;
    public $driver_name;
    public $vehicle;

    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;


    public function datewise_dispatch($from,$to,$job,$customer="")
    {
        $job = ($job)? ' AND so.job_code = '.$job : '';
        $customer = ($customer)? ' AND c.customer_id = '.$customer : '';
        return $this->db
                ->query("SELECT DATE_FORMAT(d.date,'%d/%m/%Y') AS date, d.so AS `job #`, st.job_name AS `Job Name`, d.challan AS `CH #`, IF(d.no_pcs>0,d.no_pcs,'-') AS `NO PCS`, d.gross_weight AS GROSS, d.tare_weight AS TARE, d.net_weight AS NET
                        FROM `dispatch` AS d
                        LEFT JOIN sale_order AS so ON d.so = so.job_code
                        LEFT JOIN job_master AS st ON so.structure_id = st.id
                        LEFT JOIN customer AS c ON so.customer_id = c.customer_id
                        WHERE d.date BETWEEN '$from' AND '$to' $job $customer
                        ORDER BY d.date")->result_array();
    }


    public function order_balance($from,$to,$job,$customer)
    {
        $job = ($job)? 'AND so.job_code = '.$job : '';
        $customer = ($customer)? 'AND c.customer_id = '.$customer : '';
        return $this->db
                ->query("SELECT DATE_FORMAT(so.date,'%d/%m/%Y') AS `date`,
                          so.job_code AS `job #`,
                          so.po_num AS `PO #`,
                          DATE_FORMAT(so.delivery_date,'%d/%m/%Y') AS `Due Date`,
                          c.customer_name AS customer,
                          st.job_name AS `Job Name`,
                          so.quantity,
                          IFNULL(
                            CASE WHEN so.order_type = 'pcs' THEN SUM(d.no_pcs) ELSE SUM(d.net_weight)
                          END,
                          0
                        ) AS Delevered,
                        IFNULL(
                          CASE WHEN so.order_type = 'pcs' THEN(
                            SUM(so.quantity) - SUM(d.no_pcs)
                          ) ELSE(
                            SUM(so.quantity) - SUM(d.net_weight)
                          )
                        END,
                        so.quantity
                        ) AS Balance
                        FROM
                          `sale_order` AS so
                        LEFT JOIN
                          (
                          SELECT
                            so,
                            IFNULL(SUM(no_pcs),
                            0) AS no_pcs,
                            IFNULL(SUM(net_weight),
                            0) AS net_weight
                          FROM
                            dispatch
                          GROUP BY
                            so
                        ) AS d ON so.job_code = d.so
                        LEFT JOIN
                          job_master AS st ON so.structure_id = st.id
                        LEFT JOIN
                          customer AS c ON so.customer_id = c.customer_id
                        WHERE so.date BETWEEN '$from' AND '$to' $job $customer
                        GROUP BY so.job_code
                        ORDER BY so.date")->result_array();
    }



    public function detail_order_balance($from,$to,$job,$customer)
    {
        $job = ($job)? 'AND so.job_code = '.$job : '';
        $customer = ($customer)? 'AND c.customer_id = '.$customer : '';
        $query = $this->db
                ->query("SELECT
                          so.job_code,
                          so.po_num `PO #`,
                          so.date `PO DATE`,
                          so.delivery_date `DUE DATE`,
                          c.customer_name `CUSTOMER`,
                          st.job_name 'PRODUCT NAME',
                          so.rate `RATE`,
                      IF( so.order_type = 'pcs', so.quantity, 0 ) AS PCS,
                      IF( so.order_type = 'kg', so.quantity, 0 ) AS KG,
                          so.quantity `ORDER QTY`,
                          '' `REFERENCE #`,
                          '' `DATE`,
                          '' `DELIVERED QTY`
                        FROM
                          sale_order AS so
                        INNER JOIN
                          job_master AS st ON so.structure_id = st.id
                        INNER JOIN
                          customer AS c ON so.customer_id = c.customer_id
                        WHERE so.date BETWEEN '$from' AND '$to' $job $customer
                        GROUP BY so.job_code
                        ORDER BY so.date");
                // echo "<pre>";
                // print_r($this->db->last_query());
                // echo "</pre>";
                return $query->result_array();
    }

    public function find_associated_dispatches($po_number)
    {
        $query = $this->db
                ->query("SELECT
                              d.challan AS id,
                              d.date,
                      IF( so.order_type = 'pcs', d.no_pcs, 0 ) AS PCS,
                      IF( so.order_type = 'kg', d.net_weight, 0 ) AS KG
                            FROM
                              dispatch AS d
                            JOIN
                              sale_order AS so ON d.so = so.job_code
                            WHERE
                              d.so = $po_number
                            ORDER BY
                              d.date");
        // echo "<pre>";
        // print_r($this->db->last_query());
        // echo "</pre>";
        return $query->result_array();
    }



}