<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Todolist_Model extends MY_Model
{
	const DB_TABLE = 'todolist';
    const DB_TABLE_PK = 'id';

    public $id;
    public $title;
    public $user_id;
    public $done;
    public $created_time;
}

 ?>