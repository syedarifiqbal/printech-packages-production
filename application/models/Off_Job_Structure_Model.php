<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Off_Job_Structure_Model extends MY_Model
{
	const DB_TABLE = 'off_job_structure';
    const DB_TABLE_PK = 'id';

    public $id;
    public $job_name;
    public $c;
    public $m;
    public $y;
    public $k;
    public $spot1;
    public $spot2;
    public $spot3;
    public $machine;
    public $sheet_type;
    public $material_id;
    public $grain_size;
    public $gsm;
    public $no_ups;
    public $board_width;
    public $board_height;
    public $water_gloss;
    public $water_matt;
    public $uv_gloss;
    public $uv_spot;
    public $uv_matt;
    public $lamination_gloss;
    public $lamination_matt;
    public $lamination_both;
    public $lamination_type;
    public $warnish_gloss;
    public $warnish_matt;
    public $foil;
    public $foil_type;
    public $color_remarks;
    public $ply;
    public $panel;
    public $roll_size;
    public $cutting_size;
    public $corrugation_remarks;
    public $flutting;
//    public $dubai_flutting;
    public $die_cutting_type;
    public $emboss;
    public $die_remarks;
    public $pasting_remarks;
    public $pasting_type;
    public $packing_qty;
    public $correction;
    public $added_by;
    public $added_time;
    public $updated_by;
    public $updated_time;

    function displayGrain(){
        return ( $this->grain_size == 'w' )? $this->board_width.'"' : $this->board_height.'"';
    }

    function displaySize(){
        return ( $this->grain_size == 'w' )? $this->board_height .'" X '. $this->board_width.'"': $this->board_width .'" X '. $this->board_height.'"';
    }
    
    public function get_dropdown_lists($first_empty=1, $active=1)
    {
       $ret = array_map(
       
               function($o){ 
                   return $o->job_name;
               },

               $this->get()
           );

       return $first_empty? array(''=>'') + $ret : $ret;
    }

}


 ?>