<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Store_Model extends MY_Model
{
	const DB_TABLE = 'store';
    const DB_TABLE_PK = 'store_id';

    public $store_id;
    public $store_name;
    public $description;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>