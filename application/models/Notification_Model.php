<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Notification_Model extends MY_Model
{
	const DB_TABLE = 'notification';
    const DB_TABLE_PK = 'notification_id';

    public $notification_id;
    public $msg_id;
    public $send_user_id;
    public $notification;

    public function get($limit = 500, $offset = 0,$desc=true)
    {
    	$current_user_id = $this->session->userdata('user_id');
    	$notification = $this->db
    	->query("SELECT n.*, ns.user_id AS seen FROM `notification` AS n
				LEFT JOIN notification_seen AS ns
					ON n.notification_id = ns.notification_id AND ns.user_id = $current_user_id
				ORDER BY n.notification_id DESC
				LIMIT 50")->result();
	
		return $notification;
    }

}


 ?>