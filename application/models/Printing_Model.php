<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Printing_Model extends MY_Model
{
	const DB_TABLE = 'printing';
    const DB_TABLE_PK = 'printing_id';

    public $printing_id;
    public $so;
    public $date;
    public $machine;
    public $operator;
    // public $assistant;
    // public $helper;
    public $production_meter;
    public $plain_wastage;
    public $printed_wastage;
    public $setting_wastage;
    public $matching_wastage;
    public $qc_hold;
    public $start_time;
    public $stop_time;

    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

 	

    public function get_monthly_printing_report($from,$to,$machine,$job_code,$customer,$type='object')
    {
        $result = ($type == 'array')? 'result_array':'result';
        $machine = ($machine)?' AND p.machine = '.$machine:'';
        $job_code = ($job_code)?' AND p.so = '.$job_code:'';
        $customer = ($customer)?' AND so.customer_id = '.$customer:'';

        $query = "SELECT DATE_FORMAT(p.date,'%d/%m/%Y') AS `date`,
                so.job_code AS CODE,
                st.job_name AS `JOB NAME`,
                pd.pain_wt AS `P WT`, 
                pd.print_wt AS `PRT WT`, 
                (pd.print_wt-pd.pain_wt) AS `WT GAIN`, 
                SUM(p.plain_wastage) AS `P WST`,
                pd.meter AS `METER`,
                (SUM(p.printed_wastage)) AS `PRT WST`,
                (SUM(p.plain_wastage)+SUM(p.printed_wastage)) AS `T WST`,
                ((SUM(p.plain_wastage)+SUM(p.printed_wastage))/pd.print_wt*100) `%`
            FROM printing AS p
            INNER JOIN sale_order AS so ON p.so = so.job_code
            INNER JOIN job_master AS st ON st.id = so.structure_id
            LEFT JOIN ( 
                SELECT printing_id,
                    SUM(plain_wt) AS pain_wt,
                    SUM(printed_wt) AS print_wt,
                    SUM(meter) AS meter
                FROM printing_detail
                GROUP BY printing_id
             ) AS pd ON pd.printing_id = p.printing_id
            WHERE p.date BETWEEN '$from' AND '$to' $machine $job_code $customer
            GROUP BY p.so, p.printing_id
            ORDER BY p.date,so.job_code";
        $qr = $this->db->query($query);
        // echo "<pre>";
        // print_r($this->db->last_query());
        // echo "</pre>";

        // echo "<pre>";
        // print_r($qr->{$result}());
        // echo "</pre>";

        return $qr->{$result}();
    }

    public function get_daily_printing_report($date, $machine=1,$type='object')
    {
        
        $result = ($type == 'array')? 'result_array':'result';
        $machine = ($machine)?' AND p.machine = '.$machine:'';

        $query = "SELECT
                st.job_name AS `JOB NAME`,
                pd.print_wt AS `PROD. QTY`, 
                (SUM(p.plain_wastage)+SUM(p.printed_wastage)) AS `T WST`,
                '' AS `BL. QTY`,
                pd.meter AS `METER`,
                '' AS `M. SPEED`,
                '' AS `S. TIME`,
                '' AS `E. TIME`,
                '' AS `MC T.S MIN`,
                '' AS `T PROD. MIN`,
                '' AS `AVG. SPD`,
                pd.remarks AS `REMARKS`
            FROM printing AS p
            INNER JOIN sale_order AS so ON p.so = so.job_code
            INNER JOIN job_master AS st ON st.id = so.structure_id
            LEFT JOIN ( 
                SELECT printing_id,
                    SUM(plain_wt) AS pain_wt,
                    SUM(printed_wt) AS print_wt,
                    SUM(meter) AS meter,
                    GROUP_CONCAT(remarks) AS remarks
                FROM printing_detail
                GROUP BY printing_id
             ) AS pd ON pd.printing_id = p.printing_id
            WHERE p.date = '$date' $machine
            GROUP BY p.so, p.printing_id
            ORDER BY p.date,so.job_code";
        $qr = $this->db->query($query);

        return $qr->{$result}();
    }


}

 ?>