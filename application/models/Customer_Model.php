<?php 

/**
* GRAVURE CUSTOMER MODAL
*/
class Customer_Model extends MY_Model
{
	const DB_TABLE = 'customer';
    const DB_TABLE_PK = 'customer_id';

    public $customer_id; # INT
    public $customer_name; # STRING
    public $customer_code; # INT
    public $active; # DATE
    public $toured; # DATE
    public $customer; # INT
    public $client_password; # STRING

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}
     
    public function get_dropdown_lists($first_empty=1, $active=1)
    {
        $ret = array_map(
        
                function($o){ 
                    return $o->customer_name;
                },

                $this->get()
            );

        return $first_empty? array(''=>'') + $ret : $ret;
    }


}

 ?>