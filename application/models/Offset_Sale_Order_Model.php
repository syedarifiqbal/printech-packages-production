<?php

/**
 * ACCOUNT GROUPS MODAL
 */
class Offset_Sale_Order_Model extends MY_Model
{
    const DB_TABLE = 'offset_sale_order';
    const DB_TABLE_PK = 'job_code';

    public $job_code;
    public $date;
    public $po_num;
    public $po_date;
    public $delivery_date;
    public $structure_id;
    public $customer_id;
    public $order_type;
    public $description;
    public $quantity;
    public $excess_quantity;
    public $meter;
    public $remarks;
    public $hold;
    public $completed;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

    public function get_all_orders()
    {
        return $this->db->query('SELECT so.job_code, st.job_name FROM sale_order AS so
                                LEFT JOIN job_master AS st ON so.structure_id=st.id')->result();
    }

    public function get_order_by_job($job_code)
    {
        return $this->db
            ->query("SELECT so.date, so.structure_id, so.po_num, so.remarks, so.po_date, so.delivery_date, so.description, so.job_code, st.*, c.customer_name, so.quantity, so.order_type, so.rate AS rate, u.user_name AS created_by, so.created_time
                        FROM
                          offset_sale_order AS so
                        JOIN
                          off_job_structure AS st ON so.structure_id = st.id
                        JOIN
                          customer AS c ON so.customer_id = c.customer_id
                        JOIN
                          `user` AS u ON so.created_by = u.user_id
                        WHERE
                          so.job_code = $job_code")->row();
    }

    public function get_job_card_data($job_code)
    {
        return $this->db
            ->query("SELECT c.customer_name,
                            u.user_name,
                            so.*,
                            st.*
                        FROM offset_sale_order AS so
                          JOIN customer AS c
                            ON c.customer_id = so.customer_id
                          JOIN off_job_structure AS st
                            ON st.id = so.structure_id
                          JOIN user AS u
                            ON u.user_id = so.created_by
                        WHERE so.job_code = $job_code")->row();
    }

} // class end


?>