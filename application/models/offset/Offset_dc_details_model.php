<?php 

/**
* OFFSET DELIVERY CHALLAN ITEM DETAIL MODAL
*/
class Offset_dc_details_model extends MY_Model
{
	const DB_TABLE = 'offset_dc_details';
    const DB_TABLE_PK = 'dc_id';

    public $dc_id; # INT
    public $job_code; # INT
    public $weight; # INT
    public $pcs = ''; # VARCHAR
    public $cartons = 0; # FLOAT

    public function get_dc_items($id)
    {
        $sql = "SELECT dc_items.*, st.job_name, so.po_num
            FROM offset_dc_details AS dc_items
            JOIN offset_sale_order AS so ON dc_items.job_code = so.job_code
            JOIN off_job_structure AS st ON st.id = so.structure_id
            WHERE dc_items.dc_id = $id";
        return $this->db->query($sql)->result();
    }

}

 ?>