<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Structure_Model extends MY_Model
{
	const DB_TABLE = 'off_job_structure';
    const DB_TABLE_PK = 'id';

    public $id;
    public $job_name;
    public $c = 0;
    public $m = 0;
    public $y = 0;
    public $k = 0;
    public $spot1;
    public $spot2;
    public $spot3;
    public $machine = '';
    public $sheet_type;
    public $material_id;
    public $grain_size;
    public $gsm;
    public $no_ups;
    public $board_width;
    public $board_height;
    public $water_gloss;
    public $water_matt;
    public $uv_gloss;
    public $uv_spot;
    public $uv_matt;
    public $lamination_gloss;
    public $lamination_matt;
    public $lamination_both;
    public $lamination_type;
    public $warnish_gloss;
    public $warnish_matt;
    public $foil;
    public $foil_type;
    public $color_remarks = '';
    public $ply;
    public $panel;
    public $roll_size;
    public $cutting_size;
    public $corrugation_remarks;
    public $flutting;
//    public $dubai_flutting;
    public $die_cutting_type;
    public $emboss;
    public $die_remarks;
    public $pasting_remarks;
    public $pasting_type;
    public $packing_qty;
    public $correction;
    public $active = 1;
    public $added_by;
    // public $added_time;
    public $updated_by;
    // public $updated_time;

    function displayGrain(){
        return ( $this->grain_size == 'w' )? $this->board_width.'"' : $this->board_height.'"';
    }

    function displaySize(){
        return ( $this->grain_size == 'w' )? $this->board_height .'" X '. $this->board_width.'"': $this->board_width .'" X '. $this->board_height.'"';
    }

    public function insert($withoutPrimaryKey=false) {
        $this->db->insert($this::DB_TABLE, $this);
        $this->{$this::DB_TABLE_PK} = $this->db->insert_id();
        // x($this->{$this::DB_TABLE_PK});
        return $this->{$this::DB_TABLE_PK};
    }
    
    // /**
    //  * Update record.
    //  */
    // public function update() {
    //     $this->db->update($this::DB_TABLE, $this, [$this::DB_TABLE_PK=>$this->{$this::DB_TABLE_PK}] );
    //     return $this->db->affected_rows();
    // }
    var $select_column = array("id", "job_name", "board_width", "board_height", "material_id", "gsm", "active");  
    var $order_column = array("id", "job_name", "board_width", "board_height", "material_id", "gsm", null);  
    
    function make_query($active=1)
    {  
       $this->db->select($this->select_column);  
       $this->db->from($this::DB_TABLE);  
       if(isset($_POST["search"]["value"]))  
       {  
            $this->db->where( "`active`= $active" );
            $this->db->group_start();
            $this->db->like("id", $_POST["search"]["value"]);  
            $this->db->or_like("job_name", $_POST["search"]["value"]);   
            $this->db->or_like("board_width", $_POST["search"]["value"]);  
            $this->db->or_like("board_height", $_POST["search"]["value"]);  
            $this->db->or_like("material_id", $_POST["search"]["value"]);  
            $this->db->or_like("gsm", $_POST["search"]["value"]);  
            $this->db->group_end();
       }
       if(isset($_POST["order"]))
       {  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
       }  
       else  
       {  
            // $this->db->order_by('callback_sort', 'ASC');
            $this->db->order_by('id', 'DESC');
       }  
    }

    function make_datatables($active=1){  
        $this->make_query($active);  
        if($_POST["length"] != -1)
        {  
            $this->db->limit($_POST['length'], $_POST['start']);  
        }
        $query = $this->db->get();
        // x($this->db->last_query());
        return $query->result();  
    }

    function get_filtered_data($active=1){  
        $this->make_query($active);  
        $query = $this->db->get();  
        return $query->num_rows();  
    }

    function get_all_data($active=1)  
    {  
        $this->db->select("*");
        $this->db->where( "`active`= $active" );
        $this->db->from($this::DB_TABLE);
        
        return $this->db->count_all_results();
    }

}

// ALTER TABLE `off_job_structure` ADD `active` BOOLEAN NOT NULL DEFAULT TRUE AFTER `correction`, ADD INDEX (`active`);
// ALTER TABLE `off_job_structure` CHANGE `c` `c` TINYINT(1) NULL DEFAULT '1', CHANGE `m` `m` TINYINT(1) NULL DEFAULT '1', CHANGE `y` `y` TINYINT(1) NULL DEFAULT '1', CHANGE `k` `k` TINYINT(1) NULL DEFAULT '1';
 // ALTER TABLE `off_job_structure` CHANGE `machine` `machine` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
// ALTER TABLE `off_job_structure` CHANGE `water_gloss` `water_gloss` TINYINT(1) NULL DEFAULT '0', CHANGE `water_matt` `water_matt` TINYINT(1) NULL DEFAULT '0', CHANGE `uv_gloss` `uv_gloss` TINYINT(1) NULL DEFAULT '0', CHANGE `uv_matt` `uv_matt` TINYINT(1) NULL DEFAULT '0', CHANGE `uv_spot` `uv_spot` TINYINT(1) NULL DEFAULT '0', CHANGE `lamination_gloss` `lamination_gloss` TINYINT(1) NULL DEFAULT '0', CHANGE `lamination_matt` `lamination_matt` TINYINT(1) NULL DEFAULT '0', CHANGE `lamination_both` `lamination_both` TINYINT(1) NULL DEFAULT '0', CHANGE `warnish_gloss` `warnish_gloss` TINYINT(1) NULL DEFAULT '0', CHANGE `warnish_matt` `warnish_matt` TINYINT(1) NULL DEFAULT '0', CHANGE `foil` `foil` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT 'None', CHANGE `color_remarks` `color_remarks` TINYTEXT CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL, CHANGE `dubai_flutting` `dubai_flutting` TINYINT(1) NULL DEFAULT '0';



 ?>