<?php 

/**
* OFFSET CUSTOMER MODAL
*/
class Offset_customer_model extends MY_Model
{
	const DB_TABLE = 'offset_customer';
    const DB_TABLE_PK = 'customer_id';

    public $customer_id; # INT
    public $customer_name; # STRING
    public $customer_code; # INT
    public $active; # DATE
    public $toured = 0; # DATE
    public $customer = 1; # INT
    public $client_password = ''; # STRING

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}
     
    public function get_dropdown_lists($first_empty=1, $active=1)
    {
        $ret = array_map(
        
                function($o){ 
                    return $o->customer_name;
                },

                $this->get()
            );

        return $first_empty? array(''=>'') + $ret : $ret;
    }

    public function extract_customers_from_gravure()
    {
        $sql = "SELECT COUNT(c.customer_id) no_of_orders, c.customer_id, c.customer_name
                FROM customer AS c
                    JOIN `offset_sale_order` AS so ON so.customer_id = c.customer_id
                GROUP BY c.customer_id
                    ORDER BY c.customer_name";

        $sql_for_customer_id = "SELECT c.customer_id
                FROM customer AS c
                    JOIN `offset_sale_order` AS so ON so.customer_id = c.customer_id
                GROUP BY c.customer_id
                    ORDER BY c.customer_name";

        $delete_sql = "DELETE FROM offset_customer WHERE customer_id NOT IN ( $sql_for_customer_id )";

        if( $this->db->simple_query($delete_sql) ){
            x('Record omitted successfully!');
        }else{
            x($this->db->last_query());            
        }

    }

    // Customers queries for list view paginations

    var $select_column = array("customer_id", "customer_name", "active");  
    var $order_column = array("customer_id", "customer_name", null);  
    
    function make_query($active=1)
    {  
       $this->db->select($this->select_column);  
       $this->db->from($this::DB_TABLE);  
       if(isset($_POST["search"]["value"]))  
       {  
            $this->db->where( "`active`= $active" );
            $this->db->group_start();
            $this->db->like("customer_name", $_POST["search"]["value"]);  
            // $this->db->or_like("description", $_POST["search"]["value"]);  
            $this->db->or_like("customer_id", $_POST["search"]["value"]);  
            $this->db->group_end();
       }
       if(isset($_POST["order"]))
       {  
            $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
       }  
       else  
       {  
            // $this->db->order_by('callback_sort', 'ASC');
            $this->db->order_by('customer_id', 'DESC');
       }  
    }

    function make_datatables($active=1){  
        $this->make_query($active);  
        if($_POST["length"] != -1)
        {  
            $this->db->limit($_POST['length'], $_POST['start']);  
        }
        $query = $this->db->get();
        // x($this->db->last_query());
        return $query->result();  
    }

    function get_filtered_data($active=1){  
        $this->make_query($active);  
        $query = $this->db->get();  
        return $query->num_rows();  
    }

    function get_all_data($active=1)  
    {  
        $this->db->select("*");
        $this->db->where( "`active`= $active" );
        $this->db->from($this::DB_TABLE);
        
        return $this->db->count_all_results();
    }


}

 ?>