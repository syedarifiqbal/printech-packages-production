<?php 

/**
* OFFSET MATERIAL MODAL
*/
class Offset_Material_Model extends MY_Model
{
	const DB_TABLE = 'offset_material';
    const DB_TABLE_PK = 'id';

    public $id;
    public $name;
    public $gsm;
    public $active = 1;
    public $width;
    public $height;
    public $unit;
    public $rate;
    public $created_by;
    public $updated_by;

/*

SELECT `category_id`, `category_name`, `description`, `active`
FROM `offset_category`
WHERE `active` = 1
AND   (
`category_name` LIKE '%a%' ESCAPE '!'
OR  `description` LIKE '%a%' ESCAPE '!'
OR  `category_id` LIKE '%a%' ESCAPE '!'
 )
ORDER BY `category_id` DESC
 LIMIT 10

*/

    function listWithCategory(){
        $sql = "SELECT m.*,
                GROUP_CONCAT(DISTINCT c.category_name SEPARATOR ', ') AS categories,
                GROUP_CONCAT(DISTINCT g.group_name SEPARATOR ', ') AS groups
                FROM offset_material m
                LEFT JOIN offset_material_categories AS mc
                  ON mc.material_id = m.id
                LEFT JOIN offset_category AS c
                  ON c.category_id = mc.category_id
                LEFT JOIN offset_material_groups AS mg
                  ON mg.material_id = m.id
                LEFT JOIN offset_material_group AS g
                  ON g.group_id = mg.group_id
                WHERE m.active = 1
                GROUP BY m.id";
        return $this->db->query($sql)->result();
    }

    function get_categories_by_material_id($id){
        $sql = "SELECT c.category_id
                FROM offset_material_categories AS c
                WHERE c.material_id = $id";
        $query = $this->db->query($sql);
        if ($query) {
            return array_column($this->db->query($sql)->result_array(), 'category_id');
        }else{
            return [];
        }
    }

    function get_groups_by_material_id($id){
        $sql = "SELECT c.group_id
                FROM offset_material_groups AS c
                WHERE c.material_id = $id";
        // return array_column($this->db->query($sql)->result_array(),'group_id');
        $query = $this->db->query($sql);
        if ($query) {
            return array_column($result,'group_id');
        }else{
            return [];
        }
    }


    // Datatable Methods
    var $select_column = array("id", "name", "width", "heigth", "gsm", "rate", "categories", "groups", "active");  
    var $order_column = array("id", "name", "width", "heigth", "gsm", "rate", "categories", "groups", null);  
    
    function make_query($active=1)
    {  
       $sql = "SELECT m.*,
                GROUP_CONCAT(DISTINCT c.category_name SEPARATOR ', ') AS categories,
                GROUP_CONCAT(DISTINCT g.group_name SEPARATOR ', ') AS groups
                FROM offset_material m
                LEFT JOIN offset_material_categories AS mc
                  ON mc.material_id = m.id
                LEFT JOIN offset_category AS c
                  ON c.category_id = mc.category_id
                LEFT JOIN offset_material_groups AS mg
                  ON mg.material_id = m.id
                LEFT JOIN offset_material_group AS g
                  ON g.group_id = mg.group_id
                WHERE m.active = $active ";

       if(isset($_POST["search"]["value"]))  
       {  
            $value = $_POST["search"]["value"];
            // $this->db->where( "`active`= $active" );
            $sql .= " AND   (
                m.`name` LIKE '%$value%' ESCAPE '!'
                OR  m.`id` LIKE '%$value%' ESCAPE '!'
                OR  m.`width` LIKE '%$value%' ESCAPE '!'
                OR  m.`height` LIKE '%$value%' ESCAPE '!'
                OR  m.`gsm` LIKE '%$value%' ESCAPE '!'
                OR  m.`rate` LIKE '%$value%' ESCAPE '!'
             ) ";
       }
       $sql .= " GROUP BY m.id ";
       if(isset($_POST["order"]))
       {
            $sql .= " ORDER BY " . $this->order_column[$_POST['order']['0']['column']] . ' ' . $_POST['order']['0']['dir'] . " ";
            // $this->db->order_by($this->order_column[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
       }  
       else  
       {
            $sql .= " ORDER BY m.id DESC ";
       }
       return $sql;
    }

    function make_datatables($active=1){  
        $sql = $this->make_query($active);  
        if($_POST["length"] != -1)
        {
            $sql .= " LIMIT " . $_POST['start'] . ', ' . $_POST['length'];
        }
        $query = $this->db->query($sql);
        return $query->result();  
    }

    function get_filtered_data($active=1){  
        $sql = $this->make_query($active);  
        $query = $this->db->query($sql);  
        $ret = $query->num_rows();
        return $ret;
    }

    function get_all_data($active=1)  
    {  
        $this->db->select("*");
        $this->db->where( "`active`= $active" );
        $this->db->from($this::DB_TABLE);
        $ret = $this->db->count_all_results();
        // $this->write_in_console($ret);
        return $ret;
    }

    function write_in_console($value)
    {
        echo sprintf("<script> console.log('%s'); </script>", addslashes($value));
    }

}


// ALTER TABLE `offset_material` ADD `active` BOOLEAN NOT NULL DEFAULT TRUE AFTER `category_id`, ADD INDEX (`active`);

// ALTER TABLE `offset_material`
//   DROP `group_id`,
//   DROP `category_id`;

 ?>