<?php 

class Offset_Group_Model extends MY_Model
{
    const DB_TABLE = 'offset_material_group';
    const DB_TABLE_PK = 'group_id';

    public $group_id;
    public $group_name;
    public $description;
    public $active = 1;
    public $created_by;
    public $updated_by;

    function get_dropdown_lists($first_empty=1, $active=1)
    {
        $ret = array_map(

                function($o){ 
                    return $o->group_name; 
                },

                $this->getWhere(array('active'=>1))
            );
        return $first_empty? array(''=>'') + $ret : $ret;
    }


    var $select_column = array('group_id', "group_name", "description", "active");
    var $order_column = array('group_id', "group_name", "description", null);
    
    function make_query($active=1)
    {  
       $this->db->select($this->select_column);
       $this->db->from($this::DB_TABLE);
       if(isset($_POST["search"]["value"]))  
       {  
            $this->db->where( "`active`= $active" );
            $this->db->group_start();
            $this->db->like("group_name", $_POST["search"]["value"]);
            $this->db->or_like("description", $_POST["search"]["value"]);
            $this->db->or_like('group_id', $_POST["search"]["value"]);
            $this->db->group_end();
       }
       if(isset($_POST["order"]))
       {  
            $this->db->order_by(
                $this->order_column[$_POST['order']['0']['column']], 
                $_POST['order']['0']['dir']
            );
       }  
       else  
       {
            // $this->db->order_by('callback_sort', 'ASC');
            $this->db->order_by('group_id', 'DESC');
       }  
    }

    function make_datatables($active=1){  
        $this->make_query($active);
        if($_POST["length"] != -1)
        {  
            $this->db->limit($_POST['length'], $_POST['start']);
        }
        $query = $this->db->get();
        // x($this->db->last_query());
        return $query->result();
    }

    function get_filtered_data($active=1){  
        $this->make_query($active);
        $query = $this->db->get();
        return $query->num_rows();
    }

    function get_all_data($active=1)  
    {  
        $this->db->select("*");
        $this->db->where( "`active`= $active" );
        $this->db->from($this::DB_TABLE);
        
        return $this->db->count_all_results();
    }


    // ALTER TABLE `offset_material_group` CHANGE `updated_by` `updated_by` INT(11) NULL, CHANGE `updated_time` `updated_time` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT '0000-00-00 00:00:00';
    // ALTER TABLE `offset_material_group` ADD `active` BOOLEAN NOT NULL DEFAULT TRUE AFTER `description`;
}

 ?>