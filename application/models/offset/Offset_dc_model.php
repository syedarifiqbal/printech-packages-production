<?php 

/**
* OFFSET DELIVERY CHALLAN MODAL
*/
class Offset_dc_model extends MY_Model
{
	const DB_TABLE = 'offset_dc';
    const DB_TABLE_PK = 'id';

    public $id; # INT
    public $date; # DATE
    public $challan_no; # VARCHAR
    public $gatepass_no = ''; # VARCHAR
    public $weight = 0; # FLOAT
    public $no_pcs; # INT
    public $no_cartons; # INT
    public $created_by; # INT
    public $updated_by; # INT


 	public function get_details_by_id($id){
 		$sql = "SELECT dc.id, so.job_code, c.customer_name, dc.date, st.job_name, dcd.weight, dcd.pcs, dcd.cartons, so.po_num, dcd.description, dc.created_time
         FROM offset_dc_details AS dcd
         JOIN offset_dc AS dc
             ON dc.id = dcd.dc_id
         JOIN offset_sale_order AS so
             ON so.job_code = dcd.job_code
         JOIN off_job_structure AS st
             ON st.id = so.structure_id
         JOIN offset_customer AS c ON c.customer_id = so.customer_id
        WHERE dc_id = $id";
        return $this->db->query($sql)->result();
 	}

    // Customers queries for list view paginations

    // Datatable Methods
    public $select_column = array("id", "job_name", "job_code", "date", "po_num", "weight", "pcs", "cartons");
    public $order_column = array("id", "job_name", "job_code", "date", "po_num", "weight", "pcs", "cartons");

    public function make_query($hold = 1)
    {
        $sql = "SELECT dc.id, so.job_code, dc.date, st.job_name, dcd.weight, dcd.pcs, dcd.cartons, so.po_num
                    FROM offset_dc_details AS dcd
                    JOIN offset_dc AS dc
                        ON dc.id = dcd.dc_id
                    JOIN offset_sale_order AS so
                        ON so.job_code = dcd.job_code
                    JOIN off_job_structure AS st
                        ON st.id = so.structure_id";

        if (isset($_POST["search"]["value"])) {
            $value = $_POST["search"]["value"];
            // $this->db->where( "`hold`= $hold" );
            $sql .= " WHERE (
              so.`job_code` LIKE '%$value%' ESCAPE '!'
              OR  dc.`date` LIKE '%$value%' ESCAPE '!'
              OR  dcd.`weight` LIKE '%$value%' ESCAPE '!'
              OR  dcd.`pcs` LIKE '%$value%' ESCAPE '!'
              OR  dcd.`cartons` LIKE '%$value%' ESCAPE '!'
              OR  st.`job_name` LIKE '%$value%' ESCAPE '!'
              OR  so.`po_num` LIKE '%$value%' ESCAPE '!'
           ) ";
        }
        // $sql .= " GROUP BY so.job_code ";
        if (isset($_POST["order"])) {
            $sql .= " ORDER BY " . $this->order_column[$_POST['order']['0']['column']] . ' ' . $_POST['order']['0']['dir'] . " ";
        } else {
            $sql .= " ORDER BY dc.id DESC ";
        }
        return $sql;
    }

    public function make_datatables($hold = 1)
    {
        $sql = $this->make_query($hold);
        if ($_POST["length"] != -1) {
            $sql .= " LIMIT " . $_POST['start'] . ', ' . $_POST['length'];
        }
        $query = $this->db->query($sql);
        // x($this->db->last_query());
        return $query->result();
    }

    public function get_filtered_data($hold = 1)
    {
        $sql = $this->make_query($hold);
        $query = $this->db->query($sql);
        $ret = $query->num_rows();
        return $ret;
    }

    public function get_all_data($hold = 1)
    {
        $this->db->select("*");
        // $this->db->where("`hold`= $hold");
        $this->db->from('offset_dc_details');
        $ret = $this->db->count_all_results();
        // $this->write_in_console($ret);
        return $ret;
    }

    public function write_in_console($value)
    {
        echo sprintf("<script> console.log('%s'); </script>", addslashes($value));
    }


}

 ?>