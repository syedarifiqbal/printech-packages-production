<?php

/**
 * OFFSET SALE ORDER MODAL
 */
class Offset_sale_order_model extends MY_Model
{
    const DB_TABLE = 'offset_sale_order';
    const DB_TABLE_PK = 'job_code';

    public $job_code;
    public $date;
    public $po_num;
    public $po_date;
    public $delivery_date;
    public $structure_id;
    public $customer_id;
    public $order_type;
    public $description;
    public $quantity;
    public $excess_quantity;
    public $rate = 0;
    public $meter = 0;
    public $remarks;
    public $hold;
    public $completed;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

    public function get_all_orders()
    {
        return $this->db->query('SELECT so.job_code, st.job_name FROM sale_order AS so
                                LEFT JOIN job_master AS st ON so.structure_id=st.id')->result();
    }

    public function get_order_by_job($job_code)
    {
        return $this->db
            ->query("SELECT so.date, so.structure_id, so.po_num, so.remarks, so.po_date, so.delivery_date, so.description, so.job_code, st.*, c.customer_name, so.quantity, so.order_type, so.rate AS rate, u.user_name AS created_by, so.created_time
                        FROM
                          offset_sale_order AS so
                        JOIN
                          off_job_structure AS st ON so.structure_id = st.id
                        JOIN
                          customer AS c ON so.customer_id = c.customer_id
                        JOIN
                          `user` AS u ON so.created_by = u.user_id
                        WHERE
                          so.job_code = $job_code")->row();
    }

    public function get_job_card_data($job_code)
    {
        return $this->db
            ->query("SELECT c.customer_name,
                            u.user_name,
                            so.*,
                            st.*
                        FROM offset_sale_order AS so
                          JOIN customer AS c
                            ON c.customer_id = so.customer_id
                          JOIN off_job_structure AS st
                            ON st.id = so.structure_id
                          JOIN user AS u
                            ON u.user_id = so.created_by
                        WHERE so.job_code = $job_code")->row();
    }

    public function get_join_data_by_id($id)
    {
        $sql = "SELECT so.*, st.job_name, cs.customer_name
        FROM offset_sale_order AS so
        LEFT JOIN off_job_structure AS st
          ON so.structure_id = st.id
        LEFT JOIN customer AS cs
          ON so.customer_id = cs.customer_id
        WHERE so.job_code = {$id}";

        return $this->db->query($sql)->row();
    }

    // Queries for datatables
    public function listWithCategory()
    {
        $sql = "SELECT m.*,
              GROUP_CONCAT(DISTINCT c.category_name SEPARATOR ', ') AS categories,
              GROUP_CONCAT(DISTINCT g.group_name SEPARATOR ', ') AS groups
              FROM offset_material m
              LEFT JOIN offset_material_categories AS mc
                ON mc.material_id = m.id
              LEFT JOIN offset_category AS c
                ON c.category_id = mc.category_id
              LEFT JOIN offset_material_groups AS mg
                ON mg.material_id = m.id
              LEFT JOIN offset_material_group AS g
                ON g.group_id = mg.group_id
              WHERE m.active = 1
              GROUP BY m.id";
        return $this->db->query($sql)->result();
    }

    public function get_categories_by_material_id($id)
    {
        $sql = "SELECT c.category_id
              FROM offset_material_categories AS c
              WHERE c.material_id = $id";
        $query = $this->db->query($sql);
        if ($query) {
            return array_column($this->db->query($sql)->result_array(), 'category_id');
        } else {
            return [];
        }
    }

    public function get_groups_by_material_id($id)
    {
        $sql = "SELECT c.group_id
              FROM offset_material_groups AS c
              WHERE c.material_id = $id";
        // return array_column($this->db->query($sql)->result_array(),'group_id');
        $query = $this->db->query($sql);
        if ($query) {
            return array_column($result, 'group_id');
        } else {
            return [];
        }
    }
    
    public function count_all_orders_current_month()
    {
        $sql = "SELECT COUNT(job_code) AS total 
        FROM offset_sale_order 
        WHERE created_time 
            BETWEEN DATE_FORMAT(CURRENT_DATE(), '%Y-%m-01') AND LAST_DAY(CURRENT_DATE())";
        $count = $this->db->query($sql)->row();
        return $count->total;
    }
    
    public function count_last_six_month_orders()
    {
        // DATE_SUB(curdate(), INTERVAL 1 MONTH)
        $sql = "SELECT COUNT(job_code) AS total 
        FROM offset_sale_order 
        WHERE created_time 
            BETWEEN DATE_FORMAT(DATE_SUB(curdate(), INTERVAL 6 MONTH), '%Y-%m-01') AND LAST_DAY(DATE_SUB(curdate(), INTERVAL 1 MONTH))";
        $count = $this->db->query($sql)->row();
        return $count->total;
    }

    // Datatable Methods
    public $select_column = array("job_code", "job_name", "date", "po_date", "delivery_date", "customer_name", "hold");
    public $order_column = array("job_code", "job_name", "date", "po_date", "delivery_date", "customer_name", null);

    public function make_query($hold = 1)
    {
        $sql = "SELECT
                so.job_code,
                so.date,
                so.po_date,
                so.delivery_date,
                st.job_name,
                cs.customer_name,
                so.remarks,
                so.hold,
                i.id AS info_id
            FROM
                offset_sale_order AS so
            LEFT JOIN
                off_job_structure AS st ON so.structure_id = st.id
            LEFT JOIN
                customer AS cs ON so.customer_id = cs.customer_id
            LEFT JOIN 
                offset_production_info AS i ON so.job_code = i.job_code";

        if (isset($_POST["search"]["value"])) {
            $value = $_POST["search"]["value"];
            // $this->db->where( "`hold`= $hold" );
            $sql .= " WHERE so.hold = $hold AND   (
              so.`job_code` LIKE '%$value%' ESCAPE '!'
              OR  so.`date` LIKE '%$value%' ESCAPE '!'
              OR  so.`po_date` LIKE '%$value%' ESCAPE '!'
              OR  so.`delivery_date` LIKE '%$value%' ESCAPE '!'
              OR  st.`job_name` LIKE '%$value%' ESCAPE '!'
              OR  cs.`customer_name` LIKE '%$value%' ESCAPE '!'
           ) ";
        }
        $sql .= " GROUP BY so.job_code ";
        if (isset($_POST["order"])) {
            $sql .= " ORDER BY " . $this->order_column[$_POST['order']['0']['column']] . ' ' . $_POST['order']['0']['dir'] . " ";
        } else {
            $sql .= " ORDER BY so.job_code DESC ";
        }
        return $sql;
    }

    public function make_datatables($hold = 1)
    {
        $sql = $this->make_query($hold);
        if ($_POST["length"] != -1) {
            $sql .= " LIMIT " . $_POST['start'] . ', ' . $_POST['length'];
        }
        $query = $this->db->query($sql);
        // x($this->db->last_query());
        return $query->result();
    }

    public function get_filtered_data($hold = 1)
    {
        $sql = $this->make_query($hold);
        $query = $this->db->query($sql);
        $ret = $query->num_rows();
        return $ret;
    }

    public function get_all_data($hold = 1)
    {
        $this->db->select("*");
        $this->db->where("`hold`= $hold");
        $this->db->from($this::DB_TABLE);
        $ret = $this->db->count_all_results();
        // $this->write_in_console($ret);
        return $ret;
    }

    public function write_in_console($value)
    {
        echo sprintf("<script> console.log('%s'); </script>", addslashes($value));
    }

} // class end
