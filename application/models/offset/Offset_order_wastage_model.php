<?php

/**
 * OFFSET SALE ORDER WASTAGE MODEL
 */
class Offset_order_wastage_model extends MY_Model
{
    const DB_TABLE = 'offset_production_info';
    const DB_TABLE_PK = 'id';

    public $id;
    public $job_code;
    public $dispatch_date;
    public $con_no_sheet;
    public $con_pollar;
    public $con_printing;
    public $con_lamination;
    public $con_embossing;
    public $con_coating;
    public $con_die_cutting;
    public $con_pasting;
    public $con_dispatch;
    public $con_sorting;
    public $con_corrugation;

    public $uv_no_sheet;
    public $uv_pollar;
    public $uv_printing;
    public $uv_lamination;
    public $uv_embossing;
    public $uv_coating;
    public $uv_die_cutting;
    public $uv_pasting;
    public $uv_dispatch;
    public $uv_sorting;
    public $uv_corrugation;

    public $added_by;
    public $added_time;
    public $updated_by;
    public $updated_time;

    public function load_by_id($id)
    {
        return $this->db->query("SELECT so.job_code, st.job_name, st.no_ups, i.*
                                FROM offset_production_info AS i
                                INNER JOIN offset_sale_order AS so
                                    ON so.job_code = i.job_code
                                INNER JOIN off_job_structure AS st 
                                    ON so.structure_id=st.id
                                WHERE i.id = $id")->row();
    }

} // class end


?>