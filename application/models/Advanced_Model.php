<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Advanced_Model extends MY_Model
{
	const DB_TABLE = 'advanced_salary';
    const DB_TABLE_PK = 'id';

    public $id;
    public $date;
    public $amount;
    public $code;
    public $designation;
    public $name;
    public $father_name;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;
}

 ?>