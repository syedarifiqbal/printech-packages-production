<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Job_Master_Model extends MY_Model
{
	const DB_TABLE = 'job_master';
    const DB_TABLE_PK = 'id';

    public $id; # INT
    public $job_name; # STRING
    public $file_number; # INT
    public $print_type; # STRING
    public $print_as_per; # STRING
    public $cylinder; # STRING
    public $cylinder_len; # INT
    public $cylinder_circum; # INT
    public $no_ups; # INT
    public $no_repeat; # INT
    public $no_lamination; # INT
    public $print_material; # INT
    public $lam1_material; # INT
    public $lam2_material; # INT
    public $lam3_material; # INT
    public $slitting_reel_width; # STRING
    public $slitting_cutting_size; # STRING
    public $slitting_direction; # STRING
    public $ink_series; # STRING
    public $no_of_inks; # INT
    public $bm_qty; # INT
    public $bm_width; # STRING
    public $bm_height; # STRING
    public $bm_types; # STRING
    public $coa_requird; # tinyint
    public $dc; # STRING
    public $hold; # tinyint
    public $remarks; # TEXT
    public $reason; # VARCHAR
    public $create_by; # INT
    public $created_time; # TIMESTUMP
    public $update_by; # INT
    public $updated_time; # TIMESTUMP

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>