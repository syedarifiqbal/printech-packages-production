<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Group_Model extends MY_Model
{
	const DB_TABLE = 'material_group';
    const DB_TABLE_PK = 'group_id';

    public $group_id;
    public $group_name;
    public $description;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;


}

 ?>