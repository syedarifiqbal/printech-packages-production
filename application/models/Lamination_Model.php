<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Lamination_Model extends MY_Model
{
	const DB_TABLE = 'lamination';
    const DB_TABLE_PK = 'id';

    public $id;
    public $date;
    public $so;
    public $machine;
    public $operator;
    public $assistant;
    public $helper;
    public $temperature;
    public $rubber_size;
    public $speed;
    public $qc_hold;
    public $start_time;
    public $end_time;
    public $blade;
    public $lamination_number;
    public $total_wastage;
    public $balance;
    public $glue;
    public $glue_qty;
    public $hardner;
    public $hardner_qty;
    public $ethyl;
    public $ethyl_qty;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}

    public function datewise_lamination($from,$to,$job,$machine)
    {
        $job = ($job)? 'AND so.job_code = '.$job : '';
        $machine  = ($machine)? "AND l.machine = '".$machine ."'" : '';
        $query = $this->db
                ->query("SELECT l.date, so.job_code AS `JOB #`, st.job_name AS `JOB NAME`, l.machine AS `MCHN`, 
                        l.operator,
                        SUM(ld.printed_weight) AS `PRINT WEIGHT`,
                        SUM(ld.plain_weight) AS `PLAIN WEIGHT`,
                        l.glue, l.glue_qty AS `GLUE WT`, SUM(ld.laminated_weight) AS `WEIGHT OUT`, 
                        ( SUM(ld.laminated_weight) - ( SUM( ld.printed_weight ) + SUM( ld.plain_weight ) ) ) AS `WEIGHT GAIN`,
                        SUM( ld.meter ) AS METER, sum( l.total_wastage ) AS `T WST`
                        FROM `lamination` AS l 
                        LEFT JOIN sale_order AS so ON so.job_code = l.so
                        LEFT JOIN lamination_detail AS ld ON l.id = ld.lamination_id
                        LEFT JOIN job_master AS st ON so.structure_id = st.id
                        WHERE l.date BETWEEN '$from' AND '$to' $job $machine
                        GROUP BY so.job_code
                        ORDER BY l.date");
        // echo "<pre>";
        // print_r($this->db->last_query());
        // echo "</pre>";
        return $query->result_array();
    }

    public function get_daily_lamination_report($date,$machine,$type='object')
    {
        $result = ($type == 'array')? 'result_array':'result';
        $machine  = ($machine)? "AND l.machine = '".$machine ."'" : '';

        $query = $this->db
                ->query("SELECT 
                    st.job_name AS `JOB NAME`,
                        SUM(ld.laminated_weight) AS `PROD. QTY`,
                        sum( l.total_wastage ) AS `T WST`,
                        '' AS `BL. QTY`,
                        SUM( ld.meter ) AS `METER`,
                        l.speed AS `M. SPEED`,
                        l.start_time AS `S. TIME`,
                        l.end_time AS `E. TIME`,
                        '' AS `MC T.S MIN`,
                        '' AS `T PROD. MIN`,
                        '' AS `AVG. SPD`,
                        GROUP_CONCAT(ld.remarks) AS `REMARKS`
                        FROM `lamination` AS l 
                        LEFT JOIN sale_order AS so ON so.job_code = l.so
                        LEFT JOIN lamination_detail AS ld ON l.id = ld.lamination_id
                        LEFT JOIN job_master AS st ON so.structure_id = st.id
                        WHERE l.date = '$date' $machine
                        GROUP BY so.job_code
                        ORDER BY l.date");
        // echo "<pre>";
        // print_r($this->db->last_query());
        // echo "</pre>";
        return $query->{$result}();
    }

}

 ?>