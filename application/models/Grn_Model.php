<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Grn_Model extends MY_Model
{
	const DB_TABLE = 'grn';
    const DB_TABLE_PK = 'grn_id';

    public $grn_id;
    public $date;
    // public $supplier_id;
    public $description;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>