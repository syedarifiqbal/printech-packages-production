<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Health_and_Safety_Model extends MY_Model
{
	const DB_TABLE = 'health_and_safety';
    const DB_TABLE_PK = 'id';

    public $id;
    public $date;
    public $department;
    public $detail;
    public $shift;
    public $reported_person;
    public $injured_person;
    public $employee_id;
    public $contractor_emp_id;
    public $risk_category;
    public $accident_category;
    public $early_decision;
    public $root_cause;
    public $preventice;
    public $reviewed_by;
    public $reviewed_time;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;
}

 ?>