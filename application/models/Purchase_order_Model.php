<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Purchase_Order_Model extends MY_Model
{
	const DB_TABLE = 'purchase_order';
    const DB_TABLE_PK = 'po_id';

    public $po_id;
    public $date;
    public $supplier_id;
    public $hold;
    public $description;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}



    public function get_pending_po_autocomplete($value='')
    {
        if ($value!="") {

            $query = $this->db
            ->query( "SELECT po.po_id, po.supplier_id, s.supplier_name, pod.material_id AS id, m.material_name AS label, pod.rate, pod.qty AS Ordered, ifnull(SUM(grnd.qty),0) AS Received, ( SUM(pod.qty) - SUM(grnd.qty) ) AS balance
                        FROM po_detail AS pod
                        LEFT JOIN grn_detail AS grnd
                            ON grnd.po_id=pod.po_id
                            AND grnd.material_id = pod.material_id
                        LEFT JOIN purchase_order AS po
                            ON po.po_id = pod.po_id
                        LEFT JOIN material AS m
                            ON m.material_id=pod.material_id
                        LEFT JOIN supplier AS s
                            ON s.supplier_id=po.supplier_id
                        WHERE pod.po_id not in(
                            SELECT grn.po_id FROM GRN_detail AS grn
                            WHERE grn.po_id != pod.po_id
                            OR grn.qty >= pod.qty )
                        AND m.material_name LIKE '%{$value}%'
                        AND po.hold = 0
                        GROUP BY pod.material_id, pod.po_id");

            // $this->db->select(['material_id AS id','material_name AS label']);
            // $this->db->like('material_name',$value);
            return json_encode( $query->result() );
            // var_dump($this->db->last_query());
        }
    }


}

 ?>