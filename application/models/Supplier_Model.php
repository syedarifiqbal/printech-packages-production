<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Supplier_Model extends MY_Model
{
	const DB_TABLE = 'supplier';
    const DB_TABLE_PK = 'supplier_id';

    public $supplier_id; # INT
    public $supplier_name; # STRING
    public $supplier_code; # INT
    public $active; # DATE
    public $toured; # DATE
    public $supplier; # INT
    public $client_password; # STRING

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}

    public function get_supplier_autocomplete($value='')
    {
        if ($value!="") {
            $this->db->select(['supplier_id AS id','supplier_name AS label']);
            $this->db->like('supplier_name',$value);
            return json_encode( $this->db->get($this::DB_TABLE)->result() );
            // var_dump($this->db->last_query());
        }
    }


}

 ?>