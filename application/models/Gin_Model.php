<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Gin_Model extends MY_Model
{
	const DB_TABLE = 'gin';
    const DB_TABLE_PK = 'gin_id';

    public $gin_id;
    public $date;
    public $issue_to;
    public $description;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>