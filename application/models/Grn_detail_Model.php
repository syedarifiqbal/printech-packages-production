<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Grn_detail_Model extends MY_Model
{
	const DB_TABLE = 'grn_detail';
    const DB_TABLE_PK = 'grnd_id';

    public $grnd_id;
    public $grn_id;
    public $material_id;
    public $store_id;
    public $roll_carton;
    public $qty;
    public $rate;
    public $defective;
    public $description;
    public $po_id;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>