<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Permission_Model extends MY_Model
{
    const DB_TABLE = 'permissions';
    const DB_TABLE_PK = 'id';

    public $id;
    public $permission;
    public $display_name;
    public $group_name;
    public $department;
}

 ?>