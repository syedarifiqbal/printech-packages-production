<?php 

class Category_Model extends MY_Model
{
	const DB_TABLE = 'offset_category';
    const DB_TABLE_PK = 'category_id';

    public $category_id;
    public $category_name;
    public $description;
    public $created_by;
    // public $created_time;
    public $updated_by;
    // public $updated_time;

    function get_dropdown_lists($first_empty=1, $active=1)
    {
        $ret = array_map(

                function($o){ 
                    return $o->category_name; 
                },

                $this->getWhere(array('active'=>1))
            );

        return $first_empty? array_unshift($ret, '') : $ret;
    }

}

 ?>