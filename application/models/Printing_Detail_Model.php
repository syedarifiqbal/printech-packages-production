<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Printing_Detail_Model extends MY_Model
{
	const DB_TABLE = 'printing_detail';
    const DB_TABLE_PK = 'pd_id';

    public $pd_id;
    public $printing_id;
    public $plain_wt;
    public $printed_wt;
    public $meter;
    public $start_time;
    public $end_time;
    public $remarks;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>