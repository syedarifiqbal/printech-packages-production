<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Gin_detail_Model extends MY_Model
{
	const DB_TABLE = 'gin_detail';
    const DB_TABLE_PK = 'gind_id';

    public $gind_id;
    public $gin_id;
    public $material_id;
    public $store_id;
    public $roll_carton;
    public $qty;
    public $rate;
    public $defective;
    public $description;
    public $job_code;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>