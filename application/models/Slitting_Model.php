<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Slitting_Model extends MY_Model
{
	const DB_TABLE = 'slitting';
    const DB_TABLE_PK = 'id';

    public $id;
    public $date;
    public $so;
    public $operator;
    public $start_time;
    public $end_time;
    public $weight_before;
    public $weight_after;
    public $wastage;
    public $trim;
    public $direction;
    public $remarks;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;


    public function get_monthly_rewinding_report($from,$to,$machine,$job_code,$customer,$type='object')
    {
        $result = ($type == 'array')? 'result_array':'result';
        $machine = ($machine)?' AND s.machine = '.$machine:'';
        $job_code = ($job_code)?' AND s.so = '.$job_code:'';
        $customer = ($customer)?' AND so.customer_id = '.$customer:'';

        $query = "SELECT 
                        DATE_FORMAT(s.date, '%d/%m/%Y') AS `date`,
                        so.job_code AS code,
                        st.job_name AS `job name`,
                        'S/L' AS `Mchn`,
                        replace(
                            SUBSTRING(
                                m.material_name FROM 
                                (locate('mm',m.material_name)-4)
                            ),
                            '-',''
                        ) AS `CUT SZ`,
                        -- m.material_name AS `tempurary name`,
                        null AS `DIRC.`,
                        SUM(s.weight_before) AS `B4 SL`,
                        SUM(s.weight_after) AS `AFT. SL`,
                        (SUM(s.trim)) AS `TRIM`,
                        ROUND(SUM(s.trim) * 100 / s.weight_before, 2) `TRIM %`,
                        (SUM(s.wastage)) AS `WAST`,
                        ROUND(SUM(s.wastage) * 100 / s.weight_before, 2) `WAST %`
                    FROM
                        slitting AS s
                            INNER JOIN
                        sale_order AS so ON s.so = so.job_code
                            INNER JOIN
                        job_master AS st ON st.id = so.structure_id
                            INNER JOIN
                        material AS m ON st.print_material = m.material_id
                WHERE s.date BETWEEN '$from' AND '$to' $machine $job_code $customer
                GROUP BY s.so
                ORDER BY s.date";
        $qr = $this->db->query($query);
        // echo "<pre>";
        // print_r($this->db->last_query());
        // echo "</pre>";

        // echo "<pre>";
        // print_r($qr->{$result}());
        // echo "</pre>";

        return $qr->{$result}();
    }

}

 ?>