<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Sale_Order_Model extends MY_Model
{
	const DB_TABLE = 'sale_order';
    const DB_TABLE_PK = 'job_code';

    public $job_code;
    public $date;
    public $po_num;
    public $po_date;
    public $delivery_date;
    public $structure_id;
    public $customer_id;
    public $order_type;
    public $description;
    public $quantity;
    public $meter;
    public $film1_wt;
    public $film2_wt;
    public $film3_wt;
    public $film4_wt;
    public $remarks;
    public $deliveries;
    public $hold;
    public $completed;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}

    public function get_all_orders()
    {
        return $this->db->query('SELECT so.job_code, st.job_name FROM sale_order AS so
                                LEFT JOIN job_master AS st ON so.structure_id=st.id')->result();
    }


    public function get_order_by_job($job_code)
    {
        // $job_code = ($job_code)? 'AND so.job_code = '.$job_code : '';
        return $this->db
                ->query("SELECT so.date, so.structure_id, so.po_num, so.po_date, so.delivery_date, so.description, so.job_code, st.job_name, c.customer_name, so.quantity, so.order_type, so.meter, so.rate AS rate,
                                        f1.material_name AS printing_material, so.film1_wt AS printing_weight,
                                        f2.material_name AS lm1_material, so.film2_wt AS lam1_weight,
                                        f3.material_name AS lm2_material, so.film3_wt AS lam2_weight,
                                        f4.material_name AS lm3_material, so.film4_wt AS lam3_weight,
                                        ap.user_name AS aproved_by,u.user_name AS created_by, so.created_time
                                        FROM sale_order AS so
                                            LEFT JOIN job_master AS st ON so.structure_id = st.id
                                            LEFT JOIN customer AS c ON so.customer_id = c.customer_id
                                            LEFT JOIN material AS f1 ON st.print_material = f1.material_id
                                            LEFT JOIN material AS f2 ON st.lam1_material = f2.material_id
                                            LEFT JOIN material AS f3 ON st.lam2_material = f3.material_id
                                            LEFT JOIN material AS f4 ON st.lam3_material = f4.material_id
                                            LEFT JOIN `user` AS u ON so.created_by = u.user_id
                                            LEFT JOIN `user` AS ap ON so.approved = ap.user_id
                                        WHERE so.job_code = $job_code")->row();
    }


    public function get_wastage_report_by_date($from,$to,$machine,$type='object',$third_lam = true)
    {
        $result = ($type == 'array')? 'result_array':'result';
        $machine = ($machine)?' AND p.machine = '.$machine:'';
        if( $third_lam ){
            $third_lam="l3.L3_weight_in AS l3_in,
              l3.L3_plain_wt AS l3_pln,
              l3.L3_weight_out AS l3_ot,
              l3.L3_wastage AS l3_wst,";
        }else{ $third_lam=''; }
        $query = "SELECT DATE_FORMAT(p.date,'%d/%m/%Y') AS `date`,
                   so.job_code AS code,
                   st.job_name AS Job,
                   p.machine AS mchn,
                   p.p_plain AS p_pln,
                   p.p_printed AS p_prtd,
                   p.p_wastage AS p_wst,
                   r.R_in,
                   r.R_out,
                   r.R_wastage AS r_wst,
                   l1.L1_weight_in AS l1_in,
                   l1.L1_plain_wt AS l1_pln,
                   l1.L1_weight_out AS l1_ot,
                   l1.L1_wastage AS l1_wst,
                   l2.L2_weight_in AS l2_in,
                   l2.L2_plain_wt AS l2_pln,
                   l2.L2_weight_out AS l2_ot,
                   l2.L2_wastage AS l2_wst, $third_lam
                   s.S_weight_in AS s_wt_in,
                   s.S_weight_out AS s_wt_ot,
                   s.S_wst,
                   s.S_trim,
                   d.gross,
                   d.net
            FROM sale_order AS SO
            INNER JOIN job_master AS st ON so.structure_id = st.id
            INNER JOIN
              ( SELECT p.date,
                       p.so,
                       p.machine,
                       SUM(pd.p_plain) AS p_plain,
                       SUM(pd.p_printed) AS p_printed,
                       SUM( p.plain_wastage + p.printed_wastage + p.setting_wastage + p.matching_wastage ) AS p_wastage
               FROM printing AS p
               INNER JOIN
                 ( SELECT spd.printing_id,
                          SUM(spd.plain_wt) AS p_plain,
                          SUM(spd.printed_wt) AS p_printed
                  FROM printing_detail AS spd
                  GROUP BY spd.printing_id ) AS pd ON p.printing_id = pd.printing_id
               GROUP BY p.so) AS p ON p.so = so.job_code
            LEFT JOIN
              ( SELECT so,
                       SUM(weight_before) AS R_in,
                       SUM(weight_after) AS R_out,
                       SUM(wastage) AS R_wastage
               FROM rewinding
               GROUP BY so) AS r ON so.job_code = r.so
            LEFT JOIN
              ( SELECT
                  l.so,
                    SUM(sld.printed_weight) AS L1_weight_in,
                    SUM(sld.plain_weight) AS L1_plain_wt,
                    SUM(sld.laminated_weight) AS L1_weight_out,
                    SUM(l.total_wastage) AS L1_wastage
                FROM
                  lamination AS l
                  JOIN lamination_detail AS sld
                  ON l.id = sld.lamination_id  
                WHERE
                  l.lamination_number = 1
                GROUP BY
                    l.so) AS l1 ON so.job_code = l1.so
            LEFT JOIN
              ( SELECT
                  l.so,
                    SUM(sld.printed_weight) AS L2_weight_in,
                    SUM(sld.plain_weight) AS L2_plain_wt,
                    SUM(sld.laminated_weight) AS L2_weight_out,
                    SUM(l.total_wastage) AS L2_wastage
                FROM
                  lamination AS l
                  JOIN lamination_detail AS sld
                  ON l.id = sld.lamination_id  
                WHERE
                  l.lamination_number = 2
                GROUP BY
                    l.so) AS l2 ON so.job_code = l2.so
            LEFT JOIN
              ( SELECT
                  l.so,
                    SUM(sld.printed_weight) AS L3_weight_in,
                    SUM(sld.plain_weight) AS L3_plain_wt,
                    SUM(sld.laminated_weight) AS L3_weight_out,
                    SUM(l.total_wastage) AS L3_wastage
                FROM
                  lamination AS l
                  JOIN lamination_detail AS sld
                  ON l.id = sld.lamination_id  
                WHERE
                  l.lamination_number = 3
                GROUP BY
                    l.so) AS l3 ON so.job_code = l3.so
            LEFT JOIN
              ( SELECT so,
                       SUM(weight_before) AS S_weight_in,
                       SUM(weight_after) AS S_weight_out,
                       SUM(wastage) AS S_wst,
                       SUM(TRIM) AS S_trim
               FROM slitting
               GROUP BY so) AS s ON so.job_code = s.so
            LEFT JOIN
              ( SELECT d.so,
                       SUM(d.gross_weight) AS gross,
                       SUM(d.net_weight) AS net
               FROM DISPATCH AS d
               GROUP BY d.so) AS d ON so.job_code = d.so
            WHERE p.date BETWEEN '$from' AND '$to' $machine
            ORDER BY p.date,so.job_code";
        $qr = $this->db->query($query);
        // echo "<pre>";
        // print_r($this->db->last_query());
        // echo "</pre>";

        // echo "<pre>";
        // print_r($qr->{$result}());
        // echo "</pre>";

        return $qr->{$result}();
    }


} // class end








 ?>