<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Notification_Seen_Model extends MY_Model
{
	const DB_TABLE = 'notification_seen';
    const DB_TABLE_PK = 'id';

    public $id;
    public $notification_id;
    public $user_id;
}

 ?>