<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Personal_Message_Model extends MY_Model
{
	const DB_TABLE = 'personal_message';
    const DB_TABLE_PK = 'id';

    public $id;
    public $from;
    public $to;
    public $flag;
    public $message;
    public $timestamp;
    public $seen;
    public $attachment;


}

// 7.5 sikndr
// 36.5 arif

 /* my sample */
// SELECT
//   m1.*
// FROM
//   `personal_message` AS m1
// INNER JOIN
//   (
//   SELECT id, MAX(`timestamp`)
//   FROM
//     personal_message
//       GROUP BY `from`, `to`
// ) AS m2 ON m1.id = m2.id
// GROUP BY m1.timestamp



// SELECT
//   personal_message.*
// FROM
//   ( SELECT MAX(id) AS id
//   	FROM
//     	personal_message
//   	WHERE
//     	1 IN(`from`,`to`)
//   	GROUP BY IF (1 = `from`, `to`, `from`)
//    ) AS latest
// LEFT JOIN
//   personal_message USING(id)



 ?>