<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Offset_Costing_Model extends MY_Model
{
	const DB_TABLE = 'offset_costing';
    const DB_TABLE_PK = 'id';

    public $id;
    public $date;
    public $customer_id;
    public $item_name;
    public $moq;
    public $no_of_color;
    public $rate_per_color;
    public $pcs_per_sheet;
    public $sale_tax;
    public $show_both_amount;
    public $board_width;
    public $board_height;
    public $board_gsm;
    public $board_rate;
    public $no_unit_per_packet;
    public $wastage_percent;
    public $uv_width;
    public $uv_height;
    public $uv_rate;
    public $lamination_width;
    public $lamination_height;
    public $lamination_rate;
    public $foil_rate;
    public $both_side_lamination;
    public $sorting_rate;
    public $die_cutting_rate;
    public $embossing_rate;
    public $pasting_rate;
    public $packing_rate;
    public $pcs_per_carton;
    public $film_width;
    public $film_height;
    public $no_film_development;
    public $film_rate;
    public $no_plate;
    public $plate_rate;
    public $no_ups;
    public $ups_rate;
    public $proofing;
    public $blanket;
    public $block;
    public $development_foil;
    public $designing;
    public $cartage;
    public $pcs_per_delivery;
    public $margin;
    public $attachment;
    public $description;
    public $die_to_die_width;
    public $die_to_die_height;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

    public function get_printing_cost()
    {
        // return ($this->no_of_color * $this->rate_per_color) / $this->pcs_per_sheet / 1000; 
        return ($this->no_of_color * $this->rate_per_color) / $this->pcs_per_sheet;
    }

    public function get_board_weight()
    {
        return ($this->board_width * $this->board_height * $this->board_gsm) / 15500; 
    }

    public function get_board_packet_cost()
    {
        if ($this->sale_tax>0) {
            $rate = $this->board_rate/(100+$this->sale_tax)*100;
        }else{
            $rate = $this->board_rate;
        }
        return ( $rate * $this->get_board_weight() ); 
    }

    public function get_board_per_pcs_cost()
    {
        return ( $this->get_board_packet_cost() / $this->no_unit_per_packet ); 
    }

    public function get_wastage_percentage()
    {
        return ( $this->get_board_per_pcs_cost() * ($this->wastage_percent /100 ) ); 
    }

    public function get_total_board_cost()
    {
        return number_format( $this->get_board_per_pcs_cost() + ($this->wastage_percent /100 * $this->get_board_per_pcs_cost()),3 ); 
    }

    public function get_uv_rate_per_packet()
    {
        return ( ($this->uv_width * $this->uv_height) / 144 * $this->uv_rate ); 
    }

    public function get_uv_rate_per_pcs()
    {
        return ( $this->get_uv_rate_per_packet() / $this->pcs_per_sheet );
    }

    public function get_cartage_cost()
    {
        return ($this->pcs_per_delivery>0)?( $this->cartage / $this->pcs_per_delivery ):'';
    }

    public function get_lamination_rate_per_packet()
    {
        $number_of_lamination = ($this->both_side_lamination) ? 2 : 1;
        return ( $this->lamination_width * $this->lamination_height / 144 * $this->lamination_rate * $number_of_lamination ); 
    }

    public function get_lamination_rate_per_pcs()
    {
        return ( $this->get_lamination_rate_per_packet() / $this->pcs_per_sheet );
    }

    public function get_sorting_cost()
    {
        // return ( $this->sorting_rate / ($this->pcs_per_sheet*1000) );
        return ( $this->sorting_rate / ($this->pcs_per_sheet) );
    }

    public function get_die_cutting_cost()
    {
        // return ( $this->die_cutting_rate / ($this->pcs_per_sheet*1000) );
        return ( $this->die_cutting_rate / ($this->pcs_per_sheet) );
    }
    
    public function get_foil_cost()
    {
        $this->load->model('Offset_Costing_Foil_Model');
        $foils = $this->db->query("SELECT SUM(width*height) AS squire FROM `offset_costing_foil` WHERE costing_id = ".$this->id)->row();
        
        return ( $this->foil_rate * $foils->squire );
    }


    public function get_embossing_cost()
    {
        return ( $this->embossing_rate / ($this->pcs_per_sheet*1000) );
    }

    public function get_pasting_cost()
    {
        return ( $this->pasting_rate / 1000 );
    }

    public function get_packing_cost()
    {
        return ( $this->packing_rate / $this->pcs_per_carton );
    }

    public function get_film_cost()
    {
        return ( $this->film_width * $this->film_height * $this->film_rate * $this->no_film_development );
    }

    public function get_plates_cost()
    {
        return ( $this->no_plate * $this->plate_rate );
    }

    public function get_die_cost()
    {
        return ( $this->no_ups * $this->ups_rate );
    }

    public function get_margin_amount()
    {
        $c = 0;
        $c += $this->get_total_board_cost(); // board total cost
        $c += $this->get_printing_cost();
        $c += $this->get_sorting_cost();
        $c += $this->get_uv_rate_per_pcs();
        $c += $this->get_lamination_rate_per_pcs();
        $c += $this->get_foil_cost();
        $c += $this->get_die_cutting_cost();
        $c += $this->get_embossing_cost();
        $c += $this->get_packing_cost();
        $c += $this->get_cartage_cost();
        $c += $this->get_development_cost() / $this->moq;
        return $c * $this->margin/100;

    }

    public function get_total_cost_befor_margin()
    {
        $c = 0;
        $c += $this->get_total_board_cost(); // board total cost
        $c += $this->get_printing_cost();
        $c += $this->get_sorting_cost();
        $c += $this->get_uv_rate_per_pcs();
        $c += $this->get_lamination_rate_per_pcs();
        $c += $this->get_foil_cost();
        $c += $this->get_die_cutting_cost();
        $c += $this->get_embossing_cost();
        $c += $this->get_packing_cost();
        $c += $this->get_pasting_cost();
        $c += $this->get_cartage_cost();
        $c += $this->get_development_cost_per_pcs();
        return $c;

    }

    public function get_development_cost()
    {
        return ( $this->get_film_cost() + $this->get_plates_cost() + $this->get_die_cost() + $this->proofing + $this->block + $this->blanket + $this->development_foil + $this->designing );
    }

    public function get_development_cost_per_pcs()
    {
        return ( $this->get_development_cost() / $this->moq );
    }

    public function get_final_cost($with_tax=false)
    {
            return $this->get_total_cost_befor_margin() + $this->get_margin_amount();
    }

}

 ?>