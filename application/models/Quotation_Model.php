<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Quotation_Model extends MY_Model
{
	const DB_TABLE = 'quotation';
    const DB_TABLE_PK = 'id';

    public $id;
    public $gender;
    public $party;
    public $attention;
    public $date;
    public $product;
    public $structure;
    public $printing;
    public $gsm;
    public $quality;
    public $other_specification;
    public $finishing;
    public $price;
    public $bag_making;
    public $moq;
    public $department;
    public $person_name;
    public $designation;
    public $contact;
    public $condition_1;
    public $condition_2;
    public $condition_3;
    public $condition_4;
    public $condition_5;
    public $yield;
    public $cylinder_cost;
    public $size;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>