<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Vocancy_Model extends MY_Model
{
	const DB_TABLE = 'vocancy';
    const DB_TABLE_PK = 'id';

    public $id;
    public $start_date;
    public $end_date;
    public $no_position;
    public $position_name;
    public $description;
    public $department;
    public $position_register;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;



}

 ?>