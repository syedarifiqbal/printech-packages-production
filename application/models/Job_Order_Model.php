<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Job_Order_Model extends MY_Model
{
	const DB_TABLE = 'job_order';
    const DB_TABLE_PK = 'id';

    public $id;
    public $cd_date; # DATE
    public $po_num; # STRING
    public $po_date; # DATE
    public $del_date; # DATE
    public $job_code; # INT
    public $job_name; # STRING
    public $customer_id; # INT
    public $quantity; # DECIMAL
    public $print_type; # STRING
    public $print_as_per; # STRING
    public $cylinder; # STRING
    public $cylinder_len; # INT
    public $cylinder_circum; # INT
    public $no_ups; # INT
    public $no_repeat; # INT
    public $no_lamination; # INT

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>