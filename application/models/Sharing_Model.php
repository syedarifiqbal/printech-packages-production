<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Sharing_Model extends MY_Model
{
	const DB_TABLE = 'sharing';
    const DB_TABLE_PK = 'id';

    public $id;
    public $file_name;
    public $size;
    public $file_type;
    public $uploaded_by;
    public $uploaded_time;

    public function get_current_user_file()
    {
    	$res = $this->db->query("SELECT * FROM sharing WHERE uploaded_by = ".$this->session->userdata('user_id').' ORDER BY id DESC');
    	return $res->result();
    }

}

 ?>