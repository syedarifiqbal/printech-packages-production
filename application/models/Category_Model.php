<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Category_Model extends MY_Model
{
	const DB_TABLE = 'category';
    const DB_TABLE_PK = 'category_id';

    public $category_id;
    public $category_name;
    public $description;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;

 	public function __construct(){
 		parent::__construct();
 		$this->roles = array();
 	}


}

 ?>