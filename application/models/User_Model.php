<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class User_Model extends MY_Model
{
    const DB_TABLE = 'user';
    const DB_TABLE_PK = 'user_id';

    public $user_id;
    public $user_name;
    public $hashed_password;
    public $plain_password;
    public $role_id;
    public $avater_path;
    public $cell;
    public $active;
    public $address;
    public $added_by;
    public $added_time;
    public $updated_by;
    public $updated_time;

    public $roles;
    public static $permissions;

    public function __construct(){
        parent::__construct();
        $this->roles = array();
    }

    public function authenticate($user_name,$password)
    {
        $query = $this->db->get_where('user',['user_name'=>$user_name,'active'=>1]);
        $user = $query->first_row('array');

        if ( $user && password_verify($password,$user['hashed_password']) ) {
            return $user;
        }
        else
        {
            return FALSE;
        }
    }

    public function getRolePerms($user_id) // this method is deprecated by new update
    {
        $q = $this->db->query('SELECT u.user_id, ur.role_id, r.role_name from user AS u 
                                JOIN user_role AS ur ON ur.user_id = u.user_id
                                JOIN roles AS r ON r.role_id = ur.role_id
                                WHERE u.user_id='.$user_id);
        // return $q->result();
        foreach ( $q->result() AS $role) {
            $this->roles[$role->role_name] = true;
        }
    } // this method is deprecated by new update

    public static function hasAccess($value)
    {
        return in_array($value, self::$permissions);
    }

    public function getPermissions(){
        $sql = "SELECT `permission` FROM `permissions` AS p
                JOIN role_permission AS rp ON p.id = rp.perm_id
                WHERE rp.role_id = ".$this->session->userdata('role_id');
        $perms = $this->db->query($sql);
        if($perms){
            $perms = $perms->result();
            foreach ($perms as $perm) {
                self::$permissions[] = $perm->permission;
            }
        }else{
            return null;
        }

    }

    public function getName()
    {
        return ucfirst(str_replace('_',' ',$this->user_name));
    }



}

 ?>