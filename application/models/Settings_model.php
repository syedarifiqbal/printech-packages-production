<?php 

/**
* SYSTEM SETTINGS MODAL
*/
class Settings_model extends MY_Model
{
	const DB_TABLE = 'settings';
    const DB_TABLE_PK = 'id';

    public $id; # INT
    public $name; # VARCHAR
    public $title; # VARCHAR
    public $value = ''; # VARCHAR
    public $private = 0; # BOOLEAN

}

 ?>