<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class PO_Detail_Model extends MY_Model
{
	const DB_TABLE = 'po_detail';
    const DB_TABLE_PK = 'po_id';

    public $po_id;
    public $material_id;
    public $qty;
    public $rate;
    // public $defected;
    public $description;
    public $job_code;

}

 ?>