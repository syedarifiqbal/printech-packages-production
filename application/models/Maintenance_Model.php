<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Maintenance_Model extends MY_Model
{
	const DB_TABLE = 'maintenance';
    const DB_TABLE_PK = 'id';

    public $id;
    public $req_type;
    public $req_for;
    public $request_from;
    public $request_person;
    public $problem;
    public $suggestion;
    public $created_by;
    public $created_time;
    public $treatment;
    public $solution;
    public $treat_by;
    public $treat_time;
    public $treat_update;
    public $pending;
    public $start_time;
    public $end_time;
    public $solved_by;
    public $verified_incharge;
    public $verified_requester;

}

 ?>