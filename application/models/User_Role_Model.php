<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class User_Role_Model extends MY_Model
{
    const DB_TABLE = 'user_role';
    const DB_TABLE_PK = 'user_role_id';

    public $user_role_id;
    public $user_id;
    public $role_id;

    public $roles;

    public function __construct(){
        parent::__construct();
        $this->roles = array();
    }

}

 ?>