<?php 

/**
* ACCOUNT GROUPS MODAL
*/
class Rewinding_Model extends MY_Model
{
	const DB_TABLE = 'rewinding';
    const DB_TABLE_PK = 'id';

    public $id;
    public $date;
    public $so;
    public $operator;
    public $start_time;
    public $end_time;
    public $weight_before;
    public $weight_after;
    public $wastage;
    public $trim;
    public $qc_hold;
    public $direction;
    public $remarks;
    public $created_by;
    public $created_time;
    public $updated_by;
    public $updated_time;


    public function get_monthly_rewinding_report($from,$to,$machine,$job_code,$customer,$type='object')
    {
        $result = ($type == 'array')? 'result_array':'result';
        $machine = ($machine)?' AND p.machine = '.$machine:'';
        $job_code = ($job_code)?' AND p.so = '.$job_code:'';
        $customer = ($customer)?' AND so.customer_id = '.$customer:'';

        $query = "SELECT DATE_FORMAT(r.date,'%d/%m/%Y') AS `date`,
                    so.job_code AS code,
                    st.job_name AS `job name`,
                    m.material_name `Material Name`, 
                    null AS `direc.`,
                    SUM(r.weight_before) AS `B4 REW`,
                    SUM(r.weight_after) AS `AFT. REW`,
                    null AS hold,
                    (SUM(r.trim)) AS `TRIM`,
                    ROUND(SUM(r.trim)*100/r.weight_before,2) `TRIM %`,
                    (SUM(r.wastage)) AS `WAST`,
                    ROUND(SUM(r.wastage)*100/r.weight_before,2) `WAST %`
                FROM rewinding AS r
                INNER JOIN sale_order AS so ON r.so = so.job_code
                INNER JOIN job_master AS st ON st.id = so.structure_id
                INNER JOIN material AS m ON st.print_material = m.material_id
            WHERE r.date BETWEEN '$from' AND '$to' $machine $job_code $customer
                GROUP BY r.so
                ORDER BY r.date";
        $qr = $this->db->query($query);
        // echo "<pre>";
        // print_r($this->db->last_query());
        // echo "</pre>";

        // echo "<pre>";
        // print_r($qr->{$result}());
        // echo "</pre>";

        return $qr->{$result}();
    }



}

 ?>