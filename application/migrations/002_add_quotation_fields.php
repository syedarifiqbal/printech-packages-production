<?php 
/**
* HISTORY TABLE CREATION
*/
class Migration_add_quotation_fields extends CI_Migration
{
	
	function up()
	{
		$fields = array(
            'yield' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
            'cylinder_cost' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
            'size' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
            'condition_4' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
            'condition_5' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE),
        );
        $this->dbforge->add_column('quotation', $fields);
	}

	function down()
	{
		$this->dbforge->drop_column('quotation', 'yield');
		$this->dbforge->drop_column('quotation', 'cylinder_cost');
		$this->dbforge->drop_column('quotation', 'size');
		$this->dbforge->drop_column('quotation', 'condition_4');
		$this->dbforge->drop_column('quotation', 'condition_5');
	}
}

 ?>