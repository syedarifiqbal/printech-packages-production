<?php 
/**
* HISTORY TABLE CREATION
*/
class Migration_create_job_mapping extends CI_Migration
{
	
	function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type'=> 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'auto_increament' => TRUE
			),
			'job_name' => array(
				'type'=> 'VARCHAR',
				'constraint' => '255'
			),
			'customer_id' => array(
				'type'=> 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'colors' => array(
				'type'=> 'VARCHAR',
                'constraint' => '50',
                'null'=> TRUE
			),
			'length' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
			),
			'circum' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'file_no' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'ep_no' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'cylinder_no' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'vendor' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'receiving_date' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'film_type' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'film_size' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'up_size' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'col' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
            ),
			'remarks' => array(
				'type'=> 'TEXT',
            ),
			'added_by' => array(
				'type'=> 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'updated_by' => array(
				'type'=> 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'null' => TRUE,
			)
		));

		$this->dbforge->add_field('added_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
		$this->dbforge->add_field('updated_time TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT NULL');
		// $this->dbforge->add_key('id',TRUE);;
		$this->dbforge->create_table('job_mapping');
		echo "This history create table function.";
	}

	function down()
	{
		$this->dbforge->drop_table('job_mapping');
		echo "This history drop table function.";
	}
}

 ?>