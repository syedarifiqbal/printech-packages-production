<?php 
/**
* HISTORY TABLE CREATION
*/
class Migration_create_history extends CI_Migration
{
	
	function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type'=> 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
				'auto_increament' => TRUE
			),
			'context_id' => array(
				'type'=> 'INT',
				'constraint' => '11',
				'unsigned' => TRUE,
			),
			'context' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
			),
			'user_id' => array(
				'type'=> 'INT',
				'constraint' => '11',
			),
			'ip' => array(
				'type'=> 'VARCHAR',
				'constraint' => '100',
			),
			'description' => array(
				'type'=> 'text'
			)
		));

		$this->dbforge->add_field('timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');

		// $this->dbforge->add_key('id',TRUE);
		$this->dbforge->create_table('history');
		echo "This history create table function.";
	}

	// public function up() {
 //      $this->dbforge->add_field('id');
 //      $this->dbforge->add_field(array(
 //        'date' => array(
 //          'type' => 'DATE',
 //          'null' => FALSE,
 //        ),
 //        'location' => array(
 //          'type' => 'VARCHAR',
 //          'constraint' => '255',
 //          'null' => FALSE,
 //        ),
 //        'description' => array(
 //          'type' => 'TEXT',
 //          'null' => TRUE,
 //        ),
 //      ));
 //      $this->dbforge->create_table('shows');
 //    }

	function down()
	{
		$this->dbforge->drop_table('history');
		echo "This history drop table function.";
	}
}

 ?>