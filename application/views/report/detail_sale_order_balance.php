<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Sale Order Balance <i>Report</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Datewise Sale Order Balance</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body clearfix">  <!-- expandable BODY -->      

					<form target="_blank" action="<?php echo site_url('Report/detail_order_balance/') ?>" method="POST">
						
						<div class="col-md-offset-6 col-md-6 clearfix">
							<div id="daterange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
					          <i class="fa fa-calendar-o icon-calendar icon-large"></i>
					          <span></span> <b class="caret"></b>
					          <input type="hidden" id="from" name="from">
					          <input type="hidden" id="to" name="to">
					       </div>
						</div> <hr class="spacer"/>

						<div class="col-md-6">
						
							<hr class="spacer"/>
							<table id="listTable" class="table table-striped table-bordered table-hover datatable-col-4">
			                    <thead>
			                        <tr>
			                            <th>Job No.</th>
			                            <th>Job Name</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                        <?php foreach ($orders as $row): ?>
			                            <tr>
			                                <td><?php echo $row->job_code; ?></td>
			                                <td>
			                                	<!-- <label class="radio-label" for="<?php echo $row->job_code; ?>">
			                                	<?php echo html_escape($row->job_name); ?>
			                                	</label> -->

			                                	<label class="radio-label">
												    <input type="radio" value="<?php echo $row->job_code; ?>" name="job_code">
												    <div class="radio-button"></div>
												    <?php echo html_escape($row->job_name); ?>
												</label>
			                                </td>
			                            </tr>
			                        <?php endforeach ?>
			                    </tbody>
			                </table>

						</div>

						<div class="col-md-6">
						
							<hr class="spacer"/>
							<table id="listTable" class="table table-striped table-bordered table-hover datatable-col-4_2">
			                    <thead>
			                        <tr>
			                            <th>Customer No.</th>
			                            <th>Customer Name</th>
			                        </tr>
			                    </thead>
			                    <tbody>
			                        <?php foreach ($customers as $row): ?>
			                            <tr>
			                                <td><?php echo $row->customer_id; ?></td>
			                                <td>
			                                	<!-- <label class="radio-label" for="<?php echo $row->job_code; ?>">
			                                	<?php echo html_escape($row->job_name); ?>
			                                	</label> -->

			                                	<label class="radio-label">
												    <input type="radio" value="<?php echo $row->customer_id; ?>" name="customer_id">
												    <div class="radio-button"></div>
												    <?php echo html_escape($row->customer_name); ?>
												</label>
			                                </td>
			                            </tr>
			                        <?php endforeach ?>
			                    </tbody>
			                </table>

						</div>


						<div class="col-md-4 col-md-offset-4">
							<button type="submit" name="report" value="submit" class="btn btn-success btn-block">View</button>
						</div><hr class="spacer"/><hr class="spacer"/>

					</form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>

	$('#daterange').daterangepicker(
	{
		startDate: moment().subtract('days', 29),
		endDate: moment(),
		minDate: moment().subtract('years', 1),
		maxDate: moment(),
		dateLimit: { days: 60 },
		showDropdowns: true,
		showWeekNumbers: true,
		timePicker: false,
		timePickerIncrement: 1,
		timePicker12Hour: true,
		ranges: {
		   'Today': [moment(), moment()],
		   'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
		   'Last 7 Days': [moment().subtract('days', 6), moment()],
		   'Last 30 Days': [moment().subtract('days', 29), moment()],
		   'This Month': [moment().startOf('month'), moment().endOf('month')],
		   'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		},
		opens: 'left',
		buttonClasses: ['btn btn-default'],
		applyClass: 'btn-small btn-primary',
		cancelClass: 'btn-small',
		format: 'DD/MM/YYYY',
		separator: ' to ',
		locale: {
			applyLabel: 'Submit',
			fromLabel: 'From',
			toLabel: 'To',
			customRangeLabel: 'Custom Range',
			daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
			monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			firstDay: 1
			}
		},
		function(start, end) {
			$('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			$('#from').val(start.format('YYYY-MM-DD'));
  			$('#to').val(end.format('YYYY-MM-DD'));
		}
  	);

  //Set the initial state of the picker label
  $('#daterange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
  $('#from').val(moment().subtract('days',29).format('YYYY-MM-DD'));
  $('#to').val(moment().format('YYYY-MM-DD'));



    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-4, .datatable-col-4_2').dataTable({
        // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
        //         "t"+
        //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
        "autoWidth" : false,
        "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
        "aoColumns": [
            { "sWidth": "20%" },
            { "sWidth": "80%" },
        ],
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_datatable_col_reorder) {
                responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_datatable_col_reorder.respond();
        }
    });


</script>