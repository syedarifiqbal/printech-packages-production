<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Production <i>Report</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Production Report</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body clearfix">  <!-- expandable BODY -->      

					<form target="_blank" action="<?php echo site_url('Report/production_report/') ?>" method="POST">
						
						<div class="col-md-6">
							<div id="date">
						        <input type="hidden" id="from" name="selected_date">
					       	</div><hr class="spacer clearfix"/>
						</div>  <hr class="spacer"/>

						<div class="col-md-4 col-md-offset-4">  <hr class="spacer"/>
							<div class="btn-group">
								<!-- <button type="submit" name="excel" value="excel" class="btn btn-success">Save to File</button> -->
								<button type="submit" name="PDF" value="PDF" class="btn btn-success">View</button>
							</div>
						</div><hr class="spacer"/><hr class="spacer"/>

					</form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>

	$('#date').datepicker(
	{
        inline: true,
		format: 'DD/MM/YYYY',
        onSelect: function(dateText, inst) { 
            var dateAsString = dateText; //the first parameter of this function
            var d = $(this).datepicker( 'getDate' ); //the getDate method
            var m = (d.getMonth()+1);
            var dd = d.getDate();
            var date = (d.getFullYear()+'-'+((m<10)?'0'+m:m)+'-'+((dd<10)?'0'+dd:dd));
            $('[name=selected_date]').val(date);
         }
	});
    
//    $('#date').on('dp.change', function(event) {
//        $('#selected-date').text(event.date);
//        var formatted_date = event.date.format('YYYY-MM-DD');
//        $('[name=selected_date]').val(formatted_date);
//    });
    
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };

    $('.datatable-col-4, .datatable-col-4_2').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "aoColumns": [
                    { "sWidth": "20%" },
                    { "sWidth": "80%" },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });


</script>