<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>WASTAGE <i>Report</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Datewise</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body clearfix">  <!-- expandable BODY -->      

					<form target="_blank" action="<?php echo site_url('Report/wastage_report/') ?>" method="POST">
						
						<div class="col-md-6">
						</div> <hr class="spacer"/>

						<div class="col-md-6">
							<div id="daterange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
						        <i class="fa fa-calendar-o icon-calendar icon-large"></i>
						        <span></span> <b class="caret"></b>
						        <input type="hidden" id="from" name="from">
						        <input type="hidden" id="to" name="to">
					       	</div><hr class="spacer clearfix"/>
							<select name="machine" class="form-control">
								<option value="">Choose Machine</option>
								<option value="1">Machine 01</option>
								<option value="2">Machine 02</option>
								<option value="3">Machine 03</option>
							</select>
						</div> <hr class="spacer"/>

						<div class="col-md-4 col-md-offset-4">
							<div class="btn-group">
								<button type="submit" name="excel" value="excel" class="btn btn-success">Save to File</button>
								<button type="submit" name="PDF" value="PDF" class="btn btn-success">View</button>
							</div>

							<!-- <button type="submit" name="excel" value="excel" class="btn btn-success btn-block">Save to File</button>
							<button type="submit" name="PDF" value="PDF" class="btn btn-success btn-block">PDF</button> -->
						</div><hr class="spacer"/><hr class="spacer"/>

					</form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>

	$('#daterange').daterangepicker(
	{
		startDate: moment().subtract('days', 29),
		endDate: moment(),
		minDate: moment().subtract('years', 1),
		maxDate: moment(),
		dateLimit: { days: 60 },
		showDropdowns: true,
		showWeekNumbers: true,
		timePicker: false,
		timePickerIncrement: 1,
		timePicker12Hour: true,
		ranges: {
		   'Today': [moment(), moment()],
		   'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
		   'Last 7 Days': [moment().subtract('days', 6), moment()],
		   'Last 30 Days': [moment().subtract('days', 29), moment()],
		   'This Month': [moment().startOf('month'), moment().endOf('month')],
		   'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		},
		opens: 'left',
		buttonClasses: ['btn btn-default'],
		applyClass: 'btn-small btn-primary',
		cancelClass: 'btn-small',
		format: 'DD/MM/YYYY',
		separator: ' to ',
		locale: {
			applyLabel: 'Submit',
			fromLabel: 'From',
			toLabel: 'To',
			customRangeLabel: 'Custom Range',
			daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
			monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			firstDay: 1
			}
		},
		function(start, end) {
			$('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			$('#from').val(start.format('YYYY-MM-DD'));
  			$('#to').val(end.format('YYYY-MM-DD'));
		}
  	);

  //Set the initial state of the picker label
  $('#daterange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
  $('#from').val(moment().subtract('days',29).format('YYYY-MM-DD'));
  $('#to').val(moment().format('YYYY-MM-DD'));



    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-4').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "aoColumns": [
                    { "sWidth": "20%" },
                    { "sWidth": "80%" },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });


</script>