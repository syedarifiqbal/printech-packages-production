<?php

  $role = (isset($update) && $update==true)? 'editRotoJobStructure':'addRotoJobStructure';
  $pageTitle = (isset($update) && $update==true)? 'Update ':'New ';
  $job_code = (isset($update) && $update==true)? $structure->id:$job_code;
  $file_number = (isset($update) && $update==true)? $structure->file_number:'';
  $job_name = (isset($update) && $update==true)? $structure->job_name:'';
  $print_type = (isset($update) && $update==true)? $structure->print_type:'';
  $print_as_per = (isset($update) && $update==true)? $structure->print_as_per:'';
  $cylinder = (isset($update) && $update==true)? $structure->cylinder:'';
  $cylinder_len = (isset($update) && $update==true && $structure->cylinder_len!=0)? $structure->cylinder_len:'';
  $cylinder_circum = (isset($update) && $update==true && $structure->cylinder_circum!=0)? $structure->cylinder_circum:'';
  $no_ups = (isset($update) && $update==true && $structure->no_ups!=0)? $structure->no_ups:'';
  $no_repeat = (isset($update) && $update==true && $structure->no_repeat!=0)? $structure->no_repeat:'';
  $no_lamination = (isset($update) && $update==true)? $structure->no_lamination:'';
  $print_material = (isset($update) && $update==true)? $structure->print_material:'';
  $lam1_material = (isset($update) && $update==true)? $structure->lam1_material:'';
  $lam2_material = (isset($update) && $update==true)? $structure->lam2_material:'';
  $lam3_material = (isset($update) && $update==true)? $structure->lam3_material:'';
  $slitting_reel_width = (isset($update) && $update==true && $structure->slitting_reel_width!=0)? $structure->slitting_reel_width:'';
  $slitting_cutting_size = (isset($update) && $update==true && $structure->slitting_cutting_size!=0)? $structure->slitting_cutting_size:'';
  $slitting_direction = (isset($update) && $update==true)? $structure->slitting_direction:'';
  $ink_series = (isset($update) && $update==true)? $structure->ink_series:'';
  $no_of_inks = (isset($update) && $update==true && $structure->no_of_inks!=0)? $structure->no_of_inks:'';
  $bm_qty = (isset($update) && $update==true && $structure->bm_qty!=0)? $structure->bm_qty:'';
  $bm_types = (isset($update) && $update==true)? $structure->bm_types:'';
  $bm_width = (isset($update) && $update==true)? $structure->bm_width:'';
  $bm_height = (isset($update) && $update==true)? $structure->bm_height:'';
  $coa_requird = (isset($update) && $update==true)? $structure->coa_requird:'';
  $dc = (isset($update) && $update==true)? $structure->dc:'';
  $hold = (isset($update) && $update==true)? $structure->hold:'';
  $remarks = (isset($update) && $update==true)? $structure->remarks:'';
  $reason = (isset($update) && $update==true)? $structure->reason:'';
  $url = site_url('');
  $actionLink = (isset($update) && $update==true) ? $url.'/Update/job_structure/'.$structure->id: $url.'/Add/job_structure';

 if (User_Model::hasAccess($role)):

?>

<?php echo $feed_back ?>
<!-- Tables -->
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>Job Structure</i></h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" role="form" action="<?php echo $actionLink; ?>" method="POST">
                        <fieldset>
                            <legend class="text-center header">General</legend>

                            <div class="row">

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                        <input type="text" name="job_code" readonly value="<?php echo $job_code; ?>" autocomplete="off" class="form-control is_num required" placeholder="JOB NUMBER">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-file-o"></i></span>
                                        <input type="text" name="file_number" value="<?php echo $file_number; ?>" autocomplete="off" class="form-control is_num required" placeholder="FILE NUMBER">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="job_name" value="<?php echo $job_name; ?>" autocomplete="off" class="form-control is_num required" placeholder="JOB NAME">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12"> <!-- PRINT TYPE -->
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon">Type:</span>
                                        <select class="form-control required" name="print_type">
                                          <option <?php echo $print_type=='Top'? 'selected':''; ?>>Top</option>
                                          <option <?php echo $print_type=='Reverse'? 'selected':''; ?>>Reverse</option>
                                          <option <?php echo $print_type=='Unprinted'? 'selected':''; ?>>Unprinted</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div> <!-- /PRINT TYPE -->
                                
                                <div class="col-sm-6 col-xs-12"> <!-- PRINT AS PER -->
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon">As Per</span>
                                        <select class="form-control" name="print_as_per">
                                            <option <?php echo $print_as_per=='Party Approval'? 'selected':''; ?>>Party Approval</option>
                                            <option <?php echo $print_as_per=='Amendment'? 'selected':''; ?>>Amendment</option>
                                            <option <?php echo $print_as_per=='Approved Sample'? 'selected':''; ?>>Approved Sample</option>
                                            <option <?php echo $print_as_per=='Last Production'? 'selected':''; ?>>Last Production</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div> <!-- /PRINT AS PER -->

                                
                                <div class="col-sm-6 col-xs-12"> <!-- CYLINDER -->
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon">Cylinder:</span>
                                        <select class="form-control" name="cylinder">
                                          <option <?php echo $cylinder=='Old Cylinders'? 'selected':''; ?>>Old Cylinders</option>
                                          <option <?php echo $cylinder=='New Cylinders'? 'selected':''; ?>>New Cylinders</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div> <!-- CYLINDER -->

                                <div class="col-sm-6 col-xs-12"> <!-- CYLINDER LENGTH -->
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                      <span class="input-group-addon">Length:</span>
                                        <input type="text" name="cylinder_len" value="<?php echo $cylinder_len; ?>" autocomplete="off" class="form-control is_num" placeholder="CYLINDER LENGTH">
                                    </div><hr class="spacer"/>
                                </div> <!-- CYLINDER LENGTH -->

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm"> <!-- CYLINDER CIRCUM -->
                                        <span class="input-group-addon">Circum:</span>
                                        <input type="text" name="cylinder_circum" value="<?php echo $cylinder_circum; ?>" autocomplete="off" class="form-control is_num" id="job_Name" placeholder="CYLINDER CIRCUM">
                                    </div><hr class="spacer"/>
                                </div> <!-- CYLINDER CIRCUM -->

                                <div class="col-sm-6 col-xs-12"> <!-- NO. OF UPS -->
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon">Ups #:</span>
                                        <input type="text" name="no_ups" value="<?php echo $no_ups; ?>" autocomplete="off" class="form-control is_num" id="customer_Name" placeholder="NUMBER OF UPS">
                                    </div><hr class="spacer"/>
                                </div> <!-- NO. OF UPS -->

                                <div class="col-sm-6 col-xs-12"> <!-- NO. REPEAT -->
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon">Repeat #:</span>
                                        <input type="text" name="no_repeat" value="<?php echo $no_repeat; ?>" autocomplete="off" class="form-control is_num" id="customer_Name" placeholder="NUMBER OF REPEAT">
                                    </div><hr class="spacer"/>
                                </div> <!-- NO. REPEAT -->

                                <div class="col-sm-6 col-xs-12"> <!-- NO. OF LAMINATION -->
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Lamination #:</span>
                                        <select class="form-control" id="noLamination" name="no_lamination">
                                            <option <?php echo $no_lamination=='null'? 'selected':''; ?> value="null">Select Lamination</option>
                                            <option <?php echo $no_lamination=='1'? 'selected':''; ?> value="1">1</option>
                                            <option <?php echo $no_lamination=='2'? 'selected':''; ?> value="2">2</option>
                                            <option <?php echo $no_lamination=='3'? 'selected':''; ?> value="3">3</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div> <!-- NO. OF LAMINATION -->

                            </div>

                        </fieldset>

                        <fieldset> <!-- MATERIAL FIELDSET -->
                            <legend class="text-center header">Materials</legend>

                            <div class="row">

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><i class="fa fa-paint-brush"></i></span>
                                        <input type="hidden" value="<?php echo $print_material; ?>" name="print_material"/>
                                        <input type="text" class="form-control autocomplete required" value="<?php echo $print_material_name; ?>" placeholder="PRINTING SUBSTRATE" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><i class="fa fa-paperclip"></i></span>
                                        <input type="hidden" value="<?php echo $lam1_material; ?>" name="lam1_material"/>
                                        <input type="text" class="form-control autocomplete " value="<?php echo $lam1_material_name; ?>" placeholder="1st LAMINATION SUBSTRATE" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><i class="fa fa-paperclip"></i></span>
                                        <input type="hidden" value="<?php echo $lam2_material; ?>" name="lam2_material"/>
                                        <input type="text" class="form-control autocomplete " value="<?php echo $lam2_material_name; ?>" placeholder="2nd LAMINATION SUBSTRATE" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon"><i class="fa fa-paperclip"></i></span>
                                        <input type="hidden" value="<?php echo $lam3_material; ?>" name="lam3_material"/>
                                        <input type="text" class="form-control autocomplete " value="<?php echo $lam3_material_name; ?>" placeholder="3rd LAMINATION SUBSTRATE" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
                                    </div><hr class="spacer"/>
                                </div>

                            </div>

                        </fieldset> <!-- /MATERIAL FIELDSET -->

                        <fieldset> <!-- SLITTING FIELDSET -->

                            <legend class="text-center header">Slitting</legend>
                            
                            <div class="row">
        
                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm"> <!-- SLITTING WIDTH -->
                                        <span class="input-group-addon">Width:</span>
                                            <input type="text" name="slitting_reel_width" value="<?php echo $slitting_reel_width; ?>" autocomplete="off" class="form-control is_num" placeholder="REAL WIDTH">
                                        <span class="input-group-addon">MM</span>
                                    </div><hr class="spacer"/>
                                </div> <!-- SLITTING WIDTH -->

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm"> <!-- CUT OF LENGTH -->
                                        <span class="input-group-addon">Cutting:</span>
                                        <input type="text" name="slitting_cutting_size" value="<?php echo $slitting_cutting_size; ?>" autocomplete="off" class="form-control is_num" placeholder="CUT OF LENGTH">
                                        <span class="input-group-addon">MM</span>
                                    </div><hr class="spacer"/>
                                </div> <!-- CUT OF LENGTH -->

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm"> <!-- DIRECTION -->
                                        <span class="input-group-addon">Direction:</span>
                                        <select class="form-control required" name="slitting_direction">
                                            <option <?php echo $slitting_direction=='Left Hand Side'? 'selected':''; ?>>Left Hand Side</option>
                                            <option <?php echo $slitting_direction=='Right Hand Side'? 'selected':''; ?>>Right Hand Side</option>
                                            <option <?php echo $slitting_direction=='Print Straight'? 'selected':''; ?>>Print Straight</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div> <!-- DIRECTION -->

                            </div>

                        </fieldset> <!-- /SLITTING FIELDSET -->

                        <fieldset> <!-- RECOMMENDED INKS FIELDSET -->
                            
                            <legend class="text-center header">Recommended INKs</legend>

                            <div class="row">

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm"> <!-- INK SERIES -->
                                        <span class="input-group-addon">INK Series:</span>
                                        <select class="form-control required" name="ink_series">
                                            <option <?php echo $ink_series=='Cynex'? 'selected':''; ?>>Cynex</option>
                                            <option <?php echo $ink_series=='Flash'? 'selected':''; ?>>Flash</option>
                                            <option <?php echo $ink_series=='S-Lam'? 'selected':''; ?>>S-Lam</option>
                                            <option <?php echo $ink_series=='GFR'? 'selected':''; ?>>GFR</option>
                                            <option <?php echo $ink_series=='Pinacl'? 'selected':''; ?>>Pinacl</option>
                                            <option <?php echo $ink_series=='SW-1'? 'selected':''; ?>>SW-1</option>
                                            <option <?php echo $ink_series=='NTS'? 'selected':''; ?>>NTS</option>
                                            <option <?php echo $ink_series=='GMR'? 'selected':''; ?>>GMR</option>
                                            <option <?php echo $ink_series=='096 (Dic)'? 'selected':''; ?>>096 (Dic)</option>
                                            <option <?php echo $ink_series=='GR-8'? 'selected':''; ?>>GR-8</option>
                                            <option <?php echo $ink_series=='LAM'? 'selected':''; ?>>LAM</option>
                                            <option <?php echo $ink_series=='EGL'? 'selected':''; ?>>EGL</option>
                                            <option <?php echo $ink_series=='Concentrates'? 'selected':''; ?>>Concentrates</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm"> <!-- # OF INK -->
                                        <span class="input-group-addon">INKs:</span>
                                        <input type="text" name="no_of_inks" value="<?php echo $no_of_inks; ?>" autocomplete="off" class="form-control is_num" placeholder="Number Of INKS">
                                    </div>
                                </div>

                            </div> <!-- ROW -->

                        </fieldset> <!-- /RECOMMENDED INKS FIELDSET -->

                        <fieldset> <!-- BAG MAKING FIELDSET -->
                            
                            <legend class="text-center header">BAG MAKING</legend>

                            <div class="row">

                                <!-- <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Quantity:</span>
                                        <input type="text" name="bm_qty" value="<?php echo $bm_qty; ?>" autocomplete="off" class="form-control is_num" placeholder="ORDER QUANTITY">
                                        <span class="input-group-addon">PCS</span>
                                    </div><hr class="spacer"/>
                                </div> -->

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Width:</span>
                                        <input type="text" name="bm_width" value="<?php echo $bm_width; ?>" autocomplete="off" class="form-control" id="job_Name" placeholder="WIDTH">
                                        <span class="input-group-addon">MM</span>
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Height:</span>
                                        <input type="text" name="bm_height" value="<?php echo $bm_height; ?>" autocomplete="off" class="form-control" id="job_Name" placeholder="HEIGHT">
                                        <span class="input-group-addon">MM</span>
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12"> <!-- BAG TYPE -->
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Type:</span>
                                        <select class="form-control" name="bm_type">
                                          <option <?php echo $bm_types=='Center Seal'? 'selected':''; ?>>Center Seal</option>
                                          <option <?php echo $bm_types=='Center Seal + Gusset'? 'selected':''; ?>>Center Seal + Gusset</option>
                                          <option <?php echo $bm_types=='Bottom Seal'? 'selected':''; ?>>Bottom Seal</option>
                                          <option <?php echo $bm_types=='3 Side Seal'? 'selected':''; ?>>3 Side Seal</option>
                                          <option <?php echo $bm_types=='2 Side Seal'? 'selected':''; ?>>2 Side Seal</option>
                                          <option <?php echo $bm_types=='Stand Up Pouch'? 'selected':''; ?>>Stand Up Pouch</option>
                                          <option <?php echo $bm_types=='Stand Up Pouch + Zipper'? 'selected':''; ?>>Stand Up Pouch + Zipper</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div> <!-- BAG TYPE -->

                            </div> <!-- ROW -->

                        </fieldset> <!-- /BAG MAKIN FIELDSET -->

                        <fieldset> <!-- DISPATCH FIELDSET -->
                            
                            <legend class="text-center header">DISPATCH</legend>

                            <div class="row">

                                <div class="col-sm-6 col-xs-12"> <!-- BAG TYPE -->
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">C.O.A:</span>
                                        <select class="form-control" name="dc">
                                          <option <?php echo $coa_requird=="yes"?'selected':''; ?>>Yes</option>
                                          <option <?php echo $coa_requird=="no"?'selected':''; ?>>No</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12"> <!-- BAG TYPE -->
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Delivery Challan:</span>
                                        <select class="form-control" name="dc">
                                          <option <?php echo $dc=="Non Official"?'selected':''; ?>>Non Official</option>
                                          <option <?php echo $dc=="Official"?'selected':''; ?>>Official</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div> <!-- BAG TYPE -->

                                <div class="col-xs-12"> <!-- REMARKS -->
                                    <div class="input-group input-group-sm">
                                        <span class="input-group-addon">Remarks:</span>
                                        <input type="text" name="remarks" value="<?php echo $remarks; ?>" autocomplete="off" class="form-control" placeholder="Remarks">
                                    </div><hr class="spacer"/>
                                </div> <!-- BAG TYPE -->

                            </div> <!-- ROW -->

                        </fieldset> <!-- /DISPATCH FIELDSET -->

                        <div class="row">
                            <div class="col-md-12 text-center"><br>
                                <div class="btn-group">
                                    <button type="reset" class="btn btn-danger btn-sm">Reset</button>
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm">Save</button>
                                </div>
                                    <br><br>
                            </div>
                        </div>

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>
    
    $('.autocomplete').each(function(i, el){
        var el = $(el),
            url = el.data('method');

        el.autocomplete({
        
            source: function( request, response ) {
                //console.log(el.data('method'));

                jQuery.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: { term: request.term },
                    success: function(data) {
                        var parsed = JSON.parse(data);
                        var newArray = new Array();

                        parsed.forEach(function (entry) {
                            var newObject = {
                                id: entry.id,
                                label: entry.label
                            };
                            newArray.push(newObject);
                        });

                        response(newArray);

                    },

                    error: function(xhr, ajaxOptions, thrownError) {
                        console.log(url);
                        container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>');
                    },
                    async: false
                });

            },

            minLength: 1,

            select: function( event, ui ) {
                $(this).prev().prev().val(ui.item.id);
             },

            focus: function (event, ui) {
                $(this).prev().prev().val(ui.item.id);
                $(this).val(ui.item.label);
            }

        });
    });

</script>
<?php 
else:
    echo not_permitted();
endif; 
?>