<?php

$url = site_url('Add/sticker_print/');

if (User_Model::hasAccess('addSticker')):
    ?>

    <?php echo $feed_back ?>

    <div class="container"> <!-- this is from here -->
        <div class="row">
            <div class="col-md-10">
                <div class="heading-sec" id="intro6">
                    <h1>Sticker <i>Print</i></h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <form id="formToSubmits" method="post" action="<?php echo $url; ?>">
                <div class="col-md-6 col-md-offset-2">
                    <div class="widget-body custom-form">

                        <div class="form-group"><!-- PRODUCT NAME -->
                            <label for="date">Date</label>
                            <input type="text" name="date" class="form-control required" id="date"
                                   value="<?php echo date('d/m/Y') ?>" placeholder="DATE"/>
                        </div><!-- PRODUCT NAME -->

                        <div class="form-group">
                            <label for="machine">Machine</label>
                            <?php echo form_dropdown('machine_no', ['One'=>1,'Two'=>2,'Three'=>3,'Four'=>4,'Five'=>5,'Six'=>6], '', 'class="form-control" id="machine"') ?>
                        </div>

                        <div class="form-group"><!-- Job Number -->
                            <label for="job_number">Job Number</label>
                            <input type="text" name="job_number" class="form-control required" id="job_number"
                                    placeholder="JOB NUMBER"/>
                        </div><!-- Job Number -->

                        <div class="form-group"><!-- Roll # -->
                            <label for="roll">Number of Rolls</label>
                            <input type="text" name="no_rolls" class="form-control required" id="roll"
                                   placeholder="NUMBER OF ROLLS"/>
                        </div><!-- Roll # -->

                        <div class="form-group"><!-- WIDTH -->
                            <label for="no_set">Set No</label>
                            <input type="text" name="number_of_set" class="form-control required" id="no_set"
                                   placeholder="SET NUMBER"/>
                        </div><!-- GSM -->

                        <div class="form-group"><!-- HEIGHT -->
                            <label for="operator">Operator</label>
                            <input type="text" name="operator" class="form-control required" id="operator"
                                   placeholder="OPERATOR"/>
                        </div><!-- HEIGHT -->


                        <button type="submit" name="submit" value="submit" class="btn green btn-primary">Save</button>

                    </div>
                </div>
            </form>
        </div>
    </div>


    <?php
else:
    echo not_permitted();
endif;
?>