<?php
 $role = (isset($update) && $update==true)? 'editRotoRewindingEntry':'addRotoRewindingEntry';
 $pageTitle = (isset($update) && $update==true)? 'Update':'New';
 $id = (isset($update) && $update==true)? $rewinding->id:'';
 $date = (isset($update) && $update==true)? PKdate($rewinding->date):'';
 $so = (isset($update) && $update==true)? $rewinding->so:'';
 $job_name = (isset($update) && $update==true)? $rewinding->job_name:'Choose Job';
 $operator = (isset($update) && $update==true)? $rewinding->operator:'';
 $start_time = (isset($update) && $update==true)? $rewinding->start_time:'';
 $end_time = (isset($update) && $update==true)? $rewinding->end_time:'';
 $weight_before = (isset($update) && $update==true)? $rewinding->weight_before:'';
 $weight_after = (isset($update) && $update==true)? $rewinding->weight_after:'';
 $wastage = (isset($update) && $update==true)? $rewinding->wastage:'';
 $trim = (isset($update) && $update==true)? $rewinding->trim:'';
 $qc_hold = (isset($update) && $update==true)? $rewinding->qc_hold:'';
 $direction = (isset($update) && $update==true)? $rewinding->direction:'';
 $remarks = (isset($update) && $update==true)? $rewinding->remarks:'';
 $url = site_url();
 $actionLink = (isset($update) && $update==true) ? $url.'/Update/rewinding_entry/'.$rewinding->id:$url.'/Add/rewinding_entry';

 if (User_Model::hasAccess($role)):
    echo $feed_back;
?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>Rewinding Entry</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">

                            <div class="col-sm-6 col-md-6"> <!-- DATE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Date:</div>
                                    <input name="date" type="text" readonly data-date="true" placeholder="DATE" class="form-control required" value="<?php echo $date; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-6 col-md-6"> <!-- CHOOSE JOB -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Choose Job:</span>
                                    <input type="hidden" value="<?php echo $so; ?>" name="so"/>
                                    <p class="findMaterial selfvalidate" title="CHOOSE JOB" data-balance="<?php echo site_url('Balance/rewinding/'); ?>" data-method="<?php echo site_url("autocomplete/findJob/"); ?>"><?php echo $job_name ?></p>
                                </div><hr class="spacer"/>
                            </div> <!-- /CHOOSE JOB -->

                            <div class="col-sm-6 col-md-6"> <!-- OPERATOR NAMER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Operator Name:</div>
                                    <input name="operator" title="OPERATOR NAME" type="text" placeholder="OPERATOR NAME" class="form-control required" value="<?php echo $operator; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /OPERATOR NAMER -->

                            <div class="col-sm-6 col-md-6"> <!-- START TIME -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Start Time:</div>
                                    <input name="start_time" title="START TIME" type="text" placeholder="START TIME" class="form-control required dateTime" value="<?php echo $start_time; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /START TIME -->

                            <div class="col-sm-6 col-md-6"> <!-- END TIME -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">End Time:</div>
                                    <input name="end_time" title="STOP TIME" type="text" placeholder="STOP TIME" class="form-control required dateTime" value="<?php echo $end_time; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /END TIME -->

                            <div class="col-sm-6 col-md-6"> <!-- WEIGHT BEFORE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Weight Before:</div>
                                    <input name="weight_before" title="WEIGHT BEFORE" data-type="decimal" type="text" placeholder="Weight Before" class="form-control required" value="<?php echo $weight_before; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /WEIGHT BEFORE -->

                            <div class="col-sm-6 col-md-6"> <!-- WEIGHT AFTER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Weight After:</div>
                                    <input name="weight_after" title="WEIGHT AFTER" data-type="decimal" type="text" placeholder="WEIGHT AFTER" class="form-control required" value="<?php echo $weight_after; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /WEIGHT AFTER -->

                            <div class="col-sm-6 col-md-6"> <!-- WASTAGE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Wastage:</div>
                                    <input name="wastage" title="WASTAGE" data-type="decimal" type="text" placeholder="WASTAGE" class="form-control required" value="<?php echo $wastage; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /WASTAGE -->

                            <div class="col-sm-6 col-md-6"> <!-- QC HOLD -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">QC Hold</div>
                                    <input name="qc_hold" title="QC HOLD" type="text" placeholder="QC HOLD" class="form-control" value="<?php echo $qc_hold; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /QC HOLD -->

                            <div class="col-sm-12"> <!-- REMARKS -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Remarks:</div>
                                    <input title="Remarks if any" name="remarks" type="text" placeholder="REMARKS" class="form-control" value="<?php echo $remarks; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /REMARKS -->

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    

    #inputTable tr th:nth-child(3){
        width: 50px;
    }
    #inputTable tr td:nth-child(3){
        padding: 0px 0 0 9px;
        margin-bottom: -5px;
    }
    #inputTable #fancy-checkbox + label {
        margin-top: 1px;
        margin-bottom: -3px;
    }
    #inputTable tr th:nth-child(1),
    #inputTable tr th:nth-child(2),
    #inputTable tr th:nth-child(3),
    #inputTable tr th:nth-child(4),
    #inputTable tr th:nth-child(5) {
        width: 150px;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });


</script>



<?php 
else:
    echo not_permitted();
endif; 
?>