<?php
 $role = (isset($update) && $update==true)? 'editRotoPrintingEntry':'addRotoPrintingEntry';
 $pageTitle = (isset($update) && $update==true)? 'Update':'New';
 $printing_id = (isset($update) && $update==true)? $print->printing_id:'';
 $so = (isset($update) && $update==true)? $print->so:'';
 $job_name = (isset($update) && $update==true)? $print->job_name:'Choose Job';
 $date = (isset($update) && $update==true)? PKdate($print->date):'';
 $machine = (isset($update) && $update==true)? $print->machine:'';
 $operator = (isset($update) && $update==true)? $print->operator:'';
 $assistant = (isset($update) && $update==true)? $print->assistant:'';
 $helper = (isset($update) && $update==true)? $print->helper:'';
 $production_meter = (isset($update) && $update==true)? $print->production_meter:'';
 $plain_wastage = (isset($update) && $update==true)? $print->plain_wastage:'';
 $printed_wastage = (isset($update) && $update==true)? $print->printed_wastage:'';
 $setting_wastage = (isset($update) && $update==true)? $print->setting_wastage:'';
 $matching_wastage = (isset($update) && $update==true)? $print->matching_wastage:'';
 $qc_hold = (isset($update) && $update==true)? $print->qc_hold:'';
 $machine_start_time = (isset($update) && $update==true)? $print->start_time:'';
 $machine_stop_time = (isset($update) && $update==true)? $print->stop_time:'';

 $url = site_url();
 $actionLink = (isset($update) && $update==true) ? $url.'/Update/printing_entry/'.$print->printing_id:$url.'/Add/printing_entry';

 if (User_Model::hasAccess($role)):
 echo $feed_back; ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>Printing Entry</i></h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">

                            <div class="col-sm-6 col-md-6"> <!-- DATE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Date</div>
                                    <input name="date" type="text" readonly data-date="true" placeholder="DATE" class="form-control required" value="<?php echo $date; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-6 col-md-6"> <!-- CHOOSE JOB -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Choose Job</span>
                                    <input type="hidden" value="<?php echo $so; ?>" name="so"/>
                                    <p class="findMaterial selfvalidate" data-balance="<?php echo site_url('Balance/printing/'); ?>" data-method="<?php echo site_url("autocomplete/findJob/"); ?>"><?php echo $job_name ?></p>
                                </div><hr class="spacer"/>
                            </div> <!-- /CHOOSE JOB -->

                            <div class="col-sm-6 col-md-6"> <!-- SELECT MACHINE -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Choose Machine</span>
                                    <select name="machine" class="form-control required">
                                      <option value="" readonly>Select Machine</option>
                                      <option <?php echo ($machine=='1')?'selected':''; ?> value="1">Machine# 1</option>
                                      <option <?php echo ($machine=='2')?'selected':''; ?> value="2">Machine# 2</option>
                                      <option <?php echo ($machine=='3')?'selected':''; ?> value="3">Machine# 3</option>
                                      <option <?php echo ($machine=='4')?'selected':''; ?> value="4">Machine# 4</option>
                                    </select>
                                </div><hr class="spacer"/>
                            </div> <!-- /SELECT MACHINE -->

                            <div class="col-sm-6 col-md-6"> <!-- OPERATOR NAMER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Operator Name</div>
                                    <input name="operator" type="text" placeholder="OPERATOR NAME" class="form-control required" value="<?php echo $operator; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /OPERATOR NAMER -->

                            <!-- <div class="col-sm-6 col-md-6"> ASSISTANT NAME
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input name="assistant" type="text" placeholder="ASSISTANT NAME" class="form-control required" value="<?php echo $assistant; ?>">
                                </div><hr class="spacer"/>
                            </div> /ASSISTANT NAME -->

                            <!-- <div class="col-sm-6 col-md-6"> HELPER NAME 
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input name="helper" type="text" placeholder="HELPER NAME" class="form-control required" value="<?php echo $helper; ?>">
                                </div><hr class="spacer"/>
                            </div> /HELPER NAME -->

                            <div class="col-sm-6 col-md-6"> <!-- PORDUCTION METER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Meter</div>
                                    <input name="production_meter" type="text" placeholder="PRODCUTION METER" class="form-control required" value="<?php echo $production_meter; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /PORDUCTION METER -->

                            <div class="col-sm-6 col-md-6"> <!-- PLAIN WASTAGE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Plain Wastage</div>
                                    <input name="plain_wastage" type="text" placeholder="PLAIN WASTAGE" class="form-control required" value="<?php echo $plain_wastage; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /PLAIN WASTAGE -->

                            <div class="col-sm-6 col-md-6"> <!-- PRINTED WASTAGE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Printed Wastage</div>
                                    <input name="printed_wastage" type="text" placeholder="PRINTED WASTAGE" class="form-control required" value="<?php echo $printed_wastage; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /PRINTED WASTAGE -->

                            <div class="col-sm-6 col-md-6"> <!-- SETTING WASTAGE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Setting Wastage</div>
                                    <input name="setting_wastage" type="text" placeholder="SETTING WASTAGE" class="form-control required" value="<?php echo $setting_wastage; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /SETTING WASTAGE -->

                            <div class="col-sm-6 col-md-6"> <!-- MATCHING WASTAGE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Color Matching Wastage</div>
                                    <input name="matching_wastage" type="text" placeholder="COLOR MATCHING WASTAGE" class="form-control required" value="<?php echo $matching_wastage; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /MATCHING WASTAGE -->

                            <div class="col-sm-6 col-md-6"> <!-- RUBBER SIZE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">QC Hold</div>
                                    <input name="qc_hold" type="text" placeholder="QC HOLD" class="form-control" value="<?php echo $qc_hold; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /RUBBER SIZE -->

                            <div class="col-sm-6 col-md-6"> <!-- CIRCUM -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Start Time</div>
                                    <input name="machine_start_time" type="text" placeholder="START TIME" class="form-control required dateTime" value="<?php echo $machine_start_time; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /CIRCUM -->

                            <div class="col-sm-6 col-md-6"> <!-- CIRCUM -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">End Time</div>
                                    <input name="machine_stop_time" type="text" placeholder="STOP TIME" class="form-control required dateTime" value="<?php echo $machine_stop_time; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /CIRCUM -->


                            <div class="col-sm-2 col-sm-offset-10 text-right">
                                <div class="btn-group">
                                    <button type="button" id="add" class="btn btn-success fa fa-plus"></button>
                                    <button type="button" id="delete" class="btn btn-danger fa fa-trash"></button>
                                </div>
                                <hr class="spacer"/>
                            </div> <!-- /CONTROL BUTTONS -->

                            <hr class="spacer"/>
                            <div class="col-sm-12"> <!-- Entry Table col -->
                                <div class="table-responsive">
                                    <table class="table" id="inputTable">
                                        <tr>
                                            <th>PLAIN WEIGHT</th>
                                            <th>PRINTED WEIGHT</th>
                                            <th>METER</th>
                                            <th>START TIME</th>
                                            <th>STOP TIME</th>
                                            <th>REMARK</th>
                                        </tr>
                                        <?php if ( isset($update) && $update==true ){ ?>
                                        <?php foreach ($print_detail as $item): ?>
                                        <tr class="data-row">
                                            <td><input name="plain_wt[]" type="text" placeholder="PLAIN WEIGHT" class="form-control selfvalidate" value="<?php echo $item->plain_wt; ?>"></td>
                                            <td><input name="printed_wt[]" type="text" placeholder="PRINTED WEIGHT" class="form-control selfvalidate" value="<?php echo $item->printed_wt; ?>"></td>
                                            <td><input name="meter[]" type="text" placeholder="METER" class="form-control selfvalidate" value="<?php echo $item->meter; ?>"></td>
                                            <td><input name="start_time[]" type="text" placeholder="START TIME" class="form-control dateTime" value="<?php echo $item->start_time; ?>"></td>
                                            <td><input name="end_time[]" type="text" placeholder="END TIME" class="form-control dateTime mQty" value="<?php echo $item->end_time; ?>"></td>
                                            <td><input name="remarks[]" type="text" placeholder="REMARKS" class="form-control mRate" value="<?php echo $item->remarks; ?>"></td>
                                        </tr>
                                        <?php endforeach ?>
                                        <?php }else{ ?>
                                        <tr class="data-row">
                                            <td><input name="plain_wt[]" type="text" placeholder="PLAIN WEIGHT" class="form-control selfvalidate" value="<?php ?>"></td>
                                            <td><input name="printed_wt[]" type="text" placeholder="PRINTED WEIGHT" class="form-control selfvalidate" value="<?php ?>"></td>
                                            <td><input name="meter[]" type="text" placeholder="METER" class="form-control selfvalidate" value="<?php ?>"></td>
                                            <td><input name="start_time[]" type="text" placeholder="START TIME" class="form-control dateTime" value=""></td>
                                            <td><input name="end_time[]" type="text" placeholder="END TIME" class="form-control dateTime mQty" value="<?php ?>"></td>
                                            <td><input name="remarks[]" type="text" placeholder="REMARKS" class="form-control mRate" value="<?php ?>"></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div> <!-- Responsive Table -->
                            </div> <!-- Entry Table col -->

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    

    #inputTable tr th:nth-child(3){
        width: 50px;
    }
    #inputTable tr td:nth-child(3){
        padding: 0px 0 0 9px;
        margin-bottom: -5px;
    }
    #inputTable #fancy-checkbox + label {
        margin-top: 1px;
        margin-bottom: -3px;
    }
    #inputTable tr th:nth-child(1),
    #inputTable tr th:nth-child(2),
    #inputTable tr th:nth-child(3),
    #inputTable tr th:nth-child(4),
    #inputTable tr th:nth-child(5) {
        width: 150px;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });

    $('#add').on('click', function(event) {
        event.preventDefault();
        // $('.autocomplete').autocomplete('destroy');
        var row = '<tr class="data-row">';
                row += '<td><input name="plain_wt[]" type="text" placeholder="PLAIN WEIGHT" class="form-control selfvalidate" value="<?php ?>"></td>';
                row += '<td><input name="printed_wt[]" type="text" placeholder="PRINTED WEIGHT" class="form-control selfvalidate" value="<?php ?>"></td>';
                row += '<td><input name="meter[]" type="text" placeholder="METER" class="form-control selfvalidate" value="<?php ?>"></td>';
                row += '<td><input name="start_time[]" type="text" placeholder="START TIME" class="form-control dateTime" value=""></td>';
                row += '<td><input name="end_time[]" type="text" placeholder="END TIME" class="form-control dateTime mQty" value="<?php ?>"></td>';
                row += '<td><input name="remarks[]" type="text" placeholder="REMARKS" class="form-control mRate" value="<?php ?>"></td>';
            row += '</tr>'; 
        $('#inputTable').append( $(row) );
        // $('.dateTime').appendDtpicker();
        // autocompleteInit();
    });

    $('#delete').on('click', function(event) {
        event.preventDefault();
        $('.del-row').remove();

        $('#formToSubmit').find('tr.data-row').each(function(index, el) {

            if( $('#formToSubmit').find('tr.data-row').length > 1 ){

                    $('<span class="del-row fa fa-times"></span>')
                        .appendTo( $(this) )
                        .fadeIn('slow')
                        .click(function(event) {

                            if ( confirm("Are you sure to delete this entry?") ) {
                                $(this).parent('tr.data-row')
                                    .fadeOut('slow', function() {
                                        $(this).remove();
                                        $('.del-row').remove();
                                    });

                            };
                       
                        }); // click event on delete button


            } // if data row more than one
            else{
                alert("Sorry One Row Must Be Fill.");
            }

        });
        // console.log(x);
    });

</script>


<?php 
else:
    echo not_permitted();
endif; 
?>