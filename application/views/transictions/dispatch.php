<?php
$role = (isset($update) && $update==true)? 'editRotoDispatchEntry':'addRotoDispatchEntry';
$pageTitle = (isset($update) && $update==true)? 'Update':'New';
$id = (isset($update) && $update==true)? $dispatch->id:'';
$date = (isset($update) && $update==true)? PKdate($dispatch->date):'';
$so = (isset($update) && $update==true)? $dispatch->so:'';
$job_name = (isset($update) && $update==true)? $dispatch->job_name:'Choose Job';
$challan = (isset($update) && $update==true)? $dispatch->challan:'';
$gross_weight = (isset($update) && $update==true)? $dispatch->gross_weight:'';
$tare_weight = (isset($update) && $update==true)? $dispatch->tare_weight:'';
$net_weight = (isset($update) && $update==true)? $dispatch->net_weight:'';
$destination = (isset($update) && $update==true)? $dispatch->destination:'';
$receiver = (isset($update) && $update==true)? $dispatch->receiver:'';
$no_carton = (isset($update) && $update==true)? $dispatch->no_carton:'';
$no_pcs = (isset($update) && $update==true)? $dispatch->no_pcs:'';
$no_roll = (isset($update) && $update==true)? $dispatch->no_roll:'';
$meter = (isset($update) && $update==true)? $dispatch->meter:'';
$remarks = (isset($update) && $update==true)? $dispatch->remarks:'';
$driver_name = (isset($update) && $update==true)? $dispatch->driver_name:'';
$vehicle = (isset($update) && $update==true)? $dispatch->vehicle:'';
$remarks = (isset($update) && $update==true)? $dispatch->remarks:'';
$url = site_url();
$actionLink = (isset($update) && $update==true) ? $url.'/Update/dispatch_entry/'.$dispatch->id:$url.'/Add/dispatch_entry';

 if (User_Model::hasAccess($role)):
    echo $feed_back;
?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Dispatch Entry <i><?php echo $pageTitle; ?> </i></h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmits" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">

                            <div class="col-sm-6 col-md-6"> <!-- DATE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Date: </div>
                                    <input name="date" type="text" readonly data-date="true" placeholder="DATE" class="form-control required" value="<?php echo $date; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-6 col-md-6"> <!-- CHOOSE JOB -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Choose Job: </span>
                                    <input type="hidden" value="<?php echo $so; ?>" name="so"/>
                                    <p class="findMaterial selfvalidate" data-balance="<?php echo site_url('Balance/dispatch/'); ?>" data-method="<?php echo site_url("autocomplete/findJob/"); ?>"><?php echo $job_name ?></p>
                                </div><hr class="spacer"/>
                            </div> <!-- /CHOOSE JOB -->

                            <div class="col-sm-6 col-md-6"> <!-- CHALLAN NUMBER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Challan:</div>
                                    <input name="challan" type="text" data-type="decimal" placeholder="CHALLAN NUMBER" class="form-control required" value="<?php echo $challan; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /CHALLAN NUMBER -->

                            <div class="col-sm-6 col-md-6"> <!-- METER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Meter: </div>
                                    <input name="meter" type="text" placeholder="METER" class="form-control" value="<?php echo $meter; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /METER -->

                            <div class="col-sm-6 col-md-6"> <!-- GROSS WEIGHT -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Gross Weight: </div>
                                    <input name="gross_weight" type="text" placeholder="GROSS WEIGHT" class="form-control required" value="<?php echo $gross_weight; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /GROSS WEIGHT -->

                            <div class="col-sm-6 col-md-6"> <!-- TARE WEIGHT -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Tare Weight: </div>
                                    <input name="tare_weight" type="text" data-type="decimal" placeholder="TARE WEIGHT" class="form-control required" value="<?php echo $tare_weight; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /TARE WEIGHT -->

                            <div class="col-sm-6 col-md-6"> <!-- NO PCS -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">No of PCS: </div>
                                    <input name="no_pcs" type="text" placeholder="NO PCS" class="form-control" value="<?php echo $no_pcs; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /NO PCS -->

                            <div class="col-sm-6 col-md-6"> <!-- NO ROLL -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">No of Roll</div>
                                    <input name="no_roll" type="text" placeholder="NO ROLL" class="form-control" value="<?php echo $no_roll; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /NO ROLL -->

                            <div class="col-sm-6 col-md-6"> <!-- NET WEIGHT -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Net Weight: </div>
                                    <input name="net_weight" type="text" data-type="decimal" readonly placeholder="NET WEIGHT" class="form-control required" value="<?php echo $net_weight; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /NET WEIGHT -->

                            <div class="col-sm-6 col-md-6"> <!-- NUMBER OF CARTON -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">No of Cartons: </div>
                                    <input name="no_carton" type="text" placeholder="NO OF CARTON" class="form-control" value="<?php echo $no_carton; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /NUMBER OF CARTON -->

                            <div class="col-sm-6 col-md-6"> <!-- DESTINATION -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Destination: </div>
                                    <input name="destination" type="text" placeholder="DESTINATION" class="form-control required" value="<?php echo $destination; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /DESTINATION -->

                            <div class="col-sm-6 col-md-6"> <!-- RECEIVER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Receiver: </div>
                                    <input name="receiver" type="text" placeholder="RECEIVER" class="form-control" value="<?php echo $receiver; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /RECEIVER -->

                            <div class="col-sm-6 col-md-6"> <!-- DRIVER NAME -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Driver: </div>
                                    <input name="driver_name" type="text" placeholder="DRIVER NAME" class="form-control required" value="<?php echo $driver_name; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /DRIVER NAME -->

                            <div class="col-sm-6 col-md-6"> <!-- VEHICLE NUMBER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Vehicle Number: </div>
                                    <input name="vehicle" type="text" placeholder="VEHICLE NUMBER" class="form-control" value="<?php echo $vehicle; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /VEHICLE NUMBER -->

                            <div class="col-sm-10"> <!-- REMARKS -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Remarks: </div>
                                    <input name="remarks" type="text" placeholder="REMARKS" class="form-control" value="<?php echo $remarks; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /REMARKS -->

                            <div class="col-sm-2 text-right">
                                <div class="btn-group">
                                    <button type="button" id="add" class="btn btn-success fa fa-plus"></button>
                                    <button type="button" id="delete" class="btn btn-danger fa fa-trash"></button>
                                </div><hr class="spacer"/>
                            </div> <!-- /CONTROL BUTTONS -->

                            <div class="col-sm-12"> <!-- Entry Table col -->
                                <div class="table-responsive">
                                    <table class="table" id="inputTable">
                                        <tr>
                                            <th>WEIGHT</th>
                                            <th>NO ROLL</th>
                                            <th>NO PCS</th>
                                            <th>NO CARTON</th>
                                            <th>DESCRIPTION</th>
                                        </tr>
                                        <?php if ( isset($update) && $update==true ){ ?>
                                        <?php foreach ($dispatch_detail as $item): ?>
                                        <tr class="data-row">
                                            <td><input name="fl_weight[]" type="text" placeholder="WEIGHT" class="form-control" value="<?php echo $item->weight; ?>"></td>
                                            <td><input name="fl_no_roll[]" type="text" placeholder="NO ROLL" class="form-control" value="<?php echo $item->no_roll; ?>"></td>
                                            <td><input name="fl_no_pcs[]" type="text" placeholder="NO PCS" class="form-control mQty" value="<?php echo $item->no_pcs; ?>"></td>
                                            <td><input name="fl_no_carton[]" type="text" placeholder="NO CARTON" class="form-control mQty" value="<?php echo $item->no_carton; ?>"></td>
                                            <td><input name="description[]" type="text" placeholder="DESCRIPTION" class="form-control" value="<?php echo $item->description; ?>"></td>
                                        </tr>
                                        <?php endforeach ?>
                                        <?php }else{ ?>
                                        <tr class="data-row">
                                            <td><input name="fl_weight[]" type="text" placeholder="WEIGHT" class="form-control" value=""></td>
                                            <td><input name="fl_no_roll[]" type="text" placeholder="NO ROLL" class="form-control" value=""></td>
                                            <td><input name="fl_no_pcs[]" type="text" placeholder="NO PCS" class="form-control" value=""></td>
                                            <td><input name="fl_no_carton[]" type="text" placeholder="NO CARTON" class="form-control" value=""></td>
                                            <td><input name="description[]" type="text" placeholder="DESCRIPTION" class="form-control" value=""></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div> <!-- Responsive Table -->
                            </div> <!-- Entry Table col -->

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    

    #inputTable tr th:last-child{
        width: auto;
    }
    #inputTable tr td:nth-child(3){
        padding: 0px 0 0 9px;
        margin-bottom: -5px;
    }
    #inputTable #fancy-checkbox + label {
        margin-top: 1px;
        margin-bottom: -3px;
    }
    #inputTable tr th:nth-child(1),
    #inputTable tr th:nth-child(2),
    #inputTable tr th:nth-child(3),
    #inputTable tr th:nth-child(4){
        width: 120px;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });

    $('[name=gross_weight], [name=tare_weight]').blur(function() {
        var gross = parseFloat($('[name=gross_weight]').val()),
            tare = parseFloat($('[name=tare_weight]').val()),
            net = (gross-tare).toFixed(2);
        if (!isNaN(net)) {
            $('[name=net_weight]').val( net );
        }   
    });

    $('#add').on('click', function(event) {
        event.preventDefault();
        // $('.autocomplete').autocomplete('destroy');
        var row = '<tr class="data-row">';
                row += '<td><input name="fl_weight[]" type="text" placeholder="WEIGHT" class="form-control" value=""></td>';
                row += '<td><input name="fl_no_roll[]" type="text" placeholder="NO ROLL" class="form-control" value=""></td>';
                row += '<td><input name="fl_no_pcs[]" type="text" placeholder="NO PCS" class="form-control" value=""></td>';
                row += '<td><input name="fl_no_carton[]" type="text" placeholder="NO CARTON" class="form-control" value=""></td>';
                row += '<td><input name="description[]" type="text" placeholder="DESCRIPTION" class="form-control" value=""></td>';
            row += '</tr>'; 
        $('#inputTable').append( $(row) );
        // $('.dateTime').appendDtpicker();
        // autocompleteInit();
    });

    $('#delete').on('click', function(event) {
        event.preventDefault();
        $('.del-row').remove();

        $('#formToSubmit').find('tr.data-row').each(function(index, el) {

            if( $('#formToSubmit').find('tr.data-row').length > 1 ){

                    $('<span class="del-row fa fa-times"></span>')
                        .appendTo( $(this) )
                        .fadeIn('slow')
                        .click(function(event) {

                            if ( confirm("Are you sure to delete this entry?") ) {
                                $(this).parent('tr.data-row')
                                    .fadeOut('slow', function() {
                                        $(this).remove();
                                        $('.del-row').remove();
                                    });

                            };
                       
                        }); // click event on delete button


            } // if data row more than one
            else{
                alert("Sorry One Row Must Be Fill.");
            }

        });
        // console.log(x);
    });

</script>

<?php 
else:
    echo not_permitted();
endif; 
?>