<?php

  $role = (isset($update) && $update==true)? 'updateJobMasterFile':'jobMasterFile';
  $pageTitle = (isset($update) && $update==true)? 'Update Job Structure':'New Job Structure';
  $job_code = (isset($update) && $update==true)? $structure->job_code:'';
  $job_name = (isset($update) && $update==true)? $structure->job_name:'';
  $print_type = (isset($update) && $update==true)? $structure->print_type:'';
  $print_as_per = (isset($update) && $update==true)? $structure->print_as_per:'';
  $cylinder = (isset($update) && $update==true)? $structure->cylinder:'';
  $cylinder_len = (isset($update) && $update==true && $structure->cylinder_len!=0)? $structure->cylinder_len:'';
  $cylinder_circum = (isset($update) && $update==true && $structure->cylinder_circum!=0)? $structure->cylinder_circum:'';
  $no_ups = (isset($update) && $update==true && $structure->no_ups!=0)? $structure->no_ups:'';
  $no_repeat = (isset($update) && $update==true && $structure->no_repeat!=0)? $structure->no_repeat:'';
  $no_lamination = (isset($update) && $update==true)? $structure->no_lamination:'';
  $print_material = (isset($update) && $update==true)? $structure->print_material:'';
  $lam1_material = (isset($update) && $update==true)? $structure->lam1_material:'';
  $lam2_material = (isset($update) && $update==true)? $structure->lam2_material:'';
  $lam3_material = (isset($update) && $update==true)? $structure->lam3_material:'';
  $slitting_reel_width = (isset($update) && $update==true && $structure->slitting_reel_width!=0)? $structure->slitting_reel_width:'';
  $slitting_cutting_size = (isset($update) && $update==true && $structure->slitting_cutting_size!=0)? $structure->slitting_cutting_size:'';
  $slitting_direction = (isset($update) && $update==true)? $structure->slitting_direction:'';
  $ink_series = (isset($update) && $update==true)? $structure->ink_series:'';
  $no_of_inks = (isset($update) && $update==true && $structure->no_of_inks!=0)? $structure->no_of_inks:'';
  $bm_qty = (isset($update) && $update==true && $structure->bm_qty!=0)? $structure->bm_qty:'';
  $bm_size = (isset($update) && $update==true)? $structure->job_name:'';
  $bm_type = (isset($update) && $update==true)? $structure->bm_type:'';
  $coa_required = (isset($update) && $update==true)? $structure->coa_required:'';
  $dc = (isset($update) && $update==true)? $structure->dc:'';
  $hold = (isset($update) && $update==true)? $structure->hold:'';
  $remarks = (isset($update) && $update==true)? $structure->remarks:'';
  $reason = (isset($update) && $update==true)? $structure->reason:'';
  $actionLink = (isset($update) && $update==true) ? '/Update/job_structure/'.$structure->job_code:'/Add/job_structure';

 if ($roles[$role]): 

?>
<form class="form-horizontal" id="formToSubmit" role="form" action="<?php echo base_url()."index.php".$actionLink; ?>" method="POST">
<div class="singin-wrap"> <!-- FORM WRAP -->

<div class="row"> <!-- ROW -->
    <div class="form-group">
      <h1 class="col-sm-8 col-sm-offset-4"><?php echo $pageTitle; ?></h1>
    </div>
</div>
<h3>General Info</h3><br>

<div class="row"> <!-- ROW -->
      <div class="col-sm-4"> <!-- JOB CODE -->
        <div class="input-group">
          <span class="input-group-addon">Job Code:</span>
            <input type="text" name="job_code" value="<?php echo $job_code; ?>" autocomplete="off" class="form-control is_num is_empty" 
            id="user_name" placeholder="Job Number">
        </div>
      </div> <!-- JOB CODE -->
      <div class="col-sm-8"> <!-- JOB NAME -->
        <div class="input-group">
          <span class="input-group-addon">Job Name:</span>
          <input type="text" name="job_name" value="<?php echo $job_name; ?>" autocomplete="off" class="form-control is_empty" 
          id="job_Name" placeholder="Job Name">
        </div>
      </div> <!-- JOB NAME -->

</div><br> <!-- END ROW -->

<div class="row"> <!-- ROW -->
      
      <div class="col-sm-3"> <!-- PRINT TYPE -->
        <div class="input-group">
            <span class="input-group-addon">Print Type:</span>
            <select class="form-control is_empty" name="print_type">
              <option <?php echo $print_type=='Top'? 'selected':''; ?>>Top</option>
              <option <?php echo $print_type=='Reverse'? 'selected':''; ?>>Reverse</option>
              <option <?php echo $print_type=='Unprinted'? 'selected':''; ?>>Unprinted</option>
            </select>
        </div>
      </div> <!-- PRINT TYPE -->
      <div class="col-sm-3"> <!-- PRINT AS PER -->
        <div class="input-group">
            <span class="input-group-addon">Printing As Per</span>
            <select class="form-control" name="print_as_per">
              <option <?php echo $print_as_per=='Party Approval'? 'selected':''; ?>>Party Approval</option>
              <option <?php echo $print_as_per=='Amendment'? 'selected':''; ?>>Amendment</option>
              <option <?php echo $print_as_per=='Approved Sample'? 'selected':''; ?>>Approved Sample</option>
              <option <?php echo $print_as_per=='Last Production'? 'selected':''; ?>>Last Production</option>
            </select>
        </div>
      </div> <!-- PRINT AS PER -->
      <div class="col-sm-3"> <!-- CYLINDER -->
        <div class="input-group">
            <span class="input-group-addon">Cylinder:</span>
            <select class="form-control" name="cylinder">
              <option <?php echo $cylinder=='Old Cylinders'? 'selected':''; ?>>Old Cylinders</option>
              <option <?php echo $cylinder=='New Cylinders'? 'selected':''; ?>>New Cylinders</option>
            </select>
        </div>
      </div> <!-- CYLINDER -->
      <div class="col-sm-3"> <!-- CYLINDER LENGTH -->
        <div class="input-group">
          <span class="input-group-addon">Cylinder Length:</span>
            <input type="text" name="cylinder_len" value="<?php echo $cylinder_len; ?>" autocomplete="off" class="form-control is_num" id="user_name" placeholder="Cylinder Length">
        </div>
      </div> <!-- CYLINDER LENGTH -->

</div><br> <!-- END ROW -->

<div class="row"> <!-- ROW -->

      <div class="col-sm-3">
        <div class="input-group"> <!-- CYLINDER CIRCUM -->
          <span class="input-group-addon">Cylinder Circum:</span>
          <input type="text" name="cylinder_circum" value="<?php echo $cylinder_circum; ?>" autocomplete="off" class="form-control is_num" id="job_Name" placeholder="Cylinder Circum">
        </div>
      </div> <!-- CYLINDER CIRCUM -->
      <div class="col-sm-3"> <!-- NO. OF UPS -->
        <div class="input-group">
          <span class="input-group-addon">No. Of Ups:</span>
          <input type="text" name="no_ups" value="<?php echo $no_ups; ?>" autocomplete="off" class="form-control is_num" id="customer_Name" placeholder="Number Od Ups">
      </div>
    </div> <!-- NO. OF UPS -->
      <div class="col-sm-3"> <!-- NO. REPEAT -->
        <div class="input-group">
          <span class="input-group-addon">No. Repeat:</span>
          <input type="text" name="no_repeat" value="<?php echo $no_repeat; ?>" autocomplete="off" class="form-control is_num" id="customer_Name" placeholder="Number Of Repeat">
        </div>
    </div> <!-- NO. REPEAT -->
    <div class="col-sm-3"> <!-- NO. OF LAMINATION -->
        <div class="input-group">
          <span class="input-group-addon">No Of Lamination:</span>
          <select class="form-control" id="noLamination" name="no_lamination">
              <option <?php echo $no_lamination=='null'? 'selected':''; ?> value="null">Select Lamination</option>
              <option <?php echo $no_lamination=='1'? 'selected':''; ?> value="1">1</option>
              <option <?php echo $no_lamination=='2'? 'selected':''; ?> value="2">2</option>
              <option <?php echo $no_lamination=='3'? 'selected':''; ?> value="3">3</option>
            </select>
        </div>
    </div> <!-- NO. OF LAMINATION -->
    
</div><br> <!-- END ROW -->

<div class="row"> <!-- ROW -->

  <div class="col-sm-3"> <!-- PRINTING SUBSTRATE -->
      <h4>Printing Substrate</h4>
      <div class="input-group">
          <input type="hidden" value="<?php echo $print_material; ?>" name="print_material"/>
          <input type="text" class="form-control autocomplete is_empty" value="<?php echo $print_material_name; ?>" 
          placeholder="Choose Printing Material" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
      </div>
  </div> <!-- PRINTING SUBSTRATE -->
  
  <div class="col-sm-3 <?php echo ( isset($update)&&$update==true )?'':'optionalMaterial'; ?>"> <!-- LAMINATION SUBSTRATE -->
      <h4>1st Lamination Substrate</h4>
      <div class="input-group">
          <input type="hidden" value="<?php echo $lam1_material; ?>" name="lam1_material"/>
          <input type="text" class="form-control autocomplete" value="<?php echo $lam1_material_name; ?>" 
          placeholder="Choose Lamination Material" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
      </div>
  </div> <!-- LAMINATION SUBSTRATE -->

  <div class="col-sm-3 <?php echo ( isset($update)&&$update==true )?'':'optionalMaterial2'; ?>"> <!-- LAMINATION SUBSTRATE -->
      <h4>2nd Lamination Substrate</h4>
      <div class="input-group">
          <input type="hidden" value="<?php echo $lam2_material; ?>" name="lam2_material"/>
          <input type="text" class="form-control autocomplete" value="<?php echo $lam2_material_name; ?>" 
          placeholder="Choose Lamination Material" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
      </div>
  </div> <!-- LAMINATION SUBSTRATE -->

  <div class="col-sm-3 <?php echo ( isset($update)&&$update==true )?'':'optionalMaterial3'; ?>"> <!-- LAMINATION SUBSTRATE -->
      <h4>3rd Lamination Substrate</h4>
      <div class="input-group">
          <input type="hidden" value="<?php echo $lam3_material; ?>" name="lam3_material"/>
          <input type="text" class="form-control autocomplete" value="<?php echo $lam3_material_name; ?>" 
          placeholder="Choose Lamination Material" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
      </div>
  </div> <!-- LAMINATION SUBSTRATE -->
    
</div><br> <!-- END ROW -->

<div class="row"> <!-- ROW -->

  <div class="col-sm-6">
      <h4>Slitting</h4>

        <div class="input-group"> <!-- SLITTING WIDTH -->
          <span class="input-group-addon">Real Width:</span>
            <input type="text" name="slitting_reel_width" value="<?php echo $slitting_reel_width; ?>" autocomplete="off" class="form-control is_num" id="user_name" placeholder="Mili Meter">
          <span class="input-group-addon">MM</span>
        </div><br> <!-- SLITTING WIDTH -->

        <div class="input-group"> <!-- CUT OF LENGTH -->
          <span class="input-group-addon">Cut Of Length:</span>
          <input type="text" name="slitting_cutting_size" value="<?php echo $slitting_cutting_size; ?>" autocomplete="off" class="form-control is_num" id="job_Name" placeholder="Cut Of Length">
          <span class="input-group-addon">MM</span>
        </div><br> <!-- CUT OF LENGTH -->

        <div class="input-group"> <!-- DIRECTION -->
            <span class="input-group-addon">Direction:</span>
            <select class="form-control is_empty" 
              name="slitting_direction">
              <option <?php echo $slitting_direction=='Left Hand Side'? 'selected':''; ?>>Left Hand Side</option>
              <option <?php echo $slitting_direction=='Right Hand Side'? 'selected':''; ?>>Right Hand Side</option>
              <option <?php echo $slitting_direction=='Print Straight'? 'selected':''; ?>>Print Straight</option>
            </select>
        </div><br> <!-- DIRECTION -->
  </div>
  
  <div class="col-sm-6">
      <h4>Recommended INKs</h4>
      <div class="input-group"> <!-- INK SERIES -->
            <span class="input-group-addon">INK Series:</span>
            <select class="form-control is_empty" 
              name="ink_series">
              <option <?php echo $ink_series=='Cynex'? 'selected':''; ?>>Cynex</option>
              <option <?php echo $ink_series=='Flash'? 'selected':''; ?>>Flash</option>
              <option <?php echo $ink_series=='S-Lam'? 'selected':''; ?>>S-Lam</option>
              <option <?php echo $ink_series=='GFR'? 'selected':''; ?>>GFR</option>
              <option <?php echo $ink_series=='Pinacl'? 'selected':''; ?>>Pinacl</option>
              <option <?php echo $ink_series=='SW-1'? 'selected':''; ?>>SW-1</option>
              <option <?php echo $ink_series=='NTS'? 'selected':''; ?>>NTS</option>
              <option <?php echo $ink_series=='GMR'? 'selected':''; ?>>GMR</option>
              <option <?php echo $ink_series=='096 (Dic)'? 'selected':''; ?>>096 (Dic)</option>
              <option <?php echo $ink_series=='GR-8'? 'selected':''; ?>>GR-8</option>
              <option <?php echo $ink_series=='LAM'? 'selected':''; ?>>LAM</option>
              <option <?php echo $ink_series=='EGL'? 'selected':''; ?>>EGL</option>
              <option <?php echo $ink_series=='Concentrates'? 'selected':''; ?>>Concentrates</option>
            </select>
        </div><br> <!-- INK SERIES -->

        <div class="input-group"> <!-- # OF INK -->
          <span class="input-group-addon">No Of INKs:</span>
          <input type="text" name="no_of_inks" value="<?php echo $no_of_inks; ?>" autocomplete="off" class="form-control is_num" id="job_Name" placeholder="Number Of INKS">
        </div>
  </div>

    
</div><br> <!-- END ROW -->

<div class="row"> <!-- ROW -->
  <div class="col-sm-6">
      <h4>Bag Making</h4>
        <div class="input-group"> <!-- ORDER QUANTITY OF BAGS -->
          <span class="input-group-addon">Order Quantity:</span>
            <input type="text" name="bm_qty" value="<?php echo $bm_qty; ?>" autocomplete="off" class="form-control is_num" id="user_name" placeholder="Order Quantity">
          <span class="input-group-addon">PCS/KG</span>
        </div><br> <!-- ORDER QUANTITY OF BAGS -->

        <div class="input-group"> <!-- CUT OF LENGTH -->
          <span class="input-group-addon">Cut Of Length:</span>
          <input type="text" name="bm_size" value="<?php echo $bm_size; ?>" autocomplete="off" class="form-control" id="job_Name" placeholder="Cut Of Length">
          <span class="input-group-addon">MM</span>
        </div><br> <!-- CUT OF LENGTH -->

        <div class="input-group"> <!-- BAG TYPE -->
            <span class="input-group-addon">Bag Types:</span>
            <select class="form-control" name="bm_type">
              <option <?php echo $bm_size=='Center Seal'? 'selected':''; ?>>Center Seal</option>
              <option <?php echo $bm_size=='Center Seal + Gusset'? 'selected':''; ?>>Center Seal + Gusset</option>
              <option <?php echo $bm_size=='Bottom Seal'? 'selected':''; ?>>Bottom Seal</option>
              <option <?php echo $bm_size=='3 Side Seal'? 'selected':''; ?>>3 Side Seal</option>
              <option <?php echo $bm_size=='2 Side Seal'? 'selected':''; ?>>2 Side Seal</option>
              <option <?php echo $bm_size=='Stand Up Pouch'? 'selected':''; ?>>Stand Up Pouch</option>
              <option <?php echo $bm_size=='Stand Up Pouch + Zipper'? 'selected':''; ?>>Stand Up Pouch + Zipper</option>
            </select>
        </div> <!-- BAG TYPE -->
  </div>
    
  <div class="col-sm-6">

    <h4>Dispatch</h4>
    <div class="input-group"> <!-- JOB NAME -->
      <span class="input-group-addon">COA Required:</span>
        <div class="input-group">
          <span class="input-group-addon">
            <input type="radio" name="coa_required" <?php echo $coa_required=="yes"?'checked':''; ?> value="yes" checked>
          </span>
          <span class="input-group-addon">Yes</span>
          <span class="input-group-addon">
            <input type="radio" name="coa_required" <?php echo $coa_required=="no"?'checked':''; ?>  value="no">
          </span>
          <span class="input-group-addon">No</span>
        </div>
    </div><br>

    <div class="input-group">
      <span class="input-group-addon"><label for="waitingJob">Waiting?</label> 
        <input type="checkbox" value="1" <?php echo $hold=="1"?'checked':''; ?> class="waiting" id="waitingJob" name="hold">
      </span>
      <input type="text" name="reason" value="<?php echo $reason; ?>" class="form-control">
    </div><br><!-- /input-group -->

    <div class="input-group"> <!-- Delivery Challan -->
        <span class="input-group-addon">Delivery Challan:</span>
        <select class="form-control" name="dc">
          <option <?php echo $dc==">Non Official"?'selected':''; ?>>Non Official</option>
          <option <?php echo $dc==">Official"?'selected':''; ?>>Official</option>
        </select>
    </div><br>
  </div>
</div><br><!-- END ROW -->

</div class="row"><!-- ROW -->
  <div class="col-md-6">
    <div class="input-group"> <!-- JOB NAME -->
      <span class="input-group-addon">Remarks:</span>
      <textarea placeholder="Remarks (If Any)" name="remarks" rows="3" cols="55"><?php echo $remarks; ?></textarea>
    </div>
  </div>
</div><br><!-- END ROW -->

<div class="row"> <!-- ROW -->

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-2">
        <button type="submit" name="submit" class="btn btn-warning btn-lg">Submit</button>
      </div>
    </div>
</div> <!-- END ROW -->
</div> <!-- FORM WRAP -->
</form>
<?php endif; ?>
</div><!-- .container -->