<?php
$role = (isset($update) && $update==true)? 'editRotoLaminationEntry':'addRotoLaminationEntry';
$pageTitle = (isset($update) && $update==true)? 'Update':'New';
$id = (isset($update) && $update==true)? $lamination->id:'';
$date = (isset($update) && $update==true)? PKdate($lamination->date):'';
$so = (isset($update) && $update==true)? $lamination->so:'';
$job_name = (isset($update) && $update==true)? $lamination->job_name:'Choose Job';
$machine = (isset($update) && $update==true)? $lamination->machine:'';
$operator = (isset($update) && $update==true)? $lamination->operator:'';
$assistant = (isset($update) && $update==true)? $lamination->assistant:'';
$helper = (isset($update) && $update==true)? $lamination->helper:'';
$temperature = (isset($update) && $update==true)? $lamination->temperature:'';
$rubber_size = (isset($update) && $update==true)? $lamination->rubber_size:'';
$speed = (isset($update) && $update==true)? $lamination->speed:'';
$qc_hold = (isset($update) && $update==true)? $lamination->qc_hold:'';
$start_time = (isset($update) && $update==true)? $lamination->start_time:'';
$end_time = (isset($update) && $update==true)? $lamination->end_time:'';
$blade = (isset($update) && $update==true)? $lamination->blade:'';
$lamination_number = (isset($update) && $update==true)? $lamination->lamination_number:'';
$total_wastage = (isset($update) && $update==true)? $lamination->total_wastage:'';
$balance = (isset($update) && $update==true)? $lamination->balance:'';
$glue = (isset($update) && $update==true)? $lamination->glue:'';
$glue_name = (isset($update) && $update==true && $lamination->glue_name)? $lamination->glue_name:'Choose Glue';
$glue_qty = (isset($update) && $update==true)? $lamination->glue_qty:'';
$hardner = (isset($update) && $update==true)? $lamination->hardner:'';
$hardner_name = (isset($update) && $update==true && $lamination->hardner_name)? $lamination->hardner_name:'Choose hardner';
$hardner_qty = (isset($update) && $update==true)? $lamination->hardner_qty:'';
$ethyl = (isset($update) && $update==true)? $lamination->ethyl:'';
$ethyl_name = (isset($update) && $update==true && $lamination->ethyl_name)? $lamination->ethyl_name:'Choose Ethyl';
$ethyl_qty = (isset($update) && $update==true)? $lamination->ethyl_qty:'';
$url = site_url();

$actionLink = (isset($update) && $update==true) ? $url.'/Update/lamination_entry/'.$lamination->id:$url.'/Add/lamination_entry';

 if (User_Model::hasAccess($role)):
    echo $feed_back; ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Lamination Entry <i><?php echo $pageTitle; ?></i></h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">

                            <div class="col-sm-6 col-md-6"> <!-- DATE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Date</div>
                                    <input name="date" type="text" readonly data-date="true" placeholder="DATE" class="form-control required" value="<?php echo $date; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-6 col-md-6"> <!-- CHOOSE JOB -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Choose Job</span>
                                    <input type="hidden" value="<?php echo $so; ?>" name="so"/>
                                    <p class="findMaterial selfvalidate" data-balance="<?php echo site_url('Balance/Lamination/'); ?>" data-method="<?php echo site_url("autocomplete/findJob/"); ?>"><?php echo $job_name ?></p>
                                </div><hr class="spacer"/>
                            </div> <!-- /CHOOSE JOB -->

                            <div class="col-sm-6 col-md-6"> <!-- SELECT MACHINE -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Machine #</span>
                                    <select name="machine" class="form-control required">
                                      <option value="">Select Machine</option>
                                      <option <?php echo ($machine=='dl')?'selected':''; ?> value="dl">Dry Lamination (DL)</option>
                                      <option <?php echo ($machine=='sl')?'selected':''; ?> value="sl">Solvent Lamination (SL)</option>
                                    </select>
                                </div><hr class="spacer"/>
                            </div> <!-- /SELECT MACHINE -->

                            <div class="col-sm-6 col-md-6"> <!-- SELECT LAMINATION -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Lamination #</span>
                                    <select name="lamination_number" class="form-control required">
                                      <option value="">Select Number</option>
                                      <option <?php echo ($lamination_number=='1')?'selected':''; ?> value="1">First Lamination</option>
                                      <option <?php echo ($lamination_number=='2')?'selected':''; ?> value="2">Second Lamination</option>
                                      <option <?php echo ($lamination_number=='3')?'selected':''; ?> value="3">Third Lamination</option>
                                    </select>
                                </div><hr class="spacer"/>
                            </div> <!-- /SELECT LAMINATION -->

                            <div class="col-sm-6 col-md-6"> <!-- OPERATOR NAMER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Operater Name</div>
                                    <input name="operator" type="text" placeholder="OPERATOR NAME" class="form-control required" value="<?php echo $operator; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /OPERATOR NAMER -->

                            <div class="col-sm-6 col-md-6"> <!-- ASSISTANT NAME -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Assistant Name</div>
                                    <input name="assistant" type="text" placeholder="ASSISTANT NAME" class="form-control required" value="<?php echo $assistant; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /ASSISTANT NAME -->

                            <div class="col-sm-6 col-md-6"> <!-- HELPER NAME -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Helper Name</div>
                                    <input name="helper" type="text" placeholder="HELPER NAME" class="form-control required" value="<?php echo $helper; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /HELPER NAME -->

                            <div class="col-sm-6 col-md-6"> <!-- TEMPERATURE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Temperature</div>
                                    <input name="temperature" type="text" placeholder="TEMPERATURE" class="form-control required" value="<?php echo $temperature; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /TEMPERATURE -->

                            <div class="col-sm-6 col-md-6"> <!-- SPEED -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Speed</div>
                                    <input name="speed" type="text" placeholder="SPEED" class="form-control required" value="<?php echo $speed; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /SPEED -->

                            <div class="col-sm-6 col-md-6"> <!-- SPEED -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">QC Hold</div>
                                    <input name="qc_hold" type="text" placeholder="QC HOLD" class="form-control" value="<?php echo $qc_hold; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /SPEED -->

                            <div class="col-sm-6 col-md-6"> <!-- TOTAL WASTAGE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Wastage</div>
                                    <input name="total_wastage" type="text" placeholder="TOTAL WASTAGE" class="form-control required" value="<?php echo $total_wastage; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /TOTAL WASTAGE -->

                            <div class="col-sm-6 col-md-6"> <!-- RUBBER SIZE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Rubbar Size</div>
                                    <input name="rubber_size" type="text" placeholder="RUBBER SIZE" class="form-control required" value="<?php echo $rubber_size; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /RUBBER SIZE -->

                            <div class="col-sm-6 col-md-6"> <!-- START TIME -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Start Time</div>
                                    <input name="start_time" type="text" placeholder="START TIME" class="form-control required dateTime" value="<?php echo $start_time; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /START TIME -->

                            <div class="col-sm-6 col-md-6"> <!-- STOP TIME -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Stop Time</div>
                                    <input name="end_time" type="text" placeholder="STOP TIME" class="form-control required dateTime" value="<?php echo $end_time; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /STOP TIME -->

                            <div class="col-sm-6 col-md-6"> <!-- CHOOSE GLUE -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Choose Glue</span>
                                    <input type="hidden" value="<?php echo $glue; ?>" name="glue"/>
                                    <p class="findMaterial selfvalidate" data-method="<?php echo site_url("autocomplete/findGlue/"); ?>"><?php echo $glue_name ?></p>
                                </div><hr class="spacer"/>
                            </div> <!-- /CHOOSE GLUE -->

                            <div class="col-sm-6 col-md-6"> <!-- GLUE QUANTITY -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Glue Qty</div>
                                    <input name="glue_qty" data-type="decimal" type="text" placeholder="GLUE QUANTITY" class="form-control required" value="<?php echo $glue_qty; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /GLUE QUANTITY -->

                            <div class="col-sm-6 col-md-6"> <!-- CHOOSE HARDNER -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Choose Hardner</span>
                                    <input type="hidden" value="<?php echo $hardner; ?>" name="hardner"/>
                                    <p class="findMaterial selfvalidate" data-method="<?php echo site_url("autocomplete/findHardner/"); ?>"><?php echo $hardner_name ?></p>
                                </div><hr class="spacer"/>
                            </div> <!-- /CHOOSE HARDNER -->

                            <div class="col-sm-6 col-md-6"> <!-- HARDNER QUANTITY -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Hardner Qty</div>
                                    <input name="hardner_qty" data-type="decimal" type="text" placeholder="HARDNER QUANTITY" class="form-control required" value="<?php echo $hardner_qty; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /HARDNER QUANTITY -->

                            <div class="col-sm-6 col-md-6"> <!-- CHOOSE ETHYL -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Choose Ethyl</span>
                                    <input type="hidden" value="<?php echo $ethyl; ?>" name="ethyl"/>
                                    <p class="findMaterial selfvalidate" data-method="<?php echo site_url("autocomplete/findEthyl/"); ?>"><?php echo $ethyl_name ?></p>
                                </div><hr class="spacer"/>
                            </div> <!-- /CHOOSE ETHYL -->

                            <div class="col-sm-6 col-md-6"> <!-- ETHYL QUANTITY -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Ethyl Qty</div>
                                    <input name="ethyl_qty" data-type="decimal" type="text" placeholder="ETHYL QUANTITY" class="form-control required" value="<?php echo $ethyl_qty; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /ETHYL QUANTITY -->

                            <div class="col-sm-6 col-md-6"> <!-- BALANCE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Balance</div>
                                    <input name="balance" type="text" placeholder="BALACE" class="form-control required" value="<?php echo $balance; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /BALANCE -->

                            <div class="col-sm-6 col-md-6"> <!-- BLADE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Blade</div>
                                    <input name="blade" type="text" placeholder="BLADE" class="form-control required" value="<?php echo $blade; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /BLADE -->

                            <div class="col-sm-2 col-sm-offset-10 text-right">
                                <div class="btn-group">
                                    <button type="button" id="add" class="btn btn-success fa fa-plus"></button>
                                    <button type="button" id="delete" class="btn btn-danger fa fa-trash"></button>
                                </div>
                                <hr class="spacer"/>
                            </div> <!-- /CONTROL BUTTONS -->

                            <hr class="spacer"/>
                            <div class="col-sm-12"> <!-- Entry Table col -->
                                <div class="table-responsive">
                                    <table class="table" id="inputTable">
                                        <tr>
                                            <th>PRTD WEIGHT</th>
                                            <th>PLN WEIGHT</th>
                                            <th>LMTD WEIGHT</th>
                                            <th>METER</th>
                                            <th>TREATMENT</th>
                                            <th>GSM</th>
                                            <th>REMARK</th>
                                        </tr>
                                        <?php if ( isset($update) && $update==true ){ ?>
                                        <?php foreach ($lamination_detail as $item): ?>
                                        <tr class="data-row">
                                            <td><input name="printed_weight[]" type="text" data-type="decimal" placeholder="PRINTED WEIGHT" class="form-control selfvalidate" value="<?php echo $item->printed_weight; ?>"></td>
                                            <td><input name="plain_weight[]" type="text" data-type="decimal" placeholder="PLAIN WEIGHT" class="form-control selfvalidate" value="<?php echo $item->plain_weight; ?>"></td>
                                            <td><input name="laminated_weight[]" type="text" data-type="decimal" placeholder="LAMINATED WEIGHT" class="form-control selfvalidate" value="<?php echo $item->laminated_weight; ?>"></td>
                                            <td><input name="meter[]" type="text" data-type="decimal" placeholder="METER" class="form-control selfvalidate" value="<?php echo $item->meter; ?>"></td>
                                            <td><input name="treatment[]" type="text" data-type="decimal" placeholder="TREATMENT" class="form-control selfvalidate mQty" value="<?php echo $item->treatment; ?>"></td>
                                            <td><input name="gsm[]" type="text" data-type="decimal" placeholder="GSM" class="form-control selfvalidate mQty" value="<?php echo $item->gsm; ?>"></td>
                                            <td><input name="remarks[]" type="text" placeholder="REMARKS" class="form-control" value="<?php echo $item->remarks; ?>"></td>
                                        </tr>
                                        <?php endforeach ?>
                                        <?php }else{ ?>
                                        <tr class="data-row">
                                            <td><input name="printed_weight[]" type="text" data-type="decimal" placeholder="PRINTED WEIGHT" class="form-control selfvalidate" value=""></td>
                                            <td><input name="plain_weight[]" type="text" data-type="decimal" placeholder="PLAIN WEIGHT" class="form-control selfvalidate" value=""></td>
                                            <td><input name="laminated_weight[]" type="text" data-type="decimal" placeholder="LAMINATED WEIGHT" class="form-control selfvalidate" value=""></td>
                                            <td><input name="meter[]" type="text" data-type="decimal" placeholder="METER" class="form-control selfvalidate" value=""></td>
                                            <td><input name="treatment[]" type="text" data-type="decimal" placeholder="TREATMENT" class="form-control selfvalidate" value=""></td>
                                            <td><input name="gsm[]" type="text" data-type="decimal" placeholder="GSM" class="form-control selfvalidate mQty" value=""></td>
                                            <td><input name="remarks[]" type="text" placeholder="REMARKS" class="form-control" value=""></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div> <!-- Responsive Table -->
                            </div> <!-- Entry Table col -->

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    

    #inputTable tr th:last-child{
        width: auto;
    }
    #inputTable tr td:nth-child(3){
        padding: 0px 0 0 9px;
        margin-bottom: -5px;
    }
    #inputTable #fancy-checkbox + label {
        margin-top: 1px;
        margin-bottom: -3px;
    }
    #inputTable tr th:nth-child(1),
    #inputTable tr th:nth-child(2),
    #inputTable tr th:nth-child(3),
    #inputTable tr th:nth-child(4),
    #inputTable tr th:nth-child(5),
    #inputTable tr th:nth-child(6) {
        width: 120px;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });

    $('#add').on('click', function(event) {
        event.preventDefault();
        // $('.autocomplete').autocomplete('destroy');
        var row = '<tr class="data-row">';
                row += '<td><input name="printed_weight[]" type="text" data-type="decimal" placeholder="PRINTED WEIGHT" class="form-control selfvalidate" value=""></td>';
                row += '<td><input name="plain_weight[]" type="text" data-type="decimal" placeholder="PLAIN WEIGHT" class="form-control selfvalidate" value=""></td>';
                row += '<td><input name="laminated_weight[]" type="text" data-type="decimal" placeholder="LAMINATED WEIGHT" class="form-control selfvalidate" value=""></td>';
                row += '<td><input name="meter[]" type="text" data-type="decimal" placeholder="METER" class="form-control selfvalidate" value=""></td>';
                row += '<td><input name="treatment[]" type="text" data-type="decimal" placeholder="TREATMENT" class="form-control selfvalidate" value=""></td>';
                row += '<td><input name="gsm[]" type="text" data-type="decimal" placeholder="GSM" class="form-control selfvalidate mQty" value=""></td>';
                row += '<td><input name="remarks[]" type="text" placeholder="REMARKS" class="form-control" value=""></td>';
                row += '</tr>'; 
        $('#inputTable').append( $(row) );
        // $('.dateTime').appendDtpicker();
        // autocompleteInit();
    });

    $('#delete').on('click', function(event) {
        event.preventDefault();
        $('.del-row').remove();

        $('#formToSubmit').find('tr.data-row').each(function(index, el) {

            if( $('#formToSubmit').find('tr.data-row').length > 1 ){

                    $('<span class="del-row fa fa-times"></span>')
                        .appendTo( $(this) )
                        .fadeIn('slow')
                        .click(function(event) {

                            if ( confirm("Are you sure to delete this entry?") ) {
                                $(this).parent('tr.data-row')
                                    .fadeOut('slow', function() {
                                        $(this).remove();
                                        $('.del-row').remove();
                                    });

                            };
                       
                        }); // click event on delete button


            } // if data row more than one
            else{
                alert("Sorry One Row Must Be Fill.");
            }

        });
        // console.log(x);
    });

</script>

<?php 
else:
    echo not_permitted();
endif; 
?>