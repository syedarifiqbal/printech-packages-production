<?php
$role = (isset($update) && $update==true)? 'editRotoRewindingEntry':'addRotoRewindingEntry';
$pageTitle = (isset($update) && $update==true)? 'Update':'New';
$id = (isset($update) && $update==true)? $slitting->id:'';
$date = (isset($update) && $update==true)? PKdate($slitting->date):'';
$so = (isset($update) && $update==true)? $slitting->so:'';
$job_name = (isset($update) && $update==true)? $slitting->job_name:'Choose Job';
$operator = (isset($update) && $update==true)? $slitting->operator:'';
$start_time = (isset($update) && $update==true)? $slitting->start_time:'';
$end_time = (isset($update) && $update==true)? $slitting->end_time:'';
$weight_before = (isset($update) && $update==true)? $slitting->weight_before:'';
$weight_after = (isset($update) && $update==true)? $slitting->weight_after:'';
$wastage = (isset($update) && $update==true)? $slitting->wastage:'';
$trim = (isset($update) && $update==true)? $slitting->trim:'';
$direction = (isset($update) && $update==true)? $slitting->direction:'';
$remarks = (isset($update) && $update==true)? $slitting->remarks:'';
$url = site_url();

$actionLink = (isset($update) && $update==true) ? $url.'/Update/slitting_entry/'.$slitting->id:$url.'/Add/slitting_entry';

 if (User_Model::hasAccess($role)):
    echo $feed_back; ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>Slitting Entry</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">

                            <div class="col-sm-6 col-md-6"> <!-- DATE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input name="date" type="text" readonly data-date="true" placeholder="DATE" class="form-control required" value="<?php echo $date; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-6 col-md-6"> <!-- CHOOSE JOB -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-credit-card"></i></span>
                                    <input type="hidden" value="<?php echo $so; ?>" name="so"/>
                                    <p class="findMaterial selfvalidate" title="CHOOSE JOB" data-balance="<?php echo site_url('Balance/slitting/'); ?>" data-method="<?php echo site_url("autocomplete/findJob/"); ?>"><?php echo $job_name ?></p>
                                </div><hr class="spacer"/>
                            </div> <!-- /CHOOSE JOB -->

                            <div class="col-sm-6 col-md-6"> <!-- OPERATOR NAMER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input name="operator" title="OPERATOR NAME" type="text" placeholder="OPERATOR NAME" class="form-control required" value="<?php echo $operator; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /OPERATOR NAMER -->

                            <div class="col-sm-6 col-md-6"> <!-- START TIME -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                    <input name="start_time" title="START TIME" type="text" placeholder="START TIME" class="form-control required dateTime" value="<?php echo $start_time; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /START TIME -->

                            <div class="col-sm-6 col-md-6"> <!-- END TIME -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                    <input name="end_time" title="STOP TIME" type="text" placeholder="STOP TIME" class="form-control required dateTime" value="<?php echo $end_time; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /END TIME -->

                            <div class="col-sm-6 col-md-6"> <!-- WEIGHT BEFORE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input name="weight_before" title="WEIGHT BEFORE" data-type="decimal" type="text" placeholder="Weight Before" class="form-control required" value="<?php echo $weight_before; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /WEIGHT BEFORE -->

                            <div class="col-sm-6 col-md-6"> <!-- WEIGHT AFTER -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    <input name="weight_after" title="WEIGHT AFTER" data-type="decimal" type="text" placeholder="WEIGHT AFTER" class="form-control required" value="<?php echo $weight_after; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /WEIGHT AFTER -->

                            <div class="col-sm-6 col-md-6"> <!-- WASTAGE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-trash-o"></i></div>
                                    <input name="wastage" title="WASTAGE" data-type="decimal" type="text" placeholder="WASTAGE" class="form-control required" value="<?php echo $wastage; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /WASTAGE -->

                            <div class="col-sm-6 col-md-6"> <!-- TRIM -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-trash-o"></i></div>
                                    <input name="trim" title="TRIM" data-type="decimal" type="text" placeholder="TRIM" class="form-control required" value="<?php echo $trim; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /TRIM -->

                            <div class="col-sm-6 col-md-6"> <!-- DIRECTION -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-trash-o"></i></div>
                                    <input name="direction" title="DIRECTION" type="text" placeholder="DIRECTION" class="form-control" value="<?php echo $direction; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /TRIM -->

                            <div class="col-sm-12"> <!-- REMARKS -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
                                    <input title="Remarks if any" name="remarks" type="text" placeholder="REMARKS" class="form-control" value="<?php echo $remarks; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /REMARKS -->

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    

    #inputTable tr th:nth-child(3){
        width: 50px;
    }
    #inputTable tr td:nth-child(3){
        padding: 0px 0 0 9px;
        margin-bottom: -5px;
    }
    #inputTable #fancy-checkbox + label {
        margin-top: 1px;
        margin-bottom: -3px;
    }
    #inputTable tr th:nth-child(1),
    #inputTable tr th:nth-child(2),
    #inputTable tr th:nth-child(3),
    #inputTable tr th:nth-child(4),
    #inputTable tr th:nth-child(5) {
        width: 150px;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });


</script>



<?php 
else:
    echo not_permitted();
endif; 
?>