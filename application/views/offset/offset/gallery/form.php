

<?php
$this->load->view('offset/partials/header');
 if (User_Model::hasAccess($record->id?'editOffsetMaterial' : 'addOffsetMaterial')): ?>

<div class="page-title">
    <div class="title_left">
        <h3><?php echo isset($page_title)?$page_title:''; ?></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Gallery <small>add new Gallery for job</small></h2>
        <?php if ($record->id): ?>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="delete" href="<?php echo site_url( "offset/category/delete/$record->id" ); ?>"><i class="fa fa-close confirm" title="Delete item"></i></a></li>
                <!-- <li><a class="close-link"><i class="fa fa-close confirm" title="Delete item"></i></a></li> -->
            </ul>
        <?php endif ?>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="demo-form2" data-parsley-validate="" action="<?php echo site_url( "gallery/save/offset_structure/$context_id" ); ?>" class="form-horizontal form-label-left" novalidate="" method="POST">

            <?php display_input(['name'=>'name', 'value'=>$record->name], 'Gallery Name/Title'); ?>

            <?php display_textarea(['name'=>'description', 'value'=>$record->description], 'Description'); ?>

            <?php if (!$record->id): ?>
            <div class="form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12">Choose Files</label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="file" class="form-control" id="imageFile" name="upl_files[]" multiple>
                </div>
            </div>
            <?php endif ?>

            <div class="ln_solid"></div>

            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a href="<?php echo site_url( 'offset/category' ); ?>" class="btn btn-primary">Cancel</a>
                    <button type="submit" class="btn btn-success" name="submit">Submit</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>



<?php 
else:
    echo not_permitted();
endif; 

$this->load->view('offset/partials/footer'); ?>