<div class="x_panel">
    <div class="x_title">
        <h2>Die cutting details</h2>
        <div class="clearfix"></div>
    </div> <!-- .x_title -->
    <div class="x_content">
        <br> 
        <?php $colors = ['None'=>'None', 'New Manual'=>'New Manual', 'Old Manual'=>'Old Manual', 'New Automatic'=>'New Automatic', 'Old Automatic'=>'Old Automatic'];
            default_dropdown(['name'=>'die_cutting_type', 'options'=> $colors, 'value'=>$record->die_cutting_type, 'empty'=>false], 'Die type'); ?>

        <?php $emboss = ['Emboss(new)'=>'Emboss(new)', 'Emboss(old)'=>'Emboss(old)'];
            default_dropdown(['name'=>'emboss', 'options'=> $emboss, 'value'=>$record->emboss, 'placeholder'=>'Select Emboss type'], 'Emboss'); ?>
        
        <?php display_textarea(['name'=>'die_remarks', 'value'=>$record->die_remarks], 'Remarks'); ?>
        
    </div> <!-- .x_content -->
</div> <!-- .x_panel -->

