<?php $this->load->view('offset/partials/header');

if (User_Model::hasAccess($record->id? 'editOffsetJobStructure':'addOffsetJobStructure')): ?>

<div class="container-fluid">
<div class="row">
    <div class="page-title">
        <div class="title_left">
            <h3><?php echo isset($page_title)?$page_title:''; ?></h3>
        </div>

        <div class="title_right">
            <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">Go!</button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<form id="demo-form2" data-parsley-validate="" action="<?php echo site_url( "offset/$controller/save/$record->id" ); ?>" class="form-horizontal form-label-left" novalidate="" method="POST">
    <div class="row">
    <div class="grid">
        <div class="col-md-6 col-xs-12 grid-item">
            
            <?php $this->load->view('offset/offset/structure/form_partial_print'); ?>

        </div> <!-- .col-md-6.col-xs-12 -->
        <div class="col-md-6 col-xs-12 grid-item">
            
            <?php $this->load->view('offset/offset/structure/form_partial_board'); ?>

        </div> <!-- .col-md-6.col-xs-12 -->
        <div class="col-md-6 col-xs-12 grid-item">
            
            <?php $this->load->view('offset/offset/structure/form_partial_corrugations'); ?>

        </div> <!-- .col-md-6.col-xs-12 -->
        <div class="col-md-6 col-xs-12 grid-item">
            
            <?php $this->load->view('offset/offset/structure/form_partial_die_cutting'); ?>

        </div> <!-- .col-md-6.col-xs-12 -->
        <div class="col-md-6 col-xs-12 grid-item">
            
            <?php $this->load->view('offset/offset/structure/form_partial_pasting'); ?>

        </div> <!-- .col-md-6.col-xs-12 -->
        <div class="col-md-6 col-xs-12 grid-item">
            
            <div class="x_panel">
                <div class="x_title">
                    <h2>Dispatch details</h2>
                    <div class="clearfix"></div>
                </div> <!-- .x_title -->
                <div class="x_content">
                    <br>
                    <?php display_input(['name'=>'packing_qty', 'value'=>$record->packing_qty], 'Quantity to be Packed'); ?>
                </div> <!-- .x_content -->
            </div> <!-- .x_panel -->

        </div> <!-- .col-md-6.col-xs-12 -->

        <div class="col-xs-12 grid-item">
            
            <div class="x_panel">
                <div class="x_title">
                    <h2>Corrections details</h2>
                    <div class="clearfix"></div>
                </div> <!-- .x_title -->
                <div class="x_content">
                    <br>
                    <?php display_textarea(['name'=>'correction', 'value'=>$record->correction], 'Corrections'); ?>
                    <div class="ln_solid"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <a href="<?php echo site_url( "offset/$controller" ); ?>" class="btn btn-primary">Cancel</a>
                            <button type="submit" class="btn btn-success" name="submit">Save</button>
                            <?php if (!$record->id): ?>
                            <label>
                                <?php $data = ['name'=>'reference_type', 'value'=>1, 'checked'=>set_checkbox('reference_type', 1)]; ?>
                                <?php echo form_checkbox($data, '', false, 'class="js-switch"'); ?> New serial?
                                <!-- <input type="checkbox" class="js-switch" checked /> -->
                            </label>
                            <?php endif ?>
                        </div>
                    </div>
                </div> <!-- .x_content -->
            </div> <!-- .x_panel -->

        </div> <!-- .col-md-6.col-xs-12 -->

    </div> <!-- .grid -->
    </div> <!-- .row -->
</form>

<?php 
else:
    echo not_permitted();
endif; 

$this->load->view('offset/partials/footer'); ?>

<script>
$('[name=foil_color]').on('change', function(e){
    var color = $(this).val();
    if( color == "None" ){
        $('[name="data[foil]"]').val('').parents('.form-group').show();
    }else{
        $('[name="data[foil]"]').val(color).parents('.form-group').hide();
    }
}).trigger('change');
</script>