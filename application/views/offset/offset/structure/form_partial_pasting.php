<div class="x_panel">
    <div class="x_title">
        <h2>Pasting details</h2>
        <div class="clearfix"></div>
    </div> <!-- .x_title -->
    <div class="x_content">
        <br>
        <?php $pasting_type = ['None'=>'None', 'Hand Pasting'=>'Hand Pasting', 'Machine Pasting'=>'Machine Pasting'];
            default_dropdown(['name'=>'pasting_type', 'options'=> $pasting_type, 'value'=>$record->pasting_type, 'empty'=>false], 'Pasting type'); ?>

        <?php display_textarea(['name'=>'pasting_remarks', 'value'=>$record->pasting_remarks], 'Remarks'); ?>
        
    </div> <!-- .x_content -->
</div> <!-- .x_panel -->