<div class="x_panel">
    <div class="x_title">
        <h2>General/Print details</h2>
        <?php if ($record->id): ?>
        <ul class="nav navbar-right panel_toolbox">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                    <?php if ($record->active): ?>
                        <li><a class="inactivate" href="<?php echo site_url( "offset/$controller/change_activation/$record->id/0" ); ?>"> Inactivate</a></li>
                    <?php else: ?>
                        <li><a class="activate" href="<?php echo site_url( "offset/$controller/change_activation/$record->id/1" ); ?>"> Activate</a></li>
                    <?php endif; ?>
                </ul>
            </li>
            <li><a class="delete" href="<?php echo site_url( "offset/$controller/delete/$record->id" ); ?>"><i class="fa fa-close confirm" title="Delete item"></i></a></li>
            <!-- <li><a class="close-link"><i class="fa fa-close confirm" title="Delete item"></i></a></li> -->
        </ul>
        <?php endif ?>
        <div class="clearfix"></div>
    </div> <!-- .x_title -->
    <div class="x_content">
        <br>
        <?php display_input(['name'=>'job_name', 'value'=>$record->job_name], 'Job name'); ?>
        
        <?php display_switcher(['name'=>'data[c]', 'value'=>1, 'checked'=>set_checkbox('data[c]', 1, ($record->c == 1) )], "Cyan", "Colors") ?>
        <?php display_switcher(['name'=>'data[m]', 'value'=>2, 'checked'=>set_checkbox('data[m]', 2, ($record->m == 2) )], "Megenta") ?>
        <?php display_switcher(['name'=>'data[y]', 'value'=>3, 'checked'=>set_checkbox('data[y]', 3, ($record->y == 3) )], "Yellow") ?>
        <?php display_switcher(['name'=>'data[k]', 'value'=>4, 'checked'=>set_checkbox('data[k]', 4, ($record->k == 4) )], "Black") ?>
        
        <?php display_input(['name'=>'spot1', 'value'=>$record->spot1], 'Spot 1'); ?>
        <?php display_input(['name'=>'spot2', 'value'=>$record->spot2], 'Spot 2'); ?>
        <?php display_input(['name'=>'spot3', 'value'=>$record->spot3], 'Spot 3'); ?>

    </div> <!-- .x_content -->
</div> <!-- .x_panel -->