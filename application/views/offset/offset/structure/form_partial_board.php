<div class="x_panel">
    <div class="x_title">
        <h2>Board details</h2>
        <div class="clearfix"></div>
    </div> <!-- .x_title -->
    <div class="x_content">
        <br>
        
        <?php display_input(['name'=>'board_width', 'value'=>$record->board_width], 'Board Width'); ?>
        
        <?php display_input(['name'=>'board_height', 'value'=>$record->board_height], 'Board Height'); ?>
        
        <?php $plain_size = ['w'=>'Width', 'h'=>'Height'];
            default_dropdown(['name'=>'grain_size', 'options'=> $plain_size, 'value'=>$record->grain_size, 'empty'=>false], 'Grain Size'); ?>

        <?php display_input(['name'=>'gsm', 'value'=>$record->gsm], 'GSM'); ?>
        
        <?php display_input(['name'=>'no_ups', 'value'=>$record->no_ups], 'No Ups'); ?>
        
        <?php display_input(['name'=>'material_id', 'value'=>$record->material_id], 'Material'); ?>
        
        <?php $sheet_type = ['paper'=>'Paper', 'board'=>'Board'];
            default_dropdown(['name'=>'sheet_type', 'options'=> $sheet_type, 'value'=>$record->sheet_type, 'empty'=>false], 'Sheet type'); ?>
      
        <hr>
        <?php display_switcher(['name'=>'data[water_gloss]', 'value'=>1, 'checked'=>set_checkbox('data[water_gloss]', 1, ($record->water_gloss == 1))], "Gloss", "Water Base") ?>

        <?php display_switcher(['name'=>'data[water_matt]', 'value'=>1, 'checked'=>set_checkbox('data[water_matt]', 1,($record->water_matt == 1))], "Matt") ?>
        <hr>
        <?php display_switcher(['name'=>'data[uv_gloss]', 'value'=>1, 'checked'=>set_checkbox('data[uv_gloss]', 1, ($record->uv_gloss == 1))], "Gloss", "U.V") ?>

        <?php display_switcher(['name'=>'data[uv_matt]', 'value'=>1, 'checked'=>set_checkbox('data[uv_matt]', 1, ($record->uv_matt == 1))], "Matt") ?>
      
        <?php display_switcher(['name'=>'data[uv_spot]', 'value'=>1, 'checked'=>set_checkbox('data[uv_spot]', 1, ($record->uv_spot == 1))], "Spot") ?>
        <hr>
        <?php display_switcher(['name'=>'data[lamination_both]', 'value'=>1, 'checked'=>set_checkbox('data[lamination_both]', 1, ($record->lamination_both == 1) )], "Both side lamination", "Lanimation") ?>

        <?php display_switcher(['name'=>'data[lamination_gloss]', 'value'=>1, 'checked'=>set_checkbox('data[lamination_gloss]', 1, ($record->lamination_gloss == 1) )], "Gloss") ?>
        
        <?php display_switcher(['name'=>'data[lamination_matt]', 'value'=>1, 'checked'=>set_checkbox('data[lamination_matt]', 1, ($record->lamination_matt == 1) )], "Matt") ?>

        <?php $lam_type = ['-'=>'None', 'cold'=>'Cold', 'hot'=>'Hot'];
            default_dropdown(['name'=>'lamination_type', 'options'=> $lam_type, 'value'=>$record->lamination_type, 'empty'=>false], 'Type'); ?>

        <hr>
        <?php display_switcher(['name'=>'data[warnish_gloss]', 'value'=>1, 'checked'=>set_checkbox('data[warnish_gloss]', 1, ($record->warnish_gloss == 1) )], "Gloss", "Warnish") ?>
        
        <?php display_switcher(['name'=>'data[warnish_matt]', 'value'=>1, 'checked'=>set_checkbox('data[warnish_matt]', 1, ($record->warnish_matt == 1) )], "Matt") ?>
        <hr>

        <?php $foil_type = ['-'=>'None', 'cold'=>'Cold', 'hot'=>'Hot'];
            default_dropdown(['name'=>'foil_type', 'options'=> $foil_type, 'value'=>$record->foil_type, 'empty'=>false], 'Foil'); ?>
        
        <?php $colors = ['None'=>'Custom color', 'Silver'=>'Silver', 'Golden'=>'Golden'];
            default_dropdown(['name'=>'foil', 'options'=> $colors, 'value'=>$record->foil, 'placeholder'=>'Select Color', 'empty'=>false], 'Color'); ?>

        <?php display_input(['name'=>'foil', 'value'=>$record->foil], 'Other color'); ?>
        
    </div> <!-- .x_content -->
</div> <!-- .x_panel -->