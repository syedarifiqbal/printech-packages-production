<div class="x_panel">
    <div class="x_title">
        <h2>Corrugations details</h2>
        <div class="clearfix"></div>
    </div> <!-- .x_title -->
    <div class="x_content">
        <br>
        <?php display_input(['name'=>'flutting', 'value'=>$record->flutting], 'Flutting'); ?>
      
        <?php $plies = ['0'=>'No ply', '3'=>'3 ply', '5'=>'5 ply', '7'=>'7 ply'];
            default_dropdown(['name'=>'ply', 'options'=> $plies, 'value'=>$record->ply, 'placeholder'=>'Select no of plies', 'empty'=>false], 'No of plies'); ?>
      
        <?php $plies = ['none'=>'None', 'Single'=>'Single', 'Double(same)'=>'Double(same)', 'Double(different)'=>'Double(different)'];
            default_dropdown(['name'=>'panel', 'options'=> $plies, 'value'=>$record->panel, 'empty'=>false], 'Panel'); ?>

        <?php display_input(['name'=>'roll_size', 'value'=>$record->roll_size], 'Roll Size'); ?>
        
        <?php display_input(['name'=>'cutting_size', 'value'=>$record->cutting_size], 'Cutting Size'); ?>
        
        <?php display_textarea(['name'=>'corrugation_remarks', 'value'=>$record->corrugation_remarks], 'Remarks'); ?>
        
    </div> <!-- .x_content -->
</div> <!-- .x_panel -->