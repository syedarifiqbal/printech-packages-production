<?php
$this->load->view('offset/partials/header');
 if (User_Model::hasAccess('addOffsetCategory')): ?>

<div class="page-title">
    <div class="title_left">
        <h3><?php echo isset($page_title)?$page_title:''; ?></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2><?php echo anchor(site_url('offset/structure/save'), '<i class="fa fa-plus"></i>'); ?> <?php echo isset($table_title)?$table_title:''; ?></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Settings 1</a></li>
                            <li><a href="#">Settings 2</a></li>
                        </ul>
                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div> <!-- .x_title -->

            <div class="x_content">
                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                    <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="false">Active master file</a></li>
                        <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Inactive master file</a></li>
                    </ul>
                    <div id="myTabContent" class="tab-content">
                        <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                            <input type="hidden" id="ajax-url" value="<?php echo site_url( "offset/structure/list_json/" ); ?>">
                            <input type="hidden" id="" value="">
                            <table id="active_datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Job Name</th>
                                        <th>Material</th>
                                        <th>Board size</th>
                                        <th>GSM</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                            <tbody>
                            </tbody>
                            </table>
                        </div> <!-- .tab-pane -->
                        <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                            <table id="inactive_datatable" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Job Name</th>
                                        <th>Material</th>
                                        <th>Board size</th>
                                        <th>GSM</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div> <!-- .tab-pane -->
                    </div> <!-- .tab-content -->
                </div> <!-- togglable-tabs -->
            </div> <!-- .x_content -->
        </div> <!-- .x_panel -->
    </div> <!-- .col -->
</div> <!-- .row -->


<!-- Default bootstrap modal example -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
            
                <iframe src="#" frameborder="0" id="frame" width="100%"></iframe>
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
    

<?php 
else:
    echo not_permitted();
endif; 

$this->load->view('offset/partials/footer'); ?>

<script>

    $("#myModal").on("show.bs.modal", function(e) {
        var $link = $(e.relatedTarget);
        $('#myModalLabel').text( $link.parents('tr').find('td:eq(1)').text() );
        $(this).find("#frame").attr('src', $link.attr("href"));
        $(document).trigger('resize');
    });

</script>