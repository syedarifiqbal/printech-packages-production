<?php $this->load->view('offset/partials/header');

 if (User_Model::hasAccess($record->job_code? 'editOffsetSaleOrder':'addOffsetSaleOrder')): ?>

<div class="page-title">
    <div class="title_left">
        <h3><?php echo isset($page_title)?$page_title:''; ?></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Sale Order <small>add new sale order</small></h2>
        <?php if ($record->job_code): ?>
            <ul class="nav navbar-right panel_toolbox">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <?php if ($record->active): ?>
                            <li><a class="inactivate" href="<?php echo site_url( "offset/sale_order/change_activation/$record->job_code/0" ); ?>"> Inactivate</a></li>
                        <?php else: ?>
                            <li><a class="activate" href="<?php echo site_url( "offset/sale_order/change_activation/$record->job_code/1" ); ?>"> Activate</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
                <li><a class="delete" href="<?php echo site_url( "offset/sale_order/delete/$record->job_code" ); ?>"><i class="fa fa-close confirm" title="Delete item"></i></a></li>
                <!-- <li><a class="close-link"><i class="fa fa-close confirm" title="Delete item"></i></a></li> -->
            </ul>
        <?php endif ?>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="demo-form2" data-parsley-validate="" action="<?php echo site_url( "offset/sale_order/save/$record->job_code" ); ?>" class="form-horizontal form-label-left" novalidate="" method="POST">

            <?php 
            
            display_input(['name'=>'job_code', 'value'=>$job_code, 'disabled'=>'disabled="disabled"'], 'Job code');
            
            display_input(['name'=>'date', 'value'=>pkDate($record->date)??'', 'readonly'=> 'readonly="readonly"', 'datepicker'=>'datepicker="datepicker"'], 'Date');
            
            display_input(['name'=>'po_date', 'value'=>pkDate($record->po_date)??'', 'readonly'=> 'readonly="readonly"', 'datepicker'=>'datepicker="datepicker"'], 'PO Date');
            
            display_input(['name'=>'delivery_date', 'value'=>pkDate($record->delivery_date)??'', 'readonly'=> 'readonly="readonly"', 'datepicker'=>'datepicker="datepicker"'], 'Delivery Date');
            
            display_input(['name'=>'po_num', 'value'=>$record->po_num], 'PO Number');

            default_dropdown(['name'=>'data[customer_id]', 'options'=>$selected_customer, 'url'=> site_url('shared/api/customers/'), 'value'=>$record->customer_id, 'placeholder'=>'Select Customer', 'prefix'=>false], 'Customer');
            // default_dropdown(['name'=>'data[customer_id]', 'options'=> $customers, 'value'=>$record->customer_id, 'placeholder'=>'Select Customer', 'prefix'=>false, 'empty'=>false], 'Customer');

            default_dropdown(['name'=>'data[structure_id]', 'options'=>$selected_structure, 'url'=> site_url('shared/api/structures/'), 'value'=>$record->structure_id, 'placeholder'=>'Select Structure', 'prefix'=>false], 'Structure');
            // default_dropdown(['name'=>'data[structure_id]', 'options'=> $structures, 'value'=>$record->structure_id, 'placeholder'=>'Select Structure', 'prefix'=>false, 'empty'=>false], 'Structure');

            $options = [ 'kg'=>'KG', 'pcs'=>'PCS' ];
            default_dropdown(['name'=>'order_type', 'options'=> $options, 'value'=>$record->order_type, 'placeholder'=>'Select Order Type'], 'Order Type');

            display_input(['name'=>'quantity', 'value'=>$record->quantity], 'Order Quantity');

            display_input(['name'=>'rate', 'value'=>$record->rate], 'Rate');

            display_input(['name'=>'excess_quantity', 'value'=>$record->excess_quantity], 'Excess Quantity');

            display_textarea(['name'=>'remarks', 'value'=>$record->	remarks], 'Remarks');

            display_switcher(['name'=>'data[hold]', 'value'=>1, 'checked'=>set_checkbox('data[hold]', 1, ($record->hold == 1) )], "", "Hold");

            display_switcher(['name'=>'data[completed]', 'value'=>1, 'checked'=>set_checkbox('data[completed]', 1, ($record->completed == 1) )], "", "Completed");

            ?>
            <div class="ln_solid"></div>

            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a href="<?php echo site_url( 'offset/offset_sale_order' ); ?>" class="btn btn-primary">Cancel</a>
                    <button type="submit" class="btn btn-success" name="submit">Submit</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>



<?php 
else:
    echo not_permitted();
endif; 

$this->load->view('offset/partials/footer'); ?>