<?php $this->load->view('offset/partials/header');

if (User_Model::hasAccess($record->id? 'editOffsetDC':'addOffsetDC')): ?>

<div class="page-title">
    <div class="title_left">
        <h3><?php echo isset($page_title)?$page_title:''; ?></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Delivery Challan <small>add new Delivery Challan</small></h2>
        <?php if ($record->id): ?>
            <ul class="nav navbar-right panel_toolbox">
                <li><a class="delete" href="<?php echo site_url( "offset/dispatch/delete/$record->id" ); ?>"><i class="fa fa-close confirm" title="Delete item"></i></a></li>
                <!-- <li><a class="close-link"><i class="fa fa-close confirm" title="Delete item"></i></a></li> -->
            </ul>
        <?php endif ?>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="demo-form2" data-parsley-validate="" action="<?php echo site_url( "offset/dispatch/save/$record->id" ); ?>" class="form-horizontal form-label-left" novalidate="" method="POST">

            <?php 
            
            display_input(['name'=>'date', 'value'=>pkDate($record->date)??'', 'readonly'=> 'readonly="readonly"', 'datepicker'=>'datepicker="datepicker"'], 'Date');
            
            // default_dropdown(['name'=>'data[job_code]', 'options'=>$selected_customer, 'url'=> site_url('shared/api/sale_order/'), 'value'=>$record->job_code, 'placeholder'=>'Select Job', 'prefix'=>false], 'Select Job');
            // default_dropdown(['name'=>'data[customer_id]', 'options'=> $customers, 'value'=>$record->customer_id, 'placeholder'=>'Select Customer', 'prefix'=>false, 'empty'=>false], 'Customer');
        
            // display_textarea(['name'=>'description', 'value'=>$record->description], 'Description');
            
            display_input(['name'=>'challan_no', 'value'=>$record->challan_no], 'Challan No');

            display_input(['name'=>'gatepass_no', 'value'=>$record->gatepass_no], 'Gatepass No');
            
            // display_textarea(['name'=>'description', 'value'=>$record->description], 'Description');
            
            // display_switcher(['name'=>'data[hold]', 'value'=>1, 'checked'=>set_checkbox('data[hold]', 1, ($record->hold == 1) )], "", "Hold");
            
            // display_switcher(['name'=>'data[completed]', 'value'=>1, 'checked'=>set_checkbox('data[completed]', 1, ($record->completed == 1) )], "", "Completed");
            
            ?>
            
            <input type="hidden" name="data[weight]" id="weightField" value="<?php echo set_value('data[weight]', $record->weight)?>">
            <input type="hidden" name="data[no_pcs]" id="nopcsField" value="<?php echo set_value('data[no_pcs]', $record->no_pcs)?>">
            <input type="hidden" name="data[no_cartons]" id="nocartonsField" value="<?php echo set_value('data[no_cartons]', $record->no_cartons)?>">

            <div class="table-responsive">
                <div class="row">
                    
                </div>
                <?php $this->load->view('offset/offset/dispatch/form-table'); ?>
                
            </div>

            <div class="ln_solid"></div>

            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a href="<?php echo site_url( 'offset/dispatch' ); ?>" class="btn btn-primary">Cancel</a>
                    <button type="submit" class="btn btn-success" name="submit">Save</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>



<?php 
else:
    echo not_permitted();
endif; 

$this->load->view('offset/partials/footer'); ?>

<script>
    
function initSelect2($element) {
    $element.select2({
        minimumInputLength: 3,
        ajax: {
            url: '<?php echo site_url('shared/api/sale_orders/'); ?>',
            dataType: 'json',
            type: "GET",
            processResults: function (data) {
                return {
                    results : data
                }
            }
        },    
    }).on('change',function(){
        $(this).prev().val($(this).text())
        // console.log($(this).val(), $(this).text());
    });
}

var i=<?php echo $this->input->post('items')? count($this->input->post('items')): count($selected_jobs); ?>;

$('#addEntry').on('click', function(e){
    e.preventDefault();
    i++;
    var row = '<tr>' +
                '<td>' +
                    '<input type="hidden" name="items['+i+'][job_name]" class="form-control">'+
                    '<select name="items['+i+'][job_code]" class="form-control job"></select>'+
                '</td>'+
                '<td><input type="number" name="items['+i+'][weight]" class="form-control"></td>'+
                '<td><input type="number" name="items['+i+'][pcs]" class="form-control"></td>'+
                '<td><input type="text" name="items['+i+'][cartons]" class="form-control"></td>'+
            '</tr>'+
            '<tr>'+
                '<td colspan="4">'+
                    '<textarea name="items['+i+'][description]" id="description" rows="2" class="form-control" placeholder="Description..."></textarea>'+
                '</td>'+
            '</tr>';
    row = $(row);
    $('#itemDetails tbody').append(row);
    initSelect2(row.find('.job'));
});

initSelect2($('.job'));


$('#itemDetails').on('change', 'input', function(){
    var weight = 0, pcs = 0, cartons = 0;
    $('#itemDetails tr').each(function (ind, el) {
        if($(el).find('td:eq(1) input').val()){
            weight      += parseFloat($(el).find('td:eq(1) input').val());
            pcs         += parseInt($(el).find('td:eq(2) input').val());
            cartons     += parseInt($(el).find('td:eq(3) input').val());
        };
    });
    $('#weightField').val(weight);
    $('#nopcsField').val(pcs);
    $('#nocartonsField').val(cartons);
});

</script>