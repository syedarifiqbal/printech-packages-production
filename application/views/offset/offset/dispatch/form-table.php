<table class="table table-striped table-bordered table-hover"  id="itemDetails">  

<thead>
    <tr>
        <th>Choose Job</th>
        <th>No. of Cartons</th>
        <th>No. of PCS</th>
        <th>Weight
            <div class="pull-right">
                <div class="btn-group btn-group-sm">
                    <button type="button" class="btn btn-danger" id="deleteEntry"><i class="fa fa-minus"></i> </button>
                    <button type="button" class="btn btn-info" id="addEntry"><i class="fa fa-plus"></i> </button>
                </div>
            </div>
        </th>
    </tr>
</thead>

<tbody>
    <?php 

    $counter = 0;
    // Check if user submitted the for than populate inputs from posted data
    if($this->input->post('items')):
        foreach ($this->input->post('items') as $key => $item): ?>
            <tr>
                <td>
                    <input type="hidden" name="items[<?php echo $key; ?>][job_name]" class="form-control" value="<?php echo $item['job_name']; ?>">
                    <select name="items[<?php echo $key; ?>][job_code]" class="form-control job">
                        <?php if(isset($item['job_code'])): ?>
                            <option value="<?php echo $item['job_code']; ?>" selected><?php echo $item['job_name']; ?></option>
                        <?php endif; ?>
                    </select>
                </td>
                <td><input type="number" name="items[<?php echo $key; ?>][weight]" class="form-control" value="<?php echo $item['weight']; ?>"></td>
                <td><input type="number" name="items[<?php echo $key; ?>][pcs]" class="form-control" value="<?php echo $item['pcs']; ?>"></td>
                <td><input type="text" name="items[<?php echo $key; ?>][cartons]" class="form-control" value="<?php echo $item['cartons']; ?>"></td>
            </tr>
            <tr>
                <td colspan="4">
                    <textarea name="items[<?php echo $key; ?>][description]" id="description" rows="2" class="form-control" placeholder="Description..."><?php echo $item['description']; ?></textarea>
                </td>
            </tr>
        <?php endforeach;

    // Check if your trying to update existing record and get data from selected DC id
    elseif(!empty($selected_jobs)):

        foreach ($selected_jobs as $item): ?>
            <tr>
                <td>
                    <input type="hidden" name="items[<?php echo $counter; ?>][job_name]" class="form-control" value="<?php echo $item->job_name; ?>">
                    <select name="items[<?php echo $counter; ?>][job_code]" class="form-control job">
                        <option value="<?php echo $item->job_code; ?>" selected><?php echo $item->job_name; ?></option>
                    </select>
                </td>
                <td><input type="number" name="items[<?php echo $counter; ?>][weight]" class="form-control" value="<?php echo $item->weight; ?>"></td>
                <td><input type="number" name="items[<?php echo $counter; ?>][pcs]" class="form-control" value="<?php echo $item->pcs; ?>"></td>
                <td><input type="text" name="items[<?php echo $counter; ?>][cartons]" class="form-control" value="<?php echo $item->cartons; ?>"></td>
            </tr>
            <tr>
                <td colspan="4">
                    <textarea name="items[<?php echo $counter; ?>][description]" id="description" rows="2" class="form-control" placeholder="Description..."><?php echo $item->description; ?></textarea>
                </td>
            </tr>

        <?php 
        $counter++;
        endforeach;

    // If user not submitted the form nor editing the for than it mean it is new entry
    else: ?>

        <tr>
            <td>
                <input type="hidden" name="items[0][job_name]" class="form-control">
                <select name="items[0][job_code]" class="form-control job"></select>
            </td>
            <td><input type="number" name="items[0][weight]" class="form-control"></td>
            <td><input type="number" name="items[0][pcs]" class="form-control"></td>
            <td><input type="text" name="items[0][cartons]" class="form-control"></td>
        </tr>
        <tr>
            <td colspan="4">
                <textarea name="items[0][description]" id="description" rows="2" class="form-control" placeholder="Description..."></textarea>
            </td>
        </tr>

    <?php endif; ?>

</tbody>

</table>