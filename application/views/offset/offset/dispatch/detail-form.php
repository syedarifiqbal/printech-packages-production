<?php $this->load->view('offset/partials/header');

 if (User_Model::hasAccess($record->job_code? 'editOffsetSaleOrder':'addOffsetSaleOrder')): ?>

<div class="page-title">
    <div class="title_left">
        <h3><?php echo isset($page_title)?$page_title:''; ?></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo $site_title; ?></h2>
    
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="demo-form2" data-parsley-validate="" action="<?php echo site_url("offset/sale_order/add_wastage/$job_code/$record->id"); ?>" class="form-horizontal form-label-left" novalidate="" method="POST">

            <div class="row">
                <div class="col-sm-6">
                    <legend class="text-center header">CONVENTIONAL</legend>
                    <?php 
                    display_input(['name'=>'job_code', 'value'=>$job_code, 'readonly'=>'readonly="readonly"'], 'Job code', true);

                    display_input(['name'=>'dispatch_date', 'value'=>pkDate($record->dispatch_date)??'', 'readonly'=> 'readonly="readonly"', 'datepicker'=>'datepicker="datepicker"'], 'Date', true);
                    
                    display_input(['name'=>'con_no_sheet', 'value'=>$record->con_no_sheet], 'No Sheet', true);
                    
                    display_input(['name'=>'con_pollar', 'value'=>$record->con_pollar], 'Pollar', true);
                    
                    display_input(['name'=>'con_printing', 'value'=>$record->con_printing], 'Printing', true);
                    
                    display_input(['name'=>'con_lamination', 'value'=>$record->con_lamination], 'Lamination', true);

                    display_input(['name'=>'con_embossing', 'value'=>$record->con_embossing], 'Embossing', true);
                    
                    display_input(['name'=>'con_coating', 'value'=>$record->con_coating], 'Coating', true);
                    
                    display_input(['name'=>'con_die_cutting', 'value'=>$record->con_die_cutting], 'Die Cutting', true);
                    
                    display_input(['name'=>'con_pasting', 'value'=>$record->con_pasting], 'Pasting', true);
                    
                    display_input(['name'=>'con_dispatch', 'value'=>$record->con_dispatch], 'Dispatch', true);
                    
                    display_input(['name'=>'con_sorting', 'value'=>$record->con_sorting], 'Sorting', true);
                    
                    display_input(['name'=>'con_corrugation', 'value'=>$record->con_corrugation], 'Corrugation', true);
                    ?>

                </div>
                <!-- .col-sm-6 -->
                <div class="col-sm-6">
                    <legend class="text-center header">UV</legend>
                    <!-- <h2></h2> -->
                    <?php 
                    
                    display_input(['name'=>'uv_no_sheet', 'value'=>$record->uv_no_sheet], 'No Sheet', true);
                    
                    display_input(['name'=>'uv_pollar', 'value'=>$record->uv_pollar], 'Pollar', true);
                    
                    display_input(['name'=>'uv_printing', 'value'=>$record->uv_printing], 'Printing', true);
                    
                    display_input(['name'=>'uv_lamination', 'value'=>$record->uv_lamination], 'Lamination', true);
                    
                    display_input(['name'=>'uv_embossing', 'value'=>$record->uv_embossing], 'Embossing', true);
                    
                    display_input(['name'=>'uv_coating', 'value'=>$record->uv_coating], 'Coating', true);
                    
                    display_input(['name'=>'uv_die_cutting', 'value'=>$record->uv_die_cutting], 'Die Cutting', true);
                    
                    display_input(['name'=>'uv_pasting', 'value'=>$record->uv_pasting], 'Pasting', true);
                    
                    display_input(['name'=>'uv_dispatch', 'value'=>$record->uv_dispatch], 'Dispatch', true);
                    
                    display_input(['name'=>'uv_sorting', 'value'=>$record->uv_sorting], 'Sorting', true);
                    
                    display_input(['name'=>'uv_corrugation', 'value'=>$record->uv_corrugation], 'Corrugation', true);
                    ?>

                    <div class="ln_solid"></div>
        
                    <div class="text-center">
                        <div class="btn-group">
                            <a href="<?php echo site_url( 'offset/sale_order' ); ?>" class="btn btn-primary">Cancel</a>
                            <button type="submit" class="btn btn-success" name="submit">Save</button>
                            <?php if ($record->id): ?>
                            <a href="<?php echo site_url("offset/sale_order/production_detail/$record->id") ?>" class="btn btn-info"><i class="fa fa-file-pdf-o"></i> View</a>
                            <?php endif ?>
                        </div>
                    </div>

                </div>
                <!-- .col-sm-6 -->

            </div>
            <!-- .row -->
            
        </form>
      </div>
    </div>
  </div>
</div>



<?php 
else:
    echo not_permitted();
endif; 

$this->load->view('offset/partials/footer'); ?>