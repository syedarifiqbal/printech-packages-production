<?php
$this->load->view('offset/partials/header');
 if (User_Model::hasAccess('addOffsetCategory')): ?>

<div class="page-title">
    <div class="title_left">
        <h3><?php echo isset($page_title)?$page_title:''; ?></h3>
    </div>

    <div class="title_right">
        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search for...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Go!</button>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Material Category <small>add new category for materials</small></h2>
        <?php if ($record->category_id): ?>
            <ul class="nav navbar-right panel_toolbox">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                    <ul class="dropdown-menu" role="menu">
                        <?php if ($record->active): ?>
                            <li><a class="inactivate" href="<?php echo site_url( "offset/category/change_activation/$record->category_id/0" ); ?>"> Inactivate</a></li>
                        <?php else: ?>
                            <li><a class="activate" href="<?php echo site_url( "offset/category/change_activation/$record->category_id/1" ); ?>"> Activate</a></li>
                        <?php endif; ?>
                    </ul>
                </li>
                <li><a class="delete" href="<?php echo site_url( "offset/category/delete/$record->category_id" ); ?>"><i class="fa fa-close confirm" title="Delete item"></i></a></li>
                <!-- <li><a class="close-link"><i class="fa fa-close confirm" title="Delete item"></i></a></li> -->
            </ul>
        <?php endif ?>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br>
        <form id="demo-form2" data-parsley-validate="" action="<?php echo site_url( "offset/category/save/$record->category_id" ); ?>" class="form-horizontal form-label-left" novalidate="" method="POST">

            <?php display_input(['name'=>'category_name', 'value'=>$record->category_name], 'Category name'); ?>

            <?php display_textarea(['name'=>'description', 'value'=>$record->description], 'Description'); ?>

            <div class="ln_solid"></div>

            <div class="form-group">
                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <a href="<?php echo site_url( 'offset/category' ); ?>" class="btn btn-primary">Cancel</a>
                    <button type="submit" class="btn btn-success" name="submit">Submit</button>
                </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>



<?php 
else:
    echo not_permitted();
endif; 

$this->load->view('offset/partials/footer'); ?>