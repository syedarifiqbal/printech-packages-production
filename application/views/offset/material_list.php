<?php
if(!User_Model::hasAccess("viewOffsetMaterial")){
    $this->session->set_flashdata('userMsg', 'You do not have privilege to view material list.');
    echo not_permitted();
    exit();
}
?>
<style>
    table.datatable-col-9 th,
        table.datatable-col-9 td {
            white-space: nowrap;
        }

</style>

<!-- Tables -->
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Material List <i>filter and actions</i></h1>
            </div>
        </div>
        <div class="col-md-2">
            <?php if (User_Model::hasAccess("addRotoMaterial")): ?>
                <a href="<?php echo site_url('offset_material/add'); ?>" data-request="ajax" class="btn btn-sm btn-primary" title="Add New Material.">New Material</a>
            <?php endif ?>
        </div>
    </div>
</div>
<?php echo $feed_back ?>
<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Material List</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <table id="listTable" class="table table-striped table-bordered table-hover dt-responsive datatable-col-9">
                        <thead>
                            <tr>
                                <th>Code</th>
                                <th>Material Name</th>
                                <th>Category</th>
                                <th>Group</th>
                                <th>GSM</th>
                                <th>Width</th>
                                <th>Height</th>
                                <th>Action</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php foreach ($materials as $m): ?>
                                <tr>
                                    <td><?php echo $m->id; ?></td>
                                    <td><?php echo $m->name; ?></td>
                                    <td><?php echo $m->category_name; ?></td>
                                    <td><?php echo $m->group_name; ?></td>
                                    <td><?php echo $m->gsm; ?></td>
                                    <td><?php echo $m->width; ?></td>
                                    <td><?php echo $m->height; ?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-gear"> </i> &nbsp;
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                <li><a href="#" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                                <?php if (User_Model::hasAccess("editOffsetMaterial")): ?>
                                                    <li><a href="<?php echo site_url('offset_material/add/'.$m->id); ?>" data-request="ajax" data-action="edit"><i class="fa fa-pencil"></i> Edit</a></li>
                                                <?php endif ?>
                                                <?php if (User_Model::hasAccess("deleteOffsetMaterial")): ?>
                                                    <li><a href="<?php echo site_url('offset_material/delete/'.$m->id); ?>" data-request="ajax" data-action="delete"><i class="fa fa-trash"></i> Delete</a></li>
                                                <?php endif ?>
                                                <!-- <li><a href="#">Separated link</a></li> -->
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>

    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;

    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };

    $('.datatable-col-9, table.tddd').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "bAutoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "responsive": true,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-9'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                },
                fnDrawCallback : function (oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }

            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>