<?php
$role = (isset($update) && $update == true) ? 'editOffsetMaterial' : 'addOffsetMaterial';
$pageTitle = (isset($update) && $update == true) ? 'Update' : 'New';
$name = (isset($update) && $update == true) ? $material->name : '';
$unit = (isset($update) && $update == true) ? $material->unit : '';
$width = (isset($update) && $update == true) ? $material->width : '';
$height = (isset($update) && $update == true) ? $material->height : '';
$rate = (isset($update) && $update == true) ? $material->rate : '';
$gsm = (isset($update) && $update == true) ? $material->gsm : '';
$category_id = (isset($update) && $update == true) ? $material->category_id : '';
$group_id = (isset($update) && $update == true) ? $material->group_id : '';
$url = site_url('offset_material/add/');
$actionLink = (isset($update) && $update == true) ? $url . '/' . $material->id : $url;

if (User_Model::hasAccess($role)):
    ?>

    <?php echo $feed_back ?>

    <div class="container"> <!-- this is from here -->
        <div class="row">
            <div class="col-md-10">
                <div class="heading-sec" id="intro6">
                    <h1><?php echo $pageTitle; ?> <i>Material</i></h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <form id="formToSubmits" method="post" action="<?php echo $actionLink; ?>">
                <div class="col-md-6 col-md-offset-2">
                    <div class="widget-body custom-form">

                        <div class="form-group"><!-- PRODUCT NAME -->
                            <label for="material_name">Material Name</label>
                            <input type="text" name="material_name" class="form-control required" id="material_name"
                                   value="<?php echo $name ?>" placeholder="Material Name"/>
                        </div><!-- PRODUCT NAME -->

                        <div class="form-group">
                            <label for="unit">Unit</label>
                            <?php echo form_dropdown('unit', $units, $unit, 'class="form-control" id="unit"') ?>
                        </div>

                        <div class="form-group"><!-- PURCHASE PRICE -->
                            <label for="price">List Price</label>
                            <input type="text" name="price" class="form-control required" id="price"
                                   value="<?php echo $name ?>" placeholder="Purchase Price"/>
                        </div><!-- PURCHASE PRICE -->

                        <div class="form-group"><!-- GSM -->
                            <label for="GSM">GSM</label>
                            <input type="text" name="gsm" class="form-control required" id="GSM"
                                   value="<?php echo $gsm ?>" placeholder="GSM"/>
                        </div><!-- GSM -->

                        <div class="form-group"><!-- WIDTH -->
                            <label for="WIDTH">Width</label>
                            <input type="text" name="width" class="form-control required" id="WIDTH"
                                   value="<?php echo $width ?>" placeholder="WIDTH"/>
                        </div><!-- GSM -->

                        <div class="form-group"><!-- HEIGHT -->
                            <label for="HEIGHT">HEIGHT</label>
                            <input type="text" name="height" class="form-control required" id="HEIGHT"
                                   value="<?php echo $height ?>" placeholder="HEIGHT"/>
                        </div><!-- HEIGHT -->

                        <button type="submit" name="submit" class="btn green btn-primary">Save</button>
                        
                    </div>
                </div>

                <div class="col-md-2">

                    <div class="widget-body custom-form"><!-- Category widget -->
                        <label>Category</label>
                        <?php
                        foreach ($availableCategories as $category) {
                            echo sprintf('<div class="checkbox">
                                        <label><input type="checkbox" value="%s" %s name="categories[]"/>%s</label>
                                    </div>',
                                $category->category_id,
                                (in_array($category->category_id,$categories))?'checked':'',
                                ucfirst($category->category_name) );
                        }?>
                        <a href="<?php echo site_url('offset_category/add'); ?>" class="btn btn-primary btn-sm">Add Category</a>
                    </div><!-- Category widget -->

                    <div class="widget-body custom-form"><!-- Group widget -->
                        <label>Groups</label>
                        <?php
                        foreach ($availableGroups as $group) {
                            echo sprintf('<div class="checkbox">
                                        <label><input type="checkbox" value="%s" %s name="groups[]"/>%s</label>
                                    </div>',
                                $group->group_id,
                                (in_array($group->group_id,$groups))?'checked':'',
                                ucfirst($group->group_name) );
                        }?>
                        <a href="<?php echo site_url('offset_group/add'); ?>" class="btn btn-primary btn-sm">Add Group</a>
                    </div><!-- Group widget -->

                </div> <!-- Sidebar widget -->

            </form>
        </div>
    </div>


    <?php
else:
    echo not_permitted();
endif;
?>