<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Printech Packages | Offset</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/vendors/bootstrap/dist/css/bootstrap.min.css'); ?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('assets/vendors/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo base_url('assets/vendors/iCheck/skins/flat/green.css'); ?>" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="<?php echo base_url('assets/vendors/google-code-prettify/bin/prettify.min.css'); ?>" rel="stylesheet">
    <!-- Select2 -->
    <link href="<?php echo base_url('assets/vendors/select2/dist/css/select2.min.css'); ?>" rel="stylesheet">
    <!-- Switchery -->
    <link href="<?php echo base_url('assets/vendors/switchery/dist/switchery.min.css'); ?>" rel="stylesheet">
    <!-- starrr -->
    <link href="<?php echo base_url('assets/vendors/starrr/dist/starrr.css'); ?>" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="<?php echo base_url('assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css'); ?>" rel="stylesheet">
    <!-- PNotify -->
    <link href="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.buttons.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/pnotify/dist/pnotify.nonblock.css'); ?>" rel="stylesheet">
    <!-- Datatables -->
    <link href="<?php echo base_url('assets/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('assets/build/css/custom.min.css'); ?>" rel="stylesheet">
  </head>

  <body class="nav-md footer_fixed">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col menu_fixed mCustomScrollbar">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo site_url(); ?>" class="site_title"><i class="fa fa-paw"></i> <span>Printech Packages!</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
              <?php $pic = ($this->session->userdata('avater_path')) ? 'profile_img/' . $this->session->userdata('avater_path') : 'sign-in.jpg';
              if ($this->session->userdata('avater_path')) {
                  echo sprintf('<img src="%s" class="img-circle profile_img">', base_url('assets/img/' . $pic));
              } else {
                  $name = explode(' ', str_replace('_', ' ', $this->session->userdata('user_name')));
                  $abbr = '';
                  foreach ($name as $n) {
                      $abbr .= substr($n, 0, 1);
                  }
                  echo sprintf('<div class="pro-pic">%s</div>', $abbr);
              } ?>
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo ucwords($this->session->userdata('user_name')); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />
            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-random"></i> Sale Orders <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('offset/sale_order/save'); ?>">Add Sale Order</a></li>
                      <li><a href="<?php echo site_url('offset/sale_order/'); ?>">Sale Orders List</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-truck"></i> Dispatch <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('offset/dispatch/save'); ?>">New D.C</a></li>
                      <li><a href="<?php echo site_url('offset/dispatch/'); ?>">DC List</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
              <!-- <div class="menu_section">
                <h3>Reports</h3>
                <ul class="nav side-menu">
                    <li><a><i class="fa fa-bug"></i> Additional Pages <span class="fa fa-chevron-down"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="e_commerce.html">E-commerce</a></li>
                            <li><a href="projects.html">Projects</a></li>
                            <li><a href="project_detail.html">Project Detail</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="profile.html">Profile</a></li>
                        </ul>
                    </li>
                </ul>
              </div> -->
              <div class="menu_section">
                <h3>Masters</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-users"></i> Customers <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('offset/customers/save/'); ?>">Add Customers</a></li>
                      <li><a href="<?php echo site_url('offset/customers/'); ?>">Customers list</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-windows"></i> Categories <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('offset/category/save/'); ?>">Add Category</a></li>
                      <li><a href="<?php echo site_url('offset/category/'); ?>">Categories list</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-cube"></i> Groups <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="<?php echo site_url('offset/group/save'); ?>">Add Group</a></li>
                      <li><a href="<?php echo site_url('offset/group/'); ?>">Groups list</a></li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Materials <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo site_url('offset/material/save/'); ?>">Add Material</a>
                        <li><a href="<?php echo site_url('offset/material/'); ?>">Materials list</a>
                        </li>
                    </ul>
                  </li>
                  <li><a><i class="fa fa-sitemap"></i> Job Structures <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="<?php echo site_url('offset/structure/save/'); ?>">Add Structure</a>
                        <li><a href="<?php echo site_url('offset/structure/'); ?>">Structures list</a>
                        </li>
                    </ul>
                  </li>
                  <!-- <li><a href="javascript:void(0)"><i class="fa fa-laptop"></i> Landing Page <span class="label label-success pull-right">Coming Soon</span></a></li> -->
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <!-- <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div> -->
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="images/img.jpg" alt=""><?php echo ucwords($this->session->userdata('user_name')); ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <!-- <li><a href="javascript:;"> Profile</a></li>
                    <li>
                      <a href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                    </li>
                    <li><a href="javascript:;">Help</a></li> -->
                    <li><a href="<?php echo site_url('user/logout'); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>

                <?php if( $this->session->userdata('user_id') == 1 ): ?>
                <li role="presentation" class="dropdown">
                  <a href="<?php echo site_url('shared/settings/toggle_maintenance_model'); ?>" title="Turn on/off Maintenance Mode"  data-placement="bottom" class="dropdown-toggle info-number">
                    <?php if($this->maintenance_mode){ ?>
                      <i class="fa fa-toggle-on"></i>
                      <?php }else{ ?>
                        <i class="fa fa-toggle-off"></i>
                    <?php }?>
                    <span class="badge bg-green">M</span>
                  </a>
                </li>
                <?php endif; ?>

                <?php if ($this->session->userdata('section')): ?>
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-windows"></i> <?php echo ucfirst(get_current_department()); ?>
                    <!-- <span class="badge bg-green">6</span> -->
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                    <li>
                      <a href="<?php echo site_url('dashboard/set_section/roto'); ?>">
                        <span class="image"><i class="fa fa-print"></i> </span>
                        <span>
                          <span>Roto Gravure</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('dashboard/set_section/offset'); ?>">
                        <span class="image"><i class="fa fa-cubes"></i> </span>
                        <span>
                          <span>Offset Printing</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('dashboard/set_section/extrusion'); ?>">
                        <span class="image"><i class="fa fa-server"></i> </span>
                        <span>
                          <span>Extrusion</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('dashboard/set_section/marketing'); ?>">
                        <span class="image"><i class="fa fa-opencart"></i> </span>
                        <span>
                          <span>Marketing</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('dashboard/set_section/maintenance'); ?>">
                        <span class="image"><i class="fa fa-wrench"></i> </span>
                        <span>
                          <span>Maintenance</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('dashboard/set_section/hr'); ?>">
                        <span class="image"><i class="fa fa-users"></i> </span>
                        <span>
                          <span>Human Resource</span>
                        </span>
                      </a>
                    </li>
                    <li>
                      <a href="<?php echo site_url('dashboard/set_section/health_sefty'); ?>">
                        <span class="image"><i class="fa fa-medkit"></i> </span>
                        <span>
                          <span>Health and Safety</span>
                        </span>
                      </a>
                    </li>
                  </ul>
                </li>
                <?php endif;?>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <?php if( $this->session->flashdata('expireNotification') ) { ?>
            <style>
              #expireNotification {
                margin-top: 65px;
                margin-bottom: 0;
              }
            </style>  
            <?php 
              echo $this->session->flashdata('expireNotification'); 
            } ?>