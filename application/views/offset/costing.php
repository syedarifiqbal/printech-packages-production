<?php
$role = (isset($update) && $update == true) ? 'editOffsetCosting' : 'addOffsetCosting';
$pageTitle = (isset($update) && $update == true) ? 'Update' : 'New';
$id = (isset($update) && $update == true) ? $record->id : '';
$date = (isset($update) && $update == true) ? PKdate($record->date) : '';
$customer_id = (isset($update) && $update == true) ? $record->customer_id : '';
$customer_name = (isset($update) && $update == true) ? $record->customer_name : 'Select Customer';
$item_name = (isset($update) && $update == true) ? $record->item_name : '';
$moq = (isset($update) && $update == true) ? $record->moq : '';
$no_of_color = (isset($update) && $update == true) ? $record->no_of_color : '';
$rate_per_color = (isset($update) && $update == true) ? $record->rate_per_color : '';
$pcs_per_sheet = (isset($update) && $update == true) ? $record->pcs_per_sheet : '';
$sale_tax = (isset($update) && $update == true) ? $record->sale_tax : '';
$board_width = (isset($update) && $update == true) ? $record->board_width : '';
$board_height = (isset($update) && $update == true) ? $record->board_height : '';
$board_rate = (isset($update) && $update == true) ? $record->board_rate : '';
$no_unit_per_packet = (isset($update) && $update == true) ? $record->no_unit_per_packet : '';
$board_gsm = (isset($update) && $update == true) ? $record->board_gsm : '';
$wastage_percent = (isset($update) && $update == true) ? $record->wastage_percent : '';
$uv_width = (isset($update) && $update == true) ? $record->uv_width : '';
$uv_height = (isset($update) && $update == true) ? $record->uv_height : '';
$uv_rate = (isset($update) && $update == true) ? $record->uv_rate : '';
$lamination_width = (isset($update) && $update == true) ? $record->lamination_width : '';
$lamination_height = (isset($update) && $update == true) ? $record->lamination_height : '';
$lamination_rate = (isset($update) && $update == true) ? $record->lamination_rate : '';
$foil_rate = (isset($update) && $update == true) ? $record->foil_rate : '';
$cartage = (isset($update) && $update == true) ? $record->cartage : '';
$pcs_per_delivery = (isset($update) && $update == true) ? $record->pcs_per_delivery : '';
$margin = (isset($update) && $update == true) ? $record->margin : '';
$both_lamination_checked = (isset($update) && $update == true && $record->both_side_lamination == 1) ? ' checked' : '';
$show_both_amount_checked = (isset($update) && $update == true && $record->show_both_amount == 1) ? ' checked' : '';
$sorting_rate = (isset($update) && $update == true) ? $record->sorting_rate : '';
$die_cutting_rate = (isset($update) && $update == true) ? $record->die_cutting_rate : '';
$embossing_rate = (isset($update) && $update == true) ? $record->embossing_rate : '';
$pasting_rate = (isset($update) && $update == true) ? $record->pasting_rate : '';
$packing_rate = (isset($update) && $update == true) ? $record->packing_rate : '';
$pcs_per_carton = (isset($update) && $update == true) ? $record->pcs_per_carton : '';

$film_width = (isset($update) && $update == true) ? $record->film_width : '';
$film_height = (isset($update) && $update == true) ? $record->film_height : '';
$no_film_development = (isset($update) && $update == true) ? $record->no_film_development : '';
$film_rate = (isset($update) && $update == true) ? $record->film_rate : '';
$no_ups = (isset($update) && $update == true) ? $record->no_ups : '';
$ups_rate = (isset($update) && $update == true) ? $record->ups_rate : '';
$blanket = (isset($update) && $update == true) ? $record->blanket : '';
$block = (isset($update) && $update == true) ? $record->block : '';
$proofing = (isset($update) && $update == true) ? $record->proofing : '';
$development_foil = (isset($update) && $update == true) ? $record->development_foil : '';
$designing = (isset($update) && $update == true) ? $record->designing : '';
$no_plates = (isset($update) && $update == true) ? $record->no_plate : '';
$plate_rate = (isset($update) && $update == true) ? $record->plate_rate : '';
$description = (isset($update) && $update == true) ? $record->description : '';
$die_to_die_width = (isset($update) && $update == true) ? $record->die_to_die_width : '';
$die_to_die_height = (isset($update) && $update == true) ? $record->die_to_die_height : '';

$url = site_url();
$actionLink = (isset($update) && $update == true) ? $url . '/Update/offset_costing/' . $id : $url . '/Add/offset_costing/';

if (User_Model::hasAccess($role)):
    echo $feed_back;
    ?>

    <div class="container">
        <?php echo $feed_back; ?>
        <div class="row">
            <div class="col-md-10">
                <div class="heading-sec">
                    <h1>Costing <i><?php echo $pageTitle; ?> </i></h1>
                </div>
            </div>
        </div>

    </div>


    <div class="container">

        <div class="row">

            <div class="col-sm-12">

                <div class="expandable clearfix"> <!-- expandable -->

                    <div class="expandable-head">  <!-- expandable head -->
                        <h2><?php echo $pageTitle; ?></h2>
                        <div class="right">
                            <a href="#" class="fa fa-expand expand"></a>
                        </div>
                    </div>  <!-- expandable head -->

                    <div class="expandable-body">  <!-- expandable BODY -->

                        <form class="form-horizontal" onsubmit="return Validate(this);" enctype="multipart/form-data"
                              id="formToSubmits" method="post" action="<?php echo $actionLink; ?>">
                            <div class="row">

                                <div class="col-md-12">
                                    <div>
                                        <!-- Nav tabs -->
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#basic"
                                                                                      aria-controls="basic" role="tab"
                                                                                      data-toggle="tab">Basic Info</a>
                                            </li>
                                            <li role="presentation"><a href="#printing" aria-controls="printing"
                                                                       role="tab" data-toggle="tab">Printing</a></li>
                                            <li role="presentation"><a href="#board" aria-controls="board" role="tab"
                                                                       data-toggle="tab">Board</a></li>
                                            <li role="presentation"><a href="#uv_section" aria-controls="uv_section"
                                                                       role="tab" data-toggle="tab">UV</a></li>
                                            <li role="presentation"><a href="#lamination_section"
                                                                       aria-controls="lamination_section" role="tab"
                                                                       data-toggle="tab">Lamination</a></li>
                                            <li role="presentation"><a href="#foil_section" aria-controls="foil_section"
                                                                       role="tab" data-toggle="tab">Foil</a></li>
                                            <li role="presentation"><a href="#sorting_die_cutting"
                                                                       aria-controls="sorting_die_cutting" role="tab"
                                                                       data-toggle="tab">Sorting/Die Cutting</a></li>
                                            <li role="presentation"><a href="#pasting" aria-controls="pasting"
                                                                       role="tab" data-toggle="tab">Pasting</a></li>
                                            <li role="presentation"><a href="#development_cost"
                                                                       aria-controls="development_cost" role="tab"
                                                                       data-toggle="tab">Development</a></li>
                                            <li role="presentation"><a href="#other" aria-controls="other" role="tab"
                                                                       data-toggle="tab">Other</a></li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="FormTab" class="tab-pane active" id="basic">

                                                <!-- DATE -->
                                                <?php echo input(['name' => 'date', 'value' => $date, 'placeholder' => 'Select Date', 'data-date' => "true", ' readonly' => true], true, '', 'Date'); ?>

                                                <div class="input-group input-group-sm margin-bottom-sm">
                                                    <span class="input-group-addon">Select Customer:</span>
                                                    <input type="hidden" value="<?php echo $customer_id; ?>"
                                                           name="customer_id"/>
                                                    <p class="findMaterial selfvalidate"
                                                       data-method="<?php echo site_url("autocomplete/findCustomer/"); ?>"
                                                       style="padding: 3px; width: 100%;"><?php echo $customer_name ?></p>
                                                </div>
                                                <hr class="spacer"/>

                                                <!-- COMPANY NAME -->
                                                <?php // echo input(['name'=>'company_name','value'=>$company_name,'placeholder'=>'Company Name'],true,'','Company Name');
                                                ?>

                                                <!-- ITEM NAME -->
                                                <?php echo input(['name' => 'item_name', 'value' => $item_name, 'placeholder' => 'ITEM NAME'], true, '', 'Item Name'); ?>

                                                <!-- MOQ -->
                                                <?php echo input(['name' => 'moq', 'value' => $moq, 'placeholder' => 'MOQ'], true, '', 'M.O.Q'); ?>

                                                <?php if (!isset($update)): ?>
                                                    <input type="file" id="file" name="userfile"
                                                           class="btn btn-success">
                                                <?php endif ?>

                                                <!-- Description --><br>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i
                                                            class="fa fa-pencil-square-o"></i></span>
                                        <textarea class="form-control" name="description"
                                                  placeholder="Enter the Description About the job."
                                                  rows="7"><?php echo trim($description) ?></textarea>
                                                </div>

                                            </div> <!-- /basic tab -->

                                            <div role="FormTab" class="tab-pane" id="printing">

                                                <!-- NUMBER OF COLORS -->
                                                <?php echo input(['name' => 'no_of_color', 'id' => 'no_color', 'value' => $no_of_color, 'placeholder' => 'NUMBER OF COLORS'], true, '', 'No of Color'); ?>

                                                <!-- RS/Color -->
                                                <?php echo input(['name' => 'rate_per_color', 'id' => 'rate_per_color', 'value' => $rate_per_color, 'placeholder' => 'RS/Color'], true, '', 'RS/Color'); ?>

                                                <!-- NUMBER OF PCS IN A SHEET -->
                                                <?php echo input(['name' => 'pcs_per_sheet', 'id' => 'no_pcs_sheet', 'value' => $pcs_per_sheet, 'placeholder' => 'NUMBER OF PCS IN A SHEET'], true, '', 'PCS/Sheet'); ?>

                                                <!-- PRINTING COST PER PCS (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['printing_cost_per_pcs'], 'id' => 'cost_per_pcs', 'placeholder' => 'COST/PCS', 'readonly' => true], false, '', 'Cost/PCS'); ?>

                                            </div> <!-- /Printing tab -->

                                            <div role="FormTab" class="tab-pane" id="board">

                                                <!-- SALE TAX -->
                                                <?php echo input(['name' => 'sale_tax', 'id' => 'sale_tax', 'value' => $sale_tax, 'placeholder' => 'SALE TAX PERCENTAGE'], false, '', 'Sale Tax %'); ?>

                                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="checkbox" <?php echo $show_both_amount_checked; ?>
                                               id="show_both_amount" name="show_both_amount">
                                    </span>
                                                    <div>
                                                        <label for="show_both_amount"> SHOW BOTH WITH OR WITHOUT TAX
                                                            COST?</label>
                                                    </div>
                                                </div><!-- /input-group -->

                                                <!-- BOARD WIDTH -->
                                                <?php echo input(['name' => 'board_width', 'id' => 'board_width', 'value' => $board_width, 'placeholder' => 'BOARD WIDTH'], true, '', 'Board Width'); ?>

                                                <!-- BOARD HEIGHT -->
                                                <?php echo input(['name' => 'board_height', 'id' => 'board_height', 'value' => $board_height, 'placeholder' => 'BOARD HEIGHT'], true, '', 'Board Height'); ?>

                                                <!-- GRAMGE OF BOARD -->
                                                <?php echo input(['name' => 'board_gsm', 'id' => 'board_gsm', 'value' => $board_gsm, 'placeholder' => 'Gramage'], true, '', 'G.S.M'); ?>

                                                <!-- WEIGHT PER PACKET (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['board_weight'], 'id' => 'board_weight', 'placeholder' => 'WEIGHT PER PACKET', 'readonly' => true], false, '', 'WEIGHT/PACKET'); ?>

                                                <!-- RATE OF BOARD PER KG -->
                                                <?php echo input(['name' => 'board_rate', 'id' => 'board_rate', 'value' => $board_rate, 'placeholder' => 'RATE OF BOARD/KG'], true, '', 'Rate/KG'); ?>

                                                <!-- COST PER PACKET (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['packet_cost'], 'id' => 'cost_per_packet', 'placeholder' => 'COST PER PACKET', 'readonly' => true], false, '', 'Cost/Packet'); ?>

                                                <!-- NUMBER OF UNIT PER PACKET -->
                                                <?php echo input(['name' => 'no_unit_per_packet', 'id' => 'no_unit_per_packet', 'value' => $no_unit_per_packet, 'placeholder' => 'No of Unit/Packet'], true, '', 'No Unit/Packet'); ?>

                                                <!-- COST PER PACKET (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['per_pcs_cost'], 'id' => 'cost_per_pcs_for_board', 'placeholder' => 'COST PER PCS', 'readonly' => true], false, '', 'Cost/PCS'); ?>

                                                <!-- WASTAGE PERCENTAGE -->
                                                <?php echo input(['name' => 'wastage_percent', 'id' => 'wastage_percent', 'value' => $wastage_percent, 'placeholder' => 'WASTAGE PERCENTAGE'], true, '', 'Wastage Percent'); ?>

                                                <!-- FINAL COST (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['final_cost'], 'id' => 'final_cost', 'placeholder' => 'COST PER PCS', 'readonly' => true], false, '', 'Final Cost'); ?>

                                            </div> <!-- /Board tab -->

                                            <div role="FormTab" class="tab-pane" id="uv_section">

                                                <!-- UV WIDTH -->
                                                <?php echo input(['name' => 'uv_width', 'value' => $uv_width, 'id' => 'uv_width', 'placeholder' => 'UV WIDTH'], false, '', 'UV WIDTH'); ?>

                                                <!-- UV HEIGHT -->
                                                <?php echo input(['name' => 'uv_height', 'value' => $uv_height, 'id' => 'uv_height', 'placeholder' => 'UV HEIGHT'], false, '', 'UV HEIGHT'); ?>

                                                <!-- UV RATE -->
                                                <?php echo input(['name' => 'uv_rate', 'value' => $uv_rate, 'id' => 'uv_rate', 'placeholder' => 'UV RATE'], false, '', 'UV RATE'); ?>

                                                <!-- UV RATE/PACKET (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['uv_rate_per_packet'], 'id' => 'uv_rate_per_packet', 'placeholder' => 'UV RATE/PACKET', 'readonly' => ''], false, '', 'UV RATE/PACKET'); ?>

                                                <!-- UV RATE/PCS (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['uv_rate_per_pcs'], 'id' => 'uv_rate_per_pcs', 'placeholder' => 'UV RATE/PCS', 'readonly' => ''], false, '', 'UV RATE/PCS'); ?>

                                            </div> <!-- /uv_section tab -->

                                            <div role="FormTab" class="tab-pane" id="lamination_section">

                                                <!-- LAMINATION WIDTH -->
                                                <?php echo input(['name' => 'lamination_width', 'value' => $lamination_width, 'id' => 'lamination_width', 'placeholder' => 'LAMINATION WIDTH'], false, '', 'LAMINATION WIDTH'); ?>

                                                <!-- LAMINATION HEIGHT -->
                                                <?php echo input(['name' => 'lamination_height', 'value' => $lamination_height, 'id' => 'lamination_height', 'placeholder' => 'LAMINATION HEIGHT'], false, '', 'LAMINATION HEIGHT'); ?>

                                                <!-- LAMINATION RATE -->
                                                <?php echo input(['name' => 'lamination_rate', 'value' => $lamination_rate, 'id' => 'lamination_rate', 'placeholder' => 'LAMINATION RATE'], false, '', 'LAMINATION RATE'); ?>

                                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="checkbox" <?php echo $both_lamination_checked; ?>
                                               id="both_side_lamination" name="both_side_lamination">
                                    </span>
                                                    <div>
                                                        <label for="both_side_lamination"> Both Side Lamination?</label>
                                                    </div>
                                                </div><!-- /input-group -->

                                                <!-- LAMINATION RATE/PACKET (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['lamination_rate_per_packet'], 'id' => 'lamination_rate_per_packet', 'placeholder' => 'LAMINATION RATE/PACKET', 'readonly' => ''], false, '', 'LAMINATION RATE/PACKET'); ?>

                                                <!-- LAMINATION RATE/PCS (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['lamination_rate_per_pcs'], 'id' => 'lamination_rate_per_pcs', 'placeholder' => 'LAMINATION RATE/PCS', 'readonly' => ''], false, '', 'LAMINATION RATE/PCS'); ?>

                                            </div> <!-- /lamination_section tab -->

                                            <div role="FormTab" class="tab-pane" id="foil_section">

                                                <!-- FOIL RATE -->
                                                <?php echo input(['name' => 'foil_rate', 'value' => $foil_rate, 'id' => 'foil_rate', 'placeholder' => 'FOIL RATE'], false, '', 'FOIL RATE'); ?>

                                                <div class="col-sm-2 col-sm-offset-10 text-right">
                                                    <div class="btn-group">
                                                        <button type="button" id="add"
                                                                class="btn btn-success fa fa-plus"></button>
                                                        <button type="button" id="delete"
                                                                class="btn btn-danger fa fa-trash"></button>
                                                    </div>
                                                    <hr class="spacer"/>
                                                </div> <!-- /CONTROL BUTTONS -->

                                                <table class="table table-striped" id="inputTable">
                                                    <thead>
                                                    <tr>
                                                        <th>FOIL WIDTH</th>
                                                        <th>FOIL HEIGHT</th>
                                                        <th>RATE</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if (isset($foils) && isset($update)): ?>
                                                        <?php foreach ($foils as $foil): ?>
                                                            <tr class="data-row">
                                                                <!-- FOIL WIDTH -->
                                                                <td>
                                                                    <input type="text" name="foil_width[]"
                                                                           value="<?php echo $foil->width; ?>"
                                                                           data-type="decimal" placeholder="FOIL WIDTH"
                                                                           class="form-control foil-width selfvalidate">
                                                                </td>
                                                                <!-- FOIL HEIGHT -->
                                                                <td>
                                                                    <input type="text" name="foil_height[]"
                                                                           value="<?php echo $foil->height; ?>"
                                                                           data-type="decimal" placeholder="FOIL HEIGHT"
                                                                           class="form-control foil-height selfvalidate">
                                                                </td>
                                                                <!-- RATE PER PCS -->
                                                                <td>
                                                                    <input type="text" readonly
                                                                           placeholder="RATE PER PCS"
                                                                           value="<?php echo $foil->amount; ?>"
                                                                           class="form-control">
                                                                </td>
                                                            </tr>
                                                        <?php endforeach ?>
                                                    <?php else: ?>
                                                        <tr class="data-row">
                                                            <!-- FOIL WIDTH -->
                                                            <td>
                                                                <input type="text" name="foil_width[]"
                                                                       data-type="decimal" placeholder="FOIL WIDTH"
                                                                       class="form-control foil-width selfvalidate">
                                                            </td>
                                                            <!-- FOIL HEIGHT -->
                                                            <td>
                                                                <input type="text" name="foil_height[]"
                                                                       data-type="decimal" placeholder="FOIL HEIGHT"
                                                                       class="form-control foil-height selfvalidate">
                                                            </td>
                                                            <!-- RATE PER PCS -->
                                                            <td>
                                                                <input type="text" readonly placeholder="RATE PER PCS"
                                                                       class="form-control">
                                                            </td>
                                                        </tr>
                                                    <?php endif ?>
                                                    </tbody> <?php /*
                                    <tfooter>
                                        <tr>
                                            <!-- TOTAL FOIL WIDTH (JUST FOR VIEW) -->
                                            <td>
                                                <input type="text" readonly id="foil_width" class="form-control" placeholder="TOTAL FOIL WIDTH">
                                            </td>
                                            <!-- TOTAL FOIL HEIGHT (JUST FOR VIEW) -->
                                            <td>
                                                <input type="text" readonly id="foil_height" class="form-control" placeholder="TOTAL FOIL HEIGHT">
                                            </td>
                                            <!-- FOIL RATE/PCS (JUST FOR VIEW) -->
                                            <td>
                                                <input type="text" readonly id="foil_cost_pcs" class="form-control" placeholder="FOIL RATE / PCS">
                                            </td>
                                        </tr>
                                    </tfooter> */ ?>
                                                </table>

                                            </div> <!-- /lamination_section tab -->

                                            <div role="FormTab" class="tab-pane" id="sorting_die_cutting">

                                                <!-- SORTING RATE -->
                                                <?php echo input(['name' => 'sorting_rate', 'value' => $sorting_rate, 'id' => 'sorting_rate', 'placeholder' => 'SORTING RATE'], false, '', 'SORTING RATE'); ?>

                                                <!-- SORTING RATE/PACKET (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['sorting_cost'], 'id' => 'sorting_cost', 'placeholder' => 'COST/UNIT', 'readonly' => ''], false, '', 'COST/UNIT'); ?>

                                                <!-- DIE CUTTING RATE -->
                                                <?php echo input(['name' => 'die_cutting_rate', 'value' => $die_cutting_rate, 'id' => 'die_cutting_rate', 'placeholder' => 'DIE CUTTING RATE'], false, '', 'DIE CUTTING RATE'); ?>

                                                <!-- DIE RATE/PACKET (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['die_cutting_cost'], 'id' => 'die_cutting_cost', 'placeholder' => 'COST/UNIT', 'readonly' => ''], false, '', 'COST/UNIT'); ?>

                                                <!-- EMBOSSING RATE -->
                                                <?php echo input(['name' => 'embossing_rate', 'value' => $embossing_rate, 'id' => 'embossing_rate', 'placeholder' => 'EMBOSSING RATE'], false, '', 'EMBOSSING RATE'); ?>

                                                <!-- EMBOSSING RATE/PACKET (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['embossing_cost'], 'id' => 'embossing_cost', 'placeholder' => 'COST/UNIT', 'readonly' => ''], false, '', 'COST/UNIT'); ?>

                                            </div> <!-- /sorting_die_cutting tab -->

                                            <div role="FormTab" class="tab-pane" id="pasting">

                                                <!-- PASTING RATE -->
                                                <?php echo input(['name' => 'pasting_rate', 'value' => $pasting_rate, 'id' => 'pasting_rate', 'placeholder' => 'PASTING RATE'], false, '', 'PASTING RATE'); ?>

                                                <!-- PASTING RATE/PCS (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['pasting_cost'], 'id' => 'pasting_cost', 'placeholder' => 'COST/UNIT', 'readonly' => ''], false, '', 'COST/UNIT'); ?>

                                                <!-- PACKING RATE RATE -->
                                                <?php echo input(['name' => 'packing_rate', 'value' => $packing_rate, 'id' => 'packing_rate', 'placeholder' => 'PACKING RATE'], false, '', 'PACKING RATE'); ?>

                                                <!-- PCS PER CARTON -->
                                                <?php echo input(['name' => 'pcs_per_carton', 'value' => $pcs_per_carton, 'id' => 'pcs_per_carton', 'placeholder' => 'PCS PER CARTON'], false, '', 'PCS PER CARTON'); ?>

                                                <!-- PASTING RATE/PCS (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['pasting_cost'], 'id' => 'packing_cost', 'placeholder' => 'COST/UNIT', 'readonly' => ''], false, '', 'COST/UNIT'); ?>

                                            </div> <!-- /pasting tab -->

                                            <div role="FormTab" class="tab-pane" id="development_cost">

                                                <!-- FILM WIDTH -->
                                                <?php echo input(['name' => 'film_width', 'value' => $film_width, 'id' => 'film_width', 'placeholder' => 'FILM WIDTH'], true, '', 'FILM WIDTH'); ?>

                                                <!-- FILM HEIGHT -->
                                                <?php echo input(['name' => 'film_height', 'value' => $film_height, 'id' => 'film_height', 'placeholder' => 'FILM HEIGHT'], true, '', 'FILM HEIGHT'); ?>

                                                <!-- FILM RATE -->
                                                <?php echo input(['name' => 'no_film_development', 'value' => $no_film_development, 'id' => 'no_film_development', 'placeholder' => 'FILM COUNT'], true, '', 'FILM COUNT'); ?>

                                                <!-- FILM RATE -->
                                                <?php echo input(['name' => 'film_rate', 'value' => $film_rate, 'id' => 'film_rate', 'placeholder' => 'FILM RATE'], true, '', 'FILM RATE'); ?>

                                                <!-- FILM RATE (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['film_cost'], 'id' => 'film_cost', 'placeholder' => 'FILM COST', 'readonly' => ''], false, '', 'FILM COST'); ?>

                                                <!-- NO OF PLATES -->
                                                <?php echo input(['name' => 'no_plate', 'value' => $no_plates, 'id' => 'no_plates', 'placeholder' => 'NO OF PLATES'], true, '', 'NO OF PLATES'); ?>

                                                <!-- PLATES RATE -->
                                                <?php echo input(['name' => 'plate_rate', 'value' => $plate_rate, 'id' => 'plate_rate', 'placeholder' => 'PLATES RATE'], true, '', 'PLATES RATE'); ?>

                                                <!-- PLATES RATE (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['plates_cost'], 'id' => 'plates_cost', 'placeholder' => 'PLATES COST', 'readonly' => ''], false, '', 'PLATES COST'); ?>

                                                <!-- NO OF UPS -->
                                                <?php echo input(['name' => 'no_ups', 'value' => $no_ups, 'id' => 'no_ups', 'placeholder' => 'NO OF UPS IN DIE'], true, '', 'NO OF UPS DIE'); ?>

                                                <!-- RATE PER UPS -->
                                                <?php echo input(['name' => 'ups_rate', 'value' => $ups_rate, 'id' => 'ups_rate', 'placeholder' => 'RATE PER UPS'], true, '', 'RATE PER UPS'); ?>

                                                <!-- DIE COST (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['die_cost'], 'id' => 'die_cost', 'placeholder' => 'DIE COST', 'readonly' => ''], false, '', 'DIE COST'); ?>

                                                <!-- PROOFING -->
                                                <?php echo input(['name' => 'proofing', 'value' => $proofing, 'id' => 'proofing', 'placeholder' => 'PROOFING'], true, '', 'PROOFING'); ?>

                                                <!-- BLOCK -->
                                                <?php echo input(['name' => 'block', 'value' => $block, 'id' => 'block', 'placeholder' => 'BLOCK'], true, '', 'BLOCK'); ?>

                                                <!-- BLANKET -->
                                                <?php echo input(['name' => 'blanket', 'value' => $blanket, 'id' => 'blanket', 'placeholder' => 'BLANKET'], true, '', 'BLANKET'); ?>

                                                <!-- development_foil -->
                                                <?php echo input(['name' => 'development_foil', 'value' => $development_foil, 'id' => 'development_foil', 'placeholder' => 'development_foil'], true, '', 'development_foil'); ?>

                                                <!-- DESIGNING -->
                                                <?php echo input(['name' => 'designing', 'value' => $designing, 'id' => 'designing', 'placeholder' => 'DESIGNING'], true, '', 'DESIGNING'); ?>

                                                <!-- TOTAL DEVELOPMENT COST (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['total_development_cost'], 'id' => 'total_development_cost', 'placeholder' => 'TOTAL DEVELOPMENT', 'readonly' => ''], false, '', 'TOTAL DEVELOPMENT'); ?>

                                            </div> <!-- /development_cost tab -->

                                            <div role="FormTab" class="tab-pane" id="other">

                                                <!-- MARGIN -->
                                                <?php echo input(['name' => 'margin', 'value' => $margin, 'id' => 'margin', 'placeholder' => 'MARGIN RATE'], false, '', 'MARGIN'); ?>

                                                <!-- MARGIN RATE (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['margin_percent'], 'id' => 'margin_percent', 'placeholder' => 'MARGIN PERCENTAGE', 'readonly' => ''], false, '', 'MARGIN PERCENTAGE'); ?>

                                                <!-- CARTAGE -->
                                                <?php echo input(['name' => 'cartage', 'value' => $cartage, 'id' => 'cartage', 'placeholder' => 'CARTAGE'], false, '', 'CARTAGE'); ?>

                                                <!-- PCS IN DELIVERY -->
                                                <?php echo input(['name' => 'pcs_per_delivery', 'value' => $pcs_per_delivery, 'id' => 'pcs_per_delivery', 'placeholder' => 'PCS PER DELIVERY'], false, '', 'PCS PER DELIVERY'); ?>

                                                <!-- CARTAGE RATE (JUST FOR VIEW) -->
                                                <?php echo input(['value' => $cal['cartage_cost'], 'id' => 'cartage_cost', 'placeholder' => 'CARTAGE COST/UNIT', 'readonly' => ''], false, '', 'CARTAGE COST/UNIT'); ?>

                                                <!-- DIE TO DIE WIDTH -->
                                                <?php echo input(['name' => 'die_to_die_width', 'value' => $die_to_die_width, 'id' => 'die_to_die_width', 'placeholder' => 'DIE TO DIE WIDTH'], false, '', 'DIE TO DIE WIDTH'); ?>

                                                <!-- DIE TO DIE HEIGHT -->
                                                <?php echo input(['name' => 'die_to_die_height', 'value' => $die_to_die_height, 'id' => 'die_to_die_height', 'placeholder' => 'DIE TO DIE HEIGHT'], false, '', 'DIE TO DIE HEIGHT'); ?>

                                                <input type="submit" name="name" value="save"
                                                       class="btn btn-success text-center"/>

                                            </div> <!-- /pasting tab -->

                                        </div> <!-- TAB CONTENT -->
                                    </div>
                                </div>

                            </div> <!-- row -->

                        </form>


                    </div>  <!-- expandable BODY -->

                </div> <!-- expandable -->

            </div> <!-- .col -->

        </div> <!-- row -->

    </div> <!-- container -->

    <style>
        .tab-content {
            display: block;
        }
    </style>
    <script>

        $('[data-date=true]').datepicker({
            dateFormat: "dd-mm-yy",
            firstDay: 1,
            dayNamesShort: ["Tue", "Wed", "Thu", "Fri", "Sut", "Sun", "Mon"]
        });

        // COSTING FOR PRINTING AS PER COLOR
        $('#no_color, #rate_per_color, #no_pcs_sheet').keyup(function (event) {


        });

        // COSTING FOR BOARD //
        /**
         * COSTING
         */
        function calculate_cost() {
            var cpp = 0, cpp = 0, cpcs = 0, bw = 0;

            var no_color = parseFloat($('#no_color').val()),
                rate_per_color = parseFloat($('#rate_per_color').val()),
                no_pcs_sheet = parseFloat($('#no_pcs_sheet').val()),
                cost_per_pcs = $('#cost_per_pcs');

            if (no_color && rate_per_color && no_pcs_sheet) {
                cost_per_pcs.val((no_color * ( rate_per_color / 1000 / no_pcs_sheet )));
            } else {
                cost_per_pcs.val('');
            }

            var board_width = parseFloat($('#board_width').val()),
                board_height = parseFloat($('#board_height').val()),
                board_gsm = parseFloat($('#board_gsm').val()),
                sale_tax = parseFloat($('#sale_tax').val()),
                board_weight = $('#board_weight');

            var board_rate = parseFloat($('#board_rate').val()),
                cost_per_packet = $('#cost_per_packet');

            var no_unit_per_packet = parseFloat($('#no_unit_per_packet').val()),
                cost_per_pcs_for_board = $('#cost_per_pcs_for_board');

            var wastage_percent = parseFloat($('#wastage_percent').val()),
                final_cost = $('#final_cost');
            if (board_width && board_height && board_gsm) {
                bw = ( (board_width * board_height * board_gsm) / 15500 );
                board_weight.val(bw.toFixed(14));
            } else {
                board_weight.val('');
            }

            if (board_width && board_height && board_gsm && board_rate) {
                if (sale_tax > 0) {
                    // console.log(sale_tax);
                    cpp = ( bw * (board_rate / (sale_tax + 100) * 100) );
                } else {
                    cpp = ( bw * board_rate );
                }

                cost_per_packet.val(cpp.toFixed(14));
            } else {
                cost_per_packet.val('');
            }

            if (no_unit_per_packet) {
                cpcs = cpp / no_unit_per_packet;
                cost_per_pcs_for_board.val(cpcs.toFixed(14));
            } else {
                cost_per_pcs_for_board.val('');
            }

            if (wastage_percent) {
                final_cost.val((cpcs + (wastage_percent / 100 * cpcs)));
            } else {
                final_cost.val('');
            }

            var uv_rate = parseFloat($('#uv_rate').val()),
                uv_width = parseFloat($('#uv_width').val()),
                uv_height = parseFloat($('#uv_height').val()),
                uv_rate_per_packet = $('#uv_rate_per_packet'),
                uv_rate_per_pcs = $('#uv_rate_per_pcs'),
                lamination_rate = parseFloat($('#lamination_rate').val()),
                lamination_width = parseFloat($('#lamination_width').val()),
                lamination_height = parseFloat($('#lamination_height').val()),
                lamination_rate_per_packet = $('#lamination_rate_per_packet'),
                lamination_rate_per_pcs = $('#lamination_rate_per_pcs');
            if (uv_rate && uv_width && uv_height) {
                uv_rate_per_packet.val(uv_width * uv_height / 144 * uv_rate);
                if (no_pcs_sheet) {
                    uv_rate_per_pcs.val(uv_width * uv_height / 144 * uv_rate / no_pcs_sheet);
                }
            } else {
                uv_rate_per_packet.val('');
                uv_rate_per_pcs.val('');
            }

            if (lamination_rate && lamination_width && lamination_height) {
                var lam_packet_rate = lamination_width * lamination_height / 144 * lamination_rate;
                if ($('#both_side_lamination').is(':checked')) {
                    lamination_rate_per_packet.val(lam_packet_rate * 2);
                    if (no_pcs_sheet)
                        lamination_rate_per_pcs.val(lam_packet_rate * 2 / no_pcs_sheet);
                } else {
                    lamination_rate_per_packet.val(lam_packet_rate);
                    if (no_pcs_sheet)
                        lamination_rate_per_pcs.val(lam_packet_rate / no_pcs_sheet);
                }
            }

            var sorting_rate = parseFloat($('#sorting_rate').val()),
                sorting_cost = $('#sorting_cost');
            if (sorting_rate && no_pcs_sheet) {
                sorting_cost.val(sorting_rate / (no_pcs_sheet * 1000));
            }
            ;

            var die_cutting_rate = parseFloat($('#die_cutting_rate').val()),
                die_cutting_cost = $('#die_cutting_cost');
            if (die_cutting_rate && no_pcs_sheet) {
                die_cutting_cost.val(die_cutting_rate / (no_pcs_sheet * 1000));
            }
            ;

            var embossing_rate = parseFloat($('#embossing_rate').val()),
                embossing_cost = $('#embossing_cost');
            if (embossing_rate && no_pcs_sheet) {
                embossing_cost.val(embossing_rate / (no_pcs_sheet * 1000));
            }
            ;

            var cartage = parseFloat($('#cartage').val()),
                pcs_per_delivery = parseFloat($('#pcs_per_delivery').val()),
                cartage_cost = $('#cartage_cost');
            if (cartage && pcs_per_delivery) {
                cartage_cost.val(cartage / pcs_per_delivery);
            }
            ;

            var pasting_rate = parseFloat($('#pasting_rate').val()),
                pasting_cost = $('#pasting_cost');
            if (pasting_rate) {
                pasting_cost.val(pasting_rate / 1000);
            }

            var packing_rate = parseFloat($('#packing_rate').val()),
                pcs_per_carton = parseFloat($('#pcs_per_carton').val());
            packing_cost = $('#packing_cost');
            if (packing_rate && pcs_per_carton) {
                packing_cost.val(packing_rate / pcs_per_carton);
            }

            var film_width = parseFloat($('#film_width').val()),
                film_height = parseFloat($('#film_height').val()),
                film_rate = parseFloat($('#film_rate').val()),
                no_film_development = parseFloat($('#no_film_development').val()),
                film_cost = $('#film_cost');
            if (film_width && film_height && film_rate && no_film_development) {
                film_cost.val(film_width * film_height * film_rate * no_film_development);
            }

            var no_plates = parseFloat($('#no_plates').val()),
                plate_rate = parseFloat($('#plate_rate').val()),
                plates_cost = $('#plates_cost');
            if (no_plates && plate_rate) {
                plates_cost.val(no_plates * plate_rate);
            }

            var no_ups = parseFloat($('#no_ups').val()),
                ups_rate = parseFloat($('#ups_rate').val()),
                die_cost = $('#die_cost');
            if (no_ups && ups_rate) {
                die_cost.val(no_ups * ups_rate);
            }

            var proofing = parseFloat($('#proofing').val()),
                block = parseFloat($('#block').val()),
                blanket = parseFloat($('#blanket').val()),
                development_foil = parseFloat($('#development_foil').val()),
                designing = parseFloat($('#designing').val());

            var total_development_cost = (film_width * film_height * film_rate * no_color) + (no_plates * plate_rate) + (no_ups * ups_rate) + proofing + block + blanket + development_foil + designing;
            $('#total_development_cost').val(total_development_cost);

            var die_to_die_width = parseFloat($('#die_to_die_width').val()),
                die_to_die_height = parseFloat($('#die_to_die_height').val());

            // packaing rate / no of pcs carton

        } // end calculate_cost();

        function foil_calculation() {
            var rate = $('#foil_rate').val(),
                total_width = 0,
                total_height = 0,
                total_rate = 0,
                $table = $('#inputTable');
            $.each($table.find('tbody tr'), function () {
                var tr = $(this),
                    width = tr.find('.foil-width').val(),
                    height = tr.find('.foil-height').val(),
                    $total = tr.find('input:last');

                $total.val(width * height * rate);
                total_width += width;
                total_height += height;
                total_rate += (width * height * rate);
            });
            console.log(total_width);
            $table.find('tfooter tr input:eq(0)').val(total_width);
            $table.find('tfooter tr input:eq(1)').val(total_height);
            $table.find('tfooter tr input:eq(2)').val(total_rate);
        }

        $('.tab-content input').on('keyup change', function () {
            calculate_cost();
        });

        $('#inputTable').on('keyup', 'input', function (event) {
            foil_calculation();
        });


        $('#add').on('click', function (event) {
            event.preventDefault();
            var row = '<tr class="data-row">';
            row += '<td>';
            row += '<input type="text" name="foil_width[]" data-type="decimal" placeholder="FOIL WIDTH" class="form-control foil-width selfvalidate">';
            row += '</td>';
            row += '<td>';
            row += '<input type="text" name="foil_height[]" data-type="decimal" placeholder="FOIL HEIGHT" class="form-control foil-height selfvalidate">';
            row += '</td>';
            row += '<td>';
            row += '<input type="text" readonly placeholder="RATE PER PCS" class="form-control">';
            row += '</td>';
            row += '</tr>';
            $('#inputTable').append($(row));
        });


        $('#delete').on('click', function (event) {
            event.preventDefault();
            $('.del-row').remove();

            $('#inputTable').find('tr.data-row').each(function (index, el) {

                if ($('#inputTable').find('tr.data-row').length > 1) {

                    $('<span class="del-row fa fa-times"></span>')
                        .appendTo($(this))
                        .fadeIn('slow')
                        .click(function (event) {

                            if (confirm("Are you sure to delete this entry?")) {
                                $(this).parent('tr.data-row')
                                    .fadeOut('slow', function () {
                                        $(this).remove();
                                        $('.del-row').remove();
                                    });

                            }
                            ;

                        }); // click event on delete button


                } // if data row more than one
                else {
                    alert("Sorry One Row Must Be Fill.");
                }

            });
            // console.log(x);
        });

        var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
        function Validate(oForm) {
            // console.log(oForm);
            var sz = ($('#file')[0].files[0].size) / 1024 / 1024;
            if (sz > 25) {
                alert('File Size Must Be Lessthan 25MB.');
                return false;
            }
            var arrInputs = oForm.getElementsByTagName("input");
            for (var i = 0; i < arrInputs.length; i++) {
                var oInput = arrInputs[i];
                if (oInput.type == "file") {
                    var sFileName = oInput.value;
                    if (sFileName.length > 0) {
                        var blnValid = false;
                        for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                                blnValid = true;
                                break;
                            }
                        }

                        if (!blnValid) {
                            alert("Sorry, " + sFileName + " is invalid file, allowed Files are: " + _validFileExtensions.join(", "));
                            return false;
                        }
                    }
                }
            }

            return true;
        }

    </script>

    <?php
else:
    echo not_permitted();
endif;
?>