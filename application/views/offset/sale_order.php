<?php

$role = (isset($update) && $update==true)? 'editOffsetSaleOrder':'addOffsetSaleOrder';
$pageTitle = (isset($update) && $update==true)? 'Update ':'New ';
$job_code = (isset($update) && $update==true)? $so->job_code:$job_code;
$date = (isset($update) && $update==true)? pkDate($so->date):'';
$po_num = (isset($update) && $update==true)? $so->po_num:'';
$po_date = (isset($update) && $update==true)? pkDate($so->po_date):'';
$delivery_date = (isset($update) && $update==true)? pkDate($so->delivery_date):'';
$structure_id = (isset($update) && $update==true)? $so->structure_id:'';
$structure_name = (isset($update) && $update==true)? $so->job_name:'Choose Structure';
$customer_id = (isset($update) && $update==true && $so->customer_id!=0)? $so->customer_id:'';
$customer_name = (isset($update) && $update==true)? $so->customer_name:'Choose Customer';
$order_type = (isset($update) && $update==true)? $so->order_type:'';
$rate = (isset($update) && $update==true)? $so->rate:'';
$quantity = (isset($update) && $update==true)? $so->quantity:'';
$excess_quantity = (isset($update) && $update==true)? $so->excess_quantity:'';
$hold = (isset($update) && $update==true)? $so->hold:'';
$completed = (isset($update) && $update==true)? $so->completed:'';
$remarks = (isset($update) && $update==true)? $so->remarks:'';
$url = site_url();
$actionLink = (isset($update) && $update==true) ? $url.'/Offset_Sale_Order/add/'.$so->job_code: $url.'/Offset_Sale_Order/add';
if (User_Model::hasAccess($role)):

    echo $feed_back;
    ?>

<!-- Tables -->
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Sale Order <i><?php echo $pageTitle; ?> </i></h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmits" role="form" action="<?php echo $actionLink; ?>" method="POST">
                        <fieldset>
                            <legend class="text-center header">General</legend>

                            <div class="row">

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-cube"></i></span>
                                        <input type="text" name="job_code" readonly value="<?php echo $job_code; ?>" autocomplete="off" class="form-control is_num required" placeholder="JOB NUMBER">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="date" data-date="true" value="<?php echo $date; ?>" autocomplete="off" class="form-control is_num required" placeholder="DATE">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="po_date" data-date="true" value="<?php echo $po_date; ?>" autocomplete="off" class="form-control is_num required" placeholder="PO DATE">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                        <input type="text" name="delivery_date" data-date="true" value="<?php echo $delivery_date; ?>" autocomplete="off" class="form-control is_num required" placeholder="DELIVERY DATE">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-sort-numeric-asc"></i></span>
                                        <input type="text" name="po_num" value="<?php echo $po_num; ?>" autocomplete="off" class="form-control is_num required" placeholder="PURCHASE ORDER NUMBER">
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                        <input type="hidden" value="<?php echo $customer_id; ?>" name="customer_id"/>
                                        <p class="findMaterial selfvalidate" data-method="<?php echo site_url("autocomplete/findCustomer/"); ?>"><?php echo $customer_name ?></p>
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-file-o"></i></span>
                                        <input type="hidden" value="<?php echo $structure_id; ?>" name="structure_id"/>
                                        <p class="findMaterial selfvalidate" data-balance="<?php echo site_url("Balance/structure_balance/"); ?>" data-method="<?php echo site_url("autocomplete/findOffsetStructure/"); ?>"><?php echo $structure_name ?></p>
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                        <select name="order_type" class="form-control required">
                                          <option value="" readonly>Select Order Type</option>
                                          <option <?php echo ($order_type=="kg")?'selected':''; ?> value="kg">KG</option>
                                          <option <?php echo ($order_type=="pcs")?'selected':''; ?> value="pcs">PCS</option>
                                        </select>
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-database"></i></span>
                                        <input type="number" value="<?php echo $quantity; ?>" name="quantity" class="form-control required" placeholder="ORDER QUANTITY"/>
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-database"></i></span>
                                        <input type="text" value="<?php echo $rate; ?>" name="rate" class="form-control required" placeholder="RATE"/>
                                    </div><hr class="spacer"/>
                                </div>

                                <div class="col-sm-6 col-xs-12"> <!-- Excess Quantity -->
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-database"></i></span>
                                        <input type="number" value="<?php echo $excess_quantity; ?>" name="excess_quantity" class="form-control required" placeholder="EXCESS QUANTITY"/>
                                    </div><hr class="spacer"/>
                                </div> <!-- Excess Quantity -->

                                <div class="col-sm-6 col-xs-12"> <!-- Hode -->
                                    <input type="checkbox" value="1" name="hold" <?php echo $hold==0?'':'checked'; ?> id="fancy-checkbox"/>
                                    <label for="fancy-checkbox">Hold</label>
                                </div> 

                                <div class="col-sm-6 col-xs-12"> <!-- Completed -->
                                    <input type="checkbox" value="1" name="completed" <?php echo $completed==0?'':'checked'; ?> id="completed"/>
                                    <label for="completed">Completed</label>
                                </div> 

                                <div class="col-xs-12">
                                    <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-pencil"></i></span>
                                        <input type="text" value="<?php echo $remarks; ?>" name="remarks" class="form-control" placeholder="REMARKS"/>
                                    </div><hr class="spacer"/>
                                </div>


                        </fieldset> <!-- /DISPATCH FIELDSET -->

                        <div class="row">
                            <div class="col-md-12 text-center"><br>
                                <div class="btn-group">
                                    <button type="reset" class="btn btn-danger btn-sm">Reset</button>
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm">Save</button>
                                </div>
                                <br><br>
                            </div>
                        </div>

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });

    $('select[name="order_type"]').change(function() {
        if ( $(this).val() == 'pcs' ) {
            $('input[name="quantity"]').attr('placeholder', 'Quantity In PCS');
        }else if ( $(this).val() == 'kg' ) {
            $('input[name="quantity"]').attr('placeholder', 'Quantity In KG');
        }else{
            alert("Please Select Order Type");
        }
    });

    $('.autocomplete').each(function(i, el){
        var el = $(el),
            url = el.data('method');

        el.autocomplete({
        
            source: function( request, response ) {
                //console.log(el.data('method'));

                jQuery.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: { term: request.term },
                    success: function(data) {
                        var parsed = JSON.parse(data);
                        var newArray = new Array();

                        parsed.forEach(function (entry) {
                            var newObject = {
                                id: entry.id,
                                label: entry.label
                            };
                            newArray.push(newObject);
                        });

                        response(newArray);

                    },

                    error: function(xhr, ajaxOptions, thrownError) {
                        console.log(url);
                        container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>');
                    },
                    async: false
                });

            },

            minLength: 1,

            select: function( event, ui ) {
                $(this).prev().prev().val(ui.item.id);
             },

            focus: function (event, ui) {
                $(this).prev().prev().val(ui.item.id);
                $(this).val(ui.item.label);
            }

        });
    });

</script>

<?php 
else:
    echo not_permitted();
endif; 
?>