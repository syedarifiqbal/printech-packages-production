<?php

$role = ($update == true) ? 'editOffsetJobStructure' : 'addOffsetJobStructure';
$pageTitle = ($update == true) ? 'Update ' : 'New ';

$job_code = ($update == true) ? $structure->id : $job_code;
$job_name = ($update == true) ? $structure->job_name : '';
$c = ($update == true) ? $structure->c : 0;
$m = ($update == true) ? $structure->m : 0;
$y = ($update == true) ? $structure->y : 0;
$k = ($update == true) ? $structure->k : 0;
$spot1 = ($update == true) ? $structure->spot1 : '';
$spot2 = ($update == true) ? $structure->spot2 : '';
$spot3 = ($update == true) ? $structure->spot3 : '';
$machine = ($update == true) ? $structure->machine : '';
$sheet_type = ($update == true) ? $structure->sheet_type : '';
$material_id = ($update == true) ? $structure->material_id : '';
$grain_size = ($update == true) ? $structure->grain_size : 'h';
$gsm = ($update == true) ? $structure->gsm : '';
$no_ups = ($update == true) ? $structure->no_ups : '';
$board_width = ($update == true) ? $structure->board_width : '';
$board_height = ($update == true) ? $structure->board_height : '';
$water_gloss = ($update == true && $structure->water_gloss) ? $structure->water_gloss : 0;
$water_matt = ($update == true && $structure->water_matt) ? $structure->water_matt : 0;
$uv_gloss = ($update == true && $structure->uv_gloss) ? $structure->uv_gloss : 0;
$uv_spot = ($update == true && $structure->uv_spot) ? $structure->uv_spot : 0;
$uv_matt = ($update == true && $structure->uv_matt) ? $structure->uv_matt : 0;
$lamination_gloss = ($update == true && $structure->lamination_gloss) ? $structure->lamination_gloss : 0;
$lamination_matt = ($update == true && $structure->lamination_matt) ? $structure->lamination_matt : 0;
$lamination_both = ($update == true && $structure->lamination_both) ? $structure->lamination_both : 0;
$lamination_type = ($update == true && $structure->lamination_type) ? $structure->lamination_type : '-';
$warnish_gloss = ($update == true && $structure->warnish_gloss) ? $structure->warnish_gloss : 0;
$warnish_matt = ($update == true && $structure->warnish_matt) ? $structure->warnish_matt : 0;
$foil = ($update == true && $structure->foil) ? $structure->foil : '';
$foil_custom_color = ($update == true && in_array($structure->foil, array('Silver','Golden'))) ? ' style="display:none;"' : 'value="'.$foil.'"';
$foil_type = ($update == true && $structure->foil_type) ? $structure->foil_type : '-';
$emboss = ($update == true && $structure->emboss) ? $structure->emboss : '';
$color_remarks = ($update == true) ? $structure->color_remarks : '';
$ply = ($update == true) ? $structure->ply : '';
$panel = ($update == true) ? $structure->panel : '';
$roll_size = ($update == true) ? $structure->roll_size : '';
$cutting_size = ($update == true) ? $structure->cutting_size : '';
$corrugation_remarks = ($update == true) ? $structure->corrugation_remarks : '';
$flutting = ($update == true) ? $structure->flutting : '';
//$flutting = ($update == true && $structure->flutting) ? $structure->flutting : 0;
//$dubai_flutting = ($update == true && $structure->dubai_flutting) ? $structure->dubai_flutting : 0;
$die_cutting_type = ($update == true) ? $structure->die_cutting_type : '';
$die_remarks = ($update == true) ? $structure->die_remarks : '';
$pasting_remarks = ($update == true) ? $structure->pasting_remarks : '';
$pasting_type = ($update == true) ? $structure->pasting_type : '';
$packing_qty = ($update == true) ? $structure->packing_qty : '';
$correction = ($update == true) ? $structure->correction : '';

$url = site_url();
$actionLink = sprintf("%s/Offset_Order_Managment/job_structure/%s", $url, ($update == true) ? $structure->id : '');
//      ($update==true) ? $url.'/Offset_Order_Managment/job_structure/'.$structure->id: $url.'/Offset_Order_Managment/job_structure';
if (User_Model::hasAccess($role)): ?>

    <?php echo $feed_back ?>
    <!-- Tables -->
    <div class="container">

        <div class="row">
            <div class="col-md-10">
                <div class="heading-sec">
                    <h1><?php echo $pageTitle; ?> <i>Job Structure <?php echo ($update == true) ? (int)$structure->id : ''; ?></i></h1>
                </div>
            </div>
        </div>

    </div>


    <div class="container">

        <div class="row">

            <div class="col-md-8">

                <div class="expandable clearfix"> <!-- expandable -->

                    <div class="expandable-head">  <!-- expandable head -->
                        <h2><?php echo $pageTitle; ?></h2>
                        <div class="right">
                            <a href="#" class="fa fa-expand expand"></a>
                        </div>
                    </div>  <!-- expandable head -->

                    <div class="expandable-body">  <!-- expandable BODY -->

                        <form class="form-horizontal" id="formToSubmit" role="form" action="<?php echo $actionLink; ?>"
                              method="POST">

                            <fieldset>
                                <legend class="text-center header">General</legend>

                                <div class="row">

                                    <div class="col-sm-12 col-xs-12">
                                        <?php echo input2('Job Name', ['name' => 'job_name'], $job_name) ?>
                                    </div>

                                </div>

                            </fieldset>

                            <fieldset>
                                <legend class="text-center header">Printing</legend>

                                <div class="col-sm-12">
                                    <?php /* echo input2('Machine Name', ['name' => 'machine'], $machine, true, true); */ ?>
                                    <?php echo checkbox('Cyan', 'c', 1, $c, true, true); ?>
                                    <?php echo checkbox('Megenta', 'm', 1, $m, true, true); ?>
                                    <?php echo checkbox('Yellow', 'y', 1, $y, true, true); ?>
                                    <?php echo checkbox('Black', 'k', 1, $k, true, true); ?>
                                    <?php echo input2('Spot', ['name' => 'spot1'], $spot1) ?>
                                    <?php echo input2('Spot', ['name' => 'spot2'], $spot2) ?>
                                    <?php echo input2('Spot', ['name' => 'spot3'], $spot3) ?>
                                </div>

                            </fieldset>

                            <fieldset>
                                <legend class="text-center header">Board</legend>

                                <div class="row">
                                    <div class="col-sm-6 col-xs-12">
                                        <div class="input-group margin-bottom-sm">
                                            <span class="input-group-addon">Board Width</span>
                                            <input type="text" name="board_width" value="<?php echo $board_width; ?>" class="form-control required" placeholder="BOARD WIDTH">
                                            <span class="input-group-addon">
                                                <input type='radio' name="grain_size" value="w" <?php echo ($grain_size=='w')?'checked':''; ?>>
                                            </span>
                                        </div> <hr class="spacer">
                                        <div class="input-group margin-bottom-sm">
                                            <span class="input-group-addon">Board Height</span>
                                            <input type="text" name="board_height" value="<?php echo $board_height; ?>" class="form-control required" placeholder="BOARD HEIGHT">
                                            <span class="input-group-addon">
                                                <input type='radio' name="grain_size" value="h" <?php echo ($grain_size=='h')?'checked':''; ?>>
                                            </span>
                                        </div> <hr class="spacer">
                                        <?php echo input2('GSM', ['name' => 'gsm'], $gsm); ?>
                                        <?php echo input2('No Ups', ['name' => 'no_ups'], $no_ups); ?>
                                        <?php echo input2('Material', ['name' => 'material_id'], $material_id); ?>
                                        <?php // echo input2('Grain Size', ['name' => 'grain_size'], $grain_size); ?>
                                    </div>
                                    <div class="col-sm-6 col-xs-12">
                                        <table class="table table-bordered">

                                            <tr>
                                                <th>Sheet Type:</th>
                                                <td>
                                                    <label>
                                                        <input type="radio" name="sheet_type" id="board"
                                                               value="paper" <?php echo ($sheet_type == 'paper') ? 'checked' : '' ?>>
                                                        Paper
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="sheet_type" id="board" value="board"
                                                            <?php echo ($sheet_type == 'board') ? 'checked' : '' ?>>
                                                        Board
                                                    </label>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>Water Base:</th>
                                                <td>
                                                    <?php echo checkbox('Gloss', 'water_gloss', 1, $water_gloss, true, true); ?>
                                                    <?php echo checkbox('Matt', 'water_matt', 1, $water_matt, true, true); ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>UV:</th>
                                                <td>
                                                    <?php echo checkbox('Gloss', 'uv_gloss', 1, $uv_gloss, true, true); ?>
                                                    <?php echo checkbox('Matt', 'uv_matt', 1, $uv_matt, true, true); ?>
                                                    <?php echo checkbox('spot', 'uv_spot', 1, $uv_spot, true, true); ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>Lamination:</th>
                                                <td>
                                                    <div>
                                                        <?php echo checkbox('Gloss', 'lamination_gloss', 1, $lamination_gloss, true, true); ?>
                                                        <?php echo checkbox('Matt', 'lamination_matt', 1, $lamination_matt, true, true); ?>
                                                        <?php echo checkbox('Both Side', 'lamination_both', 1, $lamination_both, true, true); ?>
                                                    </div>
                                                    <div>
                                                        <label>
                                                            <input type="radio" name="lamination_type" id="lamination_none"
                                                                   value="-" <?php echo ($lamination_type == '-') ? 'checked' : '' ?>>
                                                            None
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="lamination_type" id="lamination_cold"
                                                                   value="cold" <?php echo ($lamination_type == 'COLD') ? 'checked' : '' ?>>
                                                            Cold
                                                        </label>
                                                        <label>
                                                            <input type="radio" name="lamination_type" id="lamination_hot" value="hot"
                                                                <?php echo ($lamination_type == 'HOT') ? 'checked' : '' ?>>
                                                            Hot
                                                        </label>
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>Varnish:</th>
                                                <td>
                                                    <?php echo checkbox('Gloss', 'warnish_gloss', 1, $warnish_gloss, true, true); ?>
                                                    <?php echo checkbox('Matt', 'warnish_matt', 1, $warnish_matt, true, true); ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <th>Foil:</th>
                                                <td>
                                                    <?php echo select(['name' => 'foil', 'options' => ['None' => 'None', 'Silver' => 'Silver', 'Golden' => 'Golden'], 'value' => $foil], false, '', 'Foil') ?>
                                                    
                                                    <input type="text" id="customFoilColor" name="custom_foil_color" class="form-control" placeholder="CUSTOM COLOR" <?php echo $foil_custom_color?>>
                                                    <label>
                                                        <input type="radio" name="foil_type" id="foil_none"
                                                               value="-" <?php echo ($foil_type == '-') ? 'checked' : '' ?>>
                                                        None
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="foil_type" id="foil_cold"
                                                               value="cold" <?php echo ($foil_type == 'cold') ? 'checked' : '' ?>>
                                                        Cold
                                                    </label>
                                                    <label>
                                                        <input type="radio" name="foil_type" id="foil_hot" value="hot"
                                                            <?php echo ($foil_type == 'hot') ? 'checked' : '' ?>>
                                                        Hot
                                                    </label>
                                                </td>
                                            </tr>

                                        </table>

                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <legend class="text-center header">Corrugations</legend>

                                <div class="col-md-12">
                                    <?php echo input2('Fluting', ['name' => 'flutting'], $flutting); ?>
                                </div>
                                <div class="col-md-6">
                                    <?php echo select(['name' => 'ply', 'options' => [0 => 'No Ply', 3 => 3, 5 => 5, 7 => 7], 'value' => $ply], false, '', 'Ply') ?>
                                </div>

                                <div class="col-md-6">
                                    <?php //echo checkbox('Flutting', 'flutting', 1, $flutting, true, true); ?>
                                    <?php //echo checkbox('Dubai Flutting', 'dubai_flutting', 1, $dubai_flutting, true, true); ?>
                                    <p><strong>Panel</strong></p>
                                    <label>
                                        <input type="radio" name="panel" id="board"
                                               value="none" <?php echo ($panel == 'none') ? 'checked' : '' ?>>
                                        None
                                    </label>
                                    <label>
                                        <input type="radio" name="panel" id="board"
                                               value="Single" <?php echo ($panel == 'Single') ? 'checked' : '' ?>>
                                        Single
                                    </label>
                                    <label>
                                        <input type="radio" name="panel" id="board" value="Double (Same)"
                                            <?php echo ($panel == 'Double (Same)') ? 'checked' : '' ?>>
                                        Double (Same)
                                    </label>
                                    <label>
                                        <input type="radio" name="panel" id="board" value="Double (Different)"
                                            <?php echo ($panel == 'Double (Different)') ? 'checked' : '' ?>>
                                        Double (Different)
                                    </label>
                                </div>

                                <div class="col-md-6">
                                    <?php echo input2('Roll Size', ['name' => 'roll_size'], $roll_size); ?>
                                    <?php echo input2('Cutting Size', ['name' => 'cutting_size'], $cutting_size); ?>
                                </div>

                                <div class="col-md-12">
                                    <?php echo input2('Remarks', ['name' => 'corrugation_remarks'], $corrugation_remarks); ?>
                                </div>

                            </fieldset>

                            <fieldset>
                                <legend class="text-center header">Die Cutting</legend>

                                <div class="col-md-6">
                                    <?php echo select(['name' => 'die_cutting_type', 'options' => ['None' => 'None', 'New Manual' => 'New Manual', 'Old Manual' => 'Old Manual', 'New Automatic' => 'New Automatic', 'Old Automatic' => 'Old Automatic'], 'value' => $die_cutting_type], false, '', 'Die Type') ?>
                                </div>

                                <div class="col-md-6">
                                    <label>
                                        <input type="radio" name="emboss" value=""
                                            <?php echo ($emboss == '') ? 'checked' : '' ?>>
                                        None
                                    </label>
                                    <label>
                                        <input type="radio" name="emboss" value="Emboss(New)"
                                            <?php echo ($emboss == 'Emboss(New)') ? 'checked' : '' ?>>
                                        Emboss(New)
                                    </label>
                                    <label>
                                        <input type="radio" name="emboss" value="Emboss(Old)"
                                            <?php echo ($emboss == 'Emboss(Old)') ? 'checked' : '' ?>>
                                        Emboss(Old)
                                    </label>
                                    <?php // echo checkbox('Emboss', 'emboss', 1, $emboss, true, true); ?>
                                </div>

                                <div class="col-md-12">
                                    <?php echo input2('Remarks', ['name' => 'die_remarks'], $die_remarks); ?>
                                </div>

                            </fieldset>

                            <fieldset>
                                <legend class="text-center header">Pasting</legend>
                                <div class="col-md-12">
                                    <?php echo select(['name' => 'pasting_type', 'options' => ['None' => 'None', 'Hand Pasting' => 'Hand Pasting', 'Machine Pasting' => 'Machine Pasting'], 'value' => $pasting_type], false, '', 'Pasting Type') ?>
                                </div>
                                <div class="col-md-12">
                                    <?php echo input2('Remarks', ['name' => 'pasting_remarks'], $pasting_remarks); ?>
                                </div>

                            </fieldset>

                            <fieldset>
                                <legend class="text-center header">Dispatch</legend>

                                <div class="col-md-12">
                                    <?php echo input2('Quantity to be packed', ['name' => 'packing_qty'], $packing_qty); ?>
                                </div>

                            </fieldset>

                            <fieldset>
                                <legend class="text-center header">Corrections</legend>

                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="correction">Correction:</label>
                                        <textarea class="form-control" name="correction" rows="5"
                                                  id="correction"><?php echo $correction; ?></textarea>
                                    </div>
                                </div>

                            </fieldset>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-center"><br>
                            <?php if (!$update): ?>
                                <input type="checkbox" name="reference_type" value="1" id="reference_type">
                                <label for="reference_type">New Serial?</label>
                            <?php endif; ?>
                            <div class="btn-group">
                                <button type="reset" class="btn btn-danger btn-sm">Reset</button>
                                <button type="submit" name="submit" class="btn btn-primary btn-sm">Save</button>
                            </div>
                            <br><br>
                        </div>
                    </div>

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

            <div class="col-md-4">
                
            </div>

        </div>

    </div>

    </div>

    <script>
        var i = 1;
        $('#add').on('click', function (event) {
            event.preventDefault();
            var row = '<tr class="data-row">';
            row += '<td>';
            row += '<input type="text" readonly name="s_no[]" value="' + (++i) + '" data-type="decimal" placeholder="S.No" class="form-control foil-height">';
            row += '</td>';
            row += '<td>';
            row += '<input type="text" name="brand[]" value="<?php echo ''; ?>" data-type="decimal" placeholder="Brand" class="form-control foil-height selfvalidate">';
            row += '</td>';
            row += '<td>';
            row += '<input type="text" name="code[]" value="<?php echo ''; ?>" data-type="decimal" placeholder="Code" class="form-control foil-height selfvalidate">';
            row += '</td>';
            row += '<td>';
            row += '<input type="text" name="color[]" value="<?php echo ''; ?>" data-type="decimal" placeholder="Color" class="form-control foil-height selfvalidate">';
            row += '</td>';
            row += '</tr>';
            $('#inputTable').append($(row));
        });


        $('#delete').on('click', function (event) {
            event.preventDefault();
            $('.del-row').remove();

            $('#inputTable').find('tr.data-row').each(function (index, el) {

                if ($('#inputTable').find('tr.data-row').length > 1) {

                    $('<span class="del-row fa fa-times"></span>')
                        .appendTo($(this))
                        .fadeIn('slow')
                        .click(function (event) {

                            if (confirm("Are you sure to delete this entry?")) {
                                $(this).parent('tr.data-row')
                                    .fadeOut('slow', function () {
                                        $(this).remove();
                                        $('.del-row').remove();
                                    });

                            }
                            ;

                        }); // click event on delete button


                } // if data row more than one
                else {
                    alert("Sorry One Row Must Be Fill.");
                }

            });
            // console.log(x);
        });

        $('[name="foil"]').on('change',function(e){
            if( $(this).val() == 'None' )
                $('#customFoilColor').show();
            else{
                $('#customFoilColor').hide();
            }
        });

        $('[name="lamination_matt"]').on('change',function(e){
            if( $(this).prop('checked') ){
                $('[name="lamination_gloss"]').prop('checked',false);
            }
        });

        $('[name="lamination_gloss"]').on('change',function(e){
            if( $(this).prop('checked') ){
                $('[name="lamination_matt"]').prop('checked',false);
            }
        });


    </script>
    <?php
else:
    echo not_permitted();
endif;
?>