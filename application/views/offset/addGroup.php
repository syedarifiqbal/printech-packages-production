<?php
 $role = (isset($update) && $update==true)? 'editOffsetGroup':'addOffsetGroup';
 $pageTitle = (isset($update) && $update==true)? 'Update':'New';
 $name = (isset($update) && $update==true)? $cate->group_name:'';
 $description = (isset($update) && $update==true)? $cate->description:'';
 $url = site_url('');
 $actionLink = (isset($update) && $update==true) ? $url.'/offset_group/Update/'.$cate->group_id:$url.'/offset_group/Add';

 if (User_Model::hasAccess($role)): ?>
<?php echo $feed_back ?>

<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>Group</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        <fieldset>
                            <!-- <legend class="text-center header"><?php echo $pageTitle; ?></legend> -->

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-cube"></i></div>
                                    <input id="fname" name="group_name" type="text" autofocus="true" placeholder="Group Name" class="form-control required" value="<?php echo $name ?>">
                                </div>
                                <br>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                                        <textarea class="form-control" name="description" placeholder="Enter your description for future indication here so that you can remind the propose of the Category." 
                                        rows="7"><?php echo trim($description) ?></textarea>
                                </div>
                                <br>
                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                    </div>
                                </div>
                        </fieldset>
                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>
<?php 
else:
    echo not_permitted();
endif; 
?>