<?php

$role = (isset($update) && $update==true)? 'editOffsetSaleOrder':'addOffsetSaleOrder';
$pageTitle = $record->id? 'Update ':'New ';

if (User_Model::hasAccess($role)):

    echo $feed_back;
    ?>

<!-- Tables -->
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Production Details <i><?php echo $pageTitle; ?> </i></h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->
                    <form class="form-horizontal" id="formToSubmits" role="form" action="<?php echo site_url("Offset_sale_order/add_wastage/$job_code/$record->id"); ?>" method="POST">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Job code</span>
                                    <input type="text" name="data[job_code]" readonly value="<?php echo $so->job_code; ?>" autocomplete="off" class="form-control is_num required" placeholder="JOB NUMBER">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" name="date" data-date="true" value="<?php echo $record->dispatch_date? pkDate($record->dispatch_date):''; ?>" autocomplete="off" class="form-control is_num required" placeholder="DATE">
                                </div><hr class="spacer"/>
                            </div>
                        </div>
                        <div class="row">
                            <fieldset>
                            <legend class="text-center header">CONVENTIONAL</legend>
                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">No Sheet</span>
                                    <input type="text" name="data[con_no_sheet]" value="<?php echo $record->con_no_sheet; ?>"class="form-control" placeholder="No Sheet">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Pollar</span>
                                    <input type="text" name="data[con_pollar]" value="<?php echo $record->con_pollar; ?>"class="form-control" placeholder="Pollar">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Printing</span>
                                    <input type="text" name="data[con_printing]" value="<?php echo $record->con_printing; ?>"class="form-control" placeholder="Printing">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Lamination</span>
                                    <input type="text" name="data[con_lamination]" value="<?php echo $record->con_lamination; ?>"class="form-control" placeholder="Lamination">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Embossing</span>
                                    <input type="text" name="data[con_embossing]" value="<?php echo $record->con_embossing; ?>"class="form-control" placeholder="Embossing">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Coating</span>
                                    <input type="text" name="data[con_coating]" value="<?php echo $record->con_coating; ?>"class="form-control" placeholder="Coating">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Die Cutting</span>
                                    <input type="text" name="data[con_die_cutting]" value="<?php echo $record->con_die_cutting; ?>"class="form-control" placeholder="Die Cutting">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Pasting</span>
                                    <input type="text" name="data[con_pasting]" value="<?php echo $record->con_pasting; ?>"class="form-control" placeholder="Pasting">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Dispatch</span>
                                    <input type="text" name="data[con_dispatch]" value="<?php echo $record->con_dispatch; ?>"class="form-control" placeholder="Dispatch">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Sorting</span>
                                    <input type="text" name="data[con_sorting]" value="<?php echo $record->con_sorting; ?>"class="form-control" placeholder="Dispatch">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Corrugation</span>
                                    <input type="text" name="data[con_corrugation]" value="<?php echo $record->con_corrugation; ?>"class="form-control" placeholder="Dispatch">
                                </div><hr class="spacer"/>
                            </div>
                            </fieldset>


                            <!-- U.V Portions -->


                            <fieldset>
                            <legend class="text-center header">UV</legend>
                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">No Sheet</span>
                                    <input type="text" name="data[uv_no_sheet]" value="<?php echo $record->uv_no_sheet; ?>"class="form-control" placeholder="No Sheet">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Pollar</span>
                                    <input type="text" name="data[uv_pollar]" value="<?php echo $record->uv_pollar; ?>"class="form-control" placeholder="Pollar">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Printing</span>
                                    <input type="text" name="data[uv_printing]" value="<?php echo $record->uv_printing; ?>"class="form-control" placeholder="Printing">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Lamination</span>
                                    <input type="text" name="data[uv_lamination]" value="<?php echo $record->uv_lamination; ?>"class="form-control" placeholder="Lamination">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Embossing</span>
                                    <input type="text" name="data[uv_embossing]" value="<?php echo $record->uv_embossing; ?>"class="form-control" placeholder="Embossing">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Coating</span>
                                    <input type="text" name="data[uv_coating]" value="<?php echo $record->uv_coating; ?>"class="form-control" placeholder="Coating">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Die Cutting</span>
                                    <input type="text" name="data[uv_die_cutting]" value="<?php echo $record->uv_die_cutting; ?>"class="form-control" placeholder="Die Cutting">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Pasting</span>
                                    <input type="text" name="data[uv_pasting]" value="<?php echo $record->uv_pasting; ?>"class="form-control" placeholder="Pasting">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Dispatch</span>
                                    <input type="text" name="data[uv_dispatch]" value="<?php echo $record->uv_dispatch; ?>"class="form-control" placeholder="Dispatch">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Sorting</span>
                                    <input type="text" name="data[uv_sorting]" value="<?php echo $record->uv_sorting; ?>"class="form-control" placeholder="Dispatch">
                                </div><hr class="spacer"/>
                            </div>

                            <div class="col-sm-6 col-xs-12">
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon">Corrugation</span>
                                    <input type="text" name="data[uv_corrugation]" value="<?php echo $record->uv_corrugation; ?>"class="form-control" placeholder="Dispatch">
                                </div><hr class="spacer"/>
                            </div>
                            </fieldset> 
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center"><br>
                                <div class="btn-group">
                                    <button type="reset" class="btn btn-danger">Reset</button>
                                    <button type="submit" name="submit" class="btn btn-primary">Save</button>
                                    <?php if ($record->id): 
                                        echo anchor(site_url("offset/sale_order/production_detail/$record->id"), '<i class="fa fa-file-pdf-o"></i> View', ' data-remote="false" data-toggle="modal" data-target="#myModal" class="btn btn-info"')
                                    ?>
                                    <?php endif ?>
                                </div>
                                <br><br>
                            </div>
                        </div>

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });

    $('select[name="order_type"]').change(function() {
        if ( $(this).val() == 'pcs' ) {
            $('input[name="quantity"]').attr('placeholder', 'Quantity In PCS');
        }else if ( $(this).val() == 'kg' ) {
            $('input[name="quantity"]').attr('placeholder', 'Quantity In KG');
        }else{
            alert("Please Select Order Type");
        }
    });

    $('.autocomplete').each(function(i, el){
        var el = $(el),
            url = el.data('method');

        el.autocomplete({
        
            source: function( request, response ) {
                //console.log(el.data('method'));

                jQuery.ajax({
                    type: "POST",
                    url: url,
                    dataType: 'json',
                    data: { term: request.term },
                    success: function(data) {
                        var parsed = JSON.parse(data);
                        var newArray = new Array();

                        parsed.forEach(function (entry) {
                            var newObject = {
                                id: entry.id,
                                label: entry.label
                            };
                            newArray.push(newObject);
                        });

                        response(newArray);

                    },

                    error: function(xhr, ajaxOptions, thrownError) {
                        console.log(url);
                        container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>');
                    },
                    async: false
                });

            },

            minLength: 1,

            select: function( event, ui ) {
                $(this).prev().prev().val(ui.item.id);
             },

            focus: function (event, ui) {
                $(this).prev().prev().val(ui.item.id);
                $(this).val(ui.item.label);
            }

        });
    });

</script>

<?php 
else:
    echo not_permitted();
endif; 
?>