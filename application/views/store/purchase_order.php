<?php
 $role = (isset($update) && $update==true)? 'updatePurchaseOrder':'addPurchaseOrder';
 $pageTitle = (isset($update) && $update==true)? 'update':'new';
 $po_id = (isset($update) && $update==true)? $po->po_id:'';
 $hold = (isset($update) && $update==true && $po->hold==1)? "checked":'';
 $date = (isset($update) && $update==true)? PKdate($po->date):'';
 $supplier_id = (isset($update) && $update==true)? $po->supplier_id:'';
 $supplier_name = (isset($update) && $update==true)? $po->supplier_name:'';
 $description = (isset($update) && $update==true)? $po->description:'';
 $actionLink = (isset($update) && $update==true) ? 'Update/purchase_order/'.$po->po_id:'Add/purchase_order';

 if ($roles[$role]): ?>
<div class="container">

    <div class="row">
        <div class="page-header col-md-12">
            <h1 align="center">Purchase Order <small><strong><i>(</i></strong><?php echo $pageTitle; ?><strong><i>)</i></strong></small></h1>
        </div>
    </div>

    <div class="row">

        <div class="col-xs-12">
            <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                
                <div class="row">

                    <div class="col-sm-3"> <!-- DATE -->
                        <div class="input-group input-group-sm">
                            <div class="input-group-addon"><i class="fa fa-cube"></i></div>
                            <input name="date" type="text" data-date="true" autofocus="true" placeholder="DATE" class="form-control required" value="<?php echo $date; ?>">
                        </div><hr class="spacer"/>
                    </div> <!-- /DATE -->

                    <div class="col-sm-3 col-sm-offset-3"> <!-- HOLD -->
                        <input type="checkbox" name="hold" <?php echo $hold; ?> id="fancy-checkbox"/>
                        <label for="fancy-checkbox">Checkbox</label>
                        <span>HOLD</span>
                    </div> <!-- /HOLD -->

                    <div class="col-sm-3"> <!-- Supplier -->
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon"><i class="fa fa-paint-brush"></i></span>
                            <input type="hidden" value="<?php echo $supplier_id; ?>" name="supplier_id"/>
                            <input type="text" value="<?php echo $supplier_name; ?>" class="form-control autocomplete required" value="<?php ?>" placeholder="CHOOSE SUPPLIER" data-method="<?php echo base_url(); ?>index.php/autocomplete/supplier/">
                        </div><hr class="spacer"/>
                    </div> <!-- /Supplier -->

                    <div class="col-sm-10"> <!-- Description -->
                        <div class="input-group input-group-sm">
                            <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                            <input name="description" type="text" autofocus="true" placeholder="DESCRIPTION" class="form-control required" value="<?php echo $description; ?>">
                        </div><hr class="spacer"/>
                    </div> <!-- /Description -->

                    <div class="col-sm-2 text-right"> <!-- Description -->
                        <div class="btn-group">
                            <button type="button" id="add" class="btn btn-success fa fa-plus"></button>
                            <button type="button" id="delete" class="btn btn-danger fa fa-trash"></button>
                        </div>
                    </div> <!-- /Description -->


                    <hr class="spacer"/>
                    <div class="col-sm-12"> <!-- Entry Table col -->
                        <div class="table-responsive">
                            <table class="table" id="inputTable">
                                <tr>
                                    <th>Material</th>
                                    <th>Description</th>
                                    <th>Rate</th>
                                    <th>Quantity</th>
                                </tr>
                                <?php if ( isset($update) && $update==true ){ ?>
                                <?php foreach ($po_items as $item): ?>
                                <tr class="data-row">
                                    <td>
                                        <input type="hidden" value="<?php echo $item->material_id; ?>" name="material[]"/>
                                        <input type="text" value="<?php echo html_escape($item->material_name) ?>" class="form-control autocomplete selfvalidate" placeholder="CHOOS MATERIAL" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
                                    </td>
                                    <td><input name="mDescription[]" type="text" placeholder="DESCRIPTION" class="form-control" value="<?php echo html_escape($item->description); ?>"></td>
                                    <td><input name="mRate[]" type="text" placeholder="RATE" class="form-control selfvalidate" value="<?php echo html_escape($item->rate); ?>"></td>
                                    <td><input name="mQuantity[]" type="text" placeholder="QUANITIY" class="form-control selfvalidate" value="<?php echo html_escape($item->qty); ?>"></td>
                                </tr>
                                <?php endforeach ?>
                                <?php }else{ ?>
                                <tr class="data-row">
                                    <td>
                                        <input type="hidden" value="" name="material[]"/>
                                        <input type="text" class="form-control autocomplete selfvalidate" placeholder="CHOOS MATERIAL" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">
                                    </td>
                                    <td><input name="mDescription[]" type="text" placeholder="DESCRIPTION" class="form-control" value="<?php ?>"></td>
                                    <td><input name="mRate[]" type="text" placeholder="RATE" class="form-control selfvalidate" value="<?php ?>"></td>
                                    <td><input name="mQuantity[]" type="text" placeholder="QUANITIY" class="form-control selfvalidate" value="<?php ?>"></td>
                                </tr>
                                <?php } ?>
                            </table>
                        </div> <!-- Responsive Table -->
                    </div> <!-- Entry Table col -->

                    <div class="form-group">
                        <div class="col-md-2 col-md-offset-5 text-center">
                            <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                        </div>
                    </div>

                </div> <!-- Inner row end -->

            </form>
        </div>
    </div>
</div>
<?php endif ?>

<style>
    

    #inputTable tr th:nth-child(4) {
        width: 100px;
    }
    #inputTable tr th:nth-child(3) {
        width: 100px;
    }
    #inputTable tr th:first-child {
        width: 300px;
    }

</style>


<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });

    function autocompleteInit () {
        $('.autocomplete').each(function(i, el){
            var el = $(el),
                url = el.data('method');

            el.autocomplete({
            
                source: function( request, response ) {
                    //console.log(el.data('method'));

                    jQuery.ajax({
                        type: "POST",
                        url: url,
                        dataType: 'json',
                        data: { term: request.term },
                        success: function(data) {
                            var parsed = JSON.parse(data);
                            var newArray = new Array();

                            parsed.forEach(function (entry) {
                                var newObject = {
                                    id: entry.id,
                                    label: entry.label
                                };
                                newArray.push(newObject);
                            });

                            response(newArray);

                        },

                        error: function(xhr, ajaxOptions, thrownError) {
                            console.log(url);
                            container.html('<h4 style="margin-top:10px; display:block; text-align:left"><i class="fa fa-warning txt-color-orangeDark"></i> Error 404! Page not found.</h4>');
                        },
                        async: false
                    });

                },

                minLength: 1,

                select: function( event, ui ) {
                    $(this).prev().prev().val(ui.item.id);
                 },

                focus: function (event, ui) {
                    $(this).prev().prev().val(ui.item.id);
                    $(this).val(ui.item.label);
                }

            });
        });

    }; // autocomplete init

    autocompleteInit();

    $('#add').on('click', function(event) {
        event.preventDefault();
        $('.autocomplete').autocomplete('destroy');
        var row = '<tr class="data-row">';
                row += '<td>';
                    row += '<input type="hidden" value="<?php ?>" name="material[]"/>';
                    row += '<input type="text" class="form-control autocomplete selfvalidate" placeholder="CHOOS MATERIAL" data-method="<?php echo base_url(); ?>index.php/autocomplete/material/">';
                row += '</td>';
                row += '<td><input name="mDescription[]" type="text" placeholder="DESCRIPTION" class="form-control" value="<?php ?>"></td>';
                row += '<td><input name="mRate[]" type="text" placeholder="RATE" class="form-control selfvalidate" value="<?php ?>"></td>';
                row += '<td><input name="mQuantity[]" type="text" placeholder="QUANITIY" class="form-control selfvalidate" value="<?php ?>"></td>';
            row += '</tr>';
        $('#inputTable').append($(row));
        autocompleteInit();
    });

    $('#delete').on('click', function(event) {
        event.preventDefault();
        $('.del-row').remove();

        $('#formToSubmit').find('tr.data-row').each(function(index, el) {

            if( $('#formToSubmit').find('tr.data-row').length > 1 ){

                    $('<span class="del-row fa fa-times"></span>')
                        .appendTo( $(this) )
                        .fadeIn('slow')
                        .click(function(event) {

                            if ( confirm("Are you sure to delete this entry?") ) {
                                $(this).parent('tr.data-row')
                                    .fadeOut('slow', function() {
                                        $(this).remove();
                                        $('.del-row').remove();
                                    });

                            };
                       
                        }); // click event on delete button


            } // if data row more than one
            else{
                alert("Sorry One Row Must Be Fill.");
            }

        });
        // console.log(x);
    });

</script>