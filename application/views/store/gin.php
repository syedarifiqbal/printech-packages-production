<?php
 $role = (isset($update) && $update==true)? 'updateGIN':'addGIN';
 $pageTitle = (isset($update) && $update==true)? 'Update':'New';
 $gin_id = (isset($update) && $update==true)? $gin->gin_id:'';
 // $hold = (isset($update) && $update==true && $po->hold==1)? "checked":'';
 $date = (isset($update) && $update==true)? PKdate($gin->date):'';
 // $supplier_id = (isset($update) && $update==true)? $po->supplier_id:'';
 // $supplier_name = (isset($update) && $update==true)? $po->supplier_name:'';
 $description = (isset($update) && $update==true)? $gin->description:'';
 $url = site_url();
 $actionLink = (isset($update) && $update==true) ? $url.'/Update/goods_issue_note/'.$gin->gin_id:$url.'/Add/goods_issue_note';

 if ($roles[$role]): 
echo $feed_back;
?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Goods Issue Note <i><?php echo $pageTitle; ?> </i></h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">

                            <div class="col-sm-3"> <!-- DATE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-cube"></i></div>
                                    <input name="date" type="text" data-date="true" placeholder="DATE" class="form-control required" value="<?php echo $date; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-3"> <!-- DATE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon">Issue To</div>
                                    <select name="issue_to" class="form-control">
                                        <option value="Printing">Printing</option>
                                        <option value="Lamination">Lamination</option>
                                    </select>
                                </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-10"> <!-- Description -->
                                <div class="input-group input-group-sm">
                                    <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                                    <input name="description" type="text" autofocus="true" placeholder="DESCRIPTION" class="form-control required" value="<?php echo $description; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /Description -->

                            <div class="col-sm-2 text-right"> <!-- Description -->
                                <div class="btn-group">
                                    <button type="button" id="add" class="btn btn-success fa fa-plus"></button>
                                    <button type="button" id="delete" class="btn btn-danger fa fa-trash"></button>
                                </div>
                            </div> <!-- /Description -->

                            <!-- Hidden Fields For User Experiense -->
                            <input type="hidden" id="poNumber" value="null">


                            <hr class="spacer"/>
                            <div class="col-sm-12"> <!-- Entry Table col -->
                                <div class="table-responsive">
                                    <table class="table" id="inputTable">
                                        <tr>
                                            <th>Material</th>
                                            <th>Description</th>
                                            <th>Defective</th>
                                            <th>Store</th>
                                            <th># Roll/Ctns</th>
                                            <th>Quantity</th>
                                            <th>Rate</th>
                                        </tr>
                                        <?php if ( isset($update) && $update==true ){ ?>
                                        <?php foreach ($gin_items as $item): ?>
                                        <tr class="data-row">
                                            <td>
                                                <div class="input-group">
                                                    <input type="hidden" value="<?php echo $item->material_id; ?>" name="material[]"/>
                                                    <p class="findMaterial selfvalidate" data-balance="<?php echo site_url('Balance/current_stock/'); ?>" data-method="<?php echo site_url('autocomplete/findMaterial/'); ?>"><?php echo html_escape($item->material_name) ?></p>
                                                </div>
                                            </td>
                                            <td><input name="mDescription[]" type="text" placeholder="DESCRIPTION" class="form-control" value="<?php echo html_escape($item->description); ?>"></td>
                                            <td>
                                                <input type="checkbox" value="1" name="mDefective[]" <?php echo ($item->defective==1) ? 'checked ':'' ?>id="fancy-checkbox"/>
                                                <label for="fancy-checkbox">Checkbox</label>
                                            </td>
                                            <td>
                                                <select name="mStore[]" class="form-control">
                                                <?php foreach ($stores as $store): ?>
                                                    <option value="<?php echo $store->store_id; ?>" <?php echo ($store->store_id==$item->store_id)?"selected":""; ?>><?php echo $store->store_name; ?></option>
                                                <?php endforeach ?>
                                                </select>
                                            </td>
                                            <td><input name="mRoll[]" type="text" placeholder="ROLL/CARTONS" class="form-control" value="<?php echo html_escape($item->roll_carton); ?>"></td>
                                            <td><input name="mQuantity[]" type="text" placeholder="QUANITIY" class="form-control selfvalidate" value="<?php echo html_escape($item->qty); ?>"></td>
                                            <td><input name="mRate[]" type="text" placeholder="RATE" class="form-control selfvalidate" value="<?php echo html_escape($item->rate); ?>"></td>
                                        </tr>
                                        <?php endforeach ?>
                                        <?php }else{ ?>
                                        <tr class="data-row">
                                            <td>
                                                <div class="input-group">
                                                    <input type="hidden" value="" name="material[]"/>
                                                    <p class="findMaterial selfvalidate" data-balance="<?php echo site_url('Balance/current_stock/'); ?>" data-method="<?php echo site_url('autocomplete/findMaterial/'); ?>">Choose Material</p>
                                                </div>
                                            </td>
                                            <td><input name="mDescription[]" type="text" placeholder="DESCRIPTION" class="form-control" value="<?php ?>"></td>
                                            <td>
                                                <input type="checkbox" value="1" name="mDefective[]" id="fancy-checkbox"/>
                                                <label for="fancy-checkbox">Checkbox</label>
                                            </td>
                                            <td>
                                                <select name="mStore[]" class="form-control">
                                                <?php foreach ($stores as $store): ?>
                                                    <option value="<?php echo $store->store_id; ?>"><?php echo $store->store_name; ?></option>
                                                <?php endforeach ?>
                                                </select>
                                            </td>
                                            <td><input name="mRoll[]" type="text" placeholder="ROLL/CARTONS" class="form-control selfvalidate" value=""></td>
                                            <td><input name="mQuantity[]" type="text" placeholder="QUANITIY" class="form-control selfvalidate mQty" value="<?php ?>"></td>
                                            <td><input name="mRate[]" type="text" placeholder="RATE" class="form-control selfvalidate mRate" value="<?php ?>"></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div> <!-- Responsive Table -->
                            </div> <!-- Entry Table col -->

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<?php endif ?>

<style>
    

    #inputTable tr th:nth-child(3){
        width: 50px;
    }
    #inputTable tr td:nth-child(3){
        padding: 0px 0 0 9px;
        margin-bottom: -5px;
    }
    #inputTable #fancy-checkbox + label {
        margin-top: 1px;
        margin-bottom: -3px;
    }
    #inputTable tr th:nth-child(4),
    #inputTable tr th:nth-child(5),
    #inputTable tr th:nth-child(6),
    #inputTable tr th:nth-child(7) {
        width: 100px;
    }
    #inputTable tr th:first-child {
        width: 300px;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });


    $('#add').on('click', function(event) {
        event.preventDefault();
        $('.autocomplete').autocomplete('destroy');
        var row = '<tr class="data-row">';
                row += '<td>';
                        row+='<div class="input-group">';
                        row+= '<input type="hidden" value="" name="material[]"/>';
                        row+= '<p class="findMaterial selfvalidate" data-method="<?php echo site_url("autocomplete/findMaterial/"); ?>">Choose Material</p>';
                        row+='</div>';
                        //row+= '<input type="text" class="form-control autocomplete selfvalidate" placeholder="CHOOS MATERIAL" data-method="<?php echo base_url(); ?>index.php/autocomplete/pending_po/">';
                    row += '</td>';
                    row+= '<td><input name="mDescription[]" type="text" placeholder="DESCRIPTION" class="form-control"></td>';
                    row += '<td>';
                        row += '<input type="checkbox" value="1" name="mDefective[]" id="fancy-checkbox"/>';
                        row += '<label for="fancy-checkbox">Checkbox</label>';
                    row += '</td>';
                    row += '<td>';
                        row+= '<select name="mStore[]" class="form-control">';
                        <?php foreach ($stores as $store): ?>
                            row+= '<option value="<?php echo $store->store_id; ?>"><?php echo $store->store_name; ?></option>';
                        <?php endforeach ?>
                        row+= '</select>';
                    row += '</td>';
                    row+= '<td><input name="mRoll[]" type="text" placeholder="ROLL/CARTONS" class="form-control selfvalidate" value=""></td>';
                    row+= '<td><input name="mQuantity[]" type="text" placeholder="QUANITIY" class="form-control selfvalidate mQty" value="<?php ?>"></td>';
                    row+= '<td><input name="mRate[]" type="text" placeholder="RATE" class="form-control selfvalidate mRate" value="<?php ?>"></td>';
                row += '</tr>'; 
        $('#inputTable').append($(row));
        autocompleteInit();
    });

    $('#delete').on('click', function(event) {
        event.preventDefault();
        $('.del-row').remove();

        $('#formToSubmit').find('tr.data-row').each(function(index, el) {

            if( $('#formToSubmit').find('tr.data-row').length > 1 ){

                    $('<span class="del-row fa fa-times"></span>')
                        .appendTo( $(this) )
                        .fadeIn('slow')
                        .click(function(event) {

                            if ( confirm("Are you sure to delete this entry?") ) {
                                $(this).parent('tr.data-row')
                                    .fadeOut('slow', function() {
                                        $(this).remove();
                                        $('.del-row').remove();
                                    });

                            };
                       
                        }); // click event on delete button


            } // if data row more than one
            else{
                alert("Sorry One Row Must Be Fill.");
            }

        });
        // console.log(x);
    });

</script>

