<div class="responsive-menu">
	<div class="responsive-menu-dropdown blue">
		<a title="" class="blue">MENU <i class="fa fa-align-justify"></i></a>
	</div>
	<ul>
		<li id="intro4"><a href="#" title=""><i class="fa fa-desktop"></i><span><i>4</i></span>Discover</a>
			<ul>
				<li><a href="dashboard.html" title="">Dashboard 1</a></li>
				<li><a href="dashboard2.html" title="">Dashboard 2</a></li>
				<li><a href="dashboard3.html" title="">Dashboard 3</a></li>
				<li><a href="dashboard4.html" title="">Dashboard 4</a></li>
			</ul>			
		</li>
		<li id="intro5"><a href="widget.html" title=""><i class="fa fa-heart-o"></i><span><i>20+</i></span>Widget</a></li>			
		<li><a href="#" title=""><i class="fa fa-tint"></i><span><i>12</i></span>Ui Kit</a>
			<ul>
				<li><a href="notifications.html" title="">Notifications</a></li>
				<li><a href="grids.html" title="">Grids</a></li>
				<li><a href="buttons.html" title="">Buttons</a></li>
				<li><a href="calendars.html" title="">Calendars</a></li>
				<li><a href="file-manager.html" title="">File Manager</a></li>
				<li><a href="gallery.html" title="">Gallery</a></li>
				<li><a href="slider.html" title="">Slider</a></li>
				<li><a href="page-tour.html" title="">Page Tour</a></li>
				<li><a href="collapse.html" title="">Collapse</a></li>
				<li><a href="range-slider.html" title="">Range Slider</a></li>
				<li><a href="typography.html" title="">Typography</a></li>
				<li><a href="tables.html" title="">Tables</a></li>

			</ul>
		</li>
		<li><a href="form.html" title=""><i class="fa fa-paperclip"></i>Form Stuff</a></li>
		<li><a href="charts.html" title=""><i class="fa fa-unlink"></i><span><i>5+</i></span>Charts</a></li>
		<li><a href="#" title=""><i class="fa fa-rocket"></i><span><i>8+</i></span>Pages</a>
			<ul>
				<li><a href="invoice.html" title="">Invoice</a></li>
				<li><a href="order-recieved.html" title="">Order Recieved</a></li>
				<li><a href="search-result.html" title="">Search Result</a></li>
				<li><a href="price-table.html" title="">Price Table</a></li>
				<li><a href="inbox.html" title="">Inbox</a></li>
				<li><a href="profile.html" title="">Profile</a></li>
				<li><a href="contact.html" title="">Contact Us</a></li>
				<li><a href="css-spinners.html" title="">Css Spinners</a></li>
			</ul>			
		</li>
		<li><a href="#" title=""><i class="fa fa-thumbs-o-up"></i><span><i>6+</i></span>Bonus</a>
			<ul>

				<li><a href="faq.html" title="">Faq</a></li>
				<li><a href="index.html" title="">Log in</a></li>
				<li><a href="blank.html" title="">blank</a></li>
				<li><a href="cart.html" title="">Cart</a></li>
				<li><a href="billing.html" title="">Billing</a></li>
				<li><a href="icons.html" title="">Icons</a></li>
			</ul>		
		</li>
	</ul>
</div>