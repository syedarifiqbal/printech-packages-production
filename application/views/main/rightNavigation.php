<div class="menu"><!-- Right Menu -->
    <div class="menu-profile" id="intro3">
        <?php $pic = ($this->session->userdata('avater_path')) ? 'profile_img/' . $this->session->userdata('avater_path') : 'sign-in.jpg';
        $rollSticker = 0;
        if ($this->session->userdata('avater_path')) {
            echo sprintf('<img src="%s" alt="PROFILE PICTURE" />', base_url('assets/img/' . $pic));
        } else {
            $name = explode(' ', str_replace('_', ' ', $this->session->userdata('user_name')));
            $abbr = '';
            foreach ($name as $n) {
                $abbr .= substr($n, 0, 1);
            }
            echo sprintf('<div class="pro-pic">%s</div>', $abbr);
        } ?>

        <span><a href="<?php echo site_url('Profile_Image'); ?>"><i class="fa fa-plus"></i></a></span>
        <div class="menu-profile-hover">
            <h1><i><?php echo ucwords($this->session->userdata('user_name')); ?></i> <!-- Kelly --></h1>
            <!-- <p><i class="fa fa-map-marker"></i>LONDON, UNITED KINGDOM</p> -->
            <a href="<?php echo base_url(); ?>index.php/user/logout/" title="Log Out From the Software."><i
                    class="fa fa-power-off"></i></a>
            <div class="menu-profile-btns">

                <h3>
                    <i class="fa fa-user blue"></i>
                    <a href="#" title="">PROFILE</a>
                </h3>
                <h3>
                    <i class="fa fa-inbox pink"></i>
                    <a href="<?php echo site_url('Dashboard/inbox/'); ?>" data-request="ajax"
                       title="">INBOX</a>
                </h3>

            </div>
        </div>
    </div>
    <ul>
        <li><a href="<?php echo site_url('Dashboard/') ?>" data-request="ajax" title="Dashboard"><i
                    class="fa fa-desktop"></i>Dashboard</a>
            <?php if ($this->session->userdata('section') == 'roto'): ?>
        </li>
        <li><a href="#" title=""><i class="fa fa-database"></i>Store</a>
            <ul>
                <?php /*if (isset($roles['addPurchaseOrder'])): ?>
				<li><a href="<?php echo site_url('add/purchase_order'); ?>" data-request="ajax" title="Generate New Purchase Order">Purchase Order</a></li>
			<?php endif ?>
				<li><a href="<?php echo site_url('maintenance/po_list'); ?>" data-request="ajax" title="Find all purchase order">PO List</a></li>
			<?php */
                if (isset($roles['addGRN'])): ?>
                    <li><a href="<?php echo site_url('add/goods_receive_note'); ?>" data-request="ajax"
                           title="Material Received">Goods Receive Note</a></li>
                <?php endif ?>
                <li><a href="<?php echo site_url('maintenance/grn_list'); ?>" data-request="ajax"
                       title="Find all Goods Received Notes">Receive List</a></li>
                <?php if (isset($roles['addGIN'])): ?>
                    <li><a href="<?php echo site_url('add/goods_issue_note'); ?>" data-request="ajax"
                           title="Material Issue To Production">Goods Issue</a></li>
                <?php endif ?>
                <li><a href="<?php echo site_url('maintenance/gin_list'); ?>" data-request="ajax"
                       title="Find all Goods Issue Notes">Issue List</a></li>
            </ul>
        </li>

        <li><a href="#" title="Visual Charts"><i class="fa fa-bar-chart-o"></i>Statistics</a>
            <ul>
                <li><a href="<?php echo site_url('statistics/order_chart'); ?>">Party Orders</a></li>
            </ul>
        </li>

        <li><a href="#" title=""><i class="fa fa-random"></i>Transitions</a>
            <ul>
                <?php if (User_Model::hasAccess('addRotoSaleOrder')): ?>
                    <li><a href="<?php echo site_url('add/sale_order'); ?>" data-request="ajax"
                           title="Sale Order">Sale Order</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess('viewRotoSaleOrder')): ?>
                    <li><a href="<?php echo site_url('maintenance/sale_order_list'); ?>" data-request="ajax"
                           title="Find all Sale order">Sale Order List</a></li>
                    <li><a href="<?php echo site_url('maintenance/completed_sale_order_list'); ?>"
                           data-request="ajax" title="Find Completed Sale Order">Completed Order List</a></li>

                    <li><a href="<?php echo site_url('maintenance/uncompleted_sale_order_list'); ?>"
                           data-request="ajax" title="Find Uncompleted Sale order">Uncompleted Order List</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess('addRotoPrintingEntry')): ?>
                    <li><a href="<?php echo site_url('add/printing_entry'); ?>" data-request="ajax"
                           title="New Printing Entry">Printing Entry</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess('viewRotoPrintingEntry')): ?>
                    <li><a href="<?php echo site_url('maintenance/printing_entry_list'); ?>" data-request="ajax"
                           title="Find all Printing List">Printing Entry List</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess('addRotoRewindingEntry')): ?>
                    <li><a href="<?php echo site_url('add/rewinding_entry'); ?>" data-request="ajax"
                           title="New Printing Entry">Rewinding Entry</a></li>
                <?php endif ?>
                <?php if ( User_Model::hasAccess('addSticker') && ((++$rollSticker)==1) ): ?>
                    <li><a href="<?php echo site_url('add/sticker_print/'); ?>" title="Generate Sticker Print">Roll Sticker Print</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess('viewRotoRewindingEntry')): ?>
                    <li><a href="<?php echo site_url('maintenance/rewinding_entry_list'); ?>" data-request="ajax"
                           title="Find all Rewinding List">Rewinding Entry List</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess('addRotoLaminationEntry')): ?>
                    <li><a href="<?php echo site_url('add/lamination_entry'); ?>" data-request="ajax"
                           title="New Printing Entry">Lamination Entry</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess('viewRotoLaminationEntry')): ?>
                    <li><a href="<?php echo site_url('maintenance/lamination_entry_list'); ?>" data-request="ajax"
                           title="Find all Lamiation List">Lamination Entry List</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess('addRotoSlittingEntry')): ?>
                    <li><a href="<?php echo site_url('add/slitting_entry'); ?>" data-request="ajax"
                           title="New Slitting Entry">Slitting Entry</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess('viewRotoSlittingEntry')): ?>
                    <li><a href="<?php echo site_url('maintenance/slitting_entry_list'); ?>" data-request="ajax"
                           title="Find all Slitting List">Slitting Entry List</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess('addSticker') && ((++$rollSticker)==1)): $rollSticker++ ?>
                    <li><a href="<?php echo site_url('add/sticker_print/'); ?>" title="Generate Sticker Print">Roll Sticker Print</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess('addRotoDispatchEntry')): ?>
                    <li><a href="<?php echo site_url('add/dispatch_entry'); ?>" data-request="ajax"
                           title="New Dispatch Entry">Dispatch Entry</a></li>
                    <?php if (User_Model::hasAccess('addSticker') && ((++$rollSticker)==1)): $rollSticker++ ?>
                        <li><a href="<?php echo site_url('add/sticker_print/'); ?>" title="Generate Sticker Print">Roll Sticker Print</a></li>
                    <?php endif ?>
                <?php endif ?>
                <?php if (User_Model::hasAccess('viewRotoDispatchEntry')): ?>
                    <li><a href="<?php echo site_url('maintenance/dispatch_entry_list'); ?>" data-request="ajax"
                           title="Find all Dispatch List">Dispatch Entry List</a></li>
                <?php endif ?>
            </ul>
        </li>
        <?php if (User_Model::hasAccess("rotoSaleOrderApprove")): ?>
            <li><a href="#" title="Approval Pending Job/Sale Order" class="menuNotification"
                   data-notify="<?php echo site_url('Notification/unapproved_sale_order/'); ?>"><i
                        class="fa fa-key"></i><span><i>20+</i></span>Approval</a>
                <ul>
                    <li><a href="<?php echo site_url('approval/sale_order'); ?>" data-request="ajax"
                           title="Pending Sale Order Approval List">Pending Sale Order</a></li>
                </ul>
            </li>
        <?php endif ?>
        <li><a href="#" title=""><i class="fa fa-random"></i>Reports</a>
            <ul>
                <?php if (User_Model::hasAccess("rotoPrintingReport")): ?>
                    <li><a href="<?php echo site_url('report/printing_monthly_report'); ?>"
                           data-request="ajax" title="Datewise Printing Production">Date wise Printing</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess("rotoRewindingReport")): ?>
                    <li><a href="<?php echo site_url('report/rewinding_monthly_report'); ?>"
                           data-request="ajax" title="Datewise Rewinding Production">Date wise Rewinding</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess("rotoLaminationReport")): ?>
                    <li><a href="<?php echo site_url('report/datewise_lamination'); ?>" data-request="ajax"
                           title="Datewise Lamination">Date wise Lamination</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess("rotoSlittingReport")): ?>
                    <li><a href="<?php echo site_url('report/slitting_monthly_report'); ?>"
                           data-request="ajax" title="Datewise Slitting">Date wise Slitting</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess("rotoDispatchReport")): ?>
                    <li><a href="<?php echo site_url('report/datewise_dispatch'); ?>" data-request="ajax">Datewise
                            Dispatch</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess("rotoOrderBalanecReport")): ?>
                    <li><a href="<?php echo site_url('report/order_balance'); ?>" data-request="ajax">PO wise
                            Order Balance</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess("rotoWastageReport")): ?>
                    <li><a href="<?php echo site_url('report/wastage_report'); ?>" data-request="ajax">Wastage Report</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess('rotoPartyWiseOrderBalanecReport')): ?>
                    <li><a href="<?php echo site_url('report/detail_order_balance'); ?>" data-request="ajax">Partywise Order Bal.</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess("rotoPrintingReport")): ?>
                    <li><a href="<?php echo site_url('report/production_report'); ?>"
                           data-request="ajax" title="Datewise Printing Production">Daily Production</a></li>
                <?php endif ?>
            </ul>
        </li>
        <li><a href="#" title=""><i class="fa fa-cogs"></i>Master</a>
            <ul>
                <?php if (User_Model::hasAccess("addRotoMaterial")): ?>
                    <li><a href="<?php echo site_url('Add/Material'); ?>" data-request="ajax"
                           title="Add New Mateial for later use.">New Material</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess("viewRotoMaterial")): ?>
                    <li><a href="<?php echo site_url('Maintenance/Material_list'); ?>" data-request="ajax"
                           title="View Material List.">Material List</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess("addRotoJobStructure")): ?>
                    <li><a href="<?php echo site_url('Add/job_structure'); ?>" data-request="ajax"
                           title="Add New Job Structure for later use.">New Job Structure</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess("viewRotoJobStructure")): ?>
                    <li><a href="<?php echo site_url('Maintenance/Structure_list'); ?>" data-request="ajax"
                           title="View Material List.">Job Structure List</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess("addRotoCustomer")): ?>
                    <li><a href="<?php echo site_url('Add/Customer'); ?>" data-request="ajax"
                           title="Add New Customer for later use.">Add Customer</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess("viewRotoCustomer")): ?>
                    <li><a href="<?php echo site_url('Maintenance/customer_list'); ?>" data-request="ajax"
                           title="View Customer List.">Customer List</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess("addRotoSupplier")): ?>
                    <li><a href="<?php echo site_url('Add/Supplier'); ?>" data-request="ajax"
                           title="Add New Supplier for later use.">Add Supplier</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess("viewRotoSupplier")): ?>
                    <li><a href="<?php echo site_url('Maintenance/supplier_list'); ?>" data-request="ajax"
                           title="View Supplier List.">Supplier List</a></li>
                <?php endif ?>


                <?php if (User_Model::hasAccess('addRotoCategory')): ?>
                    <li><a href="<?php echo site_url('Add/Category'); ?>" data-request="ajax"
                           title="Add New Product Category for later use.">Add Category</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess('viewRotoCategory')): ?>
                    <li><a href="<?php echo site_url('Maintenance/category_list'); ?>" data-request="ajax"
                           title="View Category List.">Category List</a></li>
                <?php endif ?>


                <?php if (User_Model::hasAccess('addRotoGroup')): ?>
                    <li><a href="<?php echo site_url('Add/Group'); ?>" data-request="ajax"
                           title="Add New Product Category for later use.">Add Group</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess('viewRotoGroup')): ?>
                    <li><a href="<?php echo site_url('Maintenance/group_list'); ?>" data-request="ajax"
                           title="View Group List.">Group List</a></li>
                <?php endif ?>


                <?php if (User_Model::hasAccess('addUser')): ?>
                    <li><a href="<?php echo site_url('Add/Register_user'); ?>" data-request="ajax"
                           title="Register New User">User Registration</a></li>
                <?php endif ?>
                <?php if (User_Model::hasAccess('viewUser')): ?>
                    <li><a href="<?php echo site_url('Maintenance/user_list'); ?>" data-request="ajax"
                           title="View Registered User.">User List</a></li>
                <?php endif ?>

                <?php if (User_Model::hasAccess('addAccountPermission')): ?>
                    <li><a href="<?php echo site_url('Account_Permission/'); ?>" data-request="ajax"
                           title="Register New User">Accounts Permissions</a></li>
                <?php endif ?>
            </ul>
        </li>
        <?php elseif ($this->session->userdata('section') == 'maintenance'): ?>
            <?php if (User_Model::hasAccess('maintenance_pending_request')): ?>
                <li><a href="<?php echo site_url('maintenance/maintenance_pending_list/'); ?>"
                       data-request="ajax" title="All Pending Request listed here"><i class="fa fa-clock-o"></i>Pending
                        Request</a></li>
            <?php endif ?>
            <li><a href="<?php echo site_url('Add/Maintenance_request/'); ?>" data-request="ajax"
                   title="Mainenance"><i class="fa fa-keyboard-o"></i>Request</a></li>
            <li><a href="<?php echo site_url('maintenance/maintenance_list/'); ?>" data-request="ajax"
                   title="All Request listed here"><i class="fa fa-file-o"></i>Requested List</a></li>

        <?php elseif ($this->session->userdata('section') == 'marketing'): ?>
            <li><a href="#" title="Quotation Forms"><i class="fa fa-file-o"></i>Quotation Forms</a>
                <ul>
                    <?php if (User_Model::hasAccess('addOffsetQuotation')): ?>
                        <li><a href="<?php echo site_url('Add/quotation/Offset/'); ?>" data-request="ajax"
                               title="Quotation For Offset">Offset</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess('addRotoQuotation')): ?>
                        <li><a href="<?php echo site_url('Add/quotation/Gravure/'); ?>" data-request="ajax"
                               title="Quotation For Gravure">Gravure</a></li>
                    <?php endif ?>
                </ul>
            </li>
            <li><a href="#" title="Quotation Forms"><i class="fa fa-list-ul"></i>Quotations</a>
                <ul>
                    <?php if (User_Model::hasAccess('viewOffsetQuotation')): ?>
                        <li><a href="<?php echo site_url('maintenance/quotation_list/Offset/'); ?>"
                               data-request="ajax" title="Quotation">Offset</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess('viewRotoQuotation')): ?>
                        <li><a href="<?php echo site_url('maintenance/quotation_list/Gravure/'); ?>"
                               data-request="ajax" title="Quotation">Gravure</a></li>
                    <?php endif ?>
                </ul>
            </li>

        <?php elseif ($this->session->userdata('section') == 'offset'): ?>

            <li><a href="#" title="Inventory"><i class="fa fa-cart-plus"></i>Purchases</a>
                <ul>
                    <?php if(User_Model::hasAccess('addOffsetPO')): ?>
                        <li><a href="<?php echo site_url('offset_purchase_order/add'); ?>" data-request="ajax">Purchase Order</a></li>
                    <?php endif; ?>
                    <?php if(User_Model::hasAccess('viewOffsetPO')): ?>
                        <li><a href="<?php echo site_url('offset_purchase_order/'); ?>" data-request="ajax">Purchase Order List</a></li>
                    <?php endif; ?>
                </ul>
            </li>

            <li><a href="#" title="Quotation Forms"><i class="fa fa-calculator"></i>Cost Management</a>
                <ul>
                    <?php if(User_Model::hasAccess('addOffsetCosting')): ?>
                        <li><a href="<?php echo site_url('Add/Offset_costing/'); ?>" data-request="ajax">Costing</a></li>
                    <?php endif; ?>
                    <?php if(User_Model::hasAccess('viewOffsetCosting')): ?>
                        <li><a href="<?php echo site_url('maintenance/offset_costing_list/'); ?>" data-request="ajax">Costing List</a></li>
                    <?php endif; ?>
                </ul>
            </li>

            <li><a href="#" title=""><i class="fa fa-random"></i>Transitions</a>
                <ul>
                    <?php if (User_Model::hasAccess('addOffsetSaleOrder')): ?>
                        <li><a href="<?php echo site_url('offset_sale_order/add'); ?>" data-request="ajax" title="Sale Order">Sale Order</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess('viewOffsetSaleOrder')): ?>
                        <li><a href="<?php echo site_url('offset_sale_order/lists'); ?>" data-request="ajax"
                               title="Sale Order">Sale Order List</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess('addOffsetJobcard')): ?>
                        <li><a href="<?php echo site_url('offset_job_card/add'); ?>" title="Job card">Job Card</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess('viewOffsetJobcard')): ?>
                        <li><a href="<?php echo site_url('offset_job_card/lists'); ?>" data-request="ajax"
                               title="Job card list">Job card List</a></li>
                    <?php endif ?>
                </ul>
            </li>

            <li><a href="#" title="All Master Files"><i class="fa fa-gears"></i>Masters</a>
                <ul>
                    <?php if (User_Model::hasAccess("addOffsetCategory")): ?>
                        <li><a href="<?php echo site_url('offset_category/add/'); ?>">Add Category</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess("viewOffsetCategory")): ?>
                        <li><a href="<?php echo site_url('offset_category/show_all/'); ?>"
                               data-request="ajax">Category List</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess("addOffsetGroup")): ?>
                        <li><a href="<?php echo site_url('offset_group/add/'); ?>">Add Group</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess("viewOffsetGroup")): ?>
                        <li><a href="<?php echo site_url('offset_group/show_all/'); ?>"
                               data-request="ajax">Group List</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess("addOffsetMaterial")): ?>
                        <li><a href="<?php echo site_url('offset_material/add/'); ?>">Add Material</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess("viewOffsetMaterial")): ?>
                        <li><a href="<?php echo site_url('offset_material/show_all/'); ?>"
                               data-request="ajax">Materials List</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess("addOffsetJobStructure")): ?>
                        <li><a href="<?php echo site_url('Offset_Order_Managment/job_structure/'); ?>">Job
                                Structure</a></li>
                    <?php endif ?>
                    <?php if (User_Model::hasAccess("viewOffsetJobStructure")): ?>
                        <li><a href="<?php echo site_url('Offset_Order_Managment/job_structure_list/'); ?>"
                               data-request="ajax">Structure List</a></li>
                    <?php endif ?>
                </ul>
            </li>

        <?php elseif ($this->session->userdata('section') == 'hr'): ?>
            <?php if (User_Model::hasAccess('addVocancy')): ?>
                <li><a href="<?php echo site_url('Vocancy/add_vocancy/'); ?>" title=""><i
                            class="fa fa-bullseye"></i>Vocancy</a>
                    <ul>
                        <li><a href="<?php echo site_url('Vocancy/add_vocancy/'); ?>"><i></i>Open
                                Vacancy</a></li>
                        <li><a href="<?php echo site_url('Vocancy/vocancy_list/'); ?>"><i></i>Vacancy
                                List</a></li>
                    </ul>
                </li>
            <?php endif ?>
            <li><a href="#" title=""><i class="fa fa-money"></i>Salary</a>
                <ul>
                    <?php if (User_Model::hasAccess('addAdvancedSalary')): ?>
                        <li><a href="#" title="">Advanced Salary</a></li>
                    <?php endif ?>
                    <li><a href="<?php echo site_url('Advanced_Salary/show_list/'); ?>" title="">Salary
                            List</a></li>
                    <li><a href="<?php echo site_url('Advanced_Salary/print_voucher/'); ?>" title="">Print
                            Advanced Voucher</a></li>
                </ul>
            </li>
            <li><a href="#" title=""><i class="fa fa-file-pdf-o"></i>Forms</a>
                <ul style="width: 606px;">
                    <?php
                    try {
                        $files = new RecursiveDirectoryIterator('assets/forms');
                        foreach ($files as $file) {
                            $file_name = $file->getFileName();
                            $link = $file_name;
                            if (is_dir($file_name)) {
                                continue;
                            }
                            $file_name = substr($file_name, strpos($file_name, '_'));
                            $file_name = str_replace('_', ' ', $file_name);
                            $file_name = str_replace('.pdf', '', $file_name);
                            echo '<li><a href="' . base_url('assets/forms/' . $link) . '" target="_blank">' . $file_name . '</a></li>';
                        }
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    }
                    ?>
                </ul>
            </li>
        <?php elseif ($this->session->userdata('section') == 'health_sefty'): ?>
            <?php if (array_key_exists('hse_view', $roles)): ?>
                <li><a href="<?php echo site_url('health_sefty/pendign_reviewed/'); ?>"
                       title="HSE Accident/Incident Investigation Report Generate" data-request="ajax"><i
                            class="fa fa-chain-broken"></i>H.S.E.</a></li>
            <?php endif; ?>
            <li><a href="<?php echo site_url('Health_Safety/accident_investigation/'); ?>"
                   title="HSE Accident/Incident Investigation Report Generate" data-request="ajax"><i
                        class="fa fa-chain-broken"></i>Invest.</a></li>
            <li><a href="#" title="Quotation Forms"><i class="fa fa-list-ul"></i>List</a></li>
        <?php else: ?>
            <li><a href="#" title=""><i class="fa fa-chain-broken"></i>NOT AVAILABLE</a></li>
        <?php endif; ?>
    </ul>
</div><!-- Right Menu -->