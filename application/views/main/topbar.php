
<header>
	<div id="siteURL" style="display:none;"><?php echo site_url(); ?></div>
	<div class="logo">
		<img src="<?php echo base_url(); ?>assets/images/logo.png" alt="" />
	</div>
	<div class="header-post">
		<!-- <a href="#add-post-title" data-toggle="modal" title="">POST <i class="fa fa-plus"></i></a> -->
		<ul>
		<?php if ($this->session->userdata('user_id')==1): ?>
		<!-- <li><a href="#" title="" data-tooltip="Refresh" data-placement="bottom"><i class="fa fa-refresh"></i></a></li>
		<li><a href="#" title="" data-tooltip="Custom" data-placement="bottom"><i class="fa fa-th-large"></i></a></li> -->
		<li class="upload-files-sec">
			<a href="<?php echo site_url('sharing/'); ?>" title="" data-tooltip="Uploaded Files List" data-placement="bottom" class=""><i class="fa fa-cloud-upload"></i></a>
			<a href="#" title="" data-tooltip="Upload Files" data-placement="bottom" class="upload-btn"><i class="fa fa-caret-down"></i></a>
		
		<div class="upload-files">
			<ul>
				<li>
					<p>Total Space 1GB</p>
					<div class="progress small-progress progress-striped active">
						<div style="width: 30%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar pink">
						</div>
					</div>
				</li>
				
				<li>
					<p>Used Space 0.2GB</p>
					<div class="progress small-progress progress-striped active">
						<div style="width: 58%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar yellow">
						</div>
					</div>
				</li>
				
				<li>
					<p>Total Space</p>
					<div class="progress small-progress  progress-striped active">
						<div style="width: 32%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar blue">
						</div>
					</div>
				</li>

				<li>
					<?php echo form_open_multipart('index.php/sharing/upload');?>

					<input type="file" name="userfile" size="20" />

					<br />

					<input type="submit" value="upload" class="btn btn-success" />

					</form>
				</li>
		<?php endif ?>
			
			</ul>
		</div>	
		</li>
		</ul>
	</div>
	<div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title" style="display: none;">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header blue">
				  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">&times;</button>
				  <h4 class="modal-title">Setting Section</h4>
				</div>
				<div class="modal-body">
				  <input type="text" placeholder="TITLE" />
				  <textarea placeholder="DESCRIPTION" rows="5"></textarea>
				</div>
				<div class="modal-footer">
				  <button data-dismiss="modal" class="btn btn-default black" type="button">Close</button>
				  <button class="btn btn-primary blue" type="button">Save changes</button>
				</div>
			</div><!-- /.modal-content -->
		</div>
	</div>
	<div class="header-alert">
		<ul>
			<?php if ($this->session->userdata('section')): ?>
				<!-- <li><a href="<?php echo site_url('dashboard/section/'); ?>" title=""><i class="fa fa-th-large"></i>Section</a></li> -->
			<?php endif ?>

			<?php if( $this->session->userdata('user_id') == 1 ): ?>
                <li role="presentation" class="dropdown">
                  <a href="<?php echo site_url('shared/settings/toggle_maintenance_model'); ?>" class="dropdown-toggle info-number">
                    <?php if( isset($this->maintenance_mode) && $this->maintenance_mode){ ?>
                      <i class="fa fa-toggle-on"></i>
										<?php }else{ ?>
											<i class="fa fa-toggle-off"></i>
                    <?php }?>
                    <span class="badge bg-green">M</span>
                  </a>
                </li>
            <?php endif; ?>
				
			<?php if ($this->session->userdata('section')): ?>
		<li><a title="Choose Section/Department" class="message-btn"><i class="fa fa-th-large"> </i> <?php echo get_current_department(); ?></a>
				<div class="message">
					<a title="" href="<?php echo site_url('dashboard/set_section/roto'); ?>">
						<i class="fa fa-print"> </i> Roto Gravure
					</a>
					<a title="" href="<?php echo site_url('dashboard/set_section/offset'); ?>">
						<i class="fa fa-cubes"> </i> Offset
					</a>
					<a title="" href="<?php echo site_url('dashboard/set_section/extrusion'); ?>">
						<i class="fa fa-server"> </i> Extrusion
					</a>
					<a title="" href="<?php echo site_url('dashboard/set_section/marketing'); ?>">
						<i class="fa fa-opencart"> </i> Marketing
					</a>
					<a title="" href="<?php echo site_url('dashboard/set_section/maintenance'); ?>">
						<i class="fa fa-wrench"> </i> Maintenance
					</a>
					<a title="" href="<?php echo site_url('dashboard/set_section/hr'); ?>">
						<i class="fa fa-user"> </i> H.R
					</a>
					<a title="" href="<?php echo site_url('dashboard/set_section/health_sefty'); ?>">
						<i class="fa fa-medkit"> </i> Health and Safety
					</a>
				</div>	
				<style>
					.header-alert li:last-child a:before{
						border-top:none !important;
					}
					.header-alert .message > a{
						font-size: 18px;
					}
				</style>

				<!-- <li><a title="" class="message-btn"><i class="fa fa-envelope"></i><span>3</span></a>
				<div class="message">
					<span>You have 3 New Messages</span>
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/images/notification1.jpg" alt="" />Hey! How are You Diana. I waiting for you.
					toe Check. <p><i class="fa fa-clock-o"></i>3:45pm</p></a>
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/images/notification1.jpg" alt="" />Please Can you Submit A file. I am From Korea
					toe Check. <p><i class="fa fa-clock-o"></i>1:40am</p></a>
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/images/notification1.jpg" alt="" />Hey Today is Party So you Will Have to Come <p><i class="fa fa-clock-o"></i>4 Hours ago</p></a>
					<a href="inbox.html" class="view-all">VIEW ALL MESSAGE</a>
				</div>	 -->			
			
			
			</li>
			<?php endif ?>
			<!-- <li><a title="" class="notification-btn"><i class="fa fa-bell"></i><span>4</span></a>
				<div class="notification">
					<span>You have 6 New Notification</span>
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/images/notification1.jpg" alt="" />Server 3 is Over Loader Pleas
					toe Check. <p><i class="fa fa-clock-o"></i>3:45pm</p></a>
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/images/notification1.jpg" alt="" />Server 10 is Over Loader Pleas
					toe Check. <p><i class="fa fa-clock-o"></i>1:40am</p></a>
					<a href="#" title=""><img src="<?php echo base_url(); ?>assets/images/notification1.jpg" alt="" />New User Registered Please Check This <p><i class="fa fa-clock-o"></i>4 Hours ago</p></a>
					<a href="#" class="view-all">VIEW ALL NOTIFICATIONS</a>
				</div>
			
			</li>	 -->				
		</ul>
	</div>
	<div class="right-bar-sec">
		<!-- <a href="#" title="" class="right-bar-btn"><i class="fa fa-bars rotator animation"></i></a>
		<a href="#" title="" class="right-bar-btn-mobile"><i class="fa fa-bars rotator animation"></i></a> -->
		<div id="scrollbox4" class="right-bar">
				
			<div class="my-account">
				<form />
					<input type="text" placeholder="SEARCH ACCOUNT" />
					<a href="" title="" data-tooltip="Account" data-placement="left"><i class="fa fa-cogs"></i></a>
				</form>
				<a id="account" class="toogle-head">ACCOUNT LIST<i class="fa fa-plus"></i></a>
				<div class="account2">
					<ul>
						<li>
						Private Office
							<div class="switch-account">
							  <input type="checkbox" id="check" />
							  <label for="check" class="switch">
								<span class="slide-account"></span>
							  </label>
							</div>
						</li>
						<li>
						Home Account
							<div class="switch-account">
								<input type="checkbox" id="check2" checked/="" />
							  <label for="check2" class="switch">
								<span class="slide-account"></span>
							  </label>
							</div>
						</li>
						<li>
						Personal Account
							<div class="switch-account">
								<input type="checkbox" id="check3" checked/="" />
							  <label for="check3" class="switch">
								<span class="slide-account"></span>
							  </label>
							</div>
						</li>
					</ul>
					
				<a href="#" title=""><i class="fa fa-plus"></i>ADD ACCOUNT</a>	
				</div>
			</div><!-- Add Account -->
			
			<div class="users-online">
				<a id="user-online" class="toogle-head">USERS ONLINE<i class="fa fa-plus"></i><span>26</span></a>
				<div class="user-online2">
					<ul>
						<li><img src="<?php echo base_url(); ?>assets/images/user1.jpg" alt="" /><h5><a href="#" title="">Johny Razell</a></h5><span class="offline">OFFLINE</span> </li>
						<li><img src="<?php echo base_url(); ?>assets/images/user2.jpg" alt="" /><h5><a href="#" title="">John Smith</a></h5><span class="online">ONLINE</span> </li>
						<li><img src="<?php echo base_url(); ?>assets/images/user3.jpg" alt="" /><h5><a href="#" title="">Doe Haxzer</a></h5><span class="online">ONLINE</span> </li>
						<li><img src="<?php echo base_url(); ?>assets/images/user4.jpg" alt="" /><h5><a href="#" title="">Karen Kelly</a></h5><span class="unread"><i>4</i></span> 
						<p>Hey! I am still waitin</p>
						</li>
					</ul>
				<a href="#" title=""><i>260</i>TOTAL MEMBER</a>
				</div>
			</div><!-- Users Online -->
			
			
			<div class="disk-usage-sec">
				<a id="disk-usage" class="toogle-head">USAGE<i class="fa fa-plus"></i></a>
				<div class="disk-usage">
					<p>1.31 GB of 1.50 GB used <i>75%</i></p>
					<div class="progress small-progress">
						<div style="width: 35%" class="progress-bar black">
						  <span class="sr-only">35% Complete (success)</span>
						</div>
						<div style="width: 20%" class="progress-bar blue">
						  <span class="sr-only">20% Complete (warning)</span>
						</div>
						<div style="width: 10%" class="progress-bar pink">
						  <span class="sr-only">10% Complete (danger)</span>
						</div>
					</div>						
				</div>
			</div><!-- Disk Usage -->
			
			
			<div class="pending-task-sec">
				<a id="pending-task" class="toogle-head">PENDING TASK<i class="fa fa-plus"></i></a>
				<div class="pending-task">
					<ul>
						<li><h6>Development</h6><span>75%</span><a href="#" title="" data-tooltip="Refresh" data-placement="left"><i class="fa fa-refresh"></i></a>
							<div class="progress small-progress">
								<div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar pink">
								</div>
							</div>							
						</li>
						
						<li><h6>Bug Fixes</h6><span>60%</span><a href="#" title="" data-tooltip="Refresh" data-placement="top"><i class="fa fa-refresh"></i></a>
							<div class="progress small-progress">
								<div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar blue">
								</div>
							</div>							
						</li>
						
						<li><h6>Javascript</h6><span>90%</span><a href="#" title="" data-tooltip="Refresh" data-placement="bottom"><i class="fa fa-refresh"></i></a>
							<div class="progress small-progress">
								<div style="width: 90%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar black">
								</div>
							</div>							
						</li>
						
						
					</ul>
				</div>
			</div><!-- Disk Usage -->
			
			
		</div><!-- Right Bar -->
	</div><!-- Right Bar Sec -->
</header><!-- Header -->
<?php if( $this->session->flashdata('expireNotification') ) { echo $this->session->flashdata('expireNotification'); } ?>
<?php if( $this->session->flashdata('userMsg') ) { echo $this->session->flashdata('userMsg'); } ?>
<div class="wrapper">
