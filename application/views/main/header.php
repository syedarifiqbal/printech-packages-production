<!DOCTYPE html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php echo  $site_title; ?></title>


<!-- <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=PT+Sans:700,400' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css' />
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600' rel='stylesheet' type='text/css' /> -->

<!-- Styles -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/Fonts_site/fonts_stylesheet.css" type="text/css" /><!-- Bootstrap -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/bootstrap.min.css" type="text/css" /><!-- Bootstrap -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" type="text/css" /><!-- Font Awesome -->
<link rel="stylesheet" href="<?php echo base_url().'assets/_css/jquery.ui.autocomplete.structure.css'; ?>">
<link rel="stylesheet" href="<?php echo base_url().'assets/_css/jquery-dtpicker.css'; ?>">

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/nv.d3.css" type="text/css" /><!-- VISITOR CHART -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/daterangepicker-bs3.css" type="text/css" media="all" /><!-- Date Range Picker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/animate.css" type="text/css" media="all" /><!-- Date Range Picker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/style.css" type="text/css" /><!-- Style -->	
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/jquery.notify.css" type="text/css" /><!-- Style -->	
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/responsive.css" type="text/css" /><!-- Responsive -->	
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/dataTables.css" type="text/css" media="all" /><!-- Date Range Picker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/weather.css" type="text/css" media="all" /><!-- Date Range Picker -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/dataTables.bootstrap.css" type="text/css" /><!-- Responsive -->	
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/dataTables.responsive.css" type="text/css" /><!-- Responsive -->	

<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/morris.css" type="text/css" /><!-- Morris chart -->	
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/_css/c3.min.css" type="text/css" /><!-- c3 chart -->	



 
<!-- Script -->
<script src="<?php echo base_url().'assets/js/vendor/modernizr-2.6.2.min.js'?>"></script>
<script src="<?php echo base_url(); ?>assets/_js/jquery-1.10.2.js" type="text/javascript"></script><!-- Jquery -->
<!-- <script src="<?php //echo base_url(); ?>assets/_js/d3.v2.js" type="text/javascript"></script>VISITOR CHART -->
<!-- <script src="<?php //echo base_url(); ?>assets/_js/nv.d3.js" type="text/javascript"></script>VISITOR CHART -->
<!-- <script src="<?php //echo base_url(); ?>assets/_js/live-updating-chart.js" type="text/javascript"></script>VISITOR CHART -->
<script src="<?php echo base_url(); ?>assets/_js/bootstrap.min.js" type="text/javascript"></script>

<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->

<script src="<?php echo base_url().'assets/js/jquery-ui-1.10.4.min.js'?>"></script>

<script src="<?php echo base_url(); ?>assets/_js/jquery.easypiechart.min.js"></script> <!-- Easy Pie Chart -->
<script src="<?php echo base_url(); ?>assets/_js/easy-pie-chart.js"></script> <!-- Easy Pie Chart -->

<script src="<?php echo base_url(); ?>assets/_js/skycons.js" type="text/javascript"></script> <!-- Skycons -->
<script src="<?php echo base_url(); ?>assets/_js/enscroll-0.5.2.min.js" type="text/javascript"></script> <!-- Custom Scroll bar -->
<script src="<?php echo base_url(); ?>assets/_js/moment.js" type="text/javascript"></script> <!-- Date Range Picker -->
<script src="<?php echo base_url(); ?>assets/_js/livestamp.min.js" type="text/javascript"></script> <!-- Date Range Picker -->
<script src="<?php echo base_url(); ?>assets/_js/daterangepicker.js" type="text/javascript"></script><!-- Date Range Picker -->
<script src="<?php echo base_url(); ?>assets/_js/carousal-plugins.js" type="text/javascript"></script><!-- Carousal Widget -->
<script src="<?php echo base_url(); ?>assets/_js/ticker.js" type="text/javascript"></script><!-- Ticker -->
<script src="<?php echo base_url(); ?>assets/_js/jquery.notify.js"></script><!-- notify -->
<script src="<?php echo base_url(); ?>assets/_js/jquery-dtpicker.js"></script><!-- jquery-dtpicker -->
<!-- <script src="<?php echo base_url(); ?>assets/_js/html5lightbox.js"></script>html5lightbox -->

<script type="text/javascript" src="<?php echo base_url(); ?>assets/_js/weather.js"></script>

<script src="<?php echo base_url(); ?>assets/_js/dataTables.js" type="text/javascript"></script><!-- dataTables -->
<script src="<?php echo base_url(); ?>assets/_js/dataTables.responsive.min.js" type="text/javascript"></script><!-- dataTables.responsive -->
<script src="<?php echo base_url(); ?>assets/_js/dataTables.bootstrap.js" type="text/javascript"></script><!-- dataTables.bootstrap -->
<script src="<?php echo base_url(); ?>assets/_js/script.js" type="text/javascript"></script><!-- script -->

<script src="<?php echo base_url(); ?>assets/_js/d3.min.js" type="text/javascript"></script><!-- script -->
<script src="<?php echo base_url(); ?>assets/_js/c3.min.js" type="text/javascript"></script><!-- script -->

<script src="<?php echo base_url(); ?>assets/_js/morris.min.js" type="text/javascript"></script><!-- script -->
<script src="<?php echo base_url(); ?>assets/_js/raphael-min.js" type="text/javascript"></script><!-- script -->


</head>
<body>
