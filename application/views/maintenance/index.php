
<div class="container">
	<div class="col-md-6">
		<div class="heading-sec">
			<h1>Dashboard <i>Welcome to Flat Lab</i></h1>
		</div>
	</div>
	<div class="col-md-6">
		<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
          <i class="fa fa-calendar-o icon-calendar icon-large"></i>
          <span></span> <b class="caret"></b>
       </div>
	</div>
	
	<div class="col-md-4">
		<div class="visitor-stats widget-body blue">
			<h6>
				WEDNESDAY
				<i>APRIL / 2013</i>
			</h6>
			<span>24%
			<i>VISITOR</i>
			</span>
			<div id="chart">
				<svg></svg>
			</div>
			<p>+ 16,582,00</p>
		</div>
			<div class="chart-tab">
				<div id="tabs-container">
					<div class="tab">
						<div id="tab-1" class="tab-content">
							<p>NEW </p>
							<div class="progress small-progress">
								<div style="width: 70%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar blue">
								</div>
							</div>	

							<p>LAST WEEK </p>
							<div class="progress small-progress">
								<div style="width: 30%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar black">
								</div>
							</div>	
						</div>
						<div id="tab-2" class="tab-content">
																<p>NEW </p>
							<div class="progress small-progress">
								<div style="width: 20%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar blue">
								</div>
							</div>	

							<p>LAST WEEK </p>
							<div class="progress small-progress">
								<div style="width: 90%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar black">
								</div>
							</div>	
						
						</div>
						<div id="tab-3" class="tab-content">
																<p>NEW </p>
							<div class="progress small-progress">
								<div style="width: 50%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar blue">
								</div>
							</div>	

							<p>LAST WEEK </p>
							<div class="progress small-progress">
								<div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar black">
								</div>
							</div>	
						</div>
					</div>
				</div>
					<ul class="tabs-menu">
						<li class="current"><a href="#tab-1">NEW VISITORS</a></li>
						<li><a href="#tab-2">RETURNING</a></li>
						<li><a href="#tab-3">LAST WEEK</a></li>
					</ul>
			</div>
	</div><!-- Widget Visitor -->
	
	<div class="col-md-3">
		<div class="stat-boxes widget-body">
			<span class="fa fa-shopping-cart black"></span>
			<h3 class="ticker--one">57</h3>
			<i>NEW ORDERS</i>
			
		</div>
	</div>
	
	<div class="col-md-3">
		<div class="stat-boxes widget-body">
			<span class="fa fa-usd green"></span>
			<h3 class="ticker--two">946</h3>
			<i>NEW SALES</i>
			
		</div>
	</div>
	
	<div class="col-md-2">
		<div class="stat-boxes widget-body real-time">
			<h3>36</h3>
			<i>REAL TIME</i>
			
		</div>
	</div>
	
	<div class="col-md-8">
		<div class="order-reviews widget-body">
			<div class="order-left yellow">
				<a href="#" title="" data-tooltip="$2,25824,000" data-placement="right"><i class="fa fa-thumbs-o-up"></i></a>
				<a href="#" title="" data-tooltip="$2,258,00" data-placement="right"><i class="fa fa-shield "></i></a>
				<a href="#" title="" data-tooltip="$89,003" data-placement="right"><i class="fa fa-thumbs-o-down "></i></a>
			</div>
				<div class="circle-sta">
					<div class="review chart-bg" data-percent="73">
						<span></span>
					</div>
					<span><i class="yellow"></i>REVIEWS</span>
				</div>
				
				<div class="circle-sta">
					<div class="earning chart-bg" data-percent="73">
						<span></span>
					</div>
					<span><i class="black"></i>Earning</span>
				</div>
			<div class="order-details">
				<p>TOTAl</p>
				<h4>$ 28,586,0.0</h4>
				<ul>
					<li>FROM MARKET
						<div class="progress small-progress">
							<div style="width: 60%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar black">
							</div>
						</div>	
					</li>
					<li>THIS WEEK
						<div class="progress small-progress">
							<div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar yellow">
							</div>
						</div>	
					</li>
					<li>THIS MONTH
						<div class="progress small-progress">
							<div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar black">
							</div>
						</div>	
					</li>
				</ul>
			</div>
		</div>
	</div><!-- ORDR REVIEWS -->

	<div class="col-md-5">
		<div class="timeline-sec widget-body">
			<div class="timeline-head pink">
				<span>Time line
					<i>27 - Oct - 2013</i>
				</span>
				<div class="add-btn">
					<a href="#" title=""><i class="fa fa-plus blue"></i>ADD USER</a>
				</div>
			</div>
			<div id="scrollbox3" class="timeline">
				<ul>
					<li>
						<div class="timeline-title gray">
							<h6>John Smith</h6>
							<i>2 min ago</i>
							<a href="#" title=""><i class="fa fa-thumbs-o-up"></i>LIKE</a><a href="#" title=""><i class="fa fa-comment-o"></i>REPLY</a>
						</div>
						<div class="timeline-content">
							<p>Dnim eiusmod high life accusamus terry richardson ado squid. 3 wolfmoon officia aute, non cupidatat</p>
						</div>
					</li>
					<li>
						<div class="timeline-title gray">
							<h6>John Smith</h6>
							<i>2 min ago</i>
							<a href="#" title=""><i class="fa fa-thumbs-o-up"></i>LIKE</a><a href="#" title=""><i class="fa fa-comment-o"></i>REPLY</a>
						</div>
						<div class="timeline-content">
							<p>Dnim eiusmod high life accusamus eiusmod high life accusamus terry terry. <i>ADMIN</i></p>
							<a title="LightBox Example" class="html5lightbox" href="images/timeline11.jpg"><i class="fa fa-search"></i><img src="images/timeline1.jpg" alt="" /></a>
							<a title="LightBox Example" class="html5lightbox" href="images/timeline22.jpg"><i class="fa fa-search"></i><img src="images/timeline2.jpg" alt="" /></a>
							<a title="LightBox Example" class="html5lightbox" href="images/timeline33.jpg"><i class="fa fa-search"></i><img src="images/timeline3.jpg" alt="" /></a>
						</div>
					</li>
					<li>
						<div class="timeline-title gray">
							<h6>John Smith</h6>
							<i>2 min ago</i>
							<a href="#" title=""><i class="fa fa-thumbs-o-up"></i>LIKE</a><a href="#" title=""><i class="fa fa-comment-o"></i>REPLY</a>
						</div>
						<div class="timeline-content">
							<p>Dnim eiusmod high life accusamus terry richardson ado squid. 3 wolfmoon officia aute, non cupidatat</p>
						</div>
					</li>
					<li>
						<div class="timeline-title gray">
							<h6>John Smith</h6>
							<i>2 min ago</i>
							<a href="#" title=""><i class="fa fa-thumbs-o-up"></i>LIKE</a><a href="#" title=""><i class="fa fa-comment-o"></i>REPLY</a>
						</div>
						<div class="timeline-content">
							<p>Dnim eiusmod high life accusamus terry. <i>ADMIN</i></p>
							<a title="LightBox Example" class="html5lightbox" href="images/timeline11.jpg"><i class="fa fa-search"></i><img src="images/timeline1.jpg" alt="" /></a>
							<a title="LightBox Example" class="html5lightbox" href="images/timeline22.jpg"><i class="fa fa-search"></i><img src="images/timeline2.jpg" alt="" /></a>
							<a title="LightBox Example" class="html5lightbox" href="images/timeline33.jpg"><i class="fa fa-search"></i><img src="images/timeline3.jpg" alt="" /></a>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>	<!-- TIME LINE -->
	
	<div class="col-md-4">
		<div class="recent-post-title widget-body  blue">
			<span><i class="fa fa-pencil"></i></span>
			<h2><i>Steve Jobs</i> Time Capsu le` is Finally Unearthed on <i>Skyace News</i></h2>
			<p class="pull-left"><i class="fa fa-clock-o"></i>26 minutes ago</p><p class="pull-right"><i class="fa fa-globe"></i>ABU DHABI</p>
		</div>
		<div class="recent-post">
			<img src="images/recent-post.jpg" alt="" />
			<p>Dnim eiusmod high life accusamus terry richardson ado squid. 3 wolfmoon officia aute, non cupidatat</p>
			<span><i class="fa fa-thumbs-o-up"></i>246 K</span>
			<span><i class="fa fa-heart"></i>2,520</span>
		</div>
	</div>	<!-- Recent Post -->
	
	<div class="col-md-3">
		<div class="twitter-box widget-body  black">
			<img src="images/twitter1.jpg" alt="" />
			<a href="#" title="">@MICK FOLEY</a>
			<div class="tweet-box">
				<i class="fa fa-twitter"></i>
				<a>Twitter <i>@MICK FOLEY</i> Dnim eiusmod high life accusamus.  </a>
			</div>
		</div>
	</div>	<!-- Twitter Widget -->
	
	<div class="col-md-3">
		<div class="weather-box widget-body  yellow">
			<div class="weather-day">
				<span>21</span>
				<p>THURSDAY</p>
			</div>
			
			<div class="weather-icon">
				 <canvas id="rain" width="64" height="64"></canvas>
				 <p>ABU DHABI</p>
			</div>
		</div>
	</div>	<!-- Weather Widget -->
	
</div><!-- Container -->