<!-- Tables -->
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Request <i>List, View and actions</i></h1>
            </div>
        </div>
        <div class="col-md-2">
            <a href="<?php echo site_url('Add/Maintenance_request'); ?>" data-request="ajax" class="btn btn-sm btn-primary" title="Add New Maintenance Request.">New</a>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Maintenance Request List</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->              
                    <table id="listTable" class="table table-striped table-bordered table-hover datatable-col-4">
                        <thead>
                            <tr>
                                <th>Req ID</th>
                                <th>Request Person</th>
                                <th>Request For</th>
                                <th>Problem</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($maintenance_list as $maint): ?>
                                <tr>
                                    <td><?php echo $maint->id; ?></td>
                                    <td><?php echo html_escape($maint->request_person); ?></td>
                                    <td><?php echo html_escape($maint->req_for); ?></td>
                                    <td><a href="#" data-tooltip="<?php echo html_escape($maint->problem); ?>" data-placement="left"><?php echo html_escape(substr($maint->problem, 0,50)); ?>...</a></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-gear"> </i> &nbsp;
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                <li><a href="<?php echo site_url('report/maintenance_request/'.$maint->id); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                                <li><a href="<?php echo site_url('Update/Maintenance_request/'.$maint->id); ?>" data-request="ajax" data-action="edit"><i class="fa fa-pencil"></i> Edit</a></li>
                                                <li><a href="<?php echo site_url('Delete/Maintenance_request/'.$maint->id); ?>" data-request="ajax" data-action="delete"><i class="fa fa-trash"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>
<style>
    td a[data-tooltip]::before {
        line-height: 1.5em;
        padding: 10px;
        background: rgba(0, 0, 0, 0.95);
        font-size: 14px;
        width: 400px;
        max-width: 400px;
        word-wrap: normal;
    }
</style>
<script>

    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-4').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "order": [[ 0, "desc"]],
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "aoColumns": [
                    { "sWidth": "10%" },
                    { "sWidth": "20%" },
                    { "sWidth": "20%" },
                    { "sWidth": "40%" },
                    { "sWidth": "10%", "sClass": "center td-sm", "bSortable": false },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>