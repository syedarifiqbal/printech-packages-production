<!-- Tables -->
<?php echo $feed_back; ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Goods Receiving Note <i>List, View and actions</i></h1>
            </div>
        </div>
        <div class="col-md-2">
            <?php if (isset($roles['addGRN'])): ?>
                <a href="<?php echo site_url('Add/goods_receive_note/'); ?>" data-request="ajax" class="btn btn-sm btn-primary" title="Add New Material Reveived Note.">New</a>
            <?php endif ?>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Goods Receiving Note</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->   
                    <table id="listTable" class="table table-striped table-bordered table-hover datatable-col-4">
                        <thead>
                            <tr>
                                <th>GRN #</th>
                                <th>Date</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($grnList as $grn): ?>
                                <tr>
                                    <td><?php echo $grn->grn_id; ?></td>
                                    <td><?php echo PKdate($grn->date); ?></td>
                                    <td><?php echo html_escape($grn->description); ?></td>
                                    <td>
                                        <div class="dropdown">
                                          <button class="btn btn-sm btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear"> </i> &nbsp;
                                             <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <li><a href="#"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                            <?php if ($roles['updateGRN']): ?>
                                                <li><a href="<?php echo site_url('Update/goods_receiving_note/'.$grn->grn_id); ?>" data-request="ajax" data-action="edit"><i class="fa fa-pencil"></i> Edit</a></li>
                                            <?php endif ?>
                                            <?php if ($roles['deleteGRN']): ?>
                                                <li><a href="<?php echo site_url('Delete/goods_receiving_note/'.$grn->grn_id); ?>" data-request="ajax" data-action="delete"><i class="fa fa-trash"></i> Delete</a></li>
                                            <?php endif ?>
                                          </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>



<script>

    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-4').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "aoColumns": [
                    { "sWidth": "8%" },
                    { "sWidth": "10%" },
                    { "sWidth": "72%" },
                    { "sWidth": "10%", "sClass": "center td-sm", "bSortable": false },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>