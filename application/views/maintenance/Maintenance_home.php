<div class="row">
	<div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 col-page">
		<div class="page-header">
			<h2><i class="fa fa-cubes"></i> Transactions</h2>
		</div>
		<ul class="list-group">
			<a href="<?php echo base_url(); ?>index.php/add/job_structure" class="list-group-item">
				<li>Create Job Structure</li>
			</a>
			<a href="<?php echo base_url(); ?>index.php/transaction/add_sale" class="list-group-item">
				<li>Sale</li>
			</a>
			<a href="" class="list-group-item">
				<li>Purchase Return</li>
			</a>
			<a href="" class="list-group-item">
				<li>Sale Return</li>
			</a>
		</ul>
	</div>

	<div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 col-page">
		<div class="page-header">
			<h2><i class="fa fa-list"></i> List</h2>
		</div>
		<ul class="list-group">
			<a href="<?php echo base_url(); ?>index.php/Maintenance/structure_list" class="list-group-item">
				<li>Job Structure List</li>
			</a>
			<a href="<?php echo base_url(); ?>index.php/Maintenance/material_list" class="list-group-item">
				<li>Material List</li>
			</a>
			<a href="<?php echo base_url(); ?>index.php/Maintenance/customer_list" class="list-group-item">
				<li>Customer List</li>
			</a>
			<a href="<?php echo base_url(); ?>index.php/Maintenance/supplier_list" class="list-group-item">
				<li>Supplier List</li>
			</a>
			<a href="<?php echo base_url(); ?>index.php/Maintenance/category_List" class="list-group-item">
				<li>Category List</li>
			</a>
			<a href="<?php echo base_url(); ?>index.php/Maintenance/group_List" class="list-group-item">
				<li>Group List</li>
			</a>
		</ul>
	</div>

	<div class="col-md-4 col-sm-6 col-xs-12 col-lg-4 col-page">
		<div class="page-header">
			<h2><i class="fa fa-cogs"></i> Maintenance</h2>
		</div>
		<ul class="list-group">
			<?php if (isset($roles['jobMasterFile'])): ?>
			<a href="<?php echo base_url(); ?>index.php/Add/Material" class="list-group-item">
				<li>Add Material Master</li>
			</a>
			<?php endif ?>
			<?php if ( isset($roles['addCustomer'])): ?>
			<a href="<?php echo base_url(); ?>index.php/Add/Customer" class="list-group-item">
				<li>Add Customer</li>
			</a>
			<?php endif; ?>
			<?php if ( isset($roles['addCustomer'])): ?>
			<a href="<?php echo base_url(); ?>index.php/Add/Supplier" class="list-group-item">
				<li>Add Supplier</li>
			</a>
			<?php endif; ?>
			<?php if ( isset($roles['addCategory'])): ?>
			<a href="<?php echo base_url(); ?>index.php/Add/Category" class="list-group-item">
				<li>Add Product Category</li>
			</a>
			<?php endif; ?>
			<?php if ( isset($roles['addGroup'])): ?>
			<a href="<?php echo base_url(); ?>index.php/Add/Group" class="list-group-item">
				<li>Add Product Group</li>
			</a>
			<?php endif; ?>
		</ul>
	</div>
</div>
