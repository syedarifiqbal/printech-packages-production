<?php
 $role = (isset($update) && $update==true)? 'editRotoCategory':'addRotoCategory';
 $pageTitle = (isset($update) && $update==true)? 'Update':'New';
 $name = (isset($update) && $update==true)? $cate->category_name:'';
 $density = (isset($update) && $update==true)? $cate->density:'';
 $description = (isset($update) && $update==true)? $cate->description:'';
 $url = site_url('');
 $actionLink = (isset($update) && $update==true) ? $url.'/Update/category/'.$cate->category_id:$url.'/Add/category';

 if (User_Model::hasAccess($role)): ?>
<?php echo $feed_back ?>

<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>Category</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        <fieldset>
                            <!-- <legend class="text-center header"><?php echo $pageTitle; ?></legend> -->

                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-cube"></i></div>
                                    <input id="fname" name="category_name" type="text" autofocus="true" placeholder="Category Name" class="form-control required" value="<?php echo $name ?>">
                                </div>
                                <br>
                                <div class="input-group">
                                    <div class="input-group-addon"><i class="fa fa-cube"></i></div>
                                    <input id="fname" name="density" type="text" autofocus="true" placeholder="Group Density" class="form-control required" value="<?php echo $density ?>">
                                </div>
                                <br>

                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-pencil-square-o"></i></span>
                                        <textarea class="form-control" name="description" placeholder="Enter your description for future indication here so that you can remind the propose of the Category." 
                                        rows="7"><?php echo trim($description) ?></textarea>
                                </div>
                                <br>
                                <div class="form-group">
                                    <div class="col-md-12 text-center">
                                        <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                    </div>
                                </div>
                        </fieldset>
                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>
<?php 
else:
    echo not_permitted();
endif; 
?>