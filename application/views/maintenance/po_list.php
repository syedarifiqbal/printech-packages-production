<!-- Tables -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="page-header">
                <?php if (isset($roles['addPurchaseOrder'])): ?>
                    <a href="Add/purchase_order" data-request="ajax" class="btn btn-sm btn-danger" title="Generate New Purchase Order.">New</a>
                <?php endif ?>
                <h1>Purchase Order List</h1>
            </div>
            <table class="table table-striped table-bordered table-hover datatable-col-4">
                <thead>
                    <tr>
                        <th>PO #</th>
                        <th>Date</th>
                        <th>Supplier</th>
                        <th>Description</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($pos as $po): ?>
                        <tr>
                            <td><?php echo $po->po_id; ?></td>
                            <td><?php echo PKdate($po->date); ?></td>
                            <td><?php echo html_escape($po->supplier_name); ?></td>
                            <td><?php echo html_escape($po->description); ?></td>
                            <td><?php echo ($po->status==1)? "HOLD":"OK"; ?></td>
                            <td>
                                <div class="dropdown">
                                  <button class="btn btn-sm btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-gear"> </i> &nbsp;
                                     <span class="caret"></span>
                                  </button>
                                  <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <li><a href="#"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                    <?php if ($roles['updatePurchaseOrder']): ?>
                                        <li><a href="Update/purchase_order/<?php echo $po->po_id; ?>" data-request="ajax" data-action="edit"><i class="fa fa-pencil"></i> Edit</a></li>
                                    <?php endif ?>
                                    <?php if ($roles['deletePurchaseOrder']): ?>
                                        <li><a href="Delete/purchase_order/<?php echo $po->po_id; ?>" data-request="ajax" data-action="delete"><i class="fa fa-trash"></i> Delete</a></li>
                                    <?php endif ?>
                                  </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>



<script>

    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-4').dataTable({
                "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                        "t"+
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "aoColumns": [
                    { "sWidth": "8%" },
                    { "sWidth": "10%" },
                    { "sWidth": "20%" },
                    { "sWidth": "47%" },
                    { "sWidth": "5%" },
                    { "sWidth": "10%", "sClass": "center td-sm", "bSortable": false },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>