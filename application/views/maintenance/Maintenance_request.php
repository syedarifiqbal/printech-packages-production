<?php
 $role = (isset($update) && $update==true)? 'Request Maintenance':'Update Request Maintenance';
 $pageTitle = (isset($update) && $update==true)? 'Update Request Maintenance':'Request For Maintenance';
 $id = (isset($update) && $update==true)? $req->id:'';
 // $date = (isset($update) && $update==true)? PKdate($req->date):'';
 $req_type = (isset($update) && $update==true)? $req->req_type:'Choose Type';
 $req_for = (isset($update) && $update==true)? $req->req_for:'Request For';
 $request_from = (isset($update) && $update==true)? $req->request_from:'';
 $request_person = (isset($update) && $update==true)? $req->request_person:'';
 $problem = (isset($update) && $update==true)? $req->problem:'';
 $suggestion = (isset($update) && $update==true)? $req->suggestion:'';
$url = base_url();
 $actionLink = (isset($update) && $update==true) ? $url.'/Update/maintenance_request/'.$req->id:$url.'/Add/maintenance_request';

 // if ($roles[$role]): ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>From</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">

                            <div class="col-sm-6 col-md-6"> <!-- DATE -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                        <select name="req_type" class="form-control required">
                                          <option value="" readonly>Select Request Type</option>
                                          <option <?php echo ($req_type=="urgent")?'selected':''; ?> value="urgent">Urgent</option>
                                          <option <?php echo ($req_type=="normal")?'selected':''; ?> value="normal">Normal</option>
                                        </select>
                                    </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-6 col-md-6"> <!-- REQUEST FOR -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                        <select name="req_for" class="form-control required">
                                          <option value="" readonly>Request For?</option>
                                          <option <?php echo ($req_for=="mechanical")?'selected':''; ?> value="mechanical">Mechanical</option>
                                          <option <?php echo ($req_for=="electrical")?'selected':''; ?> value="electrical">Electrical</option>
                                          <option <?php echo ($req_for=="utility")?'selected':''; ?> value="utility">Utility</option>
                                        </select>
                                    </div><hr class="spacer"/>
                            </div> <!-- /REQUEST FOR -->

                            <div class="col-sm-6 col-md-6"> <!-- REQUEST FROM -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-cube"></i></div>
                                    <input name="request_from" title="TYPE DEPART NAME..." type="text" placeholder="TYPE DEPART NAME" class="form-control required" value="<?php echo $request_from; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /REQUEST FROM -->

                            <div class="col-sm-12"> <!-- PROBLEM -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
                                    <textarea name="problem" placeholder="What is the problem Please Type here in detail." class="form-control required"><?php echo $problem ?></textarea>
                                </div><hr class="spacer"/>
                            </div> <!-- /PROBLEM -->

                            <div class="col-sm-12"> <!-- SUGGESTION -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-pencil"></i></div>
                                    <textarea name="suggestion" placeholder="What is the Possible solution (if any)" class="form-control"><?php echo $suggestion ?></textarea>
                                </div><hr class="spacer"/>
                            </div> <!-- /SUGGESTION -->

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    

    #inputTable tr th:nth-child(3){
        width: 50px;
    }
    #inputTable tr td:nth-child(3){
        padding: 0px 0 0 9px;
        margin-bottom: -5px;
    }
    #inputTable #fancy-checkbox + label {
        margin-top: 1px;
        margin-bottom: -3px;
    }
    #inputTable tr th:nth-child(1),
    #inputTable tr th:nth-child(2),
    #inputTable tr th:nth-child(3),
    #inputTable tr th:nth-child(4),
    #inputTable tr th:nth-child(5) {
        width: 150px;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });


</script>



<?php 
// else:
    // echo not_permitted();
// endif; 
?>