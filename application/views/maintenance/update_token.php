<style>
    
    body p {
        font-size: 22px;
        font-weight: bold;
        font-family: sans-serif;
        color: #666;
        background: #eee;
        padding: 10px;
        text-align: center;
    }

</style>

<?php echo $feed_back; ?>

<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Update Token!</h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Update Token</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->
                    <?php echo form_open(site_url('authentication/update_token'), 'class="form-horizontal"'); ?>
        
                        <p>Your key: <?php echo $auth->generated_key ?></p>
                        <br>
                        <div class="input-group">
                            <?php echo form_textarea('token','','placeholder="Enter your token code"'); ?>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="col-md-12 text-center">
                                <?php echo form_submit('submit','authorized','class="btn btn-primary btn-sm btn-block"'); ?>
                            </div>
                        </div>

                        <?php echo form_close(); ?>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>