<?php
if(!User_Model::hasAccess("viewRotoPrintingEntry")){
    $this->session->set_flashdata('userMsg', 'You do not have privilege to view list.');
    echo not_permitted();
    exit();
}
?>
<!-- Tables -->
<?php echo $feed_back; ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Printing <i>List, View and actions</i></h1>
            </div>
        </div>
        <div class="col-md-2">
            <?php if (User_Model::hasAccess("addRotoPrintingEntry")): ?>
                <a href="<?php echo site_url('Add/printing_entry'); ?>" data-request="ajax" class="btn btn-sm btn-primary" title="Add New Entry.">New</a>
            <?php endif ?>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Printing List</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <table id="listTable" class="table table-striped table-bordered table-hover datatable-col-4">
                        <thead>
                            <tr>
                                <th>REF#</th>
                                <th>JOB #</th>
                                <th>DATE</th>
                                <th>JOB_NAME</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($printing as $row): ?>
                                <tr>
                                    <td><?php echo $row->printing_id; ?></td>
                                    <td><?php echo html_escape($row->so); ?></td>
                                    <td><?php echo pkDate($row->date); ?></td>
                                    <td><?php echo html_escape($row->job_name); ?></td>
                                    <td>
                                        <div class="dropdown">
                                          <button class="btn btn-sm btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear"> </i> &nbsp;
                                             <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <li><a href="<?php echo site_url('view/printing_entry/'.$row->printing_id); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
                                            <?php if (User_Model::hasAccess("editRotoPrintingEntry")): ?>
                                                <li><a href="<?php echo site_url('Update/printing_entry/'.$row->printing_id); ?>" data-request="ajax" data-action="edit"><i class="fa fa-pencil"></i> Edit</a></li>
                                            <?php endif ?>
                                            <?php if (User_Model::hasAccess("deleteRotoPrintingEntry")): ?>
                                                <li><a href="<?php echo site_url('Delete/printing_entry/'.$row->printing_id); ?>" data-request="ajax" data-action="delete"><i class="fa fa-trash"></i> Delete</a></li>
                                            <?php endif ?>
                                            <!-- <li><a href="#">Separated link</a></li> -->
                                          </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>
// $('[data-toggle="tooltip"]').tooltip();
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-4').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "aoColumns": [
                    { "sWidth": "10%" },
                    { "sWidth": "10%" },
                    { "sWidth": "10%" },
                    { "sWidth": "50%" },
                    { "sWidth": "10%", "sClass": "center td-sm", "bSortable": false },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>