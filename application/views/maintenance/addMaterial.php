<?php  
    $role = (isset($update) && $update==true)? 'editRotoMaterial':'addRotoMaterial';
    $pageTitle = (isset($update) && $update==true)? 'Update':'New';
    $material_name = (isset($update) && $update==true)? $material->material_name:'';
    $unit = (isset($update) && $update==true)? $material->unit:'';
    $size = (isset($update) && $update==true)? $material->size:'';
    $thickness = (isset($update) && $update==true)? $material->thickness:'';
    $rate = (isset($update) && $update==true)? $material->rate:'';
    $density = (isset($update) && $update==true)? $material->density:'';
    $category_id = (isset($update) && $update==true)? $material->category_id:'';
    $group_id = (isset($update) && $update==true)? $material->group_id:'';
    $url = site_url();
    $actionLink = (isset($update) && $update==true) ? $url.'/Update/material/'.$material->material_id : $url.'/Add/material/';

if (User_Model::hasAccess($role)):
?>

<?php echo $feed_back ?>

<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>Material</i></h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        <fieldset>
                        <!-- PRODUCT NAME -->
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-circle-thin fa-fw"></i> Name</span>
                                <input class="form-control required" name="material_name" autofocus="true" type="text" value="<?php echo $material_name ?>" placeholder="Material Name">
                            </div>
                        <!-- PRODUCT UNIT --> <br />
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-tag fa-fw"></i> Unit</span>
                                <?php echo form_dropdown('unit',$units,$unit,'class="form-control"') ?>
                            </div>
                        <!-- PURCHASE PRICE --> <br />
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-rupee fa-fw"></i> List Price</span>
                                <input class="form-control" name="price" value="<?php echo $rate ?>" autofocus="true" type="text" placeholder="Purchase Price">
                            </div>
                        <!-- DENSITY --> <br />
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-minus fa-rotate-90 fa-fw"></i> Density</span>
                                <input class="form-control required" name="density" value="<?php echo $density ?>" type="text" placeholder="Density">
                            </div>
                        <!-- MICRON --> <br />
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-minus fa-fw"></i> Micron</span>
                                <input class="form-control required" name="micron" value="<?php echo $thickness ?>" type="text" placeholder="Micron (µ)">
                            </div>
                        <!-- MM --> <br />
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-bars fa-fw"></i> MM</span>
                                <input class="form-control required" name="size" value="<?php echo $size ?>" type="text" placeholder="MM">
                            </div>
                        <!-- CATEGORY NAME --> <br />
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-tag fa-fw"></i> Category</span>
                                <?php echo form_dropdown('category',$categories,$category_id,'class="form-control"') ?>
                            </div>
                        <!-- BRAND NAME --> <br />
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-tag fa-fw"></i> Group</span>
                                <?php echo form_dropdown('group',$groups,$group_id,'class="form-control"') ?>
                            </div>

                        <!-- SUBMIT BUTTON --> <br />
                            <div class="form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-lg">Save</button>
                                </div>
                            </div>
                        </fieldset>
                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    .header {
        color: #36A0FF;
        font-size: 27px;
        padding: 10px;
    }

    .bigicon {
        font-size: 35px;
        color: #36A0FF;
    }
    fieldset {
      border: none;
    }
</style>

<?php 
else:
    echo not_permitted();
endif; 
?>