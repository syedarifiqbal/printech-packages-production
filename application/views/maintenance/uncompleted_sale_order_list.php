<style>
    table.datatable-col-9 th,
        table.datatable-col-9 td {
            white-space: nowrap;
        }

</style>

<!-- Tables -->
<?php echo $feed_back; ?>
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>SALE ORDER LIST <i>filter and actions</i></h1>
            </div>
        </div>
        <div class="col-md-2">
            <?php if (isset($roles['addSaleOrder'])): ?>
                <a href="<?php echo site_url('add/sale_order'); ?>" data-request="ajax" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="left" title="Add New Sale Order.">New</a>
            <?php endif ?>
        </div>
    </div>
</div>
<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Structure List</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <table id="listTable" class="table table-striped table-bordered table-hover dt-responsive datatable-col-9">
                        <thead>
                            <tr>
                                <th>Job Code</th>
                                <th>Date</th>
                                <th>PO Date</th>
                                <th>Del Date</th>
                                <th>Customer</th>
                                <th>Job Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($so as $row): ?>
                                <tr>
                                    <td><?php echo $row->job_code; ?></td>
                                    <td><?php echo pkDate($row->date,"/"); ?></td>
                                    <td><?php echo pkDate($row->po_date,"/"); ?></td>
                                    <td><?php echo pkDate($row->delivery_date); ?></td>
                                    <td><?php echo $row->customer_name; ?></td>
                                    <td><?php echo $row->job_name; ?></td>
                                    <td><?php echo $row->hold==1?"HOLD":"OK"; ?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-gear"> </i> &nbsp;
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                <?php if (User_Model::hasAccess('viewRotoJobCard')): ?>
                                                    <li><a href="<?php echo site_url('report/job_card/'.$row->job_code); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> Job Card</a></li>
                                                <?php endif ?>
                                                <?php if (User_Model::hasAccess('viewRotoSaleOrder')): ?>
                                                    <li><a href="<?php echo site_url('report/sale_order/'.$row->job_code); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> Sale Order</a></li>
                                                <?php endif ?>
                                                <?php if (User_Model::hasAccess('editRotoSaleOrder')): ?>
                                                    <li><a href="<?php echo site_url('Update/sale_order/'.$row->job_code); ?>" data-request="ajax" data-action="edit"><i class="fa fa-pencil"></i> Edit</a></li>
                                                <?php endif ?>
                                                <?php if (User_Model::hasAccess('deleteRotoSaleOrder')): ?>
                                                    <li><a href="<?php echo site_url('Delete/sale_order/'.$row->job_code); ?>" data-request="ajax" data-action="delete"><i class="fa fa-trash"></i> Delete</a></li>
                                                <?php endif ?>
                                                <?php if (User_Model::hasAccess('canCompeteRotoSaleOrder')): ?>
                                                    <li><a href="<?php echo site_url('Approval/complete_job/'.$row->job_code); ?>"><i class="fa fa-check"></i> Complete?</a></li>
                                                <?php endif ?>
                                                <!-- <li><a href="#">Separated link</a></li> -->
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>

// $('[data-toggle=tooltip]').trigger('hover');

    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-9, table.tddd').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "bAutoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "responsive": true,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-9'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                },
                fnDrawCallback : function (oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
                
            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>