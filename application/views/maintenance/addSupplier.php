<?php
 $role = (isset($update) && $update==true)? 'editRotoSupplier':'addRotoSupplier';
 $pageTitle = (isset($update) && $update==true)? 'Update':'Add';
 $supplier_name = (isset($update) && $update==true)? $supplier->supplier_name:'';
 $checkedYes = (isset($update) && ($supplier->active==1) ) ? 'checked':'';
 $checkedNo = (isset($update) && ($supplier->active==0) ) ? 'checked':'';
 $url = site_url('');
 $actionLink = (isset($update) && $update==true) ? $url.'/Update/supplier/'.$supplier->supplier_id:$url.'/Add/supplier/';

 if (User_Model::hasAccess($role)): ?>
<?php echo $feed_back; ?>

<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>Supplier</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->
                
                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        <fieldset>
                        <!-- PRODUCT NAME -->
                            <div class="input-group margin-bottom-sm">
                                <span class="input-group-addon"><i class="fa fa-user fa-fw"></i></span>
                                <input class="form-control required" name="supplier_name" autofocus="true" type="text" value="<?php echo $supplier_name; ?>" placeholder="SUPPLIER NAME">
                            </div><br>
                        <!-- PRODUCT UNIT -->
                           <!--  <div id="radio">
                                <label>Active: </label>
                                <input type="radio" name="active" value="1" <?php echo $checkedYes; ?> id="radio1"><label for="radio1">Yes</label>
                                <input type="radio" name="active" value="0" <?php echo $checkedNo; ?> id="radio2"><label for="radio2">No</label>
                            </div> -->

                            <div style="text-align:center;">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-primary active">
                                        <input type="radio" name="active" value="1" id="option1" autocomplete="off" <?php echo $checkedYes; ?>> Active
                                    </label>
                                    <label class="btn btn-primary">
                                        <input type="radio" name="active" value="0" id="option2" autocomplete="off" <?php echo $checkedNo; ?>> Deactive
                                    </label>
                                </div>
                            </div>

                            <h3>Additional Options</h3>
                            <div class='contactField'>
                                <select class="form-control">
                                    <option value="mobile">Mobile</option>
                                    <option value="address">Address</option>
                                </select>
                                <input class="form-control iis_empty" name="mobile[]" autofocus="true" type="text" placeholder="Mobile">
                                <a href="#" class="removeFields btn btn-default fa fa-minus"></a>
                                <br>
                            </div>
                            <a href="#" id="addFields" class="btn btn-default fa fa-plus btn-block fa-2x"></a>
                            <br><br>
                            <button class="btn btn-success btn-block" type="submit" name="submit">Save</button>

                        </fieldset>
                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>


<style>
    .header {
        color: #36A0FF;
        font-size: 27px;
        padding: 10px;
    }

    .bigicon {
        font-size: 35px;
        color: #36A0FF;
    }
    fieldset {
      border: none;
    }
    .contactField {
        position: relative;
    }

    .contactField input{
        padding-right: 40px;
    }

    .contactField a {
        position: absolute;
        bottom: 23px;
        right: 3px;
    }
</style>

<script>

    $(function () {
        var val = '';

        $('#addFields').click(function (e) {
            e.preventDefault();
            var f = '';
                f += '<div class="contactField">';
                f +=    '<select class="form-control">';
                f +=    '    <option value="mobile">Mobile</option>';
                f +=    '    <option value="address">Address</option>';
                f +=    '</select>';
                f +=    '<input class="form-control iis_empty" name="mobile[]" autofocus="true" type="text" placeholder="Mobile">';
                f +=    '<a href="#" class="removeFields btn btn-default fa fa-minus"></a>';
            f +=    '<br>';
            f +=    '</div>';
            val = '';
            $(f).insertBefore($(this));
        });

        $('body').on('change','.contactField',function () {
            var $this = $(this),
                ph = 'Please Enter supplier '+$this.find('select option:selected').text(),
                name = $this.find('select option:selected').val()+'[]';

                val += $.trim( $this.children('input').val() );
            $this.children('input').attr({'placeholder':ph,'name':name});
            $this.children('input').val(val);
                val = '';

        });

        $('body').on('click','.removeFields',function (e) {
            e.preventDefault();
            if ( $(document).find('.contactField').length > 1 ) {

                $(this).parent().fadeOut('slow', function(){
                    $(this).remove();
                }); 

            };
            console.log( $(document).find('.contactField').length);

        });

    });

</script>

<?php 
else:
    echo not_permitted();
endif; 
?>