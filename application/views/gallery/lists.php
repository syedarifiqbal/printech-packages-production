<?php
if(!User_Model::hasAccess("viewOffsetJobStructure")){
    $this->session->set_flashdata('userMsg', 'You do not have privilege to view list.');
    echo not_permitted();
    exit();
}
?>
<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
<!-- Tables -->
<div class="container">
    

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Job Structure Gallery <i>(<?php echo $job->job_name; ?>)</i></h1>
            </div>
        </div>
        <div class="col-md-2">
            <?php if (User_Model::hasAccess("addOffsetJobStructure")): ?>
                <a href="<?php echo site_url("gallery/save/$context/$context_id"); ?>" class="btn btn-primary" title="Add New Job Structure.">New</a>
            <?php endif ?>
        </div>
    </div>

</div>

<?php echo $feed_back; ?>

<div class="container"> <!-- Container -->

    <div class="row"> <!-- Row -->

        <div class="col-lg-12"> <!-- col -->

            <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Gallery List</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <?php $this->load->view('gallery/table', array('records'=>$records)); ?>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>  <!-- Col -->

    </div> <!-- Row -->

</div> <!-- Container -->

<!-- Default bootstrap modal example -->
<div class="modal fade" id="photoModel" tabindex="-1" role="dialog" aria-labelledby="photoModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="photoModelLabel">Modal title</h4>
            </div>
            <div class="modal-body">
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<script>

    $('.delete').click(function(event) {
        if (!confirm('Do you want do delete gallery?')) {
            event.preventDefault();
        }
    });

    $("#photoModel").on("show.bs.modal", function(e) {
        var $link = $(e.relatedTarget);
        $('#photoModelLabel').text( $link.parents('tr').find('td:eq(0)').text() );
        $(this).find(".modal-body").load($link.attr("href"));
    });

    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-5').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "aoColumns": [
                    { "sWidth": "10%" },
                    { "sWidth": "30%" },
                    { "sWidth": "40%" },
                    { "sWidth": "10%", "sClass": "center td-sm", "bSortable": false },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-5'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>