<?php if (User_Model::hasAccess($record->id?'editOffsetMaterial' : 'addOffsetMaterial')): ?>

    <?php echo $feed_back ?>

    <div class="container"> <!-- this is from here -->
        <div class="row">
            <div class="col-md-10">
                <div class="heading-sec" id="intro6">
                    <h1><?php echo $record->id? "Edit ":"New "; ?> Gallery for <i><?php echo $job->job_name; ?></i></h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <form method="post" action="<?php echo site_url("gallery/gallery_description/$record->id"); ?>" enctype="multipart/form-data">
                <div class="col-md-12">
                    <div class="row grid">
                    <?php foreach ($images as $image): ?>
                        <div class="col-sm-4 grid-item">
                            <div class="box box-primary">
                                <div class="box-header with-border">
                                  <h3 class="box-title"><?php echo $image->title; ?> </h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="box-body">
                                
                                    <div class="image">
                                        <h4>
                                            <button type="button" class="btn btn-default pull-right deleteImage" data-image="<?php echo $image->id; ?>"><i class="fa fa-times"></i></button>
                                        </h4>
                                        <img src="<?php echo base_url("uploads/gallery/$image->image"); ?>" alt="<?php echo $image->title ?>" class="img-responsive img-thumbnail">
                                        <br>
                                        <div class="form-group">
                                            <label for="image-<?php echo $image->id ?>">Image Title:</label>
                                            <input type="text" class="form-control" name="data[<?php echo $image->id ?>][title]" id="image-<?php echo $image->id ?>" placeholder="Image Title" value="<?php echo set_value('data[<?php echo $image->id ?>][title]', $image->title) ?>">
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" rows="2" name="data[<?php echo $image->id ?>][description]" placeholder="Description..."><?php echo set_value('data[<?php echo $image->id ?>][description]', $image->description); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    <?php endforeach ?>
                    
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>


    <?php
else:
    echo not_permitted();
endif;
?>
<script>
    
$(function () {

    $('.deleteImage').on('click', function(event) {
        event.preventDefault();
        var $that = $(this);
        if (!confirm('Please make sure you want to delete this photo.!')) { return; }
        
        $.ajax({
            url: '<?php echo site_url( 'offset_order_managment/delete_gallery_image' ); ?>',
            type: 'POST',
            dataType: 'json',
            data: {image_id: $(this).data('image')},
        })
        .done(function(data) {
            if(data.status){
                $that.parent().parent().fadeOut('slow', function() {
                    $(this).remove();
                });;
            }
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

    });

});

</script>
