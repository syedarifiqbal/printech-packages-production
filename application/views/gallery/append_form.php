<?php if (User_Model::hasAccess($record->id?'editOffsetMaterial' : 'addOffsetMaterial')): ?>

    <?php echo $feed_back ?>

    <div class="container"> <!-- this is from here -->
        <div class="row">
            <div class="col-md-10">
                <div class="heading-sec" id="intro6">
                    <h1><?php echo $record->id? "Edit ":"New "; ?> Gallery for <i><?php echo $job->job_name; ?></i></h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <form method="post" action="<?php echo site_url("gallery/append_gallery/$record->id"); ?>" enctype="multipart/form-data">
                <div class="col-md-6">
                    <div class="widget-body custom-form">

                        <div class="form-group">
                            <label for="image">Choose Files</label>
                            <input type="file" class="form-control" id="imageFile" name="upl_files[]" multiple>
                        </div>

                        <button type="submit" name="submit" class="btn green btn-primary">Save</button>
                        
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Add Gallery Images</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <?php foreach ($images as $image): ?>
                                <div class="image">
                                    <h4>
                                        <?php echo $image->title; ?> 
                                        <button type="button" class="btn btn-danger pull-right deleteImage" data-image="<?php echo $image->id; ?>"><i class="fa fa-times"></i></button>
                                    </h4>
                                    <img src="<?php echo base_url("uploads/gallery/$image->image"); ?>" alt="<?php echo $image->title ?>" class="img-responsive img-thumbnail">
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>


    <?php
else:
    echo not_permitted();
endif;
?>
<script>
    
$(function () {

    $('.deleteImage').on('click', function(event) {
        event.preventDefault();
        var $that = $(this);
        if (!confirm('Please make sure you want to delete this photo.!')) { return; }
        
        $.ajax({
            url: '<?php echo site_url( 'gallery/delete_gallery_image' ); ?>',
            type: 'POST',
            dataType: 'json',
            data: {image_id: $(this).data('image')},
        })
        .done(function(data) {
            if(data.status){
                $that.parent().parent().fadeOut('slow', function() {
                    $(this).remove();
                });;
            }
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

    });

});

</script>
