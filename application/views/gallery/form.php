<?php if (User_Model::hasAccess($record->id?'editOffsetMaterial' : 'addOffsetMaterial')): ?>

    <?php echo $feed_back ?>

    <div class="container"> <!-- this is from here -->
        <div class="row">
            <div class="col-md-10">
                <div class="heading-sec" id="intro6">
                    <h1><?php echo $record->id? "Edit ":"New "; ?> Gallery for <i><?php echo $job->job_name; ?></i></h1>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">

            <form method="post" action="<?php echo site_url("gallery/save/$context/$context_id/$record->id"); ?>" enctype="multipart/form-data">
                <div class="col-md-6 col-md-offset-2">
                    <div class="widget-body custom-form">

                        <div class="form-group <?php echo form_error('data[name]')? 'has-error':''; ?>">
                            <label for="name">Gallery Name/Title</label>
                            <input type="text" class="form-control" name="data[name]" id="name" placeholder="Gallery Name/Title" value="<?php echo set_value('data[name]', $record->name); ?>">
                            <?php echo form_error('data[name]','<p class="error-msg">','</p>') ?>
                        </div>
    
                        <div class="form-group <?php echo form_error('data[description]')? 'has-error':''; ?>">
                            <label for="description">Description</label>
                            <textarea class="form-control" rows="3" name="data[description]" placeholder="Description..."><?php echo set_value('data[description]', $record->description); ?></textarea>
                            <?php echo form_error('data[description]','<p class="error-msg">','</p>') ?>
                        </div>
                        <?php if (!$record->id): ?>
                        <div class="form-group">
                            <label for="image">Choose Files</label>
                            <input type="file" class="form-control" id="imageFile" name="upl_files[]" multiple>
                        </div>
                        <?php endif ?>

                        <!-- <div class="form-group">
                            <label for="material_name">Material Name</label>
                            <input type="text" name="material_name" class="form-control required" id="material_name"
                                   value="<?php echo $name ?>" placeholder="Material Name"/>
                        </div> -->

                        <!-- <div class="form-group">
                            <label for="unit">Unit</label>
                            <?php echo form_dropdown('unit', $units, $unit, 'class="form-control" id="unit"') ?>
                        </div> -->

                        

                        <button type="submit" name="submit" class="btn green btn-primary">Save</button>
                        
                    </div>
                </div>

            </form>
        </div>
    </div>


    <?php
else:
    echo not_permitted();
endif;
?>
