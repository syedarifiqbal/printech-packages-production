<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo site_title(); ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="icon" 
      type="image/png" 
      href="<?php echo base_url(); ?>assets/img/favicon.ico">
     <!-- NORMALIZ.CSS -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/normalize.css'; ?>">
     <!-- BOOTSTRAP.CSS -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>">
     <!-- FONTAWESOME.CSS -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/font-awesome.min.css'; ?>">
     <!-- BOOTSTRAP.DATETIMEPICKER.CSS -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/_css/bootstrap-datetimepicker.css'; ?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/_css/bootstrap-datetimepicker-standalone.css'; ?>">
    
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/ui-lightness/jquery-ui-1.10.4.css'; ?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/jquery-ui.css'; ?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/dataTables.css" rel="stylesheet'; ?>">
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/main.css'; ?>">

    <script src="<?php echo base_url().'assets/js/vendor/modernizr-2.6.2.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/vendor/jquery-1.10.2.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery-ui-1.10.4.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap-datatimepicker.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/mousewheel.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.js'?>"></script>
        
    <script>
        /*$('html').hide();*/
    </script>
    <!-- WEB FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
</head>


<body>

<!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- ************************************ NAVIGATION ********************************************** -->
    <nav class="navbar navbar-default" role="navigation">
        <div class="container"> <!-- CONTAINER -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#collapse">
                    <span class="sr-only"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url(); ?>"><strong>PRINTECH</strong></a>
            </div>
            <div class="navbar-collapse collapse navbar-responsive-collapse" id="collapse">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo base_url(); ?>Printing/">Printing</a></li>
                    <li><a href="<?php echo base_url(); ?>Rewinding/">Rewinding</a></li>
                    <li><a href="<?php echo base_url(); ?>Rewinding/">Lamination</a></li>
                    <li><a href="<?php echo base_url(); ?>Rewinding/">Q.C</a></li>
                    <li><a href="<?php echo base_url(); ?>Rewinding/">Store</a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/Maintenance/">Maintenance</a></li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    <?php if( $this->session->userdata('logged_in') ): ?>
                    <li><a href="#"><span class="fa fa-user fa-1x"></span></a></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php echo $this->session->userdata('logged_in') ? strtoupper($this->session->userdata['user_name']):'lol'; ?> <b class="caret"></b></a>
                      <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url(); ?>index.php/select_period">Change Accounting Period</a></li>
                        <!-- <li><a href="#">Another action</a></li> -->
                        <!-- <li><a href="#">Something else here</a></li> -->
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url(); ?>index.php/user/logout"><i class="fa fa-toggle-on"></i> Log Out</a></li>
                      </ul>
                    </li>
                    <?php else: ?>
                        <li><a href="<?php echo base_url(); ?>index.php/user/login"><i class="fa fa-toggle-off"></i> Log in</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div> <!-- endCONTAINER -->
    </nav>

<!-- ************************************ CONTENT ********************************************** -->

<div class="container main"> <!-- CONTAINER -->
    <div class="row">
        <?php if ($this->session->flashdata('userMsg')){ ?>
            <div class="col-sm-6 col-sm-offset-3 alert alert-info alert-dismissible" style="display:none" id="userData">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo $this->session->flashdata('userMsg') ?>
            </div>
        <?php } ?>
    </div> <!-- .row -->