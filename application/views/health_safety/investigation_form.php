<?php
 $pageTitle = (isset($update) && $update==true)? 'Update Investigation':'Add New Investigation';
 $date = (isset($update) && $update==true)? $hse->date:'';
 $department = (isset($update) && $update==true)? $hse->department:'';
 $detail = (isset($update) && $update==true)? $hse->detail:'';
 $shift = (isset($update) && $update==true)? $hse->shift:'';
 $reported_person = (isset($update) && $update==true)? $hse->reported_person:'';
 $injured_person = (isset($update) && $update==true)? $hse->injured_person:'';
 $employee_id = (isset($update) && $update==true)? $hse->employee_id:'';
 $contractor_emp_id = (isset($update) && $update==true)? $hse->contractor_emp_id:'';
 $risk_category = (isset($update) && $update==true)? $hse->risk_category:'';
 $accident_category = (isset($update) && $update==true)? $hse->accident_category:'';
 $early_decision = (isset($update) && $update==true)? $hse->early_decision:'';
 $root_cause = (isset($update) && $update==true)? $hse->root_cause:'';
 $preventice = (isset($update) && $update==true)? $hse->preventice:'';
 $reviewed_by = (isset($update) && $update==true)? $hse->reviewed_by:'';
 $reviewed_time = (isset($update) && $update==true)? $hse->reviewed_time:'';
 $url = site_url('');
 $actionLink = (isset($update) && $update==true) ? $url.'/Health_Safety/accident_investigation/'.$hse->id:$url.'/Health_Safety/accident_investigation/';

echo $feed_back ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i>Customer</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-6 col-lg-offset-3 col-sm-8 col-sm-offset-2 col-xs-8 col-xs-offset-2">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->
                    <form class="form-horizontal" id="formToSubmits" method="post" action="<?php echo $actionLink; ?>">
                        <fieldset>
                        
                        <!-- DATE -->
                        <?php echo input(['name'=>'date','value'=>$date,'placeholder'=>'Select Date', 'dateTime'=>"true",' readonly'=>true],true,'','Date'); ?>
                        <?php 
                            $data['name'] = 'department';
                            $data['value'] = '';
                            $data['options'] = array(
                                'offset' => 'Offset',
                                'die_cutting' => 'Die Cutting',
                                'raw_material_store' => 'Raw Material Store',
                                'hr_accounts' => 'HR & Accounts',
                                'slitting' => 'Slitting',
                                'dispatch' => 'Dispatch',
                                'lamination' => 'Lamination',
                                'technical' => 'Technical',
                                'printing' => 'Printing',
                                'ink_store' => 'INK Store',
                                'extrusion' => 'Extrusion',
                                'bag_making' => 'Bag Making',
                                'center_seal' => 'Center Seal',
                                'offset_store' => 'Offset Store',
                                'corrugation1' => 'Corrugation 5th floor',
                                'corrugation2' => 'Corrugation 6th floor'
                                );
                        echo select($data,$required=true,'','Department'); ?>
                        <?php 
                            $data['name'] = 'risk_category';
                            $data['value'] = '';
                            $data['options'] = array(
                                'l' => 'Low',
                                'm' => 'Medium',
                                'h' => 'High'
                                );
                        echo select($data,$required=true,'','Risk Category'); ?>
                        <?php 
                            $data['name'] = 'accident_category';
                            $data['value'] = '';
                            $data['options'] = array(
                                'clipper' => 'Clipper/Sci',
                                'm' => 'Medium',
                                'h' => 'High'
                                );
                        echo select($data,$required=true,'','Accident Category'); ?>

                        <?php echo input(['name'=>'accident_category','value'=>$accident_category,'placeholder'=>'Accident Category'],true,'','Accident Category'); ?>

                        <?php echo input(['name'=>'early_decision','value'=>$early_decision,'placeholder'=>'Early Decision'],true,'','Early Decision'); ?>

                        <?php echo input(['name'=>'root_cause','value'=>$root_cause,'placeholder'=>'Root Cause'],true,'','Root Cause'); ?>

                        <?php echo input(['name'=>'preventice','value'=>$preventice,'placeholder'=>'Preventice'],true,'','Preventice'); ?>

                        <!-- SUBMIT -->
                        <input type="submit" name="submit" value="Submit" class="btn btn-primary">

                        </fieldset>
                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>
    
    $('[dateTime]').appendDtpicker();


</script>