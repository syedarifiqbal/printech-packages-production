<?php
 $role = (isset($update) && $update==true)? 'updateVocancy':'addVocancy';
 $pageTitle = (isset($update) && $update==true)? 'Update Vocancy':'New Vocancy';
 $id = (isset($update) && $update==true)? $record->id:'';
 $start_date = (isset($update) && $update==true)? PKdate($record->start_date):'';
 $end_date = (isset($update) && $update==true)? PKdate($record->end_date):'';
 $no_position = (isset($update) && $update==true)? $record->no_position:'';
 $position_name = (isset($update) && $update==true)? $record->position_name:'';
 $department = (isset($update) && $update==true)? $record->department:'';
 $description = (isset($update) && $update==true)? $record->description:'';
 $url = site_url();
 $actionLink = (isset($update) && $update==true) ? $url.'/Vocancy/update_vocancy/'.$record->id:$url.'/Vocancy/add_vocancy';
 if ($roles[$role]): 
    echo $feed_back;
?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?>  <i></i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle; ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">
                            
                            <div class="col-md-6 col-md-offset-3">                            
                            
                                <!-- START DATE -->
                                <?php echo input(['name'=>'start_date','value'=>$start_date,'placeholder'=>'START DATE', 'data-date'=>"true"],true,'','START DATE'); ?>

                                <!-- END DATE -->
                                <?php echo input(['name'=>'end_date','value'=>$end_date,'placeholder'=>'END DATE', 'data-date'=>"true"],true,'','END DATE'); ?>

                                <!-- REQUIRED POSITION -->
                                <?php echo input(['name'=>'position_name','value'=>$position_name,'placeholder'=>'REQUIRED POSITION'],true,'','Required Position'); ?>
                                
                                <?php echo select(['name'=>'no_position','options'=>str_split('12345'),'value'=>$no_position],true,'','No Positions'); ?>
                                
                                <?php echo select(['name'=>'department','options'=>array('Printing'=>'Printing','Offset'=>'Offset','Extrusion'=>'Extrusion','Marketing'=>'Marketing','QC'=>'QC','Store'=>'Store'),'value'=>$department],true,'','Department'); ?>
                                
                                <div class="input-group margin-bottom-sm">
                                    <span class="input-group-addon">Description</span>
                                    <textarea name="description" placeholder="Type Description Relevant to the JOB" id="description" rows="10"><?php echo $description; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    

   textarea#description {
        width: 100%;
        resize: none;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });


</script>



<?php 
else:
    echo not_permitted();
endif; 
?>