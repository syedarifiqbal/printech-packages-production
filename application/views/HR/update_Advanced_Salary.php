<?php

    if (!$record) {
        echo 'Something goes wrong.!';
        exit;
    }
    $department = $record->department;
    $actionLink = site_url('Advanced_salary/edit/'.$record->id);

 if ($roles['updateAdvancedSalary']): 
 echo $feed_back; ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1> Update Advanced Salary </h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Update</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">

                            <div class="col-sm-6 col-md-4"> <!-- DATE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input name="date" type="text" data-date="true" placeholder="DATE" class="form-control required" value="<?php echo $record->date; ?>">
                                </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-6 col-md-4"> <!-- SELECT MACHINE -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                    <select name="department" class="form-control required">
                                      <option value="" readonly>Select Department</option>
                                      <option <?php echo ($department=='printing')?'selected':''; ?> value="printing">Printing</option>
                                      <option <?php echo ($department=='offset')?'selected':''; ?> value="offset">Offset</option>
                                      <option <?php echo ($department=='extrusion')?'selected':''; ?> value="extrusion">Extrusion</option>
                                      <option <?php echo ($department=='store')?'selected':''; ?> value="store">Store</option>
                                    </select>
                                </div><hr class="spacer"/>
                            </div> <!-- /SELECT MACHINE -->

                        </div>

                        <div class="row">

                            <hr class="spacer"/>
                            <div class="col-sm-12"> <!-- Entry Table col -->
                                <div class="table-responsive">
                                    <table class="table" id="inputTable">
                                        <tr>
                                            <th>EMP NAME</th>
                                            <th>FATHER NAME</th>
                                            <th>CODE</th>
                                            <th>DESIGNATION</th>
                                            <th>AMOUNT</th>
                                        </tr>
                                        <tr class="data-row">
                                            <td><input name="name" type="text" placeholder="EMPLOYEE NAME" class="form-control selfvalidate" value="<?php echo $record->name ?>"></td>
                                            <td><input name="father_name" type="text" placeholder="FATHER NAME" class="form-control selfvalidate" value="<?php echo $record->father_name ?>"></td>
                                            <td><input name="code" type="text" placeholder="CODE" class="form-control selfvalidate" value="<?php echo $record->code ?>"></td>
                                            <td><input name="designation" type="text" placeholder="DESIGNATION" class="form-control selfvalidate" value="<?php echo $record->designation ?>"></td>
                                            <td><input name="amount" type="text" placeholder="AMOUNT" class="form-control selfvalidate mQty" value="<?php echo $record->amount ?>"></td>
                                        </tr>
                                    </table>
                                </div> <!-- Responsive Table -->
                            </div> <!-- Entry Table col -->

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    
    #inputTable #fancy-checkbox + label {
        margin-top: 1px;
        margin-bottom: -3px;
    }
    #inputTable tr th:nth-child(1),
    #inputTable tr th:nth-child(2),
    #inputTable tr th:nth-child(3),
    #inputTable tr th:nth-child(4),
    #inputTable tr th:nth-child(5) {
        width: 150px;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });

</script>


<?php 
else:
    echo not_permitted();
endif; 
?>