<?php
 $role = (isset($update) && $update==true)? 'updateAdvancedSalary':'addAdvancedSalary';
 $pageTitle = (isset($update) && $update==true)? 'Update Advanced Salary':'New Advanced Salary';

 $url = site_url('');
 $actionLink = (isset($update) && $update==true) ? $url.'/Advanced_salary/update/'.$record->id : $url.'/Advanced_salary/create';

 if ($roles[$role]): 
 echo $feed_back; ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1><?php echo $pageTitle; ?> </h1>
            </div>
        </div>
    </div>

</div>


<div class="container">

    <div class="row">

        <div class="col-xs-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2><?php echo $pageTitle . ' '; echo dbDate('08-01-2016'); ?></h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
                        
                        <div class="row">

                            <div class="col-sm-6 col-md-4"> <!-- DATE -->
                                <div class="input-group input-group-sm">
                                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    <input name="date" type="text" data-date="true" placeholder="DATE" class="form-control required" value="">
                                </div><hr class="spacer"/>
                            </div> <!-- /DATE -->

                            <div class="col-sm-6 col-md-4"> <!-- SELECT MACHINE -->
                                <div class="input-group input-group-sm margin-bottom-sm">
                                    <span class="input-group-addon"><i class="fa fa-building-o"></i></span>
                                    <select name="department" class="form-control required">
                                      <option value="" readonly>Select Department</option>
                                      <option value="printing">Printing</option>
                                      <option value="offset">Offset</option>
                                      <option value="extrusion">Extrusion</option>
                                      <option value="store">Store</option>
                                    </select>
                                </div><hr class="spacer"/>
                            </div> <!-- /SELECT MACHINE -->

                            <div class="col-sm-s text-right">
                                <div class="btn-group">
                                    <button type="button" id="add" class="btn btn-success fa fa-plus"></button>
                                    <button type="button" id="delete" class="btn btn-danger fa fa-trash"></button>
                                </div>
                            </div> <!-- /CONTROL BUTTONS -->
                        </div>

                        <div class="row">

                            <hr class="spacer"/>
                            <div class="col-sm-12"> <!-- Entry Table col -->
                                <div class="table-responsive">
                                    <table class="table" id="inputTable">
                                        <tr>
                                            <th>EMP NAME</th>
                                            <th>FATHER NAME</th>
                                            <th>CODE</th>
                                            <th>DESIGNATION</th>
                                            <th>AMOUNT</th>
                                        </tr>
                                        <?php if ( isset($update) && $update==true ){ ?>
                                        <?php foreach ($print_detail as $item): ?>
                                        <tr class="data-row">
                                            <td><input name="name[]" type="text" placeholder="EMPLOYEE NAME" class="form-control selfvalidate" value="<?php echo $item->name; ?>"></td>
                                            <td><input name="father_name[]" type="text" placeholder="FATHER NAME" class="form-control selfvalidate" value="<?php echo $item->father_name; ?>"></td>
                                            <td><input name="code[]" type="text" placeholder="CODE" class="form-control selfvalidate" value="<?php echo $item->code; ?>"></td>
                                            <td><input name="designation[]" type="text" placeholder="DESIGNATION" class="form-control selfvalidate" value="<?php echo $item->designation; ?>"></td>
                                            <td><input name="amount[]" type="text" placeholder="AMOUNT" class="form-control selfvalidate mQty" value="<?php echo $item->amount; ?>"></td>
                                        </tr>
                                        <?php endforeach; ?>
                                        <?php }else{ ?>
                                        <tr class="data-row">
                                            <td><input name="name[]" type="text" placeholder="EMPLOYEE NAME" class="form-control selfvalidate" value="<?php ?>"></td>
                                            <td><input name="father_name[]" type="text" placeholder="FATHER NAME" class="form-control selfvalidate" value="<?php ?>"></td>
                                            <td><input name="code[]" type="text" placeholder="CODE" class="form-control selfvalidate" value="<?php ?>"></td>
                                            <td><input name="designation[]" type="text" placeholder="DESIGNATION" class="form-control selfvalidate" value=""></td>
                                            <td><input name="amount[]" type="text" placeholder="AMOUNT" class="form-control selfvalidate mQty" value="<?php ?>"></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                </div> <!-- Responsive Table -->
                            </div> <!-- Entry Table col -->

                            <div class="form-group">
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save</button>
                                </div>
                            </div>

                        </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<style>
    
    #inputTable #fancy-checkbox + label {
        margin-top: 1px;
        margin-bottom: -3px;
    }
    #inputTable tr th:nth-child(1),
    #inputTable tr th:nth-child(2),
    #inputTable tr th:nth-child(3),
    #inputTable tr th:nth-child(4),
    #inputTable tr th:nth-child(5) {
        width: 150px;
    }

</style>

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });

    $('#add').on('click', function(event) {
        event.preventDefault();
        // $('.autocomplete').autocomplete('destroy');
        var row = '<tr class="data-row">';
               row += '<td><input name="name[]" type="text" placeholder="EMPLOYEE NAME" class="form-control selfvalidate" value="<?php ?>"></td>';
               row += '<td><input name="father_name[]" type="text" placeholder="FATHER NAME" class="form-control selfvalidate" value="<?php ?>"></td>';
               row += '<td><input name="code[]" type="text" placeholder="CODE" class="form-control selfvalidate" value="<?php ?>"></td>';
               row += '<td><input name="designation[]" type="text" placeholder="DESIGNATION" class="form-control selfvalidate" value=""></td>';
               row += '<td><input name="amount[]" type="text" placeholder="AMOUNT" class="form-control selfvalidate mQty" value="<?php ?>"></td>';
            row += '</tr>'; 
        $('#inputTable').append( $(row) );
        // $('.dateTime').appendDtpicker();
        // autocompleteInit();
    });

    $('#delete').on('click', function(event) {
        event.preventDefault();
        $('.del-row').remove();

        $('#formToSubmit').find('tr.data-row').each(function(index, el) {

            if( $('#formToSubmit').find('tr.data-row').length > 1 ){

                    $('<span class="del-row fa fa-times"></span>')
                        .appendTo( $(this) )
                        .fadeIn('slow')
                        .click(function(event) {

                            if ( confirm("Are you sure to delete this entry?") ) {
                                $(this).parent('tr.data-row')
                                    .fadeOut('slow', function() {
                                        $(this).remove();
                                        $('.del-row').remove();
                                    });

                            };
                       
                        }); // click event on delete button


            } // if data row more than one
            else{
                alert("Sorry One Row Must Be Fill.");
            }

        });
        // console.log(x);
    });

</script>


<?php 
else:
    echo not_permitted();
endif; 
?>