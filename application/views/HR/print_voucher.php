<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Print Advanced Salary <i>Voucher</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Filters</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body clearfix">  <!-- expandable BODY -->      

					<form target="_blank" action="<?php echo site_url('Advanced_Salary/print_voucher/') ?>" method="GET">
						
						<!-- <div class="col-md-offset-6 col-md-6 clearfix">
							<div id="daterange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
					          <i class="fa fa-calendar-o icon-calendar icon-large"></i>
					          <span></span> <b class="caret"></b>
					          <input type="hidden" id="from" name="from">
					          <input type="hidden" id="to" name="to">
					       </div>
						</div> <hr class="spacer"/> -->
					<div class="row">
						<div class="col-md-3 col-md-offset-3">
							<select name="department" class="form-control required">
	                            <option value="" readonly>Select Department</option>
	                            <option value="printing">Printing</option>
	                            <option value="offset">Offset</option>
	                            <option value="extrusion">Extrusion</option>
	                            <option value="store">Store</option>
                            </select>
						</div>

						<div class="col-md-3">
							<select name="month" class="form-control required">
	                            <option>Select Month</option>
	                            <?php foreach ($months as $value): ?>
	                            <option><?php echo $value->month; ?></option>
	                            <?php endforeach ?>
                            </select>
						</div>
					</div>

					<div class="row"><hr class="spacer"/><hr class="spacer"/>
						<div class="col-md-2 col-md-offset-5">
							<button type="submit" name="report" value="submit" class="btn btn-success btn-block">View</button>
						</div><hr class="spacer"/><hr class="spacer"/>
					</div>

					</form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>

	$('#daterange').daterangepicker(
	{
		startDate: moment().subtract('days', 29),
		endDate: moment(),
		minDate: moment().subtract('years', 1),
		maxDate: moment(),
		dateLimit: { days: 60 },
		showDropdowns: true,
		showWeekNumbers: true,
		timePicker: false,
		timePickerIncrement: 1,
		timePicker12Hour: true,
		ranges: {
		   'Today': [moment(), moment()],
		   'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
		   'Last 7 Days': [moment().subtract('days', 6), moment()],
		   'Last 30 Days': [moment().subtract('days', 29), moment()],
		   'This Month': [moment().startOf('month'), moment().endOf('month')],
		   'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		},
		opens: 'left',
		buttonClasses: ['btn btn-default'],
		applyClass: 'btn-small btn-primary',
		cancelClass: 'btn-small',
		format: 'DD/MM/YYYY',
		separator: ' to ',
		locale: {
			applyLabel: 'Submit',
			fromLabel: 'From',
			toLabel: 'To',
			customRangeLabel: 'Custom Range',
			daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
			monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
			firstDay: 1
			}
		},
		function(start, end) {
			$('#daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
			$('#from').val(start.format('YYYY-MM-DD'));
  			$('#to').val(end.format('YYYY-MM-DD'));
		}
  	);

  //Set the initial state of the picker label
  $('#daterange span').html(moment().subtract('days', 29).format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
  $('#from').val(moment().subtract('days',29).format('YYYY-MM-DD'));
  $('#to').val(moment().format('YYYY-MM-DD'));



    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-4, .datatable-col-4_2').dataTable({
                "autoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "aoColumns": [
                    { "sWidth": "20%" },
                    { "sWidth": "80%" },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });


</script>