<!-- Tables -->
<?php echo $feed_back; ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Vocancy <i>List, View and actions</i></h1>
            </div>
        </div>
        <div class="col-md-2">
            <?php if (isset($roles['addVocancy'])): ?>
                <a href="<?php echo site_url('Vocancy/add_vocancy'); ?>" data-request="ajax" class="btn btn-sm btn-primary" data-placement="left" data-toggle="tooltip" title="Add New Vocancy.">New</a>
            <?php endif ?>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Vocancy List</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <table id="listTable" class="table table-striped table-bordered table-hover datatable-col-4">
                        <thead>
                            <tr>
                                <th>REF#</th>
                                <th>START DATE</th>
                                <th>END DATE</th>
                                <th>REQUIRED POSITION</th>
                                <th>NO POSITION</th>
                                <th>STATUS</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($records as $row): ?>
                                <tr>
                                    <td><?php echo $row->id; ?></td>
                                    <td><?php echo pkDate($row->start_date, '/'); ?></td>
                                    <td><?php echo pkDate($row->end_date, '/'); ?></td>
                                    <td><?php echo html_escape($row->position_name); ?></td>
                                    <td><?php echo html_escape($row->no_position); ?></td>
                                    <td><?php echo ($row->position_register)? 'Completed': 'Open'; ?></td>
                                    <td>
                                    <?php if (!$row->position_register): ?>
                                        <div class="dropdown">
                                          <button class="btn btn-sm btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear"> </i> &nbsp;
                                             <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <!-- <li><a href="<?php echo site_url('view/rewinding_entry/'.$row->id); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li> -->
                                            <?php if ($roles['addVocancy']): ?>
                                                <li><a href="<?php echo site_url('Vocancy/complete_vocancy/'.$row->id); ?>" data-request="ajax" data-action="edit"><i class="fa fa-pencil"></i> Appointed?</a></li>
                                            <?php endif ?>
                                            <?php if ($roles['updateVocancy']): ?>
                                                <li><a href="<?php echo site_url('Vocancy/update_vocancy/'.$row->id); ?>" data-request="ajax" data-action="edit"><i class="fa fa-pencil"></i> Edit</a></li>
                                            <?php endif ?>
                                            <?php if ($roles['deleteVocancy']): ?>
                                                <li><a href="<?php echo site_url('Vocancy/delete_vocancy/'.$row->id); ?>" data-request="ajax" data-action="delete"><i class="fa fa-trash"></i> Delete</a></li>
                                            <?php endif ?>
                                            <!-- <li><a href="#">Separated link</a></li> -->
                                          </ul>
                                        </div>
                                    <?php endif ?>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>
// $('[data-toggle="tooltip"]').tooltip();
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-4').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "aoColumns": [
                    { "sWidth": "10%" },
                    { "sWidth": "10%" },
                    { "sWidth": "10%" },
                    { "sWidth": "30%" },
                    { "sWidth": "10%" },
                    { "sWidth": "10%" },
                    { "sWidth": "10%", "sClass": "center td-sm", "bSortable": false },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>