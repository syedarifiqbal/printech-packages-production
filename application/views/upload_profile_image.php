<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form action="<?php echo site_url('profile_image/upload') ?>" method="post" enctype="multipart/form-data" class="dropzone">
                <input type="file" name="file">
                <input type="submit" value="submit">
            </form>
        </div>
    </div>
</div>
<style>
    form.dropzone.dz-clickable > input[type=file],
    form.dropzone.dz-clickable > input[type=submit] {
        display: none;
    }
    .dz-default.dz-message {
         background: white;
         padding: 50px;
         border: 2px dashed grey;
         font-size: 26px;
     }
</style>