<style>
	.shared_files {
	    display: inline-block;
	    width: 150px;
	    height: 150px;
	    position: relative;
	}

	.shared_files:hover span {
	    background: rgba(0, 0, 0, 0.7);
	    color: rgba(255, 255, 255, 0.9);
	    width: 100%;
	    height: 100%;
	}

	.shared_files>span {
	    background: #ccc;
	    display: block;
	    position: absolute;
	    width: 100%;
	    height: 100%;
	    background: rgba(0, 0, 0, 0);
	    color: rgba(0, 0, 0, 0);
	    padding: 5px;
	    border: 1px solid #ddd;
	    transition: 0.3s all ease-in;
	    transform-origin: center;
	}

	.shared_files>span>h3 {
	    font-size: 12px;
	    text-align: center;
	}

	.shared_files>span>p {
	    font-size: 10px;
	    float: left;
	    width: 50%;
	    position: absolute;
	    bottom: 0;
	    left: 5px;
	    margin-bottom: 5px;
	}
	.shared_files>span>p:last-child{
	    left: auto;
	    right: 5px;
	    text-align: right;
	}
	.row{
		margin: 0;
	}
</style>
<div class="row">
	<div class="col-md-12">
	<?php if (isset($error)): ?>
		<div class="alert alert-danger">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			<strong>Error</strong> <?php echo $error; ?>
		</div>
	<?php endif ?>
<?php 
	foreach ($files as $file) {
		if (array_key_exists($file->file_type, $icons) && $icons[$file->file_type]) {
			$src = base_url( 'assets/images/'.$icons[$file->file_type] );
		}else{
			$src = base_url( 'sharing_on_server/'.$file->file_name );
		}
		echo '<div class="shared_files">';
		echo "<span>";
		echo "<h3>";
		echo $file->file_name;
		echo "</h3>";
		echo "<p>";
		echo pkDate($file->uploaded_time);
		echo "</p>";
		echo "<p>";
		echo $file->size.'K';
		echo "</p>";
		echo "</span>";
		echo '<img src="'.$src.'" width="150" height="150"/>';
		echo '</div>';
	}
 ?>
		
	</div>
</div>