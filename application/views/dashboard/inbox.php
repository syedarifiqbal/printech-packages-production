
			<div class="col-md-12 inbox-margin">
				<div class="col-md-3">
					<div class="inbox-panel black">
						<div class="inbox-panel-head">
							<!-- <img src="images/inbox-1.jpg" alt="" /> -->
							<h1><i>Users</i> </h1>
							<!-- <a href="profile.html" title=""><i class="fa fa-user"></i>Profile</a>
							<a href="profile.html" title=""><i class="fa fa-cog"></i>Setting</a> -->
						</div>
						
						<ul id="pm_users" data-url-msg="<?php echo site_url('widget/get_message_user') ?>">
							<?php 

								foreach ($users as $user) {
									$uName = ucfirst(str_replace("_", " ", $user->user_name));
									$image = ($user->avater_path) ? base_url('assets/img/profile_img/'.$user->avater_path):base_url('assets/img/sign-in.jpg');
									echo '<li><a href="#" data-to-u="'.$user->user_id.'" title="'.$uName.'"><img src="'.$image.'" width="30" height="30"> &nbsp; &nbsp; &nbsp;'.$uName.'</a></li>';
								}

							 ?>
						</ul>
						<!-- <div class="flaged">
							<h3><i class="fa fa-flag"></i>FLAGGED</h3>
							<ul>
								<li><a href="#" title=""><i class="fa fa-tag" style="color:#ff5c5c;"></i>Family</a></li>
								<li><a href="#" title=""><i class="fa fa-tag" style="color:#3ac1e3;"></i>Friends</a></li>
								<li><a href="#" title=""><i class="fa fa-tag" style="color:#f3d547;"></i>Private</a></li>
								<li><a href="#" title=""><i class="fa fa-tag" style="color:#b447f3;"></i>Office Staff</a></li>
							</ul>
						</div> -->
					</div>
				</div>
				
				<div class="col-md-4">
					<div class="all-emails">
						<!-- <form />
							<input type="text" placeholder="SEARCH EMAILS" />
							<a href="#" title=""><i class="fa fa-search"></i></a>
						</form> -->
						<ul id="scrollbox8">
							<?php if (empty($recent_messages)): ?>
								<?php echo '<li><h5><a href="#" title="">There Is No Conversation!</a></h5></li>' ?>
							<?php else: ?>
								<?php foreach ($recent_messages as $m): ?>
									<li>
									<?php $u = new User_Model(); $u->load($m->to) ?>
										<h5><a href="#" title=""><?php echo strtoupper(str_replace("_", " ", $u->user_name)); ?></a><i class="fa fa-paperclip"></i></h5>
										<span><i class="fa fa-clock-o"></i><span data-livestamp="<?php echo $m->timestamp; ?>"></span></span>
										<p><?php echo ($m->from==$this->session->userdata('user_id'))? "You: ". html_escape($m->message): html_escape($m->message); ?></p>
										<div class="email-hover">
											<a href="#" title="View"><i class="fa fa-eye"></i></a>
											<a href="#" title="Delete Conversation"><i class="fa fa-trash-o"></i></a>
											<a href="#" title="Flag"><i class="fa fa-star-o"></i></a>
										</div>
									</li>
								<?php endforeach ?>
							<?php endif ?>
							<li>
								<h5><a href="#" title="">Penacy Conol</a><i class="fa fa-paperclip"></i></h5>
								<span><i class="fa fa-clock-o"></i>2 min ago</span>
								<p>Search Results Page Design helps the user to narrow down the search results in a specific category. It not only sells out the products but ...</p>
								<div class="email-hover">
									<a href="#" title=""><i class="fa fa-eye"></i></a>
									<a href="#" title=""><i class="fa fa-trash-o"></i></a>
									<a href="#" title=""><i class="fa fa-star-o"></i></a>
								</div>
							</li>
						
						</ul>
					</div>
				</div>
				
				<div class="col-md-5">


					<div class="read-message" id="scrollbox6">

					<!-- THE MESSAGE WILL GOES HERE! -->

					</div>
					<div class="message-details">
						<div class="reply-sec">
						<a href="#" title=""><i class="fa fa-location-arrow"></i></a>
						<a href="#" title=""><i class="fa fa-microphone"></i></a>
						<a href="#" title=""><i class="fa fa-camera"></i></a>
						<a href="#" title=""><i class="fa fa-cloud-upload"></i></a>
						<form id="sendMSGs" method="POST" action="<?php echo site_url('widget/send_personal_message/'); ?>">
							<input type="text" name="message" id="writeMSG" autocomplete="off" placeholder="TYPE YOUR MESSAGE HERE" />
							<button type="submit" class="black"><i class="fa fa-comments-o"></i></button>
						</form>
					</div>
					</div>
				</div>
				
			</div>

<style>
	
	#pm_users li.active {
	    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.2);
	}
	.inbox-panel, .all-emails {
	    height: 590px;
	    overflow: auto;
	}
	.message-details:before {
	    background: none;
	    display: none;
	}
	.message-details>.reply-sec {
	    border-top: none;
	    color: white;
	}
	.message-details>.reply-sec *  {
	    color: white;
	}
	.message-details>.reply-sec input[type=text] {
	    color: #999;
	}
	.all-emails ul li h5.inactive:before {
	    background: transparent;
	    border: 1px solid #333;
	    width: 8px;
	    height: 8px;
	}
</style>

<script>

	var $msgBox = $('#scrollbox6');
	$msgBox.scrollTop($msgBox[0].scrollHeight);
	$msgBox.enscroll({
		showOnHover: true,
		verticalTrackClass: 'track3',
		verticalHandleClass: 'handle3'
	});
	
	$('#pm_users li').click(function(event) {
		event.preventDefault();
		$(this).addClass('active').siblings().removeClass('active');
		var url = $(this).parent().data('url-msg') + "/" + $(this).find('a').data('to-u');
		$.get(url, function(data) {
			console.log(data);
		});
	});

	$('#sendMSGs').submit(function(event) {
		event.preventDefault();
		console.log($(this).serialize());
		var actionLink = $(this).attr('action') + '/' + $('#pm_users').find('li.active a').data('to-u');
		if ($('#pm_users').find('li.active a').data('to-u')) {

			$.ajax({
				method: "POST",
				url: actionLink,
				// dataType: 'json',
				data: $(this).serialize(),
			})
			.done(function(data) {
				console.dir(data);
				console.log("success");
			})
			.fail(function(xhr, ajaxOptions, thrownError) {
	            console.log(xhr);
	            console.log(ajaxOptions);
	            console.log(thrownError);
	        	alert('some error here.');
	        });
			
		}else{
			console.log("Please Select User to send!");
		}

	});

</script>