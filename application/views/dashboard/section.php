<div class="container">

    <div class="row">

        <div class="col-xs-12">
            <section>
                <link rel="stylesheet" href="<?php echo base_url(); ?>assets/fonts/Fonts_site/fonts_stylesheet.css" type="text/css" />
                <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/_css/section.css'); ?>">
                <ul id='services'>
                    <h2>Choose Section</h2>
                    <li>
                        <a href="<?php echo site_url('dashboard/set_section/roto'); ?>">
                            <div class='fa fa-print'></div>
                            <span>ROTO</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('dashboard/set_section/offset'); ?>">
                            <div class='fa fa-cubes'></div>
                            <span>OFFSET</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('dashboard/set_section/extrusion'); ?>">
                            <div class='fa fa-server'></div>
                            <span>EXTRUSION</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('dashboard/set_section/marketing'); ?>">
                            <div class='fa fa-opencart'></div>
                            <span>MARKITING</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('dashboard/set_section/maintenance'); ?>">
                            <div class='fa fa-wrench'></div>
                            <span>MAINTENANCE</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('dashboard/set_section/hr'); ?>">
                            <div class='fa fa-user'></div>
                            <span>HR</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('dashboard/set_section/health_sefty'); ?>">
                            <div class='fa fa-medkit'></div>
                            <span>Health & Safety</span>
                        </a>
                    </li>
                </ul>
            </section>

        </div>

    </div>

</div>

