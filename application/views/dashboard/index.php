<div class="container">
	<div class="col-md-6">
		<div class="heading-sec">
			<h1>Dashboard <i>Welcome to Printech Packages Production Software.</i></h1>
		</div>
	</div>
	<div class="col-md-6">
		<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
          <i class="fa fa-calendar-o icon-calendar icon-large"></i>
          <span></span> <b class="caret"></b>
       </div>
	</div>
	
	<div class="col-md-6">
        <div class="visitor-stats widget-body blue">
            <h6><b class="day_name">SUNDAY</b> <i>APRIL / 2013</i></h6><span>$
            <i>106.3</i></span>
            <ul></ul>
        </div>
        <div class="chart-tab">
            <div id="tabs-container">
                <div class="tab">
                    <div class="tab-content" id="tab-1">
                        <p>NEW</p>
                        <div class="progress small-progress">
                            <div aria-valuemax="100" aria-valuemin="0"
                            aria-valuenow="40" class="progress-bar blue" role=
                            "progressbar" style="width: 70%"></div>
                        </div>
                        <p>LAST WEEK</p>
                        <div class="progress small-progress">
                            <div aria-valuemax="100" aria-valuemin="0"
                            aria-valuenow="40" class="progress-bar black" role=
                            "progressbar" style="width: 30%"></div>
                        </div>
                    </div>
                    <div class="tab-content" id="tab-2">
                        <p>NEW</p>
                        <div class="progress small-progress">
                            <div aria-valuemax="100" aria-valuemin="0"
                            aria-valuenow="40" class="progress-bar blue" role=
                            "progressbar" style="width: 20%"></div>
                        </div>
                        <p>LAST WEEK</p>
                        <div class="progress small-progress">
                            <div aria-valuemax="100" aria-valuemin="0"
                            aria-valuenow="40" class="progress-bar black" role=
                            "progressbar" style="width: 90%"></div>
                        </div>
                    </div>
                    <div class="tab-content" id="tab-3">
                        <p>NEW</p>
                        <div class="progress small-progress">
                            <div aria-valuemax="100" aria-valuemin="0"
                            aria-valuenow="40" class="progress-bar blue" role=
                            "progressbar" style="width: 50%"></div>
                        </div>
                        <p>LAST WEEK</p>
                        <div class="progress small-progress">
                            <div aria-valuemax="100" aria-valuemin="0"
                            aria-valuenow="40" class="progress-bar black" role=
                            "progressbar" style="width: 80%"></div>
                        </div>
                    </div>
                </div>
            </div>
            <ul class="tabs-menu">
                <li class="current">
                    <a href="#tab-1">RATE <?php echo date('l'); ?></a>
                </li>
                <li>
                    <a href="#tab-2">Pocket Guide</a>
                </li>
                <li>
                    <a href="#tab-3">Calculator</a>
                </li>
            </ul>
        </div>
    </div><!-- Widget Visitor -->

    <div class="col-md-4">
		<div class="recent-post-title widget-body blue">
			<span><i class="fa fa-pencil"></i></span>
			<?php if ($vacancy): ?>
				<h2><i>Job Vacancy</i> For <i><?php echo ($vacancy->position_name); ?></i></h2>
				<p class="pull-left"><i class="fa fa-clock-o"></i><i data-livestamp="<?php echo $vacancy->created_time; ?>"></i></p>
				<p class="pull-right"><i class="fa fa-globe"></i><?php echo $vacancy->department; ?></p>
			<?php else: ?>
				<h2><i>JOB VACANCY</i></h2>
			<?php endif ?>
		</div>
		<div class="recent-post">
			<!-- <img src="images/recent-post.jpg" alt="" /> -->
			<?php if ($vacancy): ?>
			<p><?php echo ($vacancy->description); ?></p>
			<?php else: ?>
				<p><i>There is no Job Available Currently</i></p>
			<?php endif ?>
			<!-- <span><i class="fa fa-thumbs-o-up"></i>246 K</span>
			<span><i class="fa fa-heart"></i>2,520</span> -->
		</div>
	</div>	<!-- Recent Post -->
	<?php //var_dump($roles) ?>
	
	<div class="col-md-6">
		<div class="chat-widget widget-body">
			<div class="chat-widget-head yellow">
				<h4>PRINTECH FOARM</h4>
				<div class="add-btn">
					<!-- <a href="#" title=""><i class="fa fa-plus pink"></i></a> -->
				</div>	
			</div>
			<ul id="scrollbox6">
				<?php foreach ($messages as $msg) {
					$reply = ($this->session->userdata('user_id')==$msg->user_id)?' class="reply"':'';
					$image = ($msg->avater_path) ? base_url('assets/img/profile_img/'.$msg->avater_path):base_url('assets/img/sign-in.jpg');
				echo '<li'.$reply.' id="'.$msg->msg_id.'">
						<div class="chat-thumb">
							<a href="#" data-tooltip="'.ucfirst(str_replace("_", " ", $msg->user_name)).'" data-placement="top"><img src="'.$image.'" width="61" height="61" alt="'.$msg->user_name.'"></a>
						</div>
						<div class="chat-desc">
							<p>'.$msg->message.'</p>
							<i class="chat-time" data-livestamp="'.$msg->timestump.'">Syed arif iqbal</i>
						</div>
					 </li>';
				} ?>
			</ul>
			<div class="reply-sec">
				<a href="#" title=""><i class="fa fa-location-arrow"></i></a>
				<a href="#" title=""><i class="fa fa-microphone"></i></a>
				<a href="#" title=""><i class="fa fa-camera"></i></a>
				<a href="#" title=""><i class="fa fa-cloud-upload"></i></a>
				<form id="sendMSG" method="POST" action="<?php echo site_url('widget/send_message/'); ?>">
					<input type="text" name="message" id="writeMSG" placeholder="TYPE YOUR MESSAGE HERE" />
					<button type="submit" class="black"><i class="fa fa-comments-o"></i></button>
				</form>
			</div>
		</div>
	</div><!-- Chat Widget -->
	
	<div class="col-md-4">
	        <div class="tdl-holder">
	            <h2 class="pink">TO DO LIST</h2>
	            <div class="tdl-content">
	                <ul>
	                    <?php foreach ($todolist as $list) {
	                        $checked = $list->done ? 'checked': '';
                        	echo '<li>
                                    <label>
                                        <input type="checkbox" value="'.$list->id.'" class="tdl-check" '.$checked.' /><i></i><span>'.$list->title.'</span>
                                            <a href="'.site_url("widget/delete_todolist/".$list->id).'" class="fa fa-trash delete-tdl"></a>
                                    </label>
                                </li>';
	                    } ?>
	                </ul>
	            </div>
	            <form action=<?php echo site_url('widget/todolist/'); ?>
	            id="todoForm" method="post" name="todoForm">
	                <input autocomplete="off" class="tdl-new" name="title"
	                placeholder="Write new item and hit 'Enter'..." type="text">
	                <button type="submit"><i class=
	                "todo-submit fa fa-send"></i></button>
	            </form>
	        </div>
	    </div><!-- Todo list  -->
	
	<div class="col-md-2">
		<div id="weather" class="weather-box widget-body yellow">
			<!-- <div class="weather-day">
				<span>21</span>
				<p>THURSDAY</p>
			</div>
			
			<div class="weather-icon">
				 <canvas id="rain" width="64" height="64"></canvas>
				 <p>ABU DHABI</p>
			</div> -->
		</div>
	</div>	<!-- Weather Widget -->
	
</div><!-- Container -->
<style>
	.tdl-holder input[type="text"]{
		width: 100%;
    	padding-right: 45px;
    	background-color: #F4F4F4;
	}
	.tdl-content{
	    max-height: 450px;
	    overflow: auto;
    	width: 100% !important;
	}
	.tdl-holder {
	    position: relative;
	}

	.tdl-holder button[type=submit] {
	    color: #3BBADB;
	    background-color: transparent;
	    border: none;
	    position: absolute;
	    right: 0;
	    bottom: 0;
	    line-height: 60px;
	    font-size: 22px;
	    padding-right: 15px;
	}
	.visitor-stats>ul {
	    list-style: none;
	    margin: 0;
	    clear: both;
	    margin-top: 60px;
	    padding: 0;
	}

	.visitor-stats>ul>li {
        color: white;
	    border-bottom: 1px solid rgba(255,255,255,0.25);
	    padding: 10px;
	    font-family: open sans;
	    font-size: 14px;
	    font-weight: 700;
	    letter-spacing: 0.3px;
	    line-height: 11px;
	    margin: 0;
	}
	a[data-tooltip][data-placement="top"]::before {
	    white-space: nowrap;
	    z-index: 10;
	}
	a[data-tooltip][data-placement="top"]::after{
		margin-bottom: 5px;
	}
	.recent-post > p{
		width: 100%;
	}
</style>
<!-- RAIn ANIMATED ICON-->
<script>
	var $msgBox = $('#scrollbox6');
	$msgBox.scrollTop($msgBox[0].scrollHeight);
	/*** Scrollbar Timeline ***/	
	$('#scrollbox3').enscroll({
		showOnHover: true,
		verticalTrackClass: 'track3',
		verticalHandleClass: 'handle3'
	});
	$msgBox.enscroll({
		showOnHover: true,
		verticalTrackClass: 'track3',
		verticalHandleClass: 'handle3'
	});
	$('.tdl-content').enscroll({
		showOnHover: true,
		verticalTrackClass: 'track3',
		verticalHandleClass: 'handle3'
	});

	$('#todoForm').submit(function(event) {
		event.preventDefault();
		if ( $.trim($('.tdl-new').val()) != '' ) {
			$.post($(this).attr('action'), $(this).serialize(), function(data, textStatus, xhr) {
				var data = jQuery.parseJSON(data);
				if(data.result=='success'){
					var todoeditor = $('.tdl-new');
					$('<li><label><input type="checkbox" value="'+data.title_id+'" class="tdl-check" /><i></i><span>'+data.label+'</span><a href="<?php echo site_url("widget/delete_todolist/") ?>/'+data.title_id+'" class="fa fa-trash-o delete-tdl"></a></label></li>')
					.hide().appendTo($('.tdl-content').find('ul'))
					.fadeIn('slow');
					todoeditor.val('');
					$('.tdl-content').animate({
						scrollTop: $('.tdl-content').height()
						},'slow');
				}
			}); // post
		}
	});

	$('.tdl-content').on('click','ul li .delete-tdl', function(event) {
		event.preventDefault();
		$this = $(this);
		$.ajax({
			url: $(this).attr('href'),
		})
		.done(function(data) {
			if(data=="success"){
				$this.closest('li').fadeOut('slow', function() {
					$(this).remove();
				});;
			}
		});
		
	});

	$('.tdl-content').on('change','.tdl-check',function(event){
		event.preventDefault();
		var $this = $(this);
		var undo = ($this.prop('checked')) ? "/true": "";
		$.ajax({
			url: '<?php echo site_url("widget/update_todolist") ?>/'+$this.val()+undo,
		})
		.done(function(data) {
			console.log(data);
			if(data=="unchecked"){
				$this.removeAttr('checked');
			}else if(data=="checked"){
				$this.attr('checked',true);
			}
		});
					// $this.attr('checked',true);
	});



	$('#sendMSG').submit(function(event) {
		event.preventDefault();
		if ( $.trim($('#writeMSG').val()) != '' ) {
			$.post($(this).attr('action'), $(this).serialize(), function(data, textStatus, xhr) {
				var data = jQuery.parseJSON(data);
				if(data.result=='success'){
					var writeMSG = $('#writeMSG');
					$('<li class="reply" id="'+data.message_id+'">' +
							'<div class="chat-thumb">'+
							'<a href="#" data-tooltip="<?php echo ucfirst(str_replace("_", " ", $this->session->userdata("user_name"))); ?>" data-placement="top"><img src="'+data.img_path+'" width="61" height="61" /></a></div>' +
							'<div class="chat-desc">' +
								'<p>'+data.message+'</p>' +
								'<i class="chat-time" data-livestamp="'+data.timestump+'"></i>' +
							'</div>' +
						'</li>')
					.hide().appendTo($('.chat-widget').find('ul'))
					.fadeIn('slow');
					writeMSG.val('');

					var winscroll = $(window).scrollTop();
					$msgBox.animate({
						scrollTop: $msgBox[0].scrollHeight
						},'slow');
					$(window).scrollTop(winscroll);

				} // data result success
			}); // post
		}
	});

	$('h6 .day_name').text(moment().format('dddd'));
	$('h6 i').text(moment().format('Do, MMMM / YYYY'));
	function get_currency () {
		$.getJSON('<?php echo site_url("dashboard/get_currency_rate"); ?>', function(json, textStatus) {
				// console.dir(json);
				$('.visitor-stats span:not(ul li span)').html("$ "+parseFloat(Math.round(json[0].Rate[0] * 100) / 100).toFixed(2)+"<i>PKR to USD</i>");
				$('.visitor-stats span i').text();
				$.each(json, function(index, val) {
					var value = parseFloat(Math.round(val.Rate[0] * 100) / 100).toFixed(2);
					$('.visitor-stats > ul').append('<li>'+val.Name+'<span>'+value+'</span></li>')
					// console.log(val);
				});
		});
	}
	get_currency();
	
	function loadWeather(woeid) {
		if( navigator.geolocation ) { 
			navigator.geolocation.getCurrentPosition(function(position) {
			    // loadWeather(position.coords.latitude+','+position.coords.longitude); //load weather using your lat/lng coordinates

			  $.simpleWeather({
				    location: position.coords.latitude+','+position.coords.longitude,
				    woeid: woeid,
				    unit: 'c',
				    success: function(weather) {
				     	html = '<h2><i class="icon-'+weather.code+'"></i> '+weather.temp+'&deg;'+weather.units.temp+'</h2>';
				     	html += '<ul><li>'+weather.city+', '+weather.region+'</li>';
				     	html += '<li class="currently">'+weather.currently+'</li>';
				     	html += '<li>'+weather.alt.temp+'&deg;C</li></ul>';  

				      	$("#weather").html(html);
				    },
				    error: function(error) {
				      $("#weather").html('<p>'+error+'</p>');
				    }
				});
			});

		} else {
			$("#weather").html('<p>Your Browser Not Supported Weather Features.</p>');
		}
	}

	loadWeather(''); //@params location, woeid

</script>