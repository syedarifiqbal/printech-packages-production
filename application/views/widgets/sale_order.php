<link rel="stylesheet" href="<?php echo base_url('assets/_css/tokenize.css'); ?>">
<style>
	#chartwrapper{
	    /*margin: 25px;*/
	    background: white;
	    padding: 25px 10px;
	    border-radius: 10px;
	}
</style>


<div class="container">
	<button>transform to chart</button>
	<div class="row">
		<div class="col-md-6">
        	<?php echo input(['name'=>'date','placeholder'=>'From', 'id'=>'fromDate', 'data-date'=>"true",' readonly'=>true],true,'','From'); ?>
		</div>
		<div class="col-md-6">
	        <?php echo input(['name'=>'date','placeholder'=>'To', 'id'=>'toDate', 'data-date'=>"true",' readonly'=>true],true,'','To'); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
	        <?php 
	            // echo '<select id="customersOption">';
	            // echo $customers_option;
	            // echo '</select>';
	        	// echo select($data,$required=true,'','Chosse Customer');
	        ?>
		</div>
		<div class="col-md-6">
			<?php 
	            $data['name'] = 'order_type';
	            $data['value'] = '';
	            $data['options'] = array(
	                'kg' => 'Only KG',
	                'pcs' => 'Only PCS'
	                );
	        	echo select($data,$required=true,'','Select Order Type');
	        ?>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div id="chartwrapper">
				<div id="chart"></div>
			</div>
		</div>
	</div>
</div>

 <input type="hidden" id="chart_location" value="<?php echo site_url('widget/chart'); ?>" />
 <script src="<?php echo base_url('assets/_js/my_charts.js'); ?>"></script>
 <script src="<?php echo base_url('assets/_js/tokenize.js'); ?>"></script>
