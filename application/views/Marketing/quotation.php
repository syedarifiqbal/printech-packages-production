<?php
    if (strtolower($form_type) =='offset'){
        $role = (isset($update) && $update==true)? 'editOffsetQuotation':'addOffsetQuotation';
    }elseif(strtolower($form_type) =='gravure'){
        $role = (isset($update) && $update==true)? 'editRotoQuotation':'addRotoQuotation';
    }
    $pageTitle = (isset($update) && $update==true)? 'Update':'New';
    $id = (isset($update) && $update==true)? $quotation->id:'';
    $gender = (isset($update) && $update==true)? $quotation->gender:'';
    $party = (isset($update) && $update==true)? $quotation->party:'';
    $attention = (isset($update) && $update==true)? $quotation->attention:'';
    $date = (isset($update) && $update==true)? $quotation->date:'';
    $product = (isset($update) && $update==true)? $quotation->product:'';
    $structure = (isset($update) && $update==true)? $quotation->structure:'';
    $printing = (isset($update) && $update==true)? $quotation->printing:'';
    $gsm = (isset($update) && $update==true)? $quotation->gsm:'';
    $quality = (isset($update) && $update==true)? $quotation->quality:'';
    $other_specification = (isset($update) && $update==true)? $quotation->other_specification:'';
    $finishing = (isset($update) && $update==true)? $quotation->finishing:'';
    $price = (isset($update) && $update==true)? $quotation->price:'';
    $bag_making = (isset($update) && $update==true)? $quotation->bag_making:'';
    $moq = (isset($update) && $update==true)? $quotation->moq:'';
    $department = (isset($update) && $update==true)? $quotation->department:'';
    $person_name = (isset($update) && $update==true)? $quotation->person_name:'';
    $designation = (isset($update) && $update==true)? $quotation->designation:'';
    $contact = (isset($update) && $update==true)? $quotation->contact:'';
    $condition_1 = (isset($update) && $update==true)? $quotation->condition_1:'';
    $condition_2 = (isset($update) && $update==true)? $quotation->condition_2:'';
    $condition_3 = (isset($update) && $update==true)? $quotation->condition_3:'';
    $condition_4 = (isset($update) && $update==true)? $quotation->condition_4:'';
    $condition_5 = (isset($update) && $update==true)? $quotation->condition_5:'';
    $yield = (isset($update) && $update==true)? $quotation->yield:'';
    $cylinder_cost = (isset($update) && $update==true)? $quotation->cylinder_cost:'';
    $size = (isset($update) && $update==true)? $quotation->size:'';
    $url = site_url();
    $actionLink = (isset($update) && $update==true) ? $url.'/Update/quotation/'.$form_type.'/'.$id:$url.'/Add/quotation/'.$form_type;

    if (User_Model::hasAccess($role)):
?>

<div class="container">
<?php echo $feed_back; ?>
    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Quotation <?php echo $form_type; ?> <i><?php echo $pageTitle; ?> </i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

<div class="row">

<div class="col-lg-12">

 <div class="expandable clearfix"> <!-- expandable -->

    <div class="expandable-head">  <!-- expandable head -->
        <h2><?php echo $pageTitle; ?></h2>
        <div class="right">
            <a href="#" class="fa fa-expand expand"></a>
        </div>
    </div>  <!-- expandable head -->

    <div class="expandable-body">  <!-- expandable BODY -->

        <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo $actionLink; ?>">
            <fieldset>
            <div class="row">
            <div class="col-sm-4">
            
            <!-- DATE -->
            <?php echo input(['name'=>'date','value'=>$date,'placeholder'=>'Select Date', 'data-date'=>"true"],true,'fa-calendar'); ?>

            <!-- MR/MS -->
            <?php echo select(['name'=>'gender','value'=>$gender,'options'=>['mr'=>'Mr.','mrs'=>'Mrs.','ms'=>'Ms.','miss'=>'Miss.']],true,'fa-user'); ?>
            
            <!-- ATTENTION -->
            <?php echo input(['name'=>'attention','value'=>$attention,'placeholder'=>'Attention Person'],true,'fa-user'); ?>

            <!-- COMPANY -->
            <?php echo input(['name'=>'party','value'=>$party,'placeholder'=>'Company Name'],true,'fa-industry'); ?>
            
            </div>

            <div class="col-sm-4">
            
            <!-- DATE -->
            <?php echo input(['name'=>'product','value'=>$product,'placeholder'=>'Type Product Name'],true,'fa-cube'); ?>

            <?php if (strtolower($form_type)!='offset'): ?>
            <!-- STRUCTURE -->
            <?php echo input(['name'=>'structure','value'=>$structure,'placeholder'=>'STRUCTURE'],true,'fa-building'); ?>

            <?php endif ?>

            <?php if (strtolower($form_type)=='offset'): ?>
                <?php echo input(['name'=>'gsm','value'=>$gsm,'placeholder'=>'G.S.M'],true,'fa-minus'); ?>
                <?php echo input(['name'=>'quality','value'=>$quality,'placeholder'=>'QUALITY OF BOARD'],true,'fa-spinner'); ?>
                <?php echo input(['name'=>'finishing','value'=>$finishing,'placeholder'=>'FINISHING'],true,'fa-suitcase'); ?>
                <?php echo input(['name'=>'other_specification','value'=>$other_specification,'placeholder'=>'OTHER SPECIFICATION'],true,'fa-ticket'); ?>
            <?php endif ?>
            <!-- PRINTING -->
            <?php echo input(['name'=>'printing','value'=>$printing,'placeholder'=>'PRINTING'],true,'fa-print'); ?>

            <!-- PRICE -->
            <?php echo input(['name'=>'price','value'=>$price,'placeholder'=>'PRICE'],true,'fa-money'); ?>

            <!-- M.O.Q -->
            <?php echo input(['name'=>'moq','value'=>$moq,'placeholder'=>'M.O.Q'],true,'fa-print'); ?>

            <?php if (strtolower($form_type)=='gravure'): ?>
                <?php echo input(['name'=>'yield','value'=>$yield,'placeholder'=>'YIELD'],true,'fa-minus'); ?>
                <?php echo input(['name'=>'cylinder_cost','value'=>$cylinder_cost,'placeholder'=>'CYLINDER'],true,'fa-spinner'); ?>
                <?php echo input(['name'=>'size','value'=>$size,'placeholder'=>'SIZE'],true,'fa-suitcase'); ?>
                <?php echo input(['name'=>'bag_making','value'=>$bag_making,'placeholder'=>'BAG MAKING'],true,'fa-suitcase'); ?>
            <?php endif ?>

            </div>
            <div class="col-sm-4">
            
            <!-- PERSON NAME -->
            <?php echo input(['name'=>'person_name','value'=>$person_name,'placeholder'=>'Name'],true,'fa-user'); ?>

            <!-- DESIGNATION -->
            <?php echo input(['name'=>'designation','value'=>$designation,'placeholder'=>'DESIGNATION'],true,'fa-wheelchair'); ?>

            <!-- CONTACT -->
            <?php echo input(['name'=>'contact','value'=>$contact,'placeholder'=>'CONTACT'],true,'fa-mobile'); ?>

            </div> <!-- col-4 -->
            </div> <!-- row -->
            <div class="row">
            <div class="col-sm-12">
            <!-- TERMS AND CONDITION -->
            <?php echo input(['name'=>'condition_1','value'=>$condition_1,'placeholder'=>'Lead time for delivery of material: '],true,'fa-unlock-alt',' Lead time for delivery of material: '); ?>
            <?php echo input(['name'=>'condition_2','value'=>$condition_2,'placeholder'=>'Payment Terms:'],true,'fa-unlock-alt',' Payment Terms: '); ?>
            <?php echo input(['name'=>'condition_3','value'=>$condition_3,'placeholder'=>'Delivery point:'],true,'fa-unlock-alt',' Delivery point: '); ?>
            <?php if (strtolower($form_type)=='gravure'): ?>
            <?php echo input(['name'=>'condition_4','value'=>$condition_4,'placeholder'=>'Cylinder Payment:'],true,'fa-unlock-alt',' Cylinder Payment: '); ?>
            <?php endif ?>
            <?php echo input(['name'=>'condition_5','value'=>$condition_5,'placeholder'=>'Validity of this Quotation:'],true,'fa-unlock-alt',' Validity of this Quotation: '); ?>

            <input type="submit" name="name" value="save" class="btn btn-success text-center" />
            </div> <!-- col-4 -->
            </div> <!-- row -->

            </fieldset>
        </form>

    </div>  <!-- expandable BODY -->

</div> <!-- expandable -->

</div> <!-- .col -->

</div> <!-- row -->

</div> <!-- container -->

<script>
    
    $('[data-date=true]').datepicker({
        dateFormat : "dd-mm-yy",
        firstDay : 1,
        dayNamesShort : ["Tue","Wed","Thu","Fri","Sut","Sun","Mon"]
    });

</script>

<?php endif; ?>