<?php

if (strtolower($form_type)=='offset'){
    $viewRole = 'viewOffsetQuotation';
    $editRole = 'editOffsetQuotation';
    $deleteRole = 'deleteOffsetQuotation';
}else{
    $viewRole = 'viewRotoQuotation';
    $editRole = 'editRotoQuotation';
    $deleteRole = 'deleteRotoQuotation';
}

?>
<!-- Tables -->
<?php echo $feed_back; ?>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Quotation <i>List, View and actions</i></h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Quotation List</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->            
                    <table id="listTable" class="table table-striped table-bordered table-hover datatable-col-4">
                        <thead>
                            <tr>
                                <th>Q#</th>
                                <th>Date</th>
                                <th>Party Name</th>
                                <th>Company</th>
                                <th>Prodcut Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($quotations as $quotation): ?>
                                <tr>
                                    <td><?php echo $quotation->id; ?></td>
                                    <td><?php echo pkDate($quotation->date); ?></td>
                                    <td><?php echo html_escape($quotation->attention); ?></td>
                                    <td><?php echo html_escape($quotation->party); ?></td>
                                    <td><?php echo html_escape($quotation->product); ?></td>
                                    <td>
                                        <div class="dropdown">
                                          <button class="btn btn-sm btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fa fa-gear"> </i> &nbsp;
                                             <span class="caret"></span>
                                          </button>
                                          <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                        <?php if(User_Model::hasAccess($viewRole)): ?>
                                        <li><a href="<?php echo site_url('Report/quotation/'.$quotation->id); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> VIEW</a></li>
                                        <?php endif; ?>
                                        <?php if(User_Model::hasAccess($viewRole)): ?>
                                        <li><a href="<?php echo site_url('Report/quotation/'.$quotation->id.'/?download=true'); ?>" target="_blank"><i class="fa fa-save"></i> Save</a></li>
                                        <?php endif; ?>
                                        <?php if(User_Model::hasAccess($editRole)): ?>
                                            <li><a href="<?php echo site_url('Update/quotation/'.$quotation->department .'/'. $quotation->id); ?>" data-request="ajax" data-action="edit"><i class="fa fa-pencil"></i> Edit</a></li>
                                        <?php endif; ?>
                                        <?php if(User_Model::hasAccess($deleteRole)): ?>
                                            <li><a href="<?php echo site_url('Delete/quotation/'.$quotation->id); ?>" data-request="ajaxs" data-action="delete"><i class="fa fa-trash"></i> Delete</a></li>
                                        <?php endif; ?>
                                          </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>

    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-4').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "autoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "order": [[ 0, "desc" ]],
                "aoColumns": [
                    { "sWidth": "5%" },
                    { "sWidth": "10%" },
                    { "sWidth": "25%" },
                    { "sWidth": "25%" },
                    { "sWidth": "25%" },
                    { "sWidth": "10%", "sClass": "center td-sm", "bSortable": false },
                ],
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-4'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>