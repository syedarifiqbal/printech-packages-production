<!-- Nav tabs for roles -->
<ul class="nav nav-tabs vertical-tabs" role="tablist">
    <?php
    $active = 0;
    foreach ($roles as $role) {
        $active++;
        echo sprintf('<li role="presentation" class="%s"><a href="#%s" aria-controls="%s" role="tab" data-toggle="tab">%s</a></li>',
            $active == 1 ? 'active' : '',
            $role->id,
            $role->role_name,
            strtoupper($role->role_name)
        );
    } ?>
    <?php if (User_Model::hasAccess("addAccountPermission")): ?>
        <li role="presentation"><a href="#new" aria-controls="new" role="tab" data-toggle="tab">New</a></li>
    <?php endif; ?>
</ul>