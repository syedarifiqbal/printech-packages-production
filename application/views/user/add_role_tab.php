<?php $active = 0;
foreach ($roles as $role) {
    $active++;
    $allowed = getPermissionsByRoleId($role->id); ?>
    <div role="tabpanel" class="tab-pane <?php echo $active == 1 ? 'active' : ''; ?>" id="<?php echo $role->id; ?>">
        <form action="<?php echo site_url('account_permission/save') ?>" method="post" name="role-<?php echo $role->id; ?>">
            <input type="hidden" name="role_id" value="<?php echo $role->id; ?>">
            <!-- Sub Panel For Department -->
            <div class="panel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <?php
                    $active = 0;
                    foreach ($permissions as $department => $permission) {
                        $active++;
                        echo sprintf('<li role="presentation" class="%s"><a href="#%s" aria-controls="%s" role="tab" data-toggle="tab">%s</a></li>',
                            $active == 1 ? 'active' : '',
                            $department . $role->id,
                            $department,
                            strtoupper($department)
                        );
                    }
                    ?>
                </ul>
                <!-- Sub Tab panes For Department -->
                <div class="tab-content" style="display: block;">
                    <?php
                    $active = 0;
                    foreach ($permissions as $department => $permission) {
                        $active++;
                        $allowed = getPermissionsByRoleId($role->id);?>
                        <div role="tabpanel" class="tab-pane <?php echo $active == 1 ? 'active' : ''; ?>" id="<?php echo $department . $role->id; ?>">
                            <table class="table table-bordered"><tr><th>Permissions</th><th>Components</th></tr>
                                <?php foreach ($permission as $group => $components) { ?>
                                    <tr>
                                        <td><?php echo $group; ?></td>
                                        <td>
                                            <?php foreach ($components as $component) { ?>
                                                <label>
                                                    <input type="checkbox" name="permissions[]" value="<?php echo $component->id; ?>" <?php echo in_array($component->id, $allowed) ? 'checked' : ''; ?>> <i></i> <?php echo $component->display_name; ?>
                                                </label>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>
                    <?php } ?>
                </div>
            </div>
            <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
            <?php if (User_Model::hasAccess("deleteAccountPermission")): ?>
                <a class="btn btn-danger" href="<?php echo site_url('account_permission/delete/' . $role->id); ?>"><i class="fa fa-trash"></i> Delete</a>
            <?php endif; ?>
        </form>
    </div>
<?php } ?>




<?php if (User_Model::hasAccess("addAccountPermission")): ?>
    <div role="tabpanel" class="tab-pane" id="new">
        <form action="<?php echo site_url('account_permission/insert') ?>" method="post" name="role-<?php echo $role->id; ?>">
            <input type="text" class="form-control" name="role_name" placeholder="Role Name">
            <textarea name="description" class="form-control" cols="30" rows="3" placeholder="Description"></textarea>
            <div class="panel">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <?php
                    $active = 0;
                    foreach ($permissions as $department => $permission) {
                        $active++;
                        echo sprintf('<li role="presentation" class="%s"><a href="#new_%s" aria-controls="%s" role="tab" data-toggle="tab">%s</a></li>',
                            $active == 1 ? 'active' : '',
                            $department,
                            $department,
                            strtoupper($department)
                        );
                    }
                    ?>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content" style="display: block;">
                    <?php
                    $active = 0;
                    foreach ($permissions as $department => $permission) {
                        $active++; ?>
                        <div role="tabpanel" class="tab-pane <?php echo $active == 1 ? 'active' : ''; ?>" id="new_<?php echo $department; ?>">
                            <table class="table table-bordered">
                                <tr><th>Permissions</th><th>Components</th></tr>
                                <?php foreach ($permission as $group => $components) { ?>
                                    <tr>
                                        <td><?php echo $group; ?></td>
                                        <td>
                                            <?php foreach ($components as $component) { ?>
                                                <label><input type="checkbox" name="permissions[]" value="<?php echo $component->id; ?>" <i></i> <?php echo $component->display_name; ?></label>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </table>
                        </div>

                    <?php } ?>
                </div>

            </div>
            <button class="btn btn-primary" type="submit"><i class="fa fa-floppy-o"></i> Save</button>
        </form>
    </div>
<?php endif; ?>