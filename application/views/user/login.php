<!DOCTYPE html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Log In Welcome To Site.!</title>

<!--
 	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:700,400' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css' />
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600' rel='stylesheet' type='text/css' />
 -->
	<!-- Styles -->
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!-- <link rel="icon" type="image/png" href="img/favicon.ico"> -->
	<link rel="stylesheet" href="font-awesome-4.0.3/css/font-awesome.css" type="text/css" /><!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/bootstrap.min.css'; ?>"><!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/font-awesome.min.css'; ?>"><!-- Font Awesome -->
	<link rel="stylesheet" href="<?php echo base_url().'assets/_css/style.css'; ?>" type="text/css" /><!-- Style -->
	<link rel="stylesheet" href="<?php echo base_url().'assets/_css/responsive.css'; ?>" type="text/css" /><!-- Responsive -->

	<style>
	.sign-in-head p{
		margin: 1em 0;
	}
	.mb{
		margin-bottom: 20px;
	}
	
	</style>

</head>

<body class="sign-in-bg">
	<div class="sign-in">
		<div class="sign-in-head black">
			<div class="sign-in-details">
				<h1>Sign up<i class="fa fa-lock"></i></h1>
				<span>or sign up</span>
			</div>
			<div class="log-in-thumb">
				<img src="<?php echo base_url(); ?>/assets/images/sign-in.jpg" alt="" />
			</div>
			<br>
			<p>Already have an account Great! LOG IN Here</p>
		</div>
		<div class="sign-in-form">
			<h5><a href="#" title="">Forgot your password?</a></h5>
			<?php echo form_open('user/login'); ?>
				<div class="clearfix mb">
					<i class="fa fa-user"></i>
					<input type="text" name="user_name" placeholder="USER NAME" />
				</div>
				<div class="clearfix ">
					<i class="fa fa-lock mb20"></i>
					<input type="password" name="hashed_password" placeholder="PASSWORD" />
				</div>

				<div class="mb" style="margin-bottom: 20px;"></div>

				<input type="submit" class="black" name="submit" value="LOG IN"/> 
			
		</div>
		<!-- <ul>
			<li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
			<li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
			<li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
		</ul> -->
	</div>
</body>

</html>
