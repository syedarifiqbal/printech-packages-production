<?php
$role = (isset($update) && $update == true) ? 'editUser' : 'addUser';
$pageTitle = (isset($update) && $update == true) ? 'Update' : 'Add';
$user_name = (isset($update) && $update == true) ? $user->user_name : '';
$role_id = (isset($update) && $update == true) ? $user->role_id : '';
$cell = (isset($update) && $update == true) ? $user->cell : '';
$active = (isset($update) && $update == true) ? $user->active : '';
$address = (isset($update) && $update == true) ? $user->address : '';

$actionLink = (isset($update) && $update == true) ? site_url("update/register_user/$user->user_id") : site_url('Add/register_user');

if (User_Model::hasAccess($role)): ?>
    <?php echo $feed_back; ?>
    <div class="container">

        <div class="row">
            <div class="col-md-10">
                <div class="heading-sec">
                    <h1><?php echo $pageTitle; ?> <i>User</i></h1>
                </div>
            </div>
        </div>

    </div>

    <div class="container">

        <div class="row">

            <div class="col-xs-12">

                <div class="expandable clearfix"> <!-- expandable -->

                    <div class="expandable-head">  <!-- expandable head -->
                        <h2><?php echo $pageTitle; ?></h2>
                        <div class="right">
                            <a href="#" class="fa fa-expand expand"></a>
                        </div>
                    </div>  <!-- expandable head -->

                    <div class="expandable-body">  <!-- expandable BODY -->

                        <form class="form-horizontal" id="formToSubmit" method="post"
                              action="<?php echo $actionLink; ?>">

                            <div class="row">

                                <div class="col-sm-6 col-md-6"> <!-- USER NAME -->
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-addon"><i class="fa fa-signal"></i></div>
                                        <input name="user_name" type="text" placeholder="USER NAME"
                                               class="form-control required" value="<?php echo $user_name; ?>">
                                    </div>
                                    <hr class="spacer"/>
                                </div> <!-- /USER NAME -->

                                <?php if (!isset($update)) { ?>
                                    <div class="col-sm-6 col-md-6"> <!-- PASSWORD -->
                                        <div class="input-group input-group-sm has-success has-feedback">
                                            <label class="input-group-addon"><i class="fa fa-trash-o"></i></label>
                                            <input name="hashed_password" type="password" placeholder="PASSWORD"
                                                   class="form-control required" value="">
                                        </div>
                                        <hr class="spacer"/>
                                    </div> <!-- /PASSWORD -->
                                <?php } ?>

                                <div class="col-sm-6 col-md-6"> <!-- MOBILE NUMBER -->
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-addon"><i class="fa fa-trash-o"></i></div>
                                        <input type="text" name="cell" placeholder="MOBILE NUMBER"
                                               class="form-control required" value="<?php echo $cell; ?>">
                                    </div>
                                    <hr class="spacer"/>
                                </div> <!-- /MOBILE NUMBER -->

                                <div class="col-sm-6 col-md-6"> <!-- ROLES -->
                                    <div class="input-group input-group-sm">
                                        <div class="input-group-addon"><i class="fa fa-key"></i> Assign Role</div>
                                        <select name="role_id" class="form-control">
                                            <?php foreach ($roles as $role) {
                                                if ($role_id == $role->id) {
                                                    echo sprintf('<option value="%s" selected>%s</option>', $role->id, $role->role_name);
                                                } else {
                                                    echo sprintf('<option value="%s">%s</option>', $role->id, $role->role_name);
                                                }
                                            } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6 col-md-6"> <!-- ACTIVE -->
                                    <input id="active" type="checkbox" <?php echo $active == 1 ? 'checked' : ''; ?>
                                           name="active" value="1">
                                    <label for="active">ACTIVE/DEACTIVE</label>
                                </div>
                                <hr class="spacer"/>

                            </div>
                            <hr class="spacer"/>

                            <div class="form-group">
                                <hr class="spacer"/>
                                <div class="col-md-2 col-md-offset-5 text-center">
                                    <button type="submit" name="submit" class="btn btn-primary btn-sm btn-block">Save
                                    </button>
                                </div>
                            </div>

                    </div> <!-- Inner row end -->

                    </form>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

    </div>

    <script>

        $(function () {
            $('h3 > [type="checkbox"]').change(function (event) {
                $(this).parent().parent().find('[type=checkbox][name*=role_id]').prop('checked', this.checked);
            });
        });

    </script>

<?php endif ?>