<?php echo $feed_back; ?>
<style>
    ul.nav.nav-tabs.vertical-tabs {
        width: 100%;
        /* overflow-X: auto; */
        height: 100%;
        border: none;
        border-right: 1px solid #ddd;
        font-size: 1.2em;
        text-transform: uppercase;
    }

    ul.nav.nav-tabs.vertical-tabs > li {
        display: block;
        float: none;
        text-align: right;
    }

    ul.nav.nav-tabs.vertical-tabs > li > a {
        display: block;
        border: none;
        /* border-bottom: 1px solid #ddd; */
    }

    ul.nav.nav-tabs.vertical-tabs > li.active > a:after {
        content: "";
        display: block;
        width: 20px;
        height: 20px;
        position: absolute;
        top: 50%;
        left: 100%;
        z-index: 1;
        border-left: 20px solid;
        border-right: 20px solid transparent;
        border-top: 20px solid transparent;
        border-bottom: 20px solid transparent;
        transform: translateY(-50%);
        margin-left: 3px;
    }

</style>
<div class="container">

    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>Account Permissions</h1>
            </div>
        </div>
    </div>

</div>

<div class="container">

    <div class="row">

        <div class="col-md-12">

            <div class="expandable clearfix"> <!-- expandable -->
                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Account Permissions</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->
                    <div class="panel">
                        <div class="row">
                            <div class="col-sm-4">
                                <?php $this->load->view('user/tab_links'); ?>                                
                            </div>
                            <div class="col-sm-8">
                                <!-- Tab panes for roles-->
                                <div class="tab-content" style="display: block;">
                                    
                                    <?php $this->load->view('user/add_role_tab'); ?>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>
            </div>

        </div>

    </div>

</div>

<script>

    //    $("ul.nav-tabs a").click(function (e) {
    //        e.preventDefault();
    //        $(this).tab('show');
    //    });

</script>