<?php if (User_Model::hasAccess('rotoSaleOrderApprove')): ?>
<style>
    table.datatable-col-9 th,
        table.datatable-col-9 td {
            white-space: nowrap;
        }

</style>
<?php echo $feed_back; ?>
<!-- Tables -->
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="heading-sec">
                <h1>UNAPPROVED SALE ORDER LIST <i>filter and actions</i></h1>
            </div>
        </div>
    </div>
</div>
<div class="container">

    <div class="row">

        <div class="col-lg-12">

             <div class="expandable clearfix"> <!-- expandable -->

                <div class="expandable-head">  <!-- expandable head -->
                    <h2>Unapproved Sale Order List</h2>
                    <div class="right">
                        <a href="#" class="fa fa-expand expand"></a>
                    </div>
                </div>  <!-- expandable head -->

                <div class="expandable-body">  <!-- expandable BODY -->

                    <table id="listTable" class="table table-striped table-bordered table-hover dt-responsive datatable-col-9">
                        <thead>
                            <tr>
                                <th>Job Code</th>
                                <th>Date</th>
                                <th>PO Date</th>
                                <th>Del Date</th>
                                <th>Customer</th>
                                <th>Job Name</th>
                                <th>Action</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($so as $row): ?>
                                <tr>
                                    <td><?php echo $row->job_code; ?></td>
                                    <td><?php echo pkDate($row->date); ?></td>
                                    <td><?php echo pkDate($row->po_date); ?></td>
                                    <td><?php echo pkDate($row->delivery_date); ?></td>
                                    <td><?php echo $row->customer_name; ?></td>
                                    <td><?php echo $row->job_name; ?></td>
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-danger dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fa fa-gear"> </i> &nbsp;
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                                <?php if ($roles['soApproval']): ?>
                                                    <li>
                                                        <form class="form-horizontal" id="formToSubmit" method="post" action="<?php echo site_url('approval/sale_order'); ?>">
                                                            <input type="hidden" value="<?php echo $row->job_code; ?>" name="job_code">
                                                            <input type="text" name="description" placeholder="Instruction If Any">
                                                            <button type="submit" name="submit"> <i class="fa fa-check"> </i> Approve</a> </button>
                                                        </form>
                                                    </li>
                                                    <li><a href="<?php echo site_url('report/sale_order/'.$row->job_code); ?>" target="_blank"><i class="fa fa-file-pdf-o"></i> View</a></li>
                                                <?php endif ?>                                                <!-- <li><a href="#">Separated link</a></li> -->
                                            </ul>
                                        </div>
                                    </td>
                                    <td><?php echo $row->hold==1?"HOLD":"OK"; ?></td>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>

                </div>  <!-- expandable BODY -->

            </div> <!-- expandable -->

        </div>

    </div>

</div>

<script>

// $('[data-toggle=tooltip]').trigger('hover');

    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    
    $('.datatable-col-9, table.tddd').dataTable({
                // "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                //         "t"+
                //         "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
                "pageLength": 25,
                "autoWidth" : false,
                "bAutoWidth" : false,
                "oLanguage": { "sSearch": '<i class="fa fa-search"></i>' } ,
                "responsive": true,
                "preDrawCallback" : function() {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper_datatable_col_reorder) {
                        responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('.datatable-col-9'), breakpointDefinition);
                    }
                },
                "rowCallback" : function(nRow) {
                    responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
                },
                "drawCallback" : function(oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                },
                fnDrawCallback : function (oSettings) {
                    responsiveHelper_datatable_col_reorder.respond();
                }
                
            });

            // $('.dt-toolbar-footer').find('ul').addClass('pagination');

</script>

<?php 
else:
    echo not_permitted();
endif; 
?>